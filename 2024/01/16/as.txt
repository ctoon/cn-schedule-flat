# 2024-01-16
 5:00 pm | Dexter's Laboratory | Dexter's Rival/Simion/Old Man Dexter
 5:30 pm | Ed, Edd n Eddy | Know It All Ed/Dear Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Waking Nightmare/Beware of the Undertoad
 6:30 pm | Courage the Cowardly Dog | The Tower of Dr. Zalost Part 1/The Tower of Dr. Zalost Part 2
 7:00 pm | King of the Hill | Pour Some Sugar on Kahn
 7:30 pm | King of the Hill | Six Characters in Search of a House
 8:00 pm | Bob's Burgers | Synchronized Swimming
 8:30 pm | Bob's Burgers | Burgerboss
 9:00 pm | Bob's Burgers | Food Truckin
 9:30 pm | American Dad! | Can I Be Frank (With You)
10:00 pm | American Dad! | American Stepdad
10:30 pm | American Dad! | Why Can't We Be Friends
11:00 pm | American Dad! | Adventures in Hayleysitting
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Rick and Morty | The Old Man and the Seat
12:30 am | Aqua Teen Hunger Force | Allen
 1:00 am | Squidbillies | Dewey Two-ey
 1:15 am | Squidbillies | Forever Autumn
 1:30 am | American Dad! | American Stepdad
 2:00 am | American Dad! | Why Can't We Be Friends
 2:30 am | American Dad! | Adventures in Hayleysitting
 3:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:30 am | Rick and Morty | The Old Man and the Seat
 4:00 am | Xavier: Renegade Angel | Weapons Grade Life
 4:15 am | Xavier: Renegade Angel | The 6th Teat of Good Intentions
 4:30 am | Infomercials | Final Deployment 4: Queen Battle Walkthrough
 5:00 am | Bob's Burgers | Burgerboss
 5:30 am | Bob's Burgers | Food Truckin