# 2024-01-12
 5:00 pm | Cow and Chicken | Field Trip to Folsom Prison/Girl's Bathroom/This Bridge Not Weasel Bridge
 5:30 pm | Cow and Chicken | Alive/Who is Super Cow?/Deep Sea Tour
 6:00 pm | Cow and Chicken | The Ugliest Weenie Part 1/I Are Big Star/The Ugliest Weenie Epilogue
 6:30 pm | Cow and Chicken | Space Cow/The Legend of Sailcat/Happy Baboon Holidays
 7:00 pm | King of the Hill | Tears of an Inflatable Clown
 7:30 pm | King of the Hill | The Minh Who Knew Too Much
 8:00 pm | Bob's Burgers | Sheesh! Cab, Bob?
 8:30 pm | Bob's Burgers | Bed & Breakfast
 9:00 pm | Bob's Burgers | Art Crawl
 9:30 pm | American Dad! | Old Stan in the Mountain
10:00 pm | American Dad! | The Wrestler
10:30 pm | American Dad! | Dr. Klaustus
11:00 pm | American Dad! | Stan's Best Friend
11:30 pm | Rick and Morty | Morty's Mind Blowers
12:00 am | The Venture Brothers | 
 2:00 am | American Dad! | Old Stan in the Mountain
 2:30 am | American Dad! | The Wrestler
 3:00 am | American Dad! | Dr. Klaustus
 3:30 am | American Dad! | Stan's Best Friend
 4:00 am | Futurama | 2-D Blacktop
 4:30 am | Futurama | Fry and Leela's Big Fling
 5:00 am | King of the Hill | Tears of an Inflatable Clown
 5:30 am | King of the Hill | The Minh Who Knew Too Much