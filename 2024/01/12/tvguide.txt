# 2024-01-12
 6:00 am | Amazing World of Gumball | The Misunderstandings
 6:14 am | Amazing World of Gumball | The Roots
 6:28 am | Amazing World of Gumball | The Blame
 6:42 am | Amazing World of Gumball | The Slap
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Talking Tree
 7:00 am | The Looney Tunes Show | Spread Those Wings and Fly
 7:29 am | Bugs Bunny Builders | Underwater Star
 7:43 am | Bugs Bunny Builders | Honey Bunny
 7:57 am | Bugs Bunny Builders: Hard Hat Time | Awesome Duck
 8:00 am | Cocomelon | 
 8:15 am | Lucas the Spider | Bye Bye Bee / Jeepers Peepers / House is Breathing
 8:42 am | Jessica's Big Little World | Bedtime Routine
 8:57 am | Meet the Batwheels | Meet the Batwheels
 9:00 am | Craig of the Creek | Dibs Court
 9:15 am | Craig of the Creek | The Mystery Of The Timekeeper
 9:30 am | We Bare Bears | $100
 9:45 am | We Bare Bears | Neighbors
10:00 am | Scooby-Doo! and Guess Who? | One Minute Mysteries!
10:30 am | Scooby-Doo! and Guess Who? | The New York Underground!
11:00 am | Amazing World of Gumball | The Fury
11:15 am | Amazing World of Gumball | The Compilation
11:30 am | Amazing World of Gumball | The Stories
11:45 am | Amazing World of Gumball | The Disaster
12:00 pm | Apple & Onion | Pancake's Bus Tour
12:15 pm | Apple & Onion | Block Party
12:30 pm | Clarence | Where the Wild Chads Are
12:45 pm | Clarence | The Big Petey Pizza Problem
 1:00 pm | Clarence | In Dreams
 1:15 pm | Clarence | Balance
 1:30 pm | Teen Titans Go! | Warner Bros 100th Anniversary
 2:00 pm | Teen Titans Go! | Booby Trap House
 2:15 pm | Teen Titans Go! | Fish Water
 2:30 pm | Teen Titans Go! | TV Knight
 2:45 pm | Teen Titans Go! | Teen Titans Save Christmas
 3:00 pm | Craig of the Creek | The Team Up
 3:15 pm | Craig of the Creek | Putting Together the Pieces
 3:30 pm | Craig of the Creek | Heart of the Forest Finale
 4:00 pm | Amazing World of Gumball | The Guy
 4:15 pm | Amazing World of Gumball | The Boredom
 4:30 pm | Amazing World of Gumball | The Vision
 4:45 pm | Amazing World of Gumball | The Choices