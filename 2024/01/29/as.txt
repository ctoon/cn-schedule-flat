# 2024-01-29
 5:00 pm | Dexter's Laboratory | D & DD/Ham Hocks n' Arm Locks
 5:30 pm | Ed, Edd n Eddy | Urban Ed/Stop, Look, Ed
 6:00 pm | Evil con Carne | Evil con Carne/Emotional Skarr/Evil Goes Wild
 6:30 pm | Courage the Cowardly Dog | So in Louvre Are We Two/Night of the Scarecrow
 7:00 pm | King of the Hill | Death of a Propane Salesman
 7:30 pm | King of the Hill | Peggy's Headache
 8:00 pm | Bob's Burgers | The Kids Run Away
 8:30 pm | Bob's Burgers | Gene It On
 9:00 pm | Bob's Burgers | Sauce Side Story
 9:30 pm | Bob's Burgers | Some Like It Bot Part 1: Eighth Grade Runner
10:00 pm | Bob's Burgers | Some Like It Bot Part 2: Judge-Bot Day
10:30 pm | American Dad! | Big Stan on Campus
11:00 pm | American Dad! | Now and Gwen
11:30 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
12:00 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
12:30 am | Aqua Teen Hunger Force | Freda
12:45 am | Aqua Teen Hunger Force | Storage Zeebles
 1:00 am | Squidbillies | Zen and the Art of Truck-Boat-Truck Maintenance
 1:15 am | Squidbillies | Who-Gives-A-Flip?
 1:30 am | American Dad! | Big Stan on Campus
 2:00 am | American Dad! | Now and Gwen
 2:30 am | American Dad! | LGBSteve
 3:00 am | Rick and Morty | Gotron Jerrysis Rickvangelion
 3:30 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
 4:00 am | Xavier: Renegade Angel | Going Normal
 4:15 am | Xavier: Renegade Angel | Kharmarabionic Lotion
 4:30 am | Infomercials | Flayaway
 4:45 am | Infomercials | When Panties Fly
 5:00 am | Bob's Burgers | Some Like It Bot Part 1: Eighth Grade Runner
 5:30 am | Bob's Burgers | Some Like It Bot Part 2: Judge-Bot Day