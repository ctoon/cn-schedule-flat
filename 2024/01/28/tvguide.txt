# 2024-01-28
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
 6:30 am | Amazing World of Gumball | The Stink
 6:45 am | Amazing World of Gumball | The Awareness
 7:00 am | Amazing World of Gumball | The Slip
 7:15 am | Amazing World of Gumball | The Drama
 7:30 am | Amazing World of Gumball | The Buddy
 7:45 am | Amazing World of Gumball | The Possession
 8:00 am | Scooby-Doo! and Guess Who? | The Lost Mines Of Kilimanjaro!
 8:30 am | Scooby-Doo! and Guess Who? | The Legend Of The Gold Microphone!
 9:00 am | Scooby-Doo! and Batman: The Brave and the Bold | 
10:30 am | We Bare Bears | Crowbar Jones: Origins
10:45 am | We Bare Bears | Tubin'
11:00 am | We Bare Bears | Lazer Royale
11:15 am | We Bare Bears | Ranger Games
11:30 am | We Bare Bears | The Perfect Tree
11:45 am | We Bare Bears | Bearz II Men
12:00 pm | The Looney Tunes Show | Bobcats on Three!
12:30 pm | The Looney Tunes Show | You've Got Hate Mail
 1:00 pm | Teen Titans Go! | Don't Be An Icarus
 1:15 pm | Teen Titans Go! | Stockton, Ca!
 1:30 pm | Teen Titans Go! | What's Opera Titans
 1:45 pm | Teen Titans Go! | Royal Jelly
 2:00 pm | Teen Titans Go! | Strength of a Grown Man
 2:15 pm | Teen Titans Go! | Had To Be There
 2:30 pm | Teen Titans Go! | The Great Disaster
 2:45 pm | Teen Titans Go! | The Viewers Decide
 3:00 pm | Amazing World of Gumball | The Master
 3:15 pm | Amazing World of Gumball | The Silence
 3:30 pm | Amazing World of Gumball | The Future
 3:45 pm | Amazing World of Gumball | The Wish
 4:00 pm | Tiny Toons Looniversity | Prank You Very Much
 4:30 pm | Tiny Toons Looniversity | Give Pizza a Chance