# 2024-01-26
 6:00 am | Amazing World of Gumball | The Kiss
 6:14 am | Amazing World of Gumball | The Party
 6:28 am | Amazing World of Gumball | The Refund
 6:42 am | Amazing World of Gumball | The Robot
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Thiefstress
 7:00 am | The Looney Tunes Show | Here Comes the Pig
 7:29 am | Bugs Bunny Builders | Cheesy Peasy
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Boogie Button
 7:46 am | Bugs Bunny Builders | Looney Science
 8:00 am | Cocomelon | Yes Yes Dress for the Rain
 8:15 am | Lucas the Spider | Too Hot to Handle; The Big Squeak; Big Jumping Spider
 8:42 am | Meet the Batwheels | Bam!
 8:45 am | Jessica's Big Little World | Small Uncle's Big Bath
 9:00 am | Craig of the Creek | Into the Overpast
 9:14 am | Craig of the Creek | The Time Capsule
 9:28 am | We Bare Bears | Pigeons
 9:42 am | We Bare Bears | Panda 2
 9:57 am | Lamput | Hypnosis
10:00 am | Scooby-Doo! and Guess Who? | Returning Of The Key Ring!
10:30 am | Scooby-Doo! and Guess Who? | Cher, Scooby And The Sargasso Sea!
11:00 am | Amazing World of Gumball | The Picnic
11:15 am | Amazing World of Gumball | The Goons
11:30 am | Amazing World of Gumball | The Secret
11:45 am | Amazing World of Gumball | The Sock
12:00 pm | Apple & Onion | Sausage and Sweetie Smash
12:15 pm | Apple & Onion | Selfish Shellfish
12:30 pm | Clarence | Cloris
12:45 pm | Clarence | Fishing Trip
 1:00 pm | Clarence | Belson's Backpack
 1:15 pm | Clarence | Motel
 1:30 pm | Craig of the Creek | Beyond the Rapids
 1:45 pm | Craig of the Creek | The Jinxening
 2:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:30 pm | Teen Titans Go! | Beast Girl
 2:45 pm | Teen Titans Go! | Bro-Pocalypse
 3:00 pm | Craig Before the Creek: An Original Movie | 