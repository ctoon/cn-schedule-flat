# 2024-01-10
 5:00 pm | Dexter's Laboratory | Inflata Dee Dee/Can't Nap/Monstory
 5:30 pm | Ed, Edd n Eddy | Who, What, Where Ed!/Keeping Up with the Eds
 6:00 pm | The Grim Adventures of Billy and Mandy | Billy Gets an "A"/Yeti or Not, Here I Come
 6:30 pm | Courage the Cowardly Dog | Car Broke, Phone Yes/Cowboy Courage
 7:00 pm | King of the Hill | The Powder Puff Boys
 7:30 pm | King of the Hill | Four Wave Intersection
 8:00 pm | Bob's Burgers | Mr. Lonely Farts
 8:30 pm | Bob's Burgers | Sheshank Redumption
 9:00 pm | Bob's Burgers | Y Tu Tina Tambien
 9:30 pm | American Dad! | Stretched Thin
10:00 pm | American Dad! | Stan Fixes a Shingle
10:30 pm | American Dad! | Cow I Met Your Moo-ther
11:00 pm | American Dad! | Better on Paper
11:30 pm | Rick and Morty | Vindicators 3: The Return of Worldender
12:00 am | Rick and Morty | The Whirly Dirly Conspiracy
12:30 am | Aqua Teen Hunger Force | Wi-Tri
12:45 am | Aqua Teen Hunger Force | Jumpy George
 1:00 am | Squidbillies | The Ballad of the Latrine Marine
 1:15 am | Squidbillies | Debased Ball
 1:30 am | American Dad! | Stan Fixes a Shingle
 2:00 am | American Dad! | Cow I Met Your Moo-ther
 2:30 am | American Dad! | Better on Paper
 3:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
 3:30 am | Rick and Morty | The Whirly Dirly Conspiracy
 4:00 am | Off the Air | Nonsense
 4:15 am | Off the Air | Bugs
 4:30 am | Ambient Swim | Things Silently Explode
 5:00 am | Bob's Burgers | Sheshank Redumption
 5:30 am | Bob's Burgers | Y Tu Tina Tambien