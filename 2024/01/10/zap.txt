# 2024-01-10
 6:00 am | Amazing World of Gumball | The Awkwardness
 6:14 am | Amazing World of Gumball | The Nest
 6:28 am | Amazing World of Gumball | The Points
 6:42 am | Amazing World of Gumball | The Bus
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Raspberry Fairy
 7:00 am | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
 7:29 am | Bugs Bunny Builders | Taz Recycle
 7:43 am | Bugs Bunny Builders | Sea School
 7:57 am | Bugs Bunny Builders: Hard Hat Time | Daffy's Spa
 8:00 am | Cocomelon | JJ Wants a New Bed
 8:14 am | Meet the Batwheels | Wheels Just Want to Have Fun
 8:28 am | Cocomelon | Wait Your Turn
 8:42 am | Jessica's Big Little World | Glow Toy
 8:57 am | Meet the Batwheels | Batwingin' It
 9:00 am | Craig of the Creek | Secret Book Club
 9:15 am | Craig of the Creek | The Takeout Mission
 9:30 am | We Bare Bears | Jean Jacket
 9:45 am | We Bare Bears | Subway
10:00 am | Scooby-Doo! and Guess Who? | Attack of the Weird Al-losaurus!
10:30 am | Scooby-Doo! and Guess Who? | The Fastest Fast Food Fiend!
11:00 am | Amazing World of Gumball | The Advice
11:15 am | Amazing World of Gumball | The Signal
11:30 am | Amazing World of Gumball | The Parasite
11:45 am | Amazing World of Gumball | The Love
12:00 pm | Apple & Onion | Apple's in Trouble
12:15 pm | Apple & Onion | 4 on 1
12:30 pm | Clarence | Jeff Wins
12:45 pm | Clarence | Turtle Hats
 1:00 pm | Clarence | Goldfish Follies
 1:15 pm | Clarence | Straight Illin
 1:30 pm | Teen Titans Go! | Kryptonite
 1:45 pm | Teen Titans Go! | Thumb War
 2:00 pm | Teen Titans Go! | Snuggle Time
 2:15 pm | Teen Titans Go! | Oh Yeah!
 2:30 pm | Teen Titans Go! | Riding the Dragon
 2:45 pm | Teen Titans Go! | The Overbite
 3:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 3:30 pm | Craig of the Creek | Who Is the Red Poncho?
 3:45 pm | Craig of the Creek | The Once and Future King
 4:00 pm | Amazing World of Gumball | The Roots
 4:15 pm | Amazing World of Gumball | The Blame
 4:30 pm | Amazing World of Gumball | The Slap
 4:45 pm | Amazing World of Gumball | The Detective