# 2024-01-09
 6:00 am | Amazing World of Gumball | The Signal
 6:14 am | Amazing World of Gumball | The Parasite
 6:28 am | Amazing World of Gumball | The Love
 6:42 am | Amazing World of Gumball | The Awkwardness
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Pretty Poodle
 7:00 am | The Looney Tunes Show | Father Figures
 7:29 am | Bugs Bunny Builders | Mail Whale
 7:43 am | Bugs Bunny Builders | Skate Park
 7:57 am | Lellobee City Farm | Stargazing Lullaby
 8:00 am | Cocomelon | 
 8:14 am | Meet the Batwheels | Batty Race
 8:28 am | Cocomelon | 
 8:42 am | Jessica's Big Little World | Small Uncle's Big Bath
 8:57 am | Lellobee City Farm | 5 Little Monkeys
 9:00 am | Craig of the Creek | Big Pinchy
 9:15 am | Craig of the Creek | Power Punchers
 9:30 am | We Bare Bears | Panda's Date
 9:45 am | We Bare Bears | Burrito
10:00 am | Scooby-Doo! and Guess Who? | Peebles' Pet Shop of Terrible Terrors!
10:30 am | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
11:00 am | Amazing World of Gumball | The Romantic
11:15 am | Amazing World of Gumball | The Uploads
11:30 am | Amazing World of Gumball | The Apprentice
11:45 am | Amazing World of Gumball | The Hug
12:00 pm | Apple & Onion | Tips
12:15 pm | Apple & Onion | Falafel's Fun Day
12:30 pm | Clarence | Neighborhood Grill
12:45 pm | Clarence | Too Gross for Comfort
 1:00 pm | Clarence | Patients
 1:15 pm | Clarence | Nothing Ventured
 1:30 pm | Teen Titans Go! | Ghost with the Most
 1:45 pm | Teen Titans Go! | Bucket List
 2:00 pm | Teen Titans Go! | TV Knight 6
 2:15 pm | Teen Titans Go! | Booty Scooty
 2:30 pm | Teen Titans Go! | Who's Laughing Now
 2:45 pm | Teen Titans Go! | Oregon Trail
 3:00 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 3:15 pm | Craig of the Creek | Capture the Flag - Part 2: The King
 3:30 pm | Craig of the Creek | Capture the Flag Part 3:The Legend
 3:45 pm | Craig of the Creek | Capture the Flag - Part 4: The Plan
 4:00 pm | Amazing World of Gumball | The Traitor
 4:15 pm | Amazing World of Gumball | The Origins
 4:30 pm | Amazing World of Gumball | The Origins Part Two
 4:45 pm | Amazing World of Gumball | The Girlfriend