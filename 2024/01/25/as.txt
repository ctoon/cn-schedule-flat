# 2024-01-25
 5:00 pm | Dexter's Laboratory | Mom and Jerry/Chubby Cheese/That Crazy Robot
 5:30 pm | Ed, Edd n Eddy | Key to My Ed/To Sir with Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Nigel Planter & The Order of the Peanuts/The Incredible Shrinking Mandy
 6:30 pm | Courage the Cowardly Dog | Feast of the Bullfrogs/Tulip's Worm
 7:00 pm | King of the Hill | Born Again on the 4th of July
 7:30 pm | King of the Hill | Serves Me Right for Giving General George S. Patton the Bathroom Key
 8:00 pm | Bob's Burgers | Seaplane!
 8:30 pm | Bob's Burgers | My Big Fat Greek Bob
 9:00 pm | Bob's Burgers | Purple Rain-Union
 9:30 pm | American Dad! | Introducing the Naughty Stewardesses
10:00 pm | American Dad! | I Ain't No Holodeck Boy
10:30 pm | American Dad! | Stan Goes on the Pill
11:00 pm | American Dad! | Honey, I'm Homeland
11:30 pm | Rick and Morty | Rickdependence Spray
12:00 am | Rick and Morty | Amortycan Grickfitti
12:30 am | Aqua Teen Hunger Force | Working Stiffs
12:45 am | Aqua Teen Hunger Force | Skins
 1:00 am | Squidbillies | No Space Like Home
 1:15 am | Squidbillies | Scorn on the 4th of July
 1:30 am | American Dad! | I Ain't No Holodeck Boy
 2:00 am | American Dad! | Stan Goes on the Pill
 2:30 am | American Dad! | Honey, I'm Homeland
 3:00 am | Rick and Morty | Rickdependence Spray
 3:30 am | Rick and Morty | Amortycan Grickfitti
 4:00 am | Xavier: Renegade Angel | Free Range Manibalism
 4:15 am | Xavier: Renegade Angel | Damnesia Vu
 4:30 am | Infomercials | Food: The Source of Life
 4:45 am | Infomercials | Pervert Everything
 5:00 am | Bob's Burgers | My Big Fat Greek Bob
 5:30 am | Bob's Burgers | Purple Rain-Union