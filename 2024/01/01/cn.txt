# 2024-01-01
 6:00 am | Amazing World of Gumball | The Gripes
 6:14 am | Amazing World of Gumball | The Vacation
 6:28 am | Amazing World of Gumball | The Fraud
 6:42 am | Amazing World of Gumball | The Void
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Prom-Date
 7:00 am | The Looney Tunes Show | Ridiculous Journey
 7:30 am | Jessica's Big Little World | Jessica's Restaurant
 7:45 am | Jessica's Big Little World | Little Twin
 8:00 am | Jessica's Big Little World | Spike the Hedgehog
 8:15 am | Jessica's Big Little World | Sleepover
 8:30 am | Jessica's Big Little World | Family Photo
 8:45 am | Jessica's Big Little World | The Big Kid Playground
 9:00 am | Craig of the Creek | Itch To Explore
 9:15 am | Craig of the Creek | Jessica Goes to the Creek
 9:30 am | Tiny Toons Looniversity | Give Pizza a Chance
10:00 am | Tiny Toons Looniversity | Extra, So Extra
10:30 am | Tiny Toons Looniversity | Save The Loo Bru
11:00 am | Tiny Toons Looniversity | General HOGspital
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
11:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
12:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
12:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
12:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
12:45 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Swoony Swanstress
 1:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
 1:30 pm | Tiny Toons Looniversity | Prank You Very Much
 2:00 pm | Tiny Toons Looniversity | Tears of a Clone
 2:30 pm | Tiny Toons Looniversity | The Show Must Hop On
 3:00 pm | The Lego Movie | 