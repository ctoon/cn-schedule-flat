# 2024-01-20
 6:00 am | Regular Show | New Year's Kiss
 6:15 am | Regular Show | Bank Shot
 6:30 am | Regular Show | Power Tower
 6:45 am | Regular Show | Return of Mordecai and the Rigbys
 7:00 am | Amazing World of Gumball | The Diet
 7:15 am | Amazing World of Gumball | The Ex
 7:30 am | Amazing World of Gumball | The Sorcerer
 7:45 am | Amazing World of Gumball | The Menu
 8:00 am | Amazing World of Gumball | The Uncle
 8:15 am | Amazing World of Gumball | The Weirdo
 8:30 am | Amazing World of Gumball | The Heist
 8:45 am | Amazing World of Gumball | The Singing
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
 9:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Swoony Swanstress
 9:30 am | Tiny Toons Looniversity | Save The Loo Bru
10:00 am | Teen Titans Go! | My Name Is Jose
10:15 am | Teen Titans Go! | The Power of Shrimps
10:30 am | Teen Titans Go! | Monster Squad!
10:45 am | Teen Titans Go! | The Real Orangins!
11:00 am | Teen Titans Go! | Quantum Fun
11:15 am | Teen Titans Go! | The Fight
11:30 am | Craig of the Creek | Back to Cool
11:45 am | Craig of the Creek | The Jump Off
12:00 pm | The Looney Tunes Show | French Fries
12:30 pm | The Looney Tunes Show | Beauty School
 1:00 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 1:30 pm | Amazing World of Gumball | The Best
 1:45 pm | Amazing World of Gumball | The Worst
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Feather Fluffer-Upper Part 1; The Prince and The Feather Fluffer-Upper Part 2
 2:30 pm | Amazing World of Gumball | The Puppets
 2:45 pm | Amazing World of Gumball | The Nuisance
 3:00 pm | Craig Before the Creek: An Original Movie | 