# 2024-01-14
 5:00 pm | Space Jam: A New Legacy | 
 7:30 pm | Bob's Burgers | Flat-Top O' the Morning to Ya
 8:00 pm | Bob's Burgers | Just the Trip
 8:30 pm | Bob's Burgers | Tappy Tappy Tappy Tap Tap Tap
 9:00 pm | American Dad! | Epic Powder Dump
 9:30 pm | American Dad! | American Dad Graffito
10:00 pm | American Dad! | Beyond the Alcove or: How I Learned to Stop Worrying and Love Klaus
10:30 pm | American Dad! | A Song of Knives and Fire
11:00 pm | Rick and Morty | Air Force Wong
11:30 pm | Rick and Morty | That's Amorte
12:00 am | Aqua Teen Hunger Force | Scrip2 2i2le: The Ts Are 2s
12:15 am | Aqua Teen Hunger Force | Shaketopia
12:30 am | 12 Oz. Mouse | Awaken
12:45 am | Hot Streets | Snake Island
 1:00 am | Momma Named Me Sheriff | Stuck
 1:15 am | Momma Named Me Sheriff | Ganley Stoodman
 1:30 am | American Dad! | American Dad Graffito
 2:00 am | American Dad! | Beyond the Alcove or: How I Learned to Stop Worrying and Love Klaus
 2:30 am | Rick and Morty | Air Force Wong
 3:00 am | Rick and Morty | That's Amorte
 3:30 am | Aqua Teen Hunger Force | Scrip2 2i2le: The Ts Are 2s
 3:45 am | Aqua Teen Hunger Force | Shaketopia
 4:00 am | 12 Oz. Mouse | Awaken
 4:15 am | Hot Streets | Snake Island
 4:30 am | Momma Named Me Sheriff | Stuck
 4:45 am | Momma Named Me Sheriff | Ganley Stoodman
 5:00 am | Bob's Burgers | Flat-Top O' the Morning to Ya
 5:30 am | Bob's Burgers | Just the Trip