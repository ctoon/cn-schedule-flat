# 2024-01-17
 6:00 am | Amazing World of Gumball | The Ollie
 6:14 am | Amazing World of Gumball | The Catfish
 6:28 am | Amazing World of Gumball | The Cycle
 6:42 am | Amazing World of Gumball | The Stars
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Moany Mountain
 7:00 am | The Looney Tunes Show | Mrs. Porkbunny's
 7:29 am | Bugs Bunny Builders | Dim Sum
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Lunar New Year
 7:46 am | Bugs Bunny Builders | Crane Game
 8:00 am | Cocomelon | 
 8:14 am | Meet the Batwheels | Parking Problems
 8:17 am | Meet the Batwheels | The Dark Night
 8:31 am | Cocomelon | 
 8:45 am | Jessica's Big Little World | Spike the Hedgehog
 9:00 am | Craig of the Creek | Kelsey The Elder
 9:14 am | Craig of the Creek | Sour Candy Trials
 9:28 am | We Bare Bears | Poppy Rangers
 9:42 am | We Bare Bears | Lucy's Brother
 9:57 am | Lamput | Hilltop
10:00 am | Scooby-Doo! and Guess Who? | The High School Wolfman's Musical Lament!
10:30 am | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital Of Dr. Phineas Phrag!
11:00 am | Amazing World of Gumball | The Grades
11:15 am | Amazing World of Gumball | The Diet
11:30 am | Amazing World of Gumball | The Ex
11:45 am | Amazing World of Gumball | The Sorcerer
12:00 pm | Apple & Onion | Gyranoid of the Future
12:15 pm | Apple & Onion | Fun Proof
12:30 pm | Clarence | Plane Excited
12:45 pm | Clarence | Escape from Beyond the Cosmic
 1:00 pm | Clarence | Ren Faire
 1:15 pm | Clarence | Time Crimes
 1:30 pm | Craig of the Creek | Fort Williams
 1:45 pm | Craig of the Creek | Summer Wish
 2:00 pm | Teen Titans Go! | Movie Night
 2:15 pm | Teen Titans Go! | Permanent Record
 2:30 pm | Teen Titans Go! | Titan Saving Time
 2:45 pm | Teen Titans Go! | The Gold Standard
 3:00 pm | Amazing World of Gumball | The Uncle
 3:15 pm | Amazing World of Gumball | The Menu
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
 3:45 pm | Amazing World of Gumball | The Weirdo
 4:00 pm | Amazing World of Gumball | The Heist
 4:15 pm | Amazing World of Gumball | The Singing
 4:30 pm | Regular Show | Butt Dial
 4:45 pm | Regular Show | Eggscellent