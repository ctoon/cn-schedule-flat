# 2024-01-15
 6:00 am | Craig of the Creek | Capture the Flag Part 1: The Candy
 6:14 am | Craig of the Creek | Capture the Flag Part 2: The King
 6:28 am | Craig of the Creek | Capture the Flag Part 3: The Legend
 6:42 am | Craig of the Creek | Capture the Flag Part 4: The Plan
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Prom-Date
 7:00 am | Craig of the Creek | Capture the Flag Part 5: The Game
 7:30 am | Jessica's Big Little World | Jessica's Restaurant
 7:45 am | Jessica's Big Little World | Little Twin
 8:00 am | Jessica's Big Little World | Spike the Hedgehog
 8:15 am | Jessica's Big Little World | Sleepover
 8:30 am | Jessica's Big Little World | Family Photo
 8:45 am | Jessica's Big Little World | Small Uncle's Big Bath
 9:00 am | Craig of the Creek | Beyond the Overpass
 9:14 am | Craig of the Creek | The Chef's Challenge
 9:28 am | Craig of the Creek | Chrono Moss
 9:42 am | Craig of the Creek | In Search of Lore
 9:57 am | Lamput | Doc Dog
10:00 am | Craig of the Creek | Adventures in Baby Casino
10:15 am | Craig of the Creek | Hyde & Zeke
10:30 am | Craig of the Creek | Lost & Found
10:45 am | Craig of the Creek | Craig to the Future
11:00 am | Craig of the Creek | The Jump Off
11:15 am | Craig of the Creek | My Stare Lady
11:30 am | Craig of the Creek | Craig of the Campus
11:45 am | Craig of the Creek | Bernard of the Creek, Part 1
12:00 pm | Craig of the Creek | Bernard of the Creek, Part 2
12:15 pm | Craig of the Creek | Left in the Dark
12:30 pm | Craig Before the Creek: An Original Movie | 
 2:30 pm | Space Jam: A New Legacy | 