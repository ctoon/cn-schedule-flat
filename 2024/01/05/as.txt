# 2024-01-05
 5:00 pm | Ed, Edd n Eddy | Hot Buttered Ed/High Heeled Ed
 5:30 pm | Ed, Edd n Eddy | The Ed-Touchables/Nagged to Ed
 6:00 pm | Ed, Edd n Eddy | Pop Goes the Ed/Over Your Ed
 6:30 pm | Ed, Edd n Eddy | Sir Ed-A-Lot/A Pinch to Grow an Ed
 7:00 pm | King of the Hill | SerPUNt
 7:30 pm | King of the Hill | Blood and Sauce
 8:00 pm | Bob's Burgers | Bob Belcher and the Terrible, Horrible, No Good, Very Bad Kids
 8:30 pm | Bob's Burgers | The Terminalator II: Terminals of Endearment
 9:00 pm | Bob's Burgers | Mommy Boy
 9:30 pm | American Dad! | The People vs. Martin Sugar
10:00 pm | American Dad! | Fartbreak Hotel
10:30 pm | American Dad! | Stanny-Boy and Frantastic
11:00 pm | American Dad! | A Pinata Named Desire
11:30 pm | Rick and Morty | Look Who's Purging Now
12:00 am | Aqua Teen Hunger Force | 
 1:45 am | Aqua Teen Hunger Force | Shaketopia
 2:00 am | American Dad! | The People vs. Martin Sugar
 2:30 am | American Dad! | Fartbreak Hotel
 3:00 am | American Dad! | Stanny-Boy and Frantastic
 3:30 am | American Dad! | A Pinata Named Desire
 4:00 am | Futurama | Near-Death Wish
 4:30 am | Futurama | 31st Century Fox
 5:00 am | King of the Hill | SerPUNt
 5:30 am | King of the Hill | Blood and Sauce