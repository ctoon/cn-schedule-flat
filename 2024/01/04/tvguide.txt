# 2024-01-04
 6:00 am | Amazing World of Gumball | The Bros
 6:14 am | Amazing World of Gumball | The Mirror
 6:28 am | Amazing World of Gumball | The Man
 6:42 am | Amazing World of Gumball | The Pizza
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Hissy Swan
 7:00 am | The Looney Tunes Show | Best Friends Redux
 7:29 am | Bugs Bunny Builders | Blast Off
 7:43 am | Bugs Bunny Builders | Cousin Billy
 7:57 am | Lellobee City Farm | Do the ABC Dance!
 8:00 am | Cocomelon | 
 8:14 am | Meet the Batwheels | Stop That Ducky
 8:28 am | Cocomelon | 
 8:42 am | Jessica's Big Little World | Spike the Hedgehog
 8:57 am | Lellobee City Farm | Animal Sounds on the farm (I Hear With My Little Ear)
 9:00 am | Craig of the Creek | Lost in the Sewer
 9:14 am | Craig of the Creek | The Brood
 9:28 am | We Bare Bears | Baby Bears on a Plane
 9:42 am | We Bare Bears | Yuri and the Bear
 9:57 am | Lamput | Thief
10:00 am | Scooby-Doo! and Guess Who? | Scooby-Doo And The Sky Town Cool School!
10:30 am | Scooby-Doo! and Guess Who? | Falling Star Man!
11:00 am | Amazing World of Gumball | The Lie
11:15 am | Amazing World of Gumball | The Butterfly
11:30 am | Amazing World of Gumball | The Question
11:45 am | Amazing World of Gumball | The Saint
12:00 pm | Apple & Onion | Jerk Chicken
12:15 pm | Apple & Onion | Open House Cookies
12:30 pm | Clarence | Video Store
12:45 pm | Clarence | Anywhere but Sumo
 1:00 pm | Clarence | Fun Dungeon Face Off
 1:15 pm | Clarence | Money Broom Wizard
 1:30 pm | Craig of the Creek | Monster in the Garden
 1:45 pm | Craig of the Creek | Dog Decider
 2:00 pm | Teen Titans Go! | Obinray
 2:15 pm | Teen Titans Go! | Wally T
 2:30 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 2:45 pm | Teen Titans Go! | History Lesson
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 3:15 pm | Amazing World of Gumball | The Friend
 3:30 pm | Amazing World of Gumball | The Oracle
 3:45 pm | Amazing World of Gumball | The Safety
 4:00 pm | Amazing World of Gumball | The Society
 4:15 pm | Amazing World of Gumball | The Spoiler
 4:30 pm | Regular Show | Stick Hockey
 4:45 pm | Regular Show | Bet to Be Blonde