# 2024-01-04
 5:00 pm | Dexter's Laboratory | Doll House Drama/Krunk's Date/The Big Cheese
 5:30 pm | Ed, Edd n Eddy | Fool on the Ed/A Boy and His Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | The Crass Unicorn/Billy & Mandy Begins
 6:30 pm | Courage the Cowardly Dog | Human Habitrail/Mission to the Sun
 7:00 pm | King of the Hill | Edu-macating Lucky
 7:30 pm | King of the Hill | The Peggy Horror Picture Show
 8:00 pm | Bob's Burgers | Worms of In-Rear-Ment
 8:30 pm | Bob's Burgers | Copa-Bob-Bana
 9:00 pm | Bob's Burgers | Fast Time Capsules at Wagstaff School
 9:30 pm | American Dad! | Great Space Roaster
10:00 pm | American Dad! | Son of Stan
10:30 pm | American Dad! | Stan's Food Restaurant
11:00 pm | American Dad! | White Rice
11:30 pm | Rick and Morty | Big Trouble in Little Sanchez
12:00 am | Rick and Morty | Interdimensional Cable 2: Tempting Fate
12:30 am | Aqua Teen Hunger Force | Juggalo
12:45 am | Aqua Teen Hunger Force | Multiple Meat
 1:00 am | Squidbillies | Squash B'Gosh
 1:15 am | Squidbillies | The Guzzle Bumpkin
 1:30 am | American Dad! | Son of Stan
 2:00 am | American Dad! | Stan's Food Restaurant
 2:30 am | American Dad! | White Rice
 3:00 am | Rick and Morty | Big Trouble in Little Sanchez
 3:30 am | Rick and Morty | Interdimensional Cable 2: Tempting Fate
 4:00 am | Off the Air | Dreams
 4:15 am | Off the Air | Progress
 4:30 am | Ambient Swim | Tradigital
 5:00 am | Bob's Burgers | Copa-Bob-Bana
 5:30 am | Bob's Burgers | Fast Time Capsules at Wagstaff School