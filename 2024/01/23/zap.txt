# 2024-01-23
 6:00 am | Amazing World of Gumball | The Master
 6:14 am | Amazing World of Gumball | The Silence
 6:28 am | Amazing World of Gumball | The Future
 6:42 am | Amazing World of Gumball | The Wish
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
 7:00 am | The Looney Tunes Show | The Shell Game
 7:29 am | Bugs Bunny Builders | Tweety-Go-Round
 7:43 am | Lellobee City Farm | Back to School
 7:46 am | Bugs Bunny Builders | Smash House
 8:00 am | Cocomelon | My Big Brother Song
 8:14 am | Lellobee City Farm | Down by the Lellobee Farm
 8:17 am | Meet the Batwheels | A Jet Out of Water
 8:31 am | Cocomelon | Basketball Song
 8:45 am | Jessica's Big Little World | Bedtime Routine
 9:00 am | Craig of the Creek | The Cardboard Identity
 9:14 am | Craig of the Creek | Ancients of the Creek
 9:28 am | We Bare Bears | Dog Hotel
 9:42 am | We Bare Bears | Bear Lift
 9:57 am | Lamput | Fracture
10:00 am | Scooby-Doo! and Guess Who? | The Internet on Haunted House Hill!
10:30 am | Scooby-Doo! and Guess Who? | The 7th Inning Scare!
11:00 am | Amazing World of Gumball | The Factory
11:15 am | Amazing World of Gumball | The Agent
11:30 am | Amazing World of Gumball | The Ghouls
11:45 am | Amazing World of Gumball | The Web
12:00 pm | Apple & Onion | Onionless
12:15 pm | Apple & Onion | Party Popper
12:30 pm | Clarence | The Substitute
12:45 pm | Clarence | Classroom
 1:00 pm | Clarence | Dullance
 1:15 pm | Clarence | Jeff's Secret
 1:30 pm | Craig of the Creek | Mortimor to the Rescue
 1:45 pm | Craig of the Creek | Secret in a Bottle
 2:00 pm | Teen Titans Go! | Hot Salad Water
 2:15 pm | Teen Titans Go! | Lication
 2:30 pm | Teen Titans Go! | Labor Day
 2:45 pm | Teen Titans Go! | Classic Titans
 3:00 pm | Amazing World of Gumball | The Heart
 3:15 pm | Amazing World of Gumball | The Mess
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 3:45 pm | Amazing World of Gumball | The Revolt
 4:00 pm | Amazing World of Gumball | The Decisions
 4:15 pm | Amazing World of Gumball | The BFFS
 4:30 pm | Regular Show | Fists of Justice
 4:45 pm | Regular Show | Yes Dude Yes