# 2024-01-13
 6:00 am | Amazing World of Gumball | The Re-Run
 6:15 am | Amazing World of Gumball | The Guy
 6:30 am | Amazing World of Gumball | The Boredom
 6:45 am | Amazing World of Gumball | The Vision
 7:00 am | Amazing World of Gumball | The Choices
 7:15 am | Amazing World of Gumball | The Code
 7:30 am | Amazing World of Gumball | The Scam
 7:45 am | Amazing World of Gumball | The Test
 8:00 am | Amazing World of Gumball | The Slide
 8:15 am | Amazing World of Gumball | The Loophole
 8:30 am | Amazing World of Gumball | The Copycats
 8:45 am | Amazing World of Gumball | The Potato
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 9:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 9:30 am | Tiny Toons Looniversity | Episode 1
10:00 am | Craig Before the Creek: An Original Movie | 
12:00 pm | The Looney Tunes Show | The DMV
12:30 pm | Tiny Toons Looniversity | Prank You Very Much
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Wise Wizard of Wisdom
 1:15 pm | Amazing World of Gumball | The Blame
 1:30 pm | Amazing World of Gumball | The Fuss
 1:45 pm | Amazing World of Gumball | The Outside
 2:00 pm | Amazing World of Gumball | The Vase
 2:15 pm | Amazing World of Gumball | The Matchmaker
 2:30 pm | Amazing World of Gumball | The Box
 2:45 pm | Amazing World of Gumball | The Console
 3:00 pm | Scooby-Doo! and the Alien Invaders | 
 4:30 pm | Regular Show | Really Real Wrestling
 4:45 pm | Regular Show | More Smarter