# 2024-02-04
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 6:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny?
 6:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Leslie?
 7:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert?
 7:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Anyone?
 7:30 am | Amazing World of Gumball | The Debt
 7:45 am | Amazing World of Gumball | The End
 8:00 am | Scooby-Doo! and Guess Who? | Revenge of the Swamp Monster!
 8:30 am | Scooby-Doo! and Guess Who? | Peebles' Pet Shop of Terrible Terrors!
 9:00 am | Scooby-Doo! and WWE: Curse of the Speed Demon | 
10:45 am | New Looney Tunes | Buddha Bugs/Now and Zen
11:00 am | The Looney Tunes Show | Semper Lie
11:30 am | The Looney Tunes Show | Father Figures
12:00 pm | Tom & Jerry Show | Spike Gets Skooled; Cat's Ruffled Fur-niture
12:30 pm | Tom & Jerry Show | Sleep Disorder; Tom's In-Tents Adventure
 1:00 pm | We Bare Bears | Bear Squad
 1:15 pm | We Bare Bears | Lil' Squid
 1:30 pm | We Bare Bears | I, Butler
 1:45 pm | We Bare Bears | Family Troubles
 2:00 pm | Steven Universe | Gem Glow
 2:15 pm | Steven Universe | Laser Light Cannon
 2:30 pm | Steven Universe | Cheeseburger Backpack
 2:45 pm | Steven Universe | Together Breakfast
 3:00 pm | Adventure Time | Slumber Party Panic; Trouble in Lumpy Space
 3:30 pm | Adventure Time | Enchiridion; The Jiggler
 4:00 pm | Regular Show | The Power
 4:15 pm | Regular Show | Just Set Up the Chairs
 4:30 pm | Regular Show | Caffeinated Concert Tickets
 4:45 pm | Regular Show | Death Punchies