# 2024-02-09
 6:00 am | Amazing World of Gumball | The Apprentice
 6:15 am | Amazing World of Gumball | The Hug
 6:29 am | Amazing World of Gumball | The Wicked
 6:43 am | Amazing World of Gumball | The Traitor
 6:57 am | Lamput | Fracture
 7:00 am | Tiny Toons Looniversity | Soufflé, Girl Hey
 7:29 am | Bugs Bunny Builders | Mail Whale
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Lunar New Year
 7:46 am | Bugs Bunny Builders | Honey Bunny
 8:00 am | CoComelon | Twinkle Little Star/ Sharing Song/ London Bridge/ Rain Go Away (Indoors)
 8:15 am | Lucas the Spider | Boingo / Findley's Bad Day / You Rang
 8:42 am | Lellobee City Farm | Ella's Campfire Sing-A-Long
 8:45 am | Jessica's Big Little World | Glow Toy
 9:00 am | Craig of the Creek | Capture the Flag Part V: The Game
 9:29 am | We Bare Bears | Escandalosos
 9:43 am | We Bare Bears | Pizza Band
 9:57 am | Lamput | Art Gallery
10:00 am | Scooby-Doo! and Guess Who? | The Nightmare Ghost of Psychic U!
10:30 am | Scooby-Doo! and Guess Who? | Quit Clowning!
11:00 am | Amazing World of Gumball | The Saint
11:15 am | Amazing World of Gumball | The Friend
11:30 am | Amazing World of Gumball | The Oracle
11:45 am | Amazing World of Gumball | The Safety
12:00 pm | Apple & Onion | Falafel's Glory
12:15 pm | Apple & Onion | Slobbery
12:30 pm | Clarence | Money Broom Wizard
12:45 pm | Clarence | Lost in the Supermarket
 1:00 pm | Clarence | Clarence's Millions
 1:15 pm | Clarence | Clarence Gets a Girlfriend
 1:30 pm | Craig of the Creek | Sink or Swim Team
 1:45 pm | Craig of the Creek | The Quick Name
 2:00 pm | Teen Titans Go! | Zimdings
 2:15 pm | Teen Titans Go! | Pig in a Poke
 2:30 pm | Teen Titans Go! | P.P.
 2:45 pm | Teen Titans Go! | A Little Help Please
 3:00 pm | Amazing World of Gumball | The Origins Part 2
 3:15 pm | Amazing World of Gumball | The Origins
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 3:45 pm | Amazing World of Gumball | The Girlfriend
 4:00 pm | Amazing World of Gumball | The Advice
 4:15 pm | Amazing World of Gumball | The Signal
 4:30 pm | Regular Show | Ace Balthazar Lives
 4:45 pm | Regular Show | Do or Diaper