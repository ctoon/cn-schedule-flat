# 2024-02-09
 5:00 pm | I Am Weasel | I Are Terraformer/I Am Viking/The Drinking Fountain of Youth
 5:30 pm | I Am Weasel | Leave It To Weasel/Fairy Godfather/I.R. Robin Hood
 6:00 pm | I Am Weasel | The Incredible Shrinking Weasel!/Baboon Man and Boy Weasel/I.M.N. Love
 6:30 pm | I Am Weasel | I Am Cave Weasel/My Blue Hiney/Mission: Stupid
 7:00 pm | King of the Hill | The Passion of Dauterive
 7:30 pm | King of the Hill | Lucky's Wedding Suit
 8:00 pm | Bob's Burgers | Bye Bye Boo Boo
 8:30 pm | Bob's Burgers | The Horse Rider-Er
 9:00 pm | Bob's Burgers | Secret Admiral-Irer
 9:30 pm | American Dad! | Father's Daze
10:00 pm | American Dad! | Fight and Flight
10:30 pm | American Dad! | The Enlightenment of Ragi-Baba
11:00 pm | American Dad! | Portrait of Francine's Genitals
11:30 pm | Rick and Morty | Mortynight Run
12:00 am | Eric Andre Live Near Broadway | 
12:30 am | The Eric Andre Show | T.I.; Abbey Lee Miller
12:45 am | The Eric Andre Show | Jack Black; Jennette McCurdy
 1:00 am | The Eric Andre Show | Lizzo Up
 1:15 am | The Eric Andre Show | The 50th Episode!
 1:30 am | The Eric Andre Show | Don't You Say a Word
 1:45 am | The Eric Andre Show | The Cold Episode
 2:00 am | Rick and Morty | Mortynight Run
 2:30 am | Rick and Morty | Rickfending Your Mort
 3:00 am | American Dad! | The Enlightenment of Ragi-Baba
 3:30 am | American Dad! | Portrait of Francine's Genitals
 4:00 am | Futurama | Rebirth
 4:30 am | Futurama | Proposition Infinity
 5:00 am | King of the Hill | The Passion of Dauterive
 5:30 am | King of the Hill | Lucky's Wedding Suit