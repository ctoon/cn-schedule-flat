# 2024-02-12
 5:00 pm | Dexter's Laboratory | Glove at First Sight/A Mom and Dad Cartoon/Smells Like Victory
 5:30 pm | Ed, Edd n Eddy | The Ed-Touchables/Nagged to Ed
 6:00 pm | Evil con Carne | Son of Evil/The Right to Bear Arms/The Trouble with Skarrina
 6:30 pm | Courage the Cowardly Dog | Last of the Star Makers/Son of the Chicken From Outer Space
 7:00 pm | King of the Hill | Three Men and a Bastard
 7:30 pm | King of the Hill | The Boy Can't Help It
 8:00 pm | Bob's Burgers | Bob Actually
 8:30 pm | Bob's Burgers | V for Valentine-detta
 9:00 pm | Bob's Burgers | Bed, Bob & Beyond
 9:30 pm | Bob's Burgers | Romancing the Beef
10:00 pm | Bob's Burgers | Ferry on My Wayward Bob and Linda
10:30 pm | American Dad! | Whole Slotta Love
11:00 pm | American Dad! | Casino Normale
11:30 pm | Rick and Morty | Auto Erotic Assimilation
12:00 am | Rick and Morty | Total Rickall
12:30 am | The Boondocks | Guess Hoe's Coming to Dinner
 1:00 am | Mike Tyson Mysteries | Ring of Fire
 1:15 am | Mike Tyson Mysteries | A Dog's Life
 1:30 am | American Dad! | Kiss Kiss, Cam Cam
 2:00 am | American Dad! | Whole Slotta Love
 2:30 am | American Dad! | Casino Normale
 3:00 am | Rick and Morty | Auto Erotic Assimilation
 3:30 am | Rick and Morty | Total Rickall
 4:00 am | The Boondocks | Guess Hoe's Coming to Dinner
 4:30 am | Black Jesus | Fried Green Tomatoes
 5:00 am | Bob's Burgers | Bob Actually
 5:30 am | Bob's Burgers | V for Valentine-detta