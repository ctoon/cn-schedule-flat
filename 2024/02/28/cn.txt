# 2024-02-28
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Penny?
 6:14 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Leslie?
 6:28 am | The Looney Tunes Show | Off Duty Cop
 6:56 am | Tom & Jerry (APAC Shorts) | What Goes Around Comes Around
 7:00 am | The Looney Tunes Show | Devil Dog
 7:29 am | Bugs Bunny Builders | K-9: Space Puppy
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Hard Hat Time
 7:46 am | Bugs Bunny Builders | Cousin Billy
 8:00 am | Meet the Batwheels | Tough Buff Blues
 8:14 am | Meet the Batwheels | Bad to the Chrome
 8:17 am | Meet the Batwheels | Buff's BFF
 8:31 am | We Baby Bears | Bears in the Dark
 8:45 am | We Baby Bears | Big Trouble Little Babies
 9:00 am | Craig of the Creek | Jessica Goes to the Creek
 9:14 am | Craig of the Creek | The Final Book
 9:28 am | We Bare Bears | Emergency
 9:42 am | We Bare Bears | Tote Life
 9:56 am | Tom & Jerry (APAC Shorts) | What Goes Around Comes Around
10:00 am | Scooby-Doo! and Guess Who? | The Legend of the Gold Microphone!
10:30 am | Scooby-Doo! and Guess Who? | Total Jeopardy!
11:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Penny?
11:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Leslie?
11:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Bobert?
11:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Anyone?
12:00 pm | Apple & Onion | World Cup
12:15 pm | Apple & Onion | Win It or Bin It
12:30 pm | Clarence | Plane Excited
12:45 pm | Clarence | Escape from Beyond the Cosmic
 1:00 pm | Clarence | Ren Faire
 1:15 pm | Clarence | Time Crimes
 1:30 pm | Craig of the Creek | Too Many Treasures
 1:45 pm | Craig of the Creek | Wildernessa
 2:00 pm | Teen Titans Go! | Our House
 2:15 pm | Teen Titans Go! | Beard Hunter
 2:30 pm | Teen Titans Go! | New Chum
 2:45 pm | Teen Titans Go! | Negative Feels
 3:00 pm | Amazing World of Gumball | The Debt
 3:15 pm | Amazing World of Gumball | The End
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Playdate
 4:00 pm | Amazing World of Gumball | The Dress
 4:15 pm | Amazing World of Gumball | The Quest
 4:30 pm | Regular Show | Benson's Car
 4:45 pm | Regular Show | Every Meat Burritos