# 2024-02-15
 6:00 am | Amazing World of Gumball | The Fuss
 6:15 am | Amazing World of Gumball | The Potato
 6:29 am | Amazing World of Gumball | The Outside
 6:43 am | Amazing World of Gumball | The Vase
 6:57 am | Lamput | Alien
 7:00 am | The Looney Tunes Show | Year of the Duck
 7:29 am | Bugs Bunny Builders | Ice Creamed
 7:43 am | Lellobee City Farm | Yes Yes Vegetables Song
 7:46 am | Bugs Bunny Builders | Race Track Race
 8:00 am | CoComelon | Where's My Little Dog Gone?/ Opposites Song/ The Muffin Man/ Thank You Song
 8:15 am | Meet the Batwheels | Mission: Stuffed Animal
 8:29 am | CoComelon | Looby Loo/ Winter Song (Fun in the Snow)/ Hello Song/ My Mommy Song
 8:43 am | Jessica's Big Little World | Jessica's Restaurant
 8:57 am | Lellobee City Farm | L-L-Lellogames
 9:00 am | Craig of the Creek | The Anniversary Box
 9:15 am | Craig of the Creek | Lost & Found
 9:29 am | We Bare Bears | Ranger Norm
 9:43 am | We Bare Bears | Shmorby
 9:57 am | Lamput | Shape Shift
10:00 am | Scooby-Doo! and Guess Who? | Dance Matron of Mayhem!
10:30 am | Scooby-Doo! and Guess Who? | A Run Cycle Through Time!
11:00 am | Amazing World of Gumball | The Parasite
11:15 am | Amazing World of Gumball | The Love
11:30 am | Amazing World of Gumball | The Awkwardness
11:45 am | Amazing World of Gumball | The Nest
12:00 pm | Apple & Onion | Za
12:15 pm | Apple & Onion | Broccoli
12:30 pm | Clarence | Lizard Day Afternoon
12:45 pm | Clarence | The Forgotten
 1:00 pm | Clarence | Neighborhood Grill
 1:15 pm | Clarence | Belson's Sleepover
 1:30 pm | Craig of the Creek | Grandma Smugglers
 1:45 pm | Craig of the Creek | The Champion's Hike
 2:00 pm | Teen Titans Go! | Batman's Birthday Gift
 2:15 pm | Teen Titans Go! | What a Boy Wonders
 2:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 3:00 pm | Amazing World of Gumball | The Box
 3:15 pm | Amazing World of Gumball | The Matchmaker
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 3:45 pm | Amazing World of Gumball | The Console
 4:00 pm | Amazing World of Gumball | The Ollie
 4:15 pm | Amazing World of Gumball | The Catfish
 4:30 pm | Regular Show | Picking Up Margaret
 4:45 pm | Regular Show | K.I.L.I.T. Radio