# 2024-02-11
 5:00 pm | Futurama | Parasites Lost
 5:30 pm | Futurama | The Devil's Hands Are Idle Playthings
 6:00 pm | Futurama | The Beast with a Billion Backs Part 1
 6:30 pm | Futurama | The Beast with a Billion Backs Part 2
 7:00 pm | Futurama | The Beast with a Billion Backs Part 3
 7:30 pm | Futurama | The Beast with a Billion Backs Part 4
 8:00 pm | Bob's Burgers | The Gene & Courtney Show
 8:30 pm | Bob's Burgers | Bob Actually
 9:00 pm | American Dad! | Roger Passes the Bar
 9:30 pm | American Dad! | Blagsnarst: A Love Story
 9:30 pm | American Dad! | Blagsnarst: A Love Story
10:00 pm | American Dad! | My Affair Lady
10:30 pm | American Dad! | Kiss Kiss, Cam Cam
11:00 pm | Rick and Morty | How Poopy Got His Poop Back
11:30 pm | Rick and Morty | The Jerrick Trap
12:00 am | Royal Crackers | Casa de Darby
12:30 am | Momma Named Me Sheriff | Good Guys
12:45 am | Momma Named Me Sheriff | Sheriff and Roach
 1:00 am | Hot Streets | Operation: Large and in Charge
 1:15 am | 12 Oz. Mouse | Reveal
 1:30 am | American Dad! | Blagsnarst: A Love Story
 1:30 am | American Dad! | Blagsnarst: A Love Story
 2:00 am | American Dad! | My Affair Lady
 2:30 am | Rick and Morty | How Poopy Got His Poop Back
 3:00 am | Rick and Morty | The Jerrick Trap
 3:30 am | Royal Crackers | Casa de Darby
 4:00 am | 12 Oz. Mouse | Reveal
 4:15 am | Hot Streets | Operation: Large and in Charge
 4:30 am | Momma Named Me Sheriff | Good Guys
 4:45 am | Momma Named Me Sheriff | Sheriff and Roach
 5:00 am | Bob's Burgers | My Fuzzy Valentine
 5:30 am | Bob's Burgers | Can't Buy Me Math