# 2024-02-20
 6:00 am | Amazing World of Gumball | The List
 6:15 am | Amazing World of Gumball | The News
 6:29 am | Amazing World of Gumball | The Rival
 6:43 am | Amazing World of Gumball | The Lady
 6:57 am | Lamput | Martial Art
 7:00 am | The Looney Tunes Show | Best Friends Redux
 7:29 am | Bugs Bunny Builders | Play Day
 7:43 am | Lellobee City Farm | Harvest Festival
 7:46 am | Bugs Bunny Builders | Rock On
 8:00 am | Cocomelon | Hide and Go Seek in the Snow (Jingle Bells)
 8:14 am | Meet the Batwheels | Mission: Stuffed Animal
 8:29 am | Cocomelon | Getting Ready for School Song
 8:43 am | Jessica's Big Little World | Small Uncle's Big Bath
 8:57 am | Lellobee City Farm | Jelly Jam Jam
 9:00 am | Craig of the Creek | Back to Cool
 9:15 am | Craig of the Creek | The Jump Off
 9:29 am | We Bare Bears | Burrito
 9:43 am | We Bare Bears | Primal
 9:57 am | Lamput | Doc Dog
10:00 am | Scooby-Doo! and Guess Who? | The Feast of Dr. Frankenfooder!
10:30 am | Scooby-Doo! and Guess Who? | A Fashion Nightmare!
11:00 am | Amazing World of Gumball | The List
11:15 am | Amazing World of Gumball | The News
11:30 am | Amazing World of Gumball | The Rival
11:45 am | Amazing World of Gumball | The Lady
12:00 pm | Apple & Onion | A Matter of Pride
12:15 pm | Apple & Onion | Eyesore a Sunset
12:30 pm | Clarence | Dust Buddies
12:45 pm | Clarence | Hoofin' It
 1:00 pm | Clarence | Hairence
 1:15 pm | Clarence | Chalmers Santiago
 1:30 pm | Craig of the Creek | The Cow-Boy and Marie
 1:45 pm | Craig of the Creek | The Cursed Word
 2:00 pm | Teen Titans Go! | The Drip
 2:15 pm | Teen Titans Go! | Standards & Practices
 2:30 pm | Teen Titans Go! | Belly Math
 2:45 pm | Teen Titans Go! | Free Perk
 3:00 pm | Amazing World of Gumball | The Vegging
 3:15 pm | Amazing World of Gumball | The Sucker
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
 3:45 pm | Amazing World of Gumball | The One
 4:00 pm | Amazing World of Gumball | The Father
 4:15 pm | Amazing World of Gumball | The Cringe
 4:30 pm | Regular Show | Meteor Moves
 4:45 pm | Regular Show | Family BBQ