# 2024-02-22
 6:00 am | Amazing World of Gumball | The Founder
 6:14 am | Amazing World of Gumball | The Schooling
 6:28 am | Amazing World of Gumball | The Intelligence
 6:42 am | Amazing World of Gumball | The Potion
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Pretty Poodle
 7:00 am | The Looney Tunes Show | Best Friends
 7:29 am | Bugs Bunny Builders | Cheesy Peasy
 7:43 am | Lellobee City Farm | Jelly Jam Jam
 7:46 am | Bugs Bunny Builders | Looney Science
 8:00 am | Cocomelon | John Jacob Jingleheimer Schmidt
 8:14 am | Meet the Batwheels | Bat-Light Blow-Out
 8:29 am | Cocomelon | Funny Face Song
 8:43 am | Jessica's Big Little World | Grocery Store Friend
 8:57 am | Lellobee City Farm | Harvest Festival
 9:00 am | Craig of the Creek | Bernard of the Creek, Part 1
 9:15 am | Craig of the Creek | Bernard of the Creek, Part 2
 9:29 am | We Bare Bears | Cousin Jon
 9:43 am | We Bare Bears | Lord of the Poppies
 9:57 am | Lamput | Signs
10:00 am | Scooby-Doo! and Guess Who? | The Crown Jewel of Boxing!
10:30 am | Scooby-Doo! and Guess Who? | The Internet on Haunted House Hill!
11:00 am | Amazing World of Gumball | The Founder
11:15 am | Amazing World of Gumball | The Schooling
11:30 am | Amazing World of Gumball | The Intelligence
11:45 am | Amazing World of Gumball | The Potion
12:00 pm | Apple & Onion | Lowlifes
12:15 pm | Apple & Onion | Falafel's Car Keys
12:30 pm | Clarence | Straight Illin
12:45 pm | Clarence | Hurricane Dilliss
 1:00 pm | Clarence | Detention
 1:15 pm | Clarence | Lil Buddy
 1:30 pm | Craig of the Creek | Left in the Dark
 1:45 pm | Craig of the Creek | The Brood
 2:00 pm | Teen Titans Go! | Porch Pirates
 2:15 pm | Teen Titans Go! | A Sticky Situation
 2:30 pm | Teen Titans Go! | We'll Be Right Back
 2:45 pm | Teen Titans Go! | Pool Season
 3:00 pm | Amazing World of Gumball | The Transformation
 3:15 pm | Amazing World of Gumball | The Spinoffs
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 3:45 pm | Amazing World of Gumball | The Understanding
 4:00 pm | Amazing World of Gumball | The Ad
 4:15 pm | Amazing World of Gumball | The Stink
 4:30 pm | Regular Show | Blind Trust
 4:45 pm | Regular Show | World's Best Boss