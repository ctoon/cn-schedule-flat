# 2024-02-22
 5:00 pm | Dexter's Laboratory | Decode of Honor/World's Greatest Mom/Ultrajerk 2000
 5:30 pm | Ed, Edd n Eddy | Ed or Tails/Boys Will Be Eds
 6:00 pm | The Grim Adventures of Billy and Mandy | Mandy the Merciless/Creating Chaos/The Really Odd Couple
 6:30 pm | Courage the Cowardly Dog | The Demon in the Mattress/Freaky Fred
 7:00 pm | King of the Hill | Hanky Panky
 7:30 pm | King of the Hill | High Anxiety
 8:00 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 8:30 pm | Bob's Burgers | Mo Mommy Mo Problems
 9:00 pm | Bob's Burgers | Mission Impos-slug-ble
 9:30 pm | American Dad! | The Never-Ending Stories
10:00 pm | American Dad! | Railroaded
10:30 pm | American Dad! | My Purity Ball and Chain
11:00 pm | American Dad! | OreTron Trail
11:30 pm | Rick and Morty | Morty's Mind Blowers
12:00 am | Royal Crackers | Stebe
12:30 am | The Boondocks | ...Or Die Trying
 1:00 am | Mike Tyson Mysteries | Make a Wish and Blow
 1:15 am | Mike Tyson Mysteries | The Bucket List
 1:30 am | American Dad! | Railroaded
 2:00 am | American Dad! | My Purity Ball and Chain
 2:30 am | American Dad! | OreTron Trail
 3:00 am | Rick and Morty | Morty's Mind Blowers
 3:30 am | Royal Crackers | Stebe
 4:00 am | The Boondocks | ...Or Die Trying
 4:30 am | Black Jesus | Jesus Gonna Get His
 5:00 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 5:30 am | Bob's Burgers | Mo Mommy Mo Problems