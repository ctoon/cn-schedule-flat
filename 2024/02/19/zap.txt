# 2024-02-19
 6:00 am | Amazing World of Gumball | The Heist
 6:15 am | Amazing World of Gumball | The Singing
 6:29 am | Amazing World of Gumball | The Best
 6:43 am | Amazing World of Gumball | The Worst
 6:57 am | Lamput | Thief
 7:00 am | The Looney Tunes Show | Mr. Weiner
 7:29 am | Bugs Bunny Builders | Tweety-Go-Round
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Buckle Up
 7:46 am | Bugs Bunny Builders | Smash House
 8:00 am | Cocomelon | This is the Way
 8:14 am | Meet the Batwheels | Follow the Bouncing Bam
 8:29 am | Cocomelon | Boo Boo Song
 8:43 am | Jessica's Big Little World | Bedtime Routine
 8:57 am | Meet the Batwheels | Rockin' Robin
 9:00 am | Craig of the Creek | Craig to the Future
 9:14 am | Craig of the Creek | Craig of the Street
 9:28 am | We Bare Bears | Panda's Date
 9:42 am | We Bare Bears | Everyday Bears
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
10:00 am | Scooby-Doo! and Guess Who? | The Hot Dog Dog!
10:30 am | Scooby-Doo! and Guess Who? | A Moveable Mystery!
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Valiant Quest
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Chicken Of Doom
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Sing-Songy Stag
11:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Tears Of Triumph
12:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
12:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Swoony Swanstress
12:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Feather Fluffer-Upper Part 1; The Prince and The Feather Fluffer-Upper Part 2
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Feast Of The Fopdoodles
 1:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 1:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 1:45 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Wise Wizard Of Wisdom
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 2:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 2:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Playdate Part 1; The Prince and The Playdate Part 2
 3:00 pm | The Princess Bride | 