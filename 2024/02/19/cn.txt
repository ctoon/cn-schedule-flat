# 2024-02-19
 6:00 am | Amazing World of Gumball | The Heist
 6:14 am | Amazing World of Gumball | The Singing
 6:28 am | The Looney Tunes Show | Double Date
 6:57 am | Lamput | Thief
 7:00 am | The Looney Tunes Show | Mr. Weiner
 7:29 am | Bugs Bunny Builders | Tweety-Go-Round
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Buckle Up
 7:46 am | Bugs Bunny Builders | Smash House
 8:00 am | Meet the Batwheels | Follow the Bouncing Bam
 8:14 am | Meet the Batwheels | Rockin' Robin
 8:17 am | Meet the Batwheels | Zoomsday
 8:31 am | We Baby Bears | The Magical Box
 8:45 am | We Baby Bears | Bears and the Beanstalk
 9:00 am | Craig of the Creek | Craig to the Future
 9:14 am | Craig of the Creek | Craig of the Street
 9:28 am | We Bare Bears | Panda's Date
 9:42 am | We Bare Bears | Everyday Bears
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
10:00 am | Scooby-Doo! and Guess Who? | The Hot Dog Dog!
10:30 am | Scooby-Doo! and Guess Who? | A Moveable Mystery!
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
11:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
12:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
12:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Swoony Swanstress
12:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feather Fluffer-Upper (Part 1 & 2)
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feast of the Fopdoodles
 1:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 1:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 1:45 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Wise Wizard of Wisdom
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 2:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 2:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Playdate
 3:00 pm | The Princess Bride | 