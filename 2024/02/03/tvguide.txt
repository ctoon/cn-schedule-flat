# 2024-02-03
 6:00 am | Regular Show | Rigby in the Sky With Burrito
 6:15 am | Regular Show | Journey to the Bottom of the Crash Pit
 6:30 am | Regular Show | Saving Time
 6:45 am | Regular Show | Guitar of Rock
 7:00 am | Amazing World of Gumball | The Heart
 7:15 am | Amazing World of Gumball | The Revolt
 7:30 am | Amazing World Of Gumball | The Decisions
 7:45 am | Amazing World Of Gumball | The BFFs
 8:00 am | Amazing World Of Gumball | The Inquisition
 8:15 am | Amazing World Of Gumball | Darwin's Yearbook - Banana Joe
 8:30 am | Amazing World Of Gumball | Darwin's Yearbook - Clayton
 8:45 am | Amazing World Of Gumball | Darwin's Yearbook - Carrie
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 9:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feast of the Fopdoodles
 9:30 am | Tiny Toons Looniversity | Tooney Ball Lights
10:00 am | Teen Titans Go! | Cartoon Feud
10:15 am | Teen Titans Go! | Curse Of The Booty Scooty
10:30 am | Teen Titans Go! | Collect Them All
10:45 am | Teen Titans Go! | Butt Atoms
11:00 am | Teen Titans Go! | TV Knight 5
11:15 am | Teen Titans Go! | We're Off To Get Awards
11:30 am | Craig of the Creek | The Cursed Word
11:45 am | Craig of the Creek | Craig Of The Campus
12:00 pm | The Looney Tunes Show | Itsy Bitsy Gopher
12:30 pm | The Looney Tunes Show | Rebel Without a Glove
 1:00 pm | Tiny Toons Looniversity | The Show Must Hop On
 1:30 pm | Amazing World Of Gumball | Darwin's Yearbook - Alan
 1:45 pm | Amazing World Of Gumball | Darwin's Yearbook - Sarah
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 2:15 pm | Amazing World of Gumball | The Responsible
 2:30 pm | Amazing World of Gumball | The DVD
 2:45 pm | Amazing World of Gumball | The Third
 3:00 pm | Scooby-Doo! WrestleMania Mystery | 
 4:45 pm | Amazing World of Gumball | The Painting