# 2024-02-03
 5:00 pm | King of the Hill | The Wedding of Bobby Hill
 5:30 pm | King of the Hill | Sleight of Hank
 6:00 pm | King of the Hill | Jon Vitti Presents: Return to La Grunta
 6:30 pm | King of the Hill | Escape From Party Island
 7:00 pm | Bob's Burgers | Adventures in Chinchilla-Sitting
 7:30 pm | Bob's Burgers | The Runway Club
 8:00 pm | Bob's Burgers | The Itty Bitty Ditty Committee
 8:30 pm | Bob's Burgers | Eat, Spray, Linda
 9:00 pm | American Dad! | The Devil Wears a Lapel Pin
 9:30 pm | American Dad! | Stan-Dan Deliver
10:00 pm | American Dad! | Anchorfran
10:30 pm | American Dad! | The Two Hundred
11:00 pm | Rick and Morty | A Rick in King Mortur's Mort
11:30 pm | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
12:00 am | Demon Slayer: Kimetsu no Yaiba | Tonight
12:30 am | Dr. STONE | Beyond the New World
 1:00 am | Lycoris Recoil | More Haste, Less Speed
 1:30 am | One Piece | The Last -- and Bloodiest -- Block! Block D Battle Begins!
 2:00 am | Naruto: Shippuden | The Final Valley
 2:30 am | IGPX | And Then...
 3:00 am | Unicorn: Warriors Eternal | The Awakening, Part 2
 3:30 am | Genndy Tartakovsky's Primal | Slave of the Scorpion
 4:00 am | Futurama | The Series Has Landed
 4:30 am | Futurama | I, Roommate
 5:00 am | King of the Hill | The Wedding of Bobby Hill
 5:30 am | King of the Hill | Sleight of Hank