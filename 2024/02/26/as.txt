# 2024-02-26
 5:00 pm | Dexter's Laboratory | Dee Dee Be Deep/911/Down in the Dumps
 5:30 pm | Ed, Edd n Eddy | Gimme Gimme Never Ed/My Fair Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Who Killed Who?/Tween Wolf
 6:30 pm | Courage the Cowardly Dog | The Duck Brothers/Shirley the Medium
 7:00 pm | King of the Hill | Transnational Amusements Presents: Peggy's Magic Sex Feet
 7:30 pm | King of the Hill | Peggy's Fan Fair
 8:00 pm | Bob's Burgers | The Fresh Princ-ipal
 8:30 pm | Bob's Burgers | Roamin' Bob-iday
 9:00 pm | Bob's Burgers | What About Blob?
 9:30 pm | Bob's Burgers | If You Love It So Much, Why Don't You Marionette?
10:00 pm | Bob's Burgers | Long Time Listener, First Time Bob
10:30 pm | American Dad! | Fleabiscuit
11:00 pm | American Dad! | The Future Is Borax
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Rick and Morty | The Old Man and the Seat
12:30 am | The Boondocks | Tom, Sarah and Usher
 1:00 am | Mike Tyson Mysteries | The Missing Package
 1:15 am | Mike Tyson Mysteries | Pits and Peaks
 1:30 am | American Dad! | Fleabiscuit
 2:00 am | American Dad! | The Future Is Borax
 2:30 am | American Dad! | Fantasy Baseball
 3:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:30 am | Rick and Morty | The Old Man and the Seat
 4:00 am | The Boondocks | Tom, Sarah and Usher
 4:30 am | Black Jesus | False Witness
 5:00 am | Bob's Burgers | The Fresh Princ-ipal
 5:30 am | Bob's Burgers | Roamin' Bob-iday