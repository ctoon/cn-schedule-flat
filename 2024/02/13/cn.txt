# 2024-02-13
 6:00 am | Amazing World of Gumball | The Blame
 6:15 am | Amazing World of Gumball | The Slap
 6:29 am | Amazing World of Gumball | The Detective
 6:43 am | Amazing World of Gumball | The Fury
 6:57 am | Lamput | Flicker
 7:00 am | The Looney Tunes Show | The Shell Game
 7:29 am | Bugs Bunny Builders | Dim Sum
 7:43 am | Lellobee City Farm | L-L-Lellogames
 7:46 am | Bugs Bunny Builders | Crane Game
 8:00 am | CoComelon | Are We There Yet?/ The Tortoise and the Hare/ Humpty Dumpty/ Breakfast Song
 8:15 am | Meet the Batwheels | Bat-Light Blow-Out
 8:29 am | CoComelon | My Name Song/ Yes Yes Vegetables Song/ Wheels on the Bus V2 (Play Version)
 8:43 am | Jessica's Big Little World | Little Twin
 8:57 am | Lellobee City Farm | Yes Yes Vegetables Song
 9:00 am | Craig of the Creek | Fire & Ice
 9:15 am | Craig of the Creek | Chrono Moss
 9:29 am | We Bare Bears | The Gym
 9:43 am | We Bare Bears | Bubble
 9:57 am | Lamput | Gym
10:00 am | Scooby-Doo! and Guess Who? | Fear of the Fire Beast!
10:30 am | Scooby-Doo! and Guess Who? | Too Many Dummies!
11:00 am | Amazing World of Gumball | The Cycle
11:15 am | Amazing World of Gumball | The Stars
11:30 am | Amazing World of Gumball | The Grades
11:45 am | Amazing World of Gumball | The Diet
12:00 pm | Apple & Onion | Drone Shoes
12:15 pm | Apple & Onion | Walking on the Ceiling
12:30 pm | Clarence | Zoo
12:45 pm | Clarence | Rise and Shine
 1:00 pm | Clarence | Man of the House
 1:15 pm | Clarence | Puddle Eyes
 1:30 pm | Craig of the Creek | In Search of Lore
 1:45 pm | Craig of the Creek | Opposite Day
 2:00 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 2:15 pm | Teen Titans Go! | Cy & Beasty
 2:30 pm | Teen Titans Go! | T Is for Titans
 2:45 pm | Teen Titans Go! | Creative Geniuses
 3:00 pm | Amazing World of Gumball | The Stories
 3:15 pm | Amazing World of Gumball | The Compilation
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
 3:45 pm | Amazing World of Gumball | The Disaster
 4:00 pm | Amazing World of Gumball | The Re-Run
 4:15 pm | Amazing World of Gumball | The Guy
 4:30 pm | Regular Show | That's My Television
 4:45 pm | Regular Show | A Bunch of Full Grown Geese