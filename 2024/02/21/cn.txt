# 2024-02-21
 6:00 am | Amazing World of Gumball | The Cage
 6:14 am | Amazing World of Gumball | The Faith
 6:28 am | The Looney Tunes Show | Newspaper Thief
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Thieftress
 7:00 am | The Looney Tunes Show | SuperRabbit
 7:29 am | Bugs Bunny Builders | Buzz In
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Porky Perfect
 7:46 am | Bugs Bunny Builders | Stories
 8:00 am | Meet the Batwheels | Nightbike
 8:14 am | Meet the Batwheels | Bibi Bops
 8:17 am | Meet the Batwheels | Batcomputer for a Day
 8:31 am | We Baby Bears | Modern-ish Stone Age Family
 8:45 am | We Baby Bears | Excalibear
 9:00 am | Craig of the Creek | Craiggy & the Slime Factory
 9:15 am | Craig of the Creek | A Tattle Tale
 9:29 am | We Bare Bears | Sandcastle
 9:43 am | We Bare Bears | Bros in the City
 9:57 am | Lamput | Snow
10:00 am | Scooby-Doo! and Guess Who? | Scooby On Ice!
10:30 am | Scooby-Doo! and Guess Who? | Caveman on the Half Pipe!
11:00 am | Amazing World of Gumball | The Cage
11:15 am | Amazing World of Gumball | The Faith
11:30 am | Amazing World of Gumball | The Candidate
11:45 am | Amazing World of Gumball | The Anybody
12:00 pm | Apple & Onion | For Queen and Country
12:15 pm | Apple & Onion | Petri
12:30 pm | Clarence | Nothing Ventured
12:45 pm | Clarence | Jeff Wins
 1:00 pm | Clarence | Turtle Hats
 1:15 pm | Clarence | Goldfish Follies
 1:30 pm | Craig of the Creek | Craig of the Campus
 1:45 pm | Craig of the Creek | Wheels Collide
 2:00 pm | Teen Titans Go! | Go!
 2:15 pm | Teen Titans Go! | Finding Aquaman
 2:30 pm | Teen Titans Go! | Whodundidit?
 2:45 pm | Teen Titans Go! | Sweet Revenge
 3:00 pm | Amazing World of Gumball | The Neighbor
 3:15 pm | Amazing World of Gumball | The Pact
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Swoony Swanstress
 3:45 pm | Amazing World of Gumball | The Shippening
 4:00 pm | Amazing World of Gumball | The Brain
 4:15 pm | Amazing World of Gumball | The Parents
 4:30 pm | Regular Show | The Last Laserdisc Player
 4:45 pm | Regular Show | Country Club