# 2024-02-25
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 6:30 am | Amazing World of Gumball | The Move
 6:45 am | Amazing World of Gumball | The Law
 7:00 am | Amazing World of Gumball | The Allergy
 7:15 am | Amazing World of Gumball | The Mothers
 7:30 am | Amazing World of Gumball | The Password
 7:45 am | Amazing World of Gumball | The Procrastinators
 8:00 am | Scooby-Doo! and Guess Who? | The Last Inmate!
 8:30 am | Scooby-Doo! and Guess Who? | Lost Soles of Jungle River!
 9:00 am | Scooby-Doo! The Sword and the Scoob | 
10:45 am | New Looney Tunes | St. Bugs and the Dragon/Leaf It Alone
11:00 am | The Looney Tunes Show | Fish and Visitors
11:30 am | The Looney Tunes Show | Monster Talent
12:00 pm | Tom & Jerry Show | Holed Up/One of a Kind
12:30 pm | Tom & Jerry Show | Belly Achin'/Dog Daze
 1:00 pm | We Bare Bears | Nom Nom
 1:15 pm | We Bare Bears | Shush Ninjas
 1:30 pm | We Bare Bears | My Clique
 1:45 pm | We Bare Bears | Charlie
 2:00 pm | Steven Universe | Tiger Millionaire
 2:15 pm | Steven Universe | Steven's Lion
 2:30 pm | Steven Universe | Arcade Mania
 2:45 pm | Steven Universe | Giant Woman
 3:00 pm | Adventure Time | My Two Favorite People/Memories of Boom Boom Mountain
 3:30 pm | Adventure Time | Wizard/Evicted!
 4:00 pm | Regular Show | Prank Callers
 4:15 pm | Regular Show | Don
 4:30 pm | Regular Show | Rigby's Body
 4:45 pm | Regular Show | Mordecai and the Rigbys