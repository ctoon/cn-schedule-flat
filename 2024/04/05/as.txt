# 2024-04-05
 5:00 pm | Cow and Chicken | Cow Fly/Where Am I?/I Stand Corrected
 5:30 pm | Cow and Chicken | Me an' My Dog/Cow's Dream Catcher/Dessert Island
 6:00 pm | Ed, Edd n Eddy | Your Ed Here/The Good Ol' Ed
 6:30 pm | Ed, Edd n Eddy | Thick as an Ed/Sorry, Wrong Ed
 7:00 pm | King of the Hill | Dale To the Chief
 7:30 pm | King of the Hill | The Petriot Act
 8:00 pm | Bob's Burgers | The Runway Club
 8:30 pm | Bob's Burgers | The Itty Bitty Ditty Committee
 9:00 pm | Bob's Burgers | Eat, Spray, Linda
 9:30 pm | American Dad! | Bully for Steve
10:00 pm | American Dad! | An Incident at Owl Creek
10:30 pm | American Dad! | Great Space Roaster
11:00 pm | American Dad! | 100 A.D.
11:30 pm | Rick and Morty | Rickmancing the Stone
12:00 am | Metalocalypse | Metalocalypse: Army of the Doomstar
 2:00 am | Rick and Morty | Rickmancing the Stone
 2:30 am | Rick and Morty | Mort Dinner Rick Andre
 3:00 am | American Dad! | Bully for Steve
 3:30 am | American Dad! | An Incident at Owl Creek
 4:00 am | Futurama | Amazon Women in the Mood
 4:30 am | Futurama | Bendless Love
 5:00 am | King of the Hill | Dale To the Chief
 5:30 am | King of the Hill | The Petriot Act