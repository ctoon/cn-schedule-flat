# 2024-04-18
 6:00 am | Regular Show | Fool Me Twice
 6:12 am | Regular Show | Limousine Lunchtime
 6:28 am | The Looney Tunes Show | Gossamer Is Awesomer
 6:56 am | The Looney Tunes Show | Here Comes the Pig
 7:24 am | Bugs Bunny Builders | Dim Sum
 7:36 am | Bugs Bunny Builders | Crane Game
 7:52 am | Meet the Batwheels | Nightbike
 8:04 am | Meet the Batwheels | Tough Buff Blues
 8:16 am | Meet the Batwheels | Wheel Side Story
 8:32 am | Amazing World of Gumball | The Voice
 8:44 am | Amazing World of Gumball | The Promise
 9:00 am | Amazing World of Gumball | The Castle
 9:12 am | Amazing World of Gumball | The Boombox
 9:28 am | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital Of Dr. Phineas Phrag!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Pretty Poodle
10:00 am | Scooby-Doo! and Guess Who? | The Phantom, The Talking Dog And The Hot Hot Hot Sauce!
10:30 am | Tom & Jerry Show | Splinter of Discontent
10:40 am | Tom & Jerry Show | Forget Me Not
10:50 am | Tom & Jerry Show | In the Beginning
11:00 am | Tom & Jerry Show | Uncle Pecos Rides Again
11:10 am | Tom & Jerry Show | Out With the Old
11:20 am | Tom & Jerry Show | Tic-Tyke-D'oh
11:30 am | Tiny Toons Looniversity | Whatever Happened to Babsy Bunny?
12:00 pm | Craig of the Creek | Capture the Flag Part 3:The Legend
12:15 pm | Craig of the Creek | Capture the Flag - Part 4: The Plan
12:30 pm | We Bare Bears | More Everyone's Tube
12:45 pm | We Bare Bears | Money Man
 1:00 pm | Teen Titans Go! | Slumber Party
 1:15 pm | Teen Titans Go! | Love Monsters
 1:30 pm | Teen Titans Go! | Baby Hands
 1:45 pm | Teen Titans Go! | Caramel Apples
 2:00 pm | Teen Titans Go! | Bottle Episode
 2:15 pm | Teen Titans Go! | Finally a Lesson
 2:30 pm | Amazing World of Gumball | The Tape
 2:45 pm | Amazing World of Gumball | The Sweaters
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 3:15 pm | Amazing World of Gumball | The Deal
 3:30 pm | Amazing World of Gumball | The Worst
 3:45 pm | Amazing World of Gumball | The Internet
 4:00 pm | Regular Show | Over the Top
 4:15 pm | Regular Show | The Night Owl
 4:30 pm | Regular Show | A Bunch of Baby Ducks
 4:45 pm | Regular Show | More Smarter