# 2024-04-14
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Yucky Duckling
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
 6:30 am | Amazing World Of Gumball | The BFFs
 6:45 am | Amazing World Of Gumball | The Inquisition
 7:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny?
 7:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Leslie?
 7:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert?
 7:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Anyone?
 8:00 am | Scooby-Doo! and Guess Who? | The New York Underground!
 8:30 am | Scooby-Doo! and Guess Who? | Fear Of The Fire Beast!
 9:00 am | Big Top Scooby-Doo! | 
10:45 am | New Looney Tunes | Big Troubles/Manner Maid
11:00 am | The Looney Tunes Show | Spread Those Wings and Fly
11:30 am | The Looney Tunes Show | The Black Widow
12:00 pm | The Looney Tunes Show | It's a Handbag
12:30 pm | Tiny Toons Looniversity | Whatever Happened to Babsy Bunny?
 1:00 pm | Tiny Toons Looniversity | Tears of a Clone
 1:30 pm | Tom & Jerry Show | Cruisin' for a Bruisin'; Road Trippin'
 2:00 pm | Tom & Jerry Show | Magic Mirror; Bone Dry
 2:30 pm | Steven Universe | Lion 3: Straight to Video
 2:45 pm | Steven Universe | Warp Tour
 3:00 pm | Adventure Time | The Chamber of Frozen Blades; The Other Tarts
 3:30 pm | Adventure Time | The Silent King; The Pods
 4:00 pm | Regular Show | Video Game Wizard
 4:15 pm | Regular Show | Big Winner
 4:30 pm | Regular Show | The Best Burger in the World
 4:45 pm | Regular Show | Replaced