# 2024-04-15
 6:00 am | Regular Show | Ace Balthazar Lives
 6:12 am | Regular Show | Do or Diaper
 6:28 am | The Looney Tunes Show | Mrs. Porkbunny's
 6:56 am | The Looney Tunes Show | Gribbler's Quest
 7:24 am | Bugs Bunny Builders | Underwater Star
 7:36 am | Bugs Bunny Builders | Batty Kathy
 7:52 am | Meet the Batwheels | Keep Calm and Roll On
 8:04 am | Meet the Batwheels | Mission: Stuffed Animal
 8:16 am | Meet the Batwheels | When You're a Jet
 8:32 am | Amazing World of Gumball | The Flower
 8:44 am | Amazing World of Gumball | The Banana
 9:00 am | Amazing World of Gumball | The Phone
 9:12 am | Amazing World of Gumball | The Job
 9:28 am | Scooby-Doo! and Guess Who? | Too Many Dummies!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Talking Tree
10:00 am | Scooby-Doo! and Guess Who? | Dance Matron of Mayhem!
10:30 am | Tom & Jerry Show | I Quit
10:40 am | Tom & Jerry Show | Art of the Deal
10:50 am | Tom & Jerry Show | Hiccup and Away
11:00 am | Tom & Jerry Show | Tom-Fu
11:10 am | Tom & Jerry Show | You Can't Handle The Tooth
11:20 am | Tom & Jerry Show | Pain For Sale
11:30 am | Tiny Toons Looniversity | Whatever Happened to Babsy Bunny?
12:00 pm | Craig of the Creek | Beyond the Overpass
12:15 pm | Craig of the Creek | Sink or Swim Team
12:30 pm | We Bare Bears | Family Troubles
12:45 pm | We Bare Bears | Best Bears
 1:00 pm | Teen Titans Go! | Grandma Voice
 1:15 pm | Teen Titans Go! | Real Magic
 1:30 pm | Teen Titans Go! | Puppets, Whaaaaat?
 1:45 pm | Teen Titans Go! | Mr. Butt
 2:00 pm | Teen Titans Go! | Squash & Stretch
 2:15 pm | Teen Titans Go! | Garage Sale
 2:30 pm | Amazing World of Gumball | The Remote
 2:45 pm | Amazing World of Gumball | The List
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Yucky Duckling
 3:15 pm | Amazing World of Gumball | The Uncle
 3:30 pm | Amazing World of Gumball | The Menu
 3:45 pm | Amazing World of Gumball | The Treasure
 4:00 pm | Regular Show | Party Pete
 4:15 pm | Regular Show | Brain Eraser
 4:30 pm | Regular Show | Benson Be Gone
 4:45 pm | Regular Show | But I Have a Receipt