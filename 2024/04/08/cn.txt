# 2024-04-08
 6:00 am | Regular Show | Starter Pack
 6:14 am | Regular Show | Pie Contest
 6:28 am | The Looney Tunes Show | Itsy Bitsy Gopher
 6:56 am | Tom & Jerry | What's That Smell
 7:00 am | The Looney Tunes Show | Rebel Without a Glove
 7:30 am | Bugs Bunny Builders | Looney Moon
 7:45 am | Bugs Bunny Builders | Cousin Billy
 8:00 am | Jessica's Big Little World | Drawing Time
 8:15 am | Jessica's Big Little World | Little Twin
 8:29 am | Meet the Batwheels | Bam's Bubble Trouble
 8:32 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Penny?
 8:46 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Leslie?
 9:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Bobert?
 9:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Anyone?
 9:29 am | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
 9:57 am | Lamput | Orange Street
10:00 am | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!
10:30 am | Tom & Jerry Show | Smitten with the Kitten
10:40 am | Tom & Jerry Show | Squeaky Clean
10:50 am | Tom & Jerry Show | Slinging in the Rain
11:00 am | Tom & Jerry Show | Picture Imperfect
11:10 am | Tom & Jerry Show | One-Way Cricket
11:20 am | Tom & Jerry Show | The Paper Airplane Chase
11:30 am | Tiny Toons Looniversity | Spring Beak
12:30 pm | We Bare Bears | The Park
12:45 pm | We Bare Bears | I Am Ice Bear
 1:00 pm | Teen Titans Go! | Wild Card
 1:15 pm | Teen Titans Go! | Staring at the Future
 1:30 pm | Teen Titans Go! | No Power
 1:45 pm | Teen Titans Go! | Sidekick
 2:00 pm | Teen Titans Go! | The Fourth Wall
 2:15 pm | Teen Titans Go! | 40%, 40%, 20%
 2:30 pm | Amazing World of Gumball | Darwin's Yearbook -- Banana Joe
 2:45 pm | Amazing World of Gumball | Darwin's Yearbook -- Clayton
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Legendary Rabbit Hood
 3:15 pm | Amazing World of Gumball | The Console
 3:30 pm | Amazing World of Gumball | The Box
 3:45 pm | Amazing World of Gumball | Darwin's Yearbook -- Carrie
 4:00 pm | Regular Show | The Power
 4:15 pm | Regular Show | Just Set up the Chairs
 4:30 pm | Regular Show | Caffeinated Concert Tickets
 4:45 pm | Regular Show | Death Punchies