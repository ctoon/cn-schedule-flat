# 2024-04-20
 5:00 pm | King of the Hill | Glen Peggy Glen Ross
 5:30 pm | King of the Hill | Grand Theft Arlen
 6:00 pm | King of the Hill | Peggy's Gone to Pots
 6:30 pm | King of the Hill | Hair Today, Gone Today
 7:00 pm | Bob's Burgers | Mission Impos-slug-ble
 7:30 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 8:00 pm | Bob's Burgers | Just One of the Boyz 4 Now for Now
 8:30 pm | Bob's Burgers | The Taking of Funtime One Two Three
 9:00 pm | American Dad! | Finger Lenting Good
 9:30 pm | American Dad! | The Adventures of Twill Ongenbone and His Boy Jabari
10:00 pm | American Dad! | Blood Crieth Unto Heaven
10:30 pm | American Dad! | Max Jets
11:00 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
11:30 pm | Rick and Morty | The Old Man and the Seat
12:00 am | Ninja Kamui | (Dub) Episode 11
12:30 am | Zom 100: Bucket List of the Dead | Flight Attendant of the Dead
 1:00 am | One Piece | Two Great Rivals Meet Each Other! Straw Hat and Heavenly Demon!
 1:30 am | One Piece | Luffy Astonished! The Man Who Inherits Ace's Will!
 2:00 am | Naruto: Shippuden | Naruto Shippuden, Sasuke's Story: Sunrise, Part 2: Coliseum
 2:30 am | Dragon Ball Z Kai | Shenron Appears! The Saiyans Arrive Sooner Than Expected!
 3:00 am | Ninja Kamui | (Sub) Episode 11
 3:30 am | Genndy Tartakovsky's Primal | The Colossaeus, Part II
 4:00 am | Futurama | I Dated a Robot
 4:30 am | Futurama | Roswell That Ends Well
 5:00 am | King of the Hill | Peggy's Gone to Pots
 5:30 am | King of the Hill | Hair Today, Gone Today