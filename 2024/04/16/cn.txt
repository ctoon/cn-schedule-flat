# 2024-04-16
 6:00 am | Regular Show | Quips
 6:12 am | Regular Show | Caveman
 6:28 am | The Looney Tunes Show | The Grand Old Duck of York
 6:56 am | The Looney Tunes Show | Ridiculous Journey
 7:24 am | Bugs Bunny Builders | Mail Whale
 7:36 am | Bugs Bunny Builders | Honey Bunny
 7:52 am | Meet the Batwheels | Bam's Upgrade
 8:04 am | Meet the Batwheels | Bat-Light Blow-Out
 8:16 am | Meet the Batwheels | Ride Along
 8:32 am | Amazing World of Gumball | The Skull
 8:44 am | Amazing World of Gumball | The Bet
 9:00 am | Amazing World of Gumball | Christmas
 9:12 am | Amazing World of Gumball | The Watch
 9:28 am | Scooby-Doo! and Guess Who? | The Wedding Witch of Wainsly Hall!
 9:56 am | Tom & Jerry | What Goes Around Comes Around
10:00 am | Scooby-Doo! and Guess Who? | A Run Cycle Through Time!
10:30 am | Tom & Jerry Show | Downton Tabby
10:40 am | Tom & Jerry Show | Growing Pains
10:50 am | Tom & Jerry Show | Toodle Boom!
11:00 am | Tom & Jerry Show | Bringing Down the House
11:10 am | Tom & Jerry Show | Return to Sender
11:20 am | Tom & Jerry Show | Jerry Rigged
11:30 am | Tiny Toons Looniversity | Spring Beak
12:30 pm | We Bare Bears | Crowbar Jones: Origins
12:45 pm | We Bare Bears | Hot Sauce
 1:00 pm | Teen Titans Go! | Man Person
 1:15 pm | Teen Titans Go! | Pirates
 1:30 pm | Teen Titans Go! | Money Grandma
 1:45 pm | Teen Titans Go! | I See You
 2:00 pm | Teen Titans Go! | Secret Garden
 2:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 2:30 pm | Amazing World of Gumball | The Bumpkin
 2:45 pm | Amazing World of Gumball | The Flakers
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Wise Wizard of Wisdom
 3:15 pm | Amazing World of Gumball | The Heist
 3:30 pm | Amazing World of Gumball | The Weirdo
 3:45 pm | Amazing World of Gumball | The Authority
 4:00 pm | Regular Show | This is My Jam
 4:15 pm | Regular Show | Muscle Woman
 4:30 pm | Regular Show | Temp Check
 4:45 pm | Regular Show | Jinx