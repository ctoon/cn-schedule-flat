# 2024-04-09
 6:00 am | Regular Show | 150-Piece Kit
 6:15 am | Regular Show | Bald Spot
 6:29 am | The Looney Tunes Show | Semper Lie
 6:57 am | Lamput | Fracture
 7:00 am | The Looney Tunes Show | Father Figures
 7:29 am | Bugs Bunny Builders | Cheddar Days
 7:43 am | Lellobee City Farm | The Team Work song
 7:46 am | Bugs Bunny Builders | Bright Light
 8:00 am | Jessica's Big Little World | Jessica's Picnic
 8:15 am | Jessica's Big Little World | Spike the Hedgehog
 8:29 am | Lellobee City Farm | Bee a Bee dance
 8:32 am | Amazing World of Gumball | Darwin's Yearbook -- Teachers
 8:46 am | Amazing World of Gumball | The End
 9:00 am | Amazing World of Gumball | The Dress
 9:14 am | Amazing World of Gumball | The Quest
 9:28 am | Scooby-Doo! and Guess Who? | Attack of the Weird Al-Osaurus!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Raspberry Fairy
10:00 am | Scooby-Doo! and Guess Who? | When Urkel-Bots Go Bad!
10:30 am | Tom & Jerry Show | Round Tripped
10:40 am | Tom & Jerry Show | Tough Luck Duck
10:50 am | Tom & Jerry Show | Cheesy Ball Run
11:00 am | Tom & Jerry Show | Say Uncle
11:10 am | Tom & Jerry Show | Here Comes the Bribe
11:20 am | Tom & Jerry Show | Dental Case
11:30 am | Tiny Toons Looniversity | Freshman Orientoontion
12:00 pm | Craig of the Creek | Capture the Flag Part I: The Candy
12:15 pm | Craig of the Creek | Capture the Flag Part II: The King
12:30 pm | We Bare Bears | Baby Bears Can't Jump
12:45 pm | We Bare Bears | Go Fish
 1:00 pm | Teen Titans Go! | Caged Tiger
 1:15 pm | Teen Titans Go! | Second Christmas
 1:30 pm | Teen Titans Go! | Nose Mouth
 1:45 pm | Teen Titans Go! | Legs
 2:00 pm | Teen Titans Go! | Grube's Fairytales
 2:15 pm | Teen Titans Go! | A Farce
 2:30 pm | Amazing World of Gumball | The Spoon
 2:45 pm | Amazing World of Gumball | The Pressure
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feast of the Fopdoodles
 3:15 pm | Amazing World of Gumball | The Catfish
 3:30 pm | Amazing World of Gumball | The Ollie
 3:45 pm | Amazing World of Gumball | The Painting
 4:00 pm | Regular Show | Free Cake
 4:15 pm | Regular Show | Meat Your Maker
 4:30 pm | Regular Show | Grilled Cheese Deluxe
 4:45 pm | Regular Show | The Unicorns Have Got to Go