# 2024-04-17
 6:00 am | Regular Show | That's My Television
 6:12 am | Regular Show | A Bunch of Full Grown Geese
 6:28 am | The Looney Tunes Show | The Shell Game
 6:56 am | The Looney Tunes Show | Year of the Duck
 7:24 am | Bugs Bunny Builders | Castle Hassle
 7:36 am | Bugs Bunny Builders | Catwalk
 7:52 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:04 am | Meet the Batwheels | Follow the Bouncing Bam
 8:16 am | Meet the Batwheels | Wheels Just Want to Have Fun
 8:32 am | Amazing World of Gumball | The Hero
 8:44 am | Amazing World of Gumball | The Dream
 9:00 am | Amazing World of Gumball | The Sidekick
 9:12 am | Amazing World of Gumball | The Photo
 9:28 am | Scooby-Doo! and Guess Who? | I Put a Hex on You!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Thieftress
10:00 am | Scooby-Doo! and Guess Who? | The High School Wolfman's Musical Lament!
10:30 am | Tom & Jerry Show | The Art of War
10:40 am | Tom & Jerry Show | Pillow Case
10:50 am | Tom & Jerry Show | Home Insecurity
11:00 am | Tom & Jerry Show | Tail of Two Kitties
11:10 am | Tom & Jerry Show | Vanishing Creamed
11:20 am | Tom & Jerry Show | Unhappily Harried After
11:30 am | Tiny Toons Looniversity | Soufflé, Girl Hey
12:00 pm | Craig of the Creek | The Sparkle Solution
12:15 pm | Craig of the Creek | Better Than You
12:30 pm | We Bare Bears | Mom App
12:45 pm | We Bare Bears | The Limo
 1:00 pm | Teen Titans Go! | Brian
 1:15 pm | Teen Titans Go! | Nature
 1:30 pm | Teen Titans Go! | Salty Codgers
 1:45 pm | Teen Titans Go! | Knowledge
 2:00 pm | Teen Titans Go! | Pyramid Scheme
 2:15 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 2:30 pm | Amazing World of Gumball | The Tag
 2:45 pm | Amazing World of Gumball | The Storm
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Squeak of Obedience
 3:15 pm | Amazing World of Gumball | The Best
 3:30 pm | Amazing World of Gumball | The Singing
 3:45 pm | Amazing World of Gumball | The Lesson
 4:00 pm | Regular Show | See You There
 4:15 pm | Regular Show | Do Me a Solid
 4:30 pm | Regular Show | Grave Sights
 4:45 pm | Regular Show | Really Real Wrestling