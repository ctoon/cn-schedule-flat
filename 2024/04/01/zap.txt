# 2024-04-01
 6:00 am | Looney Tunes | What's Opera, Doc?
 6:10 am | Looney Tunes | Tweetie Pie
 6:20 am | Looney Tunes | Knighty Knight Bugs
 6:30 am | Looney Tunes | Birds Anonymous
 6:40 am | Looney Tunes | One Froggy Evening
 6:50 am | Looney Tunes | Rabbit Seasoning
 7:00 am | Looney Tunes | Duck Amuck
 7:10 am | Looney Tunes | Stupor Duck
 7:20 am | Looney Tunes | Zipping Along
 7:30 am | Bugs Bunny Builders | The Easter Bunnies
 7:45 am | Bugs Bunny Builders | Junior
 8:00 am | Bugs Bunny Builders | Looney Moon
 8:15 am | Bugs Bunny Builders | Glamp Out
 8:30 am | The Looney Tunes Show | Bugs & Daffy Get a Job
 9:00 am | The Looney Tunes Show | Here Comes the Pig
 9:28 am | The Looney Tunes Show | That's My Baby
 9:57 am | Lamput | Signs
10:00 am | The Looney Tunes Show | Gossamer Is Awesomer
10:30 am | New Looney Tunes | Computer Bugs; Oils Well That Ends Well
10:45 am | New Looney Tunes | Your Bunny or Your Life; Misjudgement Day
11:00 am | New Looney Tunes | Splashwater Bugs; Fwee Wange Wabbit
11:15 am | New Looney Tunes | Bugs in the Garden; Scarecrow
11:30 am | New Looney Tunes | Painter Paint Hare; The Spy Who Bugged Me
11:45 am | New Looney Tunes | Bugs vs. Snail; To Catch a Fairy
12:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
12:30 pm | Tiny Toons Looniversity | Give Pizza a Chance
 1:00 pm | Tiny Toons Looniversity | Extra, So Extra
 1:30 pm | Tiny Toons Looniversity | Tooney Ball Lights
 2:00 pm | Tiny Toons Looniversity | Save The Loo Bru
 2:30 pm | Tiny Toons Looniversity | Prank You Very Much
 3:00 pm | Tiny Toons Looniversity | General HOGspital
 3:30 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 4:00 pm | Looney Tunes | Wild Hare
 4:10 pm | Looney Tunes | Duck Dodgers in the 24 1/2th Century
 4:20 pm | Looney Tunes | Fast and Furry-ous
 4:30 pm | Looney Tunes | Porky's Duck Hunt
 4:40 pm | Looney Tunes | Devil May Hare
 4:50 pm | Looney Tunes | Beep Prepared