# 2024-04-28
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Show-Off Stallion
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
 6:30 am | Amazing World of Gumball | The Date
 6:45 am | Amazing World of Gumball | The Club
 7:00 am | Amazing World of Gumball | The Wand
 7:15 am | Amazing World of Gumball | The Ape
 7:30 am | Amazing World of Gumball | The Car
 7:45 am | Amazing World of Gumball | The Curse
 8:00 am | Scooby-Doo! and Guess Who? | Scooby-Doo and the Sky Town Cool School!
 8:30 am | Scooby-Doo! and Guess Who? | Falling Star Man!
 9:00 am | Scooby-Doo! The Sword and the Scoob | 
10:45 am | New Looney Tunes | Carrot Before the Horse/Trunk with Power
11:00 am | The Looney Tunes Show | Newspaper Thief
11:30 am | The Looney Tunes Show | Bugs & Daffy Get a Job
12:00 pm | The Looney Tunes Show | Dear John
12:30 pm | Tiny Toons Looniversity | Tooned In Space
 1:00 pm | Tiny Toons Looniversity | Extra, So Extra
 1:30 pm | Tom & Jerry Show | Cat Napped/Black Cat
 2:00 pm | Tom & Jerry Show | Hunger Strikes/Gravi-Tom
 2:30 pm | Steven Universe | Future Vision
 2:45 pm | Steven Universe | On the Run
 3:00 pm | Adventure Time | Mystery Train/Go With Me
 3:30 pm | Adventure Time | Belly of the Beast/The Limit
 4:00 pm | Regular Show | Prankless
 4:15 pm | Regular Show | Death Bear
 4:30 pm | Regular Show | Fuzzy Dice
 4:45 pm | Regular Show | Sugar Rush