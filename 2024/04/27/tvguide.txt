# 2024-04-27
 6:00 am | Regular Show | Out of Commission
 6:15 am | Regular Show | Fancy Restaurant
 6:30 am | Regular Show | Diary
 6:45 am | Regular Show | The Best VHS in the World
 7:00 am | Regular Show | Prankless
 7:15 am | Regular Show | Death Bear
 7:30 am | Scooby-Doo! Music of the Vampire | 
 9:00 am | Tiny Toons Looniversity | Tooned In Space
 9:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Show-Off Stallion
 9:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Unruly Royal
10:00 am | Teen Titans Go! | 50% Crew
10:15 am | Teen Titans Go! | T Is for Titans
10:30 am | Teen Titans Go! | Creative Geniuses
10:45 am | Teen Titans Go! | Manor and Mannerisms
11:00 am | Teen Titans Go! | Trans Oceanic Magical Cruise
11:15 am | Teen Titans Go! | Ship
11:30 am | Teen Titans Go! | Glunkakakakah
11:45 am | Teen Titans Go! | Control Freak
12:00 pm | The Looney Tunes Show | Double Date
12:30 pm | The Looney Tunes Show | To Bowl or Not to Bowl
 1:00 pm | Tiny Toons Looniversity | Tooned In Space
 1:30 pm | Amazing World of Gumball | The Secret
 1:45 pm | Amazing World of Gumball | The Sock
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Show-Off Stallion
 2:15 pm | Amazing World of Gumball | The Genius
 2:30 pm | Amazing World of Gumball | The Poltergeist
 2:45 pm | Amazing World of Gumball | The Mustache
 3:00 pm | Amazing World of Gumball | The Dream
 3:15 pm | Amazing World of Gumball | The Night
 3:30 pm | Amazing World of Gumball | The Prank
 3:45 pm | Amazing World of Gumball | The GI
 4:00 pm | Scooby-Doo! and Guess Who? | The Legend Of The Gold Microphone!
 4:30 pm | Scooby-Doo! and Guess Who? | Total Jeopardy!