# 2024-04-29
 6:00 am | Regular Show | Last Meal
 6:12 am | Regular Show | Sleep Fighter
 6:28 am | The Looney Tunes Show | That's My Baby
 6:56 am | The Looney Tunes Show | Sunday Night Slice
 7:24 am | Bugs Bunny Builders | Smash House
 7:36 am | Bugs Bunny Builders | Play Day
 7:52 am | Meet the Batwheels | Scaredy-Bat
 8:04 am | Meet the Batwheels | Improvise My Ride
 8:16 am | Meet the Batwheels | Dynamic Du-Oh-No
 8:32 am | Amazing World of Gumball | The Romantic
 8:44 am | Amazing World of Gumball | The Uploads
 9:00 am | Amazing World of Gumball | The Apprentice
 9:12 am | Amazing World of Gumball | The Hug
 9:28 am | Scooby-Doo! and Guess Who? | Dark Diner of Route 66!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Hissy Swan
10:00 am | Scooby-Doo! and Guess Who? | A Haunt of a Thousand Voices!
10:30 am | Tom & Jerry Show | Frown and Country
10:40 am | Tom & Jerry Show | It's All Relative
10:50 am | Tom & Jerry Show | Vegged Out
11:00 am | Tom & Jerry Show | Lost Marbles
11:10 am | Tom & Jerry Show | Faux Hunt
11:20 am | Tom & Jerry Show | Lame Duck
11:30 am | Tiny Toons Looniversity | Tooned In Space
12:00 pm | Craig of the Creek | Scoutguest
12:15 pm | Craig of the Creek | My Stare Lady
12:30 pm | We Bare Bears | The Gym
12:45 pm | We Bare Bears | Bubble
 1:00 pm | Teen Titans Go! | Cool School
 1:15 pm | Teen Titans Go! | Kicking a Ball and Pretending to be Hurt
 1:30 pm | Teen Titans Go! | Head Fruit
 1:45 pm | Teen Titans Go! | Yearbook Madness
 2:00 pm | Teen Titans Go! | The Overbite
 2:15 pm | Teen Titans Go! | The Cape
 2:30 pm | Amazing World of Gumball | The Wicked
 2:45 pm | Amazing World of Gumball | The Traitor
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Show-Off Stallion
 3:15 pm | Amazing World of Gumball | The Cage
 3:30 pm | Amazing World of Gumball | The Cringe
 3:45 pm | Amazing World of Gumball | The Origins
 4:00 pm | Regular Show | Replaced
 4:15 pm | Regular Show | Trash Boat
 4:30 pm | Regular Show | Fists of Justice
 4:45 pm | Regular Show | Yes Dude Yes