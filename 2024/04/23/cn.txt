# 2024-04-23
 6:00 am | Regular Show | Cool Cubed
 6:12 am | Regular Show | Trailer Trashed
 6:28 am | The Looney Tunes Show | Monster Talent
 6:56 am | The Looney Tunes Show | Reunion
 7:24 am | Bugs Bunny Builders | Outer Space
 7:36 am | Bugs Bunny Builders | Ice Creamed
 7:52 am | Meet the Batwheels | Mission: Stuffed Animal
 8:04 am | Meet the Batwheels | License to Joke
 8:16 am | Meet the Batwheels | Bibi's Bad Day
 8:32 am | Amazing World of Gumball | The Password
 8:44 am | Amazing World of Gumball | The Procrastinators
 9:00 am | Amazing World of Gumball | The Shell
 9:12 am | Amazing World of Gumball | The Burden
 9:28 am | Scooby-Doo! and Guess Who? | The 7th Inning Scare!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Raspberry Fairy
10:00 am | Scooby-Doo! and Guess Who? | The Dreaded Remake of Jekyll & Hyde!
10:30 am | Tom & Jerry Show | Fight in the Museum
10:40 am | Tom & Jerry Show | Kitten Grifters
10:50 am | Tom & Jerry Show | School of Hard Knocks
11:00 am | Tom & Jerry Show | Cat-A-Tonic Mouse
11:10 am | Tom & Jerry Show | Brain Food
11:20 am | Tom & Jerry Show | Wish Bone
11:30 am | Tiny Toons Looniversity | General HOGspital
12:00 pm | Craig of the Creek | Creek Talent Extravaganza
12:15 pm | Craig of the Creek | Dodgy Decisions
12:30 pm | We Bare Bears | Adopted
12:45 pm | We Bare Bears | Wingmen
 1:00 pm | Teen Titans Go! | The Best Robin
 1:15 pm | Teen Titans Go! | Mouth Hole
 1:30 pm | Teen Titans Go! | Hot Garbage
 1:45 pm | Teen Titans Go! | Robin Backwards
 2:00 pm | Teen Titans Go! | Operation Dude Rescue Part 1
 2:15 pm | Teen Titans Go! | Operation Dude Rescue Part 2
 2:30 pm | Amazing World of Gumball | The Bros
 2:45 pm | Amazing World of Gumball | The Mirror
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Playdate
 3:30 pm | Amazing World of Gumball | The List
 3:45 pm | Amazing World of Gumball | The News
 4:00 pm | Regular Show | Slam Dunk
 4:15 pm | Regular Show | Cool Bikes
 4:30 pm | Regular Show | House Rules
 4:45 pm | Regular Show | Rap It Up