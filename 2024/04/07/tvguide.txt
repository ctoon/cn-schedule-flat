# 2024-04-07
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Legendary Rabbit Hood
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
 6:30 am | Amazing World of Gumball | The Ad
 6:45 am | Amazing World of Gumball | The Stink
 7:00 am | Amazing World of Gumball | The Awareness
 7:15 am | Amazing World of Gumball | The Slip
 7:30 am | Amazing World of Gumball | The Drama
 7:45 am | Amazing World of Gumball | The Ghouls
 8:00 am | Scooby-Doo! and Guess Who? | Peebles' Pet Shop of Terrible Terrors!
 8:30 am | Scooby-Doo! and Guess Who? | The Scooby of a Thousand Faces!
 9:00 am | Scooby-Doo! Legend of the Phantosaur | 
10:45 am | New Looney Tunes | Hareplane Mode/Bugs of Steel
11:00 am | The Looney Tunes Show | Bobcats on Three!
11:30 am | The Looney Tunes Show | You've Got Hate Mail
12:00 pm | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
12:30 pm | Tiny Toons Looniversity | Spring Break
 1:30 pm | Tom & Jerry Show | Domestic Kingdom/Molecular Breakup
 2:00 pm | Tom & Jerry Show | Just Plane Nuts; Pets Not Welcome
 2:30 pm | Steven Universe | Garnet's Universe
 2:45 pm | Steven Universe | Watermelon Steven
 3:00 pm | Adventure Time | Power Animal; Crystals Have Power
 3:30 pm | Adventure Time | Her Parents; To Cut a Woman's Hair
 4:00 pm | Regular Show | Under the Hood
 4:15 pm | Regular Show | Weekend at Benson's
 4:30 pm | Regular Show | Fortune Cookie
 4:45 pm | Regular Show | Think Positive