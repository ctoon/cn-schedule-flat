# 2024-04-10
 6:00 am | Regular Show | Guy's Night
 6:15 am | Regular Show | One Pull Up
 6:29 am | The Looney Tunes Show | Customer Service
 6:57 am | Lamput | Art Gallery
 7:00 am | The Looney Tunes Show | It's a Handbag
 7:29 am | Bugs Bunny Builders | Mini Golf
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Daffy's Spa
 7:46 am | Bugs Bunny Builders | Goofballs
 8:00 am | Jessica's Big Little World | The Secret Invention
 8:15 am | Jessica's Big Little World | Jessica's Restaurant
 8:29 am | Meet the Batwheels | Wheelin' and Dealin'
 8:32 am | Amazing World of Gumball | The Mystery
 8:46 am | Amazing World of Gumball | The Prank
 9:00 am | Amazing World of Gumball | The GI
 9:14 am | Amazing World of Gumball | The Kiss
 9:28 am | Scooby-Doo! and Guess Who? | The Fastest Fast Food Fiend!
 9:56 am | Tom & Jerry | Sky's The Limit
10:00 am | Scooby-Doo! and Guess Who? | Space Station Scooby!
10:30 am | Tom & Jerry Show | Big Top Tom
10:40 am | Tom & Jerry Show | Reward if Lost
10:50 am | Tom & Jerry Show | Build a Beast
11:00 am | Tom & Jerry Show | Tuffy's Big Adventure
11:10 am | Tom & Jerry Show | Dragon Down the Holidays
11:20 am | Tom & Jerry Show | Slaphappy Birthday
11:30 am | Tiny Toons Looniversity | Extra, So Extra
12:00 pm | Craig of the Creek | Fan or Foe
12:15 pm | Craig of the Creek | The New Jersey
12:30 pm | We Bare Bears | Teacher's Pet
12:45 pm | We Bare Bears | Googs
 1:00 pm | Teen Titans Go! | Breakfast Cheese
 1:15 pm | Teen Titans Go! | Waffles
 1:30 pm | Teen Titans Go! | Be Mine
 1:45 pm | Teen Titans Go! | Opposites
 2:00 pm | Teen Titans Go! | Scary Figure Dance
 2:15 pm | Teen Titans Go! | Animals: It's Just a Word
 2:30 pm | Amazing World of Gumball | The Party
 2:45 pm | Amazing World of Gumball | The Refund
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 3:15 pm | Amazing World of Gumball | The Stars
 3:30 pm | Amazing World of Gumball | The Cycle
 3:45 pm | Amazing World of Gumball | The Robot
 4:00 pm | Regular Show | Prank Callers
 4:15 pm | Regular Show | Don
 4:30 pm | Regular Show | Rigby's Body
 4:45 pm | Regular Show | Mordecai and the Rigbys