# 2024-04-21
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Unruly Royal
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
 6:30 am | Amazing World of Gumball | The Spoon
 6:45 am | Amazing World of Gumball | The Pressure
 7:00 am | Amazing World of Gumball | The Painting
 7:15 am | Amazing World of Gumball | The Laziest
 7:30 am | Amazing World of Gumball | The Ghost
 7:45 am | Amazing World of Gumball | The Mystery
 8:00 am | Scooby-Doo! and Guess Who? | Scooby On Ice!
 8:30 am | Scooby-Doo! and Guess Who? | Caveman On The Half Pipe!
 9:00 am | Scooby-Doo! on Zombie Island | 
10:45 am | New Looney Tunes | Bugsfoot/Grim on Vacation
11:00 am | The Looney Tunes Show | Best Friends
11:30 am | The Looney Tunes Show | Members Only
12:00 pm | The Looney Tunes Show | We're in Big Truffle
12:30 pm | Tiny Toons Looniversity | Slay Cheese
 1:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
 1:30 pm | Tom & Jerry Show | My Bot-y Guard; Little Quacker and Mister Fuzzy Hide
 2:00 pm | Tom & Jerry Show | Pipeline; No Brain, No Gain
 2:30 pm | Steven Universe | Alone Together
 2:45 pm | Steven Universe | The Test
 3:00 pm | Adventure Time | The Real You; Guardians of Sunshine
 3:30 pm | Adventure Time | Death in Bloom; Susan Strong
 4:00 pm | Regular Show | Dead at Eight
 4:15 pm | Regular Show | Access Denied
 4:30 pm | Regular Show | Muscle Mentor
 4:45 pm | Regular Show | Trucker Hall of Fame