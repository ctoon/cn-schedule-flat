# 2024-04-21
 5:00 pm | The Lego Movie | 
 7:00 pm | Bob's Burgers | Tweentrepreneurs
 7:30 pm | Bob's Burgers | Live and Let Fly
 8:00 pm | Bob's Burgers | Bobby Driver
 8:30 pm | Bob's Burgers | Roller? I Hardly Know Her!
 9:00 pm | American Dad! | Hayley Was a Girl Scout?
 9:30 pm | American Dad! | Please Please Jeff
10:00 pm | American Dad! | Jambalaya
10:30 pm | American Dad! | Gernot and Strudel
11:00 pm | Royal Crackers | Rachel
11:30 pm | Rick and Morty | Solaricks
12:00 am | Rick and Morty | Rick: A Mort Well Lived
12:30 am | Smiling Friends | Enchanted Forest
12:45 am | Smiling Friends | Frowning Friends
 1:00 am | Aqua Teen Hunger Force | Scrip2 2i2le: The Ts Are 2s
 1:15 am | Aqua Teen Hunger Force | Get Lit Upon a Situpon
 1:30 am | American Dad! | Hayley Was a Girl Scout?
 2:00 am | American Dad! | Please Please Jeff
 2:30 am | Rick and Morty | Solaricks
 3:00 am | Rick and Morty | Rick: A Mort Well Lived
 3:30 am | Royal Crackers | Rachel
 4:00 am | Smiling Friends | Enchanted Forest
 4:15 am | Smiling Friends | Frowning Friends
 4:30 am | Aqua Teen Hunger Force | Scrip2 2i2le: The Ts Are 2s
 4:45 am | Aqua Teen Hunger Force | Get Lit Upon a Situpon
 5:00 am | Bob's Burgers | Tweentrepreneurs
 5:30 am | Bob's Burgers | Live and Let Fly