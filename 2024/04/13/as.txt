# 2024-04-13
 5:00 pm | King of the Hill | You Gotta Believe (In Moderation)
 5:30 pm | King of the Hill | Business Is Picking Up
 6:00 pm | King of the Hill | The Year of Washing Dangerously
 6:30 pm | King of the Hill | Hank Fixes Everything
 7:00 pm | Bob's Burgers | Like Gene for Chocolate
 7:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 8:00 pm | Bob's Burgers | Aquaticism
 8:30 pm | Bob's Burgers | Ain't Miss Debatin'
 9:00 pm | American Dad! | The Scarlett Getter
 9:30 pm | American Dad! | The Unbrave One
10:00 pm | American Dad! | Stanny Tendergrass
10:30 pm | American Dad! | Wheels & the Legman and the Case of Grandpa's Key
11:00 pm | Rick and Morty | The ABCs of Beth
11:30 pm | Rick and Morty | The Rickchurian Mortydate
12:00 am | Ninja Kamui | (Dub) Episode 10
12:30 am | Zom 100: Bucket List of the Dead | Best Friend of the Dead
 1:00 am | Lycoris Recoil | Recoil of Lycoris
 1:30 am | One Piece | A Showdown Between the Warlords! Law vs. Doflamingo!
 2:00 am | Naruto: Shippuden | Naruto Shippuden, Sasuke's Story: Sunrise, Part 1: The Exploding Human
 2:30 am | Dragon Ball Z Kai | The Battle with Ten-Times Gravity! Goku's Race Against the Clock!
 3:00 am | Ninja Kamui | (Sub) Episode 10
 3:30 am | Genndy Tartakovsky's Primal | The Colossaeus
 4:00 am | Futurama | The Cyber House Rules
 4:30 am | Futurama | Insane in the Mainframe
 5:00 am | King of the Hill | The Year of Washing Dangerously
 5:30 am | King of the Hill | Hank Fixes Everything