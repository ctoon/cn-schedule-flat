# 2024-05-14
 6:00 am | Regular Show | Park Managers' Lunch
 6:12 am | Regular Show | Mordecai and Rigby Down Under
 6:28 am | The Looney Tunes Show | Gribbler's Quest
 6:56 am | The Looney Tunes Show | The Grand Old Duck of York
 7:24 am | Bugs Bunny Builders | Sea School
 7:36 am | Bugs Bunny Builders | Underwater Star
 7:52 am | Meet the Batwheels | Bibi's Bad Day
 8:04 am | Meet the Batwheels | A Jet Out of Water
 8:16 am | Meet the Batwheels | Batty Body Swap
 8:32 am | Amazing World of Gumball | The Master
 8:44 am | Amazing World of Gumball | The Silence
 9:00 am | Amazing World of Gumball | The Future
 9:12 am | Amazing World of Gumball | The Wish
 9:28 am | Scooby-Doo! and Guess Who? | The Wedding Witch of Wainsly Hall!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Plucky Duck
10:00 am | Scooby-Doo! and Guess Who? | A Run Cycle Through Time!
10:30 am | Tom & Jerry Show | A Class of Their Own
10:40 am | Tom & Jerry Show | Werewolf of Catsylvania
10:50 am | Tom & Jerry Show | Scrunch Time
11:00 am | Tom & Jerry Show | The Maltese Pigeon
11:10 am | Tom & Jerry Show | Loch Ness Mess
11:20 am | Tom & Jerry Show | Mice from Mars
11:30 am | Tiny Toons Looniversity | Extra, So Extra
12:00 pm | Craig of the Creek | Sleepover at JP's
12:15 pm | Craig of the Creek | Tea Timer's Ball
12:30 pm | We Bare Bears | Everyone's Tube
12:45 pm | We Bare Bears | Money Man
 1:00 pm | Teen Titans Go! | The Power of Shrimps
 1:15 pm | Teen Titans Go! | Monster Squad!
 1:30 pm | Teen Titans Go! | The Real Orangins!
 1:45 pm | Teen Titans Go! | Quantum Fun
 2:00 pm | Teen Titans Go! | The Fight
 2:15 pm | Teen Titans Go! | The Groover
 2:30 pm | Amazing World of Gumball | The Factory
 2:45 pm | Amazing World of Gumball | The Agent
 3:00 pm | Amazing World of Gumball | The Photo
 3:15 pm | Amazing World of Gumball | The Ghouls
 3:30 pm | Amazing World of Gumball | The Web
 3:45 pm | Amazing World of Gumball | The Mess
 4:00 pm | Regular Show | Married and Broke
 4:15 pm | Regular Show | I See Turtles
 4:30 pm | Regular Show | Format Wars II
 4:45 pm | Regular Show | Happy Birthday Song Contest