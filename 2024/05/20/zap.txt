# 2024-05-20
 6:00 am | Regular Show | Sleep Cycle
 6:12 am | Regular Show | Just Friends
 6:28 am | The Looney Tunes Show | Best Friends Redux
 6:56 am | The Looney Tunes Show | SuperRabbit
 7:24 am | Bugs Bunny Builders | Crane Game
 7:36 am | Bugs Bunny Builders | Speedy
 7:52 am | Meet the Batwheels | Big Rig Bam
 8:04 am | Meet the Batwheels | Nightbike
 8:16 am | Meet the Batwheels | Kitty's Sleepover
 8:32 am | Amazing World of Gumball | The Party
 8:44 am | Amazing World of Gumball | The Refund
 9:00 am | Amazing World of Gumball | The Robot
 9:12 am | Amazing World of Gumball | The Picnic
 9:28 am | Scooby-Doo! and Guess Who? | The Feast of Dr. Frankenfooder!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Thiefstress
10:00 am | Scooby-Doo! and Guess Who? | A Fashion Nightmare!
10:30 am | Tom & Jerry Show | (Not) Your Father's Mouse-Stache
10:40 am | Tom & Jerry Show | A Clown Without Pity
10:50 am | Tom & Jerry Show | Ball of Fame
11:00 am | Tom & Jerry Show | Duck Sitting
11:10 am | Tom & Jerry Show | My Buddy Guard
11:20 am | Tom & Jerry Show | Three Heads Are Better Than One
11:30 am | Summer Camp Island | Chocolate Money Badgers
11:45 am | Summer Camp Island | Saxophone Come Home
12:00 pm | Craig of the Creek | Fire & Ice
12:15 pm | Craig of the Creek | Creek Talent Extravaganza
12:30 pm | We Bare Bears | Tabes & Charlie
12:45 pm | We Bare Bears | Panda's Birthday
 1:00 pm | Teen Titans Go! | Don't Be an Icarus
 1:15 pm | Teen Titans Go! | Stockton, CA!
 1:30 pm | Teen Titans Go! | What's Opera, Titans?
 1:45 pm | Teen Titans Go! | Royal Jelly
 2:00 pm | Teen Titans Go! | Strength of a Grown Man
 2:15 pm | Teen Titans Go! | Had to Be There
 2:30 pm | Amazing World of Gumball | The Goons
 2:45 pm | Amazing World of Gumball | The Secret
 3:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Kissy Curse
 3:15 pm | Amazing World of Gumball | The Sock
 3:30 pm | Amazing World of Gumball | The Genius
 3:45 pm | Amazing World of Gumball | The Poltergeist
 4:00 pm | Regular Show | Benson's Pig
 4:15 pm | Regular Show | The Eileen Plan
 4:30 pm | Regular Show | Hello China
 4:45 pm | Regular Show | Crazy Fake Plan