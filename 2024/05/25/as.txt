# 2024-05-25
 5:00 pm | King of the Hill | To Kill a Ladybird
 5:30 pm | King of the Hill | Old Glory
 6:00 pm | King of the Hill | Rodeo Days
 6:30 pm | King of the Hill | Hanky Panky
 7:00 pm | Bob's Burgers | Presto Tina-O
 7:30 pm | Bob's Burgers | Easy Com-Mercial, Easy Go-Mercial
 8:00 pm | Bob's Burgers | The Frond Files
 8:30 pm | Bob's Burgers | Mazel Tina
 9:00 pm | American Dad! | One-Woman Swole
 9:30 pm | American Dad! | Flavortown
10:00 pm | American Dad! | Persona Assistant
10:30 pm | American Dad! | The Legend of Old Ulysses
11:00 pm | Rick and Morty | The Rickshank Rickdemption
11:30 pm | Rick and Morty | Rickmancing the Stone
12:00 am | My Adventures with Superman | More Things in Heaven and Earth
12:30 am | My Adventures with Superman | Adventures With My Girlfriend
 1:00 am | One Piece | The Final Round Starts! Diamante the Hero Shows Up!
 1:30 am | Naruto: Shippuden | Naruto Shippuden, Sasuke's Story: Sunrise, Part 5: The Last One
 2:00 am | Dragon Ball Z Kai | Will Goku Make it in Time?! Three Hours Until the Battle Resumes!
 2:30 am | Demon Slayer: Kimetsu no Yaiba | Sound Hashira Tengen Uzui
 3:30 am | Genndy Tartakovsky's Primal | A Cold Death
 4:00 am | Futurama | The Farnsworth Parabox
 4:30 am | Futurama | Three Hundred Big Boys
 5:00 am | King of the Hill | To Kill a Ladybird
 5:30 am | King of the Hill | Old Glory