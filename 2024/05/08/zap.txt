# 2024-05-08
 6:00 am | Regular Show | Saving Time
 6:12 am | Regular Show | Guitar of Rock
 6:28 am | The Looney Tunes Show | Dear John
 6:56 am | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
 7:24 am | Bugs Bunny Builders | Cousin Billy
 7:36 am | Bugs Bunny Builders | Cheddar Days
 7:52 am | Meet the Batwheels | Batty Race
 8:04 am | Meet the Batwheels | When You're a Jet
 8:16 am | Meet the Batwheels | Ride Along
 8:32 am | Amazing World of Gumball | The Petals
 8:44 am | Amazing World of Gumball | The Puppets
 9:00 am | Amazing World of Gumball | The Nuisance
 9:12 am | Amazing World of Gumball | The Line
 9:28 am | Scooby-Doo! and Guess Who? | Quit Clowning!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Talking Tree
10:00 am | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!
10:30 am | Tom & Jerry Show | Truffle Trouble
10:40 am | Tom & Jerry Show | Cuckoo Clock
10:50 am | Tom & Jerry Show | Plant Food
11:00 am | Tom & Jerry Show | Bird Flue
11:10 am | Tom & Jerry Show | A Star Forlorn
11:20 am | Tom & Jerry Show | Eggs on a Train
11:30 am | Tiny Toons Looniversity | General HOGspital
12:00 pm | Craig of the Creek | The Jinxening
12:15 pm | Craig of the Creek | In the Key of the Creek
12:30 pm | We Bare Bears | Chloe and Ice Bear
12:45 pm | We Bare Bears | Road Trip
 1:00 pm | Teen Titans Go! | Chapter One: I Saw You Dance
 1:15 pm | Teen Titans Go! | Chapter Two: The Story in Your Eyes
 1:30 pm | Teen Titans Go! | Chapter Three: Playing Hard to Get
 1:45 pm | Teen Titans Go! | Chapter Four: The Night Begins to Shine
 2:00 pm | Teen Titans Go! | Labor Day
 2:15 pm | Teen Titans Go! | Classic Titans
 2:30 pm | Amazing World of Gumball | The List
 2:45 pm | Amazing World of Gumball | The News
 3:00 pm | Amazing World of Gumball | The Hero
 3:15 pm | Amazing World of Gumball | The Rival
 3:30 pm | Amazing World of Gumball | The Lady
 3:45 pm | Amazing World of Gumball | The Sucker
 4:00 pm | Regular Show | Return of Mordecai and the Rigbys
 4:15 pm | Regular Show | Bad Portrait
 4:30 pm | Regular Show | Video 101
 4:45 pm | Regular Show | I Like You Hi