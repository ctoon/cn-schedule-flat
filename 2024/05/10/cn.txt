# 2024-05-10
 6:00 am | Regular Show | Skips in the Saddle
 6:12 am | Regular Show | Thomas Fights Back
 6:28 am | The Looney Tunes Show | Daffy Duck Esquire
 6:56 am | The Looney Tunes Show | Spread Those Wings and Fly
 7:24 am | Bugs Bunny Builders | Goofballs
 7:36 am | Bugs Bunny Builders | Party Boat
 7:52 am | Jessica's Big Little World | Small Uncle's Big Bath
 8:04 am | Jessica's Big Little World | The Big Kid Playground
 8:16 am | Jessica's Big Little World | Grocery Store Friend
 8:32 am | Amazing World of Gumball | The Neighbor
 8:44 am | Amazing World of Gumball | The Shippening
 9:00 am | Amazing World of Gumball | The Brain
 9:12 am | Amazing World of Gumball | The Parents
 9:28 am | Scooby-Doo! and Guess Who? | The New York Underground!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Moany Mountain
10:00 am | Scooby-Doo! and Guess Who? | Fear of the Fire Beast!
10:30 am | Tom & Jerry Show | Bull Fight
10:40 am | Tom & Jerry Show | Whack a Gopher
10:50 am | Tom & Jerry Show | Mirror Image
11:00 am | Tom & Jerry Show | Fortune Hunters
11:10 am | Tom & Jerry Show | A Game of Bones
11:20 am | Tom & Jerry Show | Suitable for Framing
11:30 am | Tiny Toons Looniversity | Tears of a Clone
12:00 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
12:15 pm | Craig of the Creek | Sink or Swim Team
12:30 pm | We Bare Bears | Bear Flu
12:45 pm | We Bare Bears | The Perfect Tree
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:30 pm | Teen Titans Go! | Beast Girl
 1:45 pm | Teen Titans Go! | Bro-Pocalypse
 2:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 2:15 pm | Teen Titans Go! | TV Knight 3
 2:30 pm | Amazing World of Gumball | The Founder
 2:45 pm | Amazing World of Gumball | The Schooling
 3:00 pm | Amazing World of Gumball | The Sidekick
 3:15 pm | Amazing World of Gumball | The Intelligence
 3:30 pm | Amazing World of Gumball | The Potion
 3:45 pm | Amazing World of Gumball | The Spinoffs
 4:00 pm | Regular Show | Bachelor Party! Zingo!
 4:15 pm | Regular Show | Tent Trouble
 4:30 pm | Regular Show | Real Date
 4:45 pm | Regular Show | Maxin' and Relaxin'