# 2024-05-09
 6:00 am | Regular Show | Play Date
 6:12 am | Regular Show | Expert or Liar
 6:28 am | The Looney Tunes Show | We're in Big Truffle
 6:56 am | The Looney Tunes Show | A Christmas Carol
 7:24 am | Bugs Bunny Builders | Bright Light
 7:36 am | Bugs Bunny Builders | Mini Golf
 7:52 am | Meet the Batwheels | Wheels Just Want to Have Fun
 8:04 am | Meet the Batwheels | Tough Buff Blues
 8:16 am | Meet the Batwheels | Wheel Side Story
 8:32 am | Amazing World of Gumball | The Vegging
 8:44 am | Amazing World of Gumball | The One
 9:00 am | Amazing World of Gumball | The Father
 9:12 am | Amazing World of Gumball | The Cringe
 9:28 am | Scooby-Doo! and Guess Who? | One Minute Mysteries!
 9:56 am | Tom & Jerry | Colourful Chase
10:00 am | Scooby-Doo! and Guess Who? | Hollywood Knights!
10:30 am | Tom & Jerry Show | Game Changer
10:40 am | Tom & Jerry Show | Bars and Stripes
10:50 am | Tom & Jerry Show | Rosemary's Gravy
11:00 am | Tom & Jerry Show | Hula Whoops
11:10 am | Tom & Jerry Show | Kiss and Makeup
11:20 am | Tom & Jerry Show | No Contest
11:30 am | Tiny Toons Looniversity | Give Pizza a Chance
12:00 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
12:15 pm | Craig of the Creek | Afterschool Snackdown
12:30 pm | We Bare Bears | Slumber Party
12:45 pm | We Bare Bears | More Everyone's Tube
 1:00 pm | Teen Titans Go! | Ones and Zeros
 1:15 pm | Teen Titans Go! | Career Day
 1:30 pm | Teen Titans Go! | TV Knight 2
 1:45 pm | Teen Titans Go! | The Academy
 2:00 pm | Teen Titans Go! | Throne of Bones
 2:15 pm | Teen Titans Go! | Demon Prom
 2:30 pm | Amazing World of Gumball | The Cage
 2:45 pm | Amazing World of Gumball | The Faith
 3:00 pm | Amazing World of Gumball | The Dream
 3:15 pm | Amazing World of Gumball | The Candidate
 3:30 pm | Amazing World of Gumball | The Anybody
 3:45 pm | Amazing World of Gumball | The Pact
 4:00 pm | Regular Show | Catching the Wave
 4:15 pm | Regular Show | Gold Watch
 4:30 pm | Regular Show | Paint Job
 4:45 pm | Regular Show | Take the Cake