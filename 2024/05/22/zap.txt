# 2024-05-22
 6:00 am | Regular Show | Gary's Synthesizer
 6:12 am | Regular Show | California King
 6:28 am | The Looney Tunes Show | Jailbird and Jailbunny
 6:56 am | The Looney Tunes Show | Fish and Visitors
 7:24 am | Bugs Bunny Builders | Glamp Out
 7:36 am | Bugs Bunny Builders | Outer Space
 7:52 am | Meet the Batwheels | Monster Truck Amok
 8:04 am | Meet the Batwheels | Air-Show, Don't Tell
 8:16 am | Meet the Batwheels | Bat-Light Blow-Out
 8:32 am | Amazing World of Gumball | The Helmet
 8:44 am | Amazing World of Gumball | The Fight
 9:00 am | Amazing World of Gumball | The Colossus
 9:12 am | Amazing World of Gumball | The Knights
 9:28 am | Scooby-Doo! and Guess Who? | The Crown Jewel of Boxing!
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
10:00 am | Scooby-Doo! and Guess Who? | The Internet on Haunted House Hill!
10:30 am | Tom & Jerry Show | Always Say Never Again
10:40 am | Tom & Jerry Show | Mice Fair Ladies
10:50 am | Tom & Jerry Show | Play Date With Destiny
11:00 am | Tom & Jerry Show | Wild Goose Chase
11:10 am | Tom & Jerry Show | Hot Wings
11:20 am | Tom & Jerry Show | Oh, Brother
11:30 am | Summer Camp Island | Monster Babies
11:45 am | Summer Camp Island | Hedgehog Werewolf
12:00 pm | Craig of the Creek | Jessica's Trail
12:15 pm | Craig of the Creek | Return of the Honeysuckle Rangers
12:30 pm | We Bare Bears | Food Truck
12:45 pm | We Bare Bears | Panda's Date
 1:00 pm | Teen Titans Go! | Collect Them All
 1:15 pm | Teen Titans Go! | Butt Atoms
 1:30 pm | Teen Titans Go! | Super Hero Summer Camp
 2:30 pm | Amazing World of Gumball | The Fridge
 2:45 pm | Amazing World of Gumball | The Flower
 3:00 pm | Amazing World of Gumball | The Crew
 3:15 pm | Amazing World of Gumball | The Banana
 3:30 pm | Amazing World of Gumball | The Phone
 3:45 pm | Amazing World of Gumball | The Job
 4:00 pm | Regular Show | Cube Bros
 4:15 pm | Regular Show | Maellard's Package
 4:30 pm | Regular Show | Rigby Goes to the Prom
 4:45 pm | Regular Show | The Button