# 2024-05-07
 6:00 am | Regular Show | The Heart of a Stuntman
 6:12 am | Regular Show | Dodge This
 6:28 am | The Looney Tunes Show | Customer Service
 6:56 am | The Looney Tunes Show | It's a Handbag!
 7:24 am | Bugs Bunny Builders | Blast Off
 7:36 am | Bugs Bunny Builders | K-9: Space Puppy
 7:52 am | Meet the Batwheels | Harley Did It
 8:04 am | Meet the Batwheels | Buff in a China Shop
 8:16 am | Meet the Batwheels | A Tale of Two Bibis
 8:32 am | Amazing World of Gumball | The Sorcerer
 8:44 am | Amazing World of Gumball | The Menu
 9:00 am | Amazing World of Gumball | The Uncle
 9:12 am | Amazing World of Gumball | The Weirdo
 9:28 am | Scooby-Doo! and Guess Who? | Now You Sia, Now You Don't!
 9:56 am | Tom & Jerry | Be Careful What You Fish For
10:00 am | Scooby-Doo! and Guess Who? | The Nightmare Ghost of Psychic U!
10:30 am | Tom & Jerry Show | Charm School Dropouts
10:40 am | Tom & Jerry Show | Driven Crazy
10:50 am | Tom & Jerry Show | Alley Oops!
11:00 am | Tom & Jerry Show | Saddle Soreheads
11:10 am | Tom & Jerry Show | Magic Hat Cat
11:20 am | Tom & Jerry Show | Tom's Tangled Web
11:30 am | Tiny Toons Looniversity | Prank You Very Much
12:00 pm | Craig of the Creek | Council of the Creek
12:15 pm | Craig of the Creek | Crisis at Elder Rock
12:30 pm | We Bare Bears | My Clique
12:45 pm | We Bare Bears | The Demon
 1:00 pm | Teen Titans Go! | Orangins
 1:15 pm | Teen Titans Go! | Jinxed
 1:30 pm | Teen Titans Go! | Brain Percentages
 1:45 pm | Teen Titans Go! | BL4Z3
 2:00 pm | Teen Titans Go! | Hot Salad Water
 2:15 pm | Teen Titans Go! | Lication
 2:30 pm | Amazing World of Gumball | The Heist
 2:45 pm | Amazing World of Gumball | The Singing
 3:00 pm | Amazing World of Gumball | The Virus
 3:15 pm | Amazing World of Gumball | The Pony
 3:30 pm | Amazing World of Gumball | The Worst
 3:45 pm | Amazing World of Gumball | The Deal
 4:00 pm | Regular Show | Portable Toilet
 4:15 pm | Regular Show | The Postcard
 4:30 pm | Regular Show | Rigby in the Sky With Burrito
 4:45 pm | Regular Show | Journey to the Bottom of the Crash Pit