# 2024-05-19
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Playdate Part 1; The Prince and The Playdate Part 2
 6:30 am | Amazing World of Gumball | The Choices
 6:45 am | Amazing World of Gumball | The Fury
 7:00 am | Amazing World of Gumball | The Fridge
 7:15 am | Amazing World of Gumball | The List
 7:30 am | Amazing World of Gumball | The Parents
 7:45 am | Amazing World of Gumball | The Factory
 8:00 am | Scooby-Doo! and Guess Who? | The Dreaded Remake Of Jekyll & Hyde
 8:30 am | Scooby-Doo! and Guess Who? | Dance Matron of Mayhem!
 9:00 am | Scooby-Doo! in Where's My Mummy? | 
10:30 am | The Looney Tunes Show | Members Only
11:00 am | The Looney Tunes Show | You've Got Hate Mail
11:30 am | The Looney Tunes Show | Rebel Without a Glove
12:00 pm | The Looney Tunes Show | Point, Laser, Point
12:30 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 1:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
 1:30 pm | Tom & Jerry Show | Birds of a Feather; Vampire Mouse
 2:00 pm | Tom & Jerry Show | Top Cat; Mummy Dearest
 2:30 pm | Steven Universe | Rose's Scabbard
 2:45 pm | Steven Universe | The Message
 3:00 pm | Adventure Time | Islands, Part 5: Hide and Seek
 3:15 pm | Adventure Time | Islands, Part 6: Min & Marty
 3:30 pm | Adventure Time | Islands, Part 7: Helpers
 3:45 pm | Adventure Time | Islands, Part 8: The Light Cloud
 4:00 pm | Regular Show | My Mom
 4:15 pm | Regular Show | A Bunch of Baby Ducks
 4:30 pm | Regular Show | Maxin' and Relaxin'
 4:45 pm | Regular Show | Rigby Goes to the Prom