# 2024-05-15
 6:00 am | Regular Show | Benson's Suit
 6:12 am | Regular Show | Gamers Never Say Die
 6:28 am | The Looney Tunes Show | Ridiculous Journey
 6:56 am | The Looney Tunes Show | The Shell Game
 7:24 am | Bugs Bunny Builders | Batty Kathy
 7:36 am | Bugs Bunny Builders | Mail Whale
 7:52 am | Meet the Batwheels | Ace in the Hole
 8:04 am | Meet the Batwheels | Mechanic Panic
 8:16 am | Meet the Batwheels | Improvise My Ride
 8:32 am | Amazing World of Gumball | The Heart
 8:44 am | Amazing World of Gumball | The Revolt
 9:00 am | Amazing World of Gumball | The Decisions
 9:12 am | Amazing World of Gumball | The BFFS
 9:28 am | Scooby-Doo! and Guess Who? | I Put a Hex on You!
 9:56 am | Tom & Jerry | What Goes Around Comes Around
10:00 am | Scooby-Doo! and Guess Who? | The High School Wolfman's Musical Lament!
10:30 am | Tom & Jerry Show | The Great Catsby
10:40 am | Tom & Jerry Show | Yeti, Set, Go
10:50 am | Tom & Jerry Show | What About Blob?
11:00 am | Tom & Jerry Show | Mouse Party
11:10 am | Tom & Jerry Show | Ghoul's Gold
11:20 am | Tom & Jerry Show | Maust
11:30 am | Tiny Toons Looniversity | Save The Loo Bru
12:00 pm | Craig of the Creek | Trading Day
12:15 pm | Craig of the Creek | The End Was Here
12:30 pm | We Bare Bears | The Library
12:45 pm | We Bare Bears | Hibernation
 1:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 1:30 pm | Teen Titans Go! | Flashback
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 2:30 pm | Amazing World of Gumball | The Inquisition
 2:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 3:00 pm | Amazing World of Gumball | The Tag
 3:15 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 3:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 3:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Alan
 4:00 pm | Regular Show | 1000th Chopper Flight Party
 4:15 pm | Regular Show | Party Horse
 4:30 pm | Regular Show | Men in Uniform
 4:45 pm | Regular Show | Garage Door