# 2024-05-04
 6:00 am | Regular Show | Replaced
 6:15 am | Regular Show | Trash Boat
 6:30 am | Regular Show | Fists of Justice
 6:45 am | Regular Show | Yes Dude Yes
 7:00 am | Regular Show | Fuzzy Dice
 7:15 am | Regular Show | Sugar Rush
 7:30 am | Scooby-Doo! and the Alien Invaders | 
 9:00 am | Tiny Toons Looniversity | Twin-Con
 9:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Dark Lord of Moletown
 9:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Show-Off Stallion
10:00 am | Teen Titans Go! | Wild Card
10:15 am | Teen Titans Go! | Polly Ethylene and Tara Phthalate
10:30 am | Teen Titans Go! | EEBows
10:45 am | Teen Titans Go! | Batman's Birthday Gift
11:00 am | Teen Titans Go! | What a Boy Wonders
11:15 am | Teen Titans Go! | 50% Crew
11:30 am | Teen Titans Go! | The Drip
11:45 am | Teen Titans Go! | Standards & Practices
12:00 pm | The Looney Tunes Show | Muh-Muh-Muh-Murder
12:30 pm | The Looney Tunes Show | Point, Laser Point
 1:00 pm | Tiny Toons Looniversity | Twin-Con
 1:30 pm | Amazing World of Gumball | The Banana
 1:45 pm | Amazing World of Gumball | The Phone
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Dark Lord of Moletown
 2:15 pm | Amazing World of Gumball | The Job
 2:30 pm | Amazing World of Gumball | The Remote
 2:45 pm | Amazing World of Gumball | The Treasure
 3:00 pm | Amazing World of Gumball | The Sidekick
 3:15 pm | Amazing World of Gumball | The Photo
 3:30 pm | Amazing World of Gumball | The Microwave
 3:45 pm | Amazing World of Gumball | The Meddler
 4:00 pm | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
 4:30 pm | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!