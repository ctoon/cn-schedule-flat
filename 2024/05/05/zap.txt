# 2024-05-05
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Dark Lord of Moletown
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Swoony Swanstress
 6:30 am | Amazing World of Gumball | The Apology
 6:45 am | Amazing World of Gumball | The Words
 7:00 am | Amazing World of Gumball | The Skull
 7:15 am | Amazing World of Gumball | The Bet
 7:30 am | Amazing World of Gumball | The Watch
 7:45 am | Amazing World of Gumball | The Bumpkin
 8:00 am | Scooby-Doo! and Guess Who? | Attack of the Weird Al-losaurus!
 8:30 am | Scooby-Doo! and Guess Who? | When Urkel-Bots Go Bad!
 9:00 am | Scooby-Doo! and the Monster of Mexico | 
10:30 am | The Looney Tunes Show | Bobcats on Three
11:00 am | The Looney Tunes Show | You've Got Hate Mail
11:30 am | The Looney Tunes Show | Itsy Bitsy Gopher
12:00 pm | The Looney Tunes Show | Rebel Without a Glove
12:30 pm | Tiny Toons Looniversity | Twin-Con
 1:00 pm | Tiny Toons Looniversity | Save The Loo Bru
 1:30 pm | Tom & Jerry Show | Ghost Party; Cat-Astrophe
 2:00 pm | Tom & Jerry Show | Curse Case Scenario; Say Cheese
 2:30 pm | Steven Universe | Horror Club
 2:45 pm | Steven Universe | Winter Forecast
 3:00 pm | Adventure Time | Video Makers; Heat Signature
 3:30 pm | Adventure Time | Mortal Folly; Mortal Recoil
 4:00 pm | Regular Show | Busted Cart
 4:15 pm | Regular Show | Dead at Eight
 4:30 pm | Regular Show | Access Denied
 4:45 pm | Regular Show | Muscle Mentor