# 2024-05-05
 5:00 pm | Mortal Kombat | 
 7:00 pm | Bob's Burgers | Bridge Over Troubled Rudy
 7:30 pm | Bob's Burgers | Steal Magazine-Olias
 8:00 pm | Bob's Burgers | Tell Me Dumb Thing Good
 8:30 pm | Bob's Burgers | Vampire Disco Death Dance
 9:00 pm | American Dad! | The Book of Fischer
 9:30 pm | American Dad! | A Roger Story
10:00 pm | American Dad! | Epic Powder Dump
10:30 pm | American Dad! | American Dad Graffito
11:00 pm | Royal Crackers | Dog
11:30 pm | Rick and Morty | Final DeSmithation
12:00 am | Rick and Morty | JuRicksic Mort
12:30 am | Smiling Friends | The Smiling Friends Go to Brazil!
12:45 am | Smiling Friends | Desmond's Big Day Out
 1:00 am | Aqua Teen Hunger Force | Brain Fairy
 1:15 am | Aqua Teen Hunger Force | The Hairy Bus
 1:30 am | American Dad! | The Book of Fischer
 2:00 am | American Dad! | A Roger Story
 2:30 am | Rick and Morty | Final DeSmithation
 3:00 am | Rick and Morty | JuRicksic Mort
 3:30 am | Royal Crackers | Dog
 4:00 am | Smiling Friends | The Smiling Friends Go to Brazil!
 4:15 am | Smiling Friends | Desmond's Big Day Out
 4:30 am | Aqua Teen Hunger Force | Brain Fairy
 4:45 am | Aqua Teen Hunger Force | The Hairy Bus
 5:00 am | Bob's Burgers | Bridge Over Troubled Rudy
 5:30 am | Bob's Burgers | Steal Magazine-Olias