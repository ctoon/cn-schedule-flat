# 2024-05-24
 6:00 am | Regular Show | Deez Keys
 6:12 am | Regular Show | One Space Day at a Time
 6:28 am | The Looney Tunes Show | Casa de Calma
 6:56 am | The Looney Tunes Show | Devil Dog
 7:24 am | Bugs Bunny Builders | Ice Creamed
 7:36 am | Bugs Bunny Builders | Race Track Race
 7:52 am | Meet the Batwheels | Multi-MOE
 8:04 am | Meet the Batwheels | Razzle Dazzle Frazzle
 8:16 am | Meet the Batwheels | Big Rig Bam
 8:32 am | Amazing World of Gumball | The Bumpkin
 8:44 am | Amazing World of Gumball | The Flakers
 9:00 am | Amazing World of Gumball | The Authority
 9:12 am | Amazing World of Gumball | The Virus
 9:28 am | Scooby-Doo! and Guess Who? | The Last Inmate!
 9:56 am | Tom & Jerry | Ice Ice Paradise
10:00 am | Scooby-Doo! and Guess Who? | Lost Soles Of Jungle River!
10:30 am | Tom & Jerry Show | Not So Grand Canyon
10:40 am | Tom & Jerry Show | Cave Cat
10:50 am | Tom & Jerry Show | Hold the Cheese
11:00 am | Tom & Jerry Show | Ape for Tom and Jerry
11:10 am | Tom & Jerry Show | Eight Legs No Waiting
11:20 am | Tom & Jerry Show | Giant Problems
11:30 am | Summer Camp Island | Pajama Pajimjams
11:45 am | Summer Camp Island | The First Day
12:00 pm | Craig of the Creek | The Other Side: The Tournament
12:30 pm | We Bare Bears | Primal
12:45 pm | We Bare Bears | Jean Jacket
 1:00 pm | Teen Titans Go! | Record Book
 1:15 pm | Teen Titans Go! | Magic Man
 1:30 pm | Teen Titans Go! | The Titans Go Casual
 1:45 pm | Teen Titans Go! | Rain on Your Wedding Day
 2:00 pm | Teen Titans Go! | Teen Titans Roar!
 2:15 pm | Teen Titans Go! | Ghost With the Most
 2:30 pm | Amazing World of Gumball | The Pony
 2:45 pm | Amazing World of Gumball | The Hero
 3:00 pm | Amazing World of Gumball | The Signature
 3:15 pm | Amazing World of Gumball | The Dream
 3:30 pm | Amazing World of Gumball | The Sidekick
 3:45 pm | Amazing World of Gumball | The Photo
 4:00 pm | Regular Show | Rigby's Graduation Day Special
 4:30 pm | Regular Show | Welcome to Space
 4:45 pm | Regular Show | Lost and Found