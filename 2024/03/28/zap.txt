# 2024-03-28
 6:00 am | Amazing World of Gumball | The Best
 6:15 am | Amazing World of Gumball | The Worst
 6:29 am | The Looney Tunes Show | Casa de Calma
 6:57 am | Lamput | Martial Art
 7:00 am | The Looney Tunes Show | Devil Dog
 7:30 am | Bugs Bunny Builders | Glamp Out
 7:45 am | Bugs Bunny Builders | Looney Moon
 8:00 am | Meet the Batwheels | Rev and Let Rev
 8:14 am | Meet the Batwheels | Fly
 8:17 am | Meet the Batwheels | Batcomputer for a Day
 8:31 am | We Baby Bears | The Magical Box
 8:45 am | We Baby Bears | Bears and the Beanstalk
 9:00 am | Craig of the Creek | The Ice Pop Trio
 9:15 am | Craig of the Creek | Pencil Break Mania
 9:29 am | We Bare Bears | Bear Lift
 9:43 am | We Bare Bears | The Nom Nom Show
 9:57 am | Lamput | Doc Dog
10:00 am | Scooby-Doo! and Guess Who? | Cher, Scooby And The Sargasso Sea!
10:30 am | Scooby-Doo! and Guess Who? | The Lost Mines of Kilimanjaro!
11:00 am | Amazing World of Gumball | The Choices
11:15 am | Amazing World of Gumball | The Code
11:30 am | Amazing World of Gumball | The Scam
11:45 am | Amazing World of Gumball | The Test
12:00 pm | Amazing World of Gumball | The List
12:15 pm | Amazing World of Gumball | The News
12:30 pm | Clarence | Man of the House
12:45 pm | Clarence | Puddle Eyes
 1:00 pm | Clarence | Dream Boat
 1:15 pm | Clarence | Slumber Party
 1:30 pm | Craig of the Creek | The Last Game of Summer
 1:45 pm | Craig of the Creek | Fall Anthology
 2:00 pm | Teen Titans Go! | 50% Crew
 2:15 pm | Teen Titans Go! | Feed Me
 2:30 pm | Teen Titans Go! | Colors of Raven
 2:45 pm | Teen Titans Go! | The Left Leg
 3:00 pm | Tiny Toons Looniversity | General HOGspital
 3:30 pm | Amazing World of Gumball | The Puppets
 3:45 pm | Amazing World of Gumball | The Nuisance
 4:00 pm | Regular Show | The Brain of Evil
 4:15 pm | Regular Show | Fries Night
 4:30 pm | Regular Show | Spacey McSpaceTree
 4:45 pm | Regular Show | Can You Ear Me Now?