# 2024-03-02
 6:00 am | Regular Show | Skips in the Saddle
 6:15 am | Regular Show | Thomas Fights Back
 6:30 am | Regular Show | Bachelor Party! Zingo!
 6:45 am | Regular Show | Tent Trouble
 7:00 am | Amazing World of Gumball | The Question
 7:15 am | Amazing World of Gumball | The Saint
 7:30 am | Amazing World of Gumball | The Friend
 7:45 am | Amazing World of Gumball | The Oracle
 8:00 am | Amazing World of Gumball | The Safety
 8:15 am | Amazing World of Gumball | The Society
 8:30 am | Amazing World of Gumball | The Spoiler
 8:45 am | Amazing World of Gumball | The Countdown
 9:00 am | Tiny Toons Looniversity | Tears of a Clone
 9:30 am | Teen Titans Go! | Ship
 9:45 am | Teen Titans Go! | Intro
10:00 am | Teen Titans Go! | Cy & Beasty
10:15 am | Teen Titans Go! | T Is for Titans
10:30 am | Teen Titans Go! | Creative Geniuses
10:45 am | Teen Titans Go! | Manor and Mannerisms
11:00 am | Teen Titans Go! | Trans Oceanic Magical Cruise
11:15 am | Teen Titans Go! | Ship
11:30 am | Craig of the Creek | Who Is the Red Poncho?
11:45 am | Craig of the Creek | The Once and Future King
12:00 pm | The Looney Tunes Show | Eligible Bachelors
12:30 pm | The Looney Tunes Show | Peel of Fortune
 1:00 pm | Tiny Toons Looniversity | Extra, So Extra
 1:30 pm | Amazing World of Gumball | The Nobody
 1:45 pm | Amazing World of Gumball | The Downer
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Playdate Part 1; The Prince and The Playdate Part 2
 2:30 pm | Amazing World of Gumball | The Egg
 2:45 pm | Amazing World of Gumball | The Return
 3:00 pm | Teen Titans Go! Vs. Teen Titans | 
 4:45 pm | Teen Titans Go! | Ship