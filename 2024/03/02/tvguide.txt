# 2024-03-02
 6:00 am | Regular Show | -
 6:15 am | Regular Show | -
 6:30 am | Regular Show | -
 6:45 am | Regular Show | -
 7:00 am | Amazing World of Gumball | -
 7:15 am | Amazing World of Gumball | -
 7:30 am | Amazing World of Gumball | -
 7:45 am | Amazing World of Gumball | -
 8:00 am | Amazing World of Gumball | -
 8:15 am | Amazing World of Gumball | -
 8:30 am | Amazing World of Gumball | -
 8:45 am | Amazing World of Gumball | -
 9:00 am | Tiny Toons Looniversity | -
 9:30 am | Teen Titans Go! | -
 9:45 am | Teen Titans Go! | -
10:00 am | Teen Titans Go! | -
10:15 am | Teen Titans Go! | -
10:30 am | Teen Titans Go! | -
10:45 am | Teen Titans Go! | -
11:00 am | Teen Titans Go! | -
11:15 am | Teen Titans Go! | -
11:30 am | Craig of the Creek | -
11:45 am | Craig of the Creek | -
12:00 pm | The Looney Tunes Show | -
12:30 pm | The Looney Tunes Show | -
 1:00 pm | Tiny Toons Looniversity | -
 1:30 pm | Amazing World of Gumball | -
 1:45 pm | Amazing World of Gumball | -
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | -
 2:30 pm | Amazing World of Gumball | -
 2:45 pm | Amazing World of Gumball | -
 3:00 pm | Teen Titans Go! Vs. Teen Titans | -
 4:45 pm | Teen Titans Go! | -