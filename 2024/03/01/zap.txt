# 2024-03-01
 6:00 am | Amazing World of Gumball | The Party
 6:14 am | Amazing World of Gumball | The Refund
 6:28 am | The Looney Tunes Show | French Fries
 6:56 am | Tom & Jerry | Count On Merli
 7:00 am | Tiny Toons Looniversity | The Show Must Hop On
 7:29 am | Bugs Bunny Builders | Mini Golf
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Boogie Button
 7:46 am | Bugs Bunny Builders | Goofballs
 8:00 am | Lucas the Spider | Lucas, We Have a Problem; Egg-Scuse Me; Pokey Dokey
 8:27 am | Lellobee City Farm | Wake Up Dance
 8:30 am | Jessica's Big Little World | Little Twin
 8:45 am | Jessica's Big Little World | Spike the Hedgehog
 9:00 am | Craig of the Creek | Dog Decider
 9:14 am | Craig of the Creek | Bring Out Your Beast
 9:28 am | We Bare Bears | Pet Shop
 9:42 am | We Bare Bears | Chloe and Ice Bear
 9:56 am | Tom & Jerry | Count On Merli
10:00 am | Scooby-Doo! and Guess Who? | Dark Diner of Route 66!
10:30 am | Scooby-Doo! and Guess Who? | A Haunt of a Thousand Voices!
11:00 am | Amazing World of Gumball | The Party
11:15 am | Amazing World of Gumball | The Refund
11:30 am | Amazing World of Gumball | The Robot
11:45 am | Amazing World of Gumball | The Picnic
12:00 pm | Apple & Onion | Tips
12:15 pm | Apple & Onion | Falafel's Fun Day
12:30 pm | Clarence | Company Man
12:45 pm | Clarence | Stump Brothers
 1:00 pm | Clarence | The Tails of Mardrynia
 1:15 pm | Clarence | Clarence Wendle and the Eye of Coogan
 1:30 pm | Craig of the Creek | Lost in the Sewer
 1:45 pm | Craig of the Creek | The Future Is Cardboard
 2:00 pm | Teen Titans Go! | Utility Belt
 2:15 pm | Teen Titans Go! | Intro
 2:30 pm | Teen Titans Go! | The Brain of the Family
 3:00 pm | Amazing World of Gumball | The Secret
 3:15 pm | Amazing World of Gumball | The Goons
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 3:45 pm | Amazing World of Gumball | The Sock
 4:00 pm | Amazing World of Gumball | The Genius
 4:15 pm | Amazing World of Gumball | The Poltergeist
 4:30 pm | Regular Show | Survival Skills
 4:45 pm | Regular Show | Tants