# 2024-03-12
 6:00 am | Amazing World of Gumball | The Joy
 6:15 am | Amazing World of Gumball | The Puppy
 6:29 am | The Looney Tunes Show | Itsy Bitsy Gopher
 6:57 am | Lamput | Snow
 7:00 am | The Looney Tunes Show | Rebel Without a Glove
 7:29 am | Bugs Bunny Builders | Speedy
 7:43 am | Bugs Bunny Builders: Hard Hat Time | Car Wash
 7:46 am | Bugs Bunny Builders | Splash Zone
 8:00 am | Meet the Batwheels | Mechanic Panic
 8:14 am | Meet the Batwheels | Jet Set
 8:17 am | Meet the Batwheels | Improvise My Ride
 8:31 am | We Baby Bears | Bath on the Nile
 8:45 am | We Baby Bears | Bubble Fields
 9:00 am | Craig of the Creek | The Mystery of the Timekeeper
 9:15 am | Craig of the Creek | Alone Quest
 9:29 am | We Bare Bears | Everyone's Tube
 9:43 am | We Bare Bears | Creature Mysteries
 9:57 am | Lamput | Signs
10:00 am | Scooby-Doo! and Guess Who? | Quit Clowning!
10:30 am | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!
11:00 am | Amazing World of Gumball | The Bros
11:15 am | Amazing World of Gumball | The Mirror
11:30 am | Amazing World of Gumball | The Man
11:45 am | Amazing World of Gumball | The Pizza
12:00 pm | Amazing World of Gumball | The Fraud
12:15 pm | Amazing World of Gumball | The Void
12:30 pm | Clarence | Clarence's Stormy Sleepover Special
12:45 pm | Clarence | Jeffrey Wendle
 1:00 pm | Clarence | Badgers N' Bunkers
 1:15 pm | Clarence | Dingus and McNobrain
 1:30 pm | Craig of the Creek | Memories of Bobby
 1:45 pm | Craig of the Creek | Jacob of the Creek
 2:00 pm | Teen Titans Go! | Ship
 2:15 pm | Teen Titans Go! | Sweet Revenge
 2:30 pm | Teen Titans Go! | Porch Pirates
 2:45 pm | Teen Titans Go! | A Sticky Situation
 3:00 pm | Amazing World of Gumball | The Extras
 3:15 pm | Amazing World of Gumball | The Gripes
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Chicken Of Doom
 3:45 pm | Amazing World of Gumball | The Vacation
 4:00 pm | Regular Show | Not Great Double Date
 4:15 pm | Regular Show | Death Kwon Do-Livery
 4:30 pm | Regular Show | Lunch Break
 4:45 pm | Regular Show | Dumped at the Altar