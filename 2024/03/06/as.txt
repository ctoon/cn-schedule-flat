# 2024-03-06
 5:00 pm | Dexter's Laboratory | Rushmore Rumble/A Boy and His Bug/You Vegetabelieve It
 5:30 pm | Dexter's Laboratory | Dad Is Disturbed/Framed/That's Using Your Head
 6:00 pm | Courage the Cowardly Dog | The Revenge of Chicken from Outer Space/Journey to the Center of Nowhere
 6:30 pm | Courage the Cowardly Dog | Little Muriel/The Great Fusilli
 7:00 pm | King of the Hill | Bobby Goes Nuts
 7:30 pm | King of the Hill | Soldier of Misfortune
 8:00 pm | Bob's Burgers | Bob Belcher and the Terrible, Horrible, No Good, Very Bad Kids
 8:30 pm | Bob's Burgers | Diarrhea of a Poopy Kid
 9:00 pm | Bob's Burgers | The Terminalator II: Terminals of Endearment
 9:30 pm | American Dad! | Don't You Be My Neighbor
10:00 pm | American Dad! | Productive Panic
10:30 pm | American Dad! | Multiverse of American Dadness
11:00 pm | American Dad! | A New Era for the Smith House
11:30 pm | Rick and Morty | A Rickconvenient Mort
12:00 am | Rick and Morty | Rickdependence Spray
12:30 am | The Boondocks | Ballin'
 1:00 am | Mike Tyson Mysteries | Landon's End
 1:15 am | Mike Tyson Mysteries | The Stein Way
 1:30 am | American Dad! | Don't You Be My Neighbor
 2:00 am | American Dad! | Productive Panic
 2:30 am | American Dad! | Multiverse of American Dadness
 3:00 am | Rick and Morty | A Rickconvenient Mort
 3:30 am | Rick and Morty | Rickdependence Spray
 4:00 am | Teenage Euthanasia | The Bad Bang Theory
 4:30 am | Tuca & Bertie | Kyle
 5:00 am | Bob's Burgers | Bob Belcher and the Terrible, Horrible, No Good, Very Bad Kids
 5:30 am | Bob's Burgers | Diarrhea of a Poopy Kid