# 2024-03-27
 5:00 pm | Dexter's Laboratory | Scare Tactics/A Mom Cartoon/My Dad vs. Your Dad
 5:30 pm | Dexter's Laboratory | Beau Tie/Remember Me/Over-Labbing
 6:00 pm | Courage the Cowardly Dog | Fishy Business/Angry Nasty People
 6:30 pm | Courage the Cowardly Dog | Dome of Doom/Snowman's Revenge
 7:00 pm | King of the Hill | Rich Hank, Poor Hank
 7:30 pm | King of the Hill | Ceci N'est Pas Une King of the Hill
 8:00 pm | Bob's Burgers | Carpe Museum
 8:30 pm | Bob's Burgers | The Unnatural
 9:00 pm | Bob's Burgers | A River Runs Through Bob
 9:30 pm | American Dad! | Stretched Thin
10:00 pm | American Dad! | Stan Fixes a Shingle
10:30 pm | American Dad! | Cow I Met Your Moo-ther
11:00 pm | American Dad! | Better on Paper
11:30 pm | Rick and Morty | Ricksy Business
12:00 am | Rick and Morty | A Rickle in Time
12:30 am | The Boondocks | Smokin' With Cigarettes
 1:00 am | Mike Tyson Mysteries | Time to Fly
 1:15 am | Mike Tyson Mysteries | Make a Wish and Blow
 1:30 am | American Dad! | Stretched Thin
 2:00 am | American Dad! | Stan Fixes a Shingle
 2:30 am | American Dad! | Cow I Met Your Moo-ther
 3:00 am | Rick and Morty | Ricksy Business
 3:30 am | Rick and Morty | A Rickle in Time
 4:00 am | Teenage Euthanasia | It Happening!!! (Jellybean's Birthday)
 4:30 am | Tuca & Bertie | Salad Days
 5:00 am | Bob's Burgers | Carpe Museum
 5:30 am | Bob's Burgers | The Unnatural