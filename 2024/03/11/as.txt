# 2024-03-11
 5:00 pm | Dexter's Laboratory | SDRAWKCAB/The Continuum of Cartoon Fools/Sun, Surf and Science
 5:30 pm | Dexter's Laboratory | Dexter and Computress Get Mandark/Pain in the Mouth/Dexter vs Santa's Claws
 6:00 pm | Courage the Cowardly Dog | Family Business/1,000 Years of Courage
 6:30 pm | Courage the Cowardly Dog | Courage Meets the Mummy/Invisible Muriel
 7:00 pm | King of the Hill | A Man Without a Country Club
 7:30 pm | King of the Hill | Beer and Loathing
 8:00 pm | Bob's Burgers | Driving Big Dummy
 8:30 pm | Bob's Burgers | Seven-Tween Again
 9:00 pm | Bob's Burgers | Beach, Please
 9:30 pm | American Dad! | The Chilly Thrillies
10:00 pm | American Dad! | Dammmm, Stan!
10:30 pm | American Dad! | The Last Ride of the Dodge City Rambler
11:00 pm | American Dad! | 300
11:30 pm | Rick and Morty | Forgetting Sarick Mortshall
12:00 am | Rick and Morty | Rickmurai Jack
12:30 am | The Boondocks | Home Alone
 1:00 am | Mike Tyson Mysteries | Help a Brother Out
 1:15 am | Mike Tyson Mysteries | The Beginning
 1:30 am | American Dad! | The Chilly Thrillies
 2:00 am | American Dad! | Dammmm, Stan!
 2:30 am | American Dad! | The Last Ride of the Dodge City Rambler
 3:00 am | Rick and Morty | Forgetting Sarick Mortshall
 3:30 am | Rick and Morty | Rickmurai Jack
 4:00 am | Teenage Euthanasia | Suddenly Susan
 4:30 am | Tuca & Bertie | Vibe Check
 5:00 am | Bob's Burgers | Driving Big Dummy
 5:30 am | Bob's Burgers | Seven-Tween Again