# 2024-03-07
 6:00 am | Amazing World of Gumball | The Bumpkin
 6:14 am | Amazing World of Gumball | The Flakers
 6:28 am | The Looney Tunes Show | Working Duck
 6:56 am | Tom & Jerry | Sky's the Limit
 7:00 am | The Looney Tunes Show | French Fries
 7:29 am | Bugs Bunny Builders | Mail Whale
 7:43 am | Lellobee City Farm | Stargazing Lullaby
 7:46 am | Bugs Bunny Builders | Honey Bunny
 8:00 am | Meet the Batwheels | Bibi's Bad Day
 8:14 am | Meet the Batwheels | Bat-Surfing
 8:17 am | Meet the Batwheels | A Jet Out of Water
 8:31 am | We Baby Bears | Teddi Bear
 8:45 am | We Baby Bears | A Gross Worm
 9:00 am | Craig of the Creek | Power Punchers
 9:15 am | Craig of the Creek | Creek Cart Racers
 9:29 am | We Bare Bears | Fashion Bears
 9:43 am | We Bare Bears | The Island
 9:57 am | Lamput | Shape Shift
10:00 am | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
10:30 am | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!
11:00 am | Amazing World of Gumball | The Mustache
11:15 am | Amazing World of Gumball | The Date
11:30 am | Amazing World of Gumball | The Club
11:45 am | Amazing World of Gumball | The Wand
12:00 pm | Amazing World of Gumball | The Sidekick
12:15 pm | Amazing World of Gumball | The Photo
12:30 pm | Clarence | Clarence and Sumo's Rexcellent Adventure
12:45 pm | Clarence | Birthday
 1:00 pm | Clarence | Tree of Life
 1:15 pm | Clarence | Cloris
 1:30 pm | Craig of the Creek | Secret Book Club
 1:45 pm | Craig of the Creek | The Takeout Mission
 2:00 pm | Teen Titans Go! | Jam
 2:15 pm | Teen Titans Go! | DC
 2:30 pm | Teen Titans Go! | Breakfast
 2:45 pm | Teen Titans Go! | Captain Cool
 3:00 pm | Amazing World of Gumball | The Pony
 3:15 pm | Amazing World of Gumball | The Hero
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Princenappers
 3:45 pm | Amazing World of Gumball | The Dream
 4:00 pm | Regular Show | Married and Broke
 4:15 pm | Regular Show | I See Turtles
 4:30 pm | Regular Show | Format Wars II
 4:45 pm | Regular Show | Happy Birthday Song Contest