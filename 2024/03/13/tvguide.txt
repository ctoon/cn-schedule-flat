# 2024-03-13
 6:00 am | Amazing World of Gumball | -
 6:14 am | Amazing World of Gumball | -
 6:28 am | The Looney Tunes Show | -
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | -
 7:00 am | The Looney Tunes Show | -
 7:29 am | Bugs Bunny Builders | -
 7:43 am | Bugs Bunny Builders: Hard Hat Time | -
 7:46 am | Bugs Bunny Builders | -
 8:00 am | Meet the Batwheels | -
 8:14 am | Lellobee City Farm | -
 8:17 am | Meet the Batwheels | -
 8:31 am | We Baby Bears | -
 8:45 am | We Baby Bears | -
 9:00 am | Craig of the Creek | -
 9:15 am | Craig of the Creek | -
 9:29 am | We Bare Bears | -
 9:43 am | We Bare Bears | -
 9:57 am | Lamput | -
10:00 am | Scooby-Doo! and Guess Who? | -
10:30 am | Scooby-Doo! and Guess Who? | -
11:00 am | Amazing World of Gumball | -
11:15 am | Amazing World of Gumball | -
11:30 am | Amazing World of Gumball | -
11:45 am | Amazing World of Gumball | -
12:00 pm | Amazing World of Gumball | -
12:15 pm | Amazing World of Gumball | -
12:30 pm | Clarence | -
12:45 pm | Clarence | -
 1:00 pm | Clarence | -
 1:15 pm | Clarence | -
 1:30 pm | Craig of the Creek | -
 1:45 pm | Craig of the Creek | -
 2:00 pm | Teen Titans Go! | -
 2:15 pm | Teen Titans Go! | -
 2:30 pm | Teen Titans Go! | -
 2:45 pm | Teen Titans Go! | -
 3:00 pm | Amazing World of Gumball | -
 3:15 pm | Amazing World of Gumball | -
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | -
 3:45 pm | Amazing World of Gumball | -
 4:00 pm | Regular Show | -
 4:15 pm | Regular Show | -
 4:30 pm | Regular Show | -
 4:45 pm | Regular Show | -