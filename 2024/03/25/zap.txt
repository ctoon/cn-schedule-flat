# 2024-03-25
 6:00 am | Amazing World of Gumball | The Choices
 6:14 am | Amazing World of Gumball | The Code
 6:28 am | The Looney Tunes Show | Best Friends
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
 7:00 am | The Looney Tunes Show | Members Only
 7:30 am | Bugs Bunny Builders | The Easter Bunnies
 7:45 am | Bugs Bunny Builders | Speedy
 8:00 am | Meet the Batwheels | Bam's Upgrade
 8:14 am | Meet the Batwheels | Buff Tuff
 8:17 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:31 am | We Baby Bears | Grizz's Doc
 8:45 am | We Baby Bears | Triple T Tiger Lilies
 9:00 am | Craig of the Creek | Into the Overpast
 9:14 am | Craig of the Creek | The Time Capsule
 9:28 am | We Bare Bears | Crowbar Jones
 9:42 am | We Bare Bears | Kyle
 9:56 am | Tom & Jerry | What Goes Around Comes Around
10:00 am | Scooby-Doo! and Guess Who? | The 7th Inning Scare!
10:30 am | Scooby-Doo! and Guess Who? | The Dreaded Remake Of Jekyll & Hyde
11:00 am | Amazing World of Gumball | The Grades
11:15 am | Amazing World of Gumball | The Diet
11:30 am | Amazing World of Gumball | The Ex
11:45 am | Amazing World of Gumball | The Sorcerer
12:00 pm | Amazing World of Gumball | The Fuss
12:15 pm | Amazing World of Gumball | The Potato
12:30 pm | Clarence | Fun Dungeon Face Off
12:45 pm | Clarence | A Pretty Great Day With a Girl
 1:00 pm | Clarence | Money Broom Wizard
 1:15 pm | Clarence | Lost in the Supermarket
 1:30 pm | Craig of the Creek | Beyond the Rapids
 1:45 pm | Craig of the Creek | The Jinxening
 2:00 pm | Teen Titans Go! | Ship
 2:15 pm | Teen Titans Go! | The Teen Titans Go Easter Holiday Classic
 2:30 pm | Teen Titans Go! | Girls' Night Out
 2:45 pm | Teen Titans Go! | Five Bucks
 3:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
 3:30 pm | Amazing World of Gumball | The Slide
 3:45 pm | Amazing World of Gumball | The Copycats
 4:00 pm | Regular Show | Lame Lockdown
 4:15 pm | Regular Show | VIP Members Only
 4:30 pm | Regular Show | The Dome Experiment