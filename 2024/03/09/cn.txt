# 2024-03-09
 6:00 am | Regular Show | Dizzy
 6:15 am | Regular Show | My Mom
 6:30 am | Regular Show | High Score
 6:45 am | Regular Show | Rage Against the TV
 7:00 am | Amazing World of Gumball | The Sale
 7:15 am | Amazing World of Gumball | The Gift
 7:30 am | Amazing World of Gumball | The Parking
 7:45 am | Amazing World of Gumball | The Routine
 8:00 am | Amazing World of Gumball | The Upgrade
 8:15 am | Amazing World of Gumball | The Comic
 8:30 am | Amazing World of Gumball | The Romantic
 8:45 am | Amazing World of Gumball | The Uploads
 9:00 am | Tiny Toons Looniversity | Save The Loo Bru
 9:30 am | Teen Titans Go! | Catpin Freak
 9:45 am | Teen Titans Go! | Ship
10:00 am | Teen Titans Go! | Our House
10:15 am | Teen Titans Go! | Beard Hunter
10:30 am | Teen Titans Go! | New Chum
10:45 am | Teen Titans Go! | Negative Feels
11:00 am | Teen Titans Go! | Elasti-Bot
11:15 am | Teen Titans Go! | Catpin Freak
11:30 am | Craig of the Creek | War of the Pieces
12:00 pm | The Looney Tunes Show | The Float
12:30 pm | The Looney Tunes Show | The Shelf
 1:00 pm | Tiny Toons Looniversity | Give Pizza a Chance
 1:30 pm | Amazing World of Gumball | The Apprentice
 1:45 pm | Amazing World of Gumball | The Hug
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
 2:15 pm | Amazing World of Gumball | The Wicked
 2:30 pm | Amazing World of Gumball | The Traitor
 2:45 pm | Amazing World of Gumball | The Girlfriend
 3:00 pm | Amazing World of Gumball | The Shippening
 3:15 pm | Scooby-Doo! Abracadabra-Doo! | 
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
 6:30 am | Amazing World of Gumball | The Origins
 6:45 am | Amazing World of Gumball | The Origins Part 2