# 2024-03-26
 6:00 am | Amazing World of Gumball | The Outside
 6:15 am | Amazing World of Gumball | The Vase
 6:29 am | The Looney Tunes Show | Jailbird and Jailbunny
 6:57 am | Lamput | Diet Doc
 7:00 am | The Looney Tunes Show | Fish and Visitors
 7:30 am | Bugs Bunny Builders | Junior
 7:45 am | Bugs Bunny Builders | The Easter Bunnies
 8:00 am | Meet the Batwheels | Cave Sweet Cave
 8:14 am | Meet the Batwheels | Faster
 8:17 am | Meet the Batwheels | Scaredy-Bat
 8:31 am | We Baby Bears | After Bedtime
 8:45 am | We Baby Bears | The Parade
 9:00 am | Craig of the Creek | Creek Shorts
 9:15 am | Craig of the Creek | The Ground Is Lava!
 9:29 am | We Bare Bears | Citizen Tabes
 9:43 am | We Bare Bears | Dance Lessons
 9:57 am | Lamput | Alien
10:00 am | Scooby-Doo! and Guess Who? | The Last Inmate!
10:30 am | Scooby-Doo! and Guess Who? | Lost Soles Of Jungle River!
11:00 am | Amazing World of Gumball | The Best
11:15 am | Amazing World of Gumball | The Worst
11:30 am | Amazing World of Gumball | The Deal
11:45 am | Amazing World of Gumball | The Petals
12:00 pm | Amazing World of Gumball | The Cycle
12:15 pm | Amazing World of Gumball | The Stars
12:30 pm | Clarence | Clarence's Millions
12:45 pm | Clarence | Clarence Gets a Girlfriend
 1:00 pm | Clarence | Jeff's New Toy
 1:15 pm | Clarence | Dinner Party
 1:30 pm | Craig of the Creek | The Other Side: The Tournament
 2:00 pm | Teen Titans Go! | 50% Crew
 2:15 pm | Teen Titans Go! | Easter Creeps
 2:30 pm | Teen Titans Go! | Parasite
 2:45 pm | Teen Titans Go! | Starliar
 3:00 pm | Tiny Toons Looniversity | Extra, So Extra
 3:30 pm | Amazing World of Gumball | The Console
 3:45 pm | Amazing World of Gumball | The Catfish
 4:00 pm | Regular Show | Deez Keys
 4:15 pm | Regular Show | One Space Day at a Time
 4:30 pm | Regular Show | Cool Bro Bots
 4:45 pm | Regular Show | Welcome to Space