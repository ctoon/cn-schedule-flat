# 2024-03-26
 5:00 pm | Dexter's Laboratory | Oh Brother/Another Dad Cartoon/Bar Exam
 5:30 pm | Dexter's Laboratory | Jeepers, Creepers, Where Is Peepers/Go, Dexter Family! Go!
 6:00 pm | Courage the Cowardly Dog | So in Louvre Are We Two/Night of the Scarecrow
 6:30 pm | Courage the Cowardly Dog | Mondo Magic/Watch the Birdies
 7:00 pm | King of the Hill | Flirting With the Master
 7:30 pm | King of the Hill | After the Mold Rush
 8:00 pm | Bob's Burgers | Family Fracas
 8:30 pm | Bob's Burgers | The Kids Run the Restaurant
 9:00 pm | Bob's Burgers | Boyz 4 Now
 9:30 pm | American Dad! | Phantom of the Telethon
10:00 pm | American Dad! | Chimdale
10:30 pm | American Dad! | Stan Time
11:00 pm | American Dad! | Family Affair
11:30 pm | Rick and Morty | Something Ricked This Way Comes
12:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
12:30 am | The Boondocks | Stinkmeaner 3: The Hateocracy
 1:00 am | Mike Tyson Mysteries | Real Bitches of Newport Beach
 1:15 am | Mike Tyson Mysteries | The Pigeon Has Come Home to Roost
 1:30 am | American Dad! | Phantom of the Telethon
 2:00 am | American Dad! | Chimdale
 2:30 am | American Dad! | Stan Time
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 4:00 am | Teenage Euthanasia | A League of His Own
 4:30 am | Tuca & Bertie | Leaf Raking
 5:00 am | Bob's Burgers | Family Fracas
 5:30 am | Bob's Burgers | The Kids Run the Restaurant