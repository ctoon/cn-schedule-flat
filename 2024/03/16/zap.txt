# 2024-03-16
 6:00 am | Regular Show | This Is My Jam
 6:15 am | Regular Show | Muscle Woman
 6:30 am | Regular Show | Temp Check
 6:45 am | Regular Show | Jinx
 7:00 am | Amazing World of Gumball | The Night
 7:15 am | Amazing World of Gumball | The Misunderstandings
 7:30 am | Amazing World of Gumball | The Roots
 7:45 am | Amazing World of Gumball | The Blame
 8:00 am | Amazing World of Gumball | The Detective
 8:15 am | Amazing World of Gumball | The Fury
 8:30 am | Amazing World of Gumball | The Compilation
 8:45 am | Amazing World of Gumball | The Guy
 9:00 am | Tiny Toons Looniversity | Soufflé, Girl Hey
 9:30 am | Teen Titans Go! | 50% Crew
 9:45 am | Teen Titans Go! | Catpin Freak
10:00 am | Teen Titans Go! | Teen Titans Action
10:30 am | Teen Titans Go! | Booty Eggs
10:45 am | Teen Titans Go! | Plot Holes
11:00 am | Teen Titans Go! | Ship
11:15 am | Teen Titans Go! | 50% Crew
11:30 am | Craig of the Creek | A League of Maya's Own
11:45 am | Craig of the Creek | The Cheese Stands Alone
12:00 pm | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
12:30 pm | The Looney Tunes Show | We're In Big Truffle
 1:00 pm | Tiny Toons Looniversity | Prank You Very Much
 1:30 pm | Amazing World of Gumball | The Stories
 1:45 pm | Amazing World of Gumball | The Disaster
 2:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Chicken Of Doom
 2:15 pm | Amazing World of Gumball | The Re-Run
 2:30 pm | Amazing World of Gumball | The Boredom
 2:45 pm | Amazing World of Gumball | The Vision
 3:00 pm | Amazing World of Gumball | The Parents
 3:15 pm | LEGO Scooby-Doo!: Haunted Hollywood | 