# 2022-11-21
 9:00 pm | Bob's Burgers | The Quirk-Ducers
 9:30 pm | Bob's Burgers | Thanks-hoarding
10:00 pm | Rick and Morty | Pilot
10:30 pm | American Dad | Cock of the Sleepwalk
11:00 pm | American Dad | Introducing the Naughty Stewardesses
11:30 pm | Rick and Morty | Full Meta Jackrick
12:00 am | Mike Tyson Mysteries | The End
12:15 am | Mike Tyson Mysteries | Ultimate Judgment Day
12:30 am | Aqua Teen | Spacecadeuce
12:45 am | Aqua Teen | Mouth Quest
 1:00 am | The Boondocks | Stinkmeaner 3: The Hateocracy
 1:30 am | Rick and Morty | Full Meta Jackrick
 2:00 am | Futurama | Raging Bender
 2:30 am | Futurama | A Bicyclops Built for Two
 3:00 am | Your Pretty Face is Going to Hell | Krampus Nacht
 3:15 am | Your Pretty Face is Going to Hell | Heaven
 3:30 am | Infomercials | Too Many Cooks
 3:45 am | Infomercials | Alpha Chow & Goth Fitness
 4:00 am | Bob's Burgers | The Quirk-Ducers
 4:30 am | Bob's Burgers | Thanks-hoarding
 5:00 am | King Of the Hill | Suite Smells of Excess
 5:30 am | King Of the Hill | Bwah My Nose