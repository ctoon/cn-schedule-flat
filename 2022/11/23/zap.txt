# 2022-11-23
 6:00 am | Amazing World of Gumball | The Microwave; The Meddler
 6:30 am | Amazing World of Gumball | The Helmet; The Fight
 7:00 am | Cocomelon | YoYo's Arts and Crafts Time: Paper Airplanes
 7:30 am | Cocomelon | Tap Dancing Song
 8:00 am | Cocomelon | Yes Yes Vegetables
 8:30 am | Thomas & Friends: All Engines Go | Stink Monster
 9:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 9:30 am | Bugs Bunny Builders | Game Time
10:00 am | Meet the Batwheels | Buff's BFF
10:30 am | Thomas & Friends: All Engines Go | Overnight Stop
11:00 am | Puss in Boots | 
12:45 pm | Craig of the Creek | Creek Daycare
 1:00 pm | Teen Titans Go! | Thanksgiving
 1:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 2:00 pm | Amazing World of Gumball | The Gi; The Kiss
 2:30 pm | Amazing World of Gumball | The Party; The Refund
 3:00 pm | Amazing World of Gumball | The Robot; The Picnic
 3:30 pm | Amazing World of Gumball | The Goons; The Secret
 4:00 pm | Craig of the Creek | Sleepover at JP's
 4:30 pm | Craig of the Creek | The Cardboard Identity
 5:00 pm | Teen Titans Go! | Curse of the Booty Scooty
 5:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 6:00 pm | Early Man | 
 8:00 pm | Scooby-Doo and Guess Who? | The Feast of Dr. Frankenfooder!
 8:30 pm | Scooby-Doo and Guess Who? | A Fashion Nightmare!