# 2022-11-25
 6:00 pm | MOVIE | Charlie and the Chocolate Factory
 9:00 pm | Futurama | The Problem with Popplers
 9:30 pm | Futurama | Mother's Day
10:00 pm | American Dad | The Longest Distance Relationship
10:30 pm | American Dad | A Boy Named Michael
11:00 pm | American Dad | Roger Passes the Bar
11:30 pm | Rick and Morty | The Ricklantis Mixup
12:00 am | Genndy Tartakovsky's Primal | The Colossaeus
12:30 am | Genndy Tartakovsky's Primal | The Colossaeus, Part II
 1:00 am | Genndy Tartakovsky's Primal | The Colossaeus, Part III
 1:30 am | Genndy Tartakovsky's Primal | Echoes of Eternity
 2:00 am | Futurama | Anthology of Interest I
 2:30 am | Futurama | The Honking
 3:00 am | Futurama | The Problem with Popplers
 3:30 am | Futurama | Mother's Day
 4:00 am | King Of the Hill | The Trouble with Gribbles
 4:30 am | King Of the Hill | Hank's Back Story
 5:00 am | King Of the Hill | Bobby Goes Nuts
 5:30 am | King Of the Hill | Soldier of Misfortune