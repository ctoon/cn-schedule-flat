# 2022-11-25
 6:00 am | Amazing World of Gumball | The Laziest; The Ghost
 6:30 am | Amazing World of Gumball | The Mystery; The Prank
 7:00 am | Cocomelon | Basketball Song
 7:30 am | Cocomelon | Rain Rain Go Away Indoors Version
 8:00 am | Cocomelon | Getting Ready for School Song
 8:30 am | Thomas & Friends: All Engines Go | Stink Monster
 9:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 9:30 am | Bugs Bunny Builders | Big Feet
10:00 am | Meet the Batwheels | Zoomsday
10:30 am | Thomas & Friends: All Engines Go | Tri-and-a-Half-a-Lon
11:00 am | King Tweety | 
12:45 pm | Craig of the Creek | Ancients of the Creek
 1:00 pm | Teen Titans Go! | Black Friday
 1:30 pm | Teen Titans Go! | Had to Be There
 2:00 pm | Amazing World of Gumball | The Quest; The Spoon
 2:30 pm | Amazing World of Gumball | The Car; The Curse
 3:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:30 pm | Amazing World of Gumball | The Helmet; The Fight
 4:00 pm | Craig of the Creek | Craig of the Street
 4:30 pm | Craig of the Creek | Back to Cool
 5:00 pm | Teen Titans Go! | Black Friday
 5:30 pm | Teen Titans Go! | Record Book
 6:00 pm | Charlie and the Chocolate Factory | 
 8:30 pm | Scooby-Doo and Guess Who? | The Crown Jewel of Boxing!