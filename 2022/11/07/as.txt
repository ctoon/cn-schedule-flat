# 2022-11-07
 9:00 pm | Bob's Burgers | A River Runs Through Bob
 9:30 pm | Bob's Burgers | Seaplane!
10:00 pm | Rick and Morty | The Whirly Dirly Conspiracy
10:30 pm | American Dad | Old Stan in the Mountain
11:00 pm | American Dad | The Wrestler
11:30 pm | Rick and Morty | The Ricks Must Be Crazy
12:00 am | Mike Tyson Mysteries | The Missing Package
12:15 am | Mike Tyson Mysteries | Pits and Peaks
12:30 am | Aqua Teen | Rocket Horse and Jet Chicken
12:45 am | Aqua Teen | The Granite Family
 1:00 am | The Boondocks | The S Word
 1:30 am | Rick and Morty | The Ricks Must Be Crazy
 2:00 am | American Dad | Widow's Pique
 2:30 am | Futurama | Assie Come Home
 3:00 am | Your Pretty Face is Going to Hell | Welcome to Hell
 3:15 am | Your Pretty Face is Going to Hell | Bone Garden
 3:30 am | Infomercials | Food: The Source of Life
 3:45 am | Infomercials | Pervert Everything
 4:00 am | Bob's Burgers | A River Runs Through Bob
 4:30 am | Bob's Burgers | Seaplane!
 5:00 am | King Of the Hill | Rodeo Days
 5:30 am | King Of the Hill | Naked Ambition