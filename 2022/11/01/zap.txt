# 2022-11-01
 6:00 am | Amazing World of Gumball | The Roots
 6:30 am | Craig of the Creek | The Champion's Hike
 7:00 am | Cocomelon | Yes Yes Vegetables
 7:30 am | Cocomelon | Old MacDonald
 8:00 am | Cocomelon | Bingo Farm Version
 8:30 am | Thomas & Friends: All Engines Go | A New View for Thomas
 9:00 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
 9:30 am | Bugs Bunny Builders | Squirreled Away
 9:45 am | Bugs Bunny Builders | Big Feet
10:00 am | Meet the Batwheels | Sidekicked to the Curb
10:30 am | Thomas & Friends: All Engines Go | Skiff Sails Sodor
11:00 am | New Looney Tunes | Buddha Bugs; Now and Zen
11:30 am | New Looney Tunes | Sun Valley Freeze; The Inside Bugs
12:00 pm | Craig of the Creek | Creek Talent Extravaganza
12:30 pm | Craig of the Creek | Dodgy Decisions
 1:00 pm | Teen Titans Go! | Obinray
 1:30 pm | Teen Titans Go! | Wally T
 2:00 pm | Amazing World of Gumball | The Upgrade
 2:30 pm | Amazing World of Gumball | The Comic
 3:00 pm | Amazing World of Gumball | The Romantic
 3:30 pm | Amazing World of Gumball | The Uploads
 4:00 pm | Craig of the Creek | Lost & Found
 4:30 pm | Craig of the Creek | Grandma Smugglers
 5:00 pm | Teen Titans Go! | Grube's Fairytales
 5:30 pm | Teen Titans Go! | A Farce
 6:00 pm | Teen Titans Go! | Animals: It's Just a Word!
 6:30 pm | Teen Titans Go! | BBBDay!
 7:00 pm | Amazing World of Gumball | The Wicked
 7:30 pm | Amazing World of Gumball | The Origins
 8:00 pm | Regular Show | Cool Bikes
 8:30 pm | Regular Show | House Rules