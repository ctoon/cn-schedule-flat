# 2022-11-13
 6:00 am | Teen Titans Go! | Flashback
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 am | Teen Titans Go! | Quantum Fun; The Fight
 7:30 am | Teen Titans Go! | Thanksgetting; Thanksgiving
 8:00 am | Amazing World of Gumball | The Guy; The Console
 8:30 am | Amazing World of Gumball | The Boredom; The Outside
 9:00 am | We Baby Bears | Meat House; Snow Place Like Home
 9:30 am | We Baby Bears | Pirate Parrot Polly; Fiesta Day
10:00 am | Craig of the Creek | Craig of the Street; Galactic Goodbyes
10:30 am | Craig of the Creek | The Beyond the Overpass; Chef's Challenge
11:00 am | Craig of the Creek | Chrono Moss; In Search of Lore
11:30 am | Craig of the Creek | Adventures in Baby Casino; Hyde & Zeke
12:00 pm | Craig of the Creek | Lost & Found; My Stare Lady
12:30 pm | Craig of the Creek | Craig to the Future; Back to Cool
 1:00 pm | Early Man | 
 3:00 pm | Amazing World of Gumball | The Vision; The Copycats
 3:30 pm | Amazing World of Gumball | The Choices; The Catfish
 4:00 pm | Amazing World of Gumball | The Cycle; The Code
 4:30 pm | Amazing World of Gumball | The Stars; The Test
 5:00 pm | Amazing World of Gumball | The Slide; The; Box
 5:30 pm | Amazing World of Gumball | The Loophole; The; Matchmaker
 6:00 pm | The Polar Express | 
 8:00 pm | Bob's Burgers | A Few 'Gurt Men
 8:30 pm | Bob's Burgers | Ain't Miss Debatin'