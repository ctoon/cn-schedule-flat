# 2022-11-14
 6:00 am | Amazing World of Gumball | The Slip; The Mess
 6:30 am | Amazing World of Gumball | The Drama; The Possession
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Thomas & Friends: All Engines Go | A Brand New Track; Wide Delivery
 9:00 am | Mecha Builders | The Need for Feed; Thezipline Sisters of Treetop Woods
 9:30 am | Bugs Bunny Builders | Soup up; Tweety-Go-Round
10:00 am | Meet the Batwheels | Scaredy-Bat; Riddle Me This
10:30 am | Thomas & Friends: All Engines Go | Brand New Track; Music Is Everywhere
11:00 am | New Looney Tunes | Five Star Bugsyoga to Be Kidding Me; Grand Barbari-Yon; Thegiant Rabbit Hunters
11:30 am | New Looney Tunes | Rabbits of the Lost Arkappropriate Technology; Amusement Porknow You See Me; Now You Still See Me
12:00 pm | Craig of the Creek | Escape From Family Dinner; Bug City
12:30 pm | Craig of the Creek | Monster in the Garden Deep Creek Salvage
 1:00 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
 1:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 2:00 pm | Amazing World of Gumball | The Vegging; The; Awareness
 2:30 pm | Amazing World of Gumball | The Father; The; Parents
 3:00 pm | Amazing World of Gumball | The Cringe; The; Founder
 3:30 pm | Amazing World of Gumball | The Neighbor; The; Schooling
 4:00 pm | The Polar Express | 
 6:00 pm | Sherlock Gnomes | 
 8:00 pm | Regular Show | The Thanksgiving Special
 8:30 pm | Regular Show | Ace Balthazar Lives; Trailer Trashed