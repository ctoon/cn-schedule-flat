# 2022-11-18
 6:00 am | Amazing World of Gumball | The Candidate; The Spinoffs
 6:30 am | Amazing World of Gumball | The Anybody; The Transformation
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Thomas & Friends: All Engines Go | Brand New Track; Chasing Rainbows
 9:00 am | Mecha Builders | The Sent US a Pie / Dust In the Wind
 9:30 am | Bugs Bunny Builders | Game Time; Buzz In
10:00 am | Meet the Batwheels | Scaredy-Bat; Riddle Me This
10:30 am | Thomas & Friends: All Engines Go | Percy Disappears; Counting Cows
11:00 am | New Looney Tunes | Gorky Porkhard Hat Hare; Bigs Bunnywahder; Wahder; Everywhere
11:30 am | New Looney Tunes | Porky's Duck-Livery Servicewabbit Who Would Be King; The; Porky the Disorderlygame; Set; Wabbit
12:00 pm | Craig Of The Creek | Craig And The Kid's Table
12:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers; Turning the Tables
 1:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 2:00 pm | Amazing World of Gumball | The Wish; The; Web
 2:30 pm | Amazing World of Gumball | The Factory; The; Inquisition
 3:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 4:00 pm | Craig of the Creek | The Galactic Goodbyes; Shortcut
 4:30 pm | Craig of the Creek | Dog Decider; Dibs Court
 5:00 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
 5:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 6:00 pm | Early Man | 
 8:00 pm | Regular Show | K.I.L.I.T. Radio; Fool Me Twice
 8:30 pm | Regular Show | Carter and Briggs; Blind Trust