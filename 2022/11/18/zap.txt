# 2022-11-18
 6:00 am | Amazing World of Gumball | The Candidate
 6:30 am | Amazing World of Gumball | The Anybody
 7:00 am | Cocomelon | Old MacDonald
 7:30 am | Cocomelon | Yes Yes Vegetables
 8:00 am | Cocomelon | This Is the Way Bedtime Edition
 8:30 am | Thomas & Friends: All Engines Go | Brand New Track
 9:00 am | Mecha Builders | They Sent Us a Pie; Dust in the Wind
 9:30 am | Bugs Bunny Builders | Game Time
10:00 am | Meet the Batwheels | Scaredy-Bat
10:30 am | Thomas & Friends: All Engines Go | Percy Disappears
11:00 am | New Looney Tunes | Gorky Pork; Hard Hat Hare
11:30 am | New Looney Tunes | Porky's Duck-Livery Service; The Wabbit Who Would Be King
12:00 pm | Craig of the Creek | Craig and the Kid's Table
12:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 1:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 2:00 pm | Amazing World of Gumball | The Wish
 2:30 pm | Amazing World of Gumball | The Factory
 3:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 4:00 pm | Craig of the Creek | Galactic Goodbyes
 4:30 pm | Craig of the Creek | Dog Decider
 5:00 pm | Teen Titans Go! | Thanksgiving
 5:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 6:00 pm | Early Man | 
 8:00 pm | Regular Show | K.I.L.I.T. Radio
 8:30 pm | Regular Show | Carter and Briggs