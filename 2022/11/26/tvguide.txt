# 2022-11-26
 6:00 am | Amazing World of Gumball | The Lady; The; Candidate
 6:30 am | Amazing World of Gumball | The Anybody; The Transformation
 7:00 am | Amazing World of Gumball | The Shippening; The; Understanding
 7:30 am | Amazing World of Gumball | The Brain; The; Ad
 8:00 am | Amazing World of Gumball | The Stink; The; Slip
 8:30 am | Amazing World of Gumball | The Awareness; The; Drama
 9:00 am | We Baby Bears | Panda's Family; Dragon Pests
 9:30 am | We Baby Bears | Tale of Two Ice Bears; A; No Land; All Air!
10:00 am | Total DramaRama | Gobble Head; Royal Flush
10:30 am | Total DramaRama | Total Eclipse of the F.; Dream Worriers
11:00 am | Total DramaRama | Macarthur Park; Weekend at Buddy's
11:30 am | Total DramaRama | Carmageddon Last Mom Standing
12:00 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
12:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 1:00 pm | Teen Titans Go! | Them Soviet Boys; Teen Titans Roar!
 1:30 pm | Teen Titans Go! | Lil' Dimples; What's Opera; Titans?
 2:00 pm | Craig Of The Creek | Craig And The Kid's Table
 2:30 pm | Craig of the Creek | The Kelsey the Worthy; Time Capsule
 3:00 pm | Craig of the Creek | End Was Here; The; Beyond the Rapids
 3:30 pm | Charlie and the Chocolate Factory | 
 6:00 pm | Sherlock Gnomes | 
 7:45 pm | Amazing World of Gumball | The Advice
 8:00 pm | Regular Show | Bad Portrait; I Like You Hi
 8:30 pm | Regular Show | Postcard; The; Play Date