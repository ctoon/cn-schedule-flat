# 2022-11-02
 6:00 am | Amazing World of Gumball | The Bus
 6:30 am | Craig of the Creek | Opposite Day
 7:00 am | Cocomelon | Beach Song
 7:30 am | Cocomelon | Five Senses Song
 8:00 am | Cocomelon | Row Row Row Your Boat
 8:30 am | Thomas & Friends: All Engines Go | Carly's Screechy Squeak
 9:00 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 9:30 am | Bugs Bunny Builders | Beach Battle
 9:45 am | Bugs Bunny Builders | Squirreled Away
10:00 am | Meet the Batwheels | Bibi's Do-Over
10:30 am | Thomas & Friends: All Engines Go | A Quiet Delivery
11:00 am | New Looney Tunes | St. Bugs and the Dragon; Leaf It Alone
11:30 am | New Looney Tunes | The Bigfoot in Bed; World Wide Wabbit
12:00 pm | Craig of the Creek | Hyde & Zeke
12:30 pm | Craig of the Creek | The Anniversary Box
 1:00 pm | Teen Titans Go! | Island Adventures
 2:00 pm | Amazing World of Gumball | The Wicked
 2:30 pm | Amazing World of Gumball | The Origins
 3:00 pm | Amazing World of Gumball | The Love
 3:30 pm | Amazing World of Gumball | The Roots
 4:00 pm | Craig of the Creek | The Champion's Hike
 4:30 pm | Craig of the Creek | Bored Games
 5:00 pm | Teen Titans Go! | Black Friday
 5:30 pm | Teen Titans Go! | Two Parter: Part One
 6:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 6:30 pm | Teen Titans Go! | Bottle Episode
 7:00 pm | Amazing World of Gumball | The Awkwardness
 7:30 pm | Amazing World of Gumball | The Disaster
 8:00 pm | Regular Show | Rap It Up
 8:30 pm | Regular Show | Cruisin'