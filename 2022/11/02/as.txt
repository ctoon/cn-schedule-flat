# 2022-11-02
 9:00 pm | Bob's Burgers | Family Fracas
 9:30 pm | Bob's Burgers | The Kids Run the Restaurant
10:00 pm | American Dad | Stanny-Boy and Frantastic
10:30 pm | American Dad | A Pinata Named Desire
11:00 pm | American Dad | You Debt Your Life
11:30 pm | Rick and Morty | Auto Erotic Assimilation
12:00 am | Mike Tyson Mysteries | My Favorite Mystery
12:15 am | Mike Tyson Mysteries | Tyson of Arabia
12:30 am | Aqua Teen | Wi-tri
12:45 am | Aqua Teen | Big Bro
 1:00 am | The Boondocks | The Invasion of the Katrinas
 1:30 am | Rick and Morty | Auto Erotic Assimilation
 2:00 am | American Dad | The Unincludeds
 2:30 am | Futurama | Naturama
 3:00 am | Lucy: Daughter of the Devil | The Special Fathers vs. the Vampire Altar Boys
 3:15 am | Mary Shelley's Frankenhole | Gaston Leroux's Je Ne Sais Quoi!
 3:30 am | The Shivering Truth | Tow and Shell
 3:45 am | The Heart She Holler | Gamebored
 4:00 am | Bob's Burgers | Family Fracas
 4:30 am | Bob's Burgers | The Kids Run the Restaurant
 5:00 am | King Of the Hill | Wings of the Dope
 5:30 am | King Of the Hill | Take Me out of the Ball Game