# 2022-11-29
 6:00 am | Amazing World of Gumball | The Limit; The Game
 6:30 am | Amazing World of Gumball | The Promise; The Voice
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Thomas & Friends: All Engines Go | Lost and Found; Whistle Woes
 9:00 am | Mecha Builders | A Let's Get Rolling! Bit of a Stretch!
 9:30 am | Bugs Bunny Builders | Snow Cap; Rock On
10:00 am | Meet the Batwheels | Zoomsday Wheelin' and Dealin'; Rev and Let Rev
10:30 am | Thomas & Friends: All Engines Go | Thomas' Day off; Letting off Steam
11:00 am | Puss in Boots | 
12:45 pm | Craig of the Creek | The Children of the Dog
 1:00 pm | Teen Titans Go! | I Am Chair; P.P.
 1:30 pm | Teen Titans Go! | A Bumgorf; Little Help Please
 2:00 pm | Amazing World of Gumball | The Phone; The Job
 2:30 pm | Amazing World of Gumball | The Words; The Apology
 3:00 pm | Amazing World of Gumball | The Skull; Christmas
 3:30 pm | Amazing World of Gumball | The Watch; The Bet
 4:00 pm | Craig of the Creek | Trading Day Sleepover at Jp's
 4:30 pm | Craig of the Creek | Crisis at Elder Rock; Tea Timer's Ball
 5:00 pm | Teen Titans Go! | Bucket List; Lucky Stars
 5:30 pm | Teen Titans Go! | TV Knight 6; Various Modes of Transportation
 6:00 pm | Rise of the Guardians | 
 8:00 pm | Regular Show | Gold Watch; Maxin' and Relaxin'
 8:30 pm | Regular Show | Paint Job; New Bro on Campus