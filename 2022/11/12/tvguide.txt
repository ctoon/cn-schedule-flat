# 2022-11-12
 6:00 am | Amazing World of Gumball | The Girlfriend; The Wicked
 6:30 am | Amazing World of Gumball | The Origins; The Origins Part 2
 7:00 am | Amazing World Of Gumball | The Parasite; The Night
 7:30 am | Amazing World of Gumball | The Misunderstanding; The Love
 8:00 am | Amazing World of Gumball | The Roots; The; Nest
 8:30 am | Amazing World of Gumball | The Awkwardness; The; Blame
 9:00 am | We Baby Bears | Bubble Fields; Modern-Ish Stone Age Family
 9:30 am | We Baby Bears | Excalibear; Hashtag; Fan
10:00 am | Total DramaRama | Gnome More Mister Nice Guy; Pudding the Planet First
10:30 am | Total DramaRama | Look Who's Clocking; Supply Mom
11:00 am | Total DramaRama | Abaracaduncan; Shock & Aww
11:30 am | Total DramaRama | School District 9; Gumbearable
12:00 pm | Teen Titans Go! | Bro-Pocalypse; Tower Renovation
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 1:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 2:00 pm | Craig of the Creek | Fort Williams; Sparkle Cadet
 2:30 pm | Craig of the Creek | Kelsey the Elder Stink Bomb
 3:00 pm | Craig of the Creek | Sour Candy Trials; Summer Wish
 3:30 pm | Craig Of The Creek | Craig And The Kid's Table
 4:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 4:30 pm | Amazing World of Gumball | The Points; The; Detective
 5:00 pm | Amazing World of Gumball | The Bus; The; Fury
 5:30 pm | Amazing World of Gumball | The Stories; The Compilation
 6:00 pm | Early Man | 
 8:00 pm | Regular Show | Skips' Story
 8:30 pm | Regular Show | Bunch of Full Grown Geese; A; Skips' Stress