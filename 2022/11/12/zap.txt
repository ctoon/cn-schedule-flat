# 2022-11-12
 6:00 am | Amazing World of Gumball | The Wicked
 6:30 am | Amazing World of Gumball | The Origins
 7:00 am | Amazing World of Gumball | The Parasite
 7:30 am | Amazing World of Gumball | The Love
 8:00 am | Amazing World of Gumball | The Roots
 8:30 am | Amazing World of Gumball | The Awkwardness
 9:00 am | We Baby Bears | Bubble Fields
 9:30 am | We Baby Bears | Excalibear
10:00 am | Total DramaRama | Gnome More Mister Nice Guy
10:30 am | Total DramaRama | Look Who's Clocking
11:00 am | Total DramaRama | AbaracaDuncan
11:30 am | Total DramaRama | School District 9
12:00 pm | Teen Titans Go! | Bro-Pocalypse
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular, Part 1
 1:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 2:00 pm | Craig of the Creek | Fort Williams
 2:30 pm | Craig of the Creek | Kelsey the Elder
 3:00 pm | Craig of the Creek | Sour Candy Trials
 3:30 pm | Craig of the Creek | Craig and the Kid's Table
 4:00 pm | Amazing World of Gumball | The Disaster
 4:30 pm | Amazing World of Gumball | The Points
 5:00 pm | Amazing World of Gumball | The Bus
 5:30 pm | Amazing World of Gumball | The Compilation
 6:00 pm | Early Man | 
 8:00 pm | Regular Show | Skips' Story
 8:30 pm | Regular Show | A Bunch of Full Grown Geese