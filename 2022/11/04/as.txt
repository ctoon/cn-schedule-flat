# 2022-11-04
 9:00 pm | Futurama | Stench and Stenchibility
 9:30 pm | Futurama | Meanwhile
10:00 pm | American Dad | Jenny Fromdabloc
10:30 pm | American Dad | Home Wrecker
11:00 pm | American Dad | Flirting With Disaster
11:30 pm | Rick and Morty | Get Schwifty
12:00 am | Mr. Pickles | Mental Asylum
12:15 am | Mr. Pickles | Cops and Robbers
12:30 am | Mr. Pickles | Serial Killers
12:45 am | Mr. Pickles | Shövenpucker
 1:00 am | Mr. Pickles | Fish?
 1:15 am | Mr. Pickles | A.D.D.
 1:30 am | Mr. Pickles | My Dear Boy
 1:45 am | Mr. Pickles | Vegans
 2:00 am | Futurama | T.: The Terrestrial
 2:30 am | Futurama | Forty Percent Leadbelly
 3:00 am | Futurama | The Inhuman Torch
 3:30 am | Futurama | Saturday Morning Fun Pit
 4:00 am | King Of the Hill | Cotton's Plot
 4:30 am | King Of the Hill | Bills are Made to be Broken
 5:00 am | King Of the Hill | Little Horrors of Shop
 5:30 am | King Of the Hill | Aisle 8A