# 2022-11-05
 6:00 am | Amazing World of Gumball | The Man; The Bros
 6:30 am | Amazing World of Gumball | The Lie; The Pizza
 7:00 am | Amazing World of Gumball | The Butterfly; The Question
 7:30 am | Amazing World of Gumball | The Oracle; The Safety
 8:00 am | Amazing World of Gumball | The Friend; The Saint
 8:30 am | Amazing World of Gumball | The Spoiler; The Society
 9:00 am | We Baby Bears | The Witches; Magical Box
 9:30 am | We Baby Bears | Happy Bouncy Fun Town; Bears and the Beanstalk
10:00 am | Total DramaRama | Tooth About Zombies; The; Gobble Head
10:30 am | Total DramaRama | Driving Miss Crazy Lie-Ranosaurus Wrecked
11:00 am | Total DramaRama | Dissing Cousins
11:30 am | Total DramaRama | Encore'tney; Life of Pie
12:00 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
12:30 pm | Teen Titans Go! | Career Day; Brain Percentages
 1:00 pm | Teen Titans Go! | Bl4z3; TV Knight 2
 1:30 pm | Teen Titans Go! | The Academy; Lication
 2:00 pm | Craig of the Creek | Deep Creek Salvage; Memories of Bobby
 2:30 pm | Craig of the Creek | Dibs Court; Jacob of the Creek
 3:00 pm | Craig of the Creek | The Mystery Of The Timekeeper; The Great Fossil Rush
 3:30 pm | Craig of the Creek | Alone Quest; Return of the Honeysuckle Rangers
 4:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 4:30 pm | Amazing World of Gumball | The Egg; The Downer
 5:00 pm | Amazing World of Gumball | The Money; The Triangle
 5:30 pm | Amazing World of Gumball | The Return; The Check
 6:00 pm | Sherlock Gnomes | 
 7:45 pm | Amazing World of Gumball | The Nemesis; The Pest
 8:00 pm | Regular Show | Skips vs. Technology; Busted Cart
 8:30 pm | Regular Show | Eggscellent; Dead at Eight