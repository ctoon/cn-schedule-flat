# 2022-01-23
 6:00 am | Baby Looney Tunes | Spin Out;Taz's Fridge; Snow Day
 6:30 am | Baby Looney Tunes | The Harder They Fall; The Hare Hid Under The Fountain (song); Business As Usual
 7:00 am | Lucas the Spider | 
 7:30 am | Esme & Roy | Special Delivery; Fig Do It
 8:00 am | We Baby Bears | Bears in the Dark
 8:15 am | We Baby Bears | Big Trouble Little Babies
 8:30 am | We Baby Bears | The Magical Box
 8:45 am | We Baby Bears | Bears and the Beanstalk
 9:00 am | Teen Titans Go! | Movie Night
 9:15 am | Teen Titans Go! | Permanent Record
 9:30 am | Teen Titans Go! | BBRAE Pt 1
10:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
10:45 am | Teen Titans Go! | The Chaff
11:00 am | Amazing World of Gumball | The Catfish
11:10 am | Amazing World of Gumball | The Menu
11:20 am | Amazing World of Gumball | 
11:30 am | Amazing World of Gumball | The Cycle
11:45 am | Amazing World of Gumball | The Uncle
12:00 pm | Amazing World of Gumball | The Stars
12:15 pm | Amazing World of Gumball | The Heist
12:30 pm | Amazing World of Gumball | The Box
12:45 pm | Amazing World of Gumball | The Petals
 1:00 pm | Craig of the Creek | The Shortcut
 1:15 pm | Craig of the Creek | Fort Williams
 1:30 pm | Craig of the Creek | Deep Creek Salvage
 1:45 pm | Craig of the Creek | Kelsey the Elder
 2:00 pm | Craig of the Creek | Dibs Court
 2:15 pm | Craig of the Creek | Sour Candy Trials
 2:30 pm | Craig of the Creek | The Great Fossil Rush
 2:45 pm | Craig of the Creek | Council of the Creek
 3:00 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 3:10 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 3:20 pm | Teen Titans Go! | 
 3:30 pm | Teen Titans Go! | EEbows
 3:45 pm | Teen Titans Go! | Batman's Birthday Gift
 4:00 pm | Puss in Boots | 
 5:00 pm | Puss in Boots | 
 6:00 pm | Back to the Future Part II | 
 6:00 pm | Back to the Future Part II | 