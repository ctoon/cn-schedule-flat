# 2022-01-05
 6:00 am | Baby Looney Tunes | A Pastime For Petunia; Looney Tunes Zoo (song); Pouting Match
 6:30 am | Baby Looney Tunes | Wise Quacker; D-a-f-f-y (song); Yours, Mine...and Mine, Mine!
 7:00 am | Caillou | A Sweet & Sour Day; Caillou's Puppet Show; Caillou's Collection
 7:30 am | Caillou | Caillou's Cricket; Dog Dilemma; The Spider Issue
 8:00 am | Pocoyo | Horse
 8:07 am | Pocoyo | Double Trouble
 8:14 am | Pocoyo | Detective Pocoyo
 8:21 am | Pocoyo | Scooter Madness
 8:30 am | Pocoyo | Lost in Space
 8:40 am | Pocoyo | Boo!
 8:50 am | Pocoyo | Party Pooper
 9:00 am | Thomas & Friends: All Engines Go | Nia's Balloon Blunder
 9:15 am | Thomas & Friends: All Engines Go | Capture the Flag
 9:30 am | Thomas & Friends: All Engines Go | Mystery Boxcars
 9:45 am | Thomas & Friends: All Engines Go | Overnight Stop
10:00 am | Bing | Mural
10:10 am | Bing | Ice Lolly
10:20 am | Bing | Car Park
10:30 am | Pocoyo | Horse
10:40 am | Pocoyo | Double Trouble
10:50 am | Pocoyo | Detective Pocoyo
11:00 am | Pocoyo | Lost in Space
11:10 am | Pocoyo | Boo!
11:20 am | Pocoyo | Party Pooper
11:30 am | Mush-Mush and the Mushables | Bouncing Back
11:45 am | Mush-Mush and the Mushables | Good Vibrations
12:00 pm | Craig of the Creek | New Jersey
12:15 pm | Craig of the Creek | Body Swap
12:30 pm | Craig of the Creek | The Sunflower
12:45 pm | Craig of the Creek | Copycat Carter
 1:00 pm | Craig of the Creek | Craig World
 1:15 pm | Craig of the Creek | Brother Builder
 1:30 pm | Craig of the Creek | Jessica the Intern
 1:45 pm | Craig of the Creek | You're It
 2:00 pm | Amazing World of Gumball | The Petals
 2:15 pm | Amazing World of Gumball | The Cage
 2:30 pm | Amazing World of Gumball | The Nuisance
 2:45 pm | Amazing World of Gumball | The Rival
 3:00 pm | Amazing World of Gumball | The Best
 3:15 pm | Amazing World of Gumball | The One
 3:30 pm | Amazing World of Gumball | The Vegging
 3:45 pm | Amazing World of Gumball | The Stink
 4:00 pm | We Baby Bears | Modern-ish Stone Age Family
 4:15 pm | We Baby Bears | Excalibear
 4:30 pm | Craig of the Creek | Bug City
 4:45 pm | Craig of the Creek | Sparkle Cadet
 5:00 pm | Craig of the Creek | The Shortcut
 5:15 pm | Craig of the Creek | Council of the Creek
 5:30 pm | Craig of the Creek | Deep Creek Salvage
 5:40 pm | Craig of the Creek | Sour Candy Trials
 5:50 pm | Craig of the Creek | 
 6:00 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 6:30 pm | Teen Titans Go! | Legs; Breakfast Cheese
 7:00 pm | We Baby Bears | Boo-Dunnit
 7:15 pm | We Baby Bears | The Little Mer-Bear
 7:30 pm | Total DramaRama | Mad Math: Taffy Road
 7:45 pm | Total DramaRama | Bearly Edible