# 2022-01-02
 6:00 am | Baby Looney Tunes | Hair Cut-Ups; Does Your Tongue Hang Low; A Clean Sweep
 6:30 am | Baby Looney Tunes | Daffy Did It; Foghorn's Talkin' in the Barnyard; The Pig Who Cried Wolf
 7:00 am | Love Monster | Snowman Day
 7:10 am | Love Monster | Treasure Hunt Day
 7:20 am | Love Monster | Favourite Library Book Day
 7:30 am | Care Bears: Unlock the Magic | The Light in the Forest
 7:45 am | Care Bears: Unlock the Magic | Monsterplant
 8:00 am | We Baby Bears | The Magical Box
 8:15 am | We Baby Bears | Bears and the Beanstalk
 8:30 am | Craig of the Creek | Alternate Creekiverse
 8:45 am | Craig of the Creek | Snow Day
 9:00 am | Craig of the Creek | Winter Break
 9:30 am | Craig of the Creek | Snow Place Like Home
 9:45 am | Craig of the Creek | Breaking the Ice
10:00 am | We Baby Bears | Boo-Dunnit
10:15 am | We Baby Bears | The Little Mer-Bear
10:30 am | Amazing World of Gumball | The Hug
10:45 am | Amazing World of Gumball | The Upgrade
11:00 am | Amazing World of Gumball | The Sale
11:15 am | Amazing World of Gumball | The Comic
11:30 am | Amazing World of Gumball | The Routine
11:45 am | Amazing World of Gumball | The Romantic
12:00 pm | We Baby Bears | Modern-ish Stone Age Family
12:15 pm | We Baby Bears | Excalibear
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Teen Titans Go! | Brian; Nature
 1:30 pm | Teen Titans Go! | Salty Codgers; Knowledge
 1:45 pm | Teen Titans Go! | Control Freak
 2:00 pm | We Baby Bears | Meat House
 2:15 pm | We Baby Bears | Pirate Parrot Polly
 2:30 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 2:45 pm | Craig of the Creek | Capture the Flag Part 2: The King
 3:00 pm | Craig of the Creek | Capture the Flag Part 3: The Legend
 3:10 pm | Craig of the Creek | Capture the Flag Part 4: The Plan
 3:20 pm | Craig of the Creek | 
 3:30 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 4:00 pm | We Baby Bears | Baby Bear Genius
 4:15 pm | We Baby Bears | Bug City Sleuths
 4:30 pm | Amazing World of Gumball | The Parking
 4:45 pm | Amazing World of Gumball | The Uploads
 5:00 pm | Amazing World of Gumball | The Vegging
 5:15 pm | Amazing World of Gumball | The Brain
 5:30 pm | Amazing World of Gumball | The Ad
 5:45 pm | Amazing World of Gumball | The Master
 6:00 pm | Detective Pikachu | 