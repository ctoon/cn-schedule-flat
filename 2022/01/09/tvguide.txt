# 2022-01-09
 6:00 am | Baby Looney Tunes | Cool For Cats; Ten Loonies In A Bed (song); Time Out!
 6:30 am | Baby Looney Tunes | The Creature From the Chocolate Chip; The Looney Riddle; Card Bored Box
 7:00 am | Lucas the Spider | You Rang House Is Breathing Boingo
 7:30 am | Esme & Roy | The Greatest Show in Monsterdale; The Amazing Outdoors
 8:00 am | We Baby Bears | Hashtag; Fan; Snow Place Like Home
 8:30 am | We Baby Bears | Modern-Ish Stone Age Family; Excalibear
 9:00 am | Teen Titans Go! | Finally A Lesson; Wally T
 9:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes; Arms Race With Legs
10:00 am | Teen Titans Go! | History Lesson; Obinray
10:30 am | Teen Titans Go! | The Art of Ninjutsu; Batman vs. Teen Titans: Dark Injustice
11:00 am | Amazing World of Gumball | The Signal; The Misunderstandings
11:30 am | Amazing World of Gumball | The Girlfriend; The Roots
12:00 pm | Amazing World of Gumball | The Parasite; The Blame
12:30 pm | Amazing World of Gumball | The Love; The Slap
 1:00 pm | Craig of the Creek | Sunday Clothes; You're It
 1:30 pm | Craig of the Creek | Jessica Goes to the Creek; Escape From Family Dinner
 2:00 pm | Craig of the Creek | The Final Book; Monster in the Garden
 2:30 pm | Craig of the Creek | The Curse; Too Many Treasures
 3:00 pm | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
 3:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 4:00 pm | Sherlock Gnomes | 
 6:00 pm | Daddy Day Care | 