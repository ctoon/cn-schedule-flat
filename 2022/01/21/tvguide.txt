# 2022-01-21
 6:00 am | Baby Looney Tunes | Cat-taz-trophy; If You're Looney (song); Duck! Monster! Duck!
 6:30 am | Baby Looney Tunes | The Brave Little Tweety; Foghorn's Talkin' In The Barnyard (song); Puddle Olympics
 7:00 am | Caillou | Sharing the Rocket ship; Coach Granpa; The safety Helper
 7:30 am | Caillou | Rushing the Raspberry; Wait to Skate; Caillou's Hiccups
 8:00 am | Pocoyo | Garden; The; Pato's Shower; Pocoyolympics; Picture This
 8:30 am | Pocoyo | Whale's Birthday Pocoyo's Little Friend #146 Color My World
 9:00 am | Thomas & Friends: All Engines Go | Can-Do Submarine Crew; Roller Coasting
 9:30 am | Thomas & Friends: All Engines Go | Mystery Boxcars; Capture the Flag
10:00 am | Bing | Hippoty Hoppity Voosh; Music; Bubbles
10:30 am | Pocoyo | Garden; The; Pato's Shower; Pocoyolympics
11:00 am | Pocoyo | Whale's Birthday Pocoyo's Little Friend #146 Color My World
11:30 am | Lucas the Spider | Do You Speak Dancenew Spider in Town; Adollhouse Mates
12:00 pm | Craig of the Creek | Ancients of the Creek; Secret in a Bottle
12:30 pm | Craig of the Creek | The Cardboard Identity; Trading Day
 1:00 pm | Craig of the Creek | Crisis at Elder Rock; Tea Timer's Ball
 1:30 pm | Craig of the Creek | Mortimor to the Rescue Kelsey the Worthy
 2:00 pm | Amazing World of Gumball | The End; The DVD
 2:30 pm | Amazing World of Gumball | The Knights; The Colossus
 3:00 pm | Amazing World of Gumball | The Fridge; The Remote
 3:30 pm | Amazing World of Gumball | The Flower; The Banana
 4:00 pm | We Baby Bears | Little Mer-Bear; The; Modern-Ish Stone Age Family
 4:30 pm | Craig of the Creek | Capture the Flag pt1:The Candy;Capture the Flag pt2:The King;Capture the Flag pt3:The Legend;Capture the Flag pt4:The Plan;Cap
 6:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 6:30 pm | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
 7:00 pm | We Baby Bears | Bears in the Dark; Big Trouble Little Babies
 7:30 pm | Total DramaRama | Pinata Regatta; For a Few Duncans More