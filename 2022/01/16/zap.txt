# 2022-01-16
 6:00 am | Baby Looney Tunes | A Turtle Named Mrytle; Oh My Darling Coyote (song); There's Nothing Like A Good Book
 6:30 am | Baby Looney Tunes | For Whom The Toll Calls; John Jacob Jongle Elmer Fudd ; Cereal Boxing
 7:00 am | Lucas the Spider | 
 7:30 am | Esme & Roy | Icky Pop; Dream Submarine
 8:00 am | We Baby Bears | Fiesta Day
 8:15 am | We Baby Bears | Baba Yaga House
 8:30 am | We Baby Bears | Baby Bear Genius
 8:45 am | We Baby Bears | Bug City Sleuths
 9:00 am | Teen Titans Go! | Who's Laughing Now
 9:15 am | Teen Titans Go! | Shrimps and Prime Rib
 9:30 am | Teen Titans Go! | Oregon Trail
 9:45 am | Teen Titans Go! | Booby Trap House
10:00 am | Teen Titans Go! | Snuggle Time
10:15 am | Teen Titans Go! | Fish Water
10:30 am | Teen Titans Go! | Oh Yeah!
10:40 am | Teen Titans Go! | TV Knight
10:50 am | Teen Titans Go! | Squash & Stretch
11:00 am | Amazing World of Gumball | The Choices
11:15 am | Amazing World of Gumball | The Fuss
11:30 am | Amazing World of Gumball | The Code
11:45 am | Amazing World of Gumball | The News
12:00 pm | Amazing World of Gumball | The Test
12:10 pm | Amazing World of Gumball | 
12:20 pm | Amazing World of Gumball | The Vase
12:30 pm | Amazing World of Gumball | The Slide
12:45 pm | Amazing World of Gumball | The Ollie
 1:00 pm | Craig of the Creek | Under the Overpass
 1:15 pm | Craig of the Creek | Doorway to Helen
 1:30 pm | Craig of the Creek | The Climb
 1:45 pm | Craig of the Creek | Power Punchers
 2:00 pm | Craig of the Creek | Big Pinchy
 2:15 pm | Craig of the Creek | Creek Cart Racers
 2:30 pm | Craig of the Creek | The Kid From 3030
 2:45 pm | Craig of the Creek | Secret Book Club
 3:00 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 3:10 pm | Teen Titans Go! | 
 3:20 pm | Teen Titans Go! | Little Elvis
 3:30 pm | Shazam! | 
 6:00 pm | Back to the Future | 
 6:00 pm | Back to the Future | 