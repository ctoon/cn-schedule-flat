# 2022-01-31
 8:00 pm | King Of the Hill | Talking Shop
 8:30 pm | King Of the Hill | A Rover Runs Through It
 9:00 pm | Bob's Burgers | Mission Impos-slug-ble
 9:30 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
10:00 pm | Futurama | Leela's Homeworld
10:30 pm | Futurama | Where the Buggalo Roam
11:00 pm | American Dad | Not Particularly Desperate Housewives
11:30 pm | American Dad | Rough Trade
12:00 am | Rick and Morty | Childrick of Mort
12:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
12:45 am | Smiling Friends | Desmond's Big Day Out
 1:00 am | Aqua Teen | Big Bro
 1:15 am | Aqua Teen | Chicken and Beans
 1:30 am | Futurama | Leela's Homeworld
 2:00 am | Futurama | Where the Buggalo Roam
 2:30 am | American Dad | Not Particularly Desperate Housewives
 3:00 am | Rick and Morty | Childrick of Mort
 3:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
 3:45 am | Smiling Friends | Desmond's Big Day Out
 4:00 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
 4:15 am | Hot Streets | Blood Barn
 4:30 am | Bob's Burgers | Mission Impos-slug-ble
 5:00 am | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 5:30 am | King Of the Hill | Talking Shop