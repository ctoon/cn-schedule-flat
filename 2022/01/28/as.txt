# 2022-01-28
 8:00 pm | King Of the Hill | Daletech
 8:30 pm | King Of the Hill | How I Learned To Stop Worrying and Love the Alamo
 9:00 pm | Bob's Burgers | Mo Mommy Mo Problems
 9:30 pm | Futurama | Anthology of Interest II
10:00 pm | Futurama | Love and Rocket
10:30 pm | American Dad | Con Heir
11:00 pm | American Dad | Stan of Arabia Part 1
11:30 pm | American Dad | Stan of Arabia Part 2
12:00 am | Tuca & Bertie | Bird Mechanics
12:30 am | Tuca & Bertie | Planteau
 1:00 am | Tuca & Bertie | Kyle
 1:30 am | Tuca & Bertie | Nighttime Friend
 2:00 am | Tuca & Bertie | Vibe Check
 2:30 am | Tuca & Bertie | The Moss
 3:00 am | Futurama | Anthology of Interest II
 3:30 am | Futurama | Love and Rocket
 4:00 am | King Of the Hill | Daletech
 4:30 am | King Of the Hill | How I Learned To Stop Worrying and Love the Alamo
 5:00 am | King Of the Hill | The Petriot Act
 5:30 am | Bob's Burgers | Mo Mommy Mo Problems