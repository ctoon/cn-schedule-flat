# 2022-01-22
 6:00 am | Baby Looney Tunes | A Lot Like Lola; Baby Elmer Had a Friend; Mother's Day Madness
 6:30 am | Baby Looney Tunes | Takers Keepersd-a-F-F-y to Tell the Tooth
 7:00 am | Love Monster | Taking Turns Day; Favorite Hat Day; Snowman Day
 7:30 am | Care Bears: Unlock the Magic | The Water; Water Everywhere; Ultimate Bad Seed
 8:00 am | We Baby Bears | Hashtag; Fan; Snow Place Like Home
 8:30 am | We Baby Bears | Bears in the Dark; Big Trouble Little Babies
 9:00 am | Teen Titans Go! | BBSFBDAY;Inner Beauty of a Cactus
 9:30 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
10:00 am | Total DramaRama | Waterhose-Five; Dial B for Birder
10:30 am | Total DramaRama | A Cody the Barbarian; Hole Lot of Trouble
11:00 am | Total DramaRama | A Tp2: Judgement Bidet; Tall Tale
11:30 am | We Baby Bears | Bears in the Dark; Big Trouble Little Babies
12:00 pm | Teen Titans Go! | The Spice Game; Hey You, Don't Forget About Me in Your Memory
12:30 pm | Teen Titans Go! | I'm the Sauce; The Fourth Wall
 1:00 pm | Teen Titans Go! | 20%; Accept the Next Proposition You Hear
 1:30 pm | We Baby Bears | Fiesta Day; Baba Yaga House
 2:00 pm | Craig of the Creek | Beyond the Overpass; Sink or Swim Team
 2:30 pm | Craig Of The Creek | Sparkle Cadet; Jessica's Trail
 3:00 pm | Craig of the Creek | Summer Wish; Stink Bomb
 3:30 pm | We Baby Bears | Bears in the Dark; Big Trouble Little Babies
 4:00 pm | Amazing World of Gumball | The Cycle; The Console
 4:30 pm | Amazing World of Gumball | The Stars; The Outside
 5:00 pm | Amazing World of Gumball | The Copycats; The Box
 5:30 pm | Amazing World of Gumball | The Catfish; The Matchmaker
 6:00 pm | Puss in Boots | Darwin's Yearbook - Sarah