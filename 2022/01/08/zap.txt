# 2022-01-08
 6:00 am | Baby Looney Tunes | Melissa the Hero; My Bunny Lies Over the Ocean; The Trouble With Larry
 6:30 am | Baby Looney Tunes | The Littlest Tweety; Over in The Burrow (song); In Bugs We Trust
 7:00 am | Love Monster | Choose One Thing Day
 7:10 am | Love Monster | Topsy Turvy Day
 7:20 am | Love Monster | Happy to Help Day
 7:30 am | Care Bears: Unlock the Magic | Neon Beach Party
 8:00 am | We Baby Bears | The Magical Box
 8:15 am | We Baby Bears | Bears and the Beanstalk
 8:30 am | We Baby Bears | Hashtag Number One Fan
 8:45 am | We Baby Bears | Snow Place Like Home
 9:00 am | Teen Titans Go! | Secret Garden
 9:15 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:30 am | Teen Titans Go! | Bottle Episode
 9:45 am | Teen Titans Go! | Pyramid Scheme
10:00 am | Total DramaRama | Gumbearable
10:15 am | Total DramaRama | Weekend at Buddy's
10:30 am | Total DramaRama | Whack Mirror
10:40 am | Total DramaRama | MacArthur Park
10:50 am | Total DramaRama | Mutt Ado About Owen
11:00 am | Total DramaRama | Broken Back Kotter
11:15 am | Total DramaRama | Last Mom Standing
11:30 am | We Baby Bears | Hashtag Number One Fan
11:45 am | We Baby Bears | Snow Place Like Home
12:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 1:00 pm | Teen Titans Go! | Video Game References; Cool School
 1:15 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 1:30 pm | We Baby Bears | Boo-Dunnit
 1:45 pm | We Baby Bears | The Little Mer-Bear
 2:00 pm | Craig of the Creek | Fan or Foe
 2:15 pm | Craig of the Creek | Body Swap
 2:30 pm | Craig of the Creek | New Jersey
 2:45 pm | Craig of the Creek | Copycat Carter
 3:00 pm | Craig of the Creek | The Sunflower
 3:15 pm | Craig of the Creek | Brother Builder
 3:30 pm | We Baby Bears | Hashtag Number One Fan
 3:40 pm | We Baby Bears | Snow Place Like Home
 3:50 pm | We Baby Bears | 
 4:00 pm | Amazing World of Gumball | The Nest
 4:15 pm | Amazing World of Gumball | The Points
 4:30 pm | Amazing World of Gumball | The Traitor
 4:45 pm | Amazing World of Gumball | The Bus
 5:00 pm | Amazing World of Gumball | The Advice
 5:10 pm | Amazing World of Gumball | The Night
 5:20 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 5:30 pm | Amazing World of Gumball | The Vase
 5:45 pm | Amazing World of Gumball | The Ollie
 6:00 pm | Sherlock Gnomes | 
 7:00 pm | Sherlock Gnomes | 