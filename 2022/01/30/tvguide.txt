# 2022-01-30
 6:00 am | Baby Looney Tunes | New Cat in Town; Baby Bunny; The Magic of Spring
 6:30 am | Lucas the Spider | Boot Castleavocado's Long Time Outtanks Giving
 7:00 am | Esme & Roy | Top Dog; Picture Day Predicament
 7:30 am | The Not Too Late Show with Elmo | Batman/Pentatonix
 8:00 am | We Baby Bears | Triple T Tigers; Panda's Family
 8:30 am | We Baby Bears | The Boo-Dunnit; Little Mer-Bear
 9:00 am | Teen Titans Go! | Titan Saving Time; Orangins
 9:30 am | Teen Titans Go! | Hand Zombie; Jinxed
10:00 am | Teen Titans Go! | Master Detective Hot Salad Water
10:30 am | Teen Titans Go! | The Avogodo; Brain Percentages
11:00 am | Amazing World of Gumball | The Deal; The One
11:30 am | Amazing World of Gumball | The Line; The; Vegging
12:00 pm | Amazing World of Gumball | The Singing; The; Father
12:30 pm | Amazing World of Gumball | The Puppets; The Cringe
 1:00 pm | Craig of the Creek | Tea Timer's Ball; Secret in a Bottle
 1:30 pm | Craig of the Creek | The Cardboard Identity; Trading Day
 2:00 pm | Craig of the Creek | The Quick Name; The Chef's Challenge
 2:30 pm | Craig of the Creek | Mortimor to the Rescue Kelsey the Worthy
 3:00 pm | Teen Titans Go! | Doomsday Preppers What a Boy Wonders
 3:30 pm | Teen Titans Go! | Jam; Fat Cats
 4:00 pm | Yogi Bear | 
 6:00 pm | Back to the Future Part III | 