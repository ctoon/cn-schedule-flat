# 2022-01-30
 6:00 am | Ben 10 | The Krakken
 6:30 am | Ben 10 | Permanent Retirement
 7:00 am | Teen Titans | Divide and Conquer
 7:30 am | Teen Titans | Forces of Nature
 8:00 am | Teen Titans Go! | Titan Saving Time
 8:15 am | Teen Titans Go! | Orangins
 8:30 am | Teen Titans Go! | Hand Zombie
 8:40 am | Teen Titans Go! | Jinxed
 8:50 am | Teen Titans Go! | A Farce
 9:00 am | We Baby Bears | Triple T Tigers
 9:15 am | We Baby Bears | Panda's Family
 9:30 am | We Baby Bears | Boo-Dunnit
 9:45 am | We Baby Bears | The Little Mer-Bear
10:00 am | Total DramaRama | CodE.T.
10:15 am | Total DramaRama | Quiche It Goodbye
10:30 am | Total DramaRama | Ice Guys Finish Last
10:40 am | Total DramaRama | Trousering Inferno
10:50 am | Total DramaRama | 
11:00 am | Amazing World of Gumball | The Deal
11:15 am | Amazing World of Gumball | The One
11:30 am | Amazing World of Gumball | The Line
11:45 am | Amazing World of Gumball | The Vegging
12:00 pm | Amazing World of Gumball | The Singing
12:15 pm | Amazing World of Gumball | The Father
12:30 pm | Amazing World of Gumball | The Puppets
12:40 pm | Amazing World of Gumball | The Cringe
12:50 pm | Amazing World of Gumball | The Sucker
 1:00 pm | Craig of the Creek | Tea Timer's Ball
 1:15 pm | Craig of the Creek | Secret in a Bottle
 1:30 pm | Craig of the Creek | The Cardboard Identity
 1:45 pm | Craig of the Creek | Trading Day
 2:00 pm | Craig of the Creek | The Quick Name
 2:15 pm | Craig of the Creek | The Chef's Challenge
 2:30 pm | Craig of the Creek | Mortimor to the Rescue
 2:45 pm | Craig of the Creek | Kelsey the Worthy
 3:00 pm | Teen Titans Go! | What a Boy Wonders
 3:10 pm | Teen Titans Go! | Doomsday Preppers
 3:20 pm | Teen Titans Go! | 
 3:30 pm | Teen Titans Go! | Fat Cats
 3:45 pm | Teen Titans Go! | Jam
 4:00 pm | Yogi Bear | 
 5:00 pm | Yogi Bear | 
 6:00 pm | Back to the Future Part III | 
 6:00 pm | Back to the Future Part III | 