# 2022-02-22
 6:00 am | Care Bears: Unlock the Magic | Hide and Sneak
 6:15 am | Care Bears: Unlock the Magic | Monsterplant
 6:30 am | Esme & Roy | Achoo, Matey; Beach Bummer
 7:00 am | Cocomelon | Breakfast Song
 7:04 am | Cocomelon | Yes Yes Playground Song
 7:08 am | Cocomelon | The Muffin Man
 7:12 am | Cocomelon | Five Little Speckled Frogs
 7:16 am | Cocomelon | Yes Yes Bedtime Song
 7:20 am | Cocomelon | The Colors Song With Popsicles
 7:24 am | Cocomelon | Opposites Song
 7:30 am | Pocoyo | Farewell Friends
 7:36 am | Pocoyo | Double Trouble
 7:42 am | Pocoyo | Horse
 7:50 am | Love Monster | Carrot Day
 8:00 am | Love Monster | Choose One Thing Day
 8:10 am | Bing | Ducks
 8:20 am | Bing | Car Park
 8:30 am | Caillou | Blueberry Point; Class Pet; In the Garden
 9:00 am | Thomas & Friends: All Engines Go | Counting Cows
 9:10 am | Thomas & Friends: All Engines Go | Red Light, Green Light
 9:20 am | Thomas & Friends: All Engines Go | A Wide Delivery
 9:30 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
 9:45 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell
10:00 am | Mush-Mush and the Mushables | Mush-Mush the Predictor
10:15 am | Mush-Mush and the Mushables | The Art of Friendship
10:30 am | Care Bears: Unlock the Magic | The Return of the Gnomes
10:45 am | Care Bears: Unlock the Magic | Where Oh Where Has Our Funshine Gone?
11:00 am | Craig of the Creek | Beyond the Overpass
11:15 am | Craig of the Creek | Sink or Swim Team
11:30 am | Craig of the Creek | The Quick Name
11:45 am | Craig of the Creek | The Chef's Challenge
12:00 pm | Craig of the Creek | The Sparkle Solution
12:15 pm | Craig of the Creek | Better Than You
12:30 pm | Craig of the Creek | The Dream Team
12:45 pm | Craig of the Creek | Fire & Ice
 1:00 pm | Minions | 
 3:00 pm | Chowder | The Burple Nurple Stand; Schnitzel Makes a Deposit
 3:30 pm | Chowder | The Froggy Apple Crumble Thumpkin; Chowder's Girlfriend
 4:00 pm | We Baby Bears | Fiesta Day
 4:15 pm | We Baby Bears | Baba Yaga House
 4:30 pm | Amazing World of Gumball | The Responsible
 4:45 pm | Amazing World of Gumball | The DVD
 5:00 pm | Amazing World of Gumball | The Third
 5:15 pm | Amazing World of Gumball | The Debt
 5:30 pm | Amazing World of Gumball | The End
 5:45 pm | Amazing World of Gumball | The Dress
 6:00 pm | Amazing World of Gumball | The Quest
 6:15 pm | Amazing World of Gumball | The Spoon
 6:30 pm | Amazing World of Gumball | The Pressure
 6:45 pm | Amazing World of Gumball | The Painting
 7:00 pm | Amazing World of Gumball | The Laziest
 7:15 pm | Amazing World of Gumball | The Ghost
 7:30 pm | Amazing World of Gumball | The Mystery
 7:45 pm | Amazing World of Gumball | The Prank