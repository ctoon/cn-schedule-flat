# 2022-02-05
 8:00 pm | King Of the Hill | Redcorn Gambles With His Future
 8:30 pm | King Of the Hill | Smoking and the Bandit
 9:00 pm | Futurama | Put Your Head on My Shoulder
 9:30 pm | Futurama | The Lesser of Two Evils
10:00 pm | Rick and Morty | A Rickconvenient Mort
10:30 pm | Rick and Morty | Rickdependence Spray
11:00 pm | American Dad | Lincoln Lover
11:30 pm | American Dad | Dungeons and Wagons
12:00 am | Blade Runner: Black Lotus | Time to Die
12:30 am | Shenmue: The Animation | Thunderclap
 1:00 am | Assassination Classroom | Reaper Time, Part 1
 1:30 am | Made In Abyss | The Edge of the Abyss
 2:00 am | One Piece | The Battle Is On! Show Them What You Got from Training!
 2:30 am | One Piece | Everyone Together! Luffy, Setting Out for the New World!
 3:00 am | Naruto:Shippuden | Kakashi vs. Obito
 3:30 am | Cowboy Bebop | Jupiter Jazz Part 2
 4:00 am | King Of the Hill | Redcorn Gambles With His Future
 4:30 am | King Of the Hill | Smoking and the Bandit
 5:00 am | King Of the Hill | Gone With the Windstorm
 5:30 am | King Of the Hill | Bobby On Track