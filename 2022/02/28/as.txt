# 2022-02-28
 8:00 pm | King Of the Hill | The Minh Who Knew Too Much
 8:30 pm | King Of the Hill | Dream Weaver
 9:00 pm | Bob's Burgers | Topsy
 9:30 pm | Bob's Burgers | Two for Tina
10:00 pm | Futurama | Bender's Game Part 3
10:30 pm | Futurama | Bender's Game Part 4
11:00 pm | American Dad | Spring Break-Up
11:30 pm | American Dad | 1600 Candles
12:00 am | Rick and Morty | Meeseeks and Destroy
12:30 am | Robot Chicken | May Cause Bubbles Where You Don't Want 'Em			
12:45 am | Robot Chicken | May Cause Light Cannibalism			
 1:00 am | Squidbillies | One Man Banned
 1:15 am | Squidbillies | Let 'er R.I.P.
 1:30 am | Futurama | Bender's Game Part 3
 2:00 am | Futurama | Bender's Game Part 4
 2:30 am | American Dad | Spring Break-Up
 3:00 am | Rick and Morty | Meeseeks and Destroy
 3:30 am | Robot Chicken | May Cause Bubbles Where You Don't Want 'Em			
 3:45 am | Robot Chicken | May Cause Light Cannibalism			
 4:00 am | Black Dynamite | "Warriors Come Out" or "Mean Queens of Halloween"
 4:30 am | Bob's Burgers | Topsy
 5:00 am | Bob's Burgers | Two for Tina
 5:30 am | King Of the Hill | The Minh Who Knew Too Much