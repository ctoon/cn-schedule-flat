# 2022-02-11
 8:00 pm | King Of the Hill | Business Is Picking Up
 8:30 pm | King Of the Hill | The Year of Washing Dangerously
 9:00 pm | Bob's Burgers | Beefsquatch
 9:30 pm | Futurama | Bendless Love
10:00 pm | American Dad | One Little Word
10:30 pm | American Dad | Wife Insurance
11:00 pm | American Dad | May the Best Stan Win
11:30 pm | American Dad | My Affair Lady
12:00 am | Lazor Wulf | Still Dying to Eat
12:15 am | Lazor Wulf | If That Was Tomorrow. This Is Today.
12:30 am | Lazor Wulf | Unoccupied Lane
12:45 am | Lazor Wulf | The End Is High
 1:00 am | Lazor Wulf | We Good?
 1:15 am | Lazor Wulf | Where You Stay?
 1:30 am | Lazor Wulf | Prolly for the Better?
 1:45 am | Lazor Wulf | Keep Going
 2:00 am | Lazor Wulf | They Knew
 2:15 am | Lazor Wulf | Is It Tho?
 2:30 am | Lazor Wulf | Dying to Eat
 2:45 am | Lazor Wulf | That Was Today. This Is Tomorrow.
 3:00 am | Futurama | Why Must I Be a Crustacean in Love?
 3:30 am | Futurama | Bendless Love
 4:00 am | King Of the Hill | Business Is Picking Up
 4:30 am | King Of the Hill | The Year of Washing Dangerously
 5:00 am | King Of the Hill | Hank Fixes Everything
 5:30 am | King Of the Hill | Church Hopping