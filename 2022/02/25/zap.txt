# 2022-02-25
 6:00 am | Care Bears: Unlock the Magic | Let the Fun Shine
 6:15 am | Care Bears: Unlock the Magic | Valley of the Shamrocks
 6:30 am | Esme & Roy | Tilliesaurus; Toy Share or Not Toy Share
 7:00 am | The Not-Too-Late Show With Elmo | Jimmy Fallon; Kacey Musgraves
 7:20 am | Cocomelon | Looby Loo
 7:24 am | Cocomelon | Winter Song (Fun in the Snow)
 7:28 am | Cocomelon | Traffic Safety Song
 7:32 am | Cocomelon | The Country Mouse and the City Mouse
 7:36 am | Cocomelon | Three Little Kittens
 7:40 am | Cocomelon | The Bear Went Over the Mountain
 7:44 am | Cocomelon | The Hiccup Song
 7:50 am | Let's Go Pocoyo | Giving Loula a Bath
 7:56 am | Let's Go Pocoyo | Pocoyo's New Toys
 8:02 am | Let's Go Pocoyo | Picnic
 8:10 am | Bing | Wellies
 8:20 am | Bing | Voo Voo
 8:30 am | Lucas the Spider | Sliding; Spinning Webs
 9:00 am | Thomas & Friends: All Engines Go | A Light Delivery
 9:10 am | Thomas & Friends: All Engines Go | Nia and the Ducks
 9:20 am | Thomas & Friends: All Engines Go | Calliope Crack-Up
 9:30 am | Thomas & Friends: All Engines Go | License to Deliver
 9:45 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
10:00 am | Mush-Mush and the Mushables | Compost To Go
10:15 am | Mush-Mush and the Mushables | Bouncing Back
10:30 am | Care Bears: Unlock the Magic | Let the Fun Shine
10:45 am | Care Bears: Unlock the Magic | Valley of the Shamrocks
11:00 am | Craig of the Creek | The Sunflower
11:15 am | Craig of the Creek | Craig World
11:30 am | Craig of the Creek | Capture the Flag Part 1: The Candy
11:48 am | Craig of the Creek | Capture the Flag Part 2: The King
12:06 pm | Craig of the Creek | Capture the Flag Part 3: The Legend
12:24 pm | Craig of the Creek | Capture the Flag Part 4: The Plan
12:42 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 1:00 pm | Teen Titans Go! | EEbows
 1:15 pm | Teen Titans Go! | Creative Geniuses
 1:30 pm | Teen Titans Go! | Batman's Birthday Gift
 1:45 pm | Teen Titans Go! | Manor and Mannerisms
 2:00 pm | Teen Titans Go! | What a Boy Wonders
 2:15 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 2:30 pm | Teen Titans Go! | Doomsday Preppers
 2:45 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 3:00 pm | Adventure Time | Slumber Party Panic
 3:15 pm | Adventure Time | Trouble in Lumpy Space
 3:30 pm | Adventure Time | Prisoners of Love
 3:45 pm | Adventure Time | Tree Trunks
 4:00 pm | We Baby Bears | A Tale of Two Ice Bears
 4:15 pm | We Baby Bears | Unica
 4:30 pm | Total DramaRama | Venthalla
 4:45 pm | Total DramaRama | Duck Duck Juice
 5:00 pm | Total DramaRama | Cluckwork Orange
 5:15 pm | Total DramaRama | Free Chili
 5:30 pm | Total DramaRama | The Date
 5:45 pm | Total DramaRama | Aquarium for a Dream
 6:00 pm | Total DramaRama | Cuttin' Corners
 6:15 pm | Total DramaRama | Sharing Is Caring
 6:30 pm | Total DramaRama | Aches and Ladders
 6:40 pm | Total DramaRama | Cactus Makes Perfect
 6:50 pm | Total DramaRama | Daycare of Rock
 7:00 pm | Total DramaRama | Virtual Reality Bites
 7:15 pm | Total DramaRama | The Opening Act
 7:30 pm | Total DramaRama | Van Hogling
 7:45 pm | Total DramaRama | Ticking Crime Bomb