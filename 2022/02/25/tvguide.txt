# 2022-02-25
 6:00 am | Care Bears: Unlock the Magic | Let the Fun Shine; Valley of the Shamrocks
 6:30 am | Esme & Roy | Tilliesaurus; Toy Share or Not Toy Share
 7:00 am | The Not Too Late Show with Elmo | Jimmy Fallon / Kacey Musgraves
 7:20 am | Cocomelon | 
 7:50 am | Pocoyo | Bathing Loula; Pocoyo's New Toys; Picnic
 8:10 am | Bing | Rainy Boots; Voo Voo
 8:30 am | Lucas the Spider | Slidingspinning Webs
 9:00 am | Thomas & Friends: All Engines Go | Light Delivery; A Nia and the Ducks; Calliope Crack-Up
 9:30 am | Thomas & Friends: All Engines Go | Thomas Blasts off; License to Deliver
10:00 am | Mush Mush and the Mushables | Compost to Go; Bouncing Back
10:30 am | Care Bears: Unlock the Magic | Let the Fun Shine; Valley of the Shamrocks
11:00 am | Craig of the Creek | Sunflower; The Craig World
11:30 am | Craig of the Creek | Capture the Flag part1:The Candy;Capture the Flag part2:The King;Capture the Flag part3:TheLegend;Capture the Flagpart4:The Plan
 1:00 pm | Teen Titans Go! | Eebows; Creative Geniuses
 1:30 pm | Teen Titans Go! | Batman's Birthday Gift; Manor and Mannerisms
 2:00 pm | Teen Titans Go! | What a Boy Wonders; Trans Oceanic Magical Cruise
 2:30 pm | Teen Titans Go! | Doomsday Preppers; Polly Ethylene and Tara Phthalate
 3:00 pm | Adventure Time | Slumber Party Panic; Trouble in Lumpy Space
 3:30 pm | Adventure Time | Prisoners of Love; Tree Trunks
 4:00 pm | We Baby Bears | A Tale of Two Ice Bears; Unica
 4:30 pm | Total DramaRama | Venthalla; Duck Duck Juice
 5:00 pm | Total DramaRama | Cluckwork Orange; Free Chili
 5:30 pm | Total DramaRama | The Date; Aquarium for a Dream
 6:00 pm | Total DramaRama | Cuttin' Corners; Sharing Is Caring
 6:30 pm | Total DramaRama | Aches and Ladders; Cactus Makes Perfect
 7:00 pm | Total DramaRama | The Virtual Reality Bites; Opening Act
 7:30 pm | Total DramaRama | Ticking Crime Bomb; Van Hogling