# 2022-02-15
 6:00 am | Care Bears: Unlock the Magic | Almost Eggless Easter; An; Three Cheers for Cheer
 6:30 am | Esme & Roy | Birds of a Feather; Cowgirl Fig
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Get Lost Loula; Sneaky Shoes; Shutterbug
 7:50 am | Love Monster | Clear out Day; Plant a Seed Day
 8:10 am | Bing | Looking After Flop; Skateboard
 8:30 am | Caillou | Copycat Clementine; Caillous Poster; Playhouse Play Date
 9:00 am | Thomas & Friends: All Engines Go | No Power; No Problem! Horrible Hiccups; Roller Coasting
 9:30 am | Thomas & Friends: All Engines Go | Thomas' Day Off/The Real Number One
10:00 am | Mush Mush and the Mushables | Grasshopper Chep; Bird Call
10:30 am | Care Bears: Unlock the Magic | Birthday That Wasn't; The; One Wise Bear
11:00 am | Craig of the Creek | Winter Break
11:30 am | Craig of the Creek | Body Swap Copycat Carter
12:00 pm | Craig of the Creek | Jessica the Intern Brother Builder
12:30 pm | Teen Titans Go! | Don't Press Play 287; The Night Begins to Shine 2: You're the One
 2:00 pm | Amazing World Of Gumball | The Crew; The Apprentice
 2:30 pm | Amazing World of Gumball | The Check; The Others
 3:00 pm | Amazing World of Gumball | The Signature; The Pest
 3:30 pm | Amazing World of Gumball | The Gift; The; Hug
 4:00 pm | We Baby Bears | Baby Bear Genius; Unica
 4:30 pm | Craig of the Creek | Capture the Flag Part I: The Candy; Capture the Flag Part II: The King
 5:00 pm | Craig of the Creek | Capture the Flag part 3: The Legend; Capture the Flag part4: The Plan
 5:30 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 6:00 pm | Teen Titans Go! | Butter Wall; Villains in a Van Getting Gelato
 6:30 pm | Teen Titans Go! | Superhero Feud; I Am Chair
 7:00 pm | Amazing World of Gumball | The Anybody; The Candidate
 7:30 pm | Amazing World of Gumball | The Shippening; The Brain