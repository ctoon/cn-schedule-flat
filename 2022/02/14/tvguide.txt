# 2022-02-14
 6:00 am | Care Bears: Unlock the Magic | The Festival of Hearts; Big Rumble
 6:30 am | Esme & Roy | Simon's Sleepover; Take a Hike
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Party Time; Hide and Seek; Dance Off!
 7:50 am | Love Monster | Fluffytown Rocks Day; Red Envelope Day
 8:10 am | Bing | Potato; Chalk Dinosaur
 8:30 am | Caillou | Caillou's Valentines; Hello Spring!; Caillou's April Fool
 9:00 am | Thomas & Friends: All Engines Go | Super-Long Shortcut; The Lighthouse Disco; Tyrannosaurus Wrecks
 9:30 am | Thomas & Friends: All Engines Go | The Can-Do Submarine Crew; Tiger Train
10:00 am | Mush Mush and the Mushables | The Snake Dance; The; Missing Mushlet
10:30 am | Care Bears: Unlock the Magic | The Wish-Full Reunion; A; Badge of Courage
11:00 am | Craig of the Creek | Doorway to Helen; Tea Timer's Ball
11:30 am | Clarence | Valentimes; Clarence Gets a Girlfriend
12:00 pm | Teen Titans Go! | Friendship Rocks and Water
12:30 pm | Teen Titans Go! | BBRAE
 1:00 pm | Teen Titans Go! | Matched; Cat's Fancy
 1:30 pm | Teen Titans Go! | How 'Bout Some Effort; Dignity of Teeth
 2:00 pm | Victor and Valentino | The Guillermo's Girlfriend; Lonely Haunts Club
 2:30 pm | Amazing World of Gumball | The Storm; The; Romantic
 3:00 pm | Amazing World of Gumball | The Love; The; Slide
 3:30 pm | Amazing World of Gumball | The Shippening; The Date
 4:00 pm | We Baby Bears | Little Mer-Bear; The; Hashtag; Fan
 4:30 pm | Craig of the Creek | Fire & Ice; Breaking the Ice
 5:00 pm | Craig of the Creek | Doorway to Helen; Tea Timer's Ball
 5:30 pm | Teen Titans Go! | How 'bout Some Effort; Be Mine
 6:00 pm | Rio | 