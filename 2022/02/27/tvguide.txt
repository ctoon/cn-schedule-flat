# 2022-02-27
 6:00 am | Ben 10 | The Galactic Enforcers
 6:30 am | Ben 10 | Camp Fear
 7:00 am | Teen Titans | Date with Destiny
 7:30 am | Teen Titans | Transformation
 8:00 am | Teen Titans Go! | Cartoon Feud; TV Knight 4
 8:30 am | Teen Titans Go! | Teen Titans Roar! Don't Be an Icarus
 9:00 am | We Baby Bears | Bears in the Dark; Panda's Family
 9:30 am | We Baby Bears | A Big Trouble Little Babies; Tale of Two Ice Bears
10:00 am | Total DramaRama | Bananas & Cheese; Know It All
10:30 am | Total DramaRama | A Inglorious Toddlers; Licking Time Bomb
11:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
12:00 pm | Amazing World of Gumball | The Pressure; The Painting
12:30 pm | Amazing World of Gumball | The Responsible; The Dress
 1:00 pm | Craig of the Creek | New Jersey; Copycat Carter
 1:30 pm | Craig of the Creek | Capture the Flag pt1:The Candy;Capture the Flag pt2:The King;Capture the Flag pt3:The Legend;Capture the Flag pt4:The Plan;Cap
 3:00 pm | Teen Titans Go! | Super Summer Hero Camp
 4:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 4:30 pm | Amazing World of Gumball | The Mystery; The Prank
 5:00 pm | Amazing World of Gumball | The GI; The Kiss
 5:30 pm | Amazing World of Gumball | The Party; The Refund
 6:00 pm | She's the Man | 