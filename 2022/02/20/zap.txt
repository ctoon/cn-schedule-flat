# 2022-02-20
 6:00 am | Ben 10 | Big Tick
 6:30 am | Ben 10 | Framed
 7:00 am | Teen Titans | Every Dog Has His Day
 7:30 am | Teen Titans | Terra
 8:00 am | Teen Titans Go! | The Real Orangins!
 8:15 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:45 am | Teen Titans Go! | I'm the Sauce
 9:00 am | We Baby Bears | Snow Place Like Home
 9:15 am | We Baby Bears | Bears in the Dark
 9:30 am | We Baby Bears | Fiesta Day
 9:45 am | We Baby Bears | Big Trouble Little Babies
10:00 am | Total DramaRama | Cuttin' Corners
10:10 am | Total DramaRama | That's a Wrap
10:20 am | Total DramaRama | 
10:30 am | Total DramaRama | Sharing Is Caring
10:45 am | Total DramaRama | Tiger Fail
11:00 am | Amazing World of Gumball | The Agent
11:15 am | Amazing World of Gumball | The Decisions
11:30 am | Amazing World of Gumball | The Web
11:45 am | Amazing World of Gumball | The Inquisition
12:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 1:00 pm | Teen Titans Go! | The Art of Ninjutsu
 1:30 pm | Teen Titans Go! | The Night Begins to Shine Special
 2:00 pm | Teen Titans Go! | Night Begins to Shine 2: You're the One
 3:15 pm | Teen Titans GO! to the Movies | 
 5:00 pm | Teen Titans GO! NBA Slam Dunk Special | 
 5:30 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 5:40 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 5:50 pm | Teen Titans Go! | 
 6:00 pm | 17 Again | 