# 2022-02-20
 6:00 am | Ben 10 | Big Tick
 6:30 am | Ben 10 | Framed
 7:00 am | Teen Titans | Every Dog Has Its Day
 7:30 am | Teen Titans | Terra
 8:00 am | Teen Titans Go! | Real Orangins! ; The Nostalgia Is Not a Substitute for an Actual Story
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:00 am | We Baby Bears | Snow Place Like Home; Bears in the Dark
 9:30 am | We Baby Bears | Fiesta Day; Big Trouble Little Babies
10:00 am | Total DramaRama | Cuttin' Corners; That's a Wrap
10:30 am | Total DramaRama | Sharing Is Caring; Tiger Fail
11:00 am | Amazing World Of Gumball | The Agent; The Decisions
11:30 am | Amazing World Of Gumball | The Web; The Inquisition
12:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 1:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 2:00 pm | Teen Titans Go! | Night Begins to Shine 2: You're the One
 3:15 pm | Teen Titans Go! To the Movies | 
 5:00 pm | Cartoon Network Special Edition: Teen Titans Go! NBA All-Star Slam Dunk Special | 
 5:30 pm | Teen Titans Go! | Cruel Giggling Ghoul; The; Slapping Butts and Celebrating for No Reason
 6:00 pm | Again | 