# 2022-02-13
 6:00 am | Ben 10 | A Small Problem
 6:30 am | Ben 10 | Side Effects
 7:00 am | Teen Titans | Apprentice
 7:30 am | Teen Titans | Apprentice
 8:00 am | Teen Titans Go! | Kabooms
 8:30 am | Teen Titans Go! | Tower Renovation; Genie President; Oh Yeah!
 9:00 am | We Baby Bears | Bears and the Beanstalk; Meat House
 9:30 am | We Baby Bears | Boo-Dunnit; Pirate Parrot Polly
10:00 am | Total DramaRama | Ticking Crime Bomb; Van Hogling
10:30 am | Total DramaRama | The Opening Act; Virtual Reality Bites
11:00 am | Amazing World of Gumball | The Possession; The Buddy
11:30 am | Amazing World Of Gumball | The Heart; The Master
12:00 pm | Amazing World of Gumball | The Silence; The; Ghouls
12:30 pm | Amazing World of Gumball | The Future; The; Bffs; the; Sucker
 1:00 pm | Craig of the Creek | Fall Anthology Snow Day
 1:30 pm | Craig of the Creek | Afterschool Snackdown Snow Place Like Home
 2:00 pm | Craig of the Creek | The Dream Team; Fire & Ice
 2:30 pm | Craig of the Creek | King of Camping; Welcome to Creek Street; Jacob of the Creek
 3:00 pm | Teen Titans Go! | Quantum Fun; Little Elvis
 3:30 pm | Teen Titans Go! | The Fight; The Groover
 4:00 pm | Daddy Day Care | 
 6:00 pm | Marley & Me | 