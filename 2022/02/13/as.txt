# 2022-02-13
 8:30 pm | Bob's Burgers | Sexy Dance Fighting
 9:00 pm | Futurama | Into the Wild Green Yonder Part 1
 9:30 pm | Futurama | Into the Wild Green Yonder Part 2
10:00 pm | American Dad | Who Smarted?
10:30 pm | American Dad | The Book of Fischer
11:00 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
11:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
12:00 am | Smiling Friends | Desmond's Big Day Out
12:15 am | Smiling Friends | Mr. Frog
12:30 am | Smiling Friends | Shrimp's Odyssey
12:45 am | Smiling Friends | A Silly Halloween Special
 1:00 am | Smiling Friends | Who Violently Murdered Simon S. Salty?
 1:15 am | Smiling Friends | Enchanted Forest
 1:30 am | Smiling Friends | Frowning Friends
 1:45 am | Smiling Friends | Charlie Dies and Doesn't Come Back
 2:00 am | American Dad | The Book of Fischer
 2:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | Smiling Friends | Desmond's Big Day Out
 3:45 am | Smiling Friends | Mr. Frog
 4:00 am | King Of the Hill | Bystand Me
 4:30 am | King Of the Hill | Bill's House
 5:00 am | King Of the Hill | A Portrait of the Artist as a Young Clown
 5:30 am | King Of the Hill | You Gotta Believe (In Moderation)