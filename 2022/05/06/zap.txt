# 2022-05-06
 6:00 am | Mush-Mush and the Mushables | Fly, Chase, Fly
 6:30 am | Cocomelon | Yes Yes Bed Time Camping
 7:00 am | Cocomelon | Winter Show and Tell
 7:30 am | Let's Go Pocoyo | Pocoyo Recycles
 7:50 am | Love Monster | Frosty Frolics Day
 8:10 am | Bing | Cake
 8:30 am | Lucas the Spider | Sleeping Dogs
 9:00 am | Thomas & Friends: All Engines Go | Ghost Train
 9:30 am | Thomas & Friends: All Engines Go | Counting Cows
10:00 am | Mush-Mush and the Mushables | Save the Fun Tree
10:30 am | Baby Looney Tunes | For Whom The Toll Calls; John Jacob Jongle Elmer Fudd ; Cereal Boxing
11:00 am | Craig of the Creek | Alternate Creekiverse
11:30 am | Craig of the Creek | Snow Day
12:00 pm | Craig of the Creek | Winter Break
12:30 pm | Teen Titans Go! | Rain on Your Wedding Day
 1:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 2:00 pm | Amazing World of Gumball | The Butterfly; The Question
 2:30 pm | Amazing World of Gumball | The Oracle; The Safety
 3:00 pm | Amazing World of Gumball | The Friend; The Saint
 3:30 pm | Amazing World of Gumball | The Society; The Spoiler
 4:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 5:00 pm | We Baby Bears | Sheep Bears
 5:30 pm | We Baby Bears | Dragon Pests
 6:00 pm | We Baby Bears | Tooth Fairy Tech
 6:15 pm | We Baby Bears | Unica
 6:30 pm | Teen Titans Go! | Space House
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition