# 2022-05-06
 8:00 pm | King Of the Hill | Pigmalion
 8:30 pm | King Of the Hill | Megalo Dale
 9:00 pm | Futurama | Rebirth
 9:30 pm | Futurama | In-A-Gadda-Da-Leela
10:00 pm | American Dad | Paranoid_Frandroid
10:30 pm | American Dad | The Census of the Lambs
11:00 pm | American Dad | Shell Game
11:30 pm | Rick and Morty | Pilot
12:00 am | Birdgirl | Thirdgirl
12:30 am | Birdgirl | We Got the Internet
 1:00 am | Birdgirl | Topple the Popple
 1:30 am | Birdgirl | Baltimo
 2:00 am | Rick and Morty | Lawnmower Dog
 2:30 am | Futurama | Rebirth
 3:00 am | Futurama | In-A-Gadda-Da-Leela
 3:30 am | Infomercials | When Panties Fly
 3:45 am | Infomercials | Rate the Cookie
 4:00 am | King Of the Hill | Boxing Luanne
 4:30 am | King Of the Hill | Vision Quest
 5:00 am | King Of the Hill | Pigmalion
 5:30 am | King Of the Hill | Megalo Dale