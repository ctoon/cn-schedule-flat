# 2022-05-06
 6:00 am | Mush Mush and the Mushables | Fly; Chase; Fly; Who's Eating Mushton
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | The Pocoyo Recycles; Art; Messy Guest
 7:50 am | Love Monster | Frosty Frolics Day; Movie Night
 8:10 am | Bing | Cake; Voo Voo
 8:30 am | Lucas the Spider | Sleeping Dogsskunkedarlo and the Snake
 9:00 am | Thomas & Friends: All Engines Go | Ghost Train; Sandy Versus the Storm
 9:30 am | Thomas & Friends: All Engines Go | Music Is Everywhere Counting Cows
10:00 am | Mush Mush and the Mushables | Save the Fun Tree; Grasshopper Chep
10:30 am | Baby Looney Tunes | For Whom The Toll Calls; John Jacob Jongle Elmer Fudd (song); Cereal Boxing
11:00 am | Craig of the Creek | Alternate Creekiverse; Winter Creeklympics
11:30 am | Craig of the Creek | Snow Day; Welcome to Creek Street
12:00 pm | Craig of the Creek | Winter Break
12:30 pm | Teen Titans Go! | The Rain on Your Wedding; Day Cast
 1:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 2:00 pm | Amazing World of Gumball | The Butterfly; The Question
 2:30 pm | Amazing World of Gumball | The Oracle; The Safety
 3:00 pm | Amazing World of Gumball | The Friend; The Saint
 3:30 pm | Amazing World of Gumball | The Spoiler; The Society
 4:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 5:00 pm | We Baby Bears | Sheep Bears; Ice Bear's Pet
 5:30 pm | We Baby Bears | Dragon Pests; No Land; All Air!
 6:00 pm | We Baby Bears | Tooth Fairy Tech
 6:15 pm | We Baby Bears | Unica
 6:30 pm | Teen Titans Go! | Space House
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition