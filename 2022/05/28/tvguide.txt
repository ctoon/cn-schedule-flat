# 2022-05-28
 6:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 6:30 am | Teen Titans Go! | Permanent Record; Movie Night
 7:00 am | Teen Titans Go! | Titan Saving Time; Master Detective
 7:30 am | Teen Titans Go! | The Avogodo; Hot Salad Water
 8:00 am | Teen Titans Go! | Hand Zombie; Brain Percentages
 8:30 am | Teen Titans Go! | Employee Of The Month Redux; Bl4z3
 9:00 am | Teen Titans Go! | Orangins; Lication
 9:30 am | Teen Titans Go! | Ones and Zeros; Jinxed
10:00 am | Teen Titans Go! | TV Knight 2; Career Day
10:30 am | Teen Titans Go! | Throne of Bones; The Academy
11:00 am | Teen Titans Go! | Space House
12:00 pm | Teen Titans Go! | Mug; The; Trans Oceanic Magical Cruise
12:30 pm | Teen Titans Go! | Hafo Safo; Jam
 1:00 pm | Teen Titans Go! | Zimdings; Polly Ethylene and Tara Phthalate
 1:30 pm | Teen Titans Go! | Pig in a Poke; Eebows
 2:00 pm | Teen Titans Go! | P.P. Batman's Birthday Gift
 2:30 pm | Teen Titans Go! | Marv Wolfman and George Pérez; What a Boy Wonders
 3:00 pm | Teen Titans Go! | Cy & Beasty; Doomsday Preppers
 3:30 pm | Teen Titans Go! | Fat Cats; Control Freak
 4:00 pm | Teen Titans Go! | Whodundidit? ; Standards & Practices
 4:30 pm | Teen Titans Go! | Belly Math; Sweet Revenge
 5:00 pm | Teen Titans Go! | Free Perk; Porch Pirates
 5:30 pm | Teen Titans Go! | A Go! ; Sticky Situation
 6:00 pm | Teen Titans Go! & DC Super Hero Girls: Mayhem in the Multiverse | 
 7:45 pm | Teen Titans Go! | The Perfect Pitch?