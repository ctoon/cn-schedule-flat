# 2022-05-26
 6:00 am | Mush-Mush and the Mushables | The Mysterious Seedpod
 6:30 am | Cocomelon | Class Pet Song
 7:00 am | Cocomelon | You Can Ride a Bike
 7:30 am | Cocomelon | Reading Song
 8:00 am | Pocoyo | Party Pooper
 8:20 am | Bing | Lost
 8:30 am | Thomas & Friends: All Engines Go | The Real Number One
 9:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 9:30 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
10:00 am | Mush-Mush and the Mushables | The Breathtaking Bolly Show
10:30 am | Baby Looney Tunes | These Little Piggies Went to Market; Now Museum, Now You Don't
11:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
12:00 pm | Teen Titans Go! | Cat's Fancy
12:30 pm | Teen Titans Go! | Leg Day
 1:00 pm | Teen Titans Go! | The Dignity of Teeth
 1:30 pm | Teen Titans Go! | The Croissant
 2:00 pm | Teen Titans Go! | The Spice Game
 2:30 pm | Teen Titans Go! | I'm the Sauce
 3:00 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 3:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 4:00 pm | Teen Titans Go! | The Fourth Wall
 4:30 pm | Teen Titans Go! | BBBDay!
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 6:00 pm | Teen Titans Go! | A Sticky Situation
 6:15 pm | Teen Titans Go! | Porch Pirates
 6:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 7:00 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 7:30 pm | Teen Titans Go! | The Return of Slade; More of the Same