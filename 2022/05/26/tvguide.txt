# 2022-05-26
 6:00 am | Mush Mush and the Mushables | Mysterious Seepod; The; Star Helpers
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Pocoyo | Party P.; My Pato
 8:20 am | Bing | Lost
 8:30 am | Thomas & Friends: All Engines Go | The Tiger Train; The Real Number One
 9:00 am | Mecha Builders | Treasure of Treetop Woods; Thezee Makes an Amazing Maze
 9:30 am | Mecha Builders | That Old Time Rock Rollsmake Like a Banana Split
10:00 am | Mush Mush and the Mushables | The Breathtaking Bolly Show; The; Rollinator
10:30 am | Baby Looney Tunes | These Little Piggies Went to the Market; Now Museum, Now You Don't
11:00 am | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
11:30 am | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
12:00 pm | Teen Titans Go! | Squash & Stretch; Cat's Fancy
12:30 pm | Teen Titans Go! | Garage Sale; Leg Day
 1:00 pm | Teen Titans Go! | The Dignity of Teeth; Secret Garden
 1:30 pm | Teen Titans Go! | The Croissant; The Cruel Giggling Ghoul
 2:00 pm | Teen Titans Go! | Bottle Episode; Spice Game
 2:30 pm | Teen Titans Go! | Pyramid Scheme; I'm the Sauce
 3:00 pm | Teen Titans Go! | Accept the Next Proposition You Hear; Finally a Lesson
 3:30 pm | Teen Titans Go! | Hey You; Don't Forget About Me in Your Memory Arms Race With Legs
 4:00 pm | Teen Titans Go! | Obinray; The Fourth Wall
 4:30 pm | Teen Titans Go! | Bbbday! ; Batman vs. Teen Titans: Dark Injustice
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 6:00 pm | Teen Titans Go! | A Sticky Situation
 6:15 pm | Teen Titans Go! | Porch Pirates
 6:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 7:00 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 7:30 pm | Teen Titans Go! | The Return of Slade; More of the Same