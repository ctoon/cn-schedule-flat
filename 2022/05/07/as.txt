# 2022-05-07
 8:00 pm | King Of the Hill | Queasy Rider
 8:30 pm | King Of the Hill | Board Games
 9:00 pm | Futurama | Law & Oracle
 9:30 pm | Futurama | The Silence of the Clamps
10:00 pm | American Dad | The Mural of the Story
10:30 pm | American Dad | (You Gotta) Strike for Your Right
11:00 pm | American Dad | Klaustastrophe.tv
11:30 pm | Rick and Morty | Mort Dinner Rick Andre
12:00 am | Assassination Classroom | Valentine's Day Time
12:30 am | Lupin the 3rd Part 6 | Adventure Along the (Bogus) Transcontinental Railroad
 1:00 am | One Piece | Back to the Present! Hordy Makes a Move!
 1:30 am | One Piece | The Kingdom in Shock! An Order to Execute Neptune Issued!
 2:00 am | Naruto:Shippuden | My First Friend
 2:30 am | Naruto:Shippuden | The Adored Elder Sister
 3:00 am | Shenmue: The Animation | Thunderclap
 3:30 am | Cowboy Bebop | The Real Folk Blues -- Part 2
 4:00 am | Futurama | Law & Oracle
 4:30 am | Futurama | The Silence of the Clamps
 5:00 am | King Of the Hill | Queasy Rider
 5:30 am | King Of the Hill | Board Games