# 2022-05-15
 6:00 am | Teen Titans Go! | Garage Sale; Squash & Stretch
 6:30 am | Teen Titans Go! | Secret Garden Finally a Lesson
 7:00 am | Teen Titans | Snow Blind
 7:30 am | Teen Titans | Kole
 8:00 am | Teen Titans Go! | Arms Race with Legs; Pyramid Scheme
 8:30 am | Teen Titans Go! | Bottle Episode; Obinray
 9:00 am | We Baby Bears | Tooth Fairy Tech; Dragon Pests
 9:30 am | We Baby Bears | No Land; All Air! ; Ice Bear's Pet
10:00 am | Total DramaRama | Duck Duck Juice; Cluckwork Orange
10:30 am | Total DramaRama | Aquarium for a Dream; Free Chili
11:00 am | Craig of the Creek | Adventures in Baby Casino; Hyde & Zeke
11:30 am | Craig of the Creek | Grandma Smugglers; Lost & Found
12:00 pm | Craig of the Creek | Capture the Flag pt1:The Candy;Capture the Flag pt2:The King;Capture the Flag pt3:The Legend;Capture the Flag pt4:The Plan;Cap
 1:30 pm | Craig of the Creek | Beyond the Overpass; Sink or Swim Team
 2:00 pm | Craig of the Creek | The Quick Name; The Chef's Challenge
 2:30 pm | Craig of the Creek | The Sparkle Solution; Better Than You
 3:00 pm | Craig of the Creek | The Dream Team; Fire & Ice
 3:30 pm | Craig of the Creek | Chrono Moss; Dodgy Decisions
 4:00 pm | Craig of the Creek | In Search of Lore; Creek Talent Extravaganza
 4:30 pm | Craig of the Creek | The Opposite Day; Anniversary Box
 5:00 pm | Craig of the Creek | Adventures in Baby Casino; Hyde & Zeke
 5:30 pm | Craig of the Creek | Grandma Smugglers; Lost & Found
 6:00 pm | Rampage | 