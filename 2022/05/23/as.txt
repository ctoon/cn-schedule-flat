# 2022-05-23
 8:00 pm | King Of the Hill | Yard, She Blows!
 8:30 pm | King Of the Hill | Dale To the Chief
 9:00 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 9:30 pm | Bob's Burgers | Human Flesh
10:00 pm | Rick and Morty | A Rickle in Time
10:30 pm | American Dad | Trophy Wife, Trophy Life
11:00 pm | American Dad | Game Night
11:30 pm | American Dad | American Data?
12:00 am | The Boondocks | Good Times
12:30 am | Robot Chicken | Fridge Smell
12:45 am | Robot Chicken | Western Hay Batch
 1:00 am | Aqua Teen | The Granite Family
 1:15 am | Aqua Teen | Bookie
 1:30 am | Rick and Morty | A Rickle in Time
 2:00 am | Futurama | My Three Suns
 2:30 am | Futurama | A Big Piece of Garbage
 3:00 am | Robot Chicken | Fridge Smell
 3:15 am | Robot Chicken | Western Hay Batch
 3:30 am | Aqua Teen | The Granite Family
 3:45 am | Aqua Teen | Bookie
 4:00 am | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 4:30 am | Bob's Burgers | Human Flesh
 5:00 am | King Of the Hill | Yard, She Blows!
 5:30 am | King Of the Hill | Dale To the Chief