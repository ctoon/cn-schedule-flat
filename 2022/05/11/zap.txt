# 2022-05-11
 6:00 am | Mush-Mush and the Mushables | Snails Forever
 6:30 am | Cocomelon | Ten Little Duckies
 7:00 am | Cocomelon | Humpty Dumpty
 7:30 am | Cocomelon | Yes Yes Bedtime Song
 8:00 am | Let's Go Pocoyo | Nurse Elly
 8:20 am | Bing | Here I Go
 8:30 am | Thomas & Friends: All Engines Go | Backwards Day
 9:00 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
 9:30 am | Mecha Builders | Magnet Mayhem; Stop That Train
10:00 am | Mush-Mush and the Mushables | A Frog of a Problem
10:30 am | Baby Looney Tunes | A Pastime For Petunia; Looney Tunes Zoo (song); Pouting Match
11:00 am | Craig of the Creek | Capture the Flag Part 1: The Candy
11:30 am | Craig of the Creek | Capture the Flag Part 3: The Legend
12:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
12:30 pm | Teen Titans Go! | Various Modes of Transportation
 1:00 pm | Teen Titans Go! | Cool Uncles
 1:30 pm | Teen Titans Go! | Butter Wall
 2:00 pm | Amazing World of Gumball | The Gift
 2:30 pm | Amazing World of Gumball | The Apprentice
 3:00 pm | Amazing World of Gumball | The Check
 3:30 pm | Amazing World of Gumball | The Pest
 4:00 pm | We Baby Bears | Fiesta Day
 4:30 pm | Craig of the Creek | Fire & Ice
 5:00 pm | Craig of the Creek | Opposite Day
 5:30 pm | Craig of the Creek | The Anniversary Box
 6:00 pm | Teen Titans Go! | Zimdings
 6:30 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 7:00 pm | Amazing World of Gumball | The Girlfriend
 7:30 pm | Amazing World of Gumball | The Parasite