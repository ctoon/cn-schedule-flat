# 2022-05-20
 6:00 am | Lucas the Spider | Lucas and His Friends
 6:30 am | Cocomelon | Skidamarink
 7:00 am | Cocomelon | Clean Up Song
 7:30 am | Cocomelon | Three Little Pigs Pirates
 8:00 am | Pocoyo | Call Me
 8:20 am | Bing | Mine
 8:30 am | Thomas & Friends: All Engines Go | A Rusty Rescue
 9:00 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
 9:30 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
10:00 am | Mush-Mush and the Mushables | The Staff of Wisdom
10:30 am | Baby Looney Tunes | The Littlest Tweety; Over in The Burrow (song); In Bugs We Trust
11:00 am | Craig of the Creek | Lost in the Sewer
11:30 am | Craig of the Creek | The Future Is Cardboard
12:00 pm | Craig of the Creek | Power Punchers
12:30 pm | Teen Titans Go! | Free Perk
 1:00 pm | Teen Titans Go! | BBRAE Pt 1
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:00 pm | Amazing World of Gumball | The Sorcerer
 2:30 pm | Amazing World of Gumball | The Console
 3:00 pm | Amazing World of Gumball | The Nuisance
 3:30 pm | Amazing World of Gumball | The Worst
 4:00 pm | We Baby Bears | No Land, All Air!
 4:30 pm | Craig of the Creek | The Final Book
 5:00 pm | Craig of the Creek | Too Many Treasures
 5:30 pm | Craig of the Creek | Wildernessa
 6:00 pm | Nacho Libre | 