# 2022-05-20
 8:00 pm | King Of the Hill | Girl, You'll Be a Giant Soon
 8:30 pm | King Of the Hill | Stressed For Success
 9:00 pm | Futurama | The Duh-Vinci Code
 9:30 pm | Futurama | Lethal Inspection
10:00 pm | American Dad | Demolition Daddy
10:30 pm | American Dad | Pride Before the Fail
11:00 pm | American Dad | Enter Stanman
11:30 pm | Rick and Morty | Ricksy Business
12:00 am | Three Busy Debras | The Milk Drought
12:15 am | Three Busy Debras | The Great Debpression
12:30 am | Three Busy Debras | The Honorable Order of the Debras
12:45 am | Three Busy Debras | To Have Debra, To Hold Debra
 1:00 am | Three Busy Debras | Operation: Seal Team Debra
 1:15 am | Three Busy Debras | Who Has Done It?
 1:30 am | Three Busy Debras | Debra Gets A Boyfriend
 1:45 am | Three Busy Debras | Women's History Hour
 2:00 am | Futurama | The Late Philip J. Fry
 2:30 am | Futurama | That Darn Katz!
 3:00 am | Futurama | A Clockwork Origin
 3:30 am | Futurama | The Prisoner of Benda
 4:00 am | King Of the Hill | Hank's Back
 4:30 am | King Of the Hill | The Redneck on Rainey Street
 5:00 am | King Of the Hill | Girl, You'll Be a Giant Soon
 5:30 am | King Of the Hill | Stressed For Success