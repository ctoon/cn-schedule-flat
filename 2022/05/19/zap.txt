# 2022-05-19
 6:00 am | Mush-Mush and the Mushables | Let It Bee
 6:30 am | Cocomelon | Three Little Pigs
 7:00 am | Cocomelon | The Hiccup Song
 7:30 am | Cocomelon | Happy Birthday Song
 8:00 am | Pocoyo | Vamoosh on the Loose
 8:20 am | Bing | Bake
 8:30 am | Thomas & Friends: All Engines Go | Overnight Stop
 9:00 am | Mecha Builders | The Need For Feed; Zipline Sisters Of Treetop Woods
 9:30 am | Mecha Builders | Magnet Mayhem; Stop That Train
10:00 am | Mush-Mush and the Mushables | Stuck in the Mud
10:30 am | Baby Looney Tunes | Melissa the Hero; My Bunny Lies Over the Ocean; The Trouble With Larry
11:00 am | Craig of the Creek | The Curse
11:30 am | Craig of the Creek | Dog Decider
12:00 pm | Craig of the Creek | Bring Out Your Beast
12:30 pm | Teen Titans Go! | What a Boy Wonders
 1:00 pm | Teen Titans Go! | Control Freak
 1:30 pm | Teen Titans Go! | Standards & Practices
 2:00 pm | Amazing World of Gumball | The News
 2:30 pm | Amazing World of Gumball | The Vase
 3:00 pm | Amazing World of Gumball | The Ollie
 3:30 pm | Amazing World of Gumball | The Potato
 4:00 pm | We Baby Bears | Dragon Pests
 4:30 pm | Craig of the Creek | Adventures in Baby Casino
 5:00 pm | Craig of the Creek | Itch to Explore
 5:30 pm | Craig of the Creek | Jessica Goes to the Creek
 6:00 pm | Teen Titans Go! | Teen Titans Vroom
 6:30 pm | Teen Titans Go! | The Great Disaster
 7:00 pm | Amazing World of Gumball | The Awkwardness
 7:30 pm | Amazing World of Gumball | The Disaster