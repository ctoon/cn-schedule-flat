# 2022-05-09
 6:00 am | Mush Mush and the Mushables | The Race the Beast; Snake Dance
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Mecha Builders | The Sent US a Pie / Dust In the Wind
 8:30 am | Mecha Builders | Mecha Builders Pull Togetherlift up and Lift Off
 9:00 am | Mecha Builders | That Old Time Rock Rollsmake Like a Banana Split
 9:30 am | Mecha Builders | Magnet Mayhemstop That Train
10:00 am | Mecha Builders | Picture Perfect Park Partysun Block
10:30 am | Baby Looney Tunes | Who Said That?; Paws And Feathers (song); Let Them Make Cake
11:00 am | Craig of the Creek | Fan or Foe; Body Swap
11:30 am | Craig of the Creek | New Jersey; Copycat Carter
12:00 pm | Craig of the Creek | The Sunflower; Brother Builder
12:30 pm | Teen Titans Go! | 287 Night Begins to Shine 2: You're the One; Toddler Titans...Yay!
 2:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 2:30 pm | Amazing World of Gumball | The Egg; The Downer
 3:00 pm | Amazing World of Gumball | The Money; The Triangle
 3:30 pm | Amazing World of Gumball | The Return; The Hug
 4:00 pm | We Baby Bears | Hashtag; Fan; Dragon Pests
 4:30 pm | Craig of the Creek | Capture the Flag Part I: The Candy; Capture the Flag Part II: The King
 5:00 pm | Happy Feet Two | 
 7:00 pm | Amazing World of Gumball | The Gift; The Comic
 7:30 pm | Amazing World of Gumball | The Apprentice; The Romantic