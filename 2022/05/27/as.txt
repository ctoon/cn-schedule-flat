# 2022-05-27
 8:00 pm | King Of the Hill | Smoking and the Bandit
 8:30 pm | King Of the Hill | Gone With the Windstorm
 9:00 pm | Futurama | The Late Philip J. Fry
 9:30 pm | Futurama | That Darn Katz!
10:00 pm | American Dad | The Last Ride of the Dodge City Rambler
10:30 pm | American Dad | 300
11:00 pm | American Dad | Yule. Tide. Repeat.
11:30 pm | Rick and Morty | Get Schwifty
12:00 am | King Of the Hill | Hank and the Great Glass Elevator
12:30 am | King Of the Hill | Raise the Steaks
 1:00 am | King Of the Hill | Just Another Manic Kahn-Day
 1:30 am | King Of the Hill | To Sirloin With Love
 2:00 am | Futurama | Lrrreconcilable Ndndifferences
 2:30 am | Futurama | The Mutants Are Revolting
 3:00 am | Futurama | Holiday Spectacular
 3:30 am | Futurama | Neutopia
 4:00 am | King Of the Hill | Bobby On Track
 4:30 am | King Of the Hill | It Ain't Over Till The Fat Neighbor Sings
 5:00 am | King Of the Hill | Smoking and the Bandit
 5:30 am | King Of the Hill | Gone With the Windstorm