# 2022-05-27
 6:00 am | Lucas the Spider | The Legend of Big Butt
 6:30 am | Cocomelon | JJ's New Bed Arrives
 7:00 am | Cocomelon | This Is the Way Bedtime Edition
 7:30 am | Cocomelon | This Little Piggy
 8:00 am | Pocoyo | Hack Attack
 8:20 am | Bing | Lunch
 8:30 am | Thomas & Friends: All Engines Go | Hide and Surprise!
 9:00 am | Mecha Builders | Magnet Mayhem; Stop That Train
 9:30 am | Mecha Builders | Picture Perfect Park Party/Sun Block
10:00 am | Mush-Mush and the Mushables | Work Like a Charm
10:30 am | Baby Looney Tunes | Take Us Out to the Ballgame; Clues Encounters of the Tweety Kind
11:00 am | Teen Titans Go! | Grube's Fairytales
11:30 am | Teen Titans Go! | A Farce
12:00 pm | Teen Titans Go! | Animals: It's Just a Word!
12:30 pm | Teen Titans Go! | The Art of Ninjutsu
 1:00 pm | Teen Titans Go! | Think About Your Future
 1:30 pm | Teen Titans Go! | Booty Scooty
 2:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 2:30 pm | Teen Titans Go! | Two Parter: Part One
 3:00 pm | Teen Titans Go! | Oh Yeah!
 3:30 pm | Teen Titans Go! | Shrimps and Prime Rib
 4:00 pm | Teen Titans Go! | Island Adventures
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 6:00 pm | Teen Titans Go! | The Perfect Pitch?
 6:15 pm | Teen Titans Go! | A Sticky Situation
 6:30 pm | Teen Titans Go! | Porch Pirates
 7:00 pm | Teen Titans Go! | Whodundidit?
 7:30 pm | Teen Titans Go! | BBSFBDAY!