# 2022-05-17
 6:00 am | Mush-Mush and the Mushables | The Staff of Wisdom
 6:30 am | Cocomelon | Head Shoulders Knees & Toes
 7:00 am | Cocomelon | What Makes Me Happy
 7:30 am | Cocomelon | Jello Color Song
 8:00 am | Pocoyo | Hole Lotta Trouble
 8:20 am | Bing | Mine
 8:30 am | Thomas & Friends: All Engines Go | Pop a Wheelie
 9:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 9:30 am | Mecha Builders | Yip Yip Book Book; Stick To It
10:00 am | Mush-Mush and the Mushables | Let It Bee
10:30 am | Baby Looney Tunes | The Yolk's On You; Baby Elmer Had A Friend (song); Baby-Gate
11:00 am | Craig of the Creek | The Final Book
11:30 am | Craig of the Creek | Too Many Treasures
12:00 pm | Craig of the Creek | Wildernessa
12:30 pm | Teen Titans Go! | Space House
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 2:00 pm | Amazing World of Gumball | The Boredom
 2:30 pm | Amazing World of Gumball | The Vision
 3:00 pm | Amazing World of Gumball | The Choices
 3:30 pm | Amazing World of Gumball | The Code
 4:00 pm | We Baby Bears | Sheep Bears
 4:30 pm | Craig of the Creek | The Curse
 5:00 pm | Craig of the Creek | Dog Decider
 5:30 pm | Craig of the Creek | Bring Out Your Beast
 6:00 pm | Teen Titans Go! | Magic Man
 6:30 pm | Teen Titans Go! | The Titans Go Casual
 7:00 pm | Amazing World of Gumball | The News
 7:30 pm | Amazing World of Gumball | The Vase