# 2022-05-04
 8:00 pm | King Of the Hill | Dances With Dogs
 8:30 pm | King Of the Hill | The Son Also Roses
 9:00 pm | Bob's Burgers | Brunchsquatch
 9:30 pm | Bob's Burgers | The Silence of the Louise
10:00 pm | American Dad | Family Plan
10:30 pm | American Dad | The Long Bomb
11:00 pm | American Dad | Kloger
11:30 pm | Rick and Morty | Mort Dinner Rick Andre
12:00 am | Rick and Morty | Mortyplicity
12:30 am | Rick and Morty | A Rickconvenient Mort
 1:00 am | Rick and Morty | Rickdependence Spray
 1:30 am | Rick and Morty | Amortycan Grickfitti
 2:00 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 2:30 am | Futurama | Bender's Big Score Part 3
 3:00 am | Futurama | Bender's Big Score Part 4
 3:30 am | Joe Pera Talks With You | Joe Pera Shows You Iron
 3:45 am | Joe Pera Talks With You | Joe Pera Takes You to Breakfast
 4:00 am | Bob's Burgers | Brunchsquatch
 4:30 am | Bob's Burgers | The Silence of the Louise
 5:00 am | King Of the Hill | Dances With Dogs
 5:30 am | King Of the Hill | The Son Also Roses