# 2022-05-04
 6:00 am | Mush Mush and the Mushables | Get the Egg Home; Suprise; It's Spring
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Art; Pocoyo Goes to School; Upside Down
 7:50 am | Love Monster | Day After Woohoo Day; Invent Something New Day
 8:10 am | Bing | Shadow; Bake
 8:30 am | Caillou | Caillous Sleepover Guest, Games in the Park, The Sugar Shack, Winter Mystery, Caillou's Snow Day
 9:00 am | Thomas & Friends: All Engines Go | An Ghost Train; Unbeleafable Day
 9:30 am | Thomas & Friends: All Engines Go | The Dragon Run Biggest Adventure Club
10:00 am | Mush Mush and the Mushables | Fly; Chase; Fly; Who's Eating Mushton
10:30 am | Baby Looney Tunes | Daffy Did It! / Foghorn's Talking' in the Barnyard (Song) / Pig Who Cried Woof
11:00 am | Craig of the Creek | Plush Kingdom; Fall Anthology
11:30 am | Craig of the Creek | Ice Pop Trio; The Afterschool Snackdown
12:00 pm | Craig of the Creek | Pencil Break Mania; Creature Feature
12:30 pm | Teen Titans Go! | Beast Boy's That's What's Up
 1:30 pm | Teen Titans Go! | Bat Scouts Bucket List
 2:00 pm | Amazing World of Gumball | The Fraud; The Void
 2:30 pm | Amazing World of Gumball | The Move; The Boss
 3:00 pm | Amazing World of Gumball | The Allergy; The Law
 3:30 pm | Amazing World of Gumball | The Mothers / The Password
 4:00 pm | Amazing World Of Gumball | The Web; The Mess
 4:30 pm | Amazing World Of Gumball | The Heart; The Revolt
 5:00 pm | We Baby Bears | Modern-Ish Stone Age Family; Excalibear
 5:30 pm | We Baby Bears | Meat House; Pirate Parrot Polly
 6:00 pm | We Baby Bears | Dragon Pests
 6:15 pm | We Baby Bears | Ice Bear's Pet
 6:30 pm | Teen Titans Go! | Free Perk; Go!
 7:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro