# 2022-05-12
 8:00 pm | King Of the Hill | Night and Deity
 8:30 pm | King Of the Hill | Maid in Arlen
 9:00 pm | Bob's Burgers | Sleeping with the Frenemy
 9:30 pm | Bob's Burgers | The Hurt Soccer
10:00 pm | American Dad | Persona Assistant
10:30 pm | American Dad | The Legend of Old Ulysses
11:00 pm | American Dad | Twinanigans
11:30 pm | Rick and Morty | Meeseeks and Destroy
12:00 am | The Boondocks | Stinkmeaner 3: The Hateocracy
12:30 am | Robot Chicken | May Cause Weebles to Fall Down			
12:45 am | Robot Chicken | May Cause Season 11 to End			
 1:00 am | Aqua Teen | Mayhem of the Mooninites
 1:15 am | Aqua Teen | MC Pee Pants
 1:30 am | Rick and Morty | Meeseeks and Destroy
 2:00 am | Futurama | Into the Wild Green Yonder Part 1
 2:30 am | Futurama | Into the Wild Green Yonder Part 2
 3:00 am | Robot Chicken | May Cause Weebles to Fall Down			
 3:15 am | Robot Chicken | May Cause Season 11 to End			
 3:30 am | Aqua Teen | Mayhem of the Mooninites
 3:45 am | Aqua Teen | MC Pee Pants
 4:00 am | Bob's Burgers | Sleeping with the Frenemy
 4:30 am | Bob's Burgers | The Hurt Soccer
 5:00 am | King Of the Hill | Night and Deity
 5:30 am | King Of the Hill | Maid in Arlen