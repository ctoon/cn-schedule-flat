# 2022-05-12
 6:00 am | Mush-Mush and the Mushables | Fool the Forest
 6:30 am | Cocomelon | The Ant and the Grasshopper
 7:00 am | Cocomelon | Traffic Safety Song
 7:30 am | Cocomelon | Five Little Monkeys Jumping on the Bed
 8:00 am | Pocoyo | Scary Noises
 8:20 am | Bing | Growing
 8:30 am | Thomas & Friends: All Engines Go | Nia's Balloon Blunder
 9:00 am | Mecha Builders | Picture Perfect Park Party/Sun Block
 9:30 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
10:00 am | Mush-Mush and the Mushables | Race The Beast
10:30 am | Baby Looney Tunes | Wise Quacker; D-a-f-f-y (song); Yours, Mine...and Mine, Mine!
11:00 am | Craig of the Creek | The Sparkle Solution
11:30 am | Craig of the Creek | Better Than You
12:00 pm | Craig of the Creek | The Dream Team
12:30 pm | Teen Titans Go! | I Am Chair
 1:00 pm | Teen Titans Go! | Bumgorf
 1:30 pm | Teen Titans Go! | The Mug
 2:00 pm | Amazing World of Gumball | The Origins
 2:30 pm | Amazing World of Gumball | The Traitor
 3:00 pm | Amazing World of Gumball | The Advice
 3:30 pm | Amazing World of Gumball | The Signal
 4:00 pm | We Baby Bears | Baba Yaga House
 4:30 pm | Craig of the Creek | Fan or Foe
 5:00 pm | Craig of the Creek | New Jersey
 5:30 pm | Craig of the Creek | The Sunflower
 6:00 pm | Teen Titans Go! | Baby Mouth
 6:30 pm | Teen Titans Go! | Lucky Stars
 7:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 7:30 pm | Amazing World of Gumball | The Downer; The Egg