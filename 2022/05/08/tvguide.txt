# 2022-05-08
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Mother's Day; the; Heart
 6:30 am | Amazing World of Gumball | The Brain; The; Founder
 7:00 am | Amazing World of Gumball | The Money; The Return
 7:30 am | Amazing World of Gumball | The Oracle; The Egg
 8:00 am | Amazing World of Gumball | The Pizza; The Question
 8:30 am | Amazing World of Gumball | The Procrastinators; The Password
 9:00 am | Amazing World of Gumball | The Finale; The Castle
 9:30 am | Amazing World of Gumball | The Knights; The Authority
10:00 am | Amazing World of Gumball | The Gumball Chronicles: Mother's Day; the Fight
10:30 am | Amazing World of Gumball | The Quest; The Club
11:00 am | Amazing World of Gumball | The Signature; The Parking
11:30 am | Amazing World of Gumball | The Roots; The; Nest
12:00 pm | Amazing World of Gumball | The Copycats; The Outside
12:30 pm | Amazing World of Gumball | The Box; The Vase
 1:00 pm | Amazing World of Gumball | The Worst; The Heist
 1:30 pm | Amazing World of Gumball | The Nuisance; The Deal
 2:00 pm | Amazing World of Gumball | The Gumball Chronicles: Mother's Day; the; Line
 2:30 pm | Amazing World of Gumball | The Master; The Possession
 3:00 pm | Amazing World of Gumball | The Mothers; The; Web
 3:30 pm | Amazing World of Gumball | The Limit; The Gi
 4:00 pm | Amazing World of Gumball | The Fridge; The Dvd
 4:30 pm | Amazing World of Gumball | The Parents; The; Factory
 5:00 pm | Amazing World of Gumball | The Fuss; The List
 5:30 pm | Amazing World of Gumball | The Fury; The; Gumball Chronicles: Mother's Day
 6:00 pm | Aquaman | 