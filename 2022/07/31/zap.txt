# 2022-07-31
 6:00 am | Teen Titans Go! | Bat Scouts
 6:30 am | Teen Titans Go! | Magic Man
 7:00 am | Teen Titans | X
 7:30 am | Teen Titans | Betrothed
 8:00 am | Teen Titans Go! | That's What's Up
 9:00 am | Victor and Valentino | Decoding Valentino
 9:30 am | Victor and Valentino | Journey to Maiz Mountains, Part 1
10:00 am | Victor and Valentino | Los Perdidos
10:30 am | Victor and Valentino | Get Your Sea Legs
11:00 am | We Baby Bears | High Baby Fashion
11:30 am | We Baby Bears | A Gross Worm
12:00 pm | Total DramaRama | Be Claws I Love You, Shelley
12:30 pm | Total DramaRama | The Doomed Ballooned Marooned
 1:00 pm | We Bare Bears | Best Bears
 1:30 pm | We Bare Bears | Bear Squad
 2:00 pm | We Baby Bears | High Baby Fashion
 2:30 pm | We Baby Bears | A Gross Worm
 3:00 pm | Total DramaRama | Snots Landing
 3:30 pm | Total DramaRama | The Fuss on the Bus
 4:00 pm | Total DramaRama | Oozing Talent
 4:30 pm | Total DramaRama | Be Claws I Love You, Shelley
 5:00 pm | Total DramaRama | The Doomed Ballooned Marooned
 5:30 pm | Total DramaRama | A Bridgette Too Far
 6:00 pm | Rampage | 