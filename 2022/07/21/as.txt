# 2022-07-21
 8:00 pm | King Of the Hill | Peggy's Fan Fair
 8:30 pm | King Of the Hill | The Perils of Polling
 9:00 pm | Bob's Burgers | Best Burger
 9:30 pm | Bob's Burgers | Father of the Bob
10:00 pm | American Dad | Toy Whorey
10:30 pm | American Dad | Love, American Dad Style
11:00 pm | American Dad | Killer Vacation
11:30 pm | Rick and Morty | M. Night Shaym-Aliens!
12:00 am | Genndy Tartakovsky's Primal | Sea of Despair
12:30 am | Genndy Tartakovsky's Primal | Shadow of Fate
 1:00 am | The Venture Brothers | The Lepidopterists
 1:30 am | Rick and Morty | M. Night Shaym-Aliens!
 2:00 am | Futurama | The Beast with a Billion Backs Part 1
 2:30 am | Futurama | The Beast with a Billion Backs Part 2
 3:00 am | Genndy Tartakovsky's Primal | Sea of Despair
 3:30 am | Genndy Tartakovsky's Primal | Shadow of Fate
 4:00 am | Bob's Burgers | Best Burger
 4:30 am | Bob's Burgers | Father of the Bob
 5:00 am | King Of the Hill | Peggy's Fan Fair
 5:30 am | King Of the Hill | The Perils of Polling