# 2022-07-21
 6:00 am | Let's Go Pocoyo | Pocoyo's Band
 6:30 am | Cocomelon | Getting Ready for School Song
 7:00 am | Cocomelon | Jobs and Career Song
 7:30 am | Cocomelon | Bingo Farm Version
 8:00 am | Lellobee City Farm | 5 Little Ducks - Playtime at the Farm!
 8:15 am | Blippi Wonders | Ducks
 8:30 am | Thomas & Friends: All Engines Go | Skiff Sails Sodor
 9:00 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 9:30 am | Thomas & Friends: All Engines Go | Letting Off Steam
10:00 am | Total DramaRama | The A-bok-bok-bokalypse
10:30 am | Total DramaRama | A Dame-gerous Game
11:00 am | We Baby Bears | Meat House
11:30 am | We Bare Bears | Pizza Band
12:00 pm | Teen Titans Go! | Chicken in the Cradle
12:30 pm | Teen Titans Go! | Quantum Fun
 1:00 pm | Craig of the Creek | Tea Timer's Ball
 1:30 pm | Craig of the Creek | The Children of the Dog
 2:00 pm | Craig of the Creek | Jessica Shorts
 2:30 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 3:30 pm | Amazing World of Gumball | The Pressure; The Painting
 4:00 pm | Teen Titans Go! | Career Day
 4:30 pm | Teen Titans Go! | TV Knight 2
 5:00 pm | We Baby Bears | Bears in the Dark
 5:30 pm | We Baby Bears | Triple T Tigers
 6:00 pm | We Baby Bears | A Real Crayon
 6:15 pm | Total DramaRama | Legends of the Paul
 6:30 pm | Total DramaRama | The Big Bangs Theory
 7:00 pm | Total DramaRama | Mad Math: Taffy Road
 7:30 pm | Total DramaRama | Bearly Edible