# 2022-07-19
 8:00 pm | King Of the Hill | Meet the Propaniacs
 8:30 pm | King Of the Hill | Nancy Boys
 9:00 pm | Bob's Burgers | Work Hard or Die Trying, Girl
 9:30 pm | Bob's Burgers | Tina and the Real Ghost
10:00 pm | American Dad | The Wrestler
10:30 pm | American Dad | Dr. Klaustus
11:00 pm | American Dad | Stan's Best Friend
11:30 pm | Rick and Morty | Lawnmower Dog
12:00 am | The Boondocks | Shinin'
12:30 am | Robot Chicken | May Cause Episode Title to Cut Off Due to Word Lim			
12:45 am | Robot Chicken | Happy Russian Deathdog Dolloween 2 U			
 1:00 am | The Venture Brothers | Tears of a Sea Cow
 1:30 am | Rick and Morty | Lawnmower Dog
 2:00 am | Futurama | Bender's Big Score Part 1
 2:30 am | Futurama | Bender's Big Score Part 2
 3:00 am | Robot Chicken | May Cause Episode Title to Cut Off Due to Word Lim			
 3:15 am | Robot Chicken | Happy Russian Deathdog Dolloween 2 U			
 3:30 am | The Venture Brothers | Tears of a Sea Cow
 4:00 am | Bob's Burgers | Work Hard or Die Trying, Girl
 4:30 am | Bob's Burgers | Tina and the Real Ghost
 5:00 am | King Of the Hill | Meet the Propaniacs
 5:30 am | King Of the Hill | Nancy Boys