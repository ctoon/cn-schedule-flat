# 2022-07-04
 6:00 am | Mecha Builders | The Need For Feed; Zipline Sisters Of Treetop Woods
 6:30 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 7:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 7:30 am | Mecha Builders | Yip Yip Book Book; Stick To It
 8:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 8:30 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 9:00 am | Mecha Builders | Magnet Mayhem; Stop That Train
 9:30 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
10:00 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob
10:30 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
11:00 am | Teen Titans Go! | Colors of Raven
11:30 am | Teen Titans Go! | Cool Uncles
12:00 pm | Teen Titans Go! | Demon Prom
12:30 pm | Teen Titans Go! | Dog Hand
 1:00 pm | Teen Titans Go! | I See You
 1:30 pm | Teen Titans Go! | Leg Day
 2:00 pm | Teen Titans Go! | Legs
 2:30 pm | Teen Titans Go! | Legendary Sandwich
 3:00 pm | Teen Titans Go! | Nean
 3:30 pm | Teen Titans Go! | The Overbite
 4:00 pm | Teen Titans Go! | Real Magic
 4:30 pm | Teen Titans Go! | Salty Codgers
 5:00 pm | Teen Titans Go! | The Power of Shrimps
 5:30 pm | Teen Titans Go! | A Farce
 6:00 pm | Teen Titans Go! | Super Hero Summer Camp
 7:00 pm | Teen Titans Go! | Communicate Openly
 7:30 pm | Teen Titans Go! | Strength of a Grown Man