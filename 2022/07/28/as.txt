# 2022-07-28
 8:00 pm | King Of the Hill | Hank's Choice
 8:30 pm | King Of the Hill | It's Not Easy Being Green
 9:00 pm | Bob's Burgers | The Gayle Tales
 9:30 pm | Bob's Burgers | Hawk & Chick
10:00 pm | American Dad | Steve and Snot's Test-Tubular Adventure
10:30 pm | American Dad | Poltergasm
11:00 pm | American Dad | Buck, Wild
11:30 pm | Rick and Morty | Vindicators 3: The Return of Worldender
12:00 am | Genndy Tartakovsky's Primal | Dawn of Man
12:30 am | Robot Chicken | Punctured Jugular
12:45 am | Robot Chicken | Poisoned by Relatives
 1:00 am | The Venture Brothers | Blood of the Father, Heart of Steel
 1:30 am | Rick and Morty | Vindicators 3: The Return of Worldender
 2:00 am | Futurama | Into the Wild Green Yonder Part 1
 2:30 am | Futurama | Into the Wild Green Yonder Part 2
 3:00 am | Genndy Tartakovsky's Primal | Dawn of Man
 3:30 am | The Venture Brothers | Blood of the Father, Heart of Steel
 4:00 am | Bob's Burgers | The Gayle Tales
 4:30 am | Bob's Burgers | Hawk & Chick
 5:00 am | King Of the Hill | Hank's Choice
 5:30 am | King Of the Hill | It's Not Easy Being Green