# 2022-07-28
 6:00 am | Pocoyo | Pato's Shower; Garden; the; Cinema
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Recycle Stomp; Yes Yes Vegetables Song - Healthy Habits! ; Wash Your Hands; Scrub That Soap; Shake the Apple Tree
 8:15 am | Blippi Wonders | Volcano; Honey; Australia Kangaroo; Bats
 8:30 am | Thomas & Friends: All Engines Go | Song of Sodor; Thomas' Promise
 9:00 am | Mecha Builders | Magnet Mayhemstop That Train
 9:30 am | Bugs Bunny Builders | Tweety-Go-Round; Snow Cap
10:00 am | Total DramaRama | Legends of the Paul; Cartoon Realism
10:30 am | Total DramaRama | Grody to the Maximum; Oww
11:00 am | We Baby Bears | High Baby Fashion; Teddi Bear
11:30 am | We Bare Bears | Imaginary Friend; Fire!
12:00 pm | Teen Titans Go! | 252 Super Summer Hero Camp; Walk Away
 1:00 pm | Craig of the Creek | In the Key of the Creek; Afterschool Snackdown
 1:30 pm | Craig of the Creek | Ground Is Lava! ; The; Creature Feature
 2:00 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind King of Camping
 2:30 pm | Amazing World of Gumball | The Fridge; The Remote
 3:00 pm | Amazing World of Gumball | The Flower; The Banana
 3:30 pm | Amazing World of Gumball | The Phone; The Job
 4:00 pm | Teen Titans Go! | Them Soviet Boys; Genie President
 4:30 pm | Teen Titans Go! | Little Elvis; Tall Titan Tales
 5:00 pm | Teen Titans Go! | Tv Knight 4; I Used To Be A Peoples
 5:30 pm | Teen Titans Go! | Metric System vs. Freedom; The Lil' Dimples
 6:00 pm | We Baby Bears | Tooth Fairy Tech; No Land; All Air!
 6:30 pm | Amazing World of Gumball | The Party; The Refund
 7:00 pm | Amazing World of Gumball | The Robot; The Picnic
 7:30 pm | Amazing World of Gumball | The Goons; The Secret