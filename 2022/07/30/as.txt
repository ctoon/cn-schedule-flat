# 2022-07-30
 8:00 pm | King Of the Hill | Soldier of Misfortune
 8:30 pm | King Of the Hill | Lupe's Revenge
 9:00 pm | Futurama | Fry and Leela's Big Fling
 9:30 pm | Futurama | T.: The Terrestrial
10:00 pm | American Dad | Faking Bad
10:30 pm | American Dad | Minstrel Krampus
11:00 pm | American Dad | Vision: Impossible
11:30 pm | Rick and Morty | Pickle Rick
12:00 am | Genndy Tartakovsky's Primal | Dawn of Man
12:30 am | Yashahime: Princess Half-Demon - The Second Act | Wielding the Tenseiga
 1:00 am | Lupin the 3rd Part 6 | The Mirage Women
 1:30 am | One Piece | The Secret Revealed! The Truth About the Ancient Weapon!
 2:00 am | One Piece | The Straw Hats Stunned! The New Fleet Admiral of the Navy!
 2:30 am | Naruto:Shippuden | Tenten's Troubles
 3:00 am | Shenmue: The Animation | Guidepost
 3:30 am | Attack on Titan | Retrospective
 4:00 am | Futurama | Fry and Leela's Big Fling
 4:30 am | Futurama | T.: The Terrestrial
 5:00 am | King Of the Hill | Soldier of Misfortune
 5:30 am | King Of the Hill | Lupe's Revenge