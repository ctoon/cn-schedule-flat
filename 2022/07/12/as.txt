# 2022-07-12
 8:00 pm | King Of the Hill | Little Horrors of Shop
 8:30 pm | King Of the Hill | Aisle 8A
 9:00 pm | Bob's Burgers | The Kids Rob a Train
 9:30 pm | Bob's Burgers | I Get Psy-Chic Out of You
10:00 pm | American Dad | I Am the Walrus
10:30 pm | American Dad | School Lies
11:00 pm | American Dad | License to Till
11:30 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
12:00 am | The Boondocks | Thank You for Not Snitching
12:30 am | Robot Chicken | May Cause Immaculate Conception			
12:45 am | Robot Chicken | May Cause The Exact Thing You're Taking This To Avoid			
 1:00 am | The Venture Brothers | Home Is Where the Hate Is
 1:30 am | Rick and Morty | Gotron Jerrysis Rickvangelion
 2:00 am | Futurama | The Sting
 2:30 am | Futurama | The Farnsworth Parabox
 3:00 am | Robot Chicken | May Cause Immaculate Conception			
 3:15 am | Robot Chicken | May Cause The Exact Thing You're Taking This To Avoid			
 3:30 am | The Venture Brothers | Home Is Where the Hate Is
 4:00 am | Bob's Burgers | The Kids Rob a Train
 4:30 am | Bob's Burgers | I Get Psy-Chic Out of You
 5:00 am | King Of the Hill | Little Horrors of Shop
 5:30 am | King Of the Hill | Aisle 8A