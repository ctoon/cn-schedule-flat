# 2022-07-25
 8:00 pm | King Of the Hill | Yankee Hankie
 8:30 pm | King Of the Hill | Hank and the Great Glass Elevator
 9:00 pm | Bob's Burgers | Tina Tailor Soldier Spy
 9:30 pm | Bob's Burgers | Midday Run
10:00 pm | American Dad | Blood Crieth Unto Heaven
10:30 pm | American Dad | Max Jets
11:00 pm | American Dad | Naked to the Limit, One More Time
11:30 pm | Rick and Morty | Rick Potion #9
12:00 am | The Boondocks | Home Alone
12:30 am | Robot Chicken | May Cause an Excess of Ham			
12:45 am | Robot Chicken | May Cause Internal Diarrhea			
 1:00 am | The Venture Brothers | Orb
 1:30 am | Rick and Morty | Rick Potion #9
 2:00 am | Futurama | The Beast with a Billion Backs Part 3
 2:30 am | Futurama | The Beast with a Billion Backs Part 4
 3:00 am | Robot Chicken | May Cause an Excess of Ham			
 3:15 am | Robot Chicken | May Cause Internal Diarrhea			
 3:30 am | The Venture Brothers | Orb
 4:00 am | Bob's Burgers | Tina Tailor Soldier Spy
 4:30 am | Bob's Burgers | Midday Run
 5:00 am | King Of the Hill | Yankee Hankie
 5:30 am | King Of the Hill | Hank and the Great Glass Elevator