# 2022-07-24
 8:00 pm | Bob's Burgers | Li'l Hard Dad
 8:30 pm | Bob's Burgers | Adventures in Chinchilla-Sitting
 9:00 pm | Futurama | Forty Percent Leadbelly
 9:30 pm | Futurama | The Inhuman Torch
10:00 pm | American Dad | Russian Doll
10:30 pm | American Dad | Dressed Down
11:00 pm | Rick and Morty | Vindicators 3: The Return of Worldender
11:30 pm | Rick and Morty | Pickle Rick
12:00 am | Tuca & Bertie | Leaf Raking
12:30 am | Birdgirl | Fli On Your Own Supply
 1:00 am | Smiling Friends | Who Violently Murdered Simon S. Salty?
 1:15 am | Smiling Friends | Enchanted Forest
 1:30 am | American Dad | Russian Doll
 2:00 am | American Dad | Dressed Down
 2:30 am | Rick and Morty | Vindicators 3: The Return of Worldender
 3:00 am | Rick and Morty | Pickle Rick
 3:30 am | Tuca & Bertie | Leaf Raking
 4:00 am | Futurama | Forty Percent Leadbelly
 4:30 am | Futurama | The Inhuman Torch
 5:00 am | King Of the Hill | 'Twas the Nut Before Christmas
 5:30 am | King Of the Hill | Chasing Bobby