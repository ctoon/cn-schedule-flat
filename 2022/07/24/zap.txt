# 2022-07-24
 6:00 am | Teen Titans Go! | Girls Night In
 6:30 am | Teen Titans Go! | Royal Jelly
 7:00 am | Teen Titans | Fractured
 7:30 am | Teen Titans | Deception
 8:00 am | Teen Titans Go! | Super Hero Summer Camp
 9:00 am | The Book of Life | 
11:00 am | We Baby Bears | Bug City Sleuths
11:30 am | We Baby Bears | Snow Place Like Home
12:00 pm | Total DramaRama | From Badge to Worse
12:30 pm | Total DramaRama | All Up in Your Drill
 1:00 pm | Adventure Time | Slumber Party Panic
 1:30 pm | Adventure Time | Prisoners of Love
 2:00 pm | Craig of the Creek | Scoutguest
 2:30 pm | Craig of the Creek | Creek Talent Extravaganza
 3:00 pm | Craig of the Creek | Dodgy Decisions
 3:30 pm | Craig of the Creek | Hyde & Zeke
 4:00 pm | Craig of the Creek | The Anniversary Box
 4:30 pm | Craig of the Creek | Silver Fist Returns
 5:00 pm | Craig of the Creek | The Champion's Hike
 5:30 pm | Craig of the Creek | Scoutguest
 6:00 pm | Journey 2: The Mysterious Island | 