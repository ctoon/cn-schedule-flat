# 2022-07-18
 6:00 am | Pocoyo | Robot Pocoyo; Pink Perfume; the; Face Painting
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | No Monsters Scared of the Dark; Old Macdonald; Silly Little Song; the; Salad Salsa
 8:15 am | Blippi Wonders | Garbage Truck; Bats; Honey; Ducks
 8:30 am | Thomas & Friends: All Engines Go | Real Number One; The; Tyrannosaurus Wrecks
 9:00 am | Mecha Builders | Yip Yip Book Bookstick to It
 9:30 am | Thomas & Friends: All Engines Go | The Roller Coasting; Super-Long Shortcut
10:00 am | Total DramaRama | Stink. Stank. Stunk; Robo Teacher
10:30 am | Total DramaRama | The Tooth About Zombies; Glove Glove Me Do
11:00 am | We Baby Bears | Magical Box; The; Bears and the Beanstalk
11:30 am | We Bare Bears | Rescue Ranger; Wingmen
12:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 1:00 pm | Craig Of The Creek | Sparkle Cadet; The Shortcut
 1:30 pm | Craig of the Creek | Deep Creek Salvage; Stink Bomb
 2:00 pm | Craig of the Creek | Dibs Court; Summer Wish
 2:30 pm | Amazing World Of Gumball | The Slip; The Revolt
 3:00 pm | Amazing World Of Gumball | The Drama; The Mess
 3:30 pm | Amazing World of Gumball | The Possession; The Buddy
 4:00 pm | Teen Titans Go! | TV Knight 3; My Name Is Jose
 4:30 pm | Teen Titans Go! | The Scoop; The Power of Shrimps
 5:00 pm | We Baby Bears | Ice Bear's Pet; Dragon Pests
 5:30 pm | We Baby Bears | Tooth Fairy Tech; No Land; All Air!
 6:00 pm | We Baby Bears | High Baby Fashion
 6:15 pm | Total DramaRama | Be Claws I Love You, Shelley
 6:30 pm | Total DramaRama | Van Hogling; The Opening Act
 7:00 pm | Total DramaRama | Ticking Crime Bomb; Knit Wit
 7:30 pm | Total DramaRama | The Fuss on the Bus; The; Cone-Versation