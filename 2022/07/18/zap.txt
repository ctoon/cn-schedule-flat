# 2022-07-18
 6:00 am | Pocoyo | Robot Pocoyo
 6:30 am | Cocomelon | Johny Johny Yes Papa
 7:00 am | Cocomelon | Balloon Boat Race
 7:30 am | Cocomelon | Tap Dancing Song
 8:00 am | Lellobee City Farm | No Monsters Scared of the Dark
 8:15 am | Blippi Wonders | Garbage Truck
 8:30 am | Thomas & Friends: All Engines Go | The Real Number One
 9:00 am | Mecha Builders | Yip Yip Book Book; Stick To It
 9:30 am | Thomas & Friends: All Engines Go | Roller Coasting
10:00 am | Total DramaRama | Stink. Stank. Stunk
10:30 am | Total DramaRama | Glove Glove Me Do
11:00 am | We Baby Bears | The Magical Box
11:30 am | We Bare Bears | Rescue Ranger
12:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 1:00 pm | Craig of the Creek | The Shortcut
 1:30 pm | Craig of the Creek | Deep Creek Salvage
 2:00 pm | Craig of the Creek | Dibs Court
 2:30 pm | Amazing World of Gumball | The Slip
 3:00 pm | Amazing World of Gumball | The Drama
 3:30 pm | Amazing World of Gumball | The Buddy
 4:00 pm | Teen Titans Go! | TV Knight 3
 4:30 pm | Teen Titans Go! | The Scoop
 5:00 pm | We Baby Bears | Ice Bear's Pet
 5:30 pm | We Baby Bears | No Land, All Air!
 6:00 pm | We Baby Bears | High Baby Fashion
 6:15 pm | Total DramaRama | Be Claws I Love You, Shelley
 6:30 pm | Total DramaRama | The Opening Act
 7:00 pm | Total DramaRama | Ticking Crime Bomb
 7:30 pm | Total DramaRama | The Fuss on the Bus