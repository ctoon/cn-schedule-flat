# 2022-07-16
 6:00 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 6:30 am | Teen Titans Go! | Business Ethics Wink Wink
 7:00 am | Teen Titans | Fear Itself
 7:30 am | Teen Titans | Date With Destiny
 8:00 am | Teen Titans Go! | Think About Your Future
 8:30 am | Teen Titans Go! | I Used to Be a Peoples
 9:00 am | We Baby Bears | Hashtag Number One Fan
 9:30 am | We Baby Bears | Snow Place Like Home
10:00 am | Total DramaRama | Ant We All Just Get Along
10:30 am | Total DramaRama | Not Without My Fudgy Lumps
11:00 am | Total DramaRama | The Bad Guy Busters
11:30 am | Total DramaRama | Dissing Cousins
12:00 pm | Total DramaRama | Duck Duck Juice
12:30 pm | Total DramaRama | Mad Math: Taffy Road
 1:00 pm | We Bare Bears | Road Trip
 1:30 pm | We Bare Bears | Chloe and Ice Bear
 2:00 pm | Total DramaRama | Simons Are Forever
 2:30 pm | Total DramaRama | Ice Guys Finish Last
 3:00 pm | We Bare Bears | Icy Nights
 3:30 pm | We Bare Bears | Charlie and the Snake
 4:00 pm | Total DramaRama | Cartoon Realism
 4:30 pm | Total DramaRama | Germ Factory
 5:00 pm | Total DramaRama | That's a Wrap
 5:30 pm | Total DramaRama | Snack to the Future
 6:00 pm | Garfield | 