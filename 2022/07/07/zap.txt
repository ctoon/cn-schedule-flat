# 2022-07-07
 6:00 am | Pocoyo | Poczilla
 6:30 am | Cocomelon | Basketball Song
 7:00 am | Cocomelon | Row Row Row Your Boat
 7:30 am | Cocomelon | Five Senses Song
 8:00 am | Lellobee City Farm | ABC Song - Learn Phonics
 8:15 am | Blippi Wonders | First Car
 8:30 am | Thomas & Friends: All Engines Go | Sir Topham Hatt's Hat
 9:00 am | Mecha Builders | Mecha Builders Pull Together; Lift Up and Lift Off
 9:30 am | Thomas & Friends: All Engines Go | Kana Goes Slow
10:00 am | Craig of the Creek | The Chef's Challenge
10:30 am | Craig of the Creek | The Quick Name
11:00 am | Craig of the Creek | The Sparkle Solution
11:30 am | Craig of the Creek | Chrono Moss
12:00 pm | Teen Titans Go! | The Fourth Wall
12:30 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 1:00 pm | Teen Titans Go! | Grube's Fairytales
 1:30 pm | Teen Titans Go! | A Farce
 2:00 pm | Amazing World of Gumball | The Test
 2:30 pm | Amazing World of Gumball | The Slide
 3:00 pm | Amazing World of Gumball | The Loophole
 3:30 pm | Amazing World of Gumball | The Fuss
 4:00 pm | We Baby Bears | Boo-Dunnit
 4:30 pm | Craig of the Creek | Winter Creeklympics
 5:00 pm | Craig of the Creek | Welcome to Creek Street
 5:30 pm | Craig of the Creek | Breaking the Ice
 6:00 pm | Teen Titans Go! | Video Game References; Cool School
 6:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:00 pm | Amazing World of Gumball | The Signal
 7:30 pm | Amazing World of Gumball | The Girlfriend