# 2022-07-02
 6:00 am | Amazing World of Gumball | The Heart
 6:30 am | Amazing World of Gumball | The Founder
 7:00 am | Amazing World of Gumball | The Money
 7:30 am | Amazing World of Gumball | The Pizza
 8:00 am | Amazing World of Gumball | The Egg
 8:30 am | Amazing World of Gumball | The Castle
 9:00 am | Amazing World of Gumball | The Knights
 9:30 am | Amazing World of Gumball | The Fight
10:00 am | Amazing World of Gumball | The Club
10:30 am | Amazing World of Gumball | The Signature
11:00 am | Amazing World of Gumball | The Nest
11:30 am | Amazing World of Gumball | The Possession
12:00 pm | Teen Titans Go! | Breakfast Cheese
12:30 pm | Teen Titans Go! | Business Ethics Wink Wink
 1:00 pm | Teen Titans Go! | Little Buddies
 1:30 pm | Teen Titans Go! | Fish Water
 2:00 pm | Teen Titans Go! | Hand Zombie
 2:30 pm | Teen Titans Go! | Kryptonite
 3:00 pm | Teen Titans Go! | Snuggle Time
 3:30 pm | Teen Titans Go! | Mr. Butt
 4:00 pm | Teen Titans Go! | Record Book
 4:30 pm | Teen Titans Go! | Starfire the Terrible
 5:00 pm | Teen Titans Go! | Oregon Trail
 5:30 pm | Teen Titans Go! | Thumb War
 6:00 pm | Gulliver's Travels | 
 7:45 pm | Teen Titans Go! | Girls' Night Out