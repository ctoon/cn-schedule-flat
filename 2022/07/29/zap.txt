# 2022-07-29
 6:00 am | Pocoyo | Pocoyo's Balloon
 6:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 7:00 am | Cocomelon | Bingo Farm Version
 7:30 am | Cocomelon | Jobs and Career Song
 8:00 am | Lellobee City Farm | ABC Song - Learn Phonics
 8:15 am | Blippi Wonders | Mountain
 8:30 am | Thomas & Friends: All Engines Go | Sir Topham Hatt's Hat
 9:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 9:30 am | Bugs Bunny Builders | Play Day
10:00 am | Total DramaRama | A Bridgette Too Far
10:30 am | Total DramaRama | Wiggin' Out
11:00 am | We Baby Bears | A Gross Worm
11:30 am | We Bare Bears | Braces
12:00 pm | Teen Titans Go! | That's What's Up
 1:00 pm | Craig of the Creek | I Don't Need a Hat
 1:30 pm | Craig of the Creek | Alternate Creekiverse
 2:00 pm | Craig of the Creek | Snow Day
 2:30 pm | Amazing World of Gumball | The Watch; The Bet
 3:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 3:30 pm | Amazing World of Gumball | The Authority; The Virus
 4:00 pm | Teen Titans Go! | Don't Be an Icarus
 4:30 pm | Teen Titans Go! | Stockton, CA!
 5:00 pm | Teen Titans Go! | What's Opera, Titans?
 5:30 pm | Teen Titans Go! | Had to Be There
 6:00 pm | Gulliver's Travels | 