# 2022-06-23
 6:00 am | Mush Mush and the Mushables | Frog of a Problem; A; My Muddy Buddy
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Lellobee City Farms | Penguin Dance; The; Recycle Stomp; Bath Song; Old Macdonald
 7:45 am | Blippi Wonders | Crane; Birds Nest; Honey; Mountain
 8:00 am | Pocoyo | Sneaky Shoes; Shutterbug
 8:20 am | Bing | Swimming Pool
 8:30 am | Thomas & Friends: All Engines Go | Goodbye; Ghost-Scaring Machine; Tyrannosaurus Wrecks
 9:00 am | Mecha Builders | That Ol' Time Rock Rolls! Make Like a Banana and Split
 9:30 am | Thomas & Friends: All Engines Go | The Light Delivery; A Hay; Now! ; Super-Long Shortcut
10:00 am | Craig of the Creek | Mystery of the Timekeeper; The Summer Wish
10:30 am | Craig Of The Creek | Turning The Tables; Alone Quest
11:00 am | Craig of the Creek | Memories of Bobby; Kelsey the Author
11:30 am | Craig of the Creek | Jacob of the Creek; Council of the Creek
12:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
12:30 pm | Teen Titans Go! | No Power; Sidekick
 1:00 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 1:30 pm | Teen Titans Go! | Legs; Breakfast Cheese
 2:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 2:30 pm | Amazing World of Gumball | The Fraud; The Void
 3:00 pm | Amazing World of Gumball | The Move; The Boss
 3:30 pm | Amazing World of Gumball | The Allergy; The Law
 4:00 pm | We Baby Bears | Bug City Sleuths; Snow Place Like Home
 4:30 pm | Craig of the Creek | Escape From Family Dinner; Vulture's Nest
 5:00 pm | Craig of the Creek | Monster in the Garden; Kelsey Quest
 5:30 pm | Craig of the Creek | The Curse; Jpony
 6:00 pm | Teen Titans Go! | Dude Relax!; Laundry Day
 6:30 pm | Teen Titans Go! | Ghostboy; La Larva de Amor
 7:00 pm | Amazing World of Gumball | The Hero; The Photo
 7:30 pm | Amazing World of Gumball | The Tag; The Lesson