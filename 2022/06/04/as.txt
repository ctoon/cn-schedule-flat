# 2022-06-04
 8:00 pm | King Of the Hill | SerPUNt
 8:30 pm | King Of the Hill | Blood and Sauce
 9:00 pm | Futurama | Over Clock Wise
 9:30 pm | Futurama | Reincarnation
10:00 pm | American Dad | All About Steve
10:30 pm | American Dad | Con Heir
11:00 pm | American Dad | G-String Circus
11:30 pm | Rick and Morty | Amortycan Grickfitti
12:00 am | Genndy Tartakovsky's Primal | Terror Under the Blood Moon
12:30 am | Assassination Classroom | Graduation Time
 1:00 am | Lupin the 3rd Part 6 | An Untold Tale
 1:30 am | One Piece | Deadly Attacks One After Another! Zoro and Sanji Join the Battle!
 2:00 am | One Piece | Unveiled! The Secret Weapons of the Sunny!
 2:30 am | Naruto:Shippuden | A True Ending
 3:00 am | Shenmue: The Animation | Equal
 3:30 am | Attack on Titan | Memories of the Future
 4:00 am | Futurama | Over Clock Wise
 4:30 am | Futurama | Reincarnation
 5:00 am | King Of the Hill | SerPUNt
 5:30 am | King Of the Hill | Blood and Sauce