# 2022-06-05
 8:00 pm | Bob's Burgers | Slumber Party
 8:30 pm | Bob's Burgers | Friends with Burger-Fits
 9:00 pm | Futurama | Decision 3012
 9:30 pm | Futurama | The Thief of Baghead
10:00 pm | American Dad | The Sinister Fate!!
10:30 pm | American Dad | The Book of Fischer
11:00 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
11:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
12:00 am | Birdgirl | Topple the Popple
12:30 am | Tuca & Bertie | The Moss
 1:00 am | Three Busy Debras | The Milk Drought
 1:15 am | Three Busy Debras | The Great Debpression
 1:30 am | American Dad | The Sinister Fate!!
 2:00 am | American Dad | The Book of Fischer
 2:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | Birdgirl | Topple the Popple
 4:00 am | Futurama | Decision 3012
 4:30 am | Futurama | The Thief of Baghead
 5:00 am | King Of the Hill | Luanne Gets Lucky
 5:30 am | King Of the Hill | Hank Gets Dusted