# 2022-06-28
 6:00 am | Pocoyo | Angry Alien
 6:30 am | Cocomelon | Five Senses Song
 7:00 am | Cocomelon | The Hiccup Song
 7:30 am | Cocomelon | Basketball Song
 8:00 am | Lellobee City Farm | Accidents Happen Boo Boo Song
 8:15 am | Blippi Wonders | Volcano
 8:30 am | Thomas & Friends: All Engines Go | Letting Off Steam
 9:00 am | Mecha Builders | Magnet Mayhem; Stop That Train
 9:30 am | Thomas & Friends: All Engines Go | Nia's Perfect Plan
10:00 am | Craig of the Creek | The Evolution of Craig
10:30 am | Craig of the Creek | Creek Shorts
11:00 am | Craig of the Creek | Tea Timer's Ball
11:30 am | Craig of the Creek | The Cardboard Identity
12:00 pm | Teen Titans Go! | Pirates; I See You
12:30 pm | Teen Titans Go! | Brian; Nature
 1:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
 1:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 2:00 pm | Amazing World of Gumball | The Society; The Spoiler
 2:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 3:00 pm | Amazing World of Gumball | The Downer; The Egg
 3:30 pm | Amazing World of Gumball | The Triangle; The Money
 4:00 pm | We Baby Bears | Bears in the Dark
 4:30 pm | Craig of the Creek | Beyond the Rapids
 5:00 pm | Craig of the Creek | The Jinxening
 5:30 pm | Craig of the Creek | In the Key of the Creek
 6:00 pm | Steven Universe | Friend Ship
 6:30 pm | Steven Universe | When It Rains
 7:00 pm | Steven Universe | Too Far
 7:30 pm | Steven Universe | Message Received