# 2022-06-22
 6:00 am | Mush Mush and the Mushables | Fool the Forest; Suprise; It's Spring
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Lellobee City Farms | Silly Little Song; The; Splash Song; O'little Tree; Happy Place
 7:45 am | Blippi Wonders | Bats; Ducks; Ocean; Panda
 8:00 am | Pocoyo | Summer Hike; Sleep Guard
 8:20 am | Bing | Bubbles
 8:30 am | Thomas & Friends: All Engines Go | New View for Thomas; A; Sir Topham Hatt's Hat
 9:00 am | Mecha Builders | Picture Perfect Park Partysun Block
 9:30 am | Thomas & Friends: All Engines Go | Eggsellent Adventure; Calliope Crack-Up
10:00 am | Craig Of The Creek | The Kid From 3030; Deep Creek Salvage
10:30 am | Craig of the Creek | The Power Punchers Shortcut
11:00 am | Craig Of The Creek | Dibs Court; Creek Cart Racers
11:30 am | Craig Of The Creek | The Great Fossil Rush; Secret Book Club
12:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
12:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 1:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 1:30 pm | Teen Titans Go! | Books; Lazy Sunday
 2:00 pm | Amazing World of Gumball | The Kids; The Fan
 2:30 pm | Amazing World of Gumball | The Coach; The Joy
 3:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 3:30 pm | Amazing World of Gumball | The Name; The Extras
 4:00 pm | We Baby Bears | Pirate Parrot Polly; Baby Bear Genius
 4:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers; Sparkle Cadet
 5:00 pm | Craig of the Creek | Kelsey the Elder; Cousin of the Creek
 5:30 pm | Craig of the Creek | Fort Williams Sour Candy Trials
 6:00 pm | Teen Titans Go! | Birds; Be Mine
 6:30 pm | Teen Titans Go! | Brain Food; In and Out
 7:00 pm | Amazing World of Gumball | The Mothers / The Password
 7:30 pm | Amazing World of Gumball | The Procrastinators; The Shell