# 2022-06-21
 8:00 pm | King Of the Hill | A Bill Full Of Dollars
 8:30 pm | King Of the Hill | Straight as an Arrow
 9:00 pm | Bob's Burgers | My Fuzzy Valentine
 9:30 pm | Bob's Burgers | Lindapendent Woman
10:00 pm | American Dad | Oedipal Panties
10:30 pm | American Dad | Widowmaker
11:00 pm | American Dad | Red October Sky
11:30 pm | Rick and Morty | The Old Man and the Seat
12:00 am | The Boondocks | The Real
12:30 am | Robot Chicken | No Wait, He Has a Cane
12:45 am | Robot Chicken | Hi.
 1:00 am | The Venture Brothers | The Venture Bros. & the Curse of the Haunted Problem
 1:30 am | Rick and Morty | The Old Man and the Seat
 2:00 am | Futurama | Insane in the Mainframe
 2:30 am | Futurama | Bendin' in the Wind
 3:00 am | Robot Chicken | No Wait, He Has a Cane
 3:15 am | Robot Chicken | Hi.
 3:30 am | The Venture Brothers | The Venture Bros. & the Curse of the Haunted Problem
 4:00 am | Bob's Burgers | My Fuzzy Valentine
 4:30 am | Bob's Burgers | Lindapendent Woman
 5:00 am | King Of the Hill | A Bill Full Of Dollars
 5:30 am | King Of the Hill | Straight as an Arrow