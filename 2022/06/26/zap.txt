# 2022-06-26
 6:00 am | Teen Titans Go! | Beast Girl
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:00 am | Teen Titans | The Apprentice
 7:30 am | Teen Titans | The Apprentice
 8:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular, Part 1
 8:30 am | Teen Titans Go! | Flashback
 9:00 am | We Baby Bears | No Land, All Air!
 9:30 am | We Baby Bears | Tooth Fairy Tech
10:00 am | Total DramaRama | Virtual Reality Bites
10:30 am | Total DramaRama | The Opening Act
11:00 am | Teen Titans Go! | A Farce
11:30 am | Teen Titans Go! | BL4Z3
12:00 pm | Teen Titans Go! | Booty Scooty
12:30 pm | Teen Titans Go! | Cartoon Feud
 1:00 pm | Teen Titans Go! | Don't Press Play
 1:30 pm | Teen Titans Go! | Had to Be There
 2:00 pm | Teen Titans Go! | Huggbees
 2:30 pm | Teen Titans Go! | Little Elvis
 3:00 pm | Teen Titans Go! | A Little Help Please
 3:30 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 4:00 pm | Teen Titans Go! | DC
 4:30 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 5:00 pm | Teen Titans Go! | EEbows
 5:30 pm | Teen Titans Go! | Fat Cats
 6:00 pm | Men in Black II | 
 7:45 pm | Wipeout | Tumble Sleep Eat Repeat