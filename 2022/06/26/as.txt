# 2022-06-26
 8:00 pm | Bob's Burgers | Work Hard or Die Trying, Girl
 8:30 pm | Bob's Burgers | Tina Tailor Soldier Spy
 9:00 pm | Futurama | Free Will Hunting
 9:30 pm | Futurama | Near-Death Wish
10:00 pm | American Dad | Cry Baby
10:30 pm | American Dad | American Dad Graffito
11:00 pm | Rick and Morty | The Vat of Acid Episode
11:30 pm | Birdgirl | Fli On Your Own Supply
12:00 am | Birdgirl | The Wanky
12:30 am | Tuca & Bertie | The Dance
 1:00 am | Three Busy Debras | Debra Gets A Boyfriend
 1:15 am | Three Busy Debras | Women's History Hour
 1:30 am | American Dad | Cry Baby
 2:00 am | American Dad | American Dad Graffito
 2:30 am | Rick and Morty | The Vat of Acid Episode
 3:00 am | Birdgirl | Fli On Your Own Supply
 3:30 am | Birdgirl | The Wanky
 4:00 am | Futurama | Free Will Hunting
 4:30 am | Futurama | Near-Death Wish
 5:00 am | King Of the Hill | Uh-Oh Canada
 5:30 am | King Of the Hill | The Boy Can't Help It