# 2022-06-25
 6:00 am | Teen Titans Go! | Hot Salad Water; Labor Day
 6:30 am | Teen Titans Go! | Brain Percentages; Ones and Zeros
 7:00 am | Teen Titans | Mad Mod
 7:30 am | Teen Titans | Car Trouble
 8:00 am | Teen Titans Go! | Bl4z3; Career Day
 8:30 am | Teen Titans Go! | Lication; Tv Knight 2
 9:00 am | We Baby Bears | Dragon Pests; Snow Place Like Home
 9:30 am | We Baby Bears | Ice Bear's Pet; Bears and the Beanstalk
10:00 am | Total DramaRama | Daycare of Rock; Van Hogling
10:30 am | Total DramaRama | Aches and Ladders; Venthalla
11:00 am | Total DramaRama | The Cactus Makes Perfect; Date
11:30 am | We Baby Bears | Sheep Bears; Fiesta Day
12:00 pm | Teen Titans Go! | Throne of Bones; The Academy
12:30 pm | Teen Titans Go! | Demon Prom; TV Knight 3
 1:00 pm | Teen Titans Go! | Bbcyfshipbday Bro-Pocalypse
 1:30 pm | We Baby Bears | Hashtag; Fan; Boo-Dunnit
 2:00 pm | Craig of the Creek | Big Pinchy; Creek Cart Racers
 2:30 pm | Craig of the Creek | Kid From 3030; The; Secret Book Club
 3:00 pm | Craig of the Creek | Power Punchers; Jextra Perrestrial
 3:30 pm | We Baby Bears | Bug City Sleuths; Triple T Tigers
 4:00 pm | Amazing World of Gumball | The Origins; The Origins Part 2
 4:30 pm | Amazing World of Gumball | The Signal; The Girlfriend
 5:00 pm | Amazing World of Gumball | The Traitor; The; Parasite
 5:30 pm | Amazing World of Gumball | The Love; The Advice
 6:00 pm | Teen Titans Go! Vs. Teen Titans | 
 7:45 pm | Teen Titans Go! | Mo' Money Mo' Problems; The Scoop