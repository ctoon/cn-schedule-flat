# 2022-06-06
 8:00 pm | King Of the Hill | Glen Peggy Glen Ross
 8:30 pm | King Of the Hill | The Passion of Dauterive
 9:00 pm | Bob's Burgers | Synchronized Swimming
 9:30 pm | Bob's Burgers | Burgerboss
10:00 pm | American Dad | Stan of Arabia Part 1
10:30 pm | American Dad | Stan of Arabia Part 2
11:00 pm | American Dad | Star Trek
11:30 pm | Rick and Morty | The Rickshank Rickdemption
12:00 am | The Boondocks | I Dream of Siri
12:30 am | The Venture Brothers | Hostile Makeover
 1:00 am | Robot Chicken | Scoot to the Gute
 1:15 am | Robot Chicken | Things Look Bad for the Streepster
 1:30 am | Rick and Morty | The Rickshank Rickdemption
 2:00 am | Futurama | How Hermes Requisitioned His Groove Back
 2:30 am | Futurama | A Clone of My Own
 3:00 am | The Venture Brothers | Hostile Makeover
 3:30 am | Robot Chicken | Scoot to the Gute
 3:45 am | Robot Chicken | Things Look Bad for the Streepster
 4:00 am | Bob's Burgers | Synchronized Swimming
 4:30 am | Bob's Burgers | Burgerboss
 5:00 am | King Of the Hill | Glen Peggy Glen Ross
 5:30 am | King Of the Hill | The Passion of Dauterive