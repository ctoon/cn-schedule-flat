# 2022-06-02
 6:00 am | Baby Looney Tunes | 
 6:30 am | Baby Looney Tunes | 
 7:00 am | Caillou | 
 7:30 am | Caillou | 
 8:00 am | Pocoyo | 
 8:30 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | 
 9:30 am | Thomas & Friends: All Engines Go | 
10:00 am | Bing | 
10:30 am | Pocoyo | 
11:00 am | Pocoyo | 
11:30 am | Mush-Mush and the Mushables | 
12:00 pm | Craig of the Creek | 
12:30 pm | Craig of the Creek | 
 1:00 pm | Craig of the Creek | 
 1:30 pm | Craig of the Creek | 
 2:00 pm | Amazing World of Gumball | 
 3:30 pm | Amazing World of Gumball | 
 4:00 pm | We Bare Bears | 
 4:30 pm | Craig of the Creek | 
 5:00 pm | Craig of the Creek | 
 5:30 pm | Craig of the Creek | 
 6:00 pm | Teen Titans Go! | 
 6:30 pm | Teen Titans Go! | 
 7:00 pm | We Baby Bears | 
 7:30 pm | Total DramaRama | 