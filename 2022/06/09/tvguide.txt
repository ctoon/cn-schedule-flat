# 2022-06-09
 6:00 am | Mush Mush and the Mushables | Perfect Fritter; The; Save the Fun Tree
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Lellobee City Farms | Bath Song; Bedtime Song; Yes Yes Vegetables Song - Healthy Habits! ; No Monsters Scared of the Dark
 7:45 am | Blippi Wonders | Ocean; Panda; Gravity; Toothbrush
 8:00 am | Pocoyo | Baby Bird Sitting; Everyone's Present
 8:20 am | Bing | Hearts
 8:30 am | Thomas & Friends: All Engines Go | Hide and Surprise! ; Dragon Run
 9:00 am | Mecha Builders | Picture Perfect Park Partysun Block
 9:30 am | Thomas & Friends: All Engines Go | Quiet Delivery; A Kana Goes Slow
10:00 am | Craig of the Creek | The Other Side: The Tournament
10:30 am | Craig of the Creek | Alternate Creekiverse Snow Day
11:00 am | Craig of the Creek | Winter Break
11:30 am | Craig of the Creek | The Snow Place Like Home; Sunflower
12:00 pm | Teen Titans Go! | Superhero Feud; Hafo Safo
12:30 pm | Teen Titans Go! | Bbraebday Zimdings
 1:00 pm | Teen Titans Go! | Real Art; Don't Press Play
 1:30 pm | Teen Titans Go! | Just a Little Patience...Yeah...Yeah; Huggbees
 2:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 3:00 pm | Amazing World of Gumball | The Responsible; The Dress
 3:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 4:00 pm | Total DramaRama | Say Hello to My Little Friends; Dial B for Birder
 4:30 pm | Total DramaRama | Tp2: Judgement Bidet; Squirrels Squirrels Squirrels; Last Mom Standing
 5:00 pm | Total DramaRama | Cody the Barbarian; I Dream of Meanie
 5:30 pm | Total DramaRama | The Waterhose-Five; Cone-Versation
 6:00 pm | Total DramaRama | Oozing Talent
 6:15 pm | Victor and Valentino | The Imposter
 6:30 pm | Victor and Valentino | Bodyguard; The; Showdown at Mayahuel Garden
 7:00 pm | Victor and Valentino | The There's No V in Team; Fog
 7:30 pm | Victor and Valentino | Sal's Our Pal