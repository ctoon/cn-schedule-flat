# 2022-06-03
 8:00 pm | King Of the Hill | The Texas Panhandler
 8:30 pm | King Of the Hill | Hank's Bully
 9:00 pm | Futurama | A Clockwork Origin
 9:30 pm | Futurama | The Prisoner of Benda
10:00 pm | American Dad | Deacon Stan, Jesus Man
10:30 pm | American Dad | Bullocks to Stan
11:00 pm | American Dad | A Smith in the Hand
11:30 pm | Rick and Morty | The Wedding Squanchers
12:00 am | Metalocalypse | Fanklok
12:15 am | Metalocalypse | Diversityklok
12:30 am | Metalocalypse | Prankklok
12:45 am | Metalocalypse | Motherklok
 1:00 am | Metalocalypse | Bookklok
 1:15 am | Metalocalypse | Writersklok
 1:30 am | Metalocalypse | Dethcamp
 1:45 am | Metalocalypse | Dethvanity
 2:00 am | Futurama | Benderama
 2:30 am | Futurama | Ghost in the Machines
 3:00 am | Futurama | Law & Oracle
 3:30 am | Futurama | The Silence of the Clamps
 4:00 am | King Of the Hill | Edu-macating Lucky
 4:30 am | King Of the Hill | The Peggy Horror Picture Show
 5:00 am | King Of the Hill | The Texas Panhandler
 5:30 am | King Of the Hill | Hank's Bully