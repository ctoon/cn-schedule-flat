# 2022-12-11
 9:00 pm | Futurama | Lrrreconcilable Ndndifferences
 9:30 pm | Futurama | A Tale of Two Santas
10:00 pm | American Dad | Santa, Schmanta
10:30 pm | American Dad | Gernot and Strudel
11:00 pm | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
11:30 pm | Adult Swim Yule Log | Adult Swim Yule Log
 1:30 am | American Dad | Santa, Schmanta
 2:00 am | American Dad | Gernot and Strudel
 2:30 am | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
 3:00 am | Rick and Morty | A Rick in King Mortur's Mort
 3:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
 3:45 am | Smiling Friends | Enchanted Forest
 4:00 am | Futurama | Lrrreconcilable Ndndifferences
 4:30 am | Futurama | A Tale of Two Santas
 5:00 am | King Of the Hill | The Unbearable Blindness of Laying
 5:30 am | King Of the Hill | Pretty, Pretty Dresses