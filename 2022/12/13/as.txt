# 2022-12-13
 9:00 pm | Bob's Burgers | Hawk & Chick
 9:30 pm | Bob's Burgers | The Oeder Games
10:00 pm | American Dad | Julia Rogerts
10:30 pm | American Dad | The Life and Times of Stan Smith
11:00 pm | American Dad | The Bitchin' Race
11:30 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
12:00 am | Mike Tyson Mysteries | Life is But a Dream
12:15 am | Mike Tyson Mysteries | Unsolved Situations
12:30 am | Aqua Teen | Antenna
12:45 am | Aqua Teen | Ezekiel
 1:00 am | The Boondocks | Good Times
 1:30 am | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
 2:00 am | Futurama | Crimes of the Hot
 2:30 am | Futurama | Jurassic Bark
 3:00 am | Your Pretty Face is Going to Hell | Conceal and Gary
 3:15 am | Your Pretty Face is Going to Hell | Fried Alive
 3:30 am | Infomercials | Innovation Makers: The Coyote Suit
 3:45 am | Infomercials | A Message from the Future
 4:00 am | Bob's Burgers | Hawk & Chick
 4:30 am | Bob's Burgers | The Oeder Games
 5:00 am | King Of the Hill | Night and Deity
 5:30 am | King Of the Hill | Maid in Arlen