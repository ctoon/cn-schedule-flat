# 2022-12-18
 6:00 am | Teen Titans Go! | The True Meaning of Christmas
 6:30 am | Teen Titans Go! | Brain Food; In and Out
 7:00 am | Teen Titans Go! | Little Buddies; Missing
 7:30 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 8:00 am | Amazing World of Gumball | The Promise; The Voice
 8:30 am | Amazing World of Gumball | The Boombox; The Castle
 9:00 am | We Baby Bears | Hashtag Number One Fan
 9:30 am | We Baby Bears | Witches
10:00 am | Craig of the Creek | Copycat Carter
10:30 am | Craig of the Creek | Brother Builder
11:00 am | Craig of the Creek | Monster in the Garden
11:30 am | Craig of the Creek | Dog Decider
12:00 pm | Teen Titans Go! | A Holiday Story
12:30 pm | Teen Titans Go! | Second Christmas
 1:00 pm | Teen Titans Go! | Halloween vs. Christmas
 1:30 pm | Teen Titans Go! | Christmas Crusaders
 2:00 pm | Teen Titans Go! | 50% Chad
 2:30 pm | Teen Titans Go! | The Score
 3:00 pm | Teen Titans Go! | 365!
 3:30 pm | Teen Titans Go! | A Holiday Story
 4:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 4:30 pm | Amazing World of Gumball | The Internet; The Plan
 5:00 pm | Amazing World of Gumball | The World; The Finale
 5:30 pm | Amazing World of Gumball | Christmas
 6:00 pm | Shazam! | 
 8:30 pm | Bob's Burgers | Brunchsquatch