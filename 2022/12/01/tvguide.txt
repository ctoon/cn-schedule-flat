# 2022-12-01
 6:00 am | Amazing World of Gumball | The Fridge; The Remote
 6:30 am | Amazing World of Gumball | The Flower; The Banana
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Thomas & Friends: All Engines Go | Real Number One; The; Sandy Versus the Storm
 9:00 am | Mecha Builders | Cross That Bridgeknock Knock! Who's There?
 9:30 am | Bugs Bunny Builders | Stories; Smash House
10:00 am | Meet the Batwheels | Scaredy-Bat; Riddle Me This
10:30 am | Thomas & Friends: All Engines Go | An Whiteout! ; Unbeleafable Day
11:00 am | New Looney Tunes | Wedding Quacksher; Thefood Notwork; the; You Can't Train a Pigcopy Quack
11:30 am | New Looney Tunes | Sir Littlechin and the Phoenixlooney Luau; Amaduckusfowl Me Once
12:00 pm | Craig of the Creek | Trading Day Sleepover at Jp's
12:30 pm | Craig of the Creek | Crisis at Elder Rock; Tea Timer's Ball
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 1:30 pm | Teen Titans Go! | TV Knight 6; Various Modes of Transportation
 2:00 pm | Amazing World of Gumball | The Hero; The Photo
 2:30 pm | Amazing World of Gumball | The Tag; The Lesson
 3:00 pm | Amazing World of Gumball | The Limit; The Game
 3:30 pm | Amazing World of Gumball | The Promise; The Voice
 4:00 pm | Craig of the Creek | In the Key of the Creek; Creek Shorts
 4:30 pm | Craig of the Creek | Children of the Dog; The Ground Is Lava!
 5:00 pm | Teen Titans Go! | Superhero Feud; Villains in a Van Getting Gelato
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 6:00 pm | Arthur Christmas | 
 8:00 pm | Regular Show | Thomas Fights Back; Lift With Your Back
 8:30 pm | Regular Show | Bachelor Party! Zingo! ; Eileen Flat Screen