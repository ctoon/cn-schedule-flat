# 2022-12-09
 6:00 am | Amazing World of Gumball | The Boss; The Move
 6:30 am | Amazing World of Gumball | The Law; The Allergy
 7:00 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 7:30 am | Cocomelon | Car Wash Song
 8:00 am | Cocomelon | Skidamarink
 8:30 am | Thomas & Friends: All Engines Go | Christmas Mountain
 9:00 am | Mecha Builders | The Yip Yip Tree Tree; Snowman Scarecrow
 9:30 am | Bugs Bunny Builders | Looneyburg Lights
10:00 am | Meet the Batwheels | Holidays on Ice
10:30 am | Thomas & Friends: All Engines Go | Tyrannosaurus Wrecks
11:00 am | New Looney Tunes | Etiquette Shmetiquette; Daffy in the Science Museum
11:30 am | New Looney Tunes | Tad the Bachelor; Affaire du Jour
12:00 pm | Craig of the Creek | Beyond the Rapids
12:30 pm | Craig of the Creek | The Jinxening
 1:00 pm | Teen Titans Go! | Batman's Birthday Gift
 1:30 pm | Teen Titans Go! | What a Boy Wonders
 2:00 pm | Amazing World of Gumball | The Society; The Spoiler
 2:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 3:00 pm | Amazing World of Gumball | The Downer; The Egg
 3:30 pm | Amazing World of Gumball | The Triangle; The Money
 4:00 pm | Craig of the Creek | Winter Break
 4:30 pm | Craig of the Creek | Welcome to Creek Street
 5:00 pm | Teen Titans Go! | The Great Holiday Escape
 5:15 pm | Teen Titans Go! | A Holiday Story
 5:30 pm | Teen Titans Go! | Second Christmas
 6:00 pm | Amazing World of Gumball | Christmas
 6:30 pm | Adventure Time | Holly Jolly Secrets Part I; Holly Jolly Secrets Part II
 7:00 pm | Adventure Time | The More You Moe; The Moe You Know
 7:30 pm | Steven Universe | Winter Forecast
 8:00 pm | Regular Show | White Elephant Gift Exchange
 8:30 pm | Regular Show | Christmas in Space