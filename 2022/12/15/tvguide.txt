# 2022-12-15
 6:00 am | Amazing World of Gumball | The Crew; The; Comic; the Best Present in the World
 6:30 am | Amazing World of Gumball | Others; The; Romantic; the Sandy Tidies Up
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Holidays on Ice
 9:00 am | Mecha Builders | Yip Yip Tree Tree / The Snowman Scarecrow
 9:30 am | Thomas & Friends: All Engines Go | Christmas Mountain; Hot Air Percy Robin's Ride
10:00 am | Thomas & Friends: All Engines Go | Christmas Mountain; Percy Disappears
10:30 am | Bugs Bunny Builders | The 14 Looneyburg Lights; Beepers: The Visitor
11:00 am | New Looney Tunes | Quack to the Futureoctopepe; Planet of the Bigfoots Part 1planet of the Bigfoots Part 2
11:30 am | New Looney Tunes | No Thanks Givingdarkbat; Cold Medal Wabbitno Duck Is an Island
12:00 pm | Craig of the Creek | I Don't Need a Hat; Fan or Foe
12:30 pm | Craig of the Creek | Alternate Creekiverse; New Jersey
 1:00 pm | Teen Titans Go! | Score; The; Wally T
 1:30 pm | Teen Titans Go! | 365! Rad Dudes With Bad Tudes
 2:00 pm | Amazing World of Gumball | The Misunderstandings; The Traitor
 2:30 pm | Amazing World Of Gumball | The Advice; The Roots
 3:00 pm | Amazing World of Gumball | The Signal; The Blame
 3:30 pm | Amazing World of Gumball | The Girlfriend; The Stories
 4:00 pm | Craig of the Creek | Back to Cool; Sink or Swim Team
 4:30 pm | Craig of the Creek | The Brother Builder; Quick Name
 5:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 5:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 6:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 6:30 pm | Teen Titans Go! | Books; Lazy Sunday
 7:00 pm | Amazing World of Gumball | The Return; The Parking
 7:30 pm | Amazing World of Gumball | The Upgrade; The Nemesis
 8:00 pm | Scooby-Doo and Guess Who? | Lost Soles Of Jungle River!
 8:30 pm | Scooby-Doo and Guess Who? | The Tao Of Scoob!