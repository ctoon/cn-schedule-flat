# 2022-12-15
 9:00 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 9:30 pm | Bob's Burgers | Sexy Dance Healing
10:00 pm | American Dad | Garbage Stan
10:30 pm | American Dad | The Talented Mr. Dingleberry
11:00 pm | American Dad | West to Mexico
11:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
12:00 am | Mike Tyson Mysteries | Unholy Matrimony
12:15 am | Mike Tyson Mysteries | The Christmas Episode
12:30 am | Aqua Teen | Sirens
12:45 am | Aqua Teen | Couples Skate
 1:00 am | The Boondocks | Early Bird Special
 1:30 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 2:00 am | Futurama | Kif Gets Knocked Up a Notch
 2:30 am | Futurama | Less Than Hero
 3:00 am | Your Pretty Face is Going to Hell | Take Life by the Horns
 3:15 am | Your Pretty Face is Going to Hell | Schmickler83!
 3:30 am | Infomercials | Pervert Everything
 3:45 am | Infomercials | Flayaway
 4:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 4:30 am | Bob's Burgers | Sexy Dance Healing
 5:00 am | King Of the Hill | Reborn To Be Wild
 5:30 am | King Of the Hill | New Cowboy on the Block