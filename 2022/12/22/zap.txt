# 2022-12-22
 6:00 am | Amazing World of Gumball | The Vision
 6:30 am | Amazing World of Gumball | The Choices
 7:00 am | Cocomelon | Tap Dancing Song
 7:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 8:00 am | Cocomelon | Balloon Boat Race
 8:30 am | Meet the Batwheels | Up in the Air
 9:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 9:30 am | Thomas & Friends: All Engines Go | Rules of the Game
10:00 am | Thomas & Friends: All Engines Go | Whiteout!
10:30 am | Bugs Bunny Builders | Beach Battle
11:00 am | New Looney Tunes | Coyote Under Construction; The Bunny and the Goon
11:30 am | New Looney Tunes | Model T. Fudd; Victory Clasp
12:00 pm | Craig of the Creek | Capture the Flag Part 2: The King
12:30 pm | Craig of the Creek | Capture the Flag Part 4: The Plan
 1:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 1:30 pm | Teen Titans Go! | Brain Food; In and Out
 2:00 pm | Amazing World of Gumball | The Box
 2:30 pm | Amazing World of Gumball | The Matchmaker
 3:00 pm | Amazing World of Gumball | The Grades
 3:30 pm | Amazing World of Gumball | The Diet
 4:00 pm | Craig of the Creek | In Search of Lore
 4:30 pm | Craig of the Creek | Opposite Day
 5:00 pm | Teen Titans Go! | Love Monsters; Baby Hands
 5:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:00 pm | Teen Titans Go! | Friendship; Vegetables
 6:30 pm | Teen Titans Go! | The Mask; Slumber Party
 7:00 pm | Amazing World of Gumball | The Guy
 7:30 pm | Amazing World of Gumball | The Boredom
 8:00 pm | Scooby-Doo and Guess Who? | A Haunt of a Thousand Voices!
 8:30 pm | Scooby-Doo and Guess Who? | Scooby-Doo, Dog Wonder!