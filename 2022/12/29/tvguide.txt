# 2022-12-29
 6:00 am | Amazing World of Gumball | The Candidate; The; Awareness
 6:30 am | Amazing World of Gumball | The Faith; The; Parents
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Bam's Upgrade; Up in the Air
 9:00 am | Mecha Builders | A Let's Get Rolling! Bit of a Stretch!
 9:30 am | Thomas & Friends: All Engines Go | Super Screen Cleaners; Whistle Woes
10:00 am | Thomas & Friends: All Engines Go | Snowplow Struttin'; Letting off Steam
10:30 am | Bugs Bunny Builders | Stories; Dino Fright
11:00 am | New Looney Tunes | Big Troublesmanner Maid; Sir Little Chin Griffin Hunterbugs in Time
11:30 am | New Looney Tunes | Bugsfootgrim on Vacation; Airpork Securityhome a Clone
12:00 pm | Craig of the Creek | Sunday Clothes; Kelsey Quest
12:30 pm | Craig of the Creek | Escape From Family Dinner; Jpony
 1:00 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
 1:30 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 2:00 pm | Amazing World Of Gumball | The Slip; The Revolt
 2:30 pm | Amazing World Of Gumball | The Drama; The Mess
 3:00 pm | Amazing World of Gumball | The Possession; The Buddy
 3:30 pm | Amazing World Of Gumball | The Heart; The Master
 4:00 pm | Craig of the Creek | You're It; Bring Out Your Beast
 4:30 pm | Craig of the Creek | Itch to Explore; Lost in the Sewer
 5:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 5:30 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 6:00 pm | Teen Titans Go! | The Return of Slade; More of the Same
 6:30 pm | Teen Titans Go! | Leg Day; Cat's Fancy
 7:00 pm | Scooby-Doo and Guess Who? | Peebles' Pet Shop of Terrible Terrors!
 7:30 pm | Scooby-Doo and Guess Who? | The Scooby of a Thousand Faces!