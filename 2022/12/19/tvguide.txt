# 2022-12-19
 6:00 am | Amazing World of Gumball | Cycle; The; Nuisance; the I Sure Am Feeling Lucky
 6:30 am | Amazing World of Gumball | The Stars; The; Best; the Best Present in the World
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Holidays on Ice
 9:00 am | Mecha Builders | Yip Yip Tree Tree / The Snowman Scarecrow
 9:30 am | Thomas & Friends: All Engines Go | More Than a Pretty Engine; Whiteout!
10:00 am | Thomas & Friends: All Engines Go | Stink Monster; Kana Goes Slow
10:30 am | Bugs Bunny Builders | Looneyburg Lights
11:00 am | New Looney Tunes | Point Beakcal the Viking; Sam the Roughriderfool's Gold
11:30 am | New Looney Tunes | Rhoda's Road Houseclaire de Loon; Bonjour; Darkbatrenaissance Fair-Thee Well
12:00 pm | Craig of the Creek | Fire & Ice; Creek Talent Extravaganza
12:30 pm | Craig of the Creek | Chrono Moss; Dodgy Decisions
 1:00 pm | Teen Titans Go! | Brian; Nature
 1:30 pm | Teen Titans Go! | Knowledge; Salty Codgers
 2:00 pm | Amazing World of Gumball | The Guy; The Fuss
 2:30 pm | Amazing World of Gumball | The Boredom; The News
 3:00 pm | Amazing World of Gumball | The Vision; The; Vase
 3:30 pm | Amazing World of Gumball | The Choices; The Ollie
 4:00 pm | Craig of the Creek | Capture the Flag Part II: The King; Capture the Flag Part I: The Candy
 4:30 pm | Craig of the Creek | Capture the Flag Part IV: The Plan; Capture the Flag Part III: The Legend
 5:00 pm | Babe: Pig in the City | 
 7:00 pm | Amazing World of Gumball | The Copycats; The Heist
 7:30 pm | Amazing World of Gumball | The Catfish; The; Petals
 8:00 pm | Scooby-Doo and Guess Who? | The Lost Mines Of Kilimanjaro!
 8:30 pm | Scooby-Doo and Guess Who? | The Legend Of The Gold Microphone!