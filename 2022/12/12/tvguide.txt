# 2022-12-12
 6:00 am | Amazing World of Gumball | Sale; The; Bus; the Interview With a Crime-Fighter
 6:30 am | Amazing World of Gumball | Routine; The; Night; the Fall Fun
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Best Present in the World; The; Cave Sweet Cave; Redbird's Bogus Beach Day
 9:00 am | Mecha Builders | That Ol' Time Rock Rolls! Make Like a Banana and Split
 9:30 am | Thomas & Friends: All Engines Go | Good as New; Fast Friends My Rusty Can
10:00 am | Thomas & Friends: All Engines Go | Good as New; Carly's Screechy Squeak
10:30 am | Bugs Bunny Builders | Ice Creamed; Race Track Race; Wide Load Vacay: Big City Dancers
11:00 am | New Looney Tunes | The Top Bugsslugsmoby; Sir Littlechin and the Giantwrong Brothers
11:30 am | New Looney Tunes | Rhoda Ragegood Duck to You Cirque; Weiner Loseyankee Doodle Bunny
12:00 pm | Craig of the Creek | Craig World; Locked out Cold
12:30 pm | Craig of the Creek | Body Swap; Beyond the Overpass
 1:00 pm | Teen Titans Go! | Parasite; Starliar
 1:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 2:00 pm | Amazing World of Gumball | The Return; The Parking
 2:30 pm | Amazing World of Gumball | The Upgrade; The Nemesis
 3:00 pm | Amazing World of Gumball | The Comic; The Crew
 3:30 pm | Amazing World of Gumball | The Romantic; The Others
 4:00 pm | Craig of the Creek | Fan or Foe; Puppy Love
 4:30 pm | Craig of the Creek | Alternate Creekiverse; New Jersey
 5:00 pm | Babe | 
 7:00 pm | Amazing World of Gumball | The Nest; The Pest
 7:30 pm | Amazing World of Gumball | The Hug; The Points
 8:00 pm | Scooby-Doo and Guess Who? | Caveman On The Half Pipe!
 8:30 pm | Scooby-Doo and Guess Who? | The Crown Jewel Of Boxing!