# 2022-12-06
 9:00 pm | Bob's Burgers | The Millie-Churian Candidate
 9:30 pm | Bob's Burgers | The Gayle Tales
10:00 pm | American Dad | Criss-Cross Applesauce: The Ballad of Billy Jesusworth
10:30 pm | American Dad | Mine Struggle
11:00 pm | American Dad | Garfield and Friends
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Mike Tyson Mysteries | Old Man of the Mountain
12:15 am | Mike Tyson Mysteries | Jason B. Sucks
12:30 am | Aqua Teen | Hand Banana
12:45 am | Aqua Teen | Party All the Time
 1:00 am | The Boondocks | The Color Ruckus
 1:30 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 2:00 am | Futurama | Roswell That Ends Well
 2:30 am | Futurama | A Tale of Two Santas
 3:00 am | Your Pretty Face is Going to Hell | The Party Hole
 3:15 am | Your Pretty Face is Going to Hell | Trial by Gary
 3:30 am | Infomercials | Final Deployment 4: Queen Battle Walkthrough
 4:00 am | Bob's Burgers | The Millie-Churian Candidate
 4:30 am | Bob's Burgers | The Gayle Tales
 5:00 am | King Of the Hill | The Son Also Roses
 5:30 am | King Of the Hill | The Texas Skillsaw Massacre