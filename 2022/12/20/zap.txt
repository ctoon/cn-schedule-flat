# 2022-12-20
 6:00 am | Amazing World of Gumball | The Grades
 6:30 am | Amazing World of Gumball | The Diet
 7:00 am | Cocomelon | Basketball Song
 7:30 am | Cocomelon | Car Wash Song
 8:00 am | Cocomelon | Yes Yes Playground Song
 8:30 am | Meet the Batwheels | Stop That Ducky!
 9:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 9:30 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell
10:00 am | Thomas & Friends: All Engines Go | Thomas' Promise
10:30 am | Bugs Bunny Builders | Stories
11:00 am | New Looney Tunes | Daffy Duck: Motivational Guru; The Towering Hamsterno
11:30 am | New Looney Tunes | Viktor the Science Swede; Dorlock and the Disorient Express
12:00 pm | Craig of the Creek | In Search of Lore
12:30 pm | Craig of the Creek | Opposite Day
 1:00 pm | Teen Titans Go! | Friendship; Vegetables
 1:30 pm | Teen Titans Go! | The Mask; Slumber Party
 2:00 pm | Amazing World of Gumball | The Code
 2:30 pm | Amazing World of Gumball | The Test
 3:00 pm | Amazing World of Gumball | The Slide
 3:30 pm | Amazing World of Gumball | The Loophole
 4:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 4:30 pm | Craig of the Creek | The Dream Team
 5:00 pm | Teen Titans Go! | Little Buddies; Missing
 5:30 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 6:00 pm | Teen Titans Go! | Dreams; Grandma Voice
 6:30 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:00 pm | Amazing World of Gumball | The Box
 7:30 pm | Amazing World of Gumball | The Matchmaker
 8:00 pm | Scooby-Doo and Guess Who? | Total Jeopardy!
 8:30 pm | Scooby-Doo and Guess Who? | Scooby-Doo and the Sky Town Cool School!