# 2022-12-16
 6:00 am | Amazing World of Gumball | The Apprentice
 6:30 am | Amazing World of Gumball | The Check
 7:00 am | Cocomelon | Jobs and Career Song
 7:30 am | Cocomelon | Are We There Yet?
 8:00 am | Cocomelon | Opposites Song
 8:30 am | Meet the Batwheels | Stop That Ducky!
 9:00 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 9:30 am | Thomas & Friends: All Engines Go | 
10:00 am | Thomas & Friends: All Engines Go | Blackout!
10:30 am | Bugs Bunny Builders | Smash House
11:00 am | New Looney Tunes | My Kingdom for a Duck; Finders Keepers, Losers Sweepers
11:30 am | New Looney Tunes | Driving Miss Daffy; Second Fiddle
12:00 pm | Craig of the Creek | Snow Day
12:30 pm | Craig of the Creek | Winter Break
 1:00 pm | Teen Titans Go! | Double Trouble; The Date
 1:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 2:00 pm | Amazing World of Gumball | The Parasite
 2:30 pm | Amazing World of Gumball | The Love
 3:00 pm | Amazing World of Gumball | The Origins
 3:30 pm | Amazing World of Gumball | The Disaster
 4:00 pm | Craig of the Creek | The Chef's Challenge
 4:30 pm | Craig of the Creek | The Sparkle Solution
 5:00 pm | Teen Titans Go! | Starfire the Terrible; Second Christmas
 5:30 pm | Teen Titans Go! | Power Moves; Staring at the Future
 6:00 pm | Uncle Grandpa | Christmas Special
 6:30 pm | Uncle Grandpa | Secret Santa
 7:00 pm | Clarence | Merry Moochmas
 7:15 pm | Apple & Onion | The Eater
 7:30 pm | We Bare Bears | Christmas Parties
 8:00 pm | Regular Show | The Christmas Special
 8:30 pm | Regular Show | Merry Christmas Mordecai