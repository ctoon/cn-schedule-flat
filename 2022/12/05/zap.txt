# 2022-12-05
 6:00 am | Amazing World of Gumball | The Mirror; The Burden
 6:30 am | Amazing World of Gumball | The Bros; The Man
 7:00 am | Cocomelon | Yes Yes Vegetables
 7:30 am | Cocomelon | Car Wash Song
 8:00 am | Lellobee City Farm | The Christmas Parade
 8:15 am | Blippi Wonders | Bear Hibernation
 8:30 am | Thomas & Friends: All Engines Go | Christmas Mountain
 9:00 am | Mecha Builders | The Yip Yip Tree Tree; Snowman Scarecrow
 9:30 am | Bugs Bunny Builders | Looneyburg Lights
10:00 am | Meet the Batwheels | Holidays on Ice
10:30 am | Thomas & Friends: All Engines Go | No Power, No Problem!
11:00 am | New Looney Tunes | Quantum Sheep; Houston, We Have a Duck Problem
11:30 am | New Looney Tunes | 10-4 Good Bunny; Gold Medal Wabbit
12:00 pm | Craig of the Creek | The Other Side: The Tournament
12:30 pm | Craig of the Creek | The Last Game of Summer
 1:00 pm | Teen Titans Go! | Standards & Practices
 1:30 pm | Teen Titans Go! | Belly Math
 2:00 pm | Amazing World of Gumball | The Kids; The Fan
 2:30 pm | Amazing World of Gumball | The Coach; The Joy
 3:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 3:30 pm | Amazing World of Gumball | The Name; The Extras
 4:00 pm | Craig of the Creek | Into the Overpast
 4:30 pm | Craig of the Creek | The Time Capsule
 5:00 pm | Teen Titans Go! | A Holiday Story
 5:30 pm | Smallfoot | 
 7:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 8:00 pm | Scooby-Doo and Guess Who? | Scooby-Doo, Dog Wonder!
 8:30 pm | Scooby-Doo and Guess Who? | The Movieland Monsters!