# 2022-12-27
 8:00 pm | King Of the Hill | Bills are Made to be Broken
 8:30 pm | King Of the Hill | Après Hank, le Deluge
 9:00 pm | King Of the Hill | Daletech
 9:30 pm | Bob's Burgers | House of 1000 Bounces
10:00 pm | Bob's Burgers | Stand by Gene
10:30 pm | American Dad | Railroaded
11:00 pm | American Dad | My Purity Ball and Chain
11:30 pm | Rick and Morty | Childrick of Mort
12:00 am | Rick and Morty | Star Mort Rickturn of the Jerri
12:30 am | Mike Tyson Mysteries | Save Me!
12:45 am | Mike Tyson Mysteries | Time to Fly
 1:00 am | Robot Chicken | Rebel Appliance
 1:15 am | Robot Chicken | Legion of Super-Gyros
 1:30 am | American Dad | Flavortown
 2:00 am | Rick and Morty | Childrick of Mort
 2:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 3:00 am | Mike Tyson Mysteries | Save Me!
 3:15 am | Mike Tyson Mysteries | Time to Fly
 3:30 am | Off The Air | Journeys
 3:45 am | Off The Air | Paradise
 4:00 am | Futurama | Bender's Game Part 3
 4:30 am | Futurama | Bender's Game Part 4
 5:00 am | Bob's Burgers | House of 1000 Bounces
 5:30 am | Bob's Burgers | Stand by Gene