# 2022-12-03
 6:00 am | Amazing World of Gumball | The Revolt; The Bffs
 6:30 am | Amazing World Of Gumball | The Mess; The Agent
 7:00 am | Amazing World of Gumball | The Possession; The Decisions
 7:30 am | Amazing World Of Gumball | The Heart; The Web
 8:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 9:00 am | We Baby Bears | Witches; Happy Bouncy Fun Town
 9:30 am | We Baby Bears | Magical Box; The; Bears and the Beanstalk
10:00 am | Total DramaRama | A Sugar & Spice & Lightning & Frights; Dingo Ate My Duncan
10:30 am | Total DramaRama | Breaking Bite; Erase Yer Head
11:00 am | Total DramaRama | I Dream of Meanie; Teacher; Soldier; Chef; Spy
11:30 am | Total DramaRama | Squirrels Squirrels Squirrels; Thingameroo
12:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
12:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 1:00 pm | Teen Titans Go! | Double Trouble; The Date
 1:30 pm | Teen Titans Go! | Dude Relax!; Laundry Day
 2:00 pm | Craig of the Creek | Craig to the Future; Craig of the Street
 2:30 pm | Craig of the Creek | Puppy Love; Galactic Goodbyes
 3:00 pm | Craig of the Creek | Snow Day; Back to Cool
 3:30 pm | Craig of the Creek | Ice Pop Trio; The; Snow Place Like Home
 4:00 pm | Amazing World of Gumball | The Third; The Debt
 4:30 pm | Amazing World of Gumball | The Pressure; The Painting
 5:00 pm | Amazing World of Gumball | The Responsible; The Dress
 5:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 6:00 pm | Babe: Pig in the City | 
 8:00 pm | Regular Show | Merry Christmas Mordecai; Happy Birthday Song Contest
 8:30 pm | Regular Show | Sad Sax; Benson's Suit