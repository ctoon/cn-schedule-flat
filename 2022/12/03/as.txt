# 2022-12-03
 9:00 pm | Futurama | Insane in the Mainframe
 9:30 pm | Futurama | Bendin' in the Wind
10:00 pm | American Dad | Hayley Smith, Seal Team Six
10:30 pm | American Dad | N.S.A. (No Snoops Allowed)
11:00 pm | American Dad | Stan Smith as Keanu Reeves as Stanny Utah in Point Breakers
11:30 pm | Rick and Morty | Analyze Piss
12:00 am | My Hero Academia | A Quiet Beginning
12:30 am | Made In Abyss | Friend
 1:00 am | Yashahime: Princess Half-Demon - The Second Act | The Three Princesses Escape
 1:30 am | One Piece | An Intense Battle! Caesar Exercises His True Power!
 2:00 am | Naruto:Shippuden | The Eight Inner Gates Formation
 2:30 am | Genndy Tartakovsky's Primal | Vidarr
 3:00 am | Samurai Jack | XCVI
 3:30 am | Black Dynamite | "Panic on the Player's Ball Express" or "That's Influenza!"
 4:00 am | Futurama | Insane in the Mainframe
 4:30 am | Futurama | Bendin' in the Wind
 5:00 am | King Of the Hill | Returning Japanese (1)
 5:30 am | King Of the Hill | Returning Japanese (2)