# 2022-12-21
 6:00 am | Amazing World of Gumball | Menu; The; Lady; the; O'little Tree
 6:30 am | Amazing World of Gumball | Uncle; The; Sucker; the Crystal Cavern
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Buff's Bff; Sidekicked to the Curb
 9:00 am | Mecha Builders | Yip Yip Book Bookstick to It
 9:30 am | Thomas & Friends: All Engines Go | License to Deliver; Wide Delivery; a Thomas & the Troublesome Trucks
10:00 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment; Thomas Blasts Off
10:30 am | Bugs Bunny Builders | Cheesy Peasy; Squirreled Away
11:00 am | New Looney Tunes | Regatta de Rabbitwhen Irish Eyes Are Swinin'; St. Bugs and the Dragonleaf It Alone
11:30 am | New Looney Tunes | Bunny Manquagmire of Solace; Tis the Seasoningwinter Blunderland
12:00 pm | Craig of the Creek | Grandma Smugglers; Bored Games
12:30 pm | Craig of the Creek | The Champion's Hike; Scoutguest
 1:00 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
 1:30 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 2:00 pm | Amazing World of Gumball | The Copycats; The Heist
 2:30 pm | Amazing World of Gumball | The Catfish; The; Petals
 3:00 pm | Amazing World of Gumball | The Cycle; The; Nuisance
 3:30 pm | Amazing World of Gumball | The Stars; The; Best
 4:00 pm | Craig of the Creek | Fire & Ice; Creek Talent Extravaganza
 4:30 pm | Craig of the Creek | Chrono Moss; Dodgy Decisions
 5:00 pm | Teen Titans Go! | Mr. Butt; Man Person
 5:30 pm | Teen Titans Go! | Pirates; I See You
 6:00 pm | Teen Titans Go! | Brian; Nature
 6:30 pm | Teen Titans Go! | Knowledge; Salty Codgers
 7:00 pm | Amazing World of Gumball | The Ex; The; Singing
 7:30 pm | Amazing World of Gumball | The Weirdo; The; Puppets
 8:00 pm | Scooby-Doo and Guess Who? | Falling Star Man!
 8:30 pm | Scooby-Doo and Guess Who? | Dark Diner of Route 66!