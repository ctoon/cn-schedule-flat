# 2022-12-25
 9:00 pm | Futurama | Neutopia
 9:30 pm | Futurama | Benderama
10:00 pm | American Dad | The Sinister Fate!!
10:30 pm | American Dad | The Grounch
11:00 pm | Rick and Morty | Full Meta Jackrick
11:30 pm | Rick and Morty | Analyze Piss
12:00 am | YOLO Crystal Fantasy | Maddison's Birthday Party
12:15 am | YOLO Crystal Fantasy | The Dusty Truck 'n' Donut Muster
12:30 am | Aqua Teen | Rubberman
12:45 am | Aqua Teen | Eggball
 1:00 am | Joe Pera Talks With You | Joe Pera Answers Your Questions About Cold Weather Sports
 1:30 am | American Dad | The Sinister Fate!!
 2:00 am | American Dad | The Grounch
 2:30 am | Rick and Morty | Full Meta Jackrick
 3:00 am | Rick and Morty | Analyze Piss
 3:30 am | YOLO Crystal Fantasy | Maddison's Birthday Party
 3:45 am | YOLO Crystal Fantasy | The Dusty Truck 'n' Donut Muster
 4:00 am | Futurama | Neutopia
 4:30 am | Futurama | Benderama
 5:00 am | King Of the Hill | My Hair Lady
 5:30 am | King Of the Hill | Phish and Wildlife