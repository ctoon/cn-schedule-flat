# 2022-12-26
 9:00 pm | King Of the Hill | Dale Be Not Proud
 9:30 pm | Bob's Burgers | Sacred Couch
10:00 pm | Bob's Burgers | Lice Things Are Lice
10:30 pm | American Dad | Death by Dinner Party
11:00 pm | American Dad | The Never-Ending Stories
11:30 pm | Rick and Morty | Promortyus
12:00 am | Rick and Morty | The Vat of Acid Episode
12:30 am | Mike Tyson Mysteries | Mystery for Hire
12:45 am | Mike Tyson Mysteries | The Bard's Curse
 1:00 am | Robot Chicken | Link's Sausages
 1:15 am | Robot Chicken | Secret of the Booze
 1:30 am | American Dad | OreTron Trail
 2:00 am | Rick and Morty | Promortyus
 2:30 am | Rick and Morty | The Vat of Acid Episode
 3:00 am | Mike Tyson Mysteries | Mystery for Hire
 3:15 am | Mike Tyson Mysteries | The Bard's Curse
 3:30 am | Off The Air | Journeys
 3:45 am | Off The Air | Progress
 4:00 am | Futurama | Bender's Game Part 1
 4:30 am | Futurama | Bender's Game Part 2
 5:00 am | Bob's Burgers | Sacred Couch
 5:30 am | Bob's Burgers | Lice Things Are Lice