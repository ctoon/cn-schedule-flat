# 2022-12-26
 6:00 am | Amazing World of Gumball | The Rival; The Understanding
 6:30 am | Amazing World of Gumball | The Cage; The; Ad
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Holidays on Ice
 9:00 am | Mecha Builders | That's the Way the Windmill Blowsmecha Machine Makers
 9:30 am | Thomas & Friends: All Engines Go | Snowplow Struttin'; More Than a Pretty Engine
10:00 am | Thomas & Friends: All Engines Go | Snowplow Struttin'/Thomas in Charge
10:30 am | Bugs Bunny Builders | Play Day; Splash Zone
11:00 am | New Looney Tunes | Wabbit's Wild; All Belts Are Off Your Bunny Or Your Life; Misjudgment Day
11:30 am | New Looney Tunes | Wabbit's Best Friend; Annoying Ex-boydfriend; Splashwater Bugs; Fwee Wange Wabbit
12:00 pm | Craig of the Creek | You're It; Bring Out Your Beast
12:30 pm | Craig of the Creek | Itch to Explore; Lost in the Sewer
 1:00 pm | Teen Titans Go! | Cool School; Video Game References
 1:30 pm | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
 2:00 pm | Amazing World of Gumball | The Shippening; The Brain
 2:30 pm | Amazing World of Gumball | The Anybody; The; Stink
 3:00 pm | Amazing World of Gumball | The Candidate; The; Awareness
 3:30 pm | Amazing World of Gumball | The Faith; The; Parents
 4:00 pm | Craig of the Creek | Locked out Cold; Beyond the Overpass
 4:30 pm | Craig of the Creek | The Sink or Swim Team; Quick Name
 5:00 pm | Arthur Christmas | 
 7:00 pm | Scooby-Doo and Guess Who? | Caveman On The Half Pipe!
 7:30 pm | Scooby-Doo and Guess Who? | Scooby On Ice!
 8:00 pm | King of the Hill | Cotton's Plot
 8:30 pm | King of the Hill | Cheer Factor