# 2022-12-26
 6:00 am | Amazing World of Gumball | The Rival
 6:30 am | Amazing World of Gumball | The Cage
 7:00 am | Cocomelon | Jobs and Career Song
 7:30 am | Cocomelon | Beach Song
 8:00 am | Cocomelon | Yes Yes Playground Song
 8:30 am | Meet the Batwheels | Holidays on Ice
 9:00 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 9:30 am | Thomas & Friends: All Engines Go | Snowplow Struttin'
10:00 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
10:30 am | Bugs Bunny Builders | Play Day
11:00 am | New Looney Tunes | All Belts Are Off; Wabbit's Wild
11:30 am | New Looney Tunes | Wabbit's Best Friend; Annoying Ex-Boydfriend
12:00 pm | Craig of the Creek | You're It
12:30 pm | Craig of the Creek | Itch to Explore
 1:00 pm | Teen Titans Go! | Video Game References; Cool School
 1:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 2:00 pm | Amazing World of Gumball | The Shippening
 2:30 pm | Amazing World of Gumball | The Anybody
 3:00 pm | Amazing World of Gumball | The Candidate
 3:30 pm | Amazing World of Gumball | The Faith
 4:00 pm | Craig of the Creek | Locked Out Cold
 4:30 pm | Craig of the Creek | Sink or Swim Team
 5:00 pm | Arthur Christmas | 
 7:00 pm | Scooby-Doo and Guess Who? | Caveman on the Half Pipe!
 7:30 pm | Scooby-Doo and Guess Who? | Scooby on Ice!
 8:00 pm | King of the Hill | Cotton's Plot
 8:30 pm | King of the Hill | Cheer Factor