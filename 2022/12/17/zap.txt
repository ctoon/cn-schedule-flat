# 2022-12-17
 6:00 am | Amazing World of Gumball | The Skull; Christmas
 6:30 am | Amazing World of Gumball | The Watch; The Bet
 7:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
 7:30 am | Amazing World of Gumball | The Authority; The Virus
 8:00 am | Amazing World of Gumball | The Pony; The Storm
 8:30 am | Amazing World of Gumball | The Pizza; The Lie
 9:00 am | We Baby Bears | Baby Bear Genius
 9:30 am | We Baby Bears | Bug City Sleuths
10:00 am | Total DramaRama | Dial B for Birder
10:30 am | Total DramaRama | A Hole Lot of Trouble
11:00 am | Total DramaRama | A Tall Tale
11:30 am | Total DramaRama | The Tree Stooges Save Christmas
12:00 pm | Teen Titans Go! | No Power; Sidekick
12:30 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 1:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 1:30 pm | Teen Titans Go! | Christmas Crusaders
 2:00 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 3:30 pm | Craig of the Creek | Body Swap
 4:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 4:30 pm | Amazing World of Gumball | The Hero; The Photo
 5:00 pm | Amazing World of Gumball | The Tag; The Lesson
 5:30 pm | Amazing World of Gumball | The Limit; The Game
 6:00 pm | Smallfoot | 
 8:00 pm | Scooby-Doo and Guess Who? | Returning Of The Key Ring!
 8:30 pm | Scooby-Doo and Guess Who? | Cher, Scooby And The Sargasso Sea!