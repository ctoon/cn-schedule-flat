# 2022-08-15
 6:00 am | Pocoyo | Pocoyo's Friend
 6:30 am | Cocomelon | Shadow Puppets
 7:00 am | Cocomelon | This Is the Way Bedtime Edition
 7:30 am | Cocomelon | Yes Yes Playground Song
 8:00 am | Lellobee City Farm | Get Active Dance
 8:15 am | Blippi Wonders | Panda
 8:30 am | Thomas & Friends: All Engines Go | The Joke Is on Thomas
 9:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 9:30 am | Bugs Bunny Builders | Tweety-Go-Round
10:00 am | Total DramaRama | Bananas & Cheese
10:30 am | Total DramaRama | Inglorious Toddlers
11:00 am | We Baby Bears | Teddi Bear
11:30 am | We Bare Bears | My Clique
12:00 pm | Dr. Seuss' the Lorax | 
 2:00 pm | Amazing World of Gumball | The Candidate
 2:30 pm | Amazing World of Gumball | The Anybody
 3:00 pm | Amazing World of Gumball | The Shippening
 3:30 pm | Amazing World of Gumball | The Phone; The Job
 4:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 4:30 pm | Amazing World of Gumball | The Mirror; The Burden
 5:00 pm | Amazing World of Gumball | The Bros; The Man
 5:30 pm | Amazing World of Gumball | The Pizza; The Lie
 6:00 pm | Teen Titans Go! Vs. Teen Titans | 
 7:45 pm | Teen Titans Go! | Beast Girl