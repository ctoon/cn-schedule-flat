# 2022-08-26
 8:00 pm | King Of the Hill | How I Learned To Stop Worrying and Love the Alamo
 8:30 pm | King Of the Hill | Girl, You'll Be a Giant Soon
 9:00 pm | Futurama | The Six Million Dollar Mon
 9:30 pm | Futurama | Fun on a Bun
10:00 pm | American Dad | The Bitchin' Race
10:30 pm | American Dad | Family Plan
11:00 pm | American Dad | The Long Bomb
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Tuca & Bertie | Salad Days
12:30 am | Tuca & Bertie | Screech Leeches
 1:00 am | Tuca & Bertie | A Very Speckle Episode
 1:30 am | Tuca & Bertie | Fledging Day
 2:00 am | Futurama | Mobius Dick
 2:30 am | Futurama | Fry Am the Egg Man
 3:00 am | Futurama | The Tip of the Zoidberg
 3:30 am | Futurama | Cold Warriors
 4:00 am | King Of the Hill | Stressed For Success
 4:30 am | King Of the Hill | Hank's Back
 5:00 am | King Of the Hill | How I Learned To Stop Worrying and Love the Alamo
 5:30 am | King Of the Hill | Girl, You'll Be a Giant Soon