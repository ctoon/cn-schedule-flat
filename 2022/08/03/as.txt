# 2022-08-03
 8:00 pm | King Of the Hill | The Substitute Spanish Prisoner
 8:30 pm | King Of the Hill | Unfortunate Son
 9:00 pm | Bob's Burgers | The Oeder Games
 9:30 pm | Bob's Burgers | Sliding Bobs
10:00 pm | American Dad | She Swill Survive
10:30 pm | American Dad | Rubberneckers
11:00 pm | American Dad | Permanent Record Wrecker
11:30 pm | Rick and Morty | Mortynight Run
12:00 am | The Boondocks | The Uncle Ruckus Reality Show
12:30 am | Robot Chicken | Hemlock, Gin, and Juice
12:45 am | Robot Chicken | Collateral Damage in Gang Turf War
 1:00 am | The Venture Brothers | Return to Malice
 1:30 am | Rick and Morty | Mortynight Run
 2:00 am | Futurama | Attack of the Killer App
 2:30 am | Futurama | Proposition Infinity
 3:00 am | Robot Chicken | Hemlock, Gin, and Juice
 3:15 am | Robot Chicken | Collateral Damage in Gang Turf War
 3:30 am | The Venture Brothers | Return to Malice
 4:00 am | Bob's Burgers | The Oeder Games
 4:30 am | Bob's Burgers | Sliding Bobs
 5:00 am | King Of the Hill | The Substitute Spanish Prisoner
 5:30 am | King Of the Hill | Unfortunate Son