# 2022-08-12
 6:00 am | Pocoyo | The Rescue
 6:30 am | Cocomelon | Rain Rain Go Away Indoors Version
 7:00 am | Cocomelon | YoYo's Arts and Crafts Time: Paper Airplanes
 7:30 am | Cocomelon | Old MacDonald
 8:00 am | Lellobee City Farm | Bath Song
 8:15 am | Blippi Wonders | Fruit & Vegetables
 8:30 am | Thomas & Friends: All Engines Go | Overnight Stop
 9:00 am | Mecha Builders | That Old Time Rock Rolls; Make Like a Banana Split
 9:30 am | Bugs Bunny Builders | Ice Creamed
10:00 am | Total DramaRama | The Cone-versation
10:30 am | Total DramaRama | Oozing Talent
11:00 am | We Baby Bears | Tooth Fairy Tech
11:30 am | We Bare Bears | Everyday Bears
12:00 pm | Craig of the Creek | Creature Feature
12:30 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 1:00 pm | Craig of the Creek | Camper on the Run
 1:30 pm | Craig of the Creek | Stink Bomb
 2:00 pm | Amazing World of Gumball | The Rival
 2:30 pm | Amazing World of Gumball | The One
 3:00 pm | Amazing World of Gumball | The Vegging
 3:30 pm | Amazing World of Gumball | The Father
 4:00 pm | Amazing World of Gumball | The Cringe
 4:30 pm | Amazing World of Gumball | The Neighbor
 5:00 pm | Amazing World of Gumball | The Pact
 5:30 pm | Amazing World of Gumball | The Faith
 6:00 pm | We Baby Bears | A Tale of Two Ice Bears
 6:30 pm | Teen Titans Go! | The Night Begins to Shine Special
 7:30 pm | Teen Titans Go! | BL4Z3