# 2022-08-12
 6:00 am | Pocoyo | The Rescue; My Hero; Angry Alien Strikes Back
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Bath Song; Recycle Stomp; Please and Thank You Song; Wash Your Hands; Scrub That Soap
 8:15 am | Blippi Wonders | Fruit & Vegetables; Garbage Truck; Sandcastle; Toothbrush
 8:30 am | Thomas & Friends: All Engines Go | The Overnight Stop; Tiger Train
 9:00 am | Mecha Builders | That Ol' Time Rock Rolls! Make Like a Banana and Split
 9:30 am | Bugs Bunny Builders | Ice Creamed; Dino Fright
10:00 am | Total DramaRama | Cone-Versation; The; Sharing Is Caring
10:30 am | Total DramaRama | Oozing Talent; Free Chili
11:00 am | We Baby Bears | Tooth Fairy Tech; High Baby Fashion
11:30 am | We Bare Bears | Shush Ninjas; Everyday Bears
12:00 pm | Craig of the Creek | Creature Feature; King of Camping
12:30 pm | Craig of the Creek | The Rise and Fall of the Green Poncho; I Don't Need a Hat
 1:00 pm | Craig of the Creek | The Camper on the Run; Cardboard Identity
 1:30 pm | Craig of the Creek | Stink Bomb; Ancients of the Creek
 2:00 pm | Amazing World of Gumball | The Rival; The; Buddy
 2:30 pm | Amazing World of Gumball | The One; The; Master
 3:00 pm | Amazing World of Gumball | The Vegging; The; Silence
 3:30 pm | Amazing World of Gumball | The Father; The; Future
 4:00 pm | Amazing World of Gumball | The Wish; The Cringe
 4:30 pm | Amazing World of Gumball | The Neighbor; The; Factory
 5:00 pm | Amazing World of Gumball | The Pact; The; Revolt
 5:30 pm | Amazing World of Gumball | The Faith; The; Mess
 6:00 pm | We Baby Bears | A Tale of Two Ice Bears; Unica
 6:30 pm | Teen Titans Go! | "Night Begins to Shine" Special
 7:30 pm | Teen Titans Go! | Lication; Bl4z3