# 2022-08-16
 6:00 am | Let's Go Pocoyo | Hide and Seek
 6:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 7:00 am | Cocomelon | Bingo Farm Version
 7:30 am | Cocomelon | Getting Ready for School Song
 8:00 am | Lellobee City Farm | Baa Baa Black Sheep
 8:15 am | Blippi Wonders | Mountain
 8:30 am | Thomas & Friends: All Engines Go | Lost and Found
 9:00 am | Mecha Builders | They Sent Us a Pie; Dust in the Wind
 9:30 am | Bugs Bunny Builders | Race Track Race
10:00 am | Total DramaRama | Not Without My Fudgy Lumps
10:30 am | Total DramaRama | Paint That a Shame
11:00 am | We Baby Bears | A Gross Worm
11:30 am | We Bare Bears | Charlie
12:00 pm | Craig of the Creek | Ace of Squares
12:30 pm | Craig of the Creek | Doorway to Helen
 1:00 pm | Craig of the Creek | Jessica Shorts
 1:30 pm | Craig of the Creek | Ferret Quest
 2:00 pm | Amazing World of Gumball | The Schooling
 2:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 3:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 3:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 4:00 pm | Amazing World of Gumball | The Butterfly; The Question
 4:30 pm | Amazing World of Gumball | The Oracle; The Safety
 5:00 pm | Amazing World of Gumball | The Friend; The Saint
 5:30 pm | Amazing World of Gumball | The Society; The Spoiler
 6:00 pm | We Baby Bears | Bears and the Beanstalk
 6:30 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:00 pm | Teen Titans Go! | Manor and Mannerisms
 7:30 pm | Teen Titans Go! | What a Boy Wonders