# 2022-08-01
 6:00 am | Pocoyo | The Silence Challenge
 6:30 am | Cocomelon | Clean Machine
 7:00 am | Cocomelon | Pretend Play
 7:30 am | Cocomelon | Beach Song
 8:00 am | Lellobee City Farm | Wheels on the Bus
 8:15 am | Blippi Wonders | Gravity
 8:30 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:00 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 9:30 am | Bugs Bunny Builders | Splash Zone
10:00 am | Total DramaRama | Thingameroo
10:30 am | Total DramaRama | CodE.T.
11:00 am | We Baby Bears | Excalibear
11:30 am | We Bare Bears | Sandcastle
12:00 pm | Craig of the Creek | Creek Shorts
12:30 pm | Craig of the Creek | The Cardboard Identity
 1:00 pm | Craig of the Creek | Opposite Day
 1:30 pm | Craig of the Creek | Adventures in Baby Casino
 2:00 pm | Amazing World of Gumball | The Pony; The Storm
 2:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 3:00 pm | Amazing World of Gumball | The Hero; The Photo
 3:30 pm | Amazing World of Gumball | The Tag; The Lesson
 4:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 4:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 5:00 pm | Teen Titans Go! | Go!
 5:30 pm | Teen Titans Go! | Glunkakakakah
 6:00 pm | Teen Titans Go! | Pool Season
 6:15 pm | Teen Titans Go! | A Sticky Situation
 6:30 pm | Teen Titans Go! | Whodundidit?
 7:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 7:30 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man