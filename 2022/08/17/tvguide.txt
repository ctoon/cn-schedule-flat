# 2022-08-17
 6:00 am | Pocoyo | Elly's Picnic; Pocoyo's Friend; Nina
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Wash Your Hands; Scrub That Soap; Happy Place; Wheels on the Bus; Bath Song
 8:15 am | Blippi Wonders | Toothbrush; Ocean; Sneeze; First Car
 8:30 am | Thomas & Friends: All Engines Go | Calliope Crack-up; Thomas' Day Off
 9:00 am | Mecha Builders | Picture Perfect Park Partysun Block
 9:30 am | Bugs Bunny Builders | Dino Fright; Splash Zone
10:00 am | Total DramaRama | The Never Gwending Story; Snots Landing
10:30 am | Total DramaRama | Know It All; There Are No Hoppy Endings
11:00 am | We Baby Bears | Real Crayon; A; Modern-Ish Stone Age Family
11:30 am | We Bare Bears | Tote Life; Brother Up
12:00 pm | Craig of the Creek | The Kid From 3030; Big Pinchy
12:30 pm | Craig of the Creek | Creek Cart Racers; Power Punchers
 1:00 pm | Craig of the Creek | Beyond the Rapids; King of Camping
 1:30 pm | Craig of the Creek | The Jinxening; The; Rise and Fall of the Green Poncho
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Amazing World of Gumball | The Mystery; The Prank
 3:30 pm | Amazing World of Gumball | The Words; The Apology
 4:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 4:30 pm | Amazing World of Gumball | The Egg; The Downer
 5:00 pm | Amazing World of Gumball | The Money; The Triangle
 5:30 pm | Amazing World of Gumball | The Return; The Nemesis
 6:00 pm | We Baby Bears | Little Mer-Bear; The; Meat House
 6:30 pm | Teen Titans Go! | Little Buddies; Missing
 7:00 pm | Teen Titans Go! | Space House