# 2022-08-09
 6:00 am | Let's Go Pocoyo | Pato's Trip
 6:30 am | Cocomelon | My Daddy Song
 7:00 am | Cocomelon | Beach Song
 7:30 am | Cocomelon | Clean Machine
 8:00 am | Lellobee City Farm | Wheels on the Bus
 8:15 am | Blippi Wonders | Volcano
 8:30 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
 9:00 am | Mecha Builders | Mecha Builders Pull Together; Lift Up and Lift Off
 9:30 am | Bugs Bunny Builders | Splash Zone
10:00 am | Total DramaRama | Total Trauma Rama
10:30 am | Total DramaRama | The Doomed Ballooned Marooned
11:00 am | We Baby Bears | A Tale of Two Ice Bears
11:30 am | We Bare Bears | Food Truck
12:00 pm | Craig of the Creek | Hyde & Zeke
12:30 pm | Craig of the Creek | Grandma Smugglers
 1:00 pm | Craig of the Creek | Lost & Found
 1:30 pm | Craig of the Creek | Bored Games
 2:00 pm | Amazing World of Gumball | The Points
 2:30 pm | Amazing World of Gumball | The Bus
 3:00 pm | Amazing World of Gumball | The Misunderstandings
 3:30 pm | Amazing World of Gumball | The Roots
 4:00 pm | Amazing World of Gumball | The Blame
 4:30 pm | Amazing World of Gumball | The Potato
 5:00 pm | Amazing World of Gumball | The Detective
 5:30 pm | Amazing World of Gumball | The Fury
 6:00 pm | We Baby Bears | Dragon Pests
 6:30 pm | Teen Titans Go! | Books; Lazy Sunday
 7:00 pm | Teen Titans Go! | Real Art
 7:30 pm | Teen Titans Go! | Bumgorf