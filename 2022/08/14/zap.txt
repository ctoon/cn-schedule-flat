# 2022-08-14
 6:00 am | Lamput | Gym
 6:30 am | Teen Titans Go! | Leg Day
 7:00 am | Teen Titans | Spellbound
 7:30 am | Teen Titans | Revolution
 8:00 am | Teen Titans Go! | The Dignity of Teeth
 8:30 am | Teen Titans Go! | The Croissant
 9:00 am | Victor and Valentino | In Your Own Skin
 9:30 am | Victor and Valentino | Ghosted
10:00 am | Victor and Valentino | Las Pesadillas
10:30 am | Victor and Valentino | Sal's Our Pal
11:00 am | We Baby Bears | Modern-ish Stone Age Family
11:30 am | We Baby Bears | Excalibear
12:00 pm | Total DramaRama | A Ninjustice to Harold
12:30 pm | Total DramaRama | Having the Timeout of Our Lives
 1:00 pm | Teen Titans Go! | Two Parter: Part One
 1:30 pm | Teen Titans Go! | Squash & Stretch
 2:00 pm | Amazing World of Gumball | The Nemesis
 2:30 pm | Amazing World of Gumball | The Crew
 3:00 pm | Amazing World of Gumball | The Others
 3:30 pm | Amazing World of Gumball | The Signature
 4:00 pm | Amazing World of Gumball | The Gift
 4:30 pm | Amazing World of Gumball | The Apprentice
 5:00 pm | Amazing World of Gumball | The Check
 5:30 pm | Amazing World of Gumball | The Pest
 6:00 pm | Dr. Seuss' the Lorax | 
 7:50 pm | Wipeout | Wipe-pedia Vol.1