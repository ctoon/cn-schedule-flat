# 2022-08-18
 6:00 am | Pocoyo | Wheels; Elly's Bath; House of Colors
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | ABC Song - Learn Phonics; Accidents Happen Boo Boo Song; Yes Yes Vegetables Song - Healthy Habits! ; Recycle Stomp
 8:15 am | Blippi Wonders | Garbage Truck; First Car; Spiderweb; Sandcastle
 8:30 am | Thomas & Friends: All Engines Go | Real Number One; The; Tyrannosaurus Wrecks
 9:00 am | Mecha Builders | Yip Yip Book Bookstick to It
 9:30 am | Bugs Bunny Builders | Race Track Race; Snow Cap
10:00 am | Total DramaRama | Licking Time Bomb; A Too Much of a Goo'd Thing
10:30 am | Total DramaRama | The From Badge to Worse Price of Advice
11:00 am | We Baby Bears | Bears and the Beanstalk; Excalibear
11:30 am | We Bare Bears | Charlie and the Snake; Occupy Bears
12:00 pm | Craig of the Creek | Secret Book Club; Jextra Perrestrial
12:30 pm | Craig of the Creek | Jessica's Trail; The Takeout Mission
 1:00 pm | Craig of the Creek | Alternate Creekiverse Snow Day
 1:30 pm | Craig of the Creek | Winter Break
 2:00 pm | Amazing World of Gumball | The Mustache; The Date
 2:30 pm | Amazing World of Gumball | The Club; The Wand
 3:00 pm | Amazing World of Gumball | The Ape; The Poltergeist
 3:30 pm | Amazing World of Gumball | The Watch; The Bet
 4:00 pm | Amazing World Of Gumball | The Crew; The Apprentice
 4:30 pm | Amazing World of Gumball | The Check; The Others
 5:00 pm | Amazing World of Gumball | The Signature; The Pest
 5:30 pm | Amazing World of Gumball | The Gift; The; Hug
 6:00 pm | We Baby Bears | The Teddi Bear; Magical Box
 6:30 pm | Teen Titans Go! | Dreams; Grandma Voice
 7:00 pm | Teen Titans Go! | Fat Cats; Whodundidit?
 7:30 pm | Teen Titans Go! | Breakfast; Standards & Practices