# 2022-08-24
 6:00 am | Pocoyo | The Tennis Racket
 6:30 am | Cocomelon | Getting Ready for School Song
 7:00 am | Cocomelon | Jobs and Career Song
 7:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 8:00 am | Lellobee City Farm | Please and Thank You Song
 8:15 am | Blippi Wonders | First Car
 8:30 am | Thomas & Friends: All Engines Go | Wonderful World
 9:00 am | Mecha Builders | The Need For Feed; Zipline Sisters Of Treetop Woods
 9:30 am | Bugs Bunny Builders | Snow Cap
10:00 am | Total DramaRama | Simons Are Forever
10:30 am | Total DramaRama | Stop! Hamster Time
11:00 am | We Baby Bears | Snow Place Like Home
11:30 am | We Bare Bears | Cupcake Job
12:00 pm | Craig of the Creek | Jacob of the Creek
12:30 pm | Craig of the Creek | The Mystery of the Timekeeper
 1:00 pm | Craig of the Creek | Better Than You
 1:30 pm | Craig of the Creek | The Dream Team
 2:00 pm | Amazing World of Gumball | The Internet; The Plan
 2:30 pm | Amazing World of Gumball | The World; The Finale
 3:00 pm | Amazing World of Gumball | The Kids; The Fan
 3:30 pm | Amazing World of Gumball | The Coach; The Joy
 4:00 pm | Teen Titans Go! | Snuggle Time
 4:30 pm | Teen Titans Go! | Oh Yeah!
 5:00 pm | Teen Titans Go! | Riding the Dragon
 5:30 pm | Teen Titans Go! | A Sticky Situation
 6:00 pm | Victor and Valentino | Mission Mariachi
 6:15 pm | Victor and Valentino | A Simpler Life: Ayohuih Nemilitzli
 6:30 pm | Victor and Valentino | Home Sweet Hole
 7:00 pm | Teen Titans Go! | Flashback
 7:30 pm | Teen Titans Go! | Kabooms