# 2022-08-04
 6:00 am | Pocoyo | Camping; Ready; Steady; Go! ; Art
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Bath Song; Happy Place; Get Active Dance; The Silly Little Song
 8:15 am | Blippi Wonders | Toothbrush; Australia Kangaroo; Mountain; Flies
 8:30 am | Thomas & Friends: All Engines Go | Dragon Run; Backwards Day
 9:00 am | Mecha Builders | That's the Way the Windmill Blowsmecha Machine Makers
 9:30 am | Bugs Bunny Builders | Dino Fright; Race Track Race
10:00 am | Total DramaRama | Virtual Reality Bites; Venthalla
10:30 am | Total DramaRama | The Opening Act; Duck Duck Juice
11:00 am | We Baby Bears | Baby Bear Genius; Baba Yaga House
11:30 am | We Bare Bears | Lord of the Poppies; Panda's Birthday
12:00 pm | Craig of the Creek | Ferret Quest; Into the Overpast
12:30 pm | Craig of the Creek | The Time Capsule; Beyond the Rapids
 1:00 pm | Craig of the Creek | Silver Fist Returns; The Brood
 1:30 pm | Craig of the Creek | Under the Overpass; The Climb
 2:00 pm | Amazing World of Gumball | The Oracle; The Safety
 2:30 pm | Amazing World of Gumball | The Friend; The Saint
 3:00 pm | Amazing World of Gumball | The Spoiler; The Society
 3:30 pm | Amazing World of Gumball | The Nobody; The Countdown
 4:00 pm | Teen Titans Go! | Hose Water; Yearbook Madness
 4:30 pm | Teen Titans Go! | Tamaranian Vacation; Let's Get Serious
 5:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 5:30 pm | Teen Titans Go! | A Sticky Situation; Pool Season
 6:00 pm | Teen Titans Go! | We'll Be Right Back
 6:15 pm | Teen Titans Go! | Kyle
 6:30 pm | Teen Titans Go! | Doomsday Preppers; Fat Cats
 7:00 pm | Teen Titans Go! | The Best Robin; Road Trip
 7:30 pm | Teen Titans Go! | Hot Garbage; Mouth Hole