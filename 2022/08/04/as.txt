# 2022-08-04
 8:00 pm | King Of the Hill | Are You There, God? It's Me, Margaret Hill
 8:30 pm | King Of the Hill | Tankin' it to the Streets
 9:00 pm | Bob's Burgers | The Land Ship
 9:30 pm | Bob's Burgers | The Hauntening
10:00 pm | American Dad | News Glance with Genevieve Vavance
10:30 pm | American Dad | The Longest Distance Relationship
11:00 pm | American Dad | A Boy Named Michael
11:30 pm | Rick and Morty | Auto Erotic Assimilation
12:00 am | Genndy Tartakovsky's Primal | The Red Mist
12:30 am | Robot Chicken | Eviscerated Post-Coital by Six Foot Mantis
12:45 am | Robot Chicken | Butchered in Burbank
 1:00 am | The Venture Brothers | The Revenge Society
 1:30 am | Rick and Morty | Auto Erotic Assimilation
 2:00 am | Futurama | The Duh-Vinci Code
 2:30 am | Futurama | Lethal Inspection
 3:00 am | Genndy Tartakovsky's Primal | The Red Mist
 3:30 am | The Venture Brothers | The Revenge Society
 4:00 am | Bob's Burgers | The Land Ship
 4:30 am | Bob's Burgers | The Hauntening
 5:00 am | King Of the Hill | Are You There, God? It's Me, Margaret Hill
 5:30 am | King Of the Hill | Tankin' it to the Streets