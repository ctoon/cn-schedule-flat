# 2022-08-02
 8:00 pm | King Of the Hill | Joust like a Woman
 8:30 pm | King Of the Hill | The Bluegrass is Always Greener
 9:00 pm | Bob's Burgers | The Runway Club
 9:30 pm | Bob's Burgers | The Itty Bitty Ditty Committee
10:00 pm | American Dad | I Ain't No Holodeck Boy
10:30 pm | American Dad | Stan Goes on the Pill
11:00 pm | American Dad | Honey, I'm Homeland
11:30 pm | Rick and Morty | A Rickle in Time
12:00 am | The Boondocks | The Hunger Strike
12:30 am | Robot Chicken | In Bed Surrounded by Loved Ones
12:45 am | Robot Chicken | Choked on Multi-Colored Scarves
 1:00 am | The Venture Brothers | Perchance to Dean
 1:30 am | Rick and Morty | A Rickle in Time
 2:00 am | Futurama | Rebirth
 2:30 am | Futurama | In-A-Gadda-Da-Leela
 3:00 am | Robot Chicken | In Bed Surrounded by Loved Ones
 3:15 am | Robot Chicken | Choked on Multi-Colored Scarves
 3:30 am | The Venture Brothers | Perchance to Dean
 4:00 am | Bob's Burgers | The Runway Club
 4:30 am | Bob's Burgers | The Itty Bitty Ditty Committee
 5:00 am | King Of the Hill | Joust like a Woman
 5:30 am | King Of the Hill | The Bluegrass is Always Greener