# 2022-08-19
 6:00 am | Pocoyo | Nina Discovers The World
 6:30 am | Cocomelon | Johny Johny Yes Papa
 7:00 am | Cocomelon | Balloon Boat Race
 7:30 am | Cocomelon | Skidamarink
 8:00 am | Lellobee City Farm | The Silly Little Song
 8:15 am | Blippi Wonders | Rainbow Colors
 8:30 am | Thomas & Friends: All Engines Go | The Super-Long Shortcut
 9:00 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 9:30 am | Bugs Bunny Builders | Splash Zone
10:00 am | Total DramaRama | Lie-Ranosaurus Wrecked
10:30 am | Total DramaRama | Exercising the Demons
11:00 am | We Baby Bears | The Little Mer-Bear
11:30 am | We Bare Bears | Panda's Sneeze
12:00 pm | Craig of the Creek | Bug City
12:30 pm | Craig of the Creek | The Shortcut
 1:00 pm | Craig of the Creek | Winter Creeklympics
 1:30 pm | Craig of the Creek | Welcome to Creek Street
 2:00 pm | Amazing World of Gumball | The Knights; The Colossus
 2:30 pm | Amazing World of Gumball | The Fridge; The Remote
 3:00 pm | Amazing World of Gumball | The Flower; The Banana
 3:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:00 pm | Amazing World of Gumball | The Routine
 4:30 pm | Amazing World of Gumball | The Parking
 5:00 pm | Amazing World of Gumball | The Upgrade
 5:30 pm | Amazing World of Gumball | The Comic
 6:00 pm | We Baby Bears | A Gross Worm
 6:30 pm | Teen Titans Go! | TV Knight 2
 7:00 pm | Teen Titans Go! | The Academy
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star