# 2022-08-19
 6:00 am | Pocoyo | Nina Discovers the World; Elly's Picnic; Disco Fleaver
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Silly Little Song; The; Splash Song; Shake the Apple Tree; No Monsters Scared of the Dark
 8:15 am | Blippi Wonders | Blippi Learns Rainbow Colors; Planets; Australia Kangaroo; Squirrels
 8:30 am | Thomas & Friends: All Engines Go | The Nia's Perfect Plan; Super-Long Shortcut
 9:00 am | Mecha Builders | That's the Way the Windmill Blowsmecha Machine Makers
 9:30 am | Bugs Bunny Builders | Splash Zone; Smash House
10:00 am | Total DramaRama | An Lie-Ranosaurus Wrecked; Egg-Stremely Bad Idea
10:30 am | Total DramaRama | Exercising the Demons; Pudding the Planet First
11:00 am | We Baby Bears | Little Mer-Bear; The; Meat House
11:30 am | We Bare Bears | Video Date; Panda's Sneeze
12:00 pm | Craig of the Creek | Dinner at the Creek Bug City
12:30 pm | Craig of the Creek | Shortcut; The; Deep Creek Salvage
 1:00 pm | Craig of the Creek | Winter Creeklympics; Body Swap
 1:30 pm | Craig of the Creek | Welcome to Creek Street; Copycat Carter
 2:00 pm | Amazing World of Gumball | The Knights; The Colossus
 2:30 pm | Amazing World of Gumball | The Fridge; The Remote
 3:00 pm | Amazing World of Gumball | The Flower; The Banana
 3:30 pm | Amazing World of Gumball | The Bumpkin; the Flakers
 4:00 pm | Amazing World of Gumball | The Routine; The Romantic
 4:30 pm | Amazing World of Gumball | The Uploads; The Parking
 5:00 pm | Amazing World of Gumball | The Upgrade; The Wicked
 5:30 pm | Amazing World of Gumball | The Traitor; The Comic
 6:00 pm | We Baby Bears | Gross Worm; A; Boo-Dunnit
 6:30 pm | Teen Titans Go! | TV Knight 2 Demon Prom
 7:00 pm | Teen Titans Go! | The Academy; Bbcyfshipbday
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star