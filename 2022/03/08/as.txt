# 2022-03-08
 8:00 pm | King Of the Hill | Life: A Loser's Manual
 8:30 pm | King Of the Hill | Dia-Bill-Ic Shock
 9:00 pm | Bob's Burgers | Purple Rain-Union
 9:30 pm | Bob's Burgers | Bob and Deliver
10:00 pm | American Dad | Every Which Way But Lose
10:30 pm | American Dad | Weiner of Our Discontent
11:00 pm | American Dad | Daddy Queerest
11:30 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
12:00 am | Robot Chicken | Garfield Stockman in: A Voice Like Wet Ham
12:15 am | Robot Chicken | Snoopy Camino Lindo in: Quick and Dirty Squirrel Shot
12:30 am | Smiling Friends | Mr. Frog
12:45 am | Aqua Teen | Allen Part 2
 1:00 am | Squidbillies | One Man Banned
 1:15 am | Squidbillies | Let 'er R.I.P.
 1:30 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 2:00 am | Rick and Morty | Ricksy Business
 2:30 am | Futurama | My Three Suns
 3:00 am | Futurama | A Big Piece of Garbage
 3:30 am | Joe Pera Talks With You | Joe Pera Takes You on a Fall Drive
 3:45 am | Joe Pera Talks With You | Joe Pera Shows You How to Dance
 4:00 am | Bob's Burgers | Purple Rain-Union
 4:30 am | Bob's Burgers | Bob and Deliver
 5:00 am | King Of the Hill | Life: A Loser's Manual
 5:30 am | King Of the Hill | Dia-Bill-Ic Shock