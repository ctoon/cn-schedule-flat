# 2022-03-31
 8:00 pm | King Of the Hill | The Passion of Dauterive
 8:30 pm | King Of the Hill | Grand Theft Arlen
 9:00 pm | Bob's Burgers | The Oeder Games
 9:30 pm | Bob's Burgers | Sliding Bobs
10:00 pm | American Dad | Finger Lenting Good
10:30 pm | American Dad | The Adventures of Twill Ongenbone and His Boy Jabari
11:00 pm | American Dad | Blood Crieth Unto Heaven
11:30 pm | Rick and Morty | Rest and Ricklaxation
12:00 am | Rick and Morty | Total Rickall
12:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
12:45 am | Eric Andre Show | Wink Martindale & Sarah Burns
 1:00 am | Aqua Teen | Muscles
 1:15 am | Birdgirl | ShareBear
 1:45 am | Joe Pera Talks With You | Joe Pera Reads You The Church Announcements
 2:00 am | Rick and Morty | M. Night Shaym-Aliens!
 2:30 am | Futurama | Bender's Big Score Part 1
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
 3:45 am | Eric Andre Show | Wink Martindale & Sarah Burns
 4:00 am | Aqua Teen | Muscles
 4:15 am | Birdgirl | ShareBear
 4:45 am | Joe Pera Talks With You | Joe Pera Reads You The Church Announcements
 5:00 am | King Of the Hill | The Passion of Dauterive
 5:30 am | King Of the Hill | Grand Theft Arlen