# 2022-03-16
 6:00 am | Care Bears: Unlock the Magic | The Beginning
 6:30 am | Esme & Roy | Gentle, Gentle; The Case of the Missing Cuddles
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Playing Dress up; Painting With Pocoyo; Umbrella; Umbrella
 7:50 am | Love Monster | Make Something Epic Day; Bouncey-Slidey Day
 8:10 am | Bing | Giving; Bubbles
 8:30 am | Caillou | Sunday Brunch; Caillou to the Rescue; Caillou's Top Bunk; Recipe for Fun
 9:00 am | Thomas & Friends: All Engines Go | The Whistle Woes When I Go Fast; Paint Problem
 9:30 am | Thomas & Friends: All Engines Go | Mystery Boxcars; Capture the Flag
10:00 am | Mush Mush and the Mushables | Race the Beast; Let It Bee
10:30 am | Care Bears: Unlock the Magic | Last Laugh; The; Say What?
11:00 am | Craig of the Creek | Craig of the Beach Afterschool Snackdown
11:30 am | Craig of the Creek | Creature Feature; Snow Place Like Home
12:00 pm | Craig of the Creek | King of Camping Breaking the Ice
12:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 1:00 pm | Teen Titans Go! | Pirates; I See You
 1:30 pm | Teen Titans Go! | Brian; Nature
 2:00 pm | Amazing World of Gumball | The Tag; The Lesson
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Promise; The Voice
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | We Baby Bears | Bug City Sleuths; Modern-Ish Stone Age Family
 4:30 pm | Craig of the Creek | Snow Day; New Jersey
 5:00 pm | Craig of the Creek | Winter Break
 5:30 pm | Craig of the Creek | The Sunflower; Copycat Carter
 6:00 pm | Teen Titans Go! | Vegetables; Friendship
 6:30 pm | Teen Titans Go! | The Mask; Slumber Party
 7:00 pm | We Baby Bears | Little Mer-Bear; The; Excalibear
 7:30 pm | Total DramaRama | Sharing Is Caring; I Dream of Meanie