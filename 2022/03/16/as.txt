# 2022-03-16
 8:00 pm | King Of the Hill | Born Again on the 4th of July
 8:30 pm | King Of the Hill | Serves Me Right for Giving General George S. Patton the Bathroom Key
 9:00 pm | Bob's Burgers | The Kids Run Away
 9:30 pm | Bob's Burgers | Gene It On
10:00 pm | American Dad | Son of Stan
10:30 pm | American Dad | Stan's Food Restaurant
11:00 pm | American Dad | White Rice
11:30 pm | Rick and Morty | Get Schwifty
12:00 am | Robot Chicken | Petless M in: Cars are Couches of the Road
12:15 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
12:30 am | Smiling Friends | Enchanted Forest
12:45 am | Aqua Teen | Vampirus
 1:00 am | Squidbillies | The Liceman Cometh
 1:15 am | Squidbillies | This Show Was Called Squidbillies
 1:30 am | Rick and Morty | Get Schwifty
 2:00 am | Rick and Morty | The Ricks Must Be Crazy
 2:30 am | Futurama | Xmas Story
 3:00 am | Futurama | Why Must I Be a Crustacean in Love?
 3:30 am | Joe Pera Talks With You | Joe Pera Waits With You
 3:45 am | Joe Pera Talks With You | Joe Pera Guides You Through the Dark
 4:00 am | Bob's Burgers | The Kids Run Away
 4:30 am | Bob's Burgers | Gene It On
 5:00 am | King Of the Hill | Born Again on the 4th of July
 5:30 am | King Of the Hill | Serves Me Right for Giving General George S. Patton the Bathroom Key