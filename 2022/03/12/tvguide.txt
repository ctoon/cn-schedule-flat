# 2022-03-12
 6:00 am | Ben 10 | Dr. Animo and the Mutant Ray
 6:30 am | Ben 10 | Back With a Vengeance
 7:00 am | Teen Titans | Aftershock
 7:30 am | Teen Titans | Aftershock
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 8:30 am | Teen Titans Go! | Bucket List; TV Knight 6
 9:00 am | We Baby Bears | Boo-Dunnit; Pirate Parrot Polly
 9:30 am | We Baby Bears | Meat House; Modern-Ish Stone Age Family
10:00 am | Total DramaRama | Price of Advice; The; Simons Are Forever
10:30 am | Total DramaRama | There Are No Hoppy Endings; Mother of All Cards
11:00 am | Total DramaRama | Duncan Disorderly Stop! Hamster Time
11:30 am | We Baby Bears | Little Mer-Bear; The; Excalibear
12:00 pm | Teen Titans Go! | Don't Press Play 287; The Night Begins to Shine 2: You're the One
 1:30 pm | We Baby Bears | A Magical Box; The; Tale of Two Ice Bears
 2:00 pm | Craig of the Creek | Brood; The Doorway to Helen
 2:30 pm | Craig of the Creek | Under the Overpass; The Climb
 3:00 pm | Craig of the Creek | The Invitation; Big Pinchy
 3:30 pm | We Baby Bears | Bears and the Beanstalk; Big Trouble Little Babies
 4:00 pm | Amazing World of Gumball | The Allergy; The Law
 4:30 pm | Amazing World of Gumball | The Mothers / The Password
 5:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 5:30 pm | Amazing World of Gumball | The Mirror; The Burden
 6:00 pm | Minions | 