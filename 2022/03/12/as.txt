# 2022-03-12
 8:00 pm | King Of the Hill | Lucky See, Monkey Do
 8:30 pm | King Of the Hill | What Happens at the National Propane Gas Convention
 9:00 pm | Futurama | Rebirth
 9:30 pm | Futurama | In-A-Gadda-Da-Leela
10:00 pm | American Dad | Rapture's Delight
10:30 pm | American Dad | Don't Look a Smith Horse in the Mouth
11:00 pm | American Dad | A Jones for a Smith
11:30 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
12:00 am | Shenmue: The Animation | Dignified
12:30 am | Attack on Titan | From You, 2,000 Years Ago
 1:00 am | Assassination Classroom | Think Outside the Box Time
 1:30 am | Made In Abyss | The Great Fault
 3:00 am | One Piece | The Ryugu Palace! Taken by the Shark That They Saved!
 3:00 am | Daylight Savings Time Adjustment - You are now springing forward... | 
 3:30 am | One Piece | A Coward and a Crybaby! The Princess in the Hard Shell Tower!
 4:00 am | Naruto:Shippuden | The Day Naruto Was Born
 4:30 am | Cowboy Bebop | Speak Like a Child
 5:00 am | King Of the Hill | Lucky See, Monkey Do
 5:30 am | King Of the Hill | What Happens at the National Propane Gas Convention