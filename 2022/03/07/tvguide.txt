# 2022-03-07
 6:00 am | Care Bears: Unlock the Magic | Some Luck; Out of the Cheer Zone
 6:30 am | Esme & Roy | Ooga, Party of Six; How Hugo Got His Toots Back
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Pato's Bedtime; Elly's Market; Tennis for Everyone
 7:50 am | Love Monster | Make Something Epic Day; Do Something New Day
 8:10 am | Bing | Bandages; Ice Cream
 8:30 am | Caillou | Butterfly Surprise; Soccer Trouble; Youre Not Miss Martin
 9:00 am | Thomas & Friends: All Engines Go | The Wonderful World; Paint Problem
 9:30 am | Thomas & Friends: All Engines Go | Eggsellent Adventure; Can-Do Submarine Crew
10:00 am | Mush Mush and the Mushables | Lightning Bark; The Perfect Fritter
10:30 am | Care Bears: Unlock the Magic | Plunk to the Rescue; Let the Fun Shine
11:00 am | Craig of the Creek | Kelsey the Elder; Kelsey the Author
11:30 am | Craig of the Creek | Sour Candy Trials; Council of the Creek
12:00 pm | Craig of the Creek | Fort Williams; Sparkle Cadet
12:30 pm | Teen Titans Go! | Ghostboy; La Larva de Amor
 1:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 1:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 2:00 pm | Amazing World of Gumball | The Pressure; The Painting
 2:30 pm | Amazing World of Gumball | The Responsible; The Dress
 3:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:30 pm | Amazing World of Gumball | The Mystery; The Prank
 4:00 pm | We Baby Bears | Bug City Sleuths; Baba Yaga House
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 6:00 pm | Teen Titans Go! | Standard & Practices
 6:15 pm | Victor and Valentino | The Matchmaker
 6:30 pm | Victor and Valentino | Pastry Peril
 6:45 pm | Amazing World of Gumball | The Disaster; The; Re-Run; the; Stories
 7:30 pm | Amazing World of Gumball | The Boredom; The Guy