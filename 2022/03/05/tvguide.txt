# 2022-03-05
 6:00 am | Ben 10 | Ultimate Weapon
 6:30 am | Ben 10 | Tough Luck
 7:00 am | Teen Titans | Titan Rising
 7:30 am | Teen Titans | Winner Take All
 8:00 am | Teen Titans Go! | Communicate Openly; Royal Jelly
 8:30 am | Teen Titans Go! | Had To Be There; Strength Of A Grown Man
 9:00 am | We Baby Bears | Panda's Family; Fiesta Day
 9:30 am | We Baby Bears | Triple T Tigers; Snow Place Like Home
10:00 am | Total DramaRama | Snow Way out; Invasion of the Booger Snatchers
10:30 am | Total DramaRama | All up in Your Drill; Wristy Business
11:00 am | Total DramaRama | Toys Will Be Toys; Melter Skelter
11:30 am | We Baby Bears | Tale of Two Ice Bears; A; Hashtag; Fan
12:00 pm | Teen Titans Go! | Girls Night In
12:30 pm | Teen Titans Go! | The Viewers Decide; The Great Disaster
 1:00 pm | Teen Titans Go! | TV Knight 5; Walk Away
 1:30 pm | We Baby Bears | Unica; Bug City Sleuths
 2:00 pm | Craig of the Creek | Itch to Explore; Escape From Family Dinner
 2:30 pm | Craig of the Creek | You're It; Monster in the Garden
 3:00 pm | Craig of the Creek | The Curse; Jessica Goes to the Creek
 3:30 pm | We Baby Bears | Bears in the Dark; The Magical Box
 4:00 pm | Amazing World of Gumball | The Promise; The Voice
 4:30 pm | Amazing World of Gumball | The Boombox; The Castle
 5:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 5:30 pm | Amazing World of Gumball | The Internet; The Plan
 6:00 pm | The LEGO Movie 2: The Second Part | 