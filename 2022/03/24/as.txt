# 2022-03-24
 8:00 pm | King Of the Hill | Good Hill Hunting
 8:30 pm | King Of the Hill | A Fire Fighting We Will Go
 9:00 pm | Bob's Burgers | The Millie-Churian Candidate
 9:30 pm | Bob's Burgers | The Gayle Tales
10:00 pm | American Dad | The Unbrave One
10:30 pm | American Dad | Stanny Tendergrass
11:00 pm | American Dad | Wheels & the Legman and the Case of Grandpa's Key
11:30 pm | Rick and Morty | The Rickshank Rickdemption
12:00 am | Robot Chicken | May Cause an Excess of Ham			
12:15 am | Robot Chicken | May Cause a Whole Lotta Scabs			
12:30 am | Birdgirl | Thirdgirl
 1:00 am | Teenage Euthanasia | The Bad Bang Theory
 1:30 am | Rick and Morty | The Rickshank Rickdemption
 2:00 am | Rick and Morty | Rickmancing the Stone
 2:30 am | Futurama | The Problem with Popplers
 3:00 am | Futurama | Mother's Day
 3:30 am | Joe Pera Talks With You | Joe Pera Shows You How to Pack a Lunch
 3:45 am | Joe Pera Talks With You | Joe Pera Talks with You on the First Day of School
 4:00 am | Bob's Burgers | The Millie-Churian Candidate
 4:30 am | Bob's Burgers | The Gayle Tales
 5:00 am | King Of the Hill | Good Hill Hunting
 5:30 am | King Of the Hill | A Fire Fighting We Will Go