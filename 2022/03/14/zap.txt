# 2022-03-14
 6:00 am | Care Bears: Unlock the Magic | The Last Laugh
 6:15 am | Care Bears: Unlock the Magic | Say What?
 6:30 am | Esme & Roy | Dark and Stormy Knight; Two Can Play That Game
 7:00 am | Cocomelon | Sea Animal Song
 7:04 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 7:08 am | Cocomelon | Stretching and Exercise Song
 7:12 am | Cocomelon | The Colors Song With Popsicles
 7:16 am | Cocomelon | Opposites Song
 7:20 am | Cocomelon | Breakfast Song
 7:24 am | Cocomelon | Yes Yes Playground Song
 7:30 am | Let's Go Pocoyo | Painting With Pocoyo
 7:36 am | Let's Go Pocoyo | Pocoyo's Camera
 7:42 am | Let's Go Pocoyo | Cooking With Elly
 7:50 am | Love Monster | Challenge Yourself Day
 8:00 am | Love Monster | Super Sound Day
 8:10 am | Bing | Cat
 8:20 am | Bing | Plasters
 8:30 am | Caillou | Caillou Beats the Heat; Back Seat Driver; Lost and Found; Holiday Magic
 9:00 am | Thomas & Friends: All Engines Go | Whistle Woes
 9:10 am | Thomas & Friends: All Engines Go | 
 9:20 am | Thomas & Friends: All Engines Go | Wonderful World
 9:30 am | Thomas & Friends: All Engines Go | Thomas' Day Off
 9:45 am | Thomas & Friends: All Engines Go | The Real Number One
10:00 am | Mush-Mush and the Mushables | Oh My Compost
10:15 am | Mush-Mush and the Mushables | The Staff of Wisdom
10:30 am | Care Bears: Unlock the Magic | Unlock the Magic: The Beginning
11:00 am | Craig of the Creek | Beyond the Rapids
11:15 am | Craig of the Creek | The Jinxening
11:30 am | Craig of the Creek | The Other Side: The Tournament
12:00 pm | Craig of the Creek | Plush Kingdom
12:15 pm | Craig of the Creek | The Ice Pop Trio
12:30 pm | Teen Titans Go! | Birds; Be Mine
 1:00 pm | Teen Titans Go! | Brain Food; In and Out
 1:30 pm | Teen Titans Go! | Little Buddies; Missing
 2:00 pm | Amazing World of Gumball | The Phone; The Job
 2:30 pm | Amazing World of Gumball | The Words; The Apology
 3:00 pm | Amazing World of Gumball | The Watch; The Bet
 3:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:00 pm | We Baby Bears | Big Trouble Little Babies
 4:15 pm | We Baby Bears | Fiesta Day
 4:30 pm | Craig of the Creek | Craig of the Beach
 4:45 pm | Craig of the Creek | Afterschool Snackdown
 5:00 pm | Madagascar | 
 7:00 pm | We Baby Bears | Modern-ish Stone Age Family
 7:15 pm | We Baby Bears | Bug City Sleuths
 7:30 pm | Total DramaRama | Free Chili
 7:45 pm | Total DramaRama | Sugar & Spice & Lightning & Frights