# 2022-03-04
 6:00 am | Care Bears: Unlock the Magic | Some Luck
 6:15 am | Care Bears: Unlock the Magic | Out of the Cheer Zone
 6:30 am | Esme & Roy | Officer Tillie; Fuzzy Ball
 7:00 am | The Not-Too-Late Show With Elmo | John Mulaney; Lil Nas X
 7:20 am | Cocomelon | Jobs and Career Song
 7:24 am | Cocomelon | Boo Boo Song
 7:28 am | Cocomelon | Potty Training Song
 7:32 am | Cocomelon | Mary Had a Little Lamb
 7:36 am | Cocomelon | Sorry, Excuse Me
 7:40 am | Cocomelon | Clean Up Song
 7:44 am | Cocomelon | Balloon Boat Race
 7:50 am | Let's Go Pocoyo | Elly's Market
 7:56 am | Let's Go Pocoyo | Cooking With Elly
 8:02 am | Let's Go Pocoyo | Play Time
 8:10 am | Bing | Ice Cream
 8:20 am | Bing | Say Goodbye
 8:30 am | Lucas the Spider | Bye Bye Bee; Jeepers Peepers; House Is Breathing
 9:00 am | Thomas & Friends: All Engines Go | The Paint Problem
 9:10 am | Thomas & Friends: All Engines Go | 
 9:20 am | Thomas & Friends: All Engines Go | Tyrannosaurus Wrecks
 9:30 am | Thomas & Friends: All Engines Go | No Power, No Problem!
 9:45 am | Thomas & Friends: All Engines Go | The Tiger Train
10:00 am | Mush-Mush and the Mushables | Getting Truffy With It
10:15 am | Mush-Mush and the Mushables | The Adventurers
10:30 am | Care Bears: Unlock the Magic | Some Luck
10:45 am | Care Bears: Unlock the Magic | Out of the Cheer Zone
11:00 am | Craig of the Creek | Creek Cart Racers
11:15 am | Craig of the Creek | Memories of Bobby
11:30 am | Craig of the Creek | Jacob of the Creek
11:45 am | Craig of the Creek | Summer Wish
12:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers
12:15 pm | Craig of the Creek | Turning the Tables
12:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 1:00 pm | Teen Titans Go! | Double Trouble; The Date
 1:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 2:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 2:15 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Leslie?
 2:30 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Bobert?
 2:45 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Anyone?
 3:00 pm | Amazing World of Gumball | The Inquisition
 3:15 pm | Amazing World of Gumball | The Traitor
 3:30 pm | Amazing World of Gumball | The Third; The Debt
 4:00 pm | We Baby Bears | Baby Bear Genius
 4:15 pm | We Baby Bears | Fiesta Day
 4:30 pm | Craig of the Creek | Kelsey Quest
 4:45 pm | Craig of the Creek | Dinner at the Creek
 5:00 pm | Craig of the Creek | JPony
 5:15 pm | Craig of the Creek | Jessica's Trail
 5:30 pm | Craig of the Creek | Ace of Squares
 5:45 pm | Craig of the Creek | Bug City
 6:00 pm | Teen Titans Go! | Creative Geniuses
 6:15 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 6:30 pm | Teen Titans Go! | Manor and Mannerisms
 6:40 pm | Teen Titans Go! | EEbows
 6:50 pm | Teen Titans Go! | Inner Beauty of a Cactus
 7:00 pm | We Baby Bears | Bears and the Beanstalk
 7:15 pm | We Baby Bears | Excalibear
 7:30 pm | Total DramaRama | Venthalla
 7:45 pm | Total DramaRama | Whack Mirror