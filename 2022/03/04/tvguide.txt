# 2022-03-04
 6:00 am | Care Bears: Unlock the Magic | Some Luck; Out of the Cheer Zone
 6:30 am | Esme & Roy | Officer Tillie; Fuzzy Ball
 7:00 am | The Not Too Late Show with Elmo | John Mulaney / Lil Nas X
 7:20 am | Cocomelon | 
 7:50 am | Pocoyo | Elly's Market; Cooking With Elly; Playtime
 8:10 am | Bing | Ice Cream; Say Goodbye
 8:30 am | Lucas the Spider | Bye Bye Beejeepers Peepershouse Is Breathing
 9:00 am | Thomas & Friends: All Engines Go | The Paint Problem; Tyrannosaurus Wrecks
 9:30 am | Thomas & Friends: All Engines Go | Tiger Train; The; No Power; No Problem!
10:00 am | Mush Mush and the Mushables | Getting Truffy With It; The Adventurers
10:30 am | Care Bears: Unlock the Magic | Some Luck; Out of the Cheer Zone
11:00 am | Craig of the Creek | Creek Cart Racers; Memories of Bobby
11:30 am | Craig of the Creek | Jacob of the Creek; Summer Wish
12:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers; Turning the Tables
12:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 1:00 pm | Teen Titans Go! | Double Trouble; The Date
 1:30 pm | Teen Titans Go! | Dude Relax!; Laundry Day
 2:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 3:00 pm | Amazing World of Gumball | The Inquisition; The Traitor
 3:30 pm | Amazing World of Gumball | The Third; The Debt
 4:00 pm | We Baby Bears | Baby Bear Genius; Fiesta Day
 4:30 pm | Craig of the Creek | Kelsey Quest; Dinner at the Creek
 5:00 pm | Craig of the Creek | Jpony; Jessica's Trail
 5:30 pm | Craig of the Creek | Ace of Squares; Bug City
 6:00 pm | Teen Titans Go! | Creative Geniuses; Polly Ethylene and Tara Phthalate
 6:30 pm | Teen Titans Go! | Manor and Mannerisms; Eebows
 7:00 pm | We Baby Bears | Bears and the Beanstalk; Excalibear
 7:30 pm | Total DramaRama | Venthalla; Whack Mirror