# 2022-03-27
 6:00 am | Amazing World of Gumball | The Night
 6:15 am | Amazing World of Gumball | The Detective
 6:30 am | Amazing World of Gumball | The Misunderstandings
 6:45 am | Amazing World of Gumball | The Fury
 7:00 am | Amazing World of Gumball | The Roots
 7:15 am | Amazing World of Gumball | The Compilation
 7:30 am | Amazing World of Gumball | The Blame
 7:45 am | Amazing World of Gumball | The Stories
 8:00 am | Amazing World of Gumball | The Disaster
 8:15 am | Amazing World of Gumball | The Re-Run
 8:30 am | Amazing World of Gumball | The Guy
 8:40 am | Amazing World of Gumball | The Test
 8:50 am | Amazing World of Gumball | The Return
 9:00 am | Amazing World of Gumball | The Boredom
 9:15 am | Amazing World of Gumball | The Slide
 9:30 am | Amazing World of Gumball | The Vision
 9:45 am | Amazing World of Gumball | The Loophole
10:00 am | Amazing World of Gumball | The Choices
10:15 am | Amazing World of Gumball | The Fuss
10:30 am | Amazing World of Gumball | The Code
10:45 am | Amazing World of Gumball | The News
11:00 am | Amazing World of Gumball | The Words; The Apology
11:30 am | Amazing World of Gumball | The Limit; The Game
12:00 pm | Amazing World of Gumball | The Watch; The Bet
12:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
12:45 pm | Amazing World of Gumball | The Apprentice
 1:00 pm | Amazing World of Gumball | The Vase
 1:15 pm | Amazing World of Gumball | The Copycats
 1:30 pm | Amazing World of Gumball | The Ollie
 1:45 pm | Amazing World of Gumball | The Catfish
 2:00 pm | Amazing World of Gumball | The Potato
 2:15 pm | Amazing World of Gumball | The Cycle
 2:30 pm | Amazing World of Gumball | The Sorcerer
 2:40 pm | Amazing World of Gumball | The Stars
 2:50 pm | Amazing World of Gumball | The Factory
 3:00 pm | Amazing World of Gumball | The Console
 3:15 pm | Amazing World of Gumball | The Box
 3:30 pm | Amazing World of Gumball | The Outside
 3:45 pm | Amazing World of Gumball | The Matchmaker
 4:00 pm | Amazing World of Gumball | The Authority; The Virus
 4:30 pm | Amazing World of Gumball | The Pony; The Storm
 4:45 pm | Amazing World of Gumball | The Check
 5:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 5:30 pm | Amazing World of Gumball | The Hero; The Photo
 6:00 pm | The Amazing Spider-Man | 