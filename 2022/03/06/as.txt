# 2022-03-06
 8:30 pm | Bob's Burgers | Mutiny on the Windbreaker
 9:00 pm | Futurama | Attack of the Killer App
 9:30 pm | Futurama | Proposition Infinity
10:00 pm | American Dad | Shakedown Steve
10:30 pm | American Dad | American Dad Graffito
11:00 pm | Rick and Morty | Childrick of Mort
11:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
12:00 am | Robot Chicken | May Cause a Squeakquel			
12:15 am | Robot Chicken | May Cause Immaculate Conception			
12:30 am | Smiling Friends | Who Violently Murdered Simon S. Salty?
12:45 am | Smiling Friends | Enchanted Forest
 1:00 am | YOLO Crystal Fantasy | A Very Extremely Very YOLO Christmas: Reloaded
 1:15 am | Three Busy Debras | Sleepover!
 1:30 am | American Dad | Shakedown Steve
 2:00 am | American Dad | American Dad Graffito
 2:30 am | Rick and Morty | Childrick of Mort
 3:00 am | Rick and Morty | Star Mort Rickturn of the Jerri
 3:30 am | Robot Chicken | May Cause a Squeakquel			
 3:45 am | Robot Chicken | May Cause Immaculate Conception			
 4:00 am | King Of the Hill | Doggone Crazy
 4:30 am | King Of the Hill | Trans-Fascism
 5:00 am | King Of the Hill | The Accidental Terrorist
 5:30 am | King Of the Hill | Behind Closed Doors