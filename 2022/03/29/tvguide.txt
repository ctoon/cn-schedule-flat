# 2022-03-29
 6:00 am | Baby Looney Tunes | Hair Cut-Ups; Does Your Tongue Hang Low; A Clean Sweep
 6:30 am | Esme & Roy | When Figs Fly; Tillie's Tough Break
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Keep Going Pocoyo; Sleepy Bird's Surprise; Having a Ball
 7:50 am | Love Monster | Special Surprise Day; Picnic Night
 8:10 am | Bing | Lunchbox; Dizzy
 8:30 am | Caillou | Clowning Around; Read All About It; Mom For A Day; Caillou Plays Baseball
 9:00 am | Thomas & Friends: All Engines Go | The Percy's Lucky Bell; Biggest Adventure Club
 9:30 am | Thomas & Friends: All Engines Go | A Sandy's Sandy Shipment; Wide Delivery
10:00 am | Mush Mush and the Mushables | The Show Must Go on; The; Disappearing Mushler
10:30 am | Care Bears: Unlock the Magic | A Things That Go Plunk; Wish-Full Reunion
11:00 am | Craig Of The Creek | Under The Overpass; The Kid From 3030
11:30 am | Craig Of The Creek | The Invitation; Power Punchers
12:00 pm | Craig Of The Creek | Vulture's Nest; Creek Cart Racers
12:30 pm | Teen Titans Go! | Hey You; Don't Forget About Me in Your Memory; Animals; It's Just a Word!
 1:00 pm | Teen Titans Go! | Fourth Wall; The; Bbbday!
 1:30 pm | Teen Titans Go! | Garage Sale; Squash & Stretch
 2:00 pm | Amazing World of Gumball | The Romantic; The Love
 2:30 pm | Amazing World of Gumball | The Uploads; The; Scam
 3:00 pm | Amazing World of Gumball | The Wicked; The Awkwardness
 3:30 pm | Amazing World of Gumball | The Origins; The Origins Part 2
 4:00 pm | Craig of the Creek | The Jessica's Trail; Shortcut
 4:30 pm | Craig of the Creek | Dinner at the Creek; Deep Creek Salvage
 5:00 pm | Spider-Man 2 | 
 7:45 pm | Amazing World of Gumball | The Nest; The Detective