# 2022-03-26
 8:00 pm | King Of the Hill | Sleight of Hank
 8:30 pm | King Of the Hill | Jon Vitti Presents: Return to La Grunta
 9:00 pm | Futurama | The Duh-Vinci Code
 9:30 pm | Futurama | Lethal Inspection
10:00 pm | American Dad | Stan's Best Friend
10:30 pm | American Dad | Less Money, Mo' Problems
11:00 pm | American Dad | The Kidney Stays in the Picture
11:30 pm | Rick and Morty | Rattlestar Ricklactica
12:00 am | Shenmue: The Animation | Aspiration
12:30 am | Attack on Titan | Sunset
 1:00 am | Assassination Classroom | Secret Identity Time
 1:30 am | Made In Abyss | Nanachi
 2:00 am | One Piece | Hordy's Onslaught! The Retaliatory Plan Set Into Motion!
 2:30 am | One Piece | The Battle in the Ryugu Palace! Zoro vs. Hordy!
 3:00 am | Naruto:Shippuden | A Shinobi's Dream
 3:30 am | Cowboy Bebop | Pierrot Le Fou
 4:00 am | Futurama | The Duh-Vinci Code
 4:30 am | Futurama | Lethal Inspection
 5:00 am | King Of the Hill | Sleight of Hank
 5:30 am | King Of the Hill | Jon Vitti Presents: Return to La Grunta