# 2022-03-30
 8:00 pm | King Of the Hill | Hank's Cowboy Movie
 8:30 pm | King Of the Hill | Dog Dale Afternoon
 9:00 pm | Bob's Burgers | Housetrap
 9:30 pm | Bob's Burgers | Hawk & Chick
10:00 pm | American Dad | Why Can't We Be Friends
10:30 pm | American Dad | Adventures in Hayleysitting
11:00 pm | American Dad | National Treasure 4: Baby Franny: She's Doing Well: The Hole Story
11:30 pm | Rick and Morty | The Whirly Dirly Conspiracy
12:00 am | Robot Chicken | Disemboweled by an Orphan
12:15 am | Robot Chicken | In Bed Surrounded by Loved Ones
12:30 am | Smiling Friends | A Silly Halloween Special
12:45 am | Aqua Teen | Chicken and Beans
 1:00 am | Squidbillies | Lipstick on a Squid
 1:15 am | Squidbillies | Trackwood Race-ist
 1:30 am | Rick and Morty | The Whirly Dirly Conspiracy
 2:00 am | Rick and Morty | Rest and Ricklaxation
 2:30 am | Futurama | Parasites Lost
 3:00 am | Futurama | Amazon Women in the Mood
 3:30 am | Joe Pera Talks With You | Joe Pera Discusses School-Appropriate Entertainment With You
 3:45 am | Joe Pera Talks With You | Joe Pera Takes You for a Flight
 4:00 am | Bob's Burgers | Housetrap
 4:30 am | Bob's Burgers | Hawk & Chick
 5:00 am | King Of the Hill | Hank's Cowboy Movie
 5:30 am | King Of the Hill | Dog Dale Afternoon