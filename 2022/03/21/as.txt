# 2022-03-21
 8:00 pm | King Of the Hill | Just Another Manic Kahn-Day
 8:30 pm | King Of the Hill | And They Call It Bobby Love
 9:00 pm | Bob's Burgers | Work Hard or Die Trying, Girl
 9:30 pm | Bob's Burgers | Friends with Burger-Fits
10:00 pm | Bob's Burgers | Best Burger
10:30 pm | American Dad | Home Wrecker
11:00 pm | American Dad | Flirting With Disaster
11:30 pm | American Dad | Gorillas in the Mist
12:00 am | Robot Chicken | Ghandi Mulholland in: Plastic Doesn't Get Cancer
12:15 am | Robot Chicken | Gracie Purgatory in: That's How You Get Hemorrhoids
12:30 am | Smiling Friends | Frowning Friends
12:45 am | Aqua Teen | Wi-tri
 1:00 am | Squidbillies | Hetero-Cephalo Agenda
 1:15 am | Squidbillies | Limbitless
 1:30 am | Rick and Morty | Interdimensional Cable 2: Tempting Fate
 2:00 am | Rick and Morty | Look Who's Purging Now
 2:30 am | Futurama | Raging Bender
 3:00 am | Futurama | A Bicyclops Built for Two
 3:30 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 3:45 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 4:00 am | Bob's Burgers | Work Hard or Die Trying, Girl
 4:30 am | Bob's Burgers | Friends with Burger-Fits
 5:00 am | King Of the Hill | Just Another Manic Kahn-Day
 5:30 am | King Of the Hill | And They Call It Bobby Love