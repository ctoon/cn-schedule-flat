# 2022-04-06
 6:00 am | Mush-Mush and the Mushables | Grasshopper Chep
 6:15 am | Mush-Mush and the Mushables | Surprise, It's Spring
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | 
 7:50 am | Love Monster | Favourite Library Book Day
 8:00 am | Love Monster | Snowman Day
 8:10 am | Bing | Sandcastle
 8:20 am | Bing | Looking After Flop
 8:30 am | Caillou | Surprise Party; Caillou's Bad Dream; Caillou Computes; Caillou's Big Discovery
 9:00 am | Thomas & Friends: All Engines Go | Something to Remember
 9:15 am | Thomas & Friends: All Engines Go | Letting Off Steam
 9:30 am | Thomas & Friends: All Engines Go | Roller Coasting
 9:45 am | Thomas & Friends: All Engines Go | No Power, No Problem!
10:00 am | Mush-Mush and the Mushables | Star Helpers
10:15 am | Mush-Mush and the Mushables | Lightning Bark
10:30 am | Baby Looney Tunes | School Daz; Mary Had A Baby Duck (song); Things That Go Bugs In The Night
11:00 am | Craig of the Creek | Turning the Tables
11:15 am | Craig of the Creek | Camper on the Run
11:30 am | Craig of the Creek | Kelsey the Author
11:45 am | Craig of the Creek | Cousin of the Creek
12:00 pm | Craig of the Creek | Creek Daycare
12:15 pm | Craig of the Creek | Sugar Smugglers
12:30 pm | Craig of the Creek | Sleepover at JP's
12:45 pm | Craig of the Creek | The Evolution of Craig
 1:00 pm | Craig of the Creek | Tea Timer's Ball
 1:15 pm | Craig of the Creek | The Cardboard Identity
 1:30 pm | Craig of the Creek | Ancients of the Creek
 1:45 pm | Craig of the Creek | The Haunted Dollhouse
 2:00 pm | Craig of the Creek | Mortimor to the Rescue
 2:15 pm | Craig of the Creek | Secret in a Bottle
 2:30 pm | Craig of the Creek | Trading Day
 2:45 pm | Craig of the Creek | Crisis at Elder Rock
 3:00 pm | Craig of the Creek | Craig and the Kid's Table
 3:30 pm | Craig of the Creek | Kelsey the Worthy
 3:45 pm | Craig of the Creek | The End Was Here
 4:00 pm | Craig of the Creek | The Children of the Dog
 4:15 pm | Craig of the Creek | Jessica Shorts
 4:30 pm | Craig of the Creek | Ferret Quest
 4:45 pm | Craig of the Creek | Into the Overpast
 5:00 pm | Craig of the Creek | The Time Capsule
 5:15 pm | Craig of the Creek | Beyond the Rapids
 5:30 pm | Craig of the Creek | The Jinxening
 5:45 pm | Craig of the Creek | In the Key of the Creek
 6:00 pm | Craig of the Creek | Winter Break
 6:30 pm | Craig of the Creek | Welcome to Creek Street
 6:45 pm | Craig of the Creek | Snow Place Like Home
 7:00 pm | Craig of the Creek | Winter Creeklympics
 7:15 pm | Craig of the Creek | Snow Day
 7:30 pm | Craig of the Creek | Breaking the Ice
 7:45 pm | Craig of the Creek | Alternate Creekiverse