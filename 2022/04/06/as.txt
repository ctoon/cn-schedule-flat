# 2022-04-06
 8:00 pm | King Of the Hill | To Kill a Ladybird
 8:30 pm | King Of the Hill | Old Glory
 9:00 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 9:30 pm | Bob's Burgers | Sexy Dance Healing
10:00 pm | American Dad | Crotchwalkers
10:30 pm | American Dad | Independent Movie
11:00 pm | American Dad | Faking Bad
11:30 pm | Rick and Morty | The Rickchurian Mortydate
12:00 am | Robot Chicken | Butchered in Burbank
12:15 am | Robot Chicken | Papercut to Aorta
12:30 am | Smiling Friends | Frowning Friends
12:45 am | Aqua Teen | Fightan Titan
 1:00 am | Aqua Teen | Buddy Nugget
 1:15 am | Squidbillies | Greener Pastor
 1:30 am | Rick and Morty | The Rickchurian Mortydate
 2:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 2:30 am | Futurama | Insane in the Mainframe
 3:00 am | Futurama | Bendin' in the Wind
 3:30 am | Joe Pera Talks With You | Joe Pera Takes You on a Fall Drive
 3:45 am | Joe Pera Talks With You | Joe Pera Shows You How to Dance
 4:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 4:30 am | Bob's Burgers | Sexy Dance Healing
 5:00 am | King Of the Hill | To Kill a Ladybird
 5:30 am | King Of the Hill | Old Glory