# 2022-04-26
 6:00 am | Mush-Mush and the Mushables | Lightning Bark
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | 
 7:50 am | Love Monster | Tidy Up Day
 8:10 am | Bing | Swing
 8:30 am | Caillou | Elephants!; Caillou and the Sheep; Caillou and the Puppies; The Duck Family; Clementine's New Pet
 9:00 am | Thomas & Friends: All Engines Go | No Power, No Problem!
 9:30 am | Thomas & Friends: All Engines Go | The Tiger Train
10:00 am | Mush-Mush and the Mushables | Special Delivery
10:30 am | Baby Looney Tunes | All Washed Up; My Bunny Lies Over The Ocean (song); Did Not! Did Too!
11:00 am | Craig of the Creek | Fort Williams
11:30 am | Craig of the Creek | The Other Side
12:00 pm | Craig of the Creek | The Evolution of Craig
12:30 pm | Teen Titans Go! | Business Ethics Wink Wink
 1:00 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 1:30 pm | Teen Titans Go! | I Used to Be a Peoples
 2:00 pm | Amazing World of Gumball | The End; The DVD
 2:30 pm | Amazing World of Gumball | The Knights; The Colossus
 3:00 pm | Amazing World of Gumball | The Fridge; The Remote
 3:30 pm | Amazing World of Gumball | The Flower; The Banana
 4:00 pm | We Baby Bears | Bears and the Beanstalk
 4:30 pm | Craig of the Creek | Sparkle Cadet
 5:00 pm | Craig of the Creek | Ancients of the Creek
 5:30 pm | Craig of the Creek | Mortimor to the Rescue
 6:00 pm | Teen Titans Go! | TV Knight 4
 6:30 pm | Teen Titans Go! | Collect Them All
 7:00 pm | Amazing World of Gumball | The Authority; The Virus
 7:30 pm | Amazing World of Gumball | The Pony; The Storm