# 2022-04-08
 6:00 am | Care Bears: Unlock the Magic | Dibble's Dust-up; Monsterplant
 6:30 am | Esme & Roy | To Cupcake with Love; A True Monsterpiece
 7:00 am | The Not Too Late Show with Elmo | Mykal-Michelle Harris / Jonathan Van Ness / H.E.R
 7:20 am | Cocomelon | 
 7:50 am | Pocoyo | A Elly's Playhouse; Pocoyo's Supermarket; Dog's Life
 8:10 am | Bing | Nature Explorer; Hippoty Hoppity Voosh
 8:30 am | Lucas the Spider | Lonesome Lucasi Can't See Youdon't Eat Me
 9:00 am | Thomas & Friends: All Engines Go | Something to Remember; Whistle Woes
 9:30 am | Thomas & Friends: All Engines Go | Calliope Crack-up; Tyrannosaurus Wrecks
10:00 am | Mush Mush and the Mushables | Day With Starmush; A; Special Delivery
10:30 am | Care Bears: Unlock the Magic | Three Cheers for Cheer; Share Your Care
11:00 am | Amazing World of Gumball | The Lady; The; One
11:30 am | Amazing World of Gumball | The Vegging; The Sucker
12:00 pm | Amazing World of Gumball | The Father; The Cage
12:30 pm | Amazing World of Gumball | The Rival; The; Cringe
 1:00 pm | Craig of the Creek | Winter Creeklympics; Welcome to Creek Street
 1:30 pm | Craig of the Creek | Breaking the Ice; Fan or Foe
 2:00 pm | Craig of the Creek | The New Jersey Sunflower
 2:30 pm | Craig of the Creek | Craig World; Body Swap
 3:00 pm | Craig of the Creek | Copycat Carter Brother Builder
 3:30 pm | Craig of the Creek | Capture the Flag Part I: The Candy; Capture the Flag Part II: The King
 4:00 pm | Craig of the Creek | Capture the Flag part 3: The Legend; Capture the Flag part4: The Plan
 4:30 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 5:00 pm | Craig of the Creek | Beyond the Overpass; Jessica the Intern
 5:30 pm | Craig of the Creek | The Sink or Swim Team; Chef's Challenge
 6:00 pm | Craig of the Creek | The Quick Name; The; Sparkle Solution
 6:30 pm | Craig of the Creek | The Better Than You; Dream Team
 7:00 pm | Craig of the Creek | The Fire & Ice; Legend of the Library
 7:30 pm | Craig of the Creek | Locked out Cold; Creek Shorts