# 2022-04-25
 8:00 pm | King Of the Hill | Joust like a Woman
 8:30 pm | King Of the Hill | The Bluegrass is Always Greener
 9:00 pm | Bob's Burgers | Ex Machtina
 9:30 pm | Bob's Burgers | There's No Business Like Mr. Business Business
10:00 pm | Bob's Burgers | Zero Larp Thirty
10:30 pm | American Dad | Mine Struggle
11:00 pm | American Dad | Garfield and Friends
11:30 pm | American Dad | Gift Me Liberty
12:00 am | Robot Chicken | Snarfer Image
12:15 am | Robot Chicken | Up Up and Buffet
12:30 am | Smiling Friends | Enchanted Forest
12:45 am | Aqua Teen | Mouth Quest
 1:00 am | Aqua Teen | Brain Fairy
 1:15 am | Squidbillies | Debased Ball
 1:30 am | Rick and Morty | A Rickconvenient Mort
 2:00 am | Rick and Morty | Rickdependence Spray
 2:30 am | Futurama | Kif Gets Knocked Up a Notch
 3:00 am | Futurama | Less Than Hero
 3:30 am | Joe Pera Talks With You | Joe Pera Shows You How to Pack a Lunch
 3:45 am | Joe Pera Talks With You | Joe Pera Talks with You on the First Day of School
 4:00 am | Bob's Burgers | Ex Machtina
 4:30 am | Bob's Burgers | There's No Business Like Mr. Business Business
 5:00 am | King Of the Hill | Joust like a Woman
 5:30 am | King Of the Hill | The Bluegrass is Always Greener