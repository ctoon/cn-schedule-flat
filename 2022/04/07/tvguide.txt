# 2022-04-07
 6:00 am | Baby Looney Tunes | For Whom The Toll Calls; John Jacob Jongle Elmer Fudd (song); Cereal Boxing
 6:30 am | Esme & Roy | Special Delivery; Fig Do It
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Mystery Footprints; Magical Watering Can; Fussy Duck
 7:50 am | Love Monster | Happy to Help Day; Treasure Hunt Day
 8:10 am | Bing | Acorns; Music
 8:30 am | Lucas the Spider | Ghost Campboonever Boop a Robot
 9:00 am | Thomas & Friends: All Engines Go | The Wonderful World; Tiger Train
 9:30 am | Thomas & Friends: All Engines Go | Eggsellent Adventure; Can-Do Submarine Crew
10:00 am | Mush Mush and the Mushables | Puff's Perfect Picnic; Good Vibrations
10:30 am | Care Bears: Unlock the Magic | Big Rumble; The; Hide and Sneak
11:00 am | Amazing World of Gumball | The Petals; The Deal
11:30 am | Amazing World of Gumball | The Line; The Nuisance
12:00 pm | Amazing World of Gumball | The Best; The Singing
12:30 pm | Amazing World of Gumball | The Worst; The; Puppets
 1:00 pm | Craig of the Creek | The Beyond the Rapids Jinxening
 1:30 pm | Craig of the Creek | in the Key of the Creek; The Ground Is Lava!
 2:00 pm | Craig of the Creek | The Other Side: The Tournament
 2:30 pm | Craig of the Creek | The Council of the Creek: Operation Hive-Mind Bike Thief
 3:00 pm | Craig of the Creek | Craig of the Beach Plush Kingdom
 3:30 pm | Craig of the Creek | Ice Pop Trio; The Pencil Break Mania
 4:00 pm | Craig of the Creek | Last Game of Summer; The Fall Anthology
 4:30 pm | Craig of the Creek | Creature Feature; Afterschool Snackdown
 5:00 pm | Craig of the Creek | Trick or Creek
 5:30 pm | Craig of the Creek | The King of Camping Rise and Fall of the Green Poncho
 6:00 pm | Craig of the Creek | I Don't Need a Hat; Alternate Creekiverse
 6:30 pm | Craig of the Creek | Snow Day Snow Place Like Home
 7:00 pm | Craig of the Creek | Winter Break
 7:30 pm | Craig of the Creek | Winter Creeklympics; Welcome to Creek Street