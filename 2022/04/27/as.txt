# 2022-04-27
 8:00 pm | King Of the Hill | Are You There, God? It's Me, Margaret Hill
 8:30 pm | King Of the Hill | Tankin' it to the Streets
 9:00 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 9:30 pm | Bob's Burgers | Aquaticism
10:00 pm | American Dad | Fight and Flight
10:30 pm | American Dad | The Enlightenment of Ragi-Baba
11:00 pm | American Dad | Portrait of Francine's Genitals
11:30 pm | Rick and Morty | Amortycan Grickfitti
12:00 am | Robot Chicken | Super Guitario Center
12:15 am | Robot Chicken | Noidstrom Rack
12:30 am | Smiling Friends | Charlie Dies and Doesn't Come Back
12:45 am | Aqua Teen | Knapsack!
 1:00 am | Aqua Teen | Rabbit, not Rabbot
 1:15 am | Squidbillies | Duel of the Dimwits
 1:30 am | Rick and Morty | Amortycan Grickfitti
 2:00 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 2:30 am | Futurama | The Sting
 3:00 am | Futurama | The Farnsworth Parabox
 3:30 am | Joe Pera Talks With You | Joe Pera Shows You His Second Fridge
 3:45 am | Joe Pera Talks With You | Joe Pera Listens to Your Drunk Story
 4:00 am | Bob's Burgers | The Grand Mama-Pest Hotel
 4:30 am | Bob's Burgers | Aquaticism
 5:00 am | King Of the Hill | Are You There, God? It's Me, Margaret Hill
 5:30 am | King Of the Hill | Tankin' it to the Streets