# 2022-04-10
 6:00 am | Over the Hedge | 
 7:00 am | Over the Hedge | 
 8:00 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 8:15 am | Teen Titans Go! | Cool Uncles
 8:30 am | Teen Titans Go! | Crazy Day
 8:45 am | Teen Titans Go! | Friendship
 9:00 am | Teen Titans Go! | I See You
 9:15 am | Teen Titans Go! | Lil' Dimples
 9:30 am | Teen Titans Go! | Lucky Stars
 9:40 am | Teen Titans Go! | Legendary Sandwich
 9:50 am | Teen Titans Go! | The Chaff
10:00 am | Teen Titans Go! | Nose Mouth
10:15 am | Teen Titans Go! | Feed Me
10:30 am | Teen Titans Go! | Salty Codgers
10:45 am | Teen Titans Go! | The Overbite
11:00 am | Teen Titans Go! | Dog Hand
11:15 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob
11:30 am | Teen Titans Go! | Cool School
11:40 am | Teen Titans Go! | Laundry Day
11:50 am | Teen Titans Go! | Rain on Your Wedding Day
12:00 pm | Teen Titans Go! | Wally T
12:15 pm | Teen Titans Go! | The Dignity of Teeth
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Teen Titans Go! | Legs
 1:15 pm | Teen Titans Go! | Leg Day
 1:30 pm | Teen Titans Go! | The Mug
 1:40 pm | Teen Titans Go! | My Name Is Jose
 1:50 pm | Teen Titans Go! | BBSFBDAY!
 2:00 pm | Teen Titans Go! | Real Magic
 2:15 pm | Teen Titans Go! | The Power of Shrimps
 2:30 pm | Teen Titans Go! | Demon Prom
 2:45 pm | Teen Titans Go! | Magic Man
 3:00 pm | Teen Titans Go! | Colors of Raven
 3:15 pm | Teen Titans Go! | Arms Race With Legs
 3:30 pm | Shazam! | 
 6:00 pm | Harry Potter 20th Anniversary: Return to Hogwarts | 