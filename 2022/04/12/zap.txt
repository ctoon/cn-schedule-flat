# 2022-04-12
 6:00 am | Mush-Mush and the Mushables | Mushpot's Treasure
 6:15 am | Mush-Mush and the Mushables | Shine, Lilit, Shine
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | 
 7:50 am | Love Monster | Choose One Thing Day
 8:00 am | Love Monster | Switch What You Do Day
 8:10 am | Bing | Dragon Breath
 8:20 am | Bing | Butterfly
 8:30 am | Caillou | Backyard Zoo; Caillou's Scavenger Hunt; Too Many Cooks; The Berry Patch
 9:00 am | Thomas & Friends: All Engines Go | The Paint Problem
 9:15 am | Thomas & Friends: All Engines Go | Wonderful World
 9:30 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
 9:45 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell
10:00 am | Mush-Mush and the Mushables | The Adventurers
10:15 am | Mush-Mush and the Mushables | The Perfect Fritter
10:30 am | Baby Looney Tunes | Cat-taz-trophy; If You're Looney (song); Duck! Monster! Duck!
11:00 am | Craig of the Creek | The Mystery of the Timekeeper
11:15 am | Craig of the Creek | Summer Wish
11:30 am | Craig of the Creek | Return of the Honeysuckle Rangers
11:45 am | Craig of the Creek | Turning the Tables
12:00 pm | Craig of the Creek | Fort Williams
12:15 pm | Craig of the Creek | Camper on the Run
12:30 pm | Craig of the Creek | Jessica Goes to the Creek
12:45 pm | Craig of the Creek | The Final Book
 1:00 pm | Amazing World of Gumball | The End; The DVD
 1:30 pm | Amazing World of Gumball | The Knights; The Colossus
 2:00 pm | Amazing World of Gumball | The Fridge; The Remote
 2:30 pm | Amazing World of Gumball | The Flower; The Banana
 3:00 pm | Amazing World of Gumball | The Shippening
 3:15 pm | Amazing World of Gumball | The Intelligence
 3:30 pm | Amazing World of Gumball | The Brain
 3:45 pm | Amazing World of Gumball | The Potion
 4:00 pm | Amazing World of Gumball | The Stink
 4:15 pm | Amazing World of Gumball | The Spinoffs
 4:30 pm | Amazing World of Gumball | The Awareness
 4:45 pm | Amazing World of Gumball | The Transformation
 5:00 pm | Craig of the Creek | In Search of Lore
 5:15 pm | Craig of the Creek | Chrono Moss
 5:30 pm | Craig of the Creek | Sink or Swim Team
 5:45 pm | Craig of the Creek | The Chef's Challenge
 6:00 pm | Teen Titans Go! | Snuggle Time
 6:15 pm | Teen Titans Go! | Shrimps and Prime Rib
 6:30 pm | Teen Titans Go! | Oh Yeah!
 6:45 pm | Teen Titans Go! | Booby Trap House
 7:00 pm | Teen Titans Go! | Riding the Dragon
 7:15 pm | Teen Titans Go! | Fish Water
 7:30 pm | Teen Titans Go! | BBSFBDAY!
 7:40 pm | Teen Titans Go! | Inner Beauty of a Cactus
 7:50 pm | Teen Titans Go! | The Chaff