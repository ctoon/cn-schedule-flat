# 2022-04-12
 6:00 am | Baby Looney Tunes | Who Said That?; Paws And Feathers (song); Let Them Make Cake
 6:30 am | Esme & Roy | Grandmonster's Day; Monster Bash Surprise
 7:00 am | Cocomelon | 
 7:30 am | Pocoyo | Table for Fun; Twinkle Twinkle; Picture This
 7:50 am | Love Monster | Choose One Thing Day; Switch What You Do Day
 8:10 am | Bing | Dragon Breath; Butterfly
 8:30 am | Caillou | The Backyard Zoocaillou's Scavenger Hunttoo Many Cooksberry Patch
 9:00 am | Thomas & Friends: All Engines Go | The Wonderful World; Paint Problem
 9:30 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell Sandy's Sandy Shipment
10:00 am | Mush Mush and the Mushables | The Adventurers; The; Perfect Fritter
10:30 am | Care Bears: Unlock the Magic | The Water; Water Everywhere; Incredible Shrinking Bears
11:00 am | Craig of the Creek | Mystery of the Timekeeper; The Summer Wish
11:30 am | Craig of the Creek | Return of the Honeysuckle Rangers; Turning the Tables
12:00 pm | Craig of the Creek | Fort Williams; Camper on the Run
12:30 pm | Teen Titans Go! | The Master Detective; Teen Titans Go Easter Holiday Classic
 1:00 pm | Teen Titans Go! | Booty Eggs; Easter Creeps
 1:30 pm | Teen Titans Go! | Egg Hunt; Feed Me
 2:00 pm | Amazing World of Gumball | The Intelligence; The Shippening
 2:30 pm | Amazing World of Gumball | The Potion; The Brain
 3:00 pm | Amazing World of Gumball | The Stink; The; Spinoffs
 3:30 pm | Amazing World of Gumball | The Awareness; The; Transformation
 4:00 pm | Craig of the Creek | Beyond the Overpass; Sink or Swim Team
 4:30 pm | Craig of the Creek | Chef's Challenge; The; Chrono Moss
 5:00 pm | Craig of the Creek | Dodgy Decisions
 5:15 pm | Craig of the Creek | Jessica the Intern
 5:30 pm | We Baby Bears | Bears and the Beanstalk; Meat House
 6:00 pm | Teen Titans Go! | Snuggle Time Shrimps and Prime Rib
 6:30 pm | Teen Titans Go! | Oh Yeah! Booby Trap House
 7:00 pm | Teen Titans Go! | Riding the Dragon; Fish Water
 7:30 pm | Teen Titans Go! | BBSFBDAY;Inner Beauty of a Cactus