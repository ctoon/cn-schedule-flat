# 2022-04-04
 8:00 pm | King Of the Hill | Little Horrors of Shop
 8:30 pm | King Of the Hill | Aisle 8A
 9:00 pm | Bob's Burgers | The Land Ship
 9:30 pm | Bob's Burgers | The Hauntening
10:00 pm | Rick and Morty | Morty's Mind Blowers
10:30 pm | American Dad | The Full Cognitive Redaction of Avery Bullock by the Coward Stan Smith
11:00 pm | American Dad | Lost in Space
11:30 pm | American Dad | Da Flippity Flop
12:00 am | Robot Chicken | Choked on Multi-Colored Scarves
12:15 am | Robot Chicken | Hemlock, Gin, and Juice
12:30 am | Smiling Friends | Who Violently Murdered Simon S. Salty?
12:45 am | Aqua Teen | Shirt Herpes
 1:00 am | Aqua Teen | Rocket Horse and Jet Chicken
 1:15 am | Squidbillies | Vicki
 1:30 am | Rick and Morty | Morty's Mind Blowers
 2:00 am | Rick and Morty | The ABC's of Beth
 2:30 am | Futurama | That's Lobstertainment!
 3:00 am | Futurama | Birdbot of Ice-Catraz
 3:30 am | Joe Pera Talks With You | Joe Pera Builds a Chair With You
 4:00 am | Bob's Burgers | The Land Ship
 4:30 am | Bob's Burgers | The Hauntening
 5:00 am | King Of the Hill | Little Horrors of Shop
 5:30 am | King Of the Hill | Aisle 8A