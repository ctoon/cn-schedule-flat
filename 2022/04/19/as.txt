# 2022-04-19
 8:00 pm | King Of the Hill | The Exterminator
 8:30 pm | King Of the Hill | Luanne Virgin 2.0
 9:00 pm | Bob's Burgers | Sea Me Now
 9:30 pm | Bob's Burgers | Teen-a Witch
10:00 pm | American Dad | Seizures Suit Stanny
10:30 pm | American Dad | Roots
11:00 pm | American Dad | The Life Aquatic with Steve Smith
11:30 pm | Rick and Morty | Childrick of Mort
12:00 am | Robot Chicken | Secret of the Booze
12:15 am | Robot Chicken | Rebel Appliance
12:30 am | Smiling Friends | A Silly Halloween Special
12:45 am | Aqua Teen | Freda
 1:00 am | Aqua Teen | Storage Zeebles
 1:15 am | Squidbillies | The Knights of the Noble Order of the Mystic Turquoise Goblet
 1:30 am | Rick and Morty | Childrick of Mort
 2:00 am | Rick and Morty | Star Mort Rickturn of the Jerri
 2:30 am | Futurama | 30% Iron Chef
 3:00 am | Futurama | Where No Fan Has Gone Before
 3:30 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 3:45 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 4:00 am | Bob's Burgers | Sea Me Now
 4:30 am | Bob's Burgers | Teen-a Witch
 5:00 am | King Of the Hill | The Exterminator
 5:30 am | King Of the Hill | Luanne Virgin 2.0