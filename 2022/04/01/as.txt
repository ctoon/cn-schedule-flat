# 2022-04-01
 8:00 pm | King Of the Hill | Wings of the Dope
 8:30 pm | King Of the Hill | Take Me out of the Ball Game
 9:00 pm | Futurama | The Beast with a Billion Backs Part 3
 9:30 pm | Futurama | The Beast with a Billion Backs Part 4
10:00 pm | American Dad | Max Jets
10:30 pm | American Dad | Naked to the Limit, One More Time
11:00 pm | American Dad | For Black Eyes Only
11:30 pm | Rick and Morty | The Ricklantis Mixup
12:00 am | Robot Chicken | May Cause Indecision...Or Not			
12:15 am | Robot Chicken | May Cause Bubbles Where You Don't Want 'Em			
12:30 am | Robot Chicken | May Cause a Squeakquel			
12:45 am | Robot Chicken | May Cause Involuntary Political Discharge			
 1:00 am | Robot Chicken | May Cause an Excess of Ham			
 1:15 am | Robot Chicken | May Cause Internal Diarrhea			
 1:30 am | Robot Chicken | May Cause Indecision...Or Not			
 1:45 am | Robot Chicken | May Cause Bubbles Where You Don't Want 'Em			
 2:00 am | Rick and Morty | Morty's Mind Blowers
 2:30 am | Futurama | The Beast with a Billion Backs Part 3
 3:00 am | Futurama | The Beast with a Billion Backs Part 4
 3:30 am | Infomercials | Final Deployment 4: Queen Battle Walkthrough
 4:00 am | King Of the Hill | As Old as the Hills (1)
 4:30 am | King Of the Hill | Peggy Hill: the Decline and Fall (2)
 5:00 am | King Of the Hill | Wings of the Dope
 5:30 am | King Of the Hill | Take Me out of the Ball Game