# 2022-09-13
 6:00 am | Pocoyo | Pocoyo's Car; Inventions; My Hero
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Silly Little Song; The; ABC Song - Learn Phonics; Penguin Dance; the; Recycle Stomp
 8:15 am | Blippi Wonders | Blippi Learns Rainbow Colors; Squirrels; Mountain; Sneeze
 8:30 am | Thomas & Friends: All Engines Go | The Tiger Train; Surprise!; A Wide Delivery
 9:00 am | Mecha Builders | Cross That Bridgeknock Knock! Who's There?
 9:30 am | Bugs Bunny Builders | Tweety-Go-Round; Ice Creamed
10:00 am | Lucas the Spider | Lonesome Lucasi Can't See Youdon't Eat Me
10:30 am | Thomas & Friends: All Engines Go | Counting Cows; Can-Do Submarine Crew
11:00 am | Total DramaRama | Baby Brother Blues; Encore'tney
11:30 am | Total DramaRama | Space Codyty; Life of Pie
12:00 pm | Craig of the Creek | The Kelsey the Worthy; Time Capsule
12:30 pm | Craig of the Creek | End Was Here; The; Beyond the Rapids
 1:00 pm | Teen Titans Go! | TV Knight 6; Toddler Titans...Yay!
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 2:00 pm | Amazing World of Gumball | The Choices; The Sorcerer
 2:30 pm | Amazing World of Gumball | The Code; The Console
 3:00 pm | Amazing World of Gumball | The Test; The Outside
 3:30 pm | Amazing World of Gumball | The Copycats; The Slide
 4:00 pm | Craig of the Creek | Adventures in Baby Casino; Hyde & Zeke
 4:30 pm | Craig of the Creek | Grandma Smugglers; Lost & Found
 5:00 pm | Teen Titans Go! | Natural Gas
 5:15 pm | Teen Titans Go! | Jump City Rock
 5:30 pm | Teen Titans Go! | Cool Uncles; Superhero Feud
 6:00 pm | Adventure Time | Love Games / Dungeon Train
 6:30 pm | Regular Show | Karaoke Video; Slam Dunk
 7:00 pm | Courage the Cowardly Dog | The Tower of Dr. Zalost Part 1; Thetower of Dr. Zalost Part 2
 7:30 pm | Courage the Cowardly Dog | The Mask