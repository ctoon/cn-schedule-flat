# 2022-09-20
 8:00 pm | King Of the Hill | Trans-Fascism
 8:30 pm | King Of the Hill | Three Men And A Bastard
 9:00 pm | Bob's Burgers | Boywatch
 9:30 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
10:00 pm | American Dad | Ghost Dad
10:30 pm | American Dad | Men ll Boyz
11:00 pm | American Dad | First, Do No Farm
11:30 pm | Rick and Morty | Mortyplicity
12:00 am | Mike Tyson Mysteries | Night Moves
12:15 am | Mike Tyson Mysteries | Ty-Stunned
12:30 am | Aqua Teen | Rocket Horse and Jet Chicken
12:45 am | Metalocalypse | Bluesklok
 1:00 am | The Boondocks | I Dream of Siri
 1:30 am | Rick and Morty | Mortyplicity
 2:00 am | Futurama | Bender's Big Score Part 2
 2:30 am | Futurama | Bender's Big Score Part 3
 3:00 am | Mike Tyson Mysteries | Night Moves
 3:15 am | Mike Tyson Mysteries | Ty-Stunned
 3:30 am | Aqua Teen | Rocket Horse and Jet Chicken
 3:45 am | Metalocalypse | Bluesklok
 4:00 am | Bob's Burgers | Boywatch
 4:30 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 5:00 am | King Of the Hill | Trans-Fascism
 5:30 am | King Of the Hill | Three Men And A Bastard