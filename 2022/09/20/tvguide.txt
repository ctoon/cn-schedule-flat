# 2022-09-20
 6:00 am | Pocoyo | Dragon Island; Swept Away; Who's on the Phone?
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Happy Place; Old Macdonald; Please and Thank You Song; Wheels on the Bus
 8:15 am | Blippi Wonders | Snowflake; Garbage Truck; Ducks; Panda
 8:30 am | Thomas & Friends: All Engines Go | Letting off Steam; Nia's Surprising Surprise
 9:00 am | Mecha Builders | Mecha Builders Pull Togetherlift up and Lift Off
 9:30 am | Bugs Bunny Builders | Snow Cap; Smash House
10:00 am | Lucas the Spider | I'm Taking You Insomething up Therehome Sweet Home
10:30 am | Thomas & Friends: All Engines Go | Nia's Perfect Plan; A New View for Thomas
11:00 am | Total DramaRama | Weekend at Buddy's; Dial B for Birder
11:30 am | Total DramaRama | Macarthur Park; A Hole Lot of Trouble
12:00 pm | Craig of the Creek | Ice Pop Trio; The Pencil Break Mania
12:30 pm | Craig of the Creek | Last Game of Summer; The Winter Creeklympics
 1:00 pm | Teen Titans Go! | I Am Chair; Huggbees
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 2:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 2:30 pm | Amazing World of Gumball | The Fraud; The Void
 3:00 pm | Amazing World of Gumball | The Move; The Boss
 3:30 pm | Amazing World of Gumball | The Allergy; The Law
 4:00 pm | Craig of the Creek | Itch to Explore; You're It
 4:30 pm | Craig of the Creek | Too Many Treasures; Jessica Goes to the Creek
 5:00 pm | Teen Titans Go! | Creative Geniuses; Polly Ethylene and Tara Phthalate
 5:30 pm | Teen Titans Go! | Batman's Birthday Gift; Manor and Mannerisms
 6:00 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | Five Short Graybles; Dad's Dungeon
 6:30 pm | Regular Show | Meteor Moves; Bank Shot
 7:00 pm | Courage the Cowardly Dog | King Ramses Curse; The Clutching Foot
 7:30 pm | Courage the Cowardly Dog | Son of the Chicken From Outer Space; The Last Starmakers