# 2022-09-24
 6:00 am | Teen Titans Go! | Little Buddies; Missing
 6:30 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 7:00 am | Teen Titans | The Prophecy
 7:30 am | Teen Titans | Stranded
 8:00 am | Teen Titans Go! | Dreams; Grandma Voice
 8:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 9:00 am | We Baby Bears | Triple T Tigers
 9:30 am | We Baby Bears | Tooth Fairy Tech
10:00 am | Total DramaRama | Robo Teacher
10:30 am | Total DramaRama | Supply Mom
11:00 am | Total DramaRama | Mooshy Mon Mons
11:30 am | Total DramaRama | Student Becomes the Teacher
12:00 pm | Teen Titans Go! | Mr. Butt; Man Person
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Teen Titans Go! | Brian; Nature
 1:30 pm | Teen Titans Go! | Salty Codgers; Knowledge
 2:00 pm | Craig of the Creek | Chrono Moss
 2:30 pm | Craig of the Creek | In Search of Lore
 3:00 pm | Craig of the Creek | Opposite Day
 3:30 pm | Craig of the Creek | Adventures in Baby Casino
 4:00 pm | Amazing World of Gumball | The Cycle
 4:30 pm | Amazing World of Gumball | The Stars
 5:00 pm | Amazing World of Gumball | The Grades
 5:30 pm | Amazing World of Gumball | The Agent
 5:45 pm | Megamind | 
 7:45 pm | Megamind: The Button of Doom | 