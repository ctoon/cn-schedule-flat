# 2022-09-24
 8:00 pm | King Of the Hill | It Came From the Garage
 8:30 pm | King Of the Hill | Life: A Loser's Manual
 9:00 pm | Futurama | The Silence of the Clamps
 9:30 pm | Futurama | Yo Leela Leela
10:00 pm | American Dad | Helping Handis
10:30 pm | American Dad | With Friends Like Steve's
11:00 pm | American Dad | Tears of a Clooney
11:30 pm | Rick and Morty | Bethic Twinstinct
12:00 am | Genndy Tartakovsky's Primal | The Primal Theory
12:30 am | Yashahime: Princess Half-Demon - The Second Act | Nanahoshi's Mini Galaxy
 1:00 am | Lupin the 3rd Part 6 | Welcome to the Island of Bubbles
 1:30 am | One Piece | Save the Children! The Straw Hats Start to Fight!
 2:00 am | One Piece | A Swordplay Showdown! Brook vs. the Mysterious Torso Samurai!
 2:30 am | Naruto:Shippuden | The Targeted Tailed Beast
 3:00 am | Made In Abyss | Incinerator
 3:30 am | Made In Abyss | Seeker Camp
 4:00 am | Futurama | The Silence of the Clamps
 4:30 am | Futurama | Yo Leela Leela
 5:00 am | King Of the Hill | It Came From the Garage
 5:30 am | King Of the Hill | Life: A Loser's Manual