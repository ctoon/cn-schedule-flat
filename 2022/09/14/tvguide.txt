# 2022-09-14
 6:00 am | Pocoyo | Rescue; The; Pocoyo's Friend; Elly's Picnic
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Shake the Apple Tree; Baa Baa Black Sheep; Accidents Happen Boo Boo Song; Salad Salsa
 8:15 am | Blippi Wonders | Honey; Birds Nest; Garbage Truck; Spiderweb
 8:30 am | Thomas & Friends: All Engines Go | Eggsellent Adventure; Percy Disappears
 9:00 am | Mecha Builders | Moo-Ving Day! Follow That Breeze
 9:30 am | Bugs Bunny Builders | Dino Fright; Smash House
10:00 am | Lucas the Spider | Slidingspinning Webs
10:30 am | Thomas & Friends: All Engines Go | Backwards Day; Calliope Crack-Up
11:00 am | Total DramaRama | Abaracaduncan; I Dream of Meanie
11:30 am | Total DramaRama | Shock & Aww; Squirrels Squirrels Squirrels
12:00 pm | Craig of the Creek | Jessica Shorts in the Key of the Creek
12:30 pm | Craig of the Creek | The Ferret Quest; Ground Is Lava!
 1:00 pm | Teen Titans Go! | Garage Sale; Secret Garden
 1:30 pm | Teen Titans Go! | The Cruel Giggling Ghoul; Bottle Episode
 2:00 pm | Amazing World of Gumball | The Loophole; The Catfish
 2:30 pm | Amazing World of Gumball | The Cycle; The Fuss
 3:00 pm | Amazing World of Gumball | The News; The Stars
 3:30 pm | Amazing World of Gumball | The Box; The Vase
 4:00 pm | Craig of the Creek | Bored Games; Silver Fist Returns
 4:30 pm | Craig of the Creek | The Champion's Hike; Scoutguest
 5:00 pm | Teen Titans Go! | 50%; Chad
 5:15 pm | Teen Titans Go! | Natural Gas
 5:30 pm | Teen Titans Go! | We're off to Get Awards; Kryptonite
 6:00 pm | Adventure Time | Thanks for the Crabapples; Guiseppe Joshua & Margaret Investigations
 6:30 pm | Regular Show | Weekend at Benson's; Video Game Wizard
 7:00 pm | The Grim Adventures of Billy and Mandy | Nigel Planter and the Chamber Pot of Secrets; Circus of Fear
 7:30 pm | The Grim Adventures of Billy and Mandy | A Grim Day; Pandora's Lunch Box