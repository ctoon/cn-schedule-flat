# 2022-09-04
 6:00 am | Ben 10 | Ben 10 vs. the Negative 10: Part 1
 6:30 am | Ben 10 | Ben 10 vs. the Negative 10: Part 2
 7:00 am | Ben 10: Alien Force | Ben 10 Returns, Part 1
 7:30 am | Ben 10: Alien Force | Ben 10 Returns, Part 2
 8:00 am | Amazing World of Gumball | The Origins; The Origins Part 2
 8:30 am | Amazing World of Gumball | The Disaster; The Re-run
 9:00 am | Teen Titans Go! | Super Summer Hero Camp
10:00 am | Teen Titans Go! | Space House
11:00 am | Total DramaRama | Camping Is In Tents
11:30 am | Total DramaRama | Dissing Cousins
12:00 pm | Craig Of The Creek | The Other Side
12:30 pm | Craig of the Creek | The Other Side: The Tournament
 1:00 pm | Craig of the Creek | Winter Break
 1:30 pm | Teen Titans | Homecoming, Part 1
 2:00 pm | Teen Titans | Homecoming, Part 2
 2:30 pm | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 3:00 pm | Teen Titans Go! | Night Begins to Shine 2: You're the One
 4:15 pm | Teen Titans Go! Vs. Teen Titans | 
 6:00 pm | The Croods | 