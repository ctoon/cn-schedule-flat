# 2022-09-08
 6:00 am | Pocoyo | Dragon Island; Pato's Phone; Robot Pocoyo
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Recycle Stomp; Bedtime Song; Shake the Apple Tree; Happy Place
 8:15 am | Blippi Wonders | Australia Kangaroo; Birds Nest; Honey; Spiderweb
 8:30 am | Thomas & Friends: All Engines Go | The Quiet Delivery; A; Joke Is on Thomas
 9:00 am | Mecha Builders | The Pinecone Paloozabig Footed Cave Bird
 9:30 am | Bugs Bunny Builders | Splash Zone; Ice Creamed
10:00 am | Lucas the Spider | Ghost Campboonever Boop a Robot
10:30 am | Thomas & Friends: All Engines Go | Kana Goes Slow; Lost and Found
11:00 am | Total DramaRama | Jelly Aches; Bad Seed
11:30 am | Total DramaRama | A Simply Perfect; Fish Called Leshawna
12:00 pm | Craig of the Creek | Kelsey the Elder; Cousin of the Creek
12:30 pm | Craig of the Creek | Sparkle Cadet; Sleepover at Jp's
 1:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 2:00 pm | Amazing World of Gumball | The Origins; The Origins Part 2
 2:30 pm | Amazing World of Gumball | The Signal; The Misunderstandings
 3:00 pm | Amazing World of Gumball | The Girlfriend; The Roots
 3:30 pm | Amazing World of Gumball | The Parasite; The Blame
 4:00 pm | Craig of the Creek | Chef's Challenge; The; Fire & Ice
 4:30 pm | Craig of the Creek | Quick Name; The; Dodgy Decisions
 5:00 pm | Teen Titans Go! | Girls Night In
 5:30 pm | Teen Titans Go! | The Viewers Decide; The Great Disaster
 6:00 pm | Adventure Time | Crystals Have Power Finn the Human
 6:30 pm | Regular Show | Ello Gov'nor; Eggscellent
 7:00 pm | Ed, Edd n Eddy | Don't Rain on My Ed; Once Bitten, Twice Ed
 7:30 pm | Ed, Edd n Eddy | Thick as an Ed; Sorry, Wrong Ed