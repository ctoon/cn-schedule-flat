# 2022-09-09
 6:00 am | Pocoyo | Fancy Dress Party
 6:30 am | Cocomelon | My Daddy Song
 7:00 am | Cocomelon | Shadow Puppets
 7:30 am | Cocomelon | Pretend Play
 8:00 am | Lellobee City Farm | Accidents Happen Boo Boo Song
 8:15 am | Blippi Wonders | Squirrels
 8:30 am | Thomas & Friends: All Engines Go | Dragon Run
 9:00 am | Mecha Builders | Let's Get Rolling!; A Bit of a Stretch!
 9:30 am | Bugs Bunny Builders | Dino Fright
10:00 am | Lucas the Spider | Puppy Love; House Grand Prix; Farewell Sweet Ride
10:30 am | Thomas & Friends: All Engines Go | The Biggest Adventure Club
11:00 am | Total DramaRama | Legends of the Paul
11:30 am | Total DramaRama | Total Trauma Rama
12:00 pm | Craig of the Creek | The Sparkle Solution
12:30 pm | Craig of the Creek | Better Than You
 1:00 pm | Teen Titans Go! | Superhero Feud
 1:30 pm | Teen Titans Go! | Real Art
 2:00 pm | Amazing World of Gumball | The Love
 2:30 pm | Amazing World of Gumball | The Night
 3:00 pm | Amazing World of Gumball | The Awkwardness
 3:30 pm | Amazing World of Gumball | The Nest
 4:00 pm | Craig of the Creek | The Dream Team
 4:30 pm | Craig of the Creek | Opposite Day
 5:00 pm | Teen Titans Go! | Magic Man
 5:30 pm | Teen Titans Go! | Record Book
 6:00 pm | Regular Show | Gut Model
 6:30 pm | Regular Show | The Best Burger in the World
 7:00 pm | Regular Show | Butt Dial
 7:30 pm | Regular Show | Fists of Justice