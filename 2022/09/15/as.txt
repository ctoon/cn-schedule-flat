# 2022-09-15
 8:00 pm | King Of the Hill | Bill, Bulk and the Body Buddies
 8:30 pm | King Of the Hill | Lucky's Wedding Suit
 9:00 pm | Bob's Burgers | Cheer Up Sleepy Gene
 9:30 pm | Bob's Burgers | The Trouble with Doubles
10:00 pm | American Dad | Downtown
10:30 pm | American Dad | Cheek to Cheek: A Stripper's Story
11:00 pm | American Dad | A Starboy is Born
11:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
12:00 am | Genndy Tartakovsky's Primal | Echoes of Eternity
12:30 am | Aqua Teen | Chicken and Beans
12:45 am | Metalocalypse | Murdering Outside the Box
 1:00 am | The Boondocks | Granddad Dates a Kardashian
 1:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 2:00 am | Futurama | Obsoletely Fabulous
 2:30 am | Futurama | Bender Should Not Be Allowed on TV
 3:00 am | Genndy Tartakovsky's Primal | Echoes of Eternity
 3:30 am | Off The Air | Bugs
 3:45 am | Off The Air | Earth
 4:00 am | Bob's Burgers | Cheer Up Sleepy Gene
 4:30 am | Bob's Burgers | The Trouble with Doubles
 5:00 am | King Of the Hill | Bill, Bulk and the Body Buddies
 5:30 am | King Of the Hill | Lucky's Wedding Suit