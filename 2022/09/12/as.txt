# 2022-09-12
 8:00 pm | King Of the Hill | Hank Gets Dusted
 8:30 pm | King Of the Hill | Glen Peggy Glen Ross
 9:00 pm | Bob's Burgers | Sit Me Baby One More Time
 9:30 pm | Bob's Burgers | V for Valentine-detta
10:00 pm | Rick and Morty | Mortyplicity
10:30 pm | American Dad | Hamerican Dad!
11:00 pm | American Dad | Demolition Daddy
11:30 pm | Rick and Morty | Rick: A Mort Well Lived
12:00 am | Mike Tyson Mysteries | The End
12:15 am | Mike Tyson Mysteries | Ultimate Judgment Day
12:30 am | Aqua Teen | Lasagna
12:45 am | Metalocalypse | Mordland
 1:00 am | The Boondocks | Breaking Granddad
 1:30 am | Rick and Morty | Rick: A Mort Well Lived
 2:00 am | Futurama | The Why of Fry
 2:30 am | Futurama | The Sting
 3:00 am | Mike Tyson Mysteries | The End
 3:15 am | Mike Tyson Mysteries | Ultimate Judgment Day
 3:30 am | Off The Air | Bugs
 3:45 am | Off The Air | Animals
 4:00 am | Bob's Burgers | Sit Me Baby One More Time
 4:30 am | Bob's Burgers | V for Valentine-detta
 5:00 am | King Of the Hill | Hank Gets Dusted
 5:30 am | King Of the Hill | Glen Peggy Glen Ross