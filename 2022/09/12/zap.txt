# 2022-09-12
 6:00 am | Pocoyo | Pocoyo's Balloon
 6:30 am | Cocomelon | Getting Ready for School Song
 7:00 am | Cocomelon | Yes Yes Playground Song
 7:30 am | Cocomelon | Johny Johny Yes Papa
 8:00 am | Lellobee City Farm | Happy Place
 8:15 am | Blippi Wonders | Ducks
 8:30 am | Thomas & Friends: All Engines Go | Percy Disappears
 9:00 am | Mecha Builders | The Pinecone Palooza; Big Footed Cave Bird
 9:30 am | Bugs Bunny Builders | Play Day
10:00 am | Meet the Batwheels | Calling All Batwheels
10:05 am | Lucas the Spider | The Mighty Boop and Fly Wonder; Lucas and Findley get Antsy; Mock Me Not
10:30 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
11:00 am | Total DramaRama | Ghoul Spirit
11:30 am | Total DramaRama | Snack to the Future
12:00 pm | Craig of the Creek | The Cardboard Identity
12:30 pm | Craig of the Creek | Ancients of the Creek
 1:00 pm | Teen Titans Go! | Go!
 1:30 pm | Teen Titans Go! | The Perfect Pitch?
 2:00 pm | Amazing World of Gumball | The Boombox; The Castle
 2:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 3:00 pm | Amazing World of Gumball | The Rival
 3:30 pm | Amazing World of Gumball | The Vegging
 4:00 pm | Craig of the Creek | Chrono Moss
 4:30 pm | Craig of the Creek | Creek Talent Extravaganza
 5:00 pm | Teen Titans Go! | Jump City Rock
 5:15 pm | Teen Titans Go! | TV Knight 7
 5:30 pm | Teen Titans Go! | Kyle
 6:00 pm | Adventure Time | James Baxter the Horse
 6:30 pm | Regular Show | Just Set Up the Chairs
 7:00 pm | Codename: Kids Next Door | Operation E.N.D.
 7:30 pm | Codename: Kids Next Door | Operation P.O.P.; Operation C.A.T.S.