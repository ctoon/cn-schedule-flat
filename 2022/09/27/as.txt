# 2022-09-27
 9:00 pm | Bob's Burgers | Sexy Dance Fighting
 9:30 pm | Bob's Burgers | Hamburger Dinner Theater
10:00 pm | American Dad | Lincoln Lover
10:30 pm | American Dad | Dungeons and Wagons
11:00 pm | American Dad | Iced, Iced Babies
11:30 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
12:00 am | Mike Tyson Mysteries | Old Man of the Mountain
12:15 am | Mike Tyson Mysteries | Jason B. Sucks
12:30 am | Aqua Teen | Buddy Nugget
12:45 am | Metalocalypse | Girlfriendklok
 1:00 am | The Boondocks | The Trial of Robert Kelly
 1:30 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 2:00 am | Futurama | Bender's Game Part 2
 2:30 am | Futurama | Bender's Game Part 3
 3:00 am | Mike Tyson Mysteries | Old Man of the Mountain
 3:15 am | Mike Tyson Mysteries | Jason B. Sucks
 3:30 am | Aqua Teen | Buddy Nugget
 3:45 am | Metalocalypse | Girlfriendklok
 4:00 am | Bob's Burgers | Sexy Dance Fighting
 4:30 am | Bob's Burgers | Hamburger Dinner Theater
 5:00 am | King Of the Hill | No Bobby Left Behind
 5:30 am | King Of the Hill | A Bill Full Of Dollars