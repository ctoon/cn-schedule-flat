# 2022-09-27
 6:00 am | Mecha Builders | Cross That Bridge; Knock Knock! Who's There?
 6:30 am | Cocomelon | Skidamarink
 7:00 am | Cocomelon | Tap Dancing Song
 7:30 am | Cocomelon | Rain Rain Go Away Indoors Version
 8:00 am | Lellobee City Farm | Baa Baa Black Sheep
 8:15 am | Blippi Wonders | Flies
 8:30 am | Thomas & Friends: All Engines Go | Ghost-Scaring Machine
 9:00 am | Mecha Builders | Yip Yip Book Book; Stick To It
 9:30 am | Bugs Bunny Builders | Buzz In
 9:45 am | Bugs Bunny Builders | Rock On
10:00 am | Thomas & Friends: All Engines Go | License to Deliver
10:30 am | Thomas & Friends: All Engines Go | More Cowbell
11:00 am | Total DramaRama | CodE.T.
11:30 am | Total DramaRama | Quiche It Goodbye
12:00 pm | Craig of the Creek | Cousin of the Creek
12:30 pm | Craig of the Creek | Camper on the Run
 1:00 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 1:30 pm | Teen Titans Go! | EEbows
 2:00 pm | Amazing World of Gumball | The Neighbor
 2:30 pm | Amazing World of Gumball | The Pact
 3:00 pm | Amazing World of Gumball | The Faith
 3:30 pm | Amazing World of Gumball | The Candidate
 4:00 pm | Craig of the Creek | Kelsey the Elder
 4:30 pm | Craig of the Creek | Sour Candy Trials
 5:00 pm | Teen Titans Go! | Cy & Beasty
 5:30 pm | Teen Titans Go! | Creative Geniuses
 6:00 pm | Adventure Time | Mystery Train; Go With Me
 6:30 pm | Adventure Time | Belly of the Beast; The Limit
 7:00 pm | Courage the Cowardly Dog | Ball of Revenge; Courageous Cure
 7:30 pm | Courage the Cowardly Dog | Fishy Business; Angry Nasty People
 8:00 pm | Regular Show | Peeps
 8:30 pm | Regular Show | Dizzy