# 2022-09-29
 9:00 pm | Bob's Burgers | Art Crawl
 9:30 pm | Bob's Burgers | Spaghetti Western and Meatballs
10:00 pm | American Dad | American Dream Factory
10:30 pm | American Dad | A.T. the Abusive Terrestrial
11:00 pm | American Dad | Black Mystery Month
11:30 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
12:00 am | Genndy Tartakovsky's Primal | Shadow of Fate
12:30 am | Aqua Teen | Totem Pole
12:45 am | Metalocalypse | The Metalocalypse Has Begun
 1:00 am | The Boondocks | Granddad's Fight
 1:30 am | Rick and Morty | Gotron Jerrysis Rickvangelion
 2:00 am | Futurama | Into the Wild Green Yonder Part 2
 2:30 am | Futurama | Into the Wild Green Yonder Part 3
 3:00 am | Genndy Tartakovsky's Primal | Shadow of Fate
 3:30 am | Aqua Teen | Totem Pole
 3:45 am | Metalocalypse | The Metalocalypse Has Begun
 4:00 am | Bob's Burgers | Art Crawl
 4:30 am | Bob's Burgers | Spaghetti Western and Meatballs
 5:00 am | King Of the Hill | What Happens at the National Propane Gas Convention
 5:30 am | King Of the Hill | Master of Puppets I'm Pulling Your Strings!