# 2022-09-22
 8:00 pm | King Of the Hill | Behind Closed Doors
 8:30 pm | King Of the Hill | Pour Some Sugar on Kahn
 9:00 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 9:30 pm | Bob's Burgers | Human Flesh
10:00 pm | American Dad | The Chilly Thrillies
10:30 pm | American Dad | Dammmm, Stan!
11:00 pm | American Dad | The Last Ride of the Dodge City Rambler
11:30 pm | Rick and Morty | A Rickconvenient Mort
12:00 am | Genndy Tartakovsky's Primal | Sea of Despair
12:30 am | Aqua Teen | Bookie
12:45 am | Metalocalypse | Dethkids
 1:00 am | The Boondocks | The New Black
 1:30 am | Rick and Morty | A Rickconvenient Mort
 2:00 am | Futurama | The Beast with a Billion Backs Part 2
 2:30 am | Futurama | The Beast with a Billion Backs Part 3
 3:00 am | Genndy Tartakovsky's Primal | Sea of Despair
 3:30 am | Aqua Teen | Bookie
 3:45 am | Metalocalypse | Dethkids
 4:00 am | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 4:30 am | Bob's Burgers | Human Flesh
 5:00 am | King Of the Hill | Behind Closed Doors
 5:30 am | King Of the Hill | Pour Some Sugar on Kahn