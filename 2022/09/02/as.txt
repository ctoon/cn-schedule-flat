# 2022-09-02
 8:00 pm | King Of the Hill | Redcorn Gambles With His Future
 8:30 pm | King Of the Hill | Smoking and the Bandit
 9:00 pm | Futurama | Free Will Hunting
 9:30 pm | Futurama | Near-Death Wish
10:00 pm | American Dad | OreTron Trail
10:30 pm | American Dad | Mean Francine
11:00 pm | Rick and Morty | Mort Dinner Rick Andre
11:30 pm | Rick and Morty | Mortyplicity
12:00 am | Rick and Morty | A Rickconvenient Mort
12:30 am | Rick and Morty | Rickdependence Spray
 1:00 am | Rick and Morty | Amortycan Grickfitti
 1:30 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 2:00 am | Rick and Morty | Gotron Jerrysis Rickvangelion
 2:30 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
 3:00 am | Rick and Morty | Forgetting Sarick Mortshall
 3:30 am | Rick and Morty | Rickmurai Jack
 4:00 am | King Of the Hill | Gone With the Windstorm
 4:30 am | King Of the Hill | Bobby On Track
 5:00 am | King Of the Hill | Redcorn Gambles With His Future
 5:30 am | King Of the Hill | Smoking and the Bandit