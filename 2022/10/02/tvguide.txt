# 2022-10-02
 6:00 am | Lamput | Shrunk Docs Rock Concert Boss's Mom #336 Boss Stache #339 Opera
 6:30 am | Teen Titans Go! | Leg Day; Cat's Fancy
 7:00 am | Teen Titans Go! | Body Adventure; Boys vs Girls
 7:30 am | Teen Titans Go! | The Best Robin; Road Trip
 8:00 am | Amazing World of Gumball | The Ad; The Future
 8:30 am | Amazing World of Gumball | The Slip; The Wish
 9:00 am | We Baby Bears | Magical Box; The; Bears and the Beanstalk
 9:30 am | We Baby Bears | The Boo-Dunnit; Little Mer-Bear
10:00 am | Amazing World of Gumball | The Drama; The Factory
10:30 am | Amazing World of Gumball | The Buddy; The Revolt
11:00 am | Amazing World of Gumball | The Master; The Mess
11:30 am | Amazing World of Gumball | The Silence; The Possession
12:00 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
12:30 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 1:00 pm | Teen Titans Go! | Real Boy Adventures; Smile Bones
 1:30 pm | Teen Titans Go! | Hose Water; Yearbook Madness
 2:00 pm | Teen Titans Go! | Tamaranian Vacation; Let's Get Serious
 2:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 3:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 3:30 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 4:00 pm | Teen Titans Go! | Cool School; Video Game References
 4:30 pm | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
 5:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 5:30 pm | Teen Titans Go! | Pepo the Pumpkinman; Costume Contest
 6:00 pm | Monsters vs. Aliens | 
 8:00 pm | Bob's Burgers | Pro Tiki/Con Tiki
 8:30 pm | Bob's Burgers | Bye Bye Boo Boo