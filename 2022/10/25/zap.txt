# 2022-10-25
 6:00 am | Amazing World of Gumball | The Apprentice
 6:30 am | Amazing World of Gumball | The Check
 7:00 am | Cocomelon | Balloon Boat Race
 7:30 am | Cocomelon | Johny Johny Yes Papa
 8:00 am | Lellobee City Farm | Accidents Happen Boo Boo Song
 8:15 am | Blippi Wonders | Chocolate Factory
 8:30 am | Thomas & Friends: All Engines Go | Nia's Surprising Surprise
 9:00 am | Mecha Builders | Cross That Bridge; Knock Knock! Who's There?
 9:30 am | Bugs Bunny Builders | Rock On
10:00 am | Meet the Batwheels | Stop That Ducky!
10:30 am | Thomas & Friends: All Engines Go | Ghost Train
11:00 am | Scooby-Doo and Guess Who? | Lost Soles Of Jungle River!
11:30 am | Scooby-Doo and Guess Who? | The Tao Of Scoob!
12:00 pm | Craig of the Creek | Creek Talent Extravaganza
12:30 pm | Craig of the Creek | Dodgy Decisions
 1:00 pm | Teen Titans Go! | Two Parter: Part One
 1:30 pm | Teen Titans Go! | Garage Sale
 2:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:30 pm | Amazing World of Gumball | The Downer; The Egg
 3:00 pm | Shrek | 
 5:00 pm | Lemony Snicket's A Series of Unfortunate Events | 
 7:00 pm | Scooby-Doo and the Goblin King | 