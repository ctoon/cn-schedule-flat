# 2022-10-24
 9:00 pm | Bob's Burgers | O.T. the Outside Toilet
 9:30 pm | Bob's Burgers | The Wolf of Wharf Street
10:00 pm | Rick and Morty | Vindicators 3: The Return of Worldender
10:30 pm | American Dad | Hot Water
11:00 pm | American Dad | Best Little Horror House in Langley Falls
11:30 pm | Rick and Morty | Rick Potion #9
12:00 am | Mike Tyson Mysteries | Mystery on Wall Street
12:15 am | Mike Tyson Mysteries | A Dog's Life
12:30 am | Aqua Teen | Bus of the Undead
12:45 am | Aqua Teen | MC Pee Pants
 1:00 am | The Boondocks | Thank You for Not Snitching
 1:30 am | Rick and Morty | Rick Potion #9
 2:00 am | American Dad | The Devil Wears a Lapel Pin
 2:30 am | Futurama | Over Clock Wise
 3:00 am | Lucy: Daughter of the Devil | Escapeoke
 3:15 am | Mary Shelley's Frankenhole | Bram Stoker's Loud Mouths!
 3:30 am | The Shivering Truth | Holeways
 3:45 am | The Heart She Holler | An Emotional Can of Mommyworms
 4:00 am | Bob's Burgers | O.T. the Outside Toilet
 4:30 am | Bob's Burgers | The Wolf of Wharf Street
 5:00 am | King Of the Hill | De-Kahnstructing Henry
 5:30 am | King Of the Hill | The Wedding of Bobby Hill