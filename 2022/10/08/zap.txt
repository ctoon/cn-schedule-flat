# 2022-10-08
 6:00 am | Amazing World of Gumball | The Decisions
 6:30 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 8:00 am | Amazing World of Gumball | The Heart
 8:30 am | Amazing World of Gumball | The BFFS
 9:00 am | We Baby Bears | Witches
 9:15 am | We Baby Bears | Happy Bouncy Fun Town
 9:30 am | We Baby Bears | The Magical Box
10:00 am | Straight Outta Nowhere: Scooby-Doo Meets Courage the Cowardly Dog | 
11:45 am | Teen Titans Go! | Welcome to Halloween
12:00 pm | Teen Titans Go! | The Dignity of Teeth
12:30 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 1:00 pm | Teen Titans Go! | The Spice Game
 1:30 pm | Teen Titans Go! | Halloween
 2:00 pm | Craig of the Creek | Bored Games
 2:30 pm | Craig of the Creek | My Stare Lady
 3:00 pm | Craig of the Creek | Itch to Explore
 3:30 pm | Craig of the Creek | Jessica Goes to the Creek
 4:00 pm | Amazing World of Gumball | The Third; The Debt
 4:30 pm | Amazing World of Gumball | The Pressure; The Painting
 5:00 pm | Amazing World of Gumball | The Responsible; The Dress
 5:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 6:00 pm | Amazing World of Gumball | The Mystery; The Prank
 6:30 pm | Amazing World of Gumball | The Gi; The Kiss
 7:00 pm | R.L. Stine's Mostly Ghostly: Have You Met My Ghoulfriend? | 