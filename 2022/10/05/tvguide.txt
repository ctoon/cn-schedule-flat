# 2022-10-05
 6:00 am | Mecha Builders | Treasure of Treetop Woods; Thezee Makes an Amazing Maze
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Bath Song; ABC Song - Learn Phonics; Old Macdonald; Shake the Apple Tree
 8:15 am | Blippi Wonders | Sandcastle; Volcano; Bats; Panda
 8:30 am | Thomas & Friends: All Engines Go | Tri-and-a-Half-a-Lon; Thomas Blasts Off
 9:00 am | Mecha Builders | Roll Chickens Rollramp up; Up; and Away
 9:30 am | Bugs Bunny Builders | Tweety-Go-Round; Snow Cap
10:00 am | Thomas & Friends: All Engines Go | Joke Is on Thomas; The; Overnight Stop
10:30 am | Thomas & Friends: All Engines Go | License to Deliver; The Super-Long Shortcut
11:00 am | Total DramaRama | Hic Hic Hooray; The Date
11:30 am | Total DramaRama | Duck Duck Juice; Bananas & Cheese
12:00 pm | Craig of the Creek | In the Key of the Creek; Craig of the Beach
12:30 pm | Craig of the Creek | Children of the Dog; The Plush Kingdom
 1:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 1:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | Amazing World of Gumball | The GI; The Kiss
 3:30 pm | Amazing World of Gumball | The Party; The Refund
 4:00 pm | Craig of the Creek | Trading Day; Creek Shorts
 4:30 pm | Craig of the Creek | The Crisis at Elder Rock Ground Is Lava!
 5:00 pm | Teen Titans Go! | 50%; Kyle; Chad
 5:30 pm | Teen Titans Go! | The TV Knight 7; Score
 6:00 pm | Amazing World of Gumball | The Mustache; The Date
 6:30 pm | Amazing World of Gumball | The Club; The Wand
 7:00 pm | Scooby-Doo and Guess Who? | Returning Of The Key Ring!
 7:30 pm | Scooby-Doo and Guess Who? | Cher, Scooby And The Sargasso Sea!
 8:00 pm | Regular Show | Regular Show Presents: Terror Tales of the Park Part III
 8:30 pm | Regular Show | Terror Tales of the Park IV