# 2022-10-19
 6:00 am | Amazing World of Gumball | The Pizza; The Lie
 6:30 am | Amazing World of Gumball | The Ghouls
 7:00 am | Cocomelon | Johny Johny Yes Papa
 7:30 am | Cocomelon | Balloon Boat Race
 8:00 am | Lellobee City Farm | Happy Place
 8:15 am | Blippi Wonders | Snowflake
 8:30 am | Thomas & Friends: All Engines Go | Ghost Train
 9:00 am | Mecha Builders | The Whatsacallit of Treetop Woods; Nat in the Dark!
 9:30 am | Bugs Bunny Builders | Race Track Race
10:00 am | Meet the Batwheels | Stop That Ducky!
10:30 am | Thomas & Friends: All Engines Go | Thomas' Day Off
11:00 am | Scooby-Doo and Guess Who? | The Movieland Monsters!
11:30 am | Scooby-Doo and Guess Who? | The Last Inmate!
12:00 pm | Craig of the Creek | Jessica the Intern
12:30 pm | Craig of the Creek | Beyond the Overpass
 1:00 pm | Teen Titans Go! | Video Game References; Cool School
 1:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | Halloween
 3:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:30 pm | Amazing World of Gumball | The Fraud; The Void
 4:00 pm | Craig of the Creek | Trick or Creek
 4:30 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 5:00 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
 5:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 6:00 pm | Amazing World of Gumball | The Mirror; The Burden
 6:30 pm | Amazing World of Gumball | The Bros; The Man
 7:00 pm | Scooby-Doo and Guess Who? | The Horrible Haunted Hospital of Dr. Phineas Phrag!
 7:30 pm | Scooby-Doo and Guess Who? | The Dreaded Remake Of Jekyll & Hyde
 8:00 pm | Courage the Cowardly Dog | Human Habitrail; Mission to the Sun
 8:30 pm | Courage the Cowardly Dog | Queen of the Black Puddle; Everyone Wants to Direct