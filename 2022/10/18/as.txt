# 2022-10-18
 9:00 pm | Bob's Burgers | The Unbearable Like-Likeness of Gene
 9:30 pm | Bob's Burgers | Mother Daughter Laser Razor
10:00 pm | American Dad | Man in the Moonbounce
10:30 pm | American Dad | Shallow Vows
11:00 pm | American Dad | My Morning Straitjacket
11:30 pm | Rick and Morty | Rixty Minutes
12:00 am | Mike Tyson Mysteries | Foxcroft Academy for Boys
12:15 am | Mike Tyson Mysteries | A Mine Is a Terrible Thing to Waste
12:30 am | Aqua Teen | Hospice
12:45 am | Aqua Teen | The Greatest Story Ever Told
 1:00 am | The Boondocks | The Passion of Reverend Ruckus
 1:30 am | Rick and Morty | Rixty Minutes
 2:00 am | Futurama | The Silence of the Clamps
 2:30 am | Futurama | Yo Leela Leela
 3:00 am | Lucy: Daughter of the Devil | Satan's School for Girls
 3:15 am | Mary Shelley's Frankenhole | H.G. Wells! Scary Monster Contest!
 3:30 am | The Shivering Truth | Nesslessness
 3:45 am | The Heart She Holler | The Telltale Butthole
 4:00 am | Bob's Burgers | The Unbearable Like-Likeness of Gene
 4:30 am | Bob's Burgers | Mother Daughter Laser Razor
 5:00 am | King Of the Hill | Junkie Business
 5:30 am | King Of the Hill | Life in the Fast Lane, Bobby's Saga