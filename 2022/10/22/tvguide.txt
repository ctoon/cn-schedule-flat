# 2022-10-22
 6:00 am | Amazing World of Gumball | The Fuss; The Console
 6:30 am | Amazing World of Gumball | The Outside; The News
 7:00 am | Amazing World of Gumball | The Vase; The Copycats
 7:30 am | Amazing World of Gumball | The Ollie; The Catfish
 8:00 am | Amazing World of Gumball | The Cycle; The Potato
 8:30 am | Amazing World of Gumball | The Sorcerer; The Stars
 9:00 am | We Baby Bears | Bath on the Nile
 9:15 am | We Baby Bears | Back to Our Regular Time Period
 9:30 am | We Baby Bears | Baby Bear Genius; Bug City Sleuths
10:00 am | Scooby-Doo and the Cyber Chase | 
11:45 am | Teen Titans Go! | Welcome to Halloween
12:00 pm | Teen Titans Go! | The Arms Race With Legs; Art of Ninjutsu
12:30 pm | Teen Titans Go! | Think About Your Future; Obinray
 1:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice; Booty Scooty
 1:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 2:00 pm | Craig of the Creek | JPony; The Invitation
 2:30 pm | Craig of the Creek | Vulture's Nest Ace of Squares
 3:00 pm | Craig of the Creek | Kelsey Quest Doorway to Helen
 3:30 pm | Craig of the Creek | The Haunted Dollhouse; The; Legend of the Library
 4:00 pm | Amazing World of Gumball | Ghouls; The Halloween
 4:30 pm | Amazing World of Gumball | The Promise; The Voice
 5:00 pm | Amazing World of Gumball | The Watch; The Bet
 5:30 pm | Amazing World of Gumball | The Bumpkin; the Flakers
 6:00 pm | Amazing World of Gumball | The Authority; The Virus
 6:30 pm | Amazing World of Gumball | The Pony; The Storm
 7:00 pm | Monsters vs. Aliens | 
 8:00 pm | King of the Hill | 
 8:30 pm | King of the Hill | 