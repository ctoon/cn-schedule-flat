# 2022-10-01
 6:00 am | Foster's Home for Imaginary Friends | Partying Is Such Sweet Soiree
 6:30 am | Teen Titans | Revolution
 7:00 am | The Powerpuff Girls | Slumbering With the Enemy
 7:30 am | Teen Titans Go! | BBBDay!
 8:00 am | Teen Titans Go! | BBSFBDAY!
 8:30 am | Steven Universe | Steven's Birthday
 9:00 am | Teen Titans Go! | BBCYFSHIPBDAY
 9:30 am | Amazing World of Gumball | The Party
10:00 am | We Bare Bears | Charlie and the Snake
10:30 am | Amazing World of Gumball | Halloween
11:00 am | Amazing World of Gumball | The Friend
11:30 am | Total DramaRama | He Who Wears the Clown
12:00 pm | Craig of the Creek | The Invitation
12:30 pm | Craig of the Creek | The Other Side: The Tournament
 1:00 pm | Clarence | A Pretty Great Day With a Girl
 1:30 pm | Craig of the Creek | Sleepover at JP's
 2:00 pm | Teen Titans Go! | BBRBDAY
 2:30 pm | Apple & Onion | Party Popper
 3:00 pm | Teen Titans Go! | BBRAEBDAY
 3:30 pm | Teen Titans Go! | DC
 4:00 pm | Teen Titans Go! | Girls Night In
 4:30 pm | Uncle Grandpa | Grounded
 5:00 pm | Regular Show | Bachelor Party! Zingo!
 5:30 pm | Adventure Time | When Wedding Bells Thaw
 6:00 pm | The Marvelous Misadventures of Flapjack | Foot Burn
 6:30 pm | Chowder | Apprentice Appreciation Day
 7:00 pm | The Grim Adventures of Billy and Mandy | House of the Ancients
 7:30 pm | Codename: Kids Next Door | Operation D.A.T.E.
 8:00 pm | Ed, Edd n Eddy | Pop Goes the Ed
 8:30 pm | Dexter's Laboratory | Surprise