# 2022-10-01
 6:00 am | Foster's Home for Imaginary Friends | Partying Is Such Sweet Soiree
 6:30 am | Teen Titans | Revolutions
 7:00 am | Powerpuff Girls | Slumbering With the Enemy; Super Sweet
 7:30 am | Teen Titans Go! | Bbbday!; Be Mine
 8:00 am | Teen Titans Go! | Bbsfbday!; Scary Figure Dance
 8:30 am | Steven Universe | Steven's Birthday; Kevin Party
 9:00 am | Teen Titans Go! | BBCYFSHIPBDAY; The Cape
 9:30 am | Amazing World of Gumball | The Party; The Father
10:00 am | We Bare Bears | Charlie and the Snake; Nom Nom
10:30 am | Amazing World of Gumball | Halloween; The Man
11:00 am | Amazing World of Gumball | The Friend; The Gift
11:30 am | Total DramaRama | He Who Wears the Clown; The Doomed Ballooned Marooned
12:00 pm | Craig of the Creek | The Invitation; Tea Timer's Ball
12:30 pm | Craig of the Creek | The Other Side: The Tournament
 1:00 pm | Clarence | Pretty Great Day With a Girl; Attack the Block Party
 1:30 pm | Craig of the Creek | Sleepover at Jp's; Locked out Cold
 2:00 pm | Teen Titans Go! | Bbrbday; Witches Brew
 2:30 pm | Apple & Onion | Party Popper; The Eater
 3:00 pm | Teen Titans Go! | Bbraebday; Starliar
 3:30 pm | Teen Titans Go! | Dc Batman's Birthday Gift
 4:00 pm | Teen Titans Go! | Girls Night In
 4:30 pm | Uncle Grandpa | Grounded; Costume Crisis
 5:00 pm | Regular Show | Bachelor Party! Zingo! ; Party Pete
 5:30 pm | Adventure Time | When Wedding Bells; Thaw Belly of the Beast
 6:00 pm | The Marvelous Misadventures of Flapjack | Footburn; Flapjack Goes to a Party
 6:30 pm | Chowder | Apprentice Appreciation Day; The Party Cruise
 7:00 pm | The Grim Adventures of Billy and Mandy | Home of the Ancients; The Show That Dare Not Speak Its Name
 7:30 pm | Codename: Kids Next Door | Operation: C.A.K.E.D. Operation: D.A.T.E.
 8:00 pm | Ed, Edd N Eddy | Pop Goes the Ed Sir Ed-a-Lot
 8:30 pm | Dexter's Laboratory | Surprise Mind Over Chatter Copping an Attitude