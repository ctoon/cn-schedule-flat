# 2022-10-07
 9:00 pm | Futurama | Saturday Morning Fun Pit
 9:30 pm | Futurama | Calculon 2.0
10:00 pm | American Dad | Red October Sky
10:30 pm | American Dad | Office Spaceman
11:00 pm | American Dad | Stanny Slickers II: The Legend of Ollie's Gold
11:30 pm | Rick and Morty | Solaricks
12:00 am | Rick and Morty | Rick: A Mort Well Lived
12:30 am | Rick and Morty | Bethic Twinstinct
 1:00 am | Rick and Morty | Night Family
 1:30 am | Rick and Morty | Final DeSmithation
 2:00 am | Futurama | Fry and Leela's Big Fling
 2:30 am | Futurama | T.: The Terrestrial
 3:00 am | Futurama | Forty Percent Leadbelly
 3:30 am | Futurama | The Inhuman Torch
 4:00 am | King Of the Hill | The Order of the Straight Arrow
 4:30 am | King Of the Hill | Luanne's Saga
 5:00 am | King Of the Hill | Hank's Got the Willies
 5:30 am | King Of the Hill | Westie Side Story