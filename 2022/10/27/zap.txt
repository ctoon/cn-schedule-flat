# 2022-10-27
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Cocomelon | This Is the Way Bedtime Edition
 7:30 am | Cocomelon | Shadow Puppets
 8:00 am | Lellobee City Farm | Splash Song
 8:15 am | Blippi Wonders | Ducks
 8:30 am | Thomas & Friends: All Engines Go | Ghost Train
 9:00 am | Mecha Builders | Let's Get Rolling!; A Bit of a Stretch!
 9:30 am | Bugs Bunny Builders | Looney Science
10:00 am | Meet the Batwheels | Buff's BFF
10:30 am | Thomas & Friends: All Engines Go | Carly's Magnificent Magnet
11:00 am | Scooby-Doo and Guess Who? | The Lost Mines of Kilimanjaro!
11:30 am | Scooby-Doo and Guess Who? | The Legend of the Gold Microphone!
12:00 pm | Craig of the Creek | Trick or Creek
12:30 pm | Craig of the Creek | The Quick Name
 1:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 1:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 2:00 pm | Amazing World of Gumball | The Apprentice
 2:30 pm | Amazing World of Gumball | The Check
 3:00 pm | Monsters vs. Aliens | 
 5:00 pm | The Book of Life | 
 7:00 pm | Trick or Treat Scooby-Doo! | 