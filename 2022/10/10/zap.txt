# 2022-10-10
 6:00 am | Amazing World of Gumball | The Words; The Apology
 6:30 am | Amazing World of Gumball | The Watch; The Bet
 7:00 am | Cocomelon | Beach Song
 7:30 am | Cocomelon | Five Senses Song
 8:00 am | Cocomelon | Pumpkin Patch Song
 8:30 am | Thomas & Friends: All Engines Go | Carly's Magnificent Magnet
 9:00 am | Mecha Builders | Moo-ving Day!; Follow That Breeze
 9:30 am | Lucas the Spider | Ghost Camp
10:00 am | Meet the Batwheels | Fly
10:05 am | Thomas & Friends: All Engines Go | Sir Topham Hatt's Hat
10:30 am | Thomas & Friends: All Engines Go | Carly's Magnificent Magnet
11:00 am | Scooby-Doo and Guess Who? | A Fashion Nightmare!
11:30 am | Scooby-Doo and Guess Who? | The Phantom, The Talking Dog, and the Hot Hot Hot Sauce!
12:00 pm | Craig of the Creek | Trick or Creek
12:30 pm | Craig of the Creek | The Haunted Dollhouse
 1:00 pm | Teen Titans Go! | Pepo the Pumpkinman
 1:30 pm | Teen Titans Go! | Scary Figure Dance
 2:00 pm | Amazing World of Gumball | The Scam
 2:30 pm | Amazing World of Gumball | The Ghouls
 3:00 pm | Shrek | 
 5:00 pm | The Book of Life | 
 7:00 pm | Scooby Doo and the Monster of Mexico | 
 8:45 pm | Teen Titans Go! | Welcome to Halloween