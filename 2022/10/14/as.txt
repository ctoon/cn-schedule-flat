# 2022-10-14
 9:00 pm | Futurama | Assie Come Home
 9:30 pm | Futurama | Leela and the Genestalk
10:00 pm | American Dad | Every Which Way But Lose
10:30 pm | American Dad | Weiner of Our Discontent
11:00 pm | American Dad | Daddy Queerest
11:30 pm | Rick and Morty | Meeseeks and Destroy
12:00 am | Teenage Euthanasia | Nobody Beats the Baba
12:30 am | Teenage Euthanasia | Suddenly Susan
 1:00 am | Teenage Euthanasia | Adventures In Beetle Sitting
 1:30 am | Teenage Euthanasia | Dada M.I.A
 2:00 am | Futurama | Saturday Morning Fun Pit
 2:30 am | Futurama | Calculon 2.0
 3:00 am | Futurama | Assie Come Home
 3:30 am | Futurama | Leela and the Genestalk
 4:00 am | King Of the Hill | The Man Who Shot Cane Skretteburg
 4:30 am | King Of the Hill | The Son That Got Away
 5:00 am | King Of the Hill | Bobby Slam
 5:30 am | King Of the Hill | Meet the Manger Babies