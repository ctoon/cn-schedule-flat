# 2022-10-16
 6:00 am | Lamput | Alien Again
 6:30 am | Teen Titans Go! | Bottle Episode
 7:00 am | Teen Titans Go! | Pyramid Scheme
 7:30 am | Teen Titans Go! | Finally a Lesson
 8:00 am | Amazing World of Gumball | The Knights; The Colossus
 8:30 am | Amazing World of Gumball | The Fridge; The Remote
 9:00 am | We Baby Bears | Meat House
 9:30 am | We Baby Bears | Back to Our Regular Time Period
10:00 am | Craig of the Creek | Trick or Creek
10:30 am | Craig of the Creek | Dog Decider
11:00 am | Craig of the Creek | Bring Out Your Beast
11:30 am | Craig of the Creek | Lost in the Sewer
12:00 pm | Total DramaRama | Gwen Scary, Gwen Lost
12:30 pm | Total DramaRama | Oozing Talent
 1:00 pm | Total DramaRama | Senior Sinisters
 1:30 pm | Total DramaRama | Be Claws I Love You, Shelley
 2:00 pm | Total DramaRama | Total Trauma Rama
 2:30 pm | Total DramaRama | The Doomed Ballooned Marooned
 3:00 pm | Total DramaRama | Legends of the Paul
 3:30 pm | Total DramaRama | A Bridgette Too Far
 4:00 pm | Amazing World of Gumball | The Flower; The Banana
 4:30 pm | Amazing World of Gumball | The Phone; The Job
 5:00 pm | Amazing World of Gumball | Halloween; The Treasure
 5:30 pm | Amazing World of Gumball | The Words; The Apology
 6:00 pm | Lemony Snicket's A Series of Unfortunate Events | 
 8:00 pm | Bob's Burgers | Glued, Where's My Bob?
 8:30 pm | Bob's Burgers | Sea Me Now