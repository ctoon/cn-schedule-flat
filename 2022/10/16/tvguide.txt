# 2022-10-16
 6:00 am | Lamput | Alien Again; Cast Away; Lamput Meets Tuzki
 6:30 am | Teen Titans Go! | Wally T; Bottle Episode
 7:00 am | Teen Titans Go! | Pyramid Scheme; Rad Dudes With Bad Tudes
 7:30 am | Teen Titans Go! | History Lesson; Finally a Lesson
 8:00 am | Amazing World of Gumball | The Knights; The Colossus
 8:30 am | Amazing World of Gumball | The Fridge; The Remote
 9:00 am | We Baby Bears | Meat House; Pirate Parrot Polly
 9:30 am | We Baby Bears | Back to Our Regular Time Period; Happy Bouncy Fun Town
10:00 am | Craig of the Creek | Trick or Creek
10:30 am | Craig Of The Creek | Dog Decider; The Future Is Cardboard
11:00 am | Craig of the Creek | The Brood; Bring out Your Beast
11:30 am | Craig of the Creek | Lost in the Sewer; Under the Overpass
12:00 pm | Total DramaRama | Gwen Scary; Gwen Lost
12:30 pm | Total DramaRama | Oozing Talent; Cactus Makes Perfect
 1:00 pm | Total DramaRama | Senior Sinisters; Virtual Reality Bites
 1:30 pm | Total DramaRama | The Be Claws I Love You; Shelley; Opening Act
 2:00 pm | Total DramaRama | Total Trauma Rama; Van Hogling
 2:30 pm | Total DramaRama | Doomed Ballooned Marooned; The; Ticking Crime Bomb
 3:00 pm | Total DramaRama | Legends of the Paul; Knit Wit
 3:30 pm | Total DramaRama | The Bridgette Too Far; A; Fuss on the Bus
 4:00 pm | Amazing World of Gumball | The Flower; The Banana
 4:30 pm | Amazing World of Gumball | The Phone; The Job
 5:00 pm | Amazing World of Gumball | Halloween; The treasure
 5:30 pm | Amazing World of Gumball | The Words; The Apology
 6:00 pm | Lemony Snicket's A Series of Unfortunate Events | 
 8:00 pm | Bob's Burgers | Glued, Where's My Bob?
 8:30 pm | Bob's Burgers | Sea Me Now