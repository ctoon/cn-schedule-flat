# 2022-10-15
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Amazing World of Gumball | The Car; The Curse
 7:30 am | Amazing World of Gumball | The Microwave; The Meddler
 8:00 am | Amazing World of Gumball | The Helmet; The Fight
 8:30 am | Amazing World of Gumball | The End; The DVD
 9:00 am | We Baby Bears | Back to Our Regular Time Period
 9:15 am | We Baby Bears | Witches
 9:30 am | We Baby Bears | Modern-ish Stone Age Family
10:00 am | Trick or Treat Scooby-Doo! | 
11:45 am | Teen Titans Go! | Welcome to Halloween
12:00 pm | Teen Titans Go! | La Larva Amor
12:30 pm | Teen Titans Go! | TV Knight 2
 1:00 pm | Teen Titans Go! | Snuggle Time
 1:30 pm | Teen Titans Go! | TV Knight 6
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 3:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 3:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 4:00 pm | Meet the Batwheels | Secret Origin of the Batwheels
 4:45 pm | Teen Titans Go! | Welcome to Halloween
 5:00 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 5:30 pm | Teen Titans Go! | Bat Scouts
 6:00 pm | Meet the Batwheels | Secret Origin of the Batwheels
 6:45 pm | Teen Titans Go! | Welcome to Halloween
 7:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 8:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition