# 2022-10-31
 6:00 am | Amazing World of Gumball | The Halloween Mirror
 6:30 am | Amazing World of Gumball | The Ghouls; The Scam
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Thomas & Friends: All Engines Go | Carly's Screechy Squeak; Thomas' Promise
 9:00 am | Mecha Builders | The Sent US a Pie / Dust In the Wind
 9:30 am | Bugs Bunny Builders | Big Feet
 9:45 am | Bugs Bunny Builders | Rock On
10:00 am | Meet the Batwheels | Bam's Bubble Trouble; Buff's Bff; up in the Air
10:30 am | Thomas & Friends: All Engines Go | Carly's Screechy Squeak; License to Deliver
11:00 am | New Looney Tunes | Survivalist Of The Fittest; Imposter; The Bugs In The Garden; Scarecrow
11:30 am | New Looney Tunes | Love it or Survivalist It; The Porklight; Best Bugs; Lewis & Pork
12:00 pm | Craig of the Creek | The Haunted Dollhouse; The; Legend of the Library
12:30 pm | Craig of the Creek | Trick or Creek
 1:00 pm | Teen Titans Go! | Witches Brew; Ghost With the Most
 1:30 pm | Teen Titans Go! | Pepo the Pumpkinman; Welcome to Halloween
 2:00 pm | Amazing World of Gumball | The Halloween Mirror
 2:30 pm | Amazing World of Gumball | The Ghouls; The Scam
 3:00 pm | Amazing World of Gumball | The Ghouls; The; Gumball Chronicles: The Curse of Elmore
 3:30 pm | We Baby Bears | Witches
 3:45 pm | We Bare Bears | Charlie's Halloween Thing
 4:00 pm | We Bare Bears | Charlie's Halloween Thing 2
 4:30 pm | Craig of the Creek | Trick or Creek
 5:00 pm | Teen Titans Go! | Halloween vs. Christmas; Scary Figure Dance
 5:30 pm | Teen Titans Go! | Witches Brew; Ghost With the Most
 6:00 pm | Teen Titans Go! | Pepo the Pumpkinman; Welcome to Halloween
 6:30 pm | Over the Garden Wall | Chapter 1: The Old Grist Mill; Chapter 2: Hard Times At The Huskin' Bee
 7:00 pm | Over the Garden Wall | Chapter 3: Schooltown Follies; Chapter 4: Songs Of The Dark Lantern
 7:30 pm | Over the Garden Wall | Chapter 5: Mad Love; Chapter 6: Lullaby In Frogland
 8:00 pm | Over the Garden Wall | Chapter 7: The Ringing Of The Bell; Chapter 8: Babes In The Wood
 8:30 pm | Over the Garden Wall | Chapter 9: Into The Unknown; Chapter 10: The Unknown