# 2022-10-21
 6:00 am | Mecha Builders | Mecha Builders Pull Togetherlift up and Lift Off
 6:30 am | Cocomelon | 
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Lellobee City Farms | Old Macdonald; No Monsters Scared of the Dark; Trick or Treat; Do the Skeleton Dance - Halloween Special
 8:15 am | Blippi Wonders | Volcano; Bats; Halloween; Halloween Song
 8:30 am | Thomas & Friends: All Engines Go | Ghost Train; Goodbye; Ghost-Scaring Machine
 9:00 am | Mecha Builders | That's the Way the Windmill Blowsmecha Machine Makers
 9:30 am | Bugs Bunny Builders | Looney Science; Splash Zone
10:00 am | Meet the Batwheels | Sidekicked to the Curb; Keep Calm and Roll on Spooky Batcave
10:30 am | Thomas & Friends: All Engines Go | Roller Coasting; More Cowbell
11:00 am | Total DramaRama | Simons Are Forever ;The Tooth About Zombies
11:30 am | Total DramaRama | Mother of All Cards Lie-Ranosaurus Wrecked
12:00 pm | Craig of the Creek | The Legend of the Library; The; Sunflower
12:30 pm | Craig of the Creek | Trick or Creek
 1:00 pm | Teen Titans Go! | Pepo the Pumpkinman; Ghost With the Most
 1:30 pm | Teen Titans Go! | The Mask; Slumber Party
 2:00 pm | Amazing World of Gumball | The Mirror; The Burden
 2:30 pm | Amazing World of Gumball | The Man; The Bros
 3:00 pm | Amazing World of Gumball | The Lie; The Pizza
 3:30 pm | Amazing World of Gumball | Ghouls; The Halloween
 4:00 pm | Craig of the Creek | The Jessica the Intern; Haunted Dollhouse
 4:30 pm | Craig of the Creek | Beyond the Overpass; Better Than You
 5:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 5:30 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 6:00 pm | Amazing World of Gumball | The World; The Finale
 6:30 pm | Amazing World of Gumball | The Kids; The Fan
 7:00 pm | Scooby-Doo in Where's My Mummy? | 
 8:45 pm | Teen Titans Go! | Welcome to Halloween