# 2022-10-21
 6:00 am | Amazing World of Gumball | The Coach; The Joy
 6:30 am | Amazing World of Gumball | The Recipe; The Puppy
 7:00 am | Cocomelon | This Is the Way Bedtime Edition
 7:30 am | Cocomelon | Five Senses Song
 8:00 am | Lellobee City Farm | Old MacDonald
 8:15 am | Blippi Wonders | Volcano
 8:30 am | Thomas & Friends: All Engines Go | Ghost Train
 9:00 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 9:30 am | Bugs Bunny Builders | Looney Science
10:00 am | Meet the Batwheels | Sidekicked to the Curb
10:30 am | Thomas & Friends: All Engines Go | Roller Coasting
11:00 am | Scooby-Doo and Guess Who? | The Crown Jewel of Boxing!
11:30 am | Scooby-Doo and Guess Who? | The Feast of Dr. Frankenfooder!
12:00 pm | Craig of the Creek | The Legend of the Library
12:30 pm | Craig of the Creek | Trick or Creek
 1:00 pm | Teen Titans Go! | Pepo the Pumpkinman
 1:30 pm | Teen Titans Go! | The Mask; Slumber Party
 2:00 pm | Amazing World of Gumball | The Mirror; The Burden
 2:30 pm | Amazing World of Gumball | The Bros; The Man
 3:00 pm | Amazing World of Gumball | The Pizza; The Lie
 3:30 pm | Amazing World of Gumball | The Ghouls
 4:00 pm | Craig of the Creek | Jessica the Intern
 4:30 pm | Craig of the Creek | Beyond the Overpass
 5:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 5:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 6:00 pm | Amazing World of Gumball | The World; The Finale
 6:30 pm | Amazing World of Gumball | The Kids; The Fan
 7:00 pm | Scooby-Doo in Where's My Mummy? | 
 8:45 pm | Teen Titans Go! | Welcome to Halloween