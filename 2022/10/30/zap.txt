# 2022-10-30
 6:00 am | Teen Titans Go! | Fish Water
 6:30 am | Teen Titans Go! | TV Knight
 7:00 am | Teen Titans Go! | Witches Brew
 7:30 am | Teen Titans Go! | The Streak, Part 1
 8:00 am | Amazing World of Gumball | The Boss; The Move
 8:30 am | Amazing World of Gumball | The Law; The Allergy
 9:00 am | We Baby Bears | Bears in the Dark
 9:30 am | We Baby Bears | Bubble Fields
10:00 am | Craig of the Creek | The Haunted Dollhouse
10:30 am | Craig of the Creek | The Takeout Mission
11:00 am | Craig of the Creek | Jessica's Trail
11:30 am | Craig of the Creek | Trick or Creek
12:00 pm | Teen Titans Go! | Island Adventures
 1:00 pm | Teen Titans Go! | Pepo the Pumpkinman
 1:30 pm | Teen Titans Go! | Hand Zombie
 2:00 pm | Teen Titans Go! | Master Detective
 2:30 pm | Teen Titans Go! | The Avogodo
 3:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 4:00 pm | Amazing World of Gumball | The Mothers; The Password
 4:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 5:00 pm | Amazing World of Gumball | The Mirror; The Burden
 5:30 pm | Amazing World of Gumball | Halloween
 6:00 pm | SCOOB! | 
 8:00 pm | Bob's Burgers | Like Gene for Chocolate
 8:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel