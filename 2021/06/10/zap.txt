# 2021-06-10
 6:00 am | Amazing World of Gumball | The Uncle
 6:15 am | Amazing World of Gumball | The Cringe
 6:30 am | Amazing World of Gumball | The Heist
 6:45 am | Amazing World of Gumball | The Neighbor
 7:00 am | Amazing World of Gumball | The Petals
 7:15 am | Amazing World of Gumball | The Pact
 7:30 am | Amazing World of Gumball | The Nuisance
 7:45 am | Amazing World of Gumball | The Faith
 8:00 am | Teen Titans Go! | A Farce
 8:15 am | Teen Titans Go! | Secret Garden
 8:30 am | Teen Titans Go! | BBBDay!
 8:45 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:00 am | Amazing World of Gumball | The Best
 9:15 am | Amazing World of Gumball | The Candidate
 9:30 am | Amazing World of Gumball | The Worst
 9:45 am | Amazing World of Gumball | The Anybody
10:00 am | Craig of the Creek | Jessica the Intern
10:15 am | Craig of the Creek | Brother Builder
10:30 am | Craig of the Creek | Creature Feature
10:45 am | Craig of the Creek | Afterschool Snackdown
11:00 am | Craig of the Creek | Creek Cart Racers
11:15 am | Craig of the Creek | Alone Quest
11:30 am | Craig of the Creek | Secret Book Club
11:45 am | Craig of the Creek | Memories of Bobby
12:00 pm | Teen Titans Go! | Think About Your Future
12:15 pm | Teen Titans Go! | Fish Water
12:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
12:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 1:00 pm | Amazing World of Gumball | The Ad
 1:15 pm | Amazing World of Gumball | The Slip
 1:30 pm | Amazing World of Gumball | The Drama
 1:45 pm | Amazing World of Gumball | The Revolt
 2:00 pm | Amazing World of Gumball | The Buddy
 2:15 pm | Amazing World of Gumball | The Mess
 2:30 pm | Amazing World of Gumball | The Master
 2:45 pm | Amazing World of Gumball | The Possession
 3:00 pm | Craig of the Creek | Creature Feature
 3:15 pm | Craig of the Creek | Afterschool Snackdown
 3:30 pm | Craig of the Creek | Body Swap
 3:45 pm | Craig of the Creek | Breaking the Ice
 4:00 pm | Craig of the Creek | Ace of Squares
 4:15 pm | Craig of the Creek | Dinner at the Creek
 4:30 pm | Craig of the Creek | Doorway to Helen
 4:45 pm | Craig of the Creek | Bug City
 5:00 pm | Total DramaRama | Dude Where's Macaw
 5:15 pm | Total DramaRama | Invasion of the Booger Snatchers
 5:30 pm | Total DramaRama | Way Back Wendel
 5:45 pm | Total DramaRama | Wristy Business
 6:00 pm | Teen Titans Go! | Two Parter: Part One
 6:15 pm | Teen Titans Go! | Two Parter: Part Two
 6:30 pm | Teen Titans Go! | Animals: It's Just a Word!
 6:45 pm | Teen Titans Go! | Garage Sale
 7:00 pm | Amazing World of Gumball | The Heist
 7:15 pm | Amazing World of Gumball | The Neighbor
 7:30 pm | Apple & Onion | Apple & Onion's Booth
 7:45 pm | Apple & Onion | Appleoni