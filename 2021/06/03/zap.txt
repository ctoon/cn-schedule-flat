# 2021-06-03
 6:00 am | Amazing World of Gumball | The Signature
 6:15 am | Amazing World of Gumball | The Comic
 6:30 am | Amazing World of Gumball | The Gift
 6:45 am | Amazing World of Gumball | The Romantic
 7:00 am | Amazing World of Gumball | The Apprentice
 7:15 am | Amazing World of Gumball | The Uploads
 7:30 am | Amazing World of Gumball | The Check
 7:45 am | Amazing World of Gumball | The Wicked
 8:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 8:30 am | Teen Titans Go! | Operation Tin Man; Nean
 9:00 am | Craig of the Creek | The Brood
 9:15 am | Craig of the Creek | The Kid From 3030
 9:30 am | Craig of the Creek | Under the Overpass
 9:45 am | Craig of the Creek | Power Punchers
10:00 am | Craig of the Creek | Fort Williams
10:15 am | Craig of the Creek | The Cardboard Identity
10:30 am | Craig of the Creek | Kelsey the Elder
10:45 am | Craig of the Creek | Ancients of the Creek
11:00 am | Victor and Valentino | Brotherly Love
11:15 am | Victor and Valentino | Suerte
11:30 am | Teen Titans Go! | Friendship; Vegetables
12:00 pm | Teen Titans Go! | The Mask; Slumber Party
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Amazing World of Gumball | The Guy
 1:15 pm | Amazing World of Gumball | The Outside
 1:30 pm | Amazing World of Gumball | The Boredom
 1:45 pm | Amazing World of Gumball | The Copycats
 2:00 pm | Amazing World of Gumball | The Sorcerer
 2:15 pm | Amazing World of Gumball | The Catfish
 2:30 pm | Amazing World of Gumball | The Choices
 2:45 pm | Amazing World of Gumball | The Cycle
 3:00 pm | Amazing World of Gumball | The Code
 3:15 pm | Amazing World of Gumball | The Stars
 3:30 pm | Amazing World of Gumball | The Test
 3:45 pm | Amazing World of Gumball | The Box
 4:00 pm | Craig of the Creek | Summer Wish
 4:15 pm | Craig of the Creek | Kelsey the Worthy
 4:30 pm | Craig of the Creek | Turning the Tables
 4:45 pm | Craig of the Creek | The End Was Here
 5:00 pm | Total DramaRama | Total Eclipse of the Fart
 5:15 pm | Total DramaRama | Paint That a Shame
 5:30 pm | Total DramaRama | For a Few Duncans More
 5:45 pm | Total DramaRama | Snots Landing
 6:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:15 pm | Teen Titans Go! | Grube's Fairytales
 6:30 pm | Teen Titans Go! | The Fourth Wall
 6:45 pm | Teen Titans Go! | A Farce
 7:00 pm | Amazing World of Gumball | The Crew
 7:15 pm | Amazing World of Gumball | The Parking
 7:30 pm | Apple & Onion | Champion
 7:45 pm | Apple & Onion | Face Your Fears