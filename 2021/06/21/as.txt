# 2021-06-21
 8:00 pm | American Dad | A Star Is Reborn
 8:30 pm | American Dad | Manhattan Magical Murder Mystery Tour
 9:00 pm | American Dad | The Shrink
 9:30 pm | Bob's Burgers | The Belchies
10:00 pm | Bob's Burgers | Bob Day Afternoon
10:30 pm | Rick and Morty | Something Ricked This Way Comes
11:00 pm | Family Guy | Model Misbehavior
11:30 pm | Family Guy | Peter's Got Woods
12:00 am | Rick and Morty | Mort Dinner Rick Andre
12:30 am | Robot Chicken | Petless M in: Cars are Couches of the Road
12:45 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
 1:00 am | Bob's Burgers | The Belchies
 1:30 am | Bob's Burgers | Bob Day Afternoon
 2:00 am | Family Guy | Model Misbehavior
 2:30 am | Family Guy | Peter's Got Woods
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Robot Chicken | Petless M in: Cars are Couches of the Road
 3:45 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
 4:00 am | Mike Tyson Mysteries | Life is But a Dream
 4:15 am | Mike Tyson Mysteries | Unsolved Situations
 4:30 am | The Venture Brothers | The Venture Bros. & the Curse of the Haunted Problem
 5:00 am | Naruto:Shippuden | The Fallen Castle
 5:30 am | Samurai Jack | XXXVI