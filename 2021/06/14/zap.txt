# 2021-06-14
 6:00 am | Amazing World of Gumball | The Party; The Refund
 6:30 am | Amazing World of Gumball | The Robot; The Picnic
 7:00 am | Amazing World of Gumball | The Car; The Curse
 7:30 am | Amazing World of Gumball | The Microwave; The Meddler
 8:00 am | Teen Titans Go! | The Night Begins to Shine Special
 9:00 am | Teen Titans Go! | Superhero Feud
 9:15 am | Teen Titans Go! | BBRAEBDAY
 9:30 am | Teen Titans Go! | Real Art
 9:45 am | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
10:00 am | Craig of the Creek | The Bike Thief
10:15 am | Craig of the Creek | Afterschool Snackdown
10:30 am | Craig of the Creek | Craig of the Beach
10:45 am | Craig of the Creek | Creature Feature
11:00 am | Craig of the Creek | Fort Williams
11:15 am | Craig of the Creek | Creek Daycare
11:30 am | Craig of the Creek | Kelsey the Elder
11:45 am | Craig of the Creek | Sugar Smugglers
12:00 pm | Teen Titans Go! | Hand Zombie
12:15 pm | Teen Titans Go! | Hot Salad Water
12:30 pm | Teen Titans Go! | Employee of the Month Redux
12:45 pm | Teen Titans Go! | Brain Percentages
 1:00 pm | Amazing World of Gumball | The Third; The Debt
 1:30 pm | Amazing World of Gumball | The Pressure; The Painting
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Craig of the Creek | Winter Break
 3:30 pm | Craig of the Creek | Snow Day
 3:45 pm | Craig of the Creek | Snow Place Like Home
 4:00 pm | Craig of the Creek | Sparkle Cadet
 4:15 pm | Craig of the Creek | Tea Timer's Ball
 4:30 pm | Craig of the Creek | Stink Bomb
 4:45 pm | Craig of the Creek | The Cardboard Identity
 5:00 pm | Total DramaRama | OWW
 5:15 pm | Total DramaRama | Too Much of a Goo'd Thing
 5:30 pm | Total DramaRama | Jelly Aches
 5:45 pm | Total DramaRama | The Price of Advice
 6:00 pm | Teen Titans Go! | Career Day
 6:15 pm | Teen Titans Go! | TV Knight 2
 6:30 pm | Teen Titans Go! | The Academy
 6:45 pm | Teen Titans Go! | Throne of Bones
 7:00 pm | Amazing World of Gumball | The Quest; The Spoon
 7:30 pm | Apple & Onion | Pat on the Head
 7:45 pm | Apple & Onion | Falafel's in Jail