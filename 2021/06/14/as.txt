# 2021-06-14
 8:00 pm | American Dad | Stan Goes on the Pill
 8:30 pm | American Dad | She Swill Survive
 9:00 pm | American Dad | Rubberneckers
 9:30 pm | Bob's Burgers | Hamburger Dinner Theater
10:00 pm | Bob's Burgers | Sheesh! Cab, Bob?
10:30 pm | Rick and Morty | Meeseeks and Destroy
11:00 pm | Family Guy | Road To Europe
11:30 pm | Family Guy | Family Guy Viewer Mail #1
12:00 am | Rick and Morty | The Vat of Acid Episode
12:30 am | Robot Chicken | Boogie Bardstown in: No Need, I have Coupons
12:45 am | Robot Chicken | Snoopy Camino Lindo in: Quick and Dirty Squirrel Shot
 1:00 am | Bob's Burgers | Hamburger Dinner Theater
 1:30 am | Bob's Burgers | Sheesh! Cab, Bob?
 2:00 am | Family Guy | Road To Europe
 2:30 am | Family Guy | Family Guy Viewer Mail #1
 3:00 am | Rick and Morty | Meeseeks and Destroy
 3:30 am | Robot Chicken | Boogie Bardstown in: No Need, I have Coupons
 3:45 am | Robot Chicken | Snoopy Camino Lindo in: Quick and Dirty Squirrel Shot
 4:00 am | Mike Tyson Mysteries | What's That Gnoise?
 4:15 am | Mike Tyson Mysteries | For the Troops
 4:30 am | The Venture Brothers | Tanks for Nuthin'
 5:00 am | Naruto:Shippuden | The Vengeful
 5:30 am | Samurai Jack | XXXI