# 2021-06-17
 6:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
 6:30 am | Amazing World of Gumball | The Pony; The Storm
 7:00 am | Amazing World of Gumball | The Responsible; The Dress
 7:30 am | Amazing World of Gumball | The Laziest; The Ghost
 8:00 am | Teen Titans Go! | Hand Zombie
 8:15 am | Teen Titans Go! | Hot Salad Water
 8:30 am | Teen Titans Go! | Employee of the Month Redux
 8:45 am | Teen Titans Go! | Brain Percentages
 9:00 am | Teen Titans Go! | Pig in a Poke
 9:15 am | Teen Titans Go! | What's Opera, Titans?
 9:30 am | Teen Titans Go! | A Little Help Please
 9:45 am | Teen Titans Go! | Marv Wolfman and George Pérez
10:00 am | Craig of the Creek | New Jersey
10:15 am | Craig of the Creek | Too Many Treasures
10:30 am | Craig of the Creek | The Sunflower
10:45 am | Craig of the Creek | Wildernessa
11:00 am | Craig of the Creek | Welcome to Creek Street
11:15 am | Craig of the Creek | Jessica Goes to the Creek
11:30 am | Craig of the Creek | Fan or Foe
11:45 am | Craig of the Creek | The Final Book
12:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
12:15 pm | Teen Titans Go! | The Scoop
12:30 pm | Teen Titans Go! | Beast Girl
12:45 pm | Teen Titans Go! | Tower Renovation
 1:00 pm | Amazing World of Gumball | The Knights; The Colossus
 1:30 pm | Amazing World of Gumball | The Fridge; The Remote
 2:00 pm | Amazing World of Gumball | The Flower; The Banana
 2:30 pm | Amazing World of Gumball | The Phone; The Job
 3:00 pm | Craig of the Creek | The Other Side: The Tournament
 3:30 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 3:45 pm | Craig of the Creek | Fall Anthology
 4:00 pm | Craig of the Creek | Fort Williams
 4:15 pm | Craig of the Creek | Creek Daycare
 4:30 pm | Craig of the Creek | Kelsey the Elder
 4:45 pm | Craig of the Creek | Sugar Smugglers
 5:00 pm | Total DramaRama | The Gold and the Stickerful
 5:15 pm | Total DramaRama | Simons Are Forever
 5:30 pm | Total DramaRama | Bad Seed
 5:45 pm | Total DramaRama | Mother of All Cards
 6:00 pm | Teen Titans Go! | BBRAE Pt 1
 6:30 pm | Teen Titans Go! | Master Detective
 6:45 pm | Teen Titans Go! | The Avogodo
 7:00 pm | Amazing World of Gumball | The Pressure; The Painting
 7:30 pm | Apple & Onion | Slobbery
 7:45 pm | Apple & Onion | Dragonhead