# 2021-06-05
 6:00 am | Amazing World of Gumball | The Third; The Debt
 6:30 am | Amazing World of Gumball | The Pressure; The Painting
 7:00 am | Amazing World of Gumball | The Responsible; The Dress
 7:30 am | Amazing World of Gumball | The Laziest; The Ghost
 8:00 am | The Fungies! | Let It Snowball
 8:15 am | The Fungies! | Sick Day
 8:30 am | Teen Titans Go! | Space House
 9:30 am | Total DramaRama | Sugar & Spice & Lightning & Frights
 9:45 am | Total DramaRama | Carmageddon
10:00 am | Craig of the Creek | The Sunflower
10:15 am | Craig of the Creek | Craig World
10:30 am | Victor and Valentino | Las Pesadillas
10:45 am | Victor and Valentino | Cenote Seekers
11:00 am | Teen Titans Go! | Road Trip; The Best Robin
11:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
12:00 pm | Amazing World of Gumball | The Sock; The Genius
12:30 pm | Amazing World of Gumball | The Mustache; The Date
 1:00 pm | Amazing World of Gumball | The Club; The Wand
 1:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 2:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:30 pm | Amazing World of Gumball | The Fridge; The Remote
 3:00 pm | Victor and Valentino | Through the Nine Realms of Mictlan
 4:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 5:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 6:30 pm | Amazing World of Gumball | The Helmet; The Fight
 7:00 pm | Amazing World of Gumball | The End; The DVD
 7:30 pm | Amazing World of Gumball | The Knights; The Colossus