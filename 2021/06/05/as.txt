# 2021-06-05
 8:00 pm | Final Space | The Dead Speak
 8:30 pm | American Dad | The Adventures of Twill Ongenbone and His Boy Jabari
 9:00 pm | American Dad | Blood Crieth Unto Heaven
 9:30 pm | American Dad | Max Jets
10:00 pm | American Dad | Naked to the Limit, One More Time
10:30 pm | Rick and Morty | Mortynight Run
11:00 pm | Family Guy | One If by Clam, Two If by Sea
11:30 pm | Family Guy | And the Wiener Is...
12:00 am | My Hero Academia | Operation New Improv Moves
12:30 am | Dr. Stone | Full Assault
 1:00 am | Food Wars! | Joan of Arc Arises
 1:30 am | The Promised Neverland | Episode 9
 2:00 am | Black Clover | The Chosen Ones
 2:30 am | Naruto:Shippuden | Obito and Madara
 3:00 am | Attack on Titan | Declaration of War
 3:30 am | Dragon Ball Super | Accelerating Tragedy - Vanishing Universes
 4:00 am | Rick and Morty | Mortynight Run
 4:30 am | Final Space | The Dead Speak
 5:00 am | Home Movies | Dad
 5:30 am | Home Movies | Therapy