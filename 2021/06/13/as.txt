# 2021-06-13
 8:00 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 8:30 pm | Bob's Burgers | Aquaticism
 9:00 pm | American Dad | Dammmm, Stan!
 9:30 pm | American Dad | Dancin' A-with My Cells
10:00 pm | Family Guy | Run, Chris, Run
10:30 pm | Family Guy | Road to India
11:00 pm | Rick and Morty | Star Mort Rickturn of the Jerri
11:30 pm | Tuca & Bertie | Bird Mechanics
12:00 am | Birdgirl | Thirdgirl
12:30 am | Final Space | The Devil's Den
 1:00 am | Family Guy | Run, Chris, Run
 1:30 am | Family Guy | Road to India
 2:00 am | Bob's Burgers | The Grand Mama-Pest Hotel
 2:30 am | Bob's Burgers | Aquaticism
 3:00 am | Rick and Morty | Star Mort Rickturn of the Jerri
 3:30 am | Tuca & Bertie | Bird Mechanics
 4:00 am | Birdgirl | Thirdgirl
 4:30 am | Final Space | The Devil's Den
 5:00 am | Home Movies | The Wedding
 5:30 am | Home Movies | Shore Leave