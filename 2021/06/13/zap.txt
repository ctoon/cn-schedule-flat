# 2021-06-13
 6:00 am | Teen Titans Go! | The Art of Ninjutsu
 6:15 am | Teen Titans Go! | Oregon Trail
 6:30 am | Teen Titans Go! | Think About Your Future
 6:45 am | Teen Titans Go! | Snuggle Time
 7:00 am | Teen Titans Go! | Booty Scooty
 7:15 am | Teen Titans Go! | Oh Yeah!
 7:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 7:45 am | Teen Titans Go! | Marv Wolfman and George Pérez
 8:00 am | DC Super Hero Girls | #DoubleDanvers
 8:15 am | DC Super Hero Girls | #AccordingToGarth
 8:30 am | Teen Titans Go! | Riding the Dragon
 8:45 am | Teen Titans Go! | The Overbite
 9:00 am | Teen Titans Go! | Island Adventures
10:00 am | Craig of the Creek | Bring Out Your Beast
10:15 am | Craig of the Creek | Lost in the Sewer
10:30 am | Craig of the Creek | The Brood
10:45 am | Craig of the Creek | Under the Overpass
11:00 am | Craig of the Creek | Body Swap
11:15 am | Craig of the Creek | Copycat Carter
11:30 am | Craig of the Creek | Brother Builder
11:45 am | Craig of the Creek | Jessica the Intern
12:00 pm | Amazing World of Gumball | The Bros; The Man
12:30 pm | Amazing World of Gumball | The Pizza; The Lie
 1:00 pm | Amazing World of Gumball | The Butterfly; The Question
 1:30 pm | Amazing World of Gumball | The Oracle; The Safety
 2:00 pm | Amazing World of Gumball | The Friend; The Saint
 2:30 pm | Amazing World of Gumball | The Society; The Spoiler
 2:45 pm | Amazing World of Gumball | The Understanding
 3:00 pm | Victor and Valentino | Churro Kings
 3:15 pm | Victor and Valentino | Go With the Flow
 3:30 pm | Victor and Valentino | Know It All
 3:45 pm | Victor and Valentino | Aluxes
 4:00 pm | Teen Titans Go! | Shrimps and Prime Rib
 4:15 pm | Teen Titans Go! | BBSFBDAY
 4:30 pm | Teen Titans Go! | Booby Trap House
 4:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 5:00 pm | Teen Titans Go! | Fish Water
 5:15 pm | Teen Titans Go! | Movie Night
 5:30 pm | Teen Titans Go! | TV Knight
 5:45 pm | Teen Titans Go! | Pig in a Poke
 6:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 6:30 pm | Amazing World of Gumball | The Downer; The Egg
 7:00 pm | Amazing World of Gumball | The Triangle; The Money
 7:30 pm | Amazing World of Gumball | The Return
 7:45 pm | Amazing World of Gumball | The Nemesis