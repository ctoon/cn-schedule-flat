# 2021-06-23
 6:00 am | Amazing World of Gumball | The Coach; The Joy
 6:30 am | Amazing World of Gumball | The Recipe; The Puppy
 7:00 am | Amazing World of Gumball | The Nemesis
 7:15 am | Amazing World of Gumball | The Parking
 7:30 am | Amazing World of Gumball | The Crew
 7:45 am | Amazing World of Gumball | The Upgrade
 8:00 am | Teen Titans Go! | TV Knight 4
 8:15 am | Teen Titans Go! | Walk Away
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:00 am | Teen Titans Go! | Pirates; I See You
 9:30 am | Teen Titans Go! | Brian; Nature
10:00 am | Craig of the Creek | Jessica's Trail
10:15 am | Craig of the Creek | Fort Williams
10:30 am | Craig of the Creek | Dinner at the Creek
10:45 am | Craig of the Creek | Kelsey the Elder
11:00 am | Craig of the Creek | Into the Overpast
11:15 am | Craig of the Creek | Pencil Break Mania
11:30 am | Craig of the Creek | The Time Capsule
11:45 am | Craig of the Creek | The Last Game of Summer
12:00 pm | Teen Titans Go! | Them Soviet Boys
12:15 pm | Teen Titans Go! | Strength of a Grown Man
12:30 pm | Teen Titans Go! | Lil' Dimples
12:45 pm | Teen Titans Go! | Had to Be There
 1:00 pm | Amazing World of Gumball | The Law; The Allergy
 1:30 pm | Amazing World of Gumball | The Mothers; The Password
 2:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:30 pm | Amazing World of Gumball | The Mirror; The Burden
 3:00 pm | Craig of the Creek | The Ground Is Lava!
 3:15 pm | Craig of the Creek | King of Camping
 3:30 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 3:45 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:00 pm | Craig of the Creek | Memories of Bobby
 4:15 pm | Craig of the Creek | Sugar Smugglers
 4:30 pm | Craig of the Creek | Jacob of the Creek
 4:45 pm | Craig of the Creek | Kelsey the Author
 5:00 pm | Total DramaRama | Dissing Cousins
 5:30 pm | Total DramaRama | Camping Is in Tents
 6:00 pm | Teen Titans Go! | How's This for a Special? Spaaaace: Pt. 1
 6:30 pm | Teen Titans Go! | Teen Titans Vroom
 7:00 pm | Amazing World of Gumball | The Guy
 7:15 pm | Amazing World of Gumball | The Console
 7:30 pm | Apple & Onion | Walking on the Ceiling
 7:45 pm | Apple & Onion | Election Day