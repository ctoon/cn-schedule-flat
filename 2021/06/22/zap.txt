# 2021-06-22
 6:00 am | Amazing World of Gumball | The Promise; The Voice
 6:30 am | Amazing World of Gumball | The Boombox; The Castle
 7:00 am | Amazing World of Gumball | The Friend; The Saint
 7:30 am | Amazing World of Gumball | The Society; The Spoiler
 8:00 am | Teen Titans Go! | Stockton, CA!
 8:15 am | Teen Titans Go! | The Great Disaster
 8:30 am | Teen Titans Go! | Collect Them All
 8:45 am | Teen Titans Go! | The Viewers Decide
 9:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
10:00 am | Craig of the Creek | Vulture's Nest
10:15 am | Craig of the Creek | Creek Cart Racers
10:30 am | Craig of the Creek | Kelsey Quest
10:45 am | Craig of the Creek | Secret Book Club
11:00 am | Craig of the Creek | Jessica Shorts
11:15 am | Craig of the Creek | Plush Kingdom
11:30 am | Craig of the Creek | Ferret Quest
11:45 am | Craig of the Creek | The Ice Pop Trio
12:00 pm | Teen Titans Go! | I Used to Be a Peoples
12:15 pm | Teen Titans Go! | Communicate Openly
12:30 pm | Teen Titans Go! | The Metric System vs. Freedom
12:45 pm | Teen Titans Go! | Teen Titans Roar!
 1:00 pm | Amazing World of Gumball | The Coach; The Joy
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Craig of the Creek | Deep Creek Salvage
 3:15 pm | Craig of the Creek | Sparkle Cadet
 3:30 pm | Craig of the Creek | Dibs Court
 3:45 pm | Craig of the Creek | Stink Bomb
 4:00 pm | Craig of the Creek | Beyond the Rapids
 4:15 pm | Craig of the Creek | Fall Anthology
 4:30 pm | Craig of the Creek | The Jinxening
 4:45 pm | Craig of the Creek | Afterschool Snackdown
 5:00 pm | Total DramaRama | Life of Pie
 5:15 pm | Total DramaRama | Gnome More Mister Nice Guy
 5:30 pm | Total DramaRama | AbaracaDuncan
 5:45 pm | Total DramaRama | Look Who's Clocking
 6:00 pm | Teen Titans Go! | Cartoon Feud
 6:15 pm | Teen Titans Go! | TV Knight 5
 6:30 pm | Teen Titans Go! | Don't Be an Icarus
 6:45 pm | Teen Titans Go! | Record Book
 7:00 pm | Amazing World of Gumball | The Triangle; The Money
 7:30 pm | Apple & Onion | Drone Shoes
 7:45 pm | Apple & Onion | The Inbetween