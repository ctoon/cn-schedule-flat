# 2021-06-20
 8:00 pm | Bob's Burgers | Ain't Miss Debatin'
 8:30 pm | Bob's Burgers | Eggs for Days
 9:00 pm | American Dad | The Last Ride of the Dodge City Rambler
 9:30 pm | American Dad | Mused and Abused
10:00 pm | Family Guy | The Boys in the Band
10:30 pm | Family Guy | Bookie of the Year
11:00 pm | Rick and Morty | Mort Dinner Rick Andre
11:30 pm | Tuca & Bertie | Planteau
12:00 am | Birdgirl | We Got the Internet
12:30 am | Aqua Teen | Robots Everywhere
12:45 am | Aqua Teen | Sirens
 1:00 am | Family Guy | The Boys in the Band
 1:30 am | Family Guy | Bookie of the Year
 2:00 am | Bob's Burgers | Ain't Miss Debatin'
 2:30 am | Bob's Burgers | Eggs for Days
 3:00 am | Rick and Morty | Mort Dinner Rick Andre
 3:30 am | Tuca & Bertie | Planteau
 4:00 am | Birdgirl | We Got the Internet
 4:30 am | Aqua Teen | Robots Everywhere
 4:45 am | Aqua Teen | Sirens
 5:00 am | Home Movies | Improving Your Life Through Improv
 5:30 am | Home Movies | Four's Company