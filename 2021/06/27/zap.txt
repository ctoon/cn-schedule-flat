# 2021-06-27
 6:00 am | Teen Titans Go! | The Academy
 6:15 am | Teen Titans Go! | Mo' Money Mo' Problems
 6:30 am | Teen Titans Go! | Throne of Bones
 6:45 am | Teen Titans Go! | Beast Girl
 7:00 am | Teen Titans Go! | TV Knight 3
 7:15 am | Teen Titans Go! | Bro-Pocalypse
 7:30 am | Teen Titans Go! | The Scoop
 7:45 am | Teen Titans Go! | Rain on Your Wedding Day
 8:00 am | DC Super Hero Girls | #SuperWonderBatBeeZeeLanterMobile
 8:15 am | DC Super Hero Girls | #SirensConch
 8:30 am | Teen Titans Go! | Don't Press Play
 8:45 am | Teen Titans Go! | Huggbees
 9:00 am | Teen Titans Go! | Pig in a Poke
 9:15 am | Teen Titans Go! | P.P.
 9:30 am | Teen Titans Go! | A Little Help Please
 9:45 am | Teen Titans Go! | Brain Percentages
10:00 am | Craig of the Creek | The Other Side: The Tournament
10:30 am | Craig of the Creek | Kelsey Quest
10:45 am | Craig of the Creek | Big Pinchy
11:00 am | Craig of the Creek | JPony
11:15 am | Craig of the Creek | The Kid From 3030
11:30 am | Craig of the Creek | Ace of Squares
11:45 am | Craig of the Creek | Power Punchers
12:00 pm | Amazing World of Gumball | The Slide
12:15 pm | Amazing World of Gumball | The Grades
12:30 pm | Amazing World of Gumball | The Loophole
12:45 pm | Amazing World of Gumball | The Diet
 1:00 pm | Amazing World of Gumball | The Fuss
 1:15 pm | Amazing World of Gumball | The Ex
 1:30 pm | Amazing World of Gumball | The News
 1:45 pm | Amazing World of Gumball | The Menu
 2:00 pm | Amazing World of Gumball | The Vase
 2:15 pm | Amazing World of Gumball | The Uncle
 2:30 pm | Amazing World of Gumball | The Ollie
 2:45 pm | Amazing World of Gumball | The Heart
 3:00 pm | Victor and Valentino | Band for Life
 3:15 pm | Victor and Valentino | On Nagual Hill
 3:30 pm | Victor and Valentino | Tez Says
 3:45 pm | Victor and Valentino | Dance Reynaldo Dance
 4:00 pm | Teen Titans Go! | Tower Renovation
 4:15 pm | Teen Titans Go! | Quantum Fun
 4:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular, Part 1
 5:00 pm | Teen Titans Go! | The Fight
 5:15 pm | Teen Titans Go! | Genie President
 5:30 pm | Teen Titans Go! | Kabooms
 5:45 pm | Teen Titans Go! | The Mug
 6:00 pm | Amazing World of Gumball | The Quest; The Spoon
 6:30 pm | Amazing World of Gumball | The Microwave; The Meddler
 7:00 pm | Amazing World of Gumball | The Petals
 7:15 pm | Amazing World of Gumball | The Deal
 7:30 pm | Amazing World of Gumball | The Nuisance
 7:45 pm | Amazing World of Gumball | The Line