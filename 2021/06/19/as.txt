# 2021-06-19
 8:00 pm | Final Space | The Devil's Den
 8:30 pm | American Dad | My Affair Lady
 9:00 pm | Family Guy | 8 Simple Rules for Buying My Teenage Daughter
 9:30 pm | Family Guy | Breaking Out Is Hard to Do
10:00 pm | The Boondocks | The Story of Catcher Freeman
10:30 pm | The Boondocks | It's a Black President, Huey Freeman
11:00 pm | Black Dynamite | "The S*** That Killed the King" or "Weekend at Presley's"
11:30 pm | Black Dynamite | "Roots: The White Album" or "The Blacker the Community the Deeper the Roots!"
12:00 am | My Hero Academia | Match 3
12:30 am | Dr. Stone | Prison Break
 1:00 am | Food Wars! | Walking the Tightrope of Umami
 1:30 am | The Promised Neverland | Episode 11
 2:00 am | Black Clover | The 5 Spirit Guardians
 2:30 am | Naruto:Shippuden | World of Dreams
 3:00 am | Attack on Titan | Assault
 3:30 am | Dragon Ball Super | A Perfect Survival Strategy! The 3rd Universe's Menacing Assassin!
 4:00 am | American Dad | My Affair Lady
 4:30 am | Final Space | The Devil's Den
 5:00 am | Home Movies | Breaking Up Is Hard to Do
 5:30 am | Home Movies | Bad Influences