# 2021-06-15
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Amazing World of Gumball | The Flower; The Banana
 7:30 am | Amazing World of Gumball | The Phone; The Job
 8:00 am | Teen Titans Go! | Mo' Money Mo' Problems
 8:15 am | Teen Titans Go! | The Scoop
 8:30 am | Teen Titans Go! | Beast Girl
 8:45 am | Teen Titans Go! | Tower Renovation
 9:00 am | Teen Titans Go! | Villains in a Van Getting Gelato
 9:15 am | Teen Titans Go! | I Am Chair
 9:30 am | Teen Titans Go! | Bumgorf
 9:45 am | Teen Titans Go! | The Mug
10:00 am | Craig of the Creek | Pencil Break Mania
10:15 am | Craig of the Creek | I Don't Need a Hat
10:30 am | Craig of the Creek | The Last Game of Summer
10:45 am | Craig of the Creek | Alternate Creekiverse
11:00 am | Craig of the Creek | Sour Candy Trials
11:15 am | Craig of the Creek | Sleepover at JP's
11:30 am | Craig of the Creek | Council of the Creek
11:45 am | Craig of the Creek | The Evolution of Craig
12:00 pm | Teen Titans Go! | Labor Day
12:15 pm | Teen Titans Go! | Ones and Zeroes
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:00 pm | Amazing World of Gumball | The Party; The Refund
 1:30 pm | Amazing World of Gumball | The Robot; The Picnic
 2:00 pm | Amazing World of Gumball | The Goons; The Secret
 2:30 pm | Amazing World of Gumball | The Sock; The Genius
 3:00 pm | Craig of the Creek | Welcome to Creek Street
 3:15 pm | Craig of the Creek | Jessica Goes to the Creek
 3:30 pm | Craig of the Creek | Fan or Foe
 3:45 pm | Craig of the Creek | The Final Book
 4:00 pm | Craig of the Creek | Summer Wish
 4:15 pm | Craig of the Creek | Ancients of the Creek
 4:30 pm | Craig of the Creek | Turning the Tables
 4:45 pm | Craig of the Creek | Mortimor to the Rescue
 5:00 pm | Total DramaRama | Simply Perfect
 5:15 pm | Total DramaRama | There Are No Hoppy Endings
 5:30 pm | Total DramaRama | Baby Brother Blues
 5:45 pm | Total DramaRama | Duncan Disorderly
 6:00 pm | Teen Titans Go! | Demon Prom
 6:15 pm | Teen Titans Go! | Bro-Pocalypse
 6:30 pm | Teen Titans Go! | BBCYFSHIPBDAY
 6:45 pm | Teen Titans Go! | Chicken in the Cradle
 7:00 pm | Amazing World of Gumball | The Fridge; The Remote
 7:30 pm | Apple & Onion | Falafel's Glory
 7:45 pm | Apple & Onion | The Music Store Thief