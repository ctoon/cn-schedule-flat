# 2021-06-01
 6:00 am | Amazing World of Gumball | The Sorcerer
 6:15 am | Amazing World of Gumball | The Catfish
 6:30 am | Amazing World of Gumball | The Choices
 6:45 am | Amazing World of Gumball | The Cycle
 7:00 am | Amazing World of Gumball | The Code
 7:15 am | Amazing World of Gumball | The Stars
 7:30 am | Amazing World of Gumball | The Test
 7:45 am | Amazing World of Gumball | The Box
 8:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
 8:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:00 am | Craig of the Creek | Dog Decider
 9:15 am | Craig of the Creek | Ace of Squares
 9:30 am | Craig of the Creek | Bring Out Your Beast
 9:45 am | Craig of the Creek | Doorway to Helen
10:00 am | Craig of the Creek | Summer Wish
10:15 am | Craig of the Creek | Kelsey the Worthy
10:30 am | Craig of the Creek | Turning the Tables
10:45 am | Craig of the Creek | The End Was Here
11:00 am | Victor and Valentino | Dead Ringer
11:15 am | Victor and Valentino | The Babysitter
11:30 am | Teen Titans GO! to the Movies | 
 1:15 pm | Amazing World of Gumball | The Traitor
 1:30 pm | Amazing World of Gumball | The Origins
 1:45 pm | Amazing World of Gumball | The Origins
 2:00 pm | Amazing World of Gumball | The Advice
 2:15 pm | Amazing World of Gumball | The Night
 2:30 pm | Amazing World of Gumball | The Signal
 2:45 pm | Amazing World of Gumball | The Misunderstandings
 3:00 pm | Amazing World of Gumball | The Girlfriend
 3:15 pm | Amazing World of Gumball | The Roots
 3:30 pm | Amazing World of Gumball | The Parasite
 3:45 pm | Amazing World of Gumball | The Blame
 4:00 pm | Craig of the Creek | Sour Candy Trials
 4:15 pm | Craig of the Creek | Mortimor to the Rescue
 4:30 pm | Craig of the Creek | Council of the Creek
 4:45 pm | Craig of the Creek | Secret in a Bottle
 5:00 pm | Total DramaRama | Sugar & Spice & Lightning & Frights
 5:15 pm | Total DramaRama | Carmageddon
 5:30 pm | Total DramaRama | A Dame-gerous Game
 5:45 pm | Total DramaRama | Inglorious Toddlers
 6:00 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 6:30 pm | Teen Titans Go! | Video Game References; Cool School
 7:00 pm | Amazing World of Gumball | The Guy
 7:15 pm | Amazing World of Gumball | The Outside
 7:30 pm | Apple & Onion | The Inbetween
 7:45 pm | Apple & Onion | Positive Attitude Theory