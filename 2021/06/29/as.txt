# 2021-06-29
 8:00 pm | Bob's Burgers | Bob Fires the Kids
 8:30 pm | Bob's Burgers | Mutiny on the Windbreaker
 9:00 pm | American Dad | Mine Struggle
 9:30 pm | American Dad | Garfield and Friends
10:00 pm | American Dad | Gift Me Liberty
10:30 pm | Rick and Morty | Mortyplicity
11:00 pm | Family Guy | Petergeist
11:30 pm | Family Guy | Untitled Griffin Family History
12:00 am | Tuca & Bertie | Kyle
12:30 am | Robot Chicken | Plastic Buffet
12:45 am | Robot Chicken | Toyz in the Hood
 1:00 am | Aqua Teen | Super Sirloin
 1:15 am | Aqua Teen | Super Squatter
 1:30 am | Family Guy | Petergeist
 2:00 am | Family Guy | Untitled Griffin Family History
 2:30 am | Bob's Burgers | Bob Fires the Kids
 3:00 am | Bob's Burgers | Mutiny on the Windbreaker
 3:30 am | Tuca & Bertie | Kyle
 4:00 am | Mike Tyson Mysteries | Love Letters
 4:15 am | Mike Tyson Mysteries | All About That Bass
 4:30 am | The Venture Brothers | The Bellicose Proxy
 5:00 am | Naruto:Shippuden | The Reanimated Allied Forces
 5:30 am | Samurai Jack | XLII