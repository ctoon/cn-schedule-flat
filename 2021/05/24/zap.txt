# 2021-05-24
 6:00 am | Amazing World of Gumball | The Boss; The Move
 6:30 am | Amazing World of Gumball | The Law; The Allergy
 7:00 am | Amazing World of Gumball | The Mothers; The Password
 7:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 8:00 am | Teen Titans Go! | Burger vs. Burrito; Matched
 8:30 am | Teen Titans Go! | Colors of Raven; The Left Leg
 9:00 am | Craig of the Creek | Winter Break
 9:30 am | Craig of the Creek | Snow Place Like Home
 9:45 am | Craig of the Creek | Craig World
10:00 am | Craig of the Creek | The Great Fossil Rush
10:15 am | Craig of the Creek | Cousin of the Creek
10:30 am | Craig of the Creek | Alone Quest
10:45 am | Craig of the Creek | Creek Daycare
11:00 am | Victor and Valentino | Tez Breaks Bread
11:15 am | Victor and Valentino | My Thirsty Little Monster
11:30 am | Teen Titans Go! | Birds; Be Mine
12:00 pm | Teen Titans Go! | Brain Food; In and Out
12:30 pm | Teen Titans Go! | Little Buddies; Missing
 1:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 1:30 pm | Amazing World of Gumball | The Hero; The Photo
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Promise; The Voice
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | Craig of the Creek | Kelsey Quest
 4:15 pm | Craig of the Creek | Bug City
 4:30 pm | Craig of the Creek | JPony
 4:45 pm | Craig of the Creek | The Shortcut
 5:00 pm | Total DramaRama | Carmageddon
 5:15 pm | Total DramaRama | Last Mom Standing
 5:30 pm | Total DramaRama | Glove Glove Me Do
 5:45 pm | Total DramaRama | Free Chili
 6:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 6:30 pm | Teen Titans Go! | Waffles; Opposites
 7:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 7:30 pm | Apple & Onion | Falafel's in Jail
 7:45 pm | Apple & Onion | Gyranoid of the Future