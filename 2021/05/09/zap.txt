# 2021-05-09
 6:00 am | Craig of the Creek | The Final Book
 6:15 am | Craig of the Creek | Too Many Treasures
 6:30 am | Craig of the Creek | Escape From Family Dinner
 6:45 am | Craig of the Creek | Monster in the Garden
 7:00 am | Craig of the Creek | The Curse
 7:15 am | Craig of the Creek | Vulture's Nest
 7:30 am | Craig of the Creek | King of Camping
 7:45 am | Craig of the Creek | I Don't Need a Hat
 8:00 am | Craig of the Creek | The Kid From 3030
 8:15 am | Craig of the Creek | Jessica's Trail
 8:30 am | Craig of the Creek | Snow Place Like Home
 8:45 am | Craig of the Creek | Breaking the Ice
 9:00 am | Craig of the Creek | Summer Wish
 9:15 am | Craig of the Creek | Cousin of the Creek
 9:30 am | Craig of the Creek | Craig and the Kid's Table
10:00 am | Craig of the Creek | Crisis at Elder Rock
10:15 am | Craig of the Creek | Kelsey the Worthy
10:30 am | Craig of the Creek | Jessica Shorts
10:45 am | Craig of the Creek | Fort Williams
11:00 am | Craig of the Creek | The Other Side: The Tournament
11:30 am | Craig of the Creek | Fall Anthology
11:45 am | Craig of the Creek | King of Camping
12:00 pm | Amazing World of Gumball | The Gumball Chronicles: Mother's Day
12:15 pm | Amazing World of Gumball | The Gumball Chronicles: Ancestor Act
12:30 pm | Amazing World of Gumball | The Signature
12:45 pm | Amazing World of Gumball | The Parking
 1:00 pm | Amazing World of Gumball | The Fuss
 1:15 pm | Amazing World of Gumball | The Choices
 1:30 pm | Amazing World of Gumball | The Outside
 1:45 pm | Amazing World of Gumball | The Copycats
 2:00 pm | Amazing World of Gumball | The Box
 2:15 pm | Amazing World of Gumball | The Vase
 2:30 pm | Amazing World of Gumball | The Worst
 2:45 pm | Amazing World of Gumball | The Heist
 3:00 pm | Amazing World of Gumball | The Nuisance
 3:15 pm | Amazing World of Gumball | The Deal
 3:30 pm | Amazing World of Gumball | The List
 3:45 pm | Amazing World of Gumball | The DVD
 4:00 pm | Amazing World of Gumball | The Ad
 4:15 pm | Amazing World of Gumball | The Parents
 4:30 pm | Amazing World of Gumball | The Master
 4:45 pm | Amazing World of Gumball | The Possession
 5:00 pm | Amazing World of Gumball | The Factory
 5:15 pm | Amazing World of Gumball | The Fury
 5:30 pm | Amazing World of Gumball | The Gumball Chronicles: Ancestor Act
 5:45 pm | Amazing World of Gumball | The Gumball Chronicles: Mother's Day
 6:00 pm | Rio 2 | 