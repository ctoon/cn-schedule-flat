# 2021-05-22
 6:00 am | Amazing World of Gumball | The Deal
 6:15 am | Amazing World of Gumball | The Transformation
 6:30 am | Amazing World of Gumball | The Line
 6:45 am | Amazing World of Gumball | The Understanding
 7:00 am | Amazing World of Gumball | The Ad
 7:15 am | Amazing World of Gumball | The Master
 7:30 am | Amazing World of Gumball | The Slip
 7:45 am | Amazing World of Gumball | The Parking
 8:00 am | Amazing World of Gumball | The Drama
 8:15 am | Amazing World of Gumball | The Future
 8:30 am | Amazing World of Gumball | The Buddy
 8:45 am | Amazing World of Gumball | The Wish
 9:00 am | Teen Titans Go! | A Little Help Please
 9:11 am | Teen Titans Go! | P.P.
 9:30 am | Total DramaRama | Carmageddon
 9:45 am | Total DramaRama | Last Mom Standing
10:00 am | Craig of the Creek | Snow Place Like Home
10:15 am | Craig of the Creek | Breaking the Ice
10:30 am | Victor and Valentino | Tez Breaks Bread
10:45 am | Victor and Valentino | My Thirsty Little Monster
11:00 am | Teen Titans Go! | The Cast
11:15 am | Teen Titans Go! | Butter Wall
11:30 am | Teen Titans Go! | Toddler Titans...Yay!
11:45 am | Teen Titans Go! | Superhero Feud
12:00 pm | Amazing World of Gumball | The Catfish
12:15 pm | Amazing World of Gumball | The Diet
12:30 pm | Amazing World of Gumball | The Cycle
12:45 pm | Amazing World of Gumball | The Ex
 1:00 pm | Amazing World of Gumball | The Stars
 1:15 pm | Amazing World of Gumball | The Menu
 1:30 pm | Amazing World of Gumball | The Box
 1:45 pm | Amazing World of Gumball | The Uncle
 2:00 pm | Amazing World of Gumball | The Matchmaker
 2:15 pm | Amazing World of Gumball | The Heist
 2:30 pm | Amazing World of Gumball | The Grades
 2:45 pm | Amazing World of Gumball | The Hug
 3:00 pm | Victor and Valentino | The Babysitter
 3:15 pm | Victor and Valentino | The Dark Room
 3:30 pm | Victor and Valentino | Hurricane Chata
 3:45 pm | Victor and Valentino | The Boy Who Cried Lechuza
 4:00 pm | Teen Titans Go! | Baby Mouth
 4:15 pm | Teen Titans Go! | BBRAEBDAY
 4:30 pm | Teen Titans Go! | Lucky Stars
 4:45 pm | Teen Titans Go! | Real Art
 5:00 pm | Teen Titans Go! | Various Modes of Transportation
 5:15 pm | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
 5:30 pm | Teen Titans Go! | Cool Uncles
 5:45 pm | Teen Titans Go! | A Little Help Please
 6:00 pm | Amazing World of Gumball | The Nuisance
 6:15 pm | Amazing World of Gumball | The Singing
 6:30 pm | Amazing World of Gumball | The Best
 6:45 pm | Amazing World of Gumball | The Puppets
 7:00 pm | Amazing World of Gumball | The Worst
 7:15 pm | Amazing World of Gumball | The Lady
 7:30 pm | Amazing World of Gumball | The List
 7:45 pm | Amazing World of Gumball | The Sucker