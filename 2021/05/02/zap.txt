# 2021-05-02
 6:00 am | Teen Titans Go! | The Groover
 6:15 am | Teen Titans Go! | I Used to Be a Peoples
 6:30 am | Teen Titans Go! | The Power of Shrimps
 6:45 am | Teen Titans Go! | The Metric System vs. Freedom
 7:00 am | Teen Titans Go! | My Name Is Jose
 7:15 am | Teen Titans Go! | The Chaff
 7:30 am | Teen Titans Go! | The Real Orangins!
 7:45 am | Teen Titans Go! | Zimdings
 8:00 am | Teen Titans Go! | How's This for a Special? Spaaaace: Pt. 1
 8:30 am | Teen Titans Go! | Teen Titans Vroom
 9:00 am | Teen Titans Go! | Super Hero Summer Camp
10:00 am | Craig of the Creek | Beyond the Rapids
10:15 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
10:30 am | Craig of the Creek | The Jinxening
10:45 am | Craig of the Creek | The Bike Thief
11:00 am | Craig of the Creek | In the Key of the Creek
11:15 am | Craig of the Creek | Craig of the Beach
11:30 am | Craig of the Creek | The Ground Is Lava!
11:45 am | Craig of the Creek | Plush Kingdom
12:00 pm | Amazing World of Gumball | The Boss; The Move
12:30 pm | Amazing World of Gumball | The Law; The Allergy
 1:00 pm | Amazing World of Gumball | The Mothers; The Password
 1:30 pm | Amazing World of Gumball | The Mirror; The Burden
 2:00 pm | Amazing World of Gumball | The Bros; The Man
 2:30 pm | Amazing World of Gumball | The Pizza; The Lie
 3:00 pm | Victor and Valentino | The Cupcake Man
 3:15 pm | Victor and Valentino | Los Pajaros
 3:30 pm | Victor and Valentino | Guillermo's Girlfriend
 3:45 pm | Victor and Valentino | Charlene Mania
 4:00 pm | Teen Titans Go! | Them Soviet Boys
 4:15 pm | Teen Titans Go! | Cartoon Feud
 4:30 pm | Teen Titans Go! | Lil' Dimples
 4:45 pm | Teen Titans Go! | Zimdings
 5:00 pm | Teen Titans Go! | Stockton, CA!
 5:15 pm | Teen Titans Go! | TV Knight 4
 5:30 pm | Teen Titans Go! | Collect Them All
 5:45 pm | Teen Titans Go! | Teen Titans Roar!
 6:00 pm | Amazing World of Gumball | The Butterfly; The Question
 6:30 pm | Amazing World of Gumball | The Oracle; The Safety
 6:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 7:30 pm | Amazing World of Gumball | The Society; The Spoiler