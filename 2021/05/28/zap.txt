# 2021-05-28
 6:00 am | Amazing World of Gumball | The World; The Finale
 6:30 am | Amazing World of Gumball | The Kids; The Fan
 7:00 am | Amazing World of Gumball | The Coach; The Joy
 7:30 am | Amazing World of Gumball | The Recipe; The Puppy
 8:00 am | Teen Titans Go! | Brian; Nature
 8:30 am | Teen Titans Go! | Salty Codgers; Knowledge
 9:00 am | Craig of the Creek | Sunday Clothes
 9:15 am | Craig of the Creek | Escape From Family Dinner
 9:30 am | Craig of the Creek | Monster in the Garden
 9:45 am | Craig of the Creek | The Curse
10:00 am | Craig of the Creek | Ace of Squares
10:15 am | Craig of the Creek | Deep Creek Salvage
10:30 am | Craig of the Creek | Dibs Court
10:45 am | Craig of the Creek | Kelsey the Author
11:00 am | Victor and Valentino | Los Pajaros
11:15 am | Victor and Valentino | Ghosted
11:30 am | Teen Titans Go! | No Power; Sidekick
12:00 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
12:30 pm | Teen Titans Go! | Legs; Breakfast Cheese
 1:00 pm | Amazing World of Gumball | The Society; The Spoiler
 1:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:00 pm | Amazing World of Gumball | The Downer; The Egg
 2:30 pm | Amazing World of Gumball | The Triangle; The Money
 3:00 pm | Amazing World of Gumball | The Return
 3:15 pm | Amazing World of Gumball | The Hug
 3:30 pm | Amazing World of Gumball | The Nemesis
 3:45 pm | Amazing World of Gumball | The Routine
 4:00 pm | Craig of the Creek | The Mystery of the Timekeeper
 4:15 pm | Craig of the Creek | The Evolution of Craig
 4:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:45 pm | Craig of the Creek | Tea Timer's Ball
 5:00 pm | Total DramaRama | Mooshy Mon Mons
 5:15 pm | Total DramaRama | Tiger Fail
 5:30 pm | Total DramaRama | Student Becomes the Teacher
 5:45 pm | Total DramaRama | A Ninjustice to Harold
 6:00 pm | Teen Titans Go! | Books; Lazy Sunday
 6:30 pm | Teen Titans Go! | Power Moves; Staring at the Future
 7:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 7:30 pm | Apple & Onion | Pulling Your Weight
 7:45 pm | Apple & Onion | Baby Boi TP