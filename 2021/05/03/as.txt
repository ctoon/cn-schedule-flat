# 2021-05-03
 8:00 pm | American Dad | Meter Made
 8:30 pm | American Dad | Dope and Faith
 9:00 pm | American Dad | Big Trouble in Little Langley
 9:30 pm | Bob's Burgers | The Horse Rider-Er
10:00 pm | Bob's Burgers | Secret Admiral-Irer
10:30 pm | Rick and Morty | Total Rickall
11:00 pm | Family Guy | Christmas Guy
11:30 pm | Family Guy | Peter Problems
12:00 am | The Boondocks | Thank You for Not Snitching
12:30 am | Robot Chicken | The Hobbit: There and Bennigan's
12:45 am | Robot Chicken | Chipotle Miserables
 1:00 am | Bob's Burgers | The Horse Rider-Er
 1:30 am | Bob's Burgers | Secret Admiral-Irer
 2:00 am | Family Guy | Christmas Guy
 2:30 am | Family Guy | Peter Problems
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | The Boondocks | Thank You for Not Snitching
 4:00 am | Hot Package | Scandal
 4:15 am | Mostly 4 Millennials | Diversity
 4:30 am | Robot Chicken | The Hobbit: There and Bennigan's
 4:45 am | Robot Chicken | Chipotle Miserables
 5:00 am | Naruto:Shippuden | A Message from the Heart
 5:30 am | Samurai Jack | I