# 2021-05-01
 6:00 am | Amazing World of Gumball | The Origins
 6:15 am | Amazing World of Gumball | The Origins
 6:30 am | The Amazing World of Gumball: Darwin's Yearbook | Alan
 6:45 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 7:00 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:15 am | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 7:30 am | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 7:45 am | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 8:00 am | Amazing World of Gumball | The Spinoffs
 8:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Anyone?
 9:00 am | Teen Titans Go! | Zimdings
 9:11 am | Teen Titans Go! | Hafo Safo
 9:30 am | Total DramaRama | Weekend at Buddy's
 9:45 am | Total DramaRama | Broken Back Kotter
10:00 am | Craig of the Creek | Winter Creeklympics
10:15 am | Craig of the Creek | Welcome to Creek Street
10:30 am | Victor and Valentino | Cenote Seekers
10:45 am | Victor and Valentino | Baby Pepito
11:00 am | Teen Titans Go! | The Scoop
11:15 am | Teen Titans Go! | Chicken in the Cradle
11:30 am | Teen Titans Go! | Tower Renovation
11:45 am | Teen Titans Go! | BBRBDAY
12:00 pm | Amazing World of Gumball | The Boombox; The Castle
12:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:00 pm | Amazing World of Gumball | The Internet; The Plan
 1:30 pm | Amazing World of Gumball | The World; The Finale
 2:00 pm | Amazing World of Gumball | The Kids; The Fan
 2:30 pm | Amazing World of Gumball | The Coach; The Joy
 2:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 3:00 pm | Victor and Valentino | Starry Night
 3:15 pm | Victor and Valentino | Villainy In Monte Macabre
 3:30 pm | Victor and Valentino | Get Your Sea Legs
 3:45 pm | Victor and Valentino | Fueygo Fest
 4:00 pm | Teen Titans Go! | Quantum Fun
 4:15 pm | Teen Titans Go! | Tall Titan Tales
 4:30 pm | Teen Titans Go! | The Fight
 4:45 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 5:00 pm | Teen Titans Go! | Genie President
 5:15 pm | Teen Titans Go! | Business Ethics Wink Wink
 5:30 pm | Teen Titans Go! | Little Elvis
 5:45 pm | Teen Titans Go! | Rain on Your Wedding Day
 6:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 6:30 pm | Amazing World of Gumball | The Name; The Extras
 7:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 7:30 pm | Amazing World of Gumball | The Fraud; The Void