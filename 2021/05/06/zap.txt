# 2021-05-06
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Amazing World of Gumball | The Origins
 7:15 am | Amazing World of Gumball | The Origins
 7:30 am | Amazing World of Gumball | The Society; The Spoiler
 8:00 am | Teen Titans Go! | My Name Is Jose
 8:15 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 8:30 am | Teen Titans Go! | The Real Orangins!
 8:45 am | Teen Titans Go! | I Used to Be a Peoples
 9:00 am | Teen Titans Go! | The Metric System vs. Freedom
 9:15 am | Teen Titans Go! | Communicate Openly
 9:30 am | Teen Titans Go! | The Chaff
 9:45 am | Teen Titans Go! | Teen Titans Roar!
10:00 am | Craig of the Creek | Ferret Quest
10:15 am | Craig of the Creek | Pencil Break Mania
10:30 am | Craig of the Creek | Into the Overpast
10:45 am | Craig of the Creek | The Last Game of Summer
11:00 am | Victor and Valentino | Cat-Pocalypse
11:15 am | Victor and Valentino | I... am Vampier
11:30 am | Teen Titans Go! | Beast Girl
11:45 am | Teen Titans Go! | The Scoop
12:00 pm | Teen Titans Go! | TV Knight 3
12:15 pm | Teen Titans Go! | Chicken in the Cradle
12:30 pm | Teen Titans Go! | Bro-Pocalypse
12:45 pm | Teen Titans Go! | Tower Renovation
 1:00 pm | Amazing World of Gumball | The Hug
 1:15 pm | Amazing World of Gumball | The Points
 1:30 pm | Amazing World of Gumball | The Disaster
 1:45 pm | Amazing World of Gumball | The Re-Run
 2:00 pm | Amazing World of Gumball | The Routine
 2:15 pm | Amazing World of Gumball | The Night
 2:30 pm | Amazing World of Gumball | The Parking
 2:45 pm | Amazing World of Gumball | The Misunderstandings
 3:00 pm | Amazing World of Gumball | The Upgrade
 3:15 pm | Amazing World of Gumball | The Roots
 3:30 pm | Amazing World of Gumball | The Comic
 3:45 pm | Amazing World of Gumball | The Blame
 4:00 pm | Craig of the Creek | The Ground Is Lava!
 4:15 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:30 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 4:45 pm | Craig of the Creek | I Don't Need a Hat
 5:00 pm | Total DramaRama | All Up in Your Drill
 5:15 pm | Total DramaRama | Gobble Head
 5:30 pm | Total DramaRama | Toys Will Be Toys
 5:45 pm | Total DramaRama | Snack to the Future
 6:00 pm | Teen Titans Go! | Lil' Dimples
 6:15 pm | Teen Titans Go! | Had to Be There
 6:30 pm | Teen Titans Go! | Stockton, CA!
 6:45 pm | Teen Titans Go! | The Great Disaster
 7:00 pm | Amazing World of Gumball | The Bros; The Man
 7:30 pm | Apple & Onion | Block Party
 7:45 pm | Apple & Onion | Face Your Fears