# 2021-05-14
 8:00 pm | American Dad | Weiner of Our Discontent
 8:30 pm | American Dad | Daddy Queerest
 9:00 pm | American Dad | Stan's Night Out
 9:30 pm | American Dad | In Country...Club
10:00 pm | Family Guy | This Little Piggy
10:30 pm | Family Guy | Quagmire's Mom
11:00 pm | Family Guy | Encyclopedia Griffin
11:30 pm | Rick and Morty | Never Ricking Morty
12:00 am | Birdgirl | Pilot
12:30 am | Birdgirl | ShareBear
 1:00 am | Birdgirl | Thirdgirl
 1:30 am | Birdgirl | We Got the Internet
 2:00 am | Birdgirl | Topple the Popple
 2:30 am | Birdgirl | Baltimo
 3:00 am | Birdgirl | Pilot
 3:30 am | Rick and Morty | Never Ricking Morty
 4:00 am | Ballmastrz: 9009 | Breathe Deep to Win! Teamwork Cuts Through the Foul Odor of Obsession!
 4:15 am | Hot Streets | Nursery Rhyme Land
 4:30 am | Infomercials | Broomshakalaka
 4:45 am | Infomercials | For-Profit Online University
 5:00 am | Naruto:Shippuden | The Helmet Splitter: Jinin Akebino!
 5:30 am | Samurai Jack | X