# 2021-05-15
 8:00 pm | American Dad | Moon Over Isla Island
 8:30 pm | American Dad | Home Adrone
 9:00 pm | American Dad | Brains, Brains, and Automobiles
 9:30 pm | American Dad | Man in the Moonbounce
10:00 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
10:30 pm | Final Space | Hyper-Transdimensional Bridge Rising
11:00 pm | Family Guy | Stewie Is Enceinte
11:30 pm | Family Guy | Dr. C & the Women
12:00 am | Dragon Ball Super | Goku vs. Kefla! Super Saiyan Blue Beaten?
12:30 am | My Hero Academia | Vestiges
 1:00 am | Dr. Stone | Stone Wars Beginning
 1:30 am | Food Wars! | Those Who Strive for the Top
 2:00 am | The Promised Neverland | Episode 6
 2:30 am | Black Clover | The Maidens' Challenge
 3:00 am | Naruto:Shippuden | Orochimaru's Return
 3:30 am | Attack on Titan | Midnight Train
 4:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 4:30 am | Final Space | Hyper-Transdimensional Bridge Rising
 5:00 am | Home Movies | Method of Acting
 5:30 am | Home Movies | Life Through a Fish Eye Lens