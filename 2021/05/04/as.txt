# 2021-05-04
 8:00 pm | American Dad | Haylias
 8:30 pm | American Dad | 42-Year-Old Virgin
 9:00 pm | American Dad | Surro-Gate
 9:30 pm | Bob's Burgers | Glued, Where's My Bob?
10:00 pm | Bob's Burgers | Flu-Ouise
10:30 pm | Rick and Morty | Get Schwifty
11:00 pm | Family Guy | Blue Harvest Part 1 & 2
12:00 am | Robot Chicken | Robot Chicken: Star Wars Special
12:30 am | Robot Chicken | Robot Chicken: Star Wars Episode II
 1:00 am | Bob's Burgers | Glued, Where's My Bob?
 1:30 am | Bob's Burgers | Flu-Ouise
 2:00 am | Family Guy | Blue Harvest Part 1 & 2
 3:00 am | Rick and Morty | Get Schwifty
 3:30 am | Robot Chicken | Robot Chicken: Star Wars Special
 4:00 am | Hot Package | Romance
 4:15 am | Mostly 4 Millennials | Bravery
 4:30 am | Robot Chicken | Robot Chicken: Star Wars Episode II
 5:00 am | Naruto:Shippuden | Attack of the Gedo Statue
 5:30 am | Samurai Jack | II