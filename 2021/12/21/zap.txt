# 2021-12-21
 6:00 am | Baby Looney Tunes | A Turtle Named Mrytle; Oh My Darling Coyote (song); There's Nothing Like A Good Book
 6:30 am | Baby Looney Tunes | The Dolly Vanishes; Pepe Le Pew The Skunk (song); Duck's Reflucks
 7:00 am | Caillou | Magnet Madness; Caillou the Dinosaur Hunter; Caillou the Astronaut
 7:30 am | Caillou | Caillou Helps Out; Caillou the Firefighter; Caillou to the Rescue
 8:00 am | Pocoyo | Sleepy Bird's Surprise
 8:07 am | Pocoyo | Where's Pocoyo?
 8:14 am | Pocoyo | Drummer Boy
 8:21 am | Pocoyo | Christmas Tree
 8:30 am | Pocoyo | 
 8:40 am | Pocoyo | An Alien Christmas Carol
 8:50 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | The Biggest Adventure Club
 9:15 am | Thomas & Friends: All Engines Go | Chasing Rainbows
 9:30 am | Thomas & Friends: All Engines Go | Capture the Flag
 9:45 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
10:00 am | Bing | Kite
10:10 am | Bing | More
10:20 am | Bing | Sparkle Magic
10:30 am | Pocoyo | Sleepy Bird's Surprise
10:40 am | Pocoyo | Where's Pocoyo?
10:50 am | Pocoyo | Christmas Tree
11:00 am | Pocoyo | 
11:10 am | Pocoyo | An Alien Christmas Carol
11:20 am | Pocoyo | 
11:30 am | Mush-Mush and the Mushables | Friends of the Forest
12:00 pm | Craig of the Creek | Kelsey the Elder
12:15 pm | Craig of the Creek | Sour Candy Trials
12:30 pm | Craig of the Creek | Council of the Creek
12:45 pm | Craig of the Creek | Sparkle Cadet
 1:00 pm | Craig of the Creek | Itch to Explore
 1:15 pm | Craig of the Creek | You're It
 1:30 pm | Craig of the Creek | Jessica Goes to the Creek
 1:45 pm | Craig of the Creek | The Final Book
 2:00 pm | The LEGO Batman Movie | 
 4:00 pm | Craig of the Creek | Winter Creeklympics
 4:15 pm | Craig of the Creek | Welcome to Creek Street
 4:30 pm | Craig of the Creek | Fan or Foe
 4:45 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 5:00 pm | Craig of the Creek | New Jersey
 5:15 pm | Craig of the Creek | The Bike Thief
 5:30 pm | Teen Titans Go! | Second Christmas
 5:45 pm | Teen Titans Go! | Breakfast
 6:00 pm | Teen Titans Go! | Captain Cool
 6:10 pm | Teen Titans Go! | Glunkakakakah
 6:20 pm | Teen Titans Go! | 
 6:30 pm | Teen Titans Go! | Control Freak
 6:40 pm | Teen Titans Go! | The Cast
 6:50 pm | Teen Titans Go! | More of the Same
 7:00 pm | Amazing World of Gumball | The Apprentice
 7:15 pm | Amazing World of Gumball | The Romantic
 7:30 pm | Amazing World of Gumball | The Check
 7:45 pm | Amazing World of Gumball | The Uploads
 8:00 pm | Amazing World of Gumball | The Pest
 8:15 pm | Amazing World of Gumball | The Wicked
 8:30 pm | Amazing World of Gumball | The Hug
 8:45 pm | Amazing World of Gumball | The Traitor