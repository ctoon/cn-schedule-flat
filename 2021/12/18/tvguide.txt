# 2021-12-18
 6:00 am | Baby Looney Tunes | Blast Off Bugs; Baby Brouhaha
 6:30 am | Baby Looney Tunes | Log Cabin Fever; A Mid-Autumn Night's Scream
 7:00 am | Lucas the Spider | Boingo Something up There Home Sweet Home
 7:30 am | Esme & Roy | Monster Mash; Block Party
 8:00 am | Amazing World of Gumball | The Box; The Stars
 8:30 am | Amazing World of Gumball | Christmas; The Lie
 9:00 am | Teen Titans Go! | Holiday Story; A; Beast Boy on a Shelf
 9:30 am | Steven Universe | Three Gems and a Baby; Winter Forecast
10:00 am | Ben 10 | Merry Christmas
10:30 am | Adventure Time | The More You Moe, the Moe You Know
11:00 am | Amazing World of Gumball | The Matchmaker; The Uncle
11:30 am | Amazing World of Gumball | The Grades; The; Heist
12:00 pm | Amazing World of Gumball | The Diet; The; Petals
12:30 pm | Amazing World of Gumball | The Ex; The; Nuisance
 1:00 pm | Craig of the Creek | The Cardboard Identity; The; Children of the Dog
 1:30 pm | Craig of the Creek | Ancients of the Creek; Jessica Shorts
 2:00 pm | Craig of the Creek | Mortimor to the Rescue Ferret Quest
 2:30 pm | Craig of the Creek | Secret in a Bottle; Jpony
 3:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 3:30 pm | Teen Titans Go! | Wafflesopposites; The Academy
 4:00 pm | Teen Titans Go! | Birds; Be Mine
 4:30 pm | Teen Titans Go! | Brain Food; In and Out
 5:00 pm | Scooby-Doo | Beast Girl
 7:00 pm | The LEGO Movie 2: The Second Part | 