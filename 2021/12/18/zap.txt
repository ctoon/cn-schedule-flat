# 2021-12-18
 6:00 am | Baby Looney Tunes | Blast Off Bugs; Baby Brouhaha
 6:30 am | Baby Looney Tunes | Log Cabin Fever; A Mid-Autumn Night's Scream
 7:00 am | Lucas the Spider | Boingo
 7:10 am | Lucas the Spider | Something Up There
 7:20 am | Lucas the Spider | Home Sweet Home
 7:30 am | Esme & Roy | Monster Mash; Block Party
 8:00 am | Amazing World of Gumball | The Stars
 8:15 am | Amazing World of Gumball | The Box
 8:30 am | Amazing World of Gumball | Christmas
 8:45 am | Amazing World of Gumball | The Lie
 9:00 am | Teen Titans Go! | A Holiday Story
 9:10 am | Teen Titans Go! | Beast Boy on a Shelf
 9:20 am | Teen Titans Go! | 
 9:30 am | Steven Universe | Winter Forecast
 9:45 am | Steven Universe | Three Gems and a Baby
10:00 am | Ben 10 | Merry Christmas
10:30 am | Adventure Time | The More You Moe; The Moe You Know
11:00 am | Amazing World of Gumball | The Matchmaker
11:15 am | Amazing World of Gumball | The Uncle
11:30 am | Amazing World of Gumball | The Grades
11:45 am | Amazing World of Gumball | The Heist
12:00 pm | Amazing World of Gumball | The Diet
12:15 pm | Amazing World of Gumball | The Petals
12:30 pm | Amazing World of Gumball | The Ex
12:45 pm | Amazing World of Gumball | The Nuisance
 1:00 pm | Craig of the Creek | The Cardboard Identity
 1:15 pm | Craig of the Creek | The Children of the Dog
 1:30 pm | Craig of the Creek | Ancients of the Creek
 1:45 pm | Craig of the Creek | Jessica Shorts
 2:00 pm | Craig of the Creek | Mortimor to the Rescue
 2:15 pm | Craig of the Creek | Ferret Quest
 2:30 pm | Craig of the Creek | Secret in a Bottle
 2:45 pm | Craig of the Creek | JPony
 3:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 3:30 pm | Teen Titans Go! | Waffles; Opposites
 3:45 pm | Teen Titans Go! | The Academy
 4:00 pm | Teen Titans Go! | Birds; Be Mine
 4:30 pm | Teen Titans Go! | Brain Food; In and Out
 5:00 pm | Scooby-Doo | 
 6:00 pm | Scooby-Doo | 
 7:00 pm | The LEGO Movie 2 | 