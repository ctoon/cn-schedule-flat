# 2021-12-17
 7:00 pm | MOVIE | Scooby-Doo / Cartoon Feud
 9:00 pm | King Of the Hill | Hillennium
 9:30 pm | King Of the Hill | 'Twas the Nut Before Christmas
10:00 pm | Bob's Burgers | The Bleakening Part 1
10:30 pm | Bob's Burgers | The Bleakening Part 2
11:00 pm | American Dad | Dreaming of a White Porsche Christmas
11:30 pm | American Dad | Ninety North, Zero West
12:00 am | Rick and Morty | Anatomy Park
12:30 am | Squidbillies | One Man Banned
12:45 am | Squidbillies | Let 'er R.I.P.
 1:00 am | Squidbillies | No Space Like Home
 1:15 am | Squidbillies | Scorn on the 4th of July
 1:30 am | Squidbillies | Zen and the Art of Truck-Boat-Truck Maintenance
 1:45 am | Squidbillies | Who-Gives-A-Flip?
 2:00 am | Squidbillies | Ol' Hootie
 2:30 am | Squidbillies | The Liceman Cometh
 2:45 am | Squidbillies | This Show Was Called Squidbillies
 3:00 am | Rick and Morty | Anatomy Park
 3:30 am | Rick and Morty | Morty's Mind Blowers
 4:00 am | King Of the Hill | Hillennium
 4:30 am | King Of the Hill | 'Twas the Nut Before Christmas
 5:00 am | Bob's Burgers | The Bleakening Part 1
 5:30 am | Bob's Burgers | The Bleakening Part 2