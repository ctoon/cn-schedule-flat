# 2021-12-07
 6:00 am | Baby Looney Tunes | Loose Change; Oh Where, Oh Where Has My Baby Martian Gone (song); Act Your Age
 6:30 am | Baby Looney Tunes | The Who's Your Granny?; If You're Looney (song); Tattletale
 7:00 am | Caillou | Caillou's Favorite Plate; Fun in the Mud; All Aboard!; Gilbert's House; I Scream for Ice Cream!
 7:30 am | Caillou | Super Caillou!; Trip on the Subway; Caillou's Race; As Good as New; I, Robot
 8:00 am | Pocoyo | Elly's Doll
 8:07 am | Pocoyo | Wackily Ever After
 8:14 am | Pocoyo | Mr Big Duck
 8:21 am | Pocoyo | Guess What?
 8:30 am | Pocoyo | All for One
 8:40 am | Pocoyo | Band of Friends
 8:50 am | Pocoyo | Upside Down
 9:00 am | Thomas & Friends: All Engines Go | Nia's Balloon Blunder
 9:15 am | Thomas & Friends: All Engines Go | Backwards Day
 9:30 am | Thomas & Friends: All Engines Go | Super Screen Cleaners
 9:45 am | Thomas & Friends: All Engines Go | Thomas' Promise
10:00 am | Bing | Balloon
10:10 am | Bing | Bye Bye
10:20 am | Bing | Smoothie
10:30 am | Bing | Car Park
10:40 am | Bing | Here I Go
10:50 am | Bing | Voo Voo
11:00 am | Pocoyo | Elly's Doll
11:10 am | Pocoyo | Wackily Ever After
11:20 am | Pocoyo | Mr Big Duck
11:30 am | Pocoyo | All for One
11:40 am | Pocoyo | Band of Friends
11:50 am | Pocoyo | Upside Down
12:00 pm | Mush-Mush and the Mushables | Dare For More
12:15 pm | Mush-Mush and the Mushables | My Muddy Buddy
12:30 pm | Mush-Mush and the Mushables | Who's Eating Mushton
12:45 pm | Mush-Mush and the Mushables | The Snake Dance
 1:00 pm | Craig of the Creek | Power Punchers
 1:15 pm | Craig of the Creek | The Shortcut
 1:30 pm | Craig of the Creek | Creek Cart Racers
 1:45 pm | Craig of the Creek | Deep Creek Salvage
 2:00 pm | Amazing World of Gumball | The Phone; The Job
 2:30 pm | Amazing World of Gumball | The Words; The Apology
 3:00 pm | Amazing World of Gumball | The Watch; The Bet
 3:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:00 pm | Apple & Onion | A Matter of Pride
 4:15 pm | Apple & Onion | Eyesore a Sunset
 4:30 pm | Apple & Onion | For Queen and Country
 4:45 pm | Apple & Onion | Petri
 5:00 pm | Apple & Onion | Lowlifes
 5:15 pm | Apple & Onion | Falafel's Car Keys
 5:30 pm | Apple & Onion | Sneakerheads
 5:45 pm | Apple & Onion | Filthy Rich
 6:00 pm | Apple & Onion | Truffle Season
 6:15 pm | Apple & Onion | Lambporcini
 6:30 pm | Apple & Onion | Jerk Chicken
 6:45 pm | Apple & Onion | Open House Cookies
 7:00 pm | Apple & Onion | World Cup
 7:15 pm | Apple & Onion | Win It or Bin It
 7:30 pm | Apple & Onion | Christmas Special
 7:45 pm | Apple & Onion | One Hit Wonder
 8:00 pm | Apple & Onion | Good Job
 8:15 pm | Apple & Onion | Nothing Can Stop Us
 8:30 pm | Apple & Onion | World Cup
 8:45 pm | Apple & Onion | Win It or Bin It