# 2021-12-08
 6:00 am | Baby Looney Tunes | The Dolly Vanishes; Vive Le Pew, Le Skunk (song); Duck's Reflucks
 6:30 am | Baby Looney Tunes | Bend It Like Petunia; He'll Be Zoomin' Round The Mountain (song); Cock-a-doodle-doo-it!
 7:00 am | Caillou | Caillou Goes Caroling; a Playschool Party; Caillou's Christmas Eve
 7:30 am | Caillou | Snowflakescaillou Shoots! He Scores! Caillou Goes Tobogganing
 8:00 am | Pocoyo | Christmas Tree; Mad Mix Machine; Messy Guest; the; New on the Planet
 8:30 am | Pocoyo | Very Special Guest; A; Space Christmas; Christmas Far From Home
 9:00 am | Thomas & Friends: All Engines Go | Real Number One; The; Lost and Found
 9:30 am | Thomas & Friends: All Engines Go | Music Is Everywhere; Overnight Stop
10:00 am | Bing | Talkie Taxi; Boo; Big Boots
10:30 am | Bing | Shadow; Fireworks; Ducks
11:00 am | Pocoyo | The Christmas Tree; Mad Mix Machine; Messy Guest
11:30 am | Pocoyo | Very Special Guest; A; Space Christmas; Christmas Far From Home
12:00 pm | Mush Mush and the Mushables | A Getting Truffy With It; Day With Starmush
12:30 pm | Mush Mush and the Mushables | Bird Call; Eyes on the Surprise
 1:00 pm | Craig of the Creek | Secret Book Club Dibs Court
 1:30 pm | Craig of the Creek | The Jextra Perrestrial Great Fossil Rush
 2:00 pm | Amazing World of Gumball | The Authority; The Virus
 2:30 pm | Amazing World of Gumball | The Pony; The Storm
 3:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 3:30 pm | Amazing World of Gumball | The Hero; The Photo
 4:00 pm | Teen Titans Go! | Body Adventure Grandma Voice
 4:30 pm | Teen Titans Go! | Little Buddies Meatball Party
 5:00 pm | Teen Titans Go! | 40% 40% 20%; ; ; Oil Drums
 5:30 pm | Teen Titans Go! | Operation Tin Man Opposites
 6:00 pm | Teen Titans Go! | Pie Bros Power Moves
 6:30 pm | Teen Titans Go! | Road Trip / Real Boy Adventures
 7:00 pm | Teen Titans Go! | A Holiday Story
 7:15 pm | Teen Titans Go! | Christmas Crusaders
 7:30 pm | Teen Titans Go! | Beast Boy On A Shelf
 8:00 pm | Teen Titans Go! | Teen Titans Save Christmas; The True Meaning of Christmas
 8:30 pm | Teen Titans Go! | Holiday Story; A Second Christmas