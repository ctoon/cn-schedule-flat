# 2021-12-03
 6:00 am | Baby Looney Tunes | Wrong!; Win, Lose or Daffy
 6:30 am | Baby Looney Tunes | Log Cabin Fever; A Mid-Autumn Night's Scream
 7:00 am | Caillou | Caillou Helps Out; Caillou the Firefighter; Caillou to the Rescue
 7:30 am | Caillou | Caillou and the Slide; Caillou and the Sailor; Caillou and the Creepy Crawlies
 8:00 am | Pocoyo | Get Lost Loula
 8:07 am | Pocoyo | Dance Off
 8:14 am | Pocoyo | Whale's Birthday
 8:21 am | Pocoyo | Pocoyo's Little Friend
 8:30 am | Pocoyo | Color My World
 8:40 am | Pocoyo | Bedtime
 8:50 am | Pocoyo | A Little Something Between Friends
 9:00 am | Thomas & Friends: All Engines Go | Thomas' Day Off
 9:15 am | Thomas & Friends: All Engines Go | Overnight Stop
 9:30 am | Thomas & Friends: All Engines Go | Chasing Rainbows
 9:45 am | Thomas & Friends: All Engines Go | Thomas' Promise
10:00 am | Bing | Blankie
10:10 am | Bing | 
10:20 am | Bing | Knack
10:30 am | Bing | Bake
10:40 am | Bing | Fireworks
10:50 am | Bing | Smoothie
11:00 am | Pocoyo | Get Lost Loula
11:10 am | Pocoyo | Dance Off
11:20 am | Pocoyo | Whale's Birthday
11:30 am | Pocoyo | Color My World
11:40 am | Pocoyo | Bedtime
11:50 am | Pocoyo | A Little Something Between Friends
12:00 pm | Mush-Mush and the Mushables | Puff's Perfect Picnic
12:15 pm | Mush-Mush and the Mushables | Star Helpers
12:30 pm | Mush-Mush and the Mushables | Stuck In The Mud
12:45 pm | Mush-Mush and the Mushables | The Hero Trap
 1:00 pm | Craig of the Creek | Kelsey Quest
 1:15 pm | Craig of the Creek | JPony
 1:30 pm | Craig of the Creek | Ace of Squares
 1:45 pm | Craig of the Creek | The Last Kid in the Creek
 2:00 pm | Amazing World of Gumball | The Quest; The Spoon
 2:30 pm | Amazing World of Gumball | The Car; The Curse
 3:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:30 pm | Amazing World of Gumball | The Helmet; The Fight
 4:00 pm | Craig of the Creek | The Evolution of Craig
 4:15 pm | Craig of the Creek | The Bike Thief
 4:30 pm | Craig of the Creek | Tea Timer's Ball
 4:45 pm | Craig of the Creek | Craig of the Beach
 5:00 pm | Craig of the Creek | The Cardboard Identity
 5:15 pm | Craig of the Creek | Plush Kingdom
 5:30 pm | Teen Titans Go! | Lucky Stars
 5:45 pm | Teen Titans Go! | Superhero Feud
 6:00 pm | Teen Titans Go! | Various Modes of Transportation
 6:15 pm | Teen Titans Go! | BBRAEBDAY
 6:30 pm | Teen Titans Go! | Cool Uncles
 6:45 pm | Teen Titans Go! | Real Art
 7:00 pm | Teenage Mutant Ninja Turtles III | 