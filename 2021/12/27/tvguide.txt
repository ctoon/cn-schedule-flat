# 2021-12-27
 6:00 am | Baby Looney Tunes | Takers Keepersd-a-F-F-y to Tell the Tooth
 6:30 am | Baby Looney Tunes | Spin Outtaz's Fridge Snow Day
 7:00 am | Caillou | Captain Caillouloudest Noise; Thecaillou's First Wedding
 7:30 am | Caillou | Shoo; Shoo Bird; Fly Away! Caillou's Road Tripcaillou and the Dragon
 8:00 am | Pocoyo | Poczilla; Monster Mystery; Having a Ball; Super Pocoyo
 8:30 am | Pocoyo | Surprise for Pocoyo; A; Let's Go Camping! ; Pocoyo; Pocoyo
 9:00 am | Thomas & Friends: All Engines Go | Race for the Sodor Cup
10:00 am | Bing | Surprise Machine; Sleepover; Here I Go
10:30 am | Bing | Growing; Voo Voo; Musical Statues
11:00 am | Pocoyo | Poczilla; Monster Mystery; Having a Ball
11:30 am | Pocoyo | Surprise for Pocoyo; A; Let's Go Camping! ; Pocoyo; Pocoyo
12:00 pm | Mush Mush and the Mushables | Watch the Storm; Special Delivery
12:30 pm | Mush Mush and the Mushables | The Adventurers; The; Perfect Fritter
 1:00 pm | Craig of the Creek | Crisis at Elder Rock Kelsey the Worthy
 1:30 pm | Craig of the Creek | The End Was Here; The; Children of the Dog
 2:00 pm | Amazing World of Gumball | The Nest; The Detective
 2:30 pm | Amazing World of Gumball | The Points; The Fury
 3:00 pm | Amazing World of Gumball | The Bus; The; Compilation
 3:30 pm | Amazing World of Gumball | The Stories; The; Vase
 4:00 pm | Craig of the Creek | Capture the Flag Part I: The Candy; Capture the Flag Part II: The King
 4:30 pm | Craig of the Creek | Capture the Flag part 3: The Legend; Capture the Flag part4: The Plan
 5:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 5:30 pm | Teen Titans Go! | Ghostboy; La Larva de Amor
 6:00 pm | TMNT | 