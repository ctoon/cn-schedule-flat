# 2021-12-19
 6:00 am | Baby Looney Tunes | Are We There Yet?; Save Our Cinnamon
 6:30 am | Baby Looney Tunes | Backstage Bugs; Lights! Camera! Tweety!
 7:00 am | Love Monster | Happy to Help Day; Picnic Night; Carrot Day
 7:30 am | Care Bears: Unlock the Music - Songs for Kids | Hide and Sneak Dibble's Dust-Up
 8:00 am | Craig of the Creek | Locked out Cold; Snow Day
 8:30 am | Codename: Kids Next Door | Operation: N.A.U.G.H.T.Y.
 9:00 am | Ed, Edd n Eddy | Fa La-La-La-La Ed; Cry Ed
 9:30 am | Courage the Cowardly Dog | The Beaver's Tale; Anutcracker
10:00 am | Craig of the Creek | Trading Day; Ace of Squares
10:30 am | Craig of the Creek | The Crisis at Elder Rock; Last Kid in the Creek
11:00 am | Amazing World of Gumball | The Weirdo; The; Best
11:30 am | Amazing World of Gumball | The Menu; The; Worst
12:00 pm | Amazing World of Gumball | Christmas; The Lie
12:30 pm | Amazing World of Gumball | The Deal; The List
 1:00 pm | Craig of the Creek | Kelsey the Worthy; Doorway to Helen
 1:30 pm | Craig of the Creek | The End Was Here; The; Climb
 2:00 pm | Craig of the Creek | The Kid From 3030; Big Pinchy
 2:30 pm | Craig of the Creek | Creek Cart Racers; Power Punchers
 3:00 pm | Teen Titans Go! | Dreams; Grandma Voice
 3:30 pm | Teen Titans Go! | Holiday Story; A; Beast Boy on a Shelf
 4:00 pm | Teen Titans Go! | Doomsday Preppers What a Boy Wonders
 4:30 pm | Teen Titans Go! | Fat Cats
 4:45 pm | Teen Titans Go! | Night Begins to Shine 2: You're the One
 6:00 pm | Minions | 
 8:00 pm | Harry Potter: Hogwarts Tournament of Houses | The Grand Final