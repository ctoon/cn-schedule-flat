# 2021-12-20
 9:00 pm | King Of the Hill | The Father, the Son and J.C.
 9:30 pm | King Of the Hill | The Unbearable Blindness of Laying
10:00 pm | Bob's Burgers | Bob Rest Ye Merry Gentle-Mannequins
10:30 pm | Bob's Burgers | Christmas in the Car
11:00 pm | American Dad | The Best Christmas Story Never Told
11:30 pm | American Dad | The Most Adequate Christmas Ever
12:00 am | Rick and Morty | Mortynight Run
12:30 am | Squidbillies | Rebel With a Claus
12:45 am | Squidbillies | The War on the War on Christmas
 1:00 am | Aqua Teen | Carl Wash
 1:15 am | Aqua Teen | Robots Everywhere
 1:30 am | The Venture Brothers | The Unicorn in Captivity
 2:00 am | American Dad | The Best Christmas Story Never Told
 2:30 am | Rick and Morty | Mortynight Run
 3:00 am | Squidbillies | Rebel With a Claus
 3:15 am | Squidbillies | The War on the War on Christmas
 3:30 am | Aqua Teen | Carl Wash
 3:45 am | Aqua Teen | Robots Everywhere
 4:00 am | Mr. Pickles | Bullies
 4:15 am | Tigtone | Tigtone and the Freaks of Love
 4:30 am | King Of the Hill | The Father, the Son and J.C.
 5:00 am | Bob's Burgers | Bob Rest Ye Merry Gentle-Mannequins
 5:30 am | Bob's Burgers | Christmas in the Car