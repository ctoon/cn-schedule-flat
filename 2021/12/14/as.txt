# 2021-12-14
 9:00 pm | King Of the Hill | Rich Hank, Poor Hank
 9:30 pm | King Of the Hill | Ceci N'est Pas Une King of the Hill
10:00 pm | Bob's Burgers | Sliding Bobs
10:30 pm | Bob's Burgers | The Land Ship
11:00 pm | American Dad | The Most Adequate Christmas Ever
11:30 pm | American Dad | For Whom the Sleigh Bell Tolls
12:00 am | Rick and Morty | Rixty Minutes
12:30 am | Rick and Morty | Something Ricked This Way Comes
 1:00 am | Aqua Teen | Global Grilling
 1:15 am | Aqua Teen | Grim Reaper Gutters
 1:30 am | The Venture Brothers | The High Cost of Loathing
 2:00 am | American Dad | The Most Adequate Christmas Ever
 2:30 am | Rick and Morty | Rixty Minutes
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Aqua Teen | Global Grilling
 3:45 am | Aqua Teen | Grim Reaper Gutters
 4:00 am | Mr. Pickles | Gorzoth
 4:15 am | Tigtone | Tigtone and the Wine Crisis
 4:30 am | King Of the Hill | Rich Hank, Poor Hank
 5:00 am | Bob's Burgers | Sliding Bobs
 5:30 am | Bob's Burgers | The Land Ship