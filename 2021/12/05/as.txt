# 2021-12-05
 9:00 pm | King Of the Hill | The Unbearable Blindness of Laying
 9:30 pm | King Of the Hill | Pretty, Pretty Dresses
10:00 pm | American Dad | Season's Beatings
10:30 pm | American Dad | Minstrel Krampus
11:00 pm | Rick and Morty | Forgetting Sarick Mortshall
11:30 pm | Rick and Morty | Rickmurai Jack
12:00 am | Squidbillies | Ol' Hootie
12:30 am | Joe Pera Talks With You | Joe Pera Shows You How to Keep Functioning in Mid-Late Winter
12:45 am | Joe Pera Talks With You | Joe Pera Talks With You About Legacy
 1:00 am | Teenage Euthanasia | Adventures In Beetle Sitting
 1:30 am | American Dad | Season's Beatings
 2:00 am | American Dad | Minstrel Krampus
 2:30 am | Squidbillies | Ol' Hootie
 3:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Keep Functioning in Mid-Late Winter
 3:15 am | Joe Pera Talks With You | Joe Pera Talks With You About Legacy
 3:30 am | Rick and Morty | Forgetting Sarick Mortshall
 4:00 am | King Of the Hill | The Unbearable Blindness of Laying
 4:30 am | King Of the Hill | Pretty, Pretty Dresses
 5:00 am | King Of the Hill | Megalo Dale
 5:30 am | King Of the Hill | Boxing Luanne