# 2021-12-31
 6:00 am | Baby Looney Tunes | Tea & Basketball; Down By The Cage (song); Taz You Like It
 6:30 am | Baby Looney Tunes | Band Together; Oh Where, Oh Where Has My Baby Martian Gone (song); War Of The Weirds
 7:00 am | Caillou | Caillou's Surprise; A Surprise for Miss Martin; A Surprise Sleepover
 7:30 am | Caillou | Caillou the Patientcaillou the Police Officergrandpa's Friend
 8:00 am | Pocoyo | Farewell Friends; Elly on Ice; Guess What? ; All for One
 8:30 am | Pocoyo | Band of Friends Upside Down #206 Mad Mix Machine
 9:00 am | Thomas & Friends: All Engines Go | Race for the Sodor Cup
10:00 am | Bing | House; Dizzy; Frog
10:30 am | Bing | Fireworks; Hearts; Smoothie
11:00 am | Pocoyo | Farewell Friends; Elly on Ice; Guess What?
11:30 am | Pocoyo | Band of Friends Upside Down #206 Mad Mix Machine
12:00 pm | Mush Mush and the Mushables | Run; Mushpie; Run; Watch the Storm
12:30 pm | Mush Mush and the Mushables | Mushpot's Treasure; Mushroomates
 1:00 pm | We Bare Bears | Burrito; Panda's Date
 1:30 pm | We Bare Bears | Primal; Everyday Bears
 2:00 pm | We Bare Bears | Panda's Sneeze; Brother Up
 2:30 pm | We Bare Bears | Chloe and Ice Bear; Hibernation
 3:00 pm | We Bare Bears | Rooms; Losing Ice
 3:30 pm | We Bare Bears | Icy Nights; Captain Craboo
 4:00 pm | We Bare Bears | Grizz Helps; Panda's Friend
 4:30 pm | We Bare Bears | Yuri And The Bear; Crowbar Jones
 5:00 pm | We Bare Bears | Grizzly the Movie; Coffee Cave
 5:30 pm | We Bare Bears | Panda 2; Panda's Art
 6:00 pm | We Bare Bears | Icy Nights II; Ice Cave
 6:30 pm | We Bare Bears | Limo; The; Crowbar Jones: Origins
 7:00 pm | We Bare Bears | I Am Ice Bear; I, Butler
 7:30 pm | We Bare Bears | Bubble; Fire!