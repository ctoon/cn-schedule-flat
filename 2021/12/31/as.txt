# 2021-12-31
 8:00 pm | Futurama | Space Pilot 3000
 8:30 pm | Futurama | The Series Has Landed
 9:00 pm | Futurama | I, Roommate
 9:30 pm | Futurama | Love's Labour's Lost in Space
10:00 pm | Futurama | Fear of a Bot Planet
10:30 pm | Futurama | A Fishful of Dollars
11:00 pm | Futurama | My Three Suns
11:30 pm | Futurama | A Big Piece of Garbage
12:00 am | Futurama | Hell Is Other Robots
12:30 am | Futurama | A Flight to Remember
 1:00 am | Futurama | Space Pilot 3000
 1:30 am | Futurama | The Series Has Landed
 2:00 am | Futurama | I, Roommate
 2:30 am | Futurama | Love's Labour's Lost in Space
 3:00 am | Futurama | Fear of a Bot Planet
 3:30 am | Futurama | A Fishful of Dollars
 4:00 am | Futurama | My Three Suns
 4:30 am | Futurama | A Big Piece of Garbage
 5:00 am | Futurama | Hell Is Other Robots
 5:30 am | Futurama | A Flight to Remember