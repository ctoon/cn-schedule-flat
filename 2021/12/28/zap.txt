# 2021-12-28
 6:00 am | Baby Looney Tunes | Shadow Of A Doubt; John Jacob Jongle Elmer Fudd (song); Christmas In July
 6:30 am | Baby Looney Tunes | Bruce Bunny; Baby Bunny (song); Leader Of The Pack
 7:00 am | Caillou | Star Light, Star Bright; All in a Day's Work; The Cat's Meow; Caillou in Space
 7:30 am | Caillou | The Treasure Chest; A Camping We Will Go; Chopsticks; A Special Dog
 8:00 am | Pocoyo | Elly's Big Chase
 8:07 am | Pocoyo | Pocoyo Gets It Right
 8:14 am | Pocoyo | Juggling Balls
 8:21 am | Pocoyo | Fussy Duck
 8:30 am | Pocoyo | A Dog's Life
 8:40 am | Pocoyo | Pocoyolympics
 8:50 am | Pocoyo | Picture This
 9:00 am | Thomas & Friends: All Engines Go | License to Deliver
 9:15 am | Thomas & Friends: All Engines Go | A Quiet Delivery
 9:30 am | Thomas & Friends: All Engines Go | Kana Goes Slow
 9:45 am | Thomas & Friends: All Engines Go | Dragon Run
10:00 am | Bing | Bubbles
10:10 am | Bing | Paddling Pool
10:20 am | Bing | Blankie
10:30 am | Pocoyo | Elly's Big Chase
10:40 am | Pocoyo | Pocoyo Gets It Right
10:50 am | Pocoyo | Juggling Balls
11:00 am | Pocoyo | A Dog's Life
11:10 am | Pocoyo | Pocoyolympics
11:20 am | Pocoyo | Picture This
11:30 am | Mush-Mush and the Mushables | Shine, Lilit, Shine
11:45 am | Mush-Mush and the Mushables | Mush-Mush the Predictor
12:00 pm | Craig of the Creek | Bug City
12:15 pm | Craig of the Creek | The Shortcut
12:30 pm | Craig of the Creek | Deep Creek Salvage
12:45 pm | Craig of the Creek | Dibs Court
 1:00 pm | Craig of the Creek | Jessica Shorts
 1:15 pm | Craig of the Creek | Ferret Quest
 1:30 pm | Craig of the Creek | Into the Overpast
 1:45 pm | Craig of the Creek | The Time Capsule
 2:00 pm | TMNT | 
 3:00 pm | TMNT | 
 4:00 pm | Craig of the Creek | Monster in the Garden
 4:15 pm | Craig of the Creek | JPony
 4:30 pm | Craig of the Creek | The Curse
 4:45 pm | Craig of the Creek | Ace of Squares
 5:00 pm | Craig of the Creek | Dog Decider
 5:10 pm | Craig of the Creek | Doorway to Helen
 5:20 pm | Craig of the Creek | Jessica the Intern
 5:30 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 6:00 pm | Teen Titans Go! | Girls' Night Out; You're Fired
 6:30 pm | Teen Titans Go! | Super Robin; Tower Power
 7:00 pm | Amazing World of Gumball | The Guy
 7:15 pm | Amazing World of Gumball | The Ollie
 7:30 pm | Amazing World of Gumball | The Boredom
 7:45 pm | Amazing World of Gumball | The Potato