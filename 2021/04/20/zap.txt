# 2021-04-20
 6:00 am | Amazing World of Gumball | The Possession
 6:15 am | Amazing World of Gumball | The Loophole
 6:30 am | Amazing World of Gumball | The Third; The Debt
 7:00 am | Amazing World of Gumball | The Pressure; The Painting
 7:30 am | Amazing World of Gumball | The Responsible; The Dress
 8:00 am | Teen Titans Go! | The Spice Game
 8:15 am | Teen Titans Go! | BBBDay!
 8:30 am | Teen Titans Go! | I'm the Sauce
 8:45 am | Teen Titans Go! | Squash & Stretch
 9:00 am | Teen Titans Go! | Two Parter: Part One
 9:15 am | Teen Titans Go! | Two Parter: Part Two
 9:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 9:45 am | Teen Titans Go! | Accept the Next Proposition You Hear
10:00 am | Craig of the Creek | Bug City
10:15 am | Craig of the Creek | Fort Williams
10:30 am | Craig of the Creek | The Shortcut
10:45 am | Craig of the Creek | Kelsey the Elder
11:00 am | Victor and Valentino | Legend of the Hidden Skate Park
11:15 am | Victor and Valentino | The Collector
11:30 am | Teen Titans Go! | Animals: It's Just a Word!
11:45 am | Teen Titans Go! | Pyramid Scheme
12:00 pm | Teen Titans Go! | Finally a Lesson
12:15 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
12:30 pm | Teen Titans Go! | Arms Race With Legs
12:45 pm | Teen Titans Go! | History Lesson
 1:00 pm | Amazing World of Gumball | The Shippening
 1:15 pm | Amazing World of Gumball | The Spinoffs
 1:30 pm | Amazing World of Gumball | The Brain
 1:45 pm | Amazing World of Gumball | The Transformation
 2:00 pm | Amazing World of Gumball | The Stink
 2:15 pm | Amazing World of Gumball | The Understanding
 2:30 pm | Amazing World of Gumball | The Awareness
 2:45 pm | Amazing World of Gumball | The Ad
 3:00 pm | Amazing World of Gumball | The Parents
 3:15 pm | Amazing World of Gumball | The Slip
 3:30 pm | Amazing World of Gumball | The Founder
 3:45 pm | Amazing World of Gumball | The Drama
 4:00 pm | Craig of the Creek | The Great Fossil Rush
 4:15 pm | Craig of the Creek | Sparkle Cadet
 4:30 pm | Craig of the Creek | Alone Quest
 4:45 pm | Craig of the Creek | Stink Bomb
 5:00 pm | Total DramaRama | Pinata Regatta
 5:15 pm | Total DramaRama | Germ Factory
 5:30 pm | Total DramaRama | A Dame-gerous Game
 5:45 pm | Total DramaRama | Aquarium for a Dream
 6:00 pm | Teen Titans Go! | Grube's Fairytales
 6:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:30 pm | Teen Titans Go! | A Farce
 6:45 pm | Teen Titans Go! | Bottle Episode
 7:00 pm | Amazing World of Gumball | The Revolt
 7:15 pm | Amazing World of Gumball | The Inquisition
 7:30 pm | Apple & Onion | 4 on 1
 7:45 pm | Apple & Onion | Fun Proof