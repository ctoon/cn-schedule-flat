# 2021-04-02
 6:00 am | Amazing World of Gumball | The Mustache; The Date
 6:30 am | Amazing World of Gumball | The Club; The Wand
 7:00 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:15 am | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 7:30 am | Amazing World of Gumball | The Quest; The Spoon
 8:00 am | Teen Titans Go! | Super Hero Summer Camp
 9:00 am | Elliott From Earth | Parallel Paradox
 9:15 am | Elliott From Earth | Chaotic Clumping
 9:30 am | Teen Titans GO! to the Movies | 
11:30 am | Teen Titans Go! | Super Robin; Tower Power
12:00 pm | Teen Titans Go! | Parasite; Starliar
12:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 1:00 pm | Amazing World of Gumball | The Hero; The Photo
 1:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 2:00 pm | Amazing World of Gumball | The Limit; The Game
 2:30 pm | Amazing World of Gumball | The Promise; The Voice
 2:45 pm | Amazing World of Gumball | The Revolt
 3:00 pm | Amazing World of Gumball | The Boombox; The Castle
 3:30 pm | Amazing World of Gumball | The Coach
 3:45 pm | Amazing World of Gumball | The Authority
 4:00 pm | Craig of the Creek | Snow Day
 4:15 pm | Craig of the Creek | Snow Place Like Home
 4:30 pm | Craig of the Creek | Winter Break
 4:45 pm | Craig of the Creek | The Sunflower
 5:00 pm | Total DramaRama | Apoca-lice Now
 5:15 pm | Total DramaRama | Gnome More Mister Nice Guy
 5:30 pm | Elliott From Earth | Chaotic Clumping
 5:45 pm | Elliott From Earth | Parallel Paradox
 6:00 pm | Despicable Me | 
 7:45 pm | Teen Titans Go! | Booty Eggs