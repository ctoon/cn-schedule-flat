# 2021-04-30
 8:00 pm | Bob's Burgers | Bye Bye Boo Boo
 8:30 pm | American Dad | Four Little Words
 9:00 pm | American Dad | When Stan Loves a Woman
 9:30 pm | American Dad | I Can't Stan You
10:00 pm | Rick and Morty | Auto Erotic Assimilation
10:30 pm | Family Guy | Peter, Chris, & Brian
11:00 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
11:30 pm | Rick and Morty | The Old Man and the Seat
12:00 am | Rick and Morty: Extras | Rick + Morty In The Eternal Nightmare Machine
12:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 1:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 1:30 am | Rick and Morty | Rattlestar Ricklactica
 2:00 am | Rick and Morty | Never Ricking Morty
 2:30 am | Rick and Morty | Promortyus
 3:00 am | Rick and Morty | The Vat of Acid Episode
 3:30 am | Rick and Morty | Childrick of Mort
 4:00 am | Rick and Morty | Star Mort Rickturn of the Jerri
 4:30 am | The Shivering Truth | The Diff
 4:45 am | JJ Villard's Fairy Tales | Rumpelstiltskin
 5:00 am | Bob's Burgers | Bye Bye Boo Boo
 5:30 am | Home Movies | I Don't Do Well in Parent-Teacher Conferences