# 2021-04-04
 8:00 pm | Bob's Burgers | Eggs for Days
 8:30 pm | American Dad | 100 Years A Solid Fool
 9:00 pm | American Dad | Downtown
 9:30 pm | American Dad | Cheek to Cheek: A Stripper's Story
10:00 pm | Family Guy | Stewie Is Enceinte
10:30 pm | Family Guy | Dr. C & the Women
11:00 pm | Rick and Morty | The Rickshank Rickdemption
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Birdgirl | Pilot
12:30 am | Final Space | The Ventrexian
 1:00 am | Momma Named Me Sheriff | Puddin
 1:15 am | Momma Named Me Sheriff | Membership
 1:30 am | Family Guy | Stewie Is Enceinte
 2:00 am | Family Guy | Dr. C & the Women
 2:30 am | American Dad | 100 Years A Solid Fool
 3:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:30 am | Birdgirl | Pilot
 4:00 am | Final Space | The Ventrexian
 4:30 am | Momma Named Me Sheriff | Puddin
 4:45 am | Momma Named Me Sheriff | Membership
 5:00 am | Bob's Burgers | Eggs for Days
 5:30 am | Home Movies | Honkey Magoo