# 2021-04-10
 6:00 am | Teen Titans Go! | Brain Food; In and Out
 6:30 am | Teen Titans Go! | Little Buddies; Missing
 7:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 7:30 am | Teen Titans Go! | Dreams; Grandma Voice
 8:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
10:00 am | Ben 10 | Ben Gen 10
11:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
12:00 pm | Amazing World of Gumball | The Catfish
12:15 pm | Amazing World of Gumball | The Menu
12:30 pm | Amazing World of Gumball | The Cycle
12:45 pm | Amazing World of Gumball | The Uncle
 1:00 pm | Amazing World of Gumball | The Stars
 1:15 pm | Amazing World of Gumball | The Heist
 1:30 pm | Amazing World of Gumball | The Petals
 1:45 pm | Amazing World of Gumball | The Nuisance
 2:00 pm | Amazing World of Gumball | The Matchmaker
 2:15 pm | Amazing World of Gumball | The Best
 2:30 pm | Amazing World of Gumball | The Grades
 2:45 pm | Amazing World of Gumball | The Worst
 3:00 pm | Victor and Valentino | Cat-Pocalypse
 3:15 pm | Victor and Valentino | Tree Buds
 3:30 pm | Victor and Valentino | Band for Life
 3:45 pm | Victor and Valentino | On Nagual Hill
 4:00 pm | Teen Titans Go! | Pirates; I See You
 4:30 pm | Teen Titans Go! | Brian; Nature
 5:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
 5:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 6:00 pm | Amazing World of Gumball | The Diet
 6:15 pm | Amazing World of Gumball | The List
 6:30 pm | Amazing World of Gumball | The Ex
 6:45 pm | Amazing World of Gumball | The Deal
 7:00 pm | Amazing World of Gumball | The Line
 7:15 pm | Amazing World of Gumball | The Cringe
 7:30 pm | Amazing World of Gumball | The Singing
 7:45 pm | Amazing World of Gumball | The Neighbor