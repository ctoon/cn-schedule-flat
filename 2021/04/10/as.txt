# 2021-04-10
 8:00 pm | American Dad | Funnyish Games
 8:30 pm | American Dad | Fleabiscuit
 9:00 pm | American Dad | The Future Is Borax
 9:30 pm | Rick and Morty | Pilot
10:00 pm | Rick and Morty | Lawnmower Dog
10:30 pm | Final Space | One of Us
11:00 pm | Family Guy | Be Careful What You Fish For
11:30 pm | Family Guy | Burning Down the Bayit
12:00 am | Dragon Ball Super | Goku Enkindled! The Awakened One's New Ultra Instinct!
12:30 am | Attack on Titan | Children of the Forest
 1:00 am | Food Wars! | The Academy Falls
 1:30 am | The Promised Neverland | Episode 1
 2:00 am | Fire Force | Weapon of Destruction
 2:30 am | Black Clover | Rescue
 3:00 am | Naruto:Shippuden | Kabuto Yakushi
 3:30 am | Demon Slayer: Kimetsu no Yaiba | Hashira Meeting
 4:00 am | The Boondocks | The Story of Lando Freeman
 4:30 am | The Boondocks | The Lovely Ebony Brown
 5:00 am | Rick and Morty | Pilot
 5:30 am | Final Space | One of Us