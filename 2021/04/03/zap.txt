# 2021-04-03
 6:00 am | Teen Titans Go! | Burger vs. Burrito; Matched
 6:30 am | Teen Titans Go! | Colors of Raven; The Left Leg
 7:00 am | Teen Titans Go! | Books; Lazy Sunday
 7:30 am | Teen Titans Go! | Girls Night In Part 1
 8:00 am | Teen Titans Go! | What's Opera, Titans?
 8:15 am | Teen Titans Go! | Easter Creeps
 8:30 am | Teen Titans Go! | Booty Eggs
 8:45 am | Teen Titans Go! | Egg Hunt
 9:00 am | Teen Titans Go! | Feed Me
 9:11 am | Teen Titans Go! | Bumgorf
 9:30 am | Total DramaRama | Gumbearable
 9:45 am | Total DramaRama | School District 9
10:00 am | Craig of the Creek | Craig World
10:15 am | Craig of the Creek | The Sunflower
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
11:00 am | Teen Titans Go! | The Teen Titans Go Easter Holiday Classic
11:15 am | Teen Titans Go! | Easter Creeps
11:30 am | Teen Titans Go! | Booty Eggs
11:45 am | Teen Titans Go! | Rain on Your Wedding Day
12:00 pm | Amazing World of Gumball | The Blame
12:15 pm | Amazing World of Gumball | The Stories
12:30 pm | Amazing World of Gumball | The Slap
12:45 pm | Amazing World of Gumball | The Guy
 1:00 pm | Amazing World of Gumball | The Detective
 1:15 pm | Amazing World of Gumball | The Boredom
 1:30 pm | Amazing World of Gumball | The Fury
 1:45 pm | Amazing World of Gumball | The Code
 2:00 pm | Amazing World of Gumball | The Compilation
 2:15 pm | Amazing World of Gumball | The Choices
 2:30 pm | Amazing World of Gumball | The Disaster
 2:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 3:00 pm | Victor and Valentino | Love at First Bite
 3:15 pm | Victor and Valentino | Lonely Haunts Club 2: Doll Island
 3:30 pm | Victor and Valentino | Go With the Flow
 3:45 pm | Victor and Valentino | Guillermo's Gathering
 4:00 pm | Despicable Me | 
 6:00 pm | Madagascar | 
 7:00 pm | Madagascar | 