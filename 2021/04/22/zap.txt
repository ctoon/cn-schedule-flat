# 2021-04-22
 6:00 am | Amazing World of Gumball | The Cringe
 6:15 am | Amazing World of Gumball | The Pact
 6:30 am | Amazing World of Gumball | The Neighbor
 6:45 am | Amazing World of Gumball | The Faith
 7:00 am | Amazing World of Gumball | The Candidate
 7:15 am | Amazing World of Gumball | The Intelligence
 7:30 am | Amazing World of Gumball | The Anybody
 7:45 am | Amazing World of Gumball | The Potion
 8:00 am | Teen Titans Go! | Animals: It's Just a Word!
 8:15 am | Teen Titans Go! | Pyramid Scheme
 8:30 am | Teen Titans Go! | Finally a Lesson
 8:45 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 9:00 am | Teen Titans Go! | Arms Race With Legs
 9:15 am | Teen Titans Go! | History Lesson
 9:30 am | Teen Titans Go! | Obinray
 9:45 am | Teen Titans Go! | The Art of Ninjutsu
10:00 am | Craig of the Creek | Winter Break
10:30 am | Craig of the Creek | Snow Place Like Home
10:45 am | Craig of the Creek | Breaking the Ice
11:00 am | Craig of the Creek | Winter Creeklympics
11:15 am | Craig of the Creek | Welcome to Creek Street
11:30 am | Craig of the Creek | Fan or Foe
11:45 am | Craig of the Creek | New Jersey
12:00 pm | Craig of the Creek | The Sunflower
12:15 pm | Craig of the Creek | Craig World
12:30 pm | Craig of the Creek | The Other Side: The Tournament
 1:00 pm | Craig of the Creek | The Ground Is Lava!
 1:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 1:30 pm | Craig of the Creek | The Bike Thief
 1:45 pm | Craig of the Creek | Craig of the Beach
 2:00 pm | Craig of the Creek | Plush Kingdom
 2:15 pm | Craig of the Creek | The Ice Pop Trio
 2:30 pm | Craig of the Creek | Pencil Break Mania
 2:45 pm | Craig of the Creek | The Last Game of Summer
 3:00 pm | Craig of the Creek | Fall Anthology
 3:15 pm | Craig of the Creek | Afterschool Snackdown
 3:30 pm | Craig of the Creek | Creature Feature
 3:45 pm | Craig of the Creek | King of Camping
 4:00 pm | Craig of the Creek | Winter Break
 4:30 pm | Craig of the Creek | Snow Place Like Home
 4:45 pm | Craig of the Creek | Breaking the Ice
 5:00 pm | Craig of the Creek | Winter Creeklympics
 5:15 pm | Craig of the Creek | Welcome to Creek Street
 5:30 pm | Craig of the Creek | Fan or Foe
 5:45 pm | Craig of the Creek | New Jersey
 6:00 pm | Craig of the Creek | The Sunflower
 6:15 pm | Craig of the Creek | Craig World
 6:30 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 6:45 pm | Craig of the Creek | I Don't Need a Hat
 7:00 pm | Craig of the Creek | Alternate Creekiverse
 7:15 pm | Craig of the Creek | Snow Day
 7:30 pm | Craig of the Creek | The Other Side