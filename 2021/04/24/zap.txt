# 2021-04-24
 6:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 6:30 am | Teen Titans Go! | Booty Scooty
 6:45 am | Teen Titans Go! | Who's Laughing Now
 7:00 am | Teen Titans Go! | Snuggle Time
 7:15 am | Teen Titans Go! | The Overbite
 7:30 am | Teen Titans Go! | Oh Yeah!
 7:45 am | Teen Titans Go! | Rain on Your Wedding Day
 8:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro Part 4
 9:00 am | Teen Titans Go! | Hafo Safo
 9:11 am | Teen Titans Go! | The Mug
 9:30 am | Total DramaRama | Broken Back Kotter
 9:45 am | Total DramaRama | Whack Mirror
10:00 am | Craig of the Creek | Snow Place Like Home
10:15 am | Craig of the Creek | Breaking the Ice
10:30 am | Victor and Valentino | Baby Pepito
10:45 am | Victor and Valentino | Las Pesadillas
11:00 am | Teen Titans Go! | Riding the Dragon
11:15 am | Teen Titans Go! | Booby Trap House
11:30 am | Teen Titans Go! | Fish Water
11:45 am | Teen Titans Go! | Them Soviet Boys
12:00 pm | Amazing World of Gumball | The Mustache; The Date
12:30 pm | Amazing World of Gumball | The Club; The Wand
 1:00 pm | Amazing World of Gumball | The Ape; The Poltergeist
 1:30 pm | Amazing World of Gumball | The Quest; The Spoon
 2:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 2:30 pm | Amazing World of Gumball | The Helmet; The Fight
 3:00 pm | Victor and Valentino | Lords of Ghost Town
 3:15 pm | Victor and Valentino | Decoding Valentino
 3:30 pm | Victor and Valentino | Kindred Spirits
 3:45 pm | Victor and Valentino | Folk Art Friends
 4:00 pm | Teen Titans Go! | TV Knight
 4:15 pm | Teen Titans Go! | Titan Saving Time
 4:30 pm | Teen Titans Go! | BBSFBDAY
 4:45 pm | Teen Titans Go! | Master Detective
 5:00 pm | Teen Titans Go! | Inner Beauty of a Cactus
 5:15 pm | Teen Titans Go! | The Avogodo
 5:30 pm | Teen Titans Go! | Movie Night
 5:45 pm | Teen Titans Go! | What's Opera, Titans?
 6:00 pm | Amazing World of Gumball | The End; The DVD
 6:30 pm | Amazing World of Gumball | The Knights; The Colossus
 7:00 pm | Amazing World of Gumball | The Fridge; The Remote
 7:30 pm | Amazing World of Gumball | The Flower; The Banana