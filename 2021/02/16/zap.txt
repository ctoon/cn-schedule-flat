# 2021-02-16
 6:00 am | Amazing World of Gumball | The Parking
 6:15 am | Amazing World of Gumball | The Misunderstandings
 6:30 am | Amazing World of Gumball | The Upgrade
 6:45 am | Amazing World of Gumball | The Roots
 7:00 am | Amazing World of Gumball | The Comic
 7:15 am | Amazing World of Gumball | The Blame
 7:30 am | Amazing World of Gumball | The Romantic
 7:45 am | Amazing World of Gumball | The Detective
 8:00 am | Teen Titans Go! | Brian; Nature
 8:30 am | Teen Titans Go! | Salty Codgers; Knowledge
 9:00 am | Teen Titans Go! | Love Monsters; Baby Hands
 9:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 9:45 am | Teen Titans Go! | Baby Mouth
10:00 am | Craig of the Creek | Sparkle Cadet
10:15 am | Craig of the Creek | Secret in a Bottle
10:30 am | Craig of the Creek | Stink Bomb
10:45 am | Craig of the Creek | Trading Day
11:00 am | Victor and Valentino | Charlene Mania
11:15 am | Victor and Valentino | Dead Ringer
11:30 am | Teen Titans Go! | Business Ethics Wink Wink
11:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
12:00 pm | Teen Titans Go! | I Used to Be a Peoples
12:15 pm | Teen Titans Go! | The Metric System vs. Freedom
12:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 1:00 pm | Amazing World of Gumball | The Downer; The Egg
 1:30 pm | Amazing World of Gumball | The Triangle; The Money
 2:00 pm | Amazing World of Gumball | The Return
 2:15 pm | Amazing World of Gumball | The Traitor
 2:30 pm | Amazing World of Gumball | The Nemesis
 2:45 pm | Amazing World of Gumball | The Advice
 3:00 pm | Amazing World of Gumball | The Crew
 3:15 pm | Amazing World of Gumball | The Signal
 3:30 pm | Amazing World of Gumball | The Others
 3:45 pm | Amazing World of Gumball | The Girlfriend
 4:00 pm | Craig of the Creek | Sugar Smugglers
 4:15 pm | Craig of the Creek | Ferret Quest
 4:30 pm | Craig of the Creek | Snow Place Like Home
 4:45 pm | Craig of the Creek | Breaking the Ice
 5:00 pm | Total DramaRama | Stingin' in the Rain
 5:15 pm | Total DramaRama | All Up in Your Drill
 5:30 pm | Total DramaRama | Cartoon Realism
 5:45 pm | Total DramaRama | Toys Will Be Toys
 6:00 pm | Teen Titans Go! | Butter Wall
 6:15 pm | Teen Titans Go! | BBRAEBDAY
 6:30 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 7:00 pm | Amazing World of Gumball | The Origins
 7:15 pm | Amazing World of Gumball | The Origins
 7:30 pm | Apple & Onion | Champion
 7:45 pm | Apple & Onion | Slobbery