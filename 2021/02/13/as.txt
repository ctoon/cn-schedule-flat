# 2021-02-13
 8:00 pm | American Dad | Can I Be Frank (With You)
 8:30 pm | American Dad | American Stepdad
 9:00 pm | American Dad | Why Can't We Be Friends
 9:30 pm | Rick and Morty | The Whirly Dirly Conspiracy
10:00 pm | Rick and Morty | Rest and Ricklaxation
10:30 pm | Final Space | The Remembered
11:00 pm | Family Guy | Saving Private Brian
11:30 pm | Family Guy | Whistle While Your Wife Works
12:00 am | Dragon Ball Super | The Power of Love Explodes?! The 2nd Universe's Witchy Warriors!!
12:30 am | Attack on Titan | The War Hammer Titan
 1:00 am | Assassination Classroom | XX Time
 1:30 am | Fire Force | Shadows Cast by Divine Light
 2:00 am | Black Clover | Charmy's Century of Hunger, Gordon's Millennium of Loneliness
 2:30 am | SSSS.Gridman | Provocation
 3:00 am | Naruto:Shippuden | Kurama
 3:30 am | Demon Slayer: Kimetsu no Yaiba | Mount Natagumo
 4:00 am | The Boondocks | It's a Black President, Huey Freeman
 4:30 am | The Boondocks | Bitches to Rags
 5:00 am | Rick and Morty | The Whirly Dirly Conspiracy
 5:30 am | Final Space | The Remembered