# 2021-02-11
 6:00 am | Amazing World of Gumball | The Microwave; The Meddler
 6:30 am | Amazing World of Gumball | The Helmet; The Fight
 7:00 am | Amazing World of Gumball | The End; The DVD
 7:30 am | Amazing World of Gumball | The Knights; The Colossus
 8:00 am | Teen Titans Go! | No Power; Sidekick
 8:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 9:00 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:30 am | Teen Titans Go! | Waffles; Opposites
10:00 am | Craig of the Creek | The Brood
10:15 am | Craig of the Creek | Deep Creek Salvage
10:30 am | Craig of the Creek | Under the Overpass
10:45 am | Craig of the Creek | Dibs Court
11:00 am | Victor and Valentino | Lords of Ghost Town
11:15 am | Victor and Valentino | Villainy In Monte Macabre
11:30 am | Teen Titans Go! | Tower Renovation
11:45 am | Teen Titans Go! | Quantum Fun
12:00 pm | Teen Titans Go! | The Fight
12:15 pm | Teen Titans Go! | Genie President
12:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 1:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:30 pm | Amazing World of Gumball | The Internet; The Plan
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Kids; The Fan
 3:00 pm | Amazing World of Gumball | The Name; The Extras
 3:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 4:00 pm | Craig of the Creek | Jessica Goes to the Creek
 4:15 pm | Craig of the Creek | The Climb
 4:30 pm | Craig of the Creek | Too Many Treasures
 4:45 pm | Craig of the Creek | Big Pinchy
 5:00 pm | Total DramaRama | Wiggin' Out
 5:15 pm | Total DramaRama | Paint That a Shame
 5:30 pm | Total DramaRama | The Upside of Hunger
 5:45 pm | Total DramaRama | Snots Landing
 6:00 pm | Teen Titans Go! | Rocks and Water
 6:15 pm | Teen Titans Go! | I See You
 6:30 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 6:45 pm | Teen Titans Go! | TV Knight 2
 7:00 pm | Amazing World of Gumball | The Quest; The Spoon
 7:30 pm | Amazing World of Gumball | The Pony; The Storm