# 2021-02-05
 6:00 am | Amazing World of Gumball | The Intelligence
 6:15 am | Amazing World of Gumball | The Wish
 6:30 am | Amazing World of Gumball | The Potion
 6:45 am | Amazing World of Gumball | The Revolt
 7:00 am | Amazing World of Gumball | The Spinoffs
 7:15 am | Amazing World of Gumball | The Mess
 7:30 am | Amazing World of Gumball | The Transformation
 7:45 am | Amazing World of Gumball | The Possession
 8:00 am | Teen Titans Go! | Baby Mouth
 8:15 am | Teen Titans Go! | Lucky Stars
 8:30 am | Teen Titans Go! | Huggbees
 8:45 am | Teen Titans Go! | Superhero Feud
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 9:30 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
10:00 am | Craig of the Creek | Snow Day
10:15 am | Craig of the Creek | Jessica Shorts
10:30 am | Craig of the Creek | Winter Break
11:00 am | Victor and Valentino | My Fair Achi
11:15 am | Victor and Valentino | Starry Night
11:30 am | Teen Titans Go! | Night Begins to Shine 2: You're the One
12:45 pm | Teen Titans Go! | Starfire the Terrible
 1:00 pm | Amazing World of Gumball | The Robot; The Picnic
 1:30 pm | Amazing World of Gumball | The Goons; The Secret
 2:00 pm | Amazing World of Gumball | The Sock; The Genius
 2:30 pm | Amazing World of Gumball | The Mustache; The Date
 3:00 pm | Amazing World of Gumball | The Club; The Wand
 3:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 4:00 pm | Craig of the Creek | Craig of the Beach
 4:15 pm | Craig of the Creek | Sleepover at JP's
 4:30 pm | Craig of the Creek | Plush Kingdom
 4:45 pm | Craig of the Creek | The Evolution of Craig
 5:00 pm | Total DramaRama | A Dame-gerous Game
 5:15 pm | Total DramaRama | That's a Wrap
 5:30 pm | Total DramaRama | Royal Flush
 5:45 pm | Total DramaRama | Tiger Fail
 6:00 pm | Teen Titans Go! | TV Knight 5
 6:15 pm | Teen Titans Go! | Walk Away
 6:30 pm | Teen Titans Go! | Record Book
 6:45 pm | Teen Titans Go! | Bat Scouts
 7:00 pm | Amazing World of Gumball | The Founder
 7:15 pm | Amazing World of Gumball | The Silence
 7:30 pm | Amazing World of Gumball | The Schooling
 7:45 pm | Amazing World of Gumball | The Future