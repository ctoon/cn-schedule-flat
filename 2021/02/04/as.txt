# 2021-02-04
 8:00 pm | Bob's Burgers | Like Gene for Chocolate
 8:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 9:00 pm | American Dad | Virtual In-Stanity
 9:30 pm | American Dad | The Scarlett Getter
10:00 pm | Rick and Morty | The Ricks Must Be Crazy
10:30 pm | Rick and Morty | Big Trouble in Little Sanchez
11:00 pm | Family Guy | Deep Throats
11:30 pm | Family Guy | Peterotica
12:00 am | Mr. Pickles | The Tree of Flesh
12:15 am | Momma Named Me Sheriff | Hats
12:30 am | Eric Andre Show | T.I.; Abbey Lee Miller
12:45 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 1:00 am | Aqua Teen | Dickesode
 1:15 am | Aqua Teen | Hand Banana
 1:30 am | Family Guy | Deep Throats
 2:00 am | Family Guy | Peterotica
 2:30 am | American Dad | Virtual In-Stanity
 3:00 am | Rick and Morty | The Ricks Must Be Crazy
 3:30 am | Mr. Pickles | The Tree of Flesh
 3:45 am | Momma Named Me Sheriff | Hats
 4:00 am | Off The Air | Dreams
 4:15 am | Wet City | Adult Swim Smalls Presents: Wet City
 4:30 am | Eric Andre Show | T.I.; Abbey Lee Miller
 4:45 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 5:00 am | Bob's Burgers | Like Gene for Chocolate
 5:30 am | Bob's Burgers | The Grand Mama-Pest Hotel