# 2021-11-30
 6:00 am | Baby Looney Tunes | Who Said That?; Paws and Feathers; Let Them Make Cake
 6:30 am | Baby Looney Tunes | Mind Your Manners; Down by the Cage; Petunia the Piggy Bank
 7:00 am | Caillou | Caillou's Gym Day; Our Rocket Ship; Caillou Helps Grandpa; Backyard Bowlers; Caillou's Milk Run
 7:30 am | Caillou | Dancing at Grandma's; Mystery Valentine; Play Ball!; Dogsled Ahead!
 8:00 am | Pocoyo | Twinkle Twinkle
 8:07 am | Pocoyo | Hiccup
 8:14 am | Pocoyo | Pato's Postal Service
 8:21 am | Pocoyo | Puppy Love
 8:30 am | Pocoyo | Bat and Ball
 8:40 am | Pocoyo | Elly Spots
 8:50 am | Pocoyo | Up, up and Away
 9:00 am | Thomas & Friends: All Engines Go | Dragon Run
 9:15 am | Thomas & Friends: All Engines Go | A Wide Delivery
 9:30 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:45 am | Thomas & Friends: All Engines Go | Kana Goes Slow
10:00 am | Bing | Giving
10:10 am | Bing | Lost
10:20 am | Bing | Swing
10:30 am | Bing | Knack
10:40 am | Bing | Growing
10:50 am | Bing | Say Goodbye
11:00 am | Pocoyo | Twinkle Twinkle
11:10 am | Pocoyo | Hiccup
11:20 am | Pocoyo | Pato's Postal Service
11:30 am | Pocoyo | Bat and Ball
11:40 am | Pocoyo | Elly Spots
11:50 am | Pocoyo | Up, up and Away
12:00 pm | Mush-Mush and the Mushables | Oh My Compost
12:15 pm | Mush-Mush and the Mushables | Fly, Chase, Fly
12:30 pm | Mush-Mush and the Mushables | Mushlers to the Rescue
12:45 pm | Mush-Mush and the Mushables | Fool the Forest
 1:00 pm | Craig of the Creek | New Jersey
 1:15 pm | Craig of the Creek | The Sunflower
 1:30 pm | Craig of the Creek | Craig World
 1:45 pm | Craig of the Creek | Body Swap
 2:00 pm | Tim Burton's Corpse Bride | 
 4:00 pm | Craig of the Creek | Stink Bomb
 4:15 pm | Craig of the Creek | Jessica Shorts
 4:30 pm | Craig of the Creek | Summer Wish
 4:45 pm | Craig of the Creek | Ferret Quest
 5:00 pm | Craig of the Creek | Turning the Tables
 5:15 pm | Craig of the Creek | Into the Overpast
 5:30 pm | Teen Titans Go! | TV Knight 5
 5:45 pm | Teen Titans Go! | Christmas Crusaders
 6:00 pm | Teen Titans Go! | Beast Boy on a Shelf
 6:15 pm | Teen Titans Go! | Walk Away
 6:30 pm | Teen Titans Go! | The True Meaning of Christmas
 6:45 pm | Teen Titans Go! | Halloween vs. Christmas
 7:00 pm | Amazing World of Gumball | The Traitor
 7:15 pm | Amazing World of Gumball | The Parasite
 7:30 pm | Amazing World of Gumball | The Advice
 7:45 pm | Amazing World of Gumball | The Love
 8:00 pm | Amazing World of Gumball | The Signal
 8:15 pm | Amazing World of Gumball | The Awkwardness
 8:30 pm | Amazing World of Gumball | The Girlfriend
 8:45 pm | Amazing World of Gumball | The Nest