# 2021-11-01
 6:00 am | Baby Looney Tunes | Melissa the Hero; My Bunny Lies Over the Ocean; The Trouble With Larry
 6:30 am | Baby Looney Tunes | Taz In Toyland; Born To Sing (song); A Secret Tweet
 7:00 am | Caillou | Elephants!; Caillou and the Sheep; Caillou and the Puppies; The Duck Family; Clementine's New Pet
 7:30 am | Caillou | Disappearing Carrots; Caillou Marches On; Who's Mooing?; Follow That Sound; Music Box
 7:45 am | Caillou | 
 8:00 am | Pocoyo | 
 8:07 am | Pocoyo | My Pato!
 8:14 am | Pocoyo | Pocoyo's Balloon
 8:21 am | Pocoyo | Who's Calling Me Now?
 8:30 am | Pocoyo | Big Scary Slide
 8:40 am | Pocoyo | 
 8:50 am | Pocoyo | Duck Stuck
 9:00 am | Thomas & Friends: All Engines Go | Super Screen Cleaners
 9:15 am | Thomas & Friends: All Engines Go | Mystery Boxcars
 9:30 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell
 9:45 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
10:00 am | Bing | Storytime
10:10 am | Bing | Hearts
10:20 am | Bing | Fireworks
10:30 am | Bing | Something for Sula
10:40 am | Bing | Growing
10:50 am | Bing | Blocks
11:00 am | Pocoyo | 
11:07 am | Pocoyo | My Pato!
11:14 am | Pocoyo | Who's Calling Me Now?
11:21 am | Pocoyo | 
11:30 am | Pocoyo | Big Scary Slide
11:40 am | Pocoyo | 
11:50 am | Pocoyo | Duck Stuck
12:00 pm | Caillou | Disappearing Carrots; Caillou Marches On; Who's Mooing?; Follow That Sound; Music Box
12:30 pm | Caillou | Caillou Makes Cookies; Caillou's Not Afraid Anymore; Caillou Hates Vegetables; Caillou's All Alone; Caillou Tidies His Toys
 1:00 pm | Mush-Mush and the Mushables | The Breathtaking Bolly Show
 1:15 pm | Mush-Mush and the Mushables | Compost to Go
 1:30 pm | Mush-Mush and the Mushables | Mushpot's Treasure
 1:45 pm | Mush-Mush and the Mushables | Save the Fun Tree
 2:00 pm | Amazing World of Gumball | The Tag; The Lesson
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Promise; The Voice
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | Craig of the Creek | Summer Wish
 4:15 pm | Craig of the Creek | The Evolution of Craig
 4:30 pm | Craig of the Creek | Turning the Tables
 4:45 pm | Craig of the Creek | Kelsey the Author
 5:00 pm | Craig of the Creek | Craig and the Kid's Table
 5:30 pm | Teen Titans Go! | Hand Zombie
 5:45 pm | Teen Titans Go! | The Avogodo
 6:00 pm | Teen Titans Go! | Orangins
 6:15 pm | Teen Titans Go! | Labor Day
 6:30 pm | Teen Titans Go! | DC
 6:45 pm | Teen Titans Go! | Jam
 7:00 pm | Victor and Valentino | Finding Posada
 7:15 pm | Victor and Valentino | La Catrina
 7:30 pm | Victor and Valentino | Through the Nine Realms of Mictlan
 7:30 pm | Victor and Valentino | Through the Nine Realms of Mictlan
 8:30 pm | Victor and Valentino | Finding Posada
 8:45 pm | Victor and Valentino | La Catrina