# 2021-11-09
 6:00 am | Baby Looney Tunes | Spin Out/Taz's Fridge /Snow Day
 6:30 am | Baby Looney Tunes | Shadow Of A Doubt; John Jacob Jongle Elmer Fudd (song); Christmas In July
 7:00 am | Caillou | Caillou Goes to the Car Wash; Far Away From Home; All You Can Eat; Caillou Stays Up Late
 7:30 am | Caillou | Backyard Zoo; Caillou's Scavenger Hunt; Too Many Cooks; The Berry Patch
 8:00 am | Pocoyo | Magical Watering Can
 8:07 am | Pocoyo | Table for Fun
 8:14 am | Pocoyo | Twinkle Twinkle
 8:21 am | Pocoyo | Hiccup
 8:30 am | Pocoyo | Pato's Postal Service
 8:40 am | Pocoyo | Puppy Love
 8:50 am | Pocoyo | Bat and Ball
 9:00 am | Thomas & Friends: All Engines Go | Chasing Rainbows
 9:15 am | Thomas & Friends: All Engines Go | Counting Cows
 9:30 am | Thomas & Friends: All Engines Go | A Quiet Delivery
 9:45 am | Thomas & Friends: All Engines Go | The Biggest Adventure Club
10:00 am | Bing | Fireworks
10:10 am | Bing | Lost
10:20 am | Bing | Say Goodbye
10:30 am | Bing | Frog
10:40 am | Bing | Growing
10:50 am | Bing | Knack
11:00 am | Pocoyo | Magical Watering Can
11:10 am | Pocoyo | Table for Fun
11:20 am | Pocoyo | Hiccup
11:30 am | Pocoyo | Pato's Postal Service
11:40 am | Pocoyo | Puppy Love
11:50 am | Pocoyo | Bat and Ball
12:00 pm | Caillou | Caillou Goes to the Car Wash; Far Away From Home; All You Can Eat; Caillou Stays Up Late
12:30 pm | Caillou | Caillou's Big Slide; Caillou Walks a Dog; Caillou Watches Rosie; Caillou Is Sick; Caillou's Phone Call
 1:00 pm | Mush-Mush and the Mushables | Fly, Chase, Fly
 1:15 pm | Mush-Mush and the Mushables | Eyes on the Surprise
 1:30 pm | Mush-Mush and the Mushables | Snails Forever
 1:45 pm | Mush-Mush and the Mushables | Oh My Compost
 2:00 pm | LEGO Batman: The Movie -- DC Superheroes Unite | 
 2:40 pm | LEGO Batman: The Movie -- DC Superheroes Unite | 
 3:20 pm | LEGO Batman: The Movie -- DC Superheroes Unite | 
 4:00 pm | Craig of the Creek | The Bike Thief
 4:15 pm | Craig of the Creek | Afterschool Snackdown
 4:30 pm | Craig of the Creek | Craig of the Beach
 4:45 pm | Craig of the Creek | Creature Feature
 5:00 pm | Craig of the Creek | Plush Kingdom
 5:15 pm | Craig of the Creek | King of Camping
 5:30 pm | Teen Titans Go! | Chicken in the Cradle
 5:45 pm | Teen Titans Go! | Quantum Fun
 6:00 pm | Teen Titans Go! | Kabooms
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 pm | Victor and Valentino | Finding Posada
 7:15 pm | Victor and Valentino | La Catrina
 7:30 pm | Victor and Valentino | In Your Own Skin
 7:45 pm | Victor and Valentino | My Thirsty Little Monster
 8:00 pm | Victor and Valentino | Baby Pepito
 8:15 pm | Victor and Valentino | Las Pesadillas
 8:30 pm | Victor and Valentino | Ghosted
 8:45 pm | Victor and Valentino | The Fog