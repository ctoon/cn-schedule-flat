# 2021-11-28
 9:00 pm | King Of the Hill | Tankin' it to the Streets
 9:30 pm | King Of the Hill | Of Mice and Little Green Men
10:00 pm | American Dad | Mused and Abused
10:30 pm | American Dad | Henderson
11:00 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
11:30 pm | Rick and Morty | Rickternal Friendshine of the Spotless Mort
12:00 am | Squidbillies | One Man Banned
12:15 am | Squidbillies | Let 'er R.I.P.
12:30 am | Joe Pera Helps Find You The Perfect Christmas Tree | Joe Pera Helps You Find the Perfect Christmas Tree
 1:00 am | Teenage Euthanasia | Suddenly Susan
 1:30 am | American Dad | Mused and Abused
 2:00 am | American Dad | Henderson
 2:30 am | Squidbillies | One Man Banned
 2:45 am | Squidbillies | Let 'er R.I.P.
 3:00 am | Joe Pera Helps Find You The Perfect Christmas Tree | Joe Pera Helps You Find the Perfect Christmas Tree
 3:30 am | Rick and Morty | Gotron Jerrysis Rickvangelion
 4:00 am | King Of the Hill | Tankin' it to the Streets
 4:30 am | King Of the Hill | Of Mice and Little Green Men
 5:00 am | King Of the Hill | A Man Without a Country Club
 5:30 am | King Of the Hill | Beer and Loathing