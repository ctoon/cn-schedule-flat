# 2021-11-06
 6:00 am | Baby Looney Tunes | The Brave Little Tweety; Foghorn's Talkin' In The Barnyard (song); Puddle Olympics
 6:30 am | Baby Looney Tunes | Melissa The Hero; My Bunny Lies Over The Ocean (song); The Trouble With Larry
 7:00 am | Lucas the Spider | The I'm Taking You in Too Hot to Handle Big Squeak
 7:30 am | Esme & Roy | Sing Out Roy; Monster's Little Helper
 8:00 am | Amazing World of Gumball | The Car; The Curse
 8:30 am | Amazing World of Gumball | The Microwave; The Meddler
 9:00 am | Teen Titans Go! | Breakfast
 9:15 am | Teen Titans Go! | Dc
 9:30 am | Jellystone! | A Fish Sticky Situation
 9:45 am | Jellystone! | A Town Video: Welcome to Jellystone
10:00 am | Total DramaRama | Mad Math: Taffy Road
10:15 am | Total DramaRama | The Big Bangs Theory
10:30 am | Victor and Valentino | Finding Posada; La Catrina
11:00 am | Amazing World of Gumball | The Helmet; The Fight
11:30 am | Amazing World of Gumball | The End; The DVD
12:00 pm | Amazing World of Gumball | The Knights; The Colossus
12:30 pm | Amazing World of Gumball | The Fridge; The Remote
 1:00 pm | Craig of the Creek | Crisis at Elder Rock; Tea Timer's Ball
 1:30 pm | Craig of the Creek | The Kelsey the Worthy Cardboard Identity
 2:00 pm | Craig of the Creek | End Was Here; The Ancients of the Creek
 2:30 pm | Craig of the Creek | Mortimor to the Rescue; The Children of the Dog
 3:00 pm | Teen Titans Go! | TV Knight 5; Walk Away
 3:30 pm | Teen Titans Go! | Beast Boy On A Shelf; Christmas Crusaders
 4:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 5:00 pm | Teen Titans Go! | Record Book; Bat Scouts
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 6:00 pm | Amazing World of Gumball | The Flower; The Banana
 6:30 pm | Amazing World of Gumball | The Phone; The Job
 7:00 pm | Amazing World of Gumball | The Words; The Apology
 7:30 pm | Amazing World of Gumball | The Skull; Christmas
 8:00 pm | Amazing World of Gumball | The Watch; The Bet
 8:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers; Adventure Academy: Sluggernaut