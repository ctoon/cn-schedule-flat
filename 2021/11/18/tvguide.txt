# 2021-11-18
 6:00 am | Baby Looney Tunes | Tea & Basketball; Down By The Cage (song); Taz You Like It
 6:30 am | Baby Looney Tunes | Band Together; Oh Where, Oh Where Has My Baby Martian Gone (song); War Of The Weirds
 7:00 am | Caillou | Caillou Tries Karate; Just Like Daddy; Caillou's Safari; Cowboy Caillou
 7:30 am | Caillou | Learns to Drivebig Friendcolorsmails a Letterlearns to Swim
 8:00 am | Pocoyo | Elly's Ballet Class; Pocoyo's Balloon; Who's' Calling Me Now? ; Big Scary Slide
 8:30 am | Pocoyo | Elly's Shoes; Duck Stuck; Scary Noises
 9:00 am | Thomas & Friends: All Engines Go | Thomas' Promise; Nia's Balloon Blunder
 9:30 am | Thomas & Friends: All Engines Go | The Kana Goes Slow; Biggest Adventure Club
10:00 am | Bing | Smoothie; Hearts; Dressing Up
10:30 am | Bing | Growing; Here I Go; Knack; Train
11:00 am | Pocoyo | Elly's Ballet Class Pocoyo's Balloon #212 Who's Calling Me Now?
11:30 am | Pocoyo | Elly's Shoes; Duck Stuck; Scary Noises
12:00 pm | Caillou | Caillou Tries Karate; Just Like Daddy; Caillou's Safari; Cowboy Caillou
12:30 pm | Caillou | Learns to Drivebig Friendcolorsmails a Letterlearns to Swim
 1:00 pm | Mush Mush and the Mushables | Mush-Mush the Predictor; Fool the Forest
 1:30 pm | Mush Mush and the Mushables | Stuck in the Mud; Get the Egg Home
 2:00 pm | Amazing World of Gumball | The Grades; The Worst
 2:30 pm | Amazing World of Gumball | The Diet; The List
 3:00 pm | Amazing World of Gumball | The Ex; The Deal
 3:30 pm | Amazing World of Gumball | The Weirdo; The; Line
 4:00 pm | Craig of the Creek | Too Many Treasures; Lost in the Sewer
 4:30 pm | Craig of the Creek | The Final Book; The Brood
 5:00 pm | Craig of the Creek | Wildernessa; Under the Overpass
 5:30 pm | Teen Titans Go! | The Viewers Decide; The Great Disaster
 6:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 7:00 pm | Apple & Onion | For Queen and Country; Petri
 7:30 pm | Apple & Onion | Lowlifes; Falafel's Car Keys
 8:00 pm | Apple & Onion | Block Party; Za
 8:30 pm | Apple & Onion | Apple's Focus; Broccoli