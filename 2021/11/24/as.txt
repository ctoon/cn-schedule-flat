# 2021-11-24
 6:00 pm | MOVIE | Fred Claus
 9:00 pm | King Of the Hill | Spin the Choice
 9:30 pm | King Of the Hill | Goodbye Normal Jeans
10:00 pm | Bob's Burgers | The Quirk-Ducers
10:30 pm | Bob's Burgers | Thanks-hoarding
11:00 pm | American Dad | There Will Be Bad Blood
11:30 pm | American Dad | Kung Pao Turkey
12:00 am | Rick and Morty | Mort Dinner Rick Andre
12:30 am | Rick and Morty | Mortyplicity
 1:00 am | Aqua Teen | The Clowning
 1:15 am | Aqua Teen | The The
 1:30 am | The Venture Brothers | Hostile Makeover
 2:00 am | American Dad | There Will Be Bad Blood
 2:30 am | Rick and Morty | Mort Dinner Rick Andre
 3:00 am | Rick and Morty | Mortyplicity
 3:30 am | Aqua Teen | The Clowning
 3:45 am | Aqua Teen | The The
 4:00 am | Mr. Pickles | Serial Killers
 4:15 am | Tigtone | Tigtone and the Screaming City
 4:30 am | King Of the Hill | Spin the Choice
 5:00 am | Bob's Burgers | The Quirk-Ducers
 5:30 am | Bob's Burgers | Thanks-hoarding