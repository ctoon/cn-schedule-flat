# 2021-11-08
 6:00 am | Baby Looney Tunes | Present Tense; The Hare Hid Under the Fountain; The Neat and the Sloppy
 6:30 am | Baby Looney Tunes | Cool For Cats; Ten Loonies In A Bed (song); Time Out!
 7:00 am | Caillou | Caillous Sleepover Guest, Games in the Park, The Sugar Shack, Winter Mystery, Caillou's Snow Day
 7:30 am | Caillou | Dancing at Grandma's, Mystery Valentine, Play Ball!, Dogsled Ahead!
 8:00 am | Pocoyo | Seed; The; Dirty Dog; Where's Pocoyo? ; Drummer Boy
 8:30 am | Pocoyo | Great Race; The Don't Touch! #120 Mystery Footprints
 9:00 am | Thomas & Friends: All Engines Go | Overnight Stop; Super Screen Cleaners
 9:30 am | Thomas & Friends: All Engines Go | Kana Goes Slow; Backwards Day
10:00 am | Bing | Giving; Storytime; Swing
10:30 am | Bing | Smoothie; Atchoo; Voo Voo; Here I Go
11:00 am | Pocoyo | Seed; The; Dirty Dog; Drummer Boy
11:30 am | Pocoyo | Great Race; The Don't Touch! #120 Mystery Footprints
12:00 pm | Caillou | Caillous Sleepover Guest, Games in the Park, The Sugar Shack, Winter Mystery, Caillou's Snow Day
12:30 pm | Caillou | Dancing at Grandma's, Mystery Valentine, Play Ball!, Dogsled Ahead!
 1:00 pm | Mush Mush and the Mushables | Disappearing Mushler; The; Work Like a Charm
 1:30 pm | Mush Mush and the Mushables | Shine; Lilit; Shine; Get the Egg Home
 2:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 2:30 pm | Amazing World of Gumball | The Egg; The Downer
 3:00 pm | Amazing World of Gumball | The Money; The Triangle
 3:30 pm | Amazing World of Gumball | The Pest; The Return
 4:00 pm | Craig of the Creek | The Other Side: The Tournament
 4:30 pm | Craig of the Creek | The Jinxening; The; Ground Is Lava!
 5:00 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind Fall Anthology
 5:30 pm | Teen Titans Go! | Mo' Money Mo' Problems; My Name Is Jose
 6:00 pm | Teen Titans Go! | The TV Knight 3; Power of Shrimps
 6:30 pm | Teen Titans Go! | Breakfast; Dc
 7:00 pm | Amazing World of Gumball | The Nemesis; The Hug
 7:30 pm | Amazing World of Gumball | The Crew; The Sale
 8:00 pm | Amazing World of Gumball | The Routine; The Others
 8:30 pm | Amazing World of Gumball | The Signature; The Parking