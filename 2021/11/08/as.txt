# 2021-11-08
 7:00 pm | MOVIE | Lego: Batman the Movie: DC Super Heroes Unite / Batman's Birthday Gift / Breakfast
 9:00 pm | Bob's Burgers | The Kids Run the Restaurant
 9:30 pm | Bob's Burgers | Boyz 4 Now
10:00 pm | American Dad | She Swill Survive
10:30 pm | American Dad | Rubberneckers
11:00 pm | American Dad | Permanent Record Wrecker
11:30 pm | Rick and Morty | Morty's Mind Blowers
12:00 am | Squidbillies | One Man Banned
12:15 am | Squidbillies | Let 'er R.I.P.
12:30 am | Aqua Teen | Super Bowl
12:45 am | Aqua Teen | Super Computer
 1:00 am | The Venture Brothers | Assisted Suicide
 1:30 am | Rick and Morty | The Ricklantis Mixup
 2:00 am | Rick and Morty | Morty's Mind Blowers
 2:30 am | Squidbillies | One Man Banned
 2:45 am | Squidbillies | Let 'er R.I.P.
 3:00 am | Aqua Teen | Super Bowl
 3:15 am | Aqua Teen | Super Computer
 3:30 am | The Venture Brothers | Assisted Suicide
 4:00 am | Off The Air | Moon
 4:15 am | Off The Air | Space
 4:30 am | Home Movies | Curses
 5:00 am | Bob's Burgers | The Kids Run the Restaurant
 5:30 am | Bob's Burgers | Boyz 4 Now