# 2021-11-14
 6:00 am | Baby Looney Tunes | Taz in Toyland; Born to Sing (Song #7); A Secret Tweet
 6:30 am | Baby Looney Tunes | Tell-a-photo; Born To Sing (song); Move It!
 7:00 am | Love Monster | Picnic Night; Three Legged Race Day; Show and Tell Day
 7:30 am | Care Bears: Unlock The Magic | Festival Of Hearts Almost Eggless Easter
 8:00 am | Teen Titans Go! | Captain Cool; Breakfast
 8:30 am | Total DramaRama | Bearly Edible; Mad Math: Taffy Road
 9:00 am | Victor and Valentino | Bone Bike; Finding Posada
 9:30 am | Apple & Onion | For Queen and Country; Petri
10:00 am | Apple & Onion | Lowlifes; Falafel's Car Keys
10:30 am | Craig of the Creek | Bike Thief; The; Fall Anthology
11:00 am | Amazing World of Gumball | The Tag; The Lesson
11:30 am | Amazing World of Gumball | The Limit; The Game
12:00 pm | Amazing World of Gumball | The Promise; The Voice
12:30 pm | Amazing World of Gumball | The Boombox; The Castle
 1:00 pm | Craig of the Creek | Craig of the Beach Afterschool Snackdown
 1:30 pm | Craig of the Creek | Plush Kingdom Creature Feature
 2:00 pm | Craig of the Creek | The Ice Pop Trio; King of Camping
 2:30 pm | Craig of the Creek | Pencil Break Mania; The Rise and Fall of the Green Poncho
 3:00 pm | Teen Titans Go! | Space House
 4:00 pm | Teen Titans Go! | We're off to Get Awards Rain on Your Wedding Day
 4:30 pm | Teen Titans Go! | Egg Hunt; Bucket List
 5:00 pm | Amazing World of Gumball | The Spoon; The Limit
 5:30 pm | Amazing World of Gumball | The Fuss; The Authority
 6:00 pm | Spider-Man 2 | 