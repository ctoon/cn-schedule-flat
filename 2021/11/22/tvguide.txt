# 2021-11-22
 6:00 am | Baby Looney Tunes | Blast Off Bugs; Baby Brouhaha
 6:30 am | Baby Looney Tunes | Oh Brother, Warehouse Art Thou?; Flu the Coop
 7:00 am | Caillou | Where I Live; A Good Friend; Just Like Me; Just in Time; Mr. Fixit
 7:30 am | Caillou | Something for Everyone; Skating Lessons; Caillou Becomes a Waiter; Sticking to It!; New Clothes
 8:00 am | Pocoyo | Magic Act; Everyone's Present; Boo! ; Party P.
 8:30 am | Pocoyo | My Pato; Baby Bird Bother; Dirty Dog
 9:00 am | Thomas & Friends: All Engines Go | The Joke is on Thomas/Lost and Found
 9:30 am | Thomas & Friends: All Engines Go | Thomas Blasts off; Chasing Rainbows
10:00 am | Bing | Big Boots; Hula Hoop; Knack
10:30 am | Bing | Growing; Musical Statues; Fireworks; Hide & Seek
11:00 am | Pocoyo | Magic Act; Everyone's Present; Party P.
11:30 am | Pocoyo | My Pato; Baby Bird Bother; Dirty Dog
12:00 pm | Caillou | Where I Live; A Good Friend; Just Like Me; Just in Time; Mr. Fixit
12:30 pm | Caillou | Something for Everyone; Skating Lessons; Caillou Becomes a Waiter; Sticking to It!; New Clothes
 1:00 pm | Mush Mush and the Mushables | The Morel in the Dark; Rollinator
 1:30 pm | Mush Mush and the Mushables | Fly; Chase; Fly; Mush-Mush the Predictor
 2:00 pm | Amazing World of Gumball | The Cage; The Anybody
 2:30 pm | Amazing World of Gumball | The Shippening; The Rival
 3:00 pm | Amazing World of Gumball | The One; The Brain
 3:30 pm | Amazing World of Gumball | The Vegging; The Stink
 4:00 pm | Craig of the Creek | The Curse; Jpony
 4:30 pm | Craig of the Creek | Future Is Cardboard; The; Ace of Squares
 5:00 pm | Craig Of The Creek | Craig And The Kid's Table
 5:30 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 6:00 pm | Charlie and the Chocolate Factory | 
 8:30 pm | Amazing World of Gumball | Christmas; The Lie