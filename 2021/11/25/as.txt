# 2021-11-25
 9:00 pm | King Of the Hill | Bobby Goes Nuts
 9:30 pm | King Of the Hill | Returning Japanese (1)
10:00 pm | King Of the Hill | Returning Japanese (2)
10:30 pm | King Of the Hill | Get Your Freak Off
11:00 pm | King Of the Hill | Vision Quest
11:30 pm | King Of the Hill | Livin' on Reds, Vitamin C and Propane
12:00 am | King Of the Hill | Rich Hank, Poor Hank
12:30 am | King Of the Hill | Mutual of OmAbwah
 1:00 am | King Of the Hill | Hank's On Board
 1:30 am | King Of the Hill | Bobby Goes Nuts
 2:00 am | King Of the Hill | Returning Japanese (1)
 2:30 am | King Of the Hill | Returning Japanese (2)
 3:00 am | King Of the Hill | Get Your Freak Off
 3:30 am | King Of the Hill | Vision Quest
 4:00 am | King Of the Hill | Livin' on Reds, Vitamin C and Propane
 4:30 am | King Of the Hill | Rich Hank, Poor Hank
 5:00 am | King Of the Hill | Mutual of OmAbwah
 5:30 am | King Of the Hill | Hank's On Board