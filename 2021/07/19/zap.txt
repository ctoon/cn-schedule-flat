# 2021-07-19
 6:00 am | Amazing World of Gumball | The Tape; The Sweaters
 6:30 am | Amazing World of Gumball | The Internet; The Plan
 7:00 am | Amazing World of Gumball | The World; The Finale
 7:30 am | Amazing World of Gumball | The Kids; The Fan
 8:00 am | Teen Titans Go! | Lucky Stars
 8:15 am | Teen Titans Go! | Various Modes of Transportation
 8:30 am | Teen Titans Go! | Cool Uncles
 8:45 am | Teen Titans Go! | Butter Wall
 9:00 am | Teen Titans Go! | Pig in a Poke
 9:15 am | Teen Titans Go! | P.P.
 9:30 am | Teen Titans Go! | A Little Help Please
 9:45 am | Teen Titans Go! | Booty Scooty
10:00 am | Craig of the Creek | I Don't Need a Hat
10:15 am | Craig of the Creek | New Jersey
10:30 am | Craig of the Creek | Capture the Flag Part 1: The Candy
11:15 am | Craig of the Creek | Capture the Flag Part 5: The Game
12:00 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Amazing World of Gumball | The Mothers; The Password
 1:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:00 pm | Amazing World of Gumball | The Mirror; The Burden
 2:30 pm | Amazing World of Gumball | The Bros; The Man
 3:00 pm | Craig of the Creek | Itch to Explore
 3:15 pm | Craig of the Creek | Bring Out Your Beast
 3:30 pm | Craig of the Creek | You're It
 3:45 pm | Craig of the Creek | Lost in the Sewer
 4:00 pm | Craig of the Creek | Jessica Goes to the Creek
 4:15 pm | Craig of the Creek | The Future Is Cardboard
 4:30 pm | Craig of the Creek | The Final Book
 4:45 pm | Craig of the Creek | The Brood
 5:00 pm | Total DramaRama | A Hole Lot of Trouble
 5:15 pm | Total DramaRama | Dial B for Birder
 5:30 pm | Total DramaRama | Breaking Bite
 5:45 pm | Total DramaRama | I Dream of Meanie
 6:00 pm | The LEGO Movie 2 | 