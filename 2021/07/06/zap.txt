# 2021-07-06
 6:00 am | Amazing World of Gumball | The Line
 6:15 am | Amazing World of Gumball | The Candidate
 6:30 am | Amazing World of Gumball | The Singing
 6:45 am | Amazing World of Gumball | The Anybody
 7:00 am | Amazing World of Gumball | The Puppets
 7:15 am | Amazing World of Gumball | The Shippening
 7:30 am | Amazing World of Gumball | The Lady
 7:45 am | Amazing World of Gumball | The Brain
 8:00 am | Teen Titans Go! | Super Robin; Tower Power
 8:30 am | Teen Titans Go! | Parasite; Starliar
 9:00 am | Looney Tunes Cartoons | Boo! Appetweet; Hole Gag: Plunger; Bubble Dum
 9:15 am | Looney Tunes Cartoons | Pain in the Ice; Tunnel Vision; Pool Bunny
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
10:00 am | Craig of the Creek | Doorway to Helen
10:15 am | Craig of the Creek | Dinner at the Creek
10:30 am | Craig of the Creek | The Last Kid in the Creek
10:45 am | Craig of the Creek | Jessica's Trail
11:00 am | Craig of the Creek | The Climb
11:15 am | Craig of the Creek | Bug City
11:30 am | Craig of the Creek | Big Pinchy
11:45 am | Craig of the Creek | Deep Creek Salvage
12:00 pm | Teen Titans Go! | Space House
12:15 pm | Teen Titans Go! | Hafo Safo
12:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton
12:45 pm | Teen Titans Go! | Zimdings
 1:00 pm | Amazing World of Gumball | The Transformation
 1:15 pm | Amazing World of Gumball | The Ghouls
 1:30 pm | Amazing World of Gumball | The Understanding
 1:45 pm | Amazing World of Gumball | The BFFS
 2:00 pm | Amazing World of Gumball | The Ad
 2:15 pm | Amazing World of Gumball | The Agent
 2:30 pm | Amazing World of Gumball | The Slip
 2:45 pm | Amazing World of Gumball | The Nest
 3:00 pm | Craig of the Creek | Alone Quest
 3:15 pm | Craig of the Creek | Sparkle Cadet
 3:30 pm | Craig of the Creek | Memories of Bobby
 3:45 pm | Craig of the Creek | Cousin of the Creek
 4:00 pm | Craig of the Creek | Jacob of the Creek
 4:15 pm | Craig of the Creek | Camper on the Run
 4:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:45 pm | Craig of the Creek | Stink Bomb
 5:00 pm | Total DramaRama | I Dream of Meanie
 5:15 pm | Total DramaRama | Breaking Bite
 5:30 pm | Total DramaRama | A Dame-gerous Game
 5:45 pm | Total DramaRama | Royal Flush
 6:00 pm | Teen Titans Go! | A Little Help Please
 6:15 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 6:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:45 pm | Teen Titans Go! | P.P.
 7:00 pm | Amazing World of Gumball | The Buddy
 7:15 pm | Amazing World of Gumball | The Inquisition
 7:30 pm | Apple & Onion | Slobbery
 7:45 pm | Apple & Onion | One Hit Wonder