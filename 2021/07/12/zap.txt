# 2021-07-12
 6:00 am | Amazing World of Gumball | The Responsible; The Dress
 6:30 am | Amazing World of Gumball | The Laziest; The Ghost
 7:00 am | Amazing World of Gumball | The Mystery; The Prank
 7:30 am | Amazing World of Gumball | The Gi; The Kiss
 8:00 am | Teen Titans Go! | Dreams; Grandma Voice
 8:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 9:00 am | Looney Tunes Cartoons | Daffuccino; Hole Gag: Moving Hole; Kitty Livin
 9:15 am | Looney Tunes Cartoons | Chain Gangster; Telephone Pole Gag: Sylvester Car Jack Lift; Falling for It
 9:30 am | Teen Titans Go! | The Titans Go Casual
 9:45 am | Teen Titans Go! | We're Off to Get Awards
10:00 am | Craig of the Creek | Turning the Tables
10:15 am | Craig of the Creek | Sleepover at JP's
10:30 am | Craig of the Creek | The Other Side
11:00 am | Craig of the Creek | Tea Timer's Ball
11:15 am | Craig of the Creek | Ferret Quest
11:30 am | Craig of the Creek | The Cardboard Identity
11:45 am | Craig of the Creek | Into the Overpast
12:00 pm | Teen Titans Go! | Books; Lazy Sunday
12:30 pm | Teen Titans Go! | Power Moves; Staring at the Future
 1:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 1:30 pm | Amazing World of Gumball | The Helmet; The Fight
 2:00 pm | Amazing World of Gumball | The End; The DVD
 2:30 pm | Amazing World of Gumball | The Knights; The Colossus
 3:00 pm | Craig of the Creek | Crisis at Elder Rock
 3:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 3:30 pm | Craig of the Creek | Kelsey the Worthy
 3:45 pm | Craig of the Creek | The Bike Thief
 4:00 pm | Craig of the Creek | The End Was Here
 4:15 pm | Craig of the Creek | Craig of the Beach
 4:30 pm | Craig of the Creek | In the Key of the Creek
 4:45 pm | Craig of the Creek | Plush Kingdom
 5:00 pm | Total DramaRama | WaterHose-Five
 5:15 pm | Total DramaRama | Say Hello to My Little Friends
 5:30 pm | Total DramaRama | Wiggin' Out
 5:45 pm | Total DramaRama | The Upside of Hunger
 6:00 pm | Teen Titans Go! | No Power; Sidekick
 6:30 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 7:00 pm | Amazing World of Gumball | The Flower; The Banana
 7:30 pm | Apple & Onion | Tips
 7:45 pm | Apple & Onion | Falafel's Fun Day