# 2021-07-18
 8:00 pm | Bob's Burgers | Brunchsquatch
 8:30 pm | Bob's Burgers | The Silence of the Louise
 9:00 pm | American Dad | A Starboy is Born
 9:30 pm | American Dad | Stan & Francine & Stan & Francine & Radika
10:00 pm | Family Guy | How the Griffin Stole Christmas
10:30 pm | Family Guy | Passenger Fatty-Seven
11:00 pm | Rick and Morty | Amortycan Grickfitti
11:30 pm | Tuca & Bertie | The Moss
12:00 am | Birdgirl | ShareBear
12:30 am | Aqua Teen | Bible Fruit
12:45 am | Aqua Teen | Gene E
 1:00 am | Family Guy | How the Griffin Stole Christmas
 1:30 am | Family Guy | Passenger Fatty-Seven
 2:00 am | American Dad | A Starboy is Born
 2:30 am | American Dad | Stan & Francine & Stan & Francine & Radika
 3:00 am | Rick and Morty | Amortycan Grickfitti
 3:30 am | Tuca & Bertie | The Moss
 4:00 am | Birdgirl | ShareBear
 4:30 am | Aqua Teen | Bible Fruit
 4:45 am | Aqua Teen | Gene E
 5:00 am | Bob's Burgers | Brunchsquatch
 5:30 am | Bob's Burgers | The Silence of the Louise