# 2021-07-01
 8:00 pm | Bob's Burgers | Tina-Rannosaurus Wrecks
 8:30 pm | Bob's Burgers | The Unbearable Like-Likeness of Gene
 9:00 pm | American Dad | Fight and Flight
 9:30 pm | American Dad | The Enlightenment of Ragi-Baba
10:00 pm | American Dad | Portrait of Francine's Genitals
10:30 pm | Rick and Morty | Mortyplicity
11:00 pm | Family Guy | Stu & Stewie's Excellent Adventure Part 3
11:30 pm | Family Guy | Mother Tucker
12:00 am | Tuca & Bertie | Kyle
12:30 am | Robot Chicken | The Deep End
12:45 am | Robot Chicken | S&M Present
 1:00 am | Aqua Teen | Total Re-Carl
 1:15 am | Aqua Teen | Revenge of the Trees
 1:30 am | Family Guy | Stu & Stewie's Excellent Adventure Part 3
 2:00 am | Family Guy | Mother Tucker
 2:30 am | Bob's Burgers | Tina-Rannosaurus Wrecks
 3:00 am | Bob's Burgers | The Unbearable Like-Likeness of Gene
 3:30 am | Tuca & Bertie | Kyle
 4:00 am | Mike Tyson Mysteries | Broken Wings
 4:15 am | Mike Tyson Mysteries | Ring of Fire
 4:30 am | The Venture Brothers | The Terminus Mandate
 5:00 am | Naruto:Shippuden | A Hole in the Heart: The Other Jinchuriki
 5:30 am | Samurai Jack | XLIV