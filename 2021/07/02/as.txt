# 2021-07-02
 8:00 pm | American Dad | Bahama Mama
 8:30 pm | American Dad | Roger's Baby
 9:00 pm | American Dad | Ninety North, Zero West
 9:30 pm | American Dad | Whole Slotta Love
10:00 pm | Family Guy | Hell Comes to Quahog
10:30 pm | Family Guy | Whistle While Your Wife Works
11:00 pm | Rick and Morty | Mortyplicity
11:30 pm | Tuca & Bertie | Kyle
12:00 am | Aqua Teen | Space Conflict from Beyond Pluto
12:15 am | Aqua Teen | Love Mummy
12:30 am | Aqua Teen | Super Birthday Snake
12:45 am | Aqua Teen | Super Trivia
 1:00 am | Aqua Teen | The The
 1:15 am | Aqua Teen | The Cloning
 1:30 am | Aqua Teen | Party All the Time
 1:45 am | Aqua Teen | Time Machine
 2:00 am | Aqua Teen | Fry Legs
 2:15 am | Aqua Teen | Kangarilla and the Magic Tarantula
 2:30 am | Aqua Teen | One Hundred
 2:45 am | Aqua Teen | Big Bro
 3:00 am | Rick and Morty | Mortyplicity
 3:30 am | Tuca & Bertie | Kyle
 4:00 am | Mike Tyson Mysteries | Mystery on Wall Street
 4:15 am | Mike Tyson Mysteries | A Dog's Life
 4:30 am | Infomercials | M.O.P.Z.
 4:45 am | Infomercials | Giles Vanderhoot
 5:00 am | Home Movies | Time to Pay the Price
 5:30 am | Home Movies | Broken Dreams