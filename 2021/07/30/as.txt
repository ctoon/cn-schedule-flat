# 2021-07-30
 8:00 pm | American Dad | The Best Christmas Story Never Told
 8:30 pm | American Dad | Bush Comes to Dinner
 9:00 pm | American Dad | American Dream Factory
 9:30 pm | American Dad | A.T. the Abusive Terrestrial
10:00 pm | Family Guy | Extra Large Medium
10:30 pm | Family Guy | Go, Stewie, Go!
11:00 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
11:30 pm | Tuca & Bertie | Sleepovers
12:00 am | Squidbillies | Forever Autumn
12:15 am | Squidbillies | Galvin
12:30 am | Squidbillies | Muscadine Wine
 1:00 am | Squidbillies | The Reenactment of the Repulsion of the Siege of Cuyler Mountain
 1:15 am | Squidbillies | Rich Dan, Poor Dan
 1:30 am | Squidbillies | Cooler-Heads Prevail
 1:45 am | Squidbillies | There's Sucker Porn Every Minute
 2:00 am | Squidbillies | Events By Russell
 2:15 am | Squidbillies | The Ballad of the Latrine Marine
 2:30 am | Squidbillies | Debased Ball
 2:45 am | Squidbillies | Tortuga de Mentiras
 3:00 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 3:30 am | Tuca & Bertie | Sleepovers
 4:00 am | The Jellies | The Invasion
 4:30 am | Infomercials | A Message from the Future
 4:45 am | Infomercials | Cool Dad - Official Trailer
 5:00 am | Home Movies | The Wizard's Baker
 5:30 am | Home Movies | Psycho-Delicate