# 2021-07-04
 8:00 pm | Bob's Burgers | Thelma and Louise Except Thelma Is Linda
 8:30 pm | Bob's Burgers | Mom, Lies, and Videotape
 9:00 pm | American Dad | Yule. Tide. Repeat.
 9:30 pm | American Dad | Hot Scoomp
10:00 pm | Family Guy | Chris Has Got a Date, Date, Date, Date, Date
10:30 pm | Family Guy | Hot Shots
11:00 pm | Rick and Morty | A Rickconvenient Mort
11:30 pm | Tuca & Bertie | Nighttime Friend
12:00 am | Birdgirl | Baltimo
12:30 am | Aqua Teen | Hoppy Bunny
12:45 am | Aqua Teen | Laser Lenses
 1:00 am | Family Guy | Chris Has Got a Date, Date, Date, Date, Date
 1:30 am | Family Guy | Hot Shots
 2:00 am | American Dad | Yule. Tide. Repeat.
 2:30 am | American Dad | Hot Scoomp
 3:00 am | Rick and Morty | A Rickconvenient Mort
 3:30 am | Tuca & Bertie | Nighttime Friend
 4:00 am | Birdgirl | Baltimo
 4:30 am | Aqua Teen | Hoppy Bunny
 4:45 am | Aqua Teen | Laser Lenses
 5:00 am | Bob's Burgers | Thelma and Louise Except Thelma Is Linda
 5:30 am | Bob's Burgers | Mom, Lies, and Videotape