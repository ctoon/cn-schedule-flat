# 2021-07-15
 8:00 pm | Bob's Burgers | The Unnatural
 8:30 pm | Bob's Burgers | A River Runs Through Bob
 9:00 pm | American Dad | The Legend of Old Ulysses
 9:30 pm | American Dad | Twinanigans
10:00 pm | American Dad | Top of the Steve
10:30 pm | Rick and Morty | Rickdependence Spray
11:00 pm | Family Guy | Long John Peter
11:30 pm | Family Guy | Love Blactually
12:00 am | Tuca & Bertie | Vibe Check
12:30 am | Robot Chicken | Celebrity Rocket
12:45 am | Robot Chicken | Dragon Nuts
 1:00 am | Aqua Teen | Gee Whiz
 1:15 am | Aqua Teen | eDork
 1:30 am | Family Guy | Long John Peter
 2:00 am | Family Guy | Love Blactually
 2:30 am | Bob's Burgers | The Unnatural
 3:00 am | Bob's Burgers | A River Runs Through Bob
 3:30 am | Tuca & Bertie | Vibe Check
 4:00 am | Mike Tyson Mysteries | The Death of Lyle Victor Linkus
 4:15 am | Mike Tyson Mysteries | San Juan Puerto Rico Blows but San Juan Capistrano…
 4:30 am | The Venture Brothers | Dr. Quymn, Medicine Woman
 5:00 am | Naruto:Shippuden | Four Tails, the King of Sage Monkeys
 5:30 am | Samurai Jack | LII