# 2021-07-08
 8:00 pm | Bob's Burgers | O.T. the Outside Toilet
 8:30 pm | Bob's Burgers | Topsy
 9:00 pm | American Dad | Garbage Stan
 9:30 pm | American Dad | The Talented Mr. Dingleberry
10:00 pm | American Dad | West to Mexico
10:30 pm | Rick and Morty | A Rickconvenient Mort
11:00 pm | Family Guy | Meet the Quagmires
11:30 pm | Family Guy | Moving Out
12:00 am | Tuca & Bertie | Nighttime Friend
12:30 am | Robot Chicken | Nightmare Generator
12:45 am | Robot Chicken | Operation Rich in Spirit
 1:00 am | Aqua Teen | The Clowning
 1:15 am | Aqua Teen | The Dressing
 1:30 am | Family Guy | Meet the Quagmires
 2:00 am | Family Guy | Moving Out
 2:30 am | Bob's Burgers | O.T. the Outside Toilet
 3:00 am | Bob's Burgers | Topsy
 3:30 am | Tuca & Bertie | Nighttime Friend
 4:00 am | Mike Tyson Mysteries | Mike Tysonland
 4:15 am | Mike Tyson Mysteries | The Gift
 4:30 am | The Venture Brothers | The Doctor Is Sin
 5:00 am | Naruto:Shippuden | Madara Uchiha
 5:30 am | Samurai Jack | XLVIII