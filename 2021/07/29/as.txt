# 2021-07-29
 8:00 pm | Bob's Burgers | I Get Psy-Chic Out of You
 8:30 pm | Bob's Burgers | Equestranauts
 9:00 pm | American Dad | Iced, Iced Babies
 9:30 pm | American Dad | Of Ice and Men
10:00 pm | American Dad | Irregarding Steve
10:30 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
11:00 pm | Family Guy | Big Man on Hippocampus
11:30 pm | Family Guy | Dial Meg for Murder
12:00 am | Tuca & Bertie | Sleepovers
12:30 am | Robot Chicken | Werewolf vs. Unicorn
12:45 am | Robot Chicken | Rabbits on a Rollercoaster
 1:00 am | Aqua Teen | Grim Reaper Gutters
 1:15 am | Aqua Teen | Moonajuana
 1:30 am | Family Guy | Big Man on Hippocampus
 2:00 am | Family Guy | Dial Meg for Murder
 2:30 am | Bob's Burgers | I Get Psy-Chic Out of You
 3:00 am | Bob's Burgers | Equestranauts
 3:30 am | Tuca & Bertie | Sleepovers
 4:00 am | The Jellies | Cornell Gets a Dog
 4:15 am | The Jellies | Trial of the Century
 4:30 am | The Venture Brothers | Blood of the Father, Heart of Steel
 5:00 am | Naruto:Shippuden | Sibling Tag Team
 5:30 am | Samurai Jack | VIII