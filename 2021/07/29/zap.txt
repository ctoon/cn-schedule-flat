# 2021-07-29
 6:00 am | Amazing World of Gumball | The News
 6:15 am | Amazing World of Gumball | The Ollie
 6:30 am | Amazing World of Gumball | The Detective
 6:45 am | Amazing World of Gumball | The Vase
 7:00 am | Amazing World of Gumball | The Fury
 7:15 am | Amazing World of Gumball | The Potato
 7:30 am | Amazing World of Gumball | The Compilation
 7:45 am | Amazing World of Gumball | The Sorcerer
 8:00 am | Teen Titans Go! | A Farce
 8:15 am | Teen Titans Go! | Pyramid Scheme
 8:30 am | Teen Titans Go! | Animals: It's Just a Word!
 8:45 am | Teen Titans Go! | Finally a Lesson
 9:00 am | Teen Titans Go! | Cartoon Feud
 9:15 am | Teen Titans Go! | Don't Be an Icarus
 9:30 am | Teen Titans Go! | TV Knight 4
 9:45 am | Teen Titans Go! | Teen Titans Roar!
10:00 am | Craig of the Creek | Fort Williams
10:15 am | Craig of the Creek | Mortimor to the Rescue
10:30 am | Craig of the Creek | Summer Wish
10:45 am | Craig of the Creek | Creek Daycare
11:00 am | Craig of the Creek | Turning the Tables
11:15 am | Craig of the Creek | Sugar Smugglers
11:30 am | Craig of the Creek | Kelsey the Author
11:45 am | Craig of the Creek | Sleepover at JP's
12:00 pm | Teen Titans Go! | Booty Scooty
12:15 pm | Teen Titans Go! | Riding the Dragon
12:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
12:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 1:00 pm | Amazing World of Gumball | The Apprentice
 1:15 pm | Amazing World of Gumball | The Parasite
 1:30 pm | Amazing World of Gumball | The Check
 1:45 pm | Amazing World of Gumball | The Love
 2:00 pm | Amazing World of Gumball | The Pest
 2:15 pm | Amazing World of Gumball | The Traitor
 2:30 pm | Amazing World of Gumball | The Hug
 2:45 pm | Amazing World of Gumball | The Advice
 3:00 pm | Craig of the Creek | Big Pinchy
 3:15 pm | Craig of the Creek | Bug City
 3:30 pm | Craig of the Creek | The Kid From 3030
 3:45 pm | Craig of the Creek | Deep Creek Salvage
 4:00 pm | Craig of the Creek | Power Punchers
 4:15 pm | Craig of the Creek | The Shortcut
 4:30 pm | Craig of the Creek | Creek Cart Racers
 4:45 pm | Craig of the Creek | Dibs Court
 5:00 pm | Total DramaRama | Breaking Bite
 5:15 pm | Total DramaRama | I Dream of Meanie
 5:30 pm | Total DramaRama | Squirrels Squirrels Squirrels
 5:45 pm | Total DramaRama | Say Hello to My Little Friends
 6:00 pm | Teen Titans Go! | Island Adventures
 7:00 pm | Amazing World of Gumball | The Routine
 7:15 pm | Amazing World of Gumball | The Girlfriend
 7:30 pm | Apple & Onion | Good Job
 7:45 pm | Apple & Onion | Lil Noodle