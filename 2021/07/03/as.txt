# 2021-07-03
 8:00 pm | Final Space | The Hidden Light
 8:30 pm | Final Space | The Ventrexian
 9:00 pm | American Dad | A Nice Night for a Drive
 9:30 pm | American Dad | Casino Normale
10:00 pm | Family Guy | Chick Cancer
10:30 pm | Family Guy | Peter's Two Dads
11:00 pm | Family Guy | The Tan Aquatic with Steve Zissou
11:30 pm | Rick and Morty | Mortyplicity
12:00 am | My Hero Academia | Early Bird!
12:30 am | Dr. Stone | Final Battle
 1:00 am | Yashahime: Princess Half-Demon | The Three Princesses
 1:30 am | Food Wars! | A Declaration of War
 2:00 am | Black Clover | Five-Leaf Clover
 2:30 am | Naruto:Shippuden | The New Akatsuki
 3:00 am | Attack on Titan | Brave Volunteers
 3:30 am | Dragon Ball Super | With His Pride on the Line! Vegeta's Challenge to Be the Strongest!
 4:00 am | Black Dynamite | "Just Beat It" or "Jackson Five Across Yo' Eyes"
 4:30 am | Black Dynamite | "Bullhorn Nights" or "Murder She Throats"
 5:00 am | Final Space | The Hidden Light
 5:30 am | Final Space | The Ventrexian