# 2021-07-16
 8:00 pm | American Dad | Funnyish Games
 8:30 pm | American Dad | Fleabiscuit
 9:00 pm | American Dad | The Future Is Borax
 9:30 pm | American Dad | Fantasy Baseball
10:00 pm | Family Guy | I Dream of Jesus
10:30 pm | Family Guy | Road to Germany
11:00 pm | Rick and Morty | Rickdependence Spray
11:30 pm | Tuca & Bertie | Vibe Check
12:00 am | Birdgirl | Pilot
12:30 am | Birdgirl | ShareBear
 1:00 am | Birdgirl | Thirdgirl
 1:30 am | Birdgirl | We Got the Internet
 2:00 am | Birdgirl | Topple the Popple
 2:30 am | Birdgirl | Baltimo
 3:00 am | Rick and Morty | Rickdependence Spray
 3:30 am | Tuca & Bertie | Vibe Check
 4:00 am | Mike Tyson Mysteries | The Yung and the Restless
 4:15 am | Mike Tyson Mysteries | A Mystery in Little Italy
 4:30 am | Infomercials | Mulchtown
 4:45 am | Infomercials | The Suplex Duplex Complex
 5:00 am | Home Movies | Everyones Entitled to My Opinion
 5:30 am | Home Movies | Camp