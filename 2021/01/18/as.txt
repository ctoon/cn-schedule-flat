# 2021-01-18
 8:00 pm | Bob's Burgers | Hawk & Chick
 8:30 pm | Bob's Burgers | The Oeder Games
 9:00 pm | American Dad | Moon Over Isla Island
 9:30 pm | American Dad | Home Adrone
10:00 pm | Rick and Morty | The Rickchurian Mortydate
10:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
11:00 pm | Family Guy | Peter Griffin: Husband, Father, Brother?
11:30 pm | Family Guy | Ready, Willing and Disabled
12:00 am | The Boondocks | The Story of Gangstalicious
12:30 am | The Boondocks | The Return of the King
 1:00 am | The Boondocks | ...Or Die Trying
 1:30 am | The Boondocks | The Hunger Strike
 2:00 am | The Boondocks | The Uncle Ruckus Reality Show
 2:30 am | The Boondocks | The Fund-Raiser
 3:00 am | The Boondocks | Pause
 3:30 am | The Boondocks | The Color Ruckus
 4:00 am | Eagleheart | Exit Wound the Gift Shop
 4:15 am | Eagleheart | Tramps
 4:30 am | Eric Andre Show | Lou Ferrigno & Downtown Julie Brown
 4:45 am | Eric Andre Show | Jodie Sweetin & Vivica A. Fox
 5:00 am | Bob's Burgers | Hawk & Chick
 5:30 am | Bob's Burgers | The Oeder Games