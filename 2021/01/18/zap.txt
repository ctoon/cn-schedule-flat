# 2021-01-18
 6:00 am | Craig of the Creek | Plush Kingdom
 6:15 am | Craig of the Creek | The Ice Pop Trio
 6:30 am | Craig of the Creek | Pencil Break Mania
 6:45 am | Craig of the Creek | The Last Game of Summer
 7:00 am | Craig of the Creek | Fall Anthology
 7:15 am | Craig of the Creek | Afterschool Snackdown
 7:30 am | Craig of the Creek | Creature Feature
 7:45 am | Craig of the Creek | King of Camping
 8:00 am | Craig of the Creek | The Other Side
 8:30 am | Craig of the Creek | The Other Side: The Tournament
 9:00 am | Craig of the Creek | The Rise and Fall of the Green Poncho
 9:15 am | Craig of the Creek | I Don't Need a Hat
 9:30 am | Craig of the Creek | Alternate Creekiverse
 9:45 am | Craig of the Creek | Snow Day
10:00 am | Craig of the Creek | Winter Break
10:30 am | Craig of the Creek | Beyond the Rapids
10:45 am | Craig of the Creek | The Jinxening
11:00 am | Craig of the Creek | Into the Overpast
11:15 am | Craig of the Creek | The Time Capsule
11:30 am | Craig of the Creek | Jessica Shorts
11:45 am | Craig of the Creek | Ferret Quest
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 2:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 3:00 pm | Teen Titans Go! | Superhero Feud
 3:15 pm | Teen Titans GO! to the Movies | 
 5:00 pm | Justice League | 