# 2021-01-12
 8:00 pm | Bob's Burgers | The Gayle Tales
 8:30 pm | Bob's Burgers | Li'l Hard Dad
 9:00 pm | American Dad | Stan Time
 9:30 pm | American Dad | Family Affair
10:00 pm | Rick and Morty | Rickmancing the Stone
10:30 pm | Rick and Morty | Pickle Rick
11:00 pm | Family Guy | One If by Clam, Two If by Sea
11:30 pm | Family Guy | And the Wiener Is...
12:00 am | Robot Chicken | Strummy Strummy Sad Sad
12:15 am | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
12:30 am | Eric Andre Show | J-Mo
12:45 am | Eric Andre Show | Evangelos
 1:00 am | Mr. Pickles | Grandpa's Night Out
 1:15 am | Mr. Pickles | Coma
 1:30 am | Family Guy | One If by Clam, Two If by Sea
 2:00 am | Family Guy | And the Wiener Is...
 2:30 am | American Dad | Stan Time
 3:00 am | Rick and Morty | Rickmancing the Stone
 3:30 am | Robot Chicken | Strummy Strummy Sad Sad
 3:45 am | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
 4:00 am | Eagleheart | Gabey, Calvin and Stu
 4:15 am | Eagleheart | Bringing Up Beezor
 4:30 am | Eric Andre Show | J-Mo
 4:45 am | Eric Andre Show | Evangelos
 5:00 am | Bob's Burgers | The Gayle Tales
 5:30 am | Bob's Burgers | Li'l Hard Dad