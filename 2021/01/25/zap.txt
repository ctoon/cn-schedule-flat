# 2021-01-25
 6:00 am | Amazing World of Gumball | The Slap
 6:15 am | Amazing World of Gumball | The Heist
 6:30 am | Amazing World of Gumball | The Detective
 6:45 am | Amazing World of Gumball | The Petals
 7:00 am | Amazing World of Gumball | The Fury
 7:15 am | Amazing World of Gumball | The Nuisance
 7:30 am | Amazing World of Gumball | The Compilation
 7:45 am | Amazing World of Gumball | The Best
 8:00 am | Teen Titans Go! | The Scoop
 8:15 am | Teen Titans Go! | The Real Orangins!
 8:30 am | Teen Titans Go! | Chicken in the Cradle
 8:45 am | Teen Titans Go! | BBRBDAY
 9:00 am | Teen Titans Go! | Tower Renovation
 9:15 am | Teen Titans Go! | Tall Titan Tales
 9:30 am | Teen Titans Go! | Quantum Fun
 9:45 am | Teen Titans Go! | Baby Mouth
10:00 am | Craig of the Creek | Escape From Family Dinner
10:15 am | Craig of the Creek | The End Was Here
10:30 am | Craig of the Creek | Monster in the Garden
10:45 am | Craig of the Creek | The Children of the Dog
11:00 am | Teen Titans Go! | The Power of Shrimps
11:15 am | Teen Titans Go! | The Chaff
11:30 am | Teen Titans Go! | My Name Is Jose
11:45 am | Teen Titans Go! | Curse of the Booty Scooty
12:00 pm | Teen Titans Go! | Kabooms
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 1:00 pm | Amazing World of Gumball | The Traitor
 1:15 pm | Amazing World of Gumball | The Advice
 1:30 pm | Amazing World of Gumball | The Signal
 1:45 pm | Amazing World of Gumball | The Sorcerer
 2:00 pm | Amazing World of Gumball | The Girlfriend
 2:15 pm | Amazing World of Gumball | The Menu
 2:30 pm | Amazing World of Gumball | The Parasite
 2:45 pm | Amazing World of Gumball | The Outside
 3:00 pm | Amazing World of Gumball | The Love
 3:15 pm | Amazing World of Gumball | The Copycats
 3:30 pm | Amazing World of Gumball | The Choices
 3:45 pm | Amazing World of Gumball | The Catfish
 4:00 pm | Craig of the Creek | Dog Decider
 4:15 pm | Craig of the Creek | Into the Overpast
 4:30 pm | Craig of the Creek | Bring Out Your Beast
 4:45 pm | Craig of the Creek | The Time Capsule
 5:00 pm | Total DramaRama | Weiner Takes All
 5:15 pm | Total DramaRama | Stingin' in the Rain
 5:30 pm | Total DramaRama | Apoca-lice Now
 5:45 pm | Total DramaRama | Cartoon Realism
 6:00 pm | Teen Titans Go! | Royal Jelly
 6:15 pm | Teen Titans Go! | Had to Be There
 6:30 pm | Teen Titans Go! | Strength of a Grown Man
 6:45 pm | Teen Titans Go! | The Great Disaster
 7:00 pm | Amazing World of Gumball | The Roots
 7:15 pm | Amazing World of Gumball | The Ex
 7:30 pm | Amazing World of Gumball | The Blame
 7:45 pm | Amazing World of Gumball | The Uncle