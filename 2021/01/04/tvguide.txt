# 2021-01-04
 6:00 am | Amazing World of Gumball | The One; The Inquisition
 6:30 am | Amazing World of Gumball | The Vegging; The Check
 7:00 am | Amazing World of Gumball | The Father; The Pest
 7:30 am | Amazing World of Gumball | The Cringe; The Hug
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 9:30 am | Teen Titans Go! | The Best Robin; Road Trip
10:00 am | Craig of the Creek | Memories of Bobby; Crisis at Elder Rock
10:30 am | Craig of the Creek | Jacob of the Creek Kelsey the Worthy
11:00 am | Teen Titans Go! | Leg Day; Cat's Fancy
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:30 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 1:00 pm | Amazing World of Gumball | The Heist; The Slip
 1:30 pm | Amazing World of Gumball | The Drama; The Petals
 2:00 pm | Amazing World of Gumball | The Nuisance; The Future
 2:30 pm | Amazing World of Gumball | The Wish; The Best
 3:00 pm | Amazing World of Gumball | The Worst; The Revolt
 3:30 pm | Amazing World of Gumball | The List; The Mess
 4:00 pm | Craig of the Creek | Fort Williams; Jessica Shorts
 4:30 pm | Craig of the Creek | Ferret Quest Kelsey the Elder
 5:00 pm | Total DramaRama | Having the Timeout of Our Lives Stink. Stank. Stunk
 5:30 pm | Total DramaRama | Hic Hic Hooray Robo Teacher
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | Waffles; Hot Salad Water
 7:00 pm | Teen Titans Go! | Booty Scooty Uncle Jokes
 7:30 pm | Teen Titans Go! | Fish Water Boys vs Girls