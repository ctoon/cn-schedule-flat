# 2021-01-16
 6:00 am | Teen Titans Go! | Beast Girl
 6:15 am | Teen Titans Go! | Superhero Feud
 6:30 am | Teen Titans Go! | TV Knight 3
 6:45 am | Teen Titans Go! | Quantum Fun
 7:00 am | Teen Titans Go! | Bro-Pocalypse
 7:15 am | Teen Titans Go! | The Fight
 7:30 am | Teen Titans Go! | The Scoop
 7:45 am | Teen Titans Go! | Genie President
 8:00 am | Teen Titans Go! | Chicken in the Cradle
 8:15 am | Teen Titans Go! | Little Elvis
 8:30 am | Teen Titans Go! | The Groover
 8:45 am | Teen Titans Go! | Baby Mouth
 9:00 am | Teen Titans Go! | The Power of Shrimps
 9:15 am | Teen Titans Go! | Tall Titan Tales
 9:30 am | Total DramaRama | Stingin' in the Rain
 9:45 am | Total DramaRama | Space Codyty
10:00 am | Craig of the Creek | The Ground Is Lava!
10:15 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
10:30 am | Amazing World of Gumball | The Stars
10:45 am | Amazing World of Gumball | The Menu
11:00 am | Amazing World of Gumball | The Box
11:15 am | Amazing World of Gumball | The Uncle
11:30 am | Amazing World of Gumball | The Matchmaker
11:45 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
12:00 pm | Amazing World of Gumball | The Grades
12:15 pm | Amazing World of Gumball | The Petals
12:30 pm | Amazing World of Gumball | The Diet
12:45 pm | Amazing World of Gumball | The Nuisance
 1:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 1:15 pm | Amazing World of Gumball | The Best
 1:30 pm | Amazing World of Gumball | The Worst
 1:45 pm | Amazing World of Gumball | The Inquisition
 2:00 pm | Amazing World of Gumball | The List
 2:15 pm | Amazing World of Gumball | The Blame
 2:30 pm | Amazing World of Gumball | The Deal
 2:45 pm | Amazing World of Gumball | The Detective
 3:00 pm | Amazing World of Gumball | The Line
 3:15 pm | Amazing World of Gumball | The Stink
 3:30 pm | Amazing World of Gumball | The Singing
 3:45 pm | Amazing World of Gumball | The Nest
 4:00 pm | Teen Titans Go! | My Name Is Jose
 4:15 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 4:30 pm | Teen Titans Go! | The Real Orangins!
 4:45 pm | Teen Titans Go! | Business Ethics Wink Wink
 5:00 pm | Teen Titans Go! | Flashback
 5:30 pm | Teen Titans Go! | Kabooms
 5:45 pm | Teen Titans Go! | TV Knight 6
 6:00 pm | Amazing World of Gumball | The Puppets
 6:15 pm | Amazing World of Gumball | The Parents
 6:30 pm | Amazing World of Gumball | The Lady
 6:45 pm | Amazing World of Gumball | The Founder
 7:00 pm | Amazing World of Gumball | The Sucker
 7:15 pm | Amazing World of Gumball | The Schooling
 7:30 pm | Amazing World of Gumball | The Cage
 7:45 pm | Amazing World of Gumball | The Intelligence