# 2021-01-28
 8:00 pm | Bob's Burgers | Secret Admiral-Irer
 8:30 pm | Bob's Burgers | Glued, Where's My Bob?
 9:00 pm | American Dad | Stanny-Boy and Frantastic
 9:30 pm | American Dad | A Pinata Named Desire
10:00 pm | Rick and Morty | Raising Gazorpazorp
10:30 pm | Rick and Morty | Rixty Minutes
11:00 pm | Family Guy | Peter's Got Woods
11:30 pm | Family Guy | The Perfect Castaway
12:00 am | Mr. Pickles | S.H.O.E.S
12:15 am | Mr. Pickles | Telemarketers Are the Devil
12:30 am | Eric Andre Show | Naturi Naughton; Ryan Phillippe
12:45 am | Eric Andre Show | Jimmy Kimmel; Tyler the Creator
 1:00 am | Aqua Teen | The Mooninites: Final Mooning
 1:15 am | Aqua Teen | The South Bronx Paradise Diet
 1:30 am | Family Guy | Peter's Got Woods
 2:00 am | Family Guy | The Perfect Castaway
 2:30 am | American Dad | Stanny-Boy and Frantastic
 3:00 am | Rick and Morty | Raising Gazorpazorp
 3:30 am | Mr. Pickles | S.H.O.E.S
 3:45 am | Mr. Pickles | Telemarketers Are the Devil
 4:00 am | Eagleheart | Honch
 4:30 am | Eric Andre Show | Naturi Naughton; Ryan Phillippe
 4:45 am | Eric Andre Show | Jimmy Kimmel; Tyler the Creator
 5:00 am | Bob's Burgers | Secret Admiral-Irer
 5:30 am | Bob's Burgers | Glued, Where's My Bob?