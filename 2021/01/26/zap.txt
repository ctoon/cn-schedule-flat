# 2021-01-26
 6:00 am | Amazing World of Gumball | The Stories
 6:15 am | Amazing World of Gumball | The Deal
 6:30 am | Amazing World of Gumball | The Guy
 6:45 am | Amazing World of Gumball | The Line
 7:00 am | Amazing World of Gumball | The Boredom
 7:15 am | Amazing World of Gumball | The Singing
 7:30 am | Amazing World of Gumball | The Slide
 7:45 am | Amazing World of Gumball | The Puppets
 8:00 am | Teen Titans Go! | The Fight
 8:15 am | Teen Titans Go! | Business Ethics Wink Wink
 8:30 am | Teen Titans Go! | Genie President
 8:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 9:00 am | Teen Titans Go! | Little Elvis
 9:15 am | Teen Titans Go! | I Used to Be a Peoples
 9:30 am | Teen Titans Go! | The Groover
 9:45 am | Teen Titans Go! | Grube's Fairytales
10:00 am | Craig of the Creek | The Curse
10:15 am | Craig of the Creek | Jessica Shorts
10:30 am | Craig of the Creek | The Future Is Cardboard
10:45 am | Craig of the Creek | Ferret Quest
11:00 am | Teen Titans Go! | How's This for a Special? Spaaaace
11:30 am | Teen Titans Go! | Teen Titans Vroom
12:00 pm | Teen Titans Go! | Them Soviet Boys
12:15 pm | Teen Titans Go! | TV Knight 4
12:30 pm | Teen Titans Go! | Lil' Dimples
12:45 pm | Teen Titans Go! | Animals: It's Just a Word!
 1:00 pm | Amazing World of Gumball | The Awkwardness
 1:15 pm | Amazing World of Gumball | The Cycle
 1:30 pm | Amazing World of Gumball | The Nest
 1:45 pm | Amazing World of Gumball | The Stars
 2:00 pm | Amazing World of Gumball | The Points
 2:15 pm | Amazing World of Gumball | The Box
 2:30 pm | Amazing World of Gumball | The Bus
 2:45 pm | Amazing World of Gumball | The Matchmaker
 3:00 pm | Amazing World of Gumball | The Night
 3:15 pm | Amazing World of Gumball | The Grades
 3:30 pm | Amazing World of Gumball | The Misunderstandings
 3:45 pm | Amazing World of Gumball | The Diet
 4:00 pm | Craig of the Creek | Lost in the Sewer
 4:15 pm | Craig of the Creek | Beyond the Rapids
 4:30 pm | Craig of the Creek | The Brood
 4:45 pm | Craig of the Creek | Under the Overpass
 5:00 pm | Total DramaRama | Gnome More Mister Nice Guy
 5:15 pm | Total DramaRama | OWW
 5:30 pm | Total DramaRama | Look Who's Clocking
 5:45 pm | Total DramaRama | Jelly Aches
 6:00 pm | Teen Titans Go! | The Viewers Decide
 6:15 pm | Teen Titans Go! | Walk Away
 6:30 pm | Teen Titans Go! | TV Knight 5
 6:45 pm | Teen Titans Go! | Record Book
 7:00 pm | Amazing World of Gumball | The Code
 7:15 pm | Amazing World of Gumball | The Worst
 7:30 pm | Amazing World of Gumball | The Test
 7:45 pm | Amazing World of Gumball | The List