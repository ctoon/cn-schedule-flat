# 2021-01-05
 8:00 pm | Bob's Burgers | Wharf Horse
 8:30 pm | Bob's Burgers | World Wharf II: The Wharfening
 9:00 pm | American Dad | Tearjerker
 9:30 pm | American Dad | Oedipal Panties
10:00 pm | Rick and Morty | Mortynight Run
10:30 pm | Rick and Morty | Auto Erotic Assimilation
11:00 pm | Family Guy | Fifteen Minutes of Shame
11:30 pm | Family Guy | Road to Rhode Island
12:00 am | Robot Chicken | Not Enough Women
12:15 am | Robot Chicken | The Angelic Sounds of Mike Giggling
12:30 am | Eric Andre Show | The 50th Episode!
12:45 am | Eric Andre Show | The Eric Andre Show: The Making of Season Five
 1:00 am | Mr. Pickles | Tommy's Cartoon
 1:15 am | Mr. Pickles | Season 3 Finale
 1:30 am | Family Guy | Fifteen Minutes of Shame
 2:00 am | Family Guy | Road to Rhode Island
 2:30 am | American Dad | Tearjerker
 3:00 am | Rick and Morty | Mortynight Run
 3:30 am | Robot Chicken | Not Enough Women
 3:45 am | Robot Chicken | The Angelic Sounds of Mike Giggling
 4:00 am | Eagleheart | Master of Da Skies
 4:15 am | Eagleheart | Me Llamo Justice
 4:30 am | Eric Andre Show | The 50th Episode!
 4:45 am | Eric Andre Show | The Eric Andre Show: The Making of Season Five
 5:00 am | Bob's Burgers | Wharf Horse
 5:30 am | Bob's Burgers | World Wharf II: The Wharfening