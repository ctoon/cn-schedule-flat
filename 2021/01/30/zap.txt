# 2021-01-30
 6:00 am | Teen Titans Go! | Collect Them All
 6:15 am | Teen Titans Go! | Communicate Openly
 6:30 am | Teen Titans Go! | Cartoon Feud
 6:45 am | Teen Titans Go! | What's Opera, Titans?
 7:00 am | Teen Titans Go! | Don't Be an Icarus
 7:15 am | Teen Titans Go! | Royal Jelly
 7:30 am | Teen Titans Go! | TV Knight 4
 7:45 am | Teen Titans Go! | Strength of a Grown Man
 8:00 am | Teen Titans Go! | Teen Titans Roar!
 8:15 am | Teen Titans Go! | Had to Be There
 8:30 am | Teen Titans Go! | Teen Titans Vroom
 8:45 am | Teen Titans Go! | TV Knight 6
 9:00 am | Teen Titans Go! | Cool Uncles
 9:15 am | Teen Titans Go! | Lucky Stars
 9:30 am | Total DramaRama | Bad Seed
 9:45 am | Total DramaRama | Snack to the Future
10:00 am | Craig of the Creek | Plush Kingdom
10:15 am | Craig of the Creek | The Ice Pop Trio
10:30 am | Amazing World of Gumball | The Boombox; The Castle
11:00 am | Amazing World of Gumball | The Tape; The Sweaters
11:30 am | Amazing World of Gumball | The Internet; The Plan
12:00 pm | Amazing World of Gumball | The World; The Finale
12:30 pm | Amazing World of Gumball | The Kids; The Fan
 1:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 1:30 pm | Amazing World of Gumball | The Name; The Extras
 2:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 2:30 pm | Amazing World of Gumball | The Fraud; The Void
 3:00 pm | Amazing World of Gumball | The Boss; The Move
 3:30 pm | Amazing World of Gumball | The Law; The Allergy
 3:45 pm | Amazing World of Gumball | The Matchmaker
 4:00 pm | Teen Titans Go! | Super Hero Summer Camp
 5:00 pm | Teen Titans Go! | The Viewers Decide
 5:15 pm | Teen Titans Go! | The Great Disaster
 5:30 pm | Teen Titans Go! | Girls Night In Part 1
 6:00 pm | Amazing World of Gumball | The Mothers; The Password
 6:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 7:00 pm | Amazing World of Gumball | The Mirror; The Burden
 7:30 pm | Amazing World of Gumball | The Bros; The Man