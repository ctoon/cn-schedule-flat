# 2021-01-21
 6:00 am | Amazing World of Gumball | The Internet; The Plan
 6:30 am | Amazing World of Gumball | The World; The Finale
 7:00 am | Amazing World of Gumball | The Kids; The Fan
 7:30 am | Amazing World of Gumball | The Return
 7:45 am | Amazing World of Gumball | The Nemesis
 8:00 am | Teen Titans Go! | Career Day
 8:15 am | Teen Titans Go! | Demon Prom
 8:30 am | Teen Titans Go! | TV Knight 2
 8:45 am | Teen Titans Go! | BBCYFSHIPBDAY
 9:00 am | Teen Titans Go! | The Academy
 9:15 am | Teen Titans Go! | Mo' Money Mo' Problems
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:45 am | Teen Titans Go! | What's Opera, Titans?
10:00 am | Craig of the Creek | Too Many Treasures
10:15 am | Craig of the Creek | Secret in a Bottle
10:30 am | Craig of the Creek | The Final Book
10:45 am | Craig of the Creek | Trading Day
11:00 am | Teen Titans Go! | The Streak, Part 1
11:15 am | Teen Titans Go! | The Streak, Part 2
11:30 am | Teen Titans Go! | Movie Night
11:45 am | Teen Titans Go! | Permanent Record
12:00 pm | Teen Titans Go! | BBRAE
12:30 pm | Teen Titans Go! | Titan Saving Time
12:45 pm | Teen Titans Go! | Master Detective
 1:00 pm | Amazing World of Gumball | The Crew
 1:15 pm | Amazing World of Gumball | The Others
 1:30 pm | Amazing World of Gumball | The Society; The Spoiler
 2:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:30 pm | Amazing World of Gumball | The Downer; The Egg
 3:00 pm | Amazing World of Gumball | The Triangle; The Money
 3:30 pm | Amazing World of Gumball | The Gift
 3:45 pm | Amazing World of Gumball | The Parking
 4:00 pm | Craig of the Creek | Afterschool Snackdown
 4:15 pm | Craig of the Creek | Sugar Smugglers
 4:30 pm | Craig of the Creek | Creature Feature
 4:45 pm | Craig of the Creek | Sleepover at JP's
 5:00 pm | Total DramaRama | Way Back Wendel
 5:15 pm | Total DramaRama | Baby Brother Blues
 5:30 pm | Total DramaRama | Camping Is in Tents
 6:00 pm | Teen Titans Go! | Stockton, CA!
 6:15 pm | Teen Titans Go! | Cartoon Feud
 6:30 pm | Teen Titans Go! | Collect Them All
 6:45 pm | Teen Titans Go! | Don't Be an Icarus
 7:00 pm | Amazing World of Gumball | The Boombox; The Castle
 7:30 pm | Amazing World of Gumball | The Tape; The Sweaters