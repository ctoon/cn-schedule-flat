# 2021-01-09
 8:00 pm | Final Space | The Grand Surrender
 8:30 pm | American Dad | Choosey Wives Choose Smith
 9:00 pm | American Dad | Escape from Pearl Bailey
 9:30 pm | American Dad | Pulling Double Booty
10:00 pm | Rick and Morty | Total Rickall
10:30 pm | Rick and Morty | Get Schwifty
11:00 pm | Family Guy | Fore Father
11:30 pm | Family Guy | Thin White Line Part 1
12:00 am | Dragon Ball Super | Survive! The "Tournament of Power" Begins at Last!!
12:30 am | Attack on Titan | The Other Side of the Sea
 1:00 am | Sword Art Online Alicization War of Underworld | Awakening
 1:30 am | Fire Force | Road to the Oasis
 2:00 am | Assassination Classroom | School's Out/1st Term
 2:30 am | Gemusetto: Death Beat(s) | Episode Thirteen: play to D.C. al Coda
 2:45 am | Gemusetto: Death Beat(s) | Episode Fourteen: al Fine
 3:00 am | Naruto:Shippuden | The Five Kage Assemble
 3:30 am | Demon Slayer: Kimetsu no Yaiba | Together Forever
 4:00 am | The Boondocks | The Story of Thugnificent
 4:30 am | The Boondocks | Attack of the Killer Kung-fu Wolf Bitch
 5:00 am | Rick and Morty | Total Rickall
 5:30 am | Final Space | The Grand Surrender