# 2021-01-20
 6:00 am | Amazing World of Gumball | The Pest
 6:15 am | Amazing World of Gumball | The Romantic
 6:30 am | Amazing World of Gumball | The Hug
 6:45 am | Amazing World of Gumball | The Uploads
 7:00 am | Amazing World of Gumball | The Origins
 7:15 am | Amazing World of Gumball | The Origins
 7:30 am | Amazing World of Gumball | The Routine
 7:45 am | Amazing World of Gumball | The Wicked
 8:00 am | Teen Titans Go! | Employee of the Month Redux
 8:15 am | Teen Titans Go! | BL4Z3
 8:30 am | Teen Titans Go! | Orangins
 8:45 am | Teen Titans Go! | Lication
 9:00 am | Teen Titans Go! | Jinxed
 9:15 am | Teen Titans Go! | Labor Day
 9:30 am | Teen Titans Go! | Ones and Zeroes
 9:45 am | Teen Titans Go! | Throne of Bones
10:00 am | Craig of the Creek | Itch to Explore
10:15 am | Craig of the Creek | Tea Timer's Ball
10:30 am | Craig of the Creek | Jessica Goes to the Creek
10:45 am | Craig of the Creek | The Cardboard Identity
11:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
11:30 am | Teen Titans Go! | Beast Girl
11:45 am | Teen Titans Go! | TV Knight 3
12:00 pm | Teen Titans Go! | Bro-Pocalypse
12:15 pm | Teen Titans Go! | The Scoop
12:30 pm | Teen Titans Go! | Flashback
12:45 pm | Teen Titans Go! | Serious Business
 1:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 1:30 pm | Amazing World of Gumball | The Mirror; The Burden
 2:00 pm | Amazing World of Gumball | The Bros; The Man
 2:30 pm | Amazing World of Gumball | The Pizza; The Lie
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Oracle; The Safety
 4:00 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:15 pm | Craig of the Creek | I Don't Need a Hat
 4:30 pm | Craig of the Creek | Alternate Creekiverse
 4:45 pm | Craig of the Creek | Snow Day
 5:00 pm | Total DramaRama | Mutt Ado About Owen
 5:15 pm | Total DramaRama | Tu Ba or Not Tu Ba
 5:30 pm | Total DramaRama | Simons Are Forever
 5:45 pm | Total DramaRama | Dude Where's Macaw
 6:00 pm | Teen Titans Go! | The Chaff
 6:15 pm | Teen Titans Go! | Them Soviet Boys
 6:30 pm | Teen Titans Go! | Curse of the Booty Scooty
 6:45 pm | Teen Titans Go! | Lil' Dimples
 7:00 pm | Amazing World of Gumball | The Apprentice
 7:15 pm | Amazing World of Gumball | The Upgrade
 7:30 pm | Amazing World of Gumball | The Check
 7:45 pm | Amazing World of Gumball | The Comic