# 2021-03-15
 6:00 am | Amazing World of Gumball | The Upgrade
 6:15 am | Amazing World of Gumball | The Roots
 6:30 am | Amazing World of Gumball | The Comic
 6:45 am | Amazing World of Gumball | The Blame
 7:00 am | Amazing World of Gumball | The Romantic
 7:15 am | Amazing World of Gumball | The Slap
 7:30 am | Amazing World of Gumball | The Uploads
 7:45 am | Amazing World of Gumball | The Apprentice
 8:00 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
 8:15 am | Teen Titans Go! | The Gold Standard
 8:30 am | Teen Titans Go! | Business Ethics Wink Wink
 8:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 9:00 am | Teen Titans Go! | I Used to Be a Peoples
 9:15 am | Teen Titans Go! | Collect Them All
 9:30 am | Teen Titans Go! | The Metric System vs. Freedom
 9:45 am | Teen Titans Go! | Don't Press Play
10:00 am | Craig of the Creek | The Mystery of the Timekeeper
10:15 am | Craig of the Creek | Cousin of the Creek
10:30 am | Craig of the Creek | Return of the Honeysuckle Rangers
10:45 am | Craig of the Creek | Creek Daycare
11:00 am | Victor and Valentino | Balloon Boys
11:15 am | Victor and Valentino | The Guest
11:30 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
11:45 am | Teen Titans Go! | The Gold Standard
12:00 pm | Teen Titans Go! | Teen Titans Vroom
12:30 pm | Teen Titans Go! | What's Opera, Titans?
12:45 pm | Teen Titans Go! | Communicate Openly
 1:00 pm | Amazing World of Gumball | The Triangle; The Money
 1:30 pm | Amazing World of Gumball | The Return
 1:45 pm | Amazing World of Gumball | The Traitor
 2:00 pm | Amazing World of Gumball | The Nemesis
 2:15 pm | Amazing World of Gumball | The Advice
 2:30 pm | Amazing World of Gumball | The Crew
 2:45 pm | Amazing World of Gumball | The Signal
 3:00 pm | Amazing World of Gumball | The Others
 3:15 pm | Amazing World of Gumball | The Girlfriend
 3:30 pm | Amazing World of Gumball | The Signature
 3:45 pm | Amazing World of Gumball | The Parasite
 4:00 pm | Craig of the Creek | Tea Timer's Ball
 4:15 pm | Craig of the Creek | Mortimor to the Rescue
 4:30 pm | Craig of the Creek | The Cardboard Identity
 4:45 pm | Craig of the Creek | Secret in a Bottle
 5:00 pm | Total DramaRama | Bananas & Cheese
 5:15 pm | Total DramaRama | Pinata Regatta
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Total DramaRama | A Dame-gerous Game
 6:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 7:00 pm | Amazing World of Gumball | The Routine
 7:15 pm | Amazing World of Gumball | The Night
 7:30 pm | Apple & Onion | Apple's Focus
 7:45 pm | Apple & Onion | Follow Your Dreams