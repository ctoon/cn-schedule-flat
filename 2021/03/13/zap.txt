# 2021-03-13
 6:00 am | Teen Titans Go! | Kabooms
 6:30 am | Teen Titans Go! | Quantum Fun
 6:45 am | Teen Titans Go! | BBRBDAY
 7:00 am | Teen Titans Go! | Don't Press Play
 7:15 am | Teen Titans Go! | BBRAEBDAY
 7:30 am | Teen Titans Go! | Real Art
 7:45 am | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
 8:00 am | Teen Titans Go! | The Chaff
 8:30 am | Teen Titans Go! | Super Hero Summer Camp
 9:00 am | Teen Titans Go! | Villains in a Van Getting Gelato
 9:11 am | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
 9:30 am | Total DramaRama | AbaracaDuncan
 9:45 am | Total DramaRama | Life of Pie
10:00 am | Craig of the Creek | New Jersey
10:15 am | Craig of the Creek | Fan or Foe
10:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular, Part 1
10:45 am | Teen Titans Go! | Rain on Your Wedding Day
11:00 am | Teen Titans Go! | Flashback
11:30 am | Teen Titans Go! | Kabooms
12:00 pm | Amazing World of Gumball | The Microwave; The Meddler
12:30 pm | Amazing World of Gumball | The Mustache; The Date
 1:00 pm | Amazing World of Gumball | The Club; The Wand
 1:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 2:00 pm | Amazing World of Gumball | The Quest; The Spoon
 2:30 pm | Amazing World of Gumball | The Helmet; The Fight
 3:00 pm | Victor and Valentino | Ghosted
 3:15 pm | Victor and Valentino | Folk Art Foes
 3:30 pm | Victor and Valentino | Dead Ringer
 3:45 pm | Victor and Valentino | Legend of the Hidden Skate Park
 4:00 pm | Teen Titans Go! | The Power of Shrimps
 4:15 pm | Teen Titans Go! | I Used to Be a Peoples
 4:30 pm | Teen Titans Go! | My Name Is Jose
 4:45 pm | Teen Titans Go! | The Metric System vs. Freedom
 5:00 pm | Teen Titans Go! | The Real Orangins!
 5:15 pm | Teen Titans Go! | The Chaff
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 5:45 pm | Teen Titans Go! | What's Opera, Titans?
 6:00 pm | Amazing World of Gumball | The End; The DVD
 6:30 pm | Amazing World of Gumball | The Sock; The Genius
 7:00 pm | Amazing World of Gumball | The Knights; The Colossus
 7:30 pm | Amazing World of Gumball | The Fridge; The Remote