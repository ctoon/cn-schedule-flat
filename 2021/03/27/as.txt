# 2021-03-27
 8:00 pm | American Dad | Julia Rogerts
 8:30 pm | American Dad | The Life and Times of Stan Smith
 9:00 pm | American Dad | The Bitchin' Race
 9:30 pm | Rick and Morty | Promortyus
10:00 pm | Rick and Morty | The Vat of Acid Episode
10:30 pm | Final Space | The Hidden Light
11:00 pm | Family Guy | Hand That Rocks the Wheelchair
11:30 pm | Family Guy | Trading Places
12:00 am | Dragon Ball Super | Frieza and Frost! A Mutual Malevolence?
12:30 am | Attack on Titan | Deceiver
 1:00 am | Food Wars! | The Shadow Over the Dining Table
 1:30 am | Fire Force | The Holy Woman's Anguish, The/Man, Assault
 2:00 am | Black Clover | The Tilted Scale
 2:30 am | SSSS.Gridman | Decisive Battle
 3:00 am | Naruto:Shippuden | Sibling Tag Team
 3:30 am | Demon Slayer: Kimetsu no Yaiba | Against Corps Rules
 4:00 am | The Boondocks | The Color Ruckus
 4:30 am | The Boondocks | It's Goin' Down
 5:00 am | Rick and Morty | Promortyus
 5:30 am | Final Space | The Hidden Light