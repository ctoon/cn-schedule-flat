# 2021-03-05
 8:00 pm | Bob's Burgers | Mutiny on the Windbreaker
 8:30 pm | American Dad | Now and Gwen
 9:00 pm | American Dad | Dreaming of a White Porsche Christmas
 9:30 pm | American Dad | LGBSteve
10:00 pm | Rick and Morty | Total Rickall
10:30 pm | Rick and Morty | Get Schwifty
11:00 pm | Family Guy | Family Gay
11:30 pm | Family Guy | The Juice Is Loose
12:00 am | Three Busy Debras | A Very Debra Christmas
12:15 am | Three Busy Debras | Cartwheel Club
12:30 am | Three Busy Debras | Sleepover!
12:45 am | Three Busy Debras | Barbra
 1:00 am | Three Busy Debras | ATM (All The Money)
 1:15 am | Three Busy Debras | Debspringa
 1:30 am | Three Busy Debras | A Very Debra Christmas
 1:45 am | Three Busy Debras | Cartwheel Club
 2:00 am | Three Busy Debras | Sleepover!
 2:15 am | Three Busy Debras | Barbra
 2:30 am | Three Busy Debras | ATM (All The Money)
 2:45 am | Three Busy Debras | Debspringa
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | Rick and Morty | Get Schwifty
 4:00 am | Tim & Eric's Bedtime Stories | Butter
 4:30 am | The Shivering Truth | ConstaDeath
 4:45 am | JJ Villard's Fairy Tales | Pinocchio
 5:00 am | Bob's Burgers | Mutiny on the Windbreaker
 5:30 am | Home Movies | Coffins and Cradles