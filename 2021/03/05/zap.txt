# 2021-03-05
 6:00 am | Amazing World of Gumball | The Third; The Debt
 6:30 am | Amazing World of Gumball | The Pressure; The Painting
 7:00 am | Amazing World of Gumball | The Responsible; The Dress
 7:30 am | Amazing World of Gumball | The Laziest; The Ghost
 8:00 am | Teen Titans Go! | The Gold Standard
 8:15 am | Teen Titans Go! | Hand Zombie
 8:30 am | Teen Titans Go! | Master Detective
 8:45 am | Teen Titans Go! | Employee of the Month Redux
 9:00 am | Teen Titans Go! | Orangins
 9:15 am | Teen Titans Go! | Hot Salad Water
 9:30 am | Teen Titans Go! | The Avogodo
 9:45 am | Teen Titans Go! | Jinxed
10:00 am | Craig of the Creek | Vulture's Nest
10:15 am | Craig of the Creek | Jextra Perrestrial
10:30 am | Craig of the Creek | Kelsey Quest
10:45 am | Craig of the Creek | The Takeout Mission
11:00 am | Victor and Valentino | Churro Kings
11:15 am | Victor and Valentino | Tree Buds
11:30 am | Teen Titans Go! | Oh Yeah!
11:45 am | Teen Titans Go! | Shrimps and Prime Rib
12:00 pm | Teen Titans Go! | Island Adventures
 1:00 pm | Amazing World of Gumball | The End; The DVD
 1:30 pm | Amazing World of Gumball | The Knights; The Colossus
 2:00 pm | Amazing World of Gumball | The Fridge; The Remote
 2:30 pm | Amazing World of Gumball | The Flower; The Banana
 3:00 pm | Amazing World of Gumball | The Phone; The Job
 3:30 pm | Amazing World of Gumball | The Words; The Apology
 4:00 pm | Craig of the Creek | Dog Decider
 4:15 pm | Craig of the Creek | The Climb
 4:30 pm | Craig of the Creek | Bring Out Your Beast
 4:45 pm | Craig of the Creek | Big Pinchy
 5:00 pm | Total DramaRama | Aquarium for a Dream
 5:15 pm | Total DramaRama | Stink. Stank. Stunk
 5:30 pm | Total DramaRama | Free Chili
 5:45 pm | Total DramaRama | Robo Teacher
 6:00 pm | Teen Titans Go! | The Art of Ninjutsu
 6:15 pm | Teen Titans Go! | Think About Your Future
 6:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 7:00 pm | Amazing World of Gumball | The Slap
 7:15 pm | Amazing World of Gumball | The Detective
 7:30 pm | Apple & Onion | Hotdog's Movie Premiere
 7:45 pm | Apple & Onion | Onionless