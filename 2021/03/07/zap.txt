# 2021-03-07
 6:00 am | Teen Titans Go! | Ones and Zeroes
 6:15 am | Teen Titans Go! | Demon Prom
 6:30 am | Bakugan: Armored Alliance | The AB Bakugan Battle League! Part 1; The AB Bakugan Battle League! Part 2
 7:00 am | Teen Titans Go! | Career Day
 7:15 am | Teen Titans Go! | BBCYFSHIPBDAY
 7:30 am | Teen Titans Go! | TV Knight 2
 7:45 am | Teen Titans Go! | Butter Wall
 8:00 am | Teen Titans Go! | The Academy
 8:15 am | Teen Titans Go! | Beast Girl
 8:30 am | Teen Titans Go! | Throne of Bones
 8:45 am | Teen Titans Go! | TV Knight 3
 9:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 9:30 am | Teen Titans Go! | Real Art
 9:45 am | Teen Titans Go! | Don't Press Play
10:00 am | Craig of the Creek | The Last Game of Summer
10:15 am | Craig of the Creek | Snow Day
10:30 am | Craig of the Creek | Fall Anthology
10:45 am | Craig of the Creek | Alternate Creekiverse
11:00 am | Craig of the Creek | Afterschool Snackdown
11:15 am | Craig of the Creek | I Don't Need a Hat
11:30 am | Craig of the Creek | King of Camping
11:45 am | Craig of the Creek | Snow Place Like Home
12:00 pm | Amazing World of Gumball | The Agent
12:15 pm | Amazing World of Gumball | The Inquisition
12:30 pm | Amazing World of Gumball | The Third; The Debt
 1:00 pm | Amazing World of Gumball | The Pressure; The Painting
 1:30 pm | Amazing World of Gumball | The Responsible; The Dress
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | Victor and Valentino | Los Pajaros
 3:15 pm | Victor and Valentino | Poacher Patrol
 3:30 pm | Victor and Valentino | Charlene Mania
 3:45 pm | Victor and Valentino | In Your Own Skin
 4:00 pm | Teen Titans Go! | Bro-Pocalypse
 4:15 pm | Teen Titans Go! | The Scoop
 4:30 pm | Teen Titans Go! | Chicken in the Cradle
 4:45 pm | Teen Titans Go! | Cool Uncles
 5:00 pm | Teen Titans Go! | Flashback
 5:30 pm | Teen Titans Go! | Real Art
 5:45 pm | Teen Titans Go! | Don't Press Play
 6:00 pm | Amazing World of Gumball | The Gi; The Kiss
 6:30 pm | Amazing World of Gumball | The Party; The Refund
 7:00 pm | Amazing World of Gumball | The Robot; The Picnic
 7:30 pm | Amazing World of Gumball | The Goons; The Secret