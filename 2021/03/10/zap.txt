# 2021-03-10
 6:00 am | The Amazing World of Gumball: Darwin's Yearbook | Alan
 6:15 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 6:30 am | Amazing World of Gumball | The Society; The Spoiler
 7:00 am | Amazing World of Gumball | The Countdown; The Nobody
 7:30 am | Amazing World of Gumball | The Downer; The Egg
 8:00 am | Teen Titans Go! | Kabooms
 8:30 am | Teen Titans Go! | Demon Prom
 8:45 am | Teen Titans Go! | BBCYFSHIPBDAY
 9:00 am | Teen Titans Go! | Mo' Money Mo' Problems
 9:15 am | Teen Titans Go! | The Scoop
 9:30 am | Teen Titans Go! | Beast Girl
 9:45 am | Teen Titans Go! | Chicken in the Cradle
10:00 am | Craig of the Creek | Deep Creek Salvage
10:15 am | Craig of the Creek | Sparkle Cadet
10:30 am | Craig of the Creek | Dibs Court
10:45 am | Craig of the Creek | Stink Bomb
11:00 am | Victor and Valentino | Love at First Bite
11:15 am | Victor and Valentino | El Silbon
11:30 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
11:45 am | Teen Titans Go! | The Gold Standard
12:00 pm | Teen Titans Go! | The Groover
12:15 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
12:30 pm | Teen Titans Go! | The Power of Shrimps
12:45 pm | Teen Titans Go! | My Name Is Jose
 1:00 pm | Amazing World of Gumball | The Kids; The Fan
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Amazing World of Gumball | The Fraud; The Void
 3:30 pm | Amazing World of Gumball | The Boss; The Move
 4:00 pm | Craig of the Creek | Memories of Bobby
 4:15 pm | Craig of the Creek | The Evolution of Craig
 4:30 pm | Craig of the Creek | Jacob of the Creek
 4:45 pm | Craig of the Creek | Kelsey the Author
 5:00 pm | Total DramaRama | The Bad Guy Busters
 5:15 pm | Total DramaRama | Exercising the Demons
 5:30 pm | Total DramaRama | That's a Wrap
 5:45 pm | Total DramaRama | Pudding the Planet First
 6:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Amazing World of Gumball | The Butterfly; The Question
 7:30 pm | Apple & Onion | Bottle Catch
 7:45 pm | Apple & Onion | Positive Attitude Theory