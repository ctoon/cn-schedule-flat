# 2021-03-18
 6:00 am | Amazing World of Gumball | The Nemesis
 6:15 am | Amazing World of Gumball | The Advice
 6:30 am | Amazing World of Gumball | The Crew
 6:45 am | Amazing World of Gumball | The Signal
 7:00 am | Amazing World of Gumball | The Others
 7:15 am | Amazing World of Gumball | The Girlfriend
 7:30 am | Amazing World of Gumball | The Signature
 7:45 am | Amazing World of Gumball | The Parasite
 8:00 am | Teen Titans Go! | Super Hero Summer Camp
 9:00 am | Teen Titans Go! | Had to Be There
 9:15 am | Teen Titans Go! | The Great Disaster
 9:30 am | Teen Titans Go! | The Viewers Decide
 9:45 am | Teen Titans Go! | TV Knight 5
10:00 am | Craig of the Creek | Ancients of the Creek
10:15 am | Craig of the Creek | Trading Day
10:30 am | Craig of the Creek | The Other Side
11:00 am | Victor and Valentino | Escape From Bebe Bay
11:15 am | Victor and Valentino | I... am Vampier
11:30 am | Teen Titans Go! | How's This for a Special? Spaaaace: Pt. 1
12:00 pm | Teen Titans Go! | Business Ethics Wink Wink
12:15 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
12:30 pm | Teen Titans Go! | I Used to Be a Peoples
12:45 pm | Teen Titans Go! | Collect Them All
 1:00 pm | Amazing World of Gumball | The Wicked
 1:15 pm | Amazing World of Gumball | The Detective
 1:30 pm | Amazing World of Gumball | The Disaster
 1:45 pm | Amazing World of Gumball | The Re-Run
 2:00 pm | Amazing World of Gumball | The Fury
 2:15 pm | Amazing World of Gumball | The Compilation
 2:30 pm | Amazing World of Gumball | The Guy
 2:45 pm | Amazing World of Gumball | The Fuss
 3:00 pm | Amazing World of Gumball | The Boredom
 3:15 pm | Amazing World of Gumball | The News
 3:30 pm | Amazing World of Gumball | The Vase
 3:45 pm | Amazing World of Gumball | The Copycats
 4:00 pm | Craig of the Creek | The Mystery of the Timekeeper
 4:15 pm | Craig of the Creek | Cousin of the Creek
 4:30 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:45 pm | Craig of the Creek | Creek Daycare
 5:00 pm | Total DramaRama | A Licking Time Bomb
 5:15 pm | Total DramaRama | For a Few Duncans More
 5:30 pm | Total DramaRama | From Badge to Worse
 5:45 pm | Total DramaRama | He Who Wears the Clown
 6:00 pm | Teen Titans Go! | What's Opera, Titans?
 6:15 pm | Teen Titans Go! | Communicate Openly
 6:30 pm | Teen Titans Go! | Royal Jelly
 6:45 pm | Teen Titans Go! | Strength of a Grown Man
 7:00 pm | Amazing World of Gumball | The Triangle; The Money
 7:30 pm | Apple & Onion | Party Popper
 7:45 pm | Apple & Onion | Apple's Formula