# 2021-03-30
 8:00 pm | Bob's Burgers | Equestranauts
 8:30 pm | Bob's Burgers | Ambergris
 9:00 pm | American Dad | Meter Made
 9:30 pm | American Dad | Garbage Stan
10:00 pm | Rick and Morty | Something Ricked This Way Comes
10:30 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
11:00 pm | Family Guy | Something, Something, Something Dark Side Part 1 & 2
12:00 am | Momma Named Me Sheriff | The Case of the Sad, Sad Sheriff
12:15 am | Momma Named Me Sheriff | Neighborhood Knight Watch pt. 1
12:30 am | Robot Chicken | Endgame
12:45 am | Robot Chicken | Executed by the State
 1:00 am | Aqua TV Show Show | Merlo Sauvignon Blanco
 1:15 am | Aqua TV Show Show | Banana Planet
 1:30 am | Family Guy | Something, Something, Something Dark Side Part 1 & 2
 2:30 am | American Dad | Meter Made
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Momma Named Me Sheriff | The Case of the Sad, Sad Sheriff
 3:45 am | Momma Named Me Sheriff | Neighborhood Knight Watch pt. 1
 4:00 am | 12oz Mouse | Auraphull
 4:15 am | 12oz Mouse | Meat Warrior
 4:30 am | Robot Chicken | Endgame
 4:45 am | Robot Chicken | Executed by the State
 5:00 am | Bob's Burgers | Equestranauts
 5:30 am | Bob's Burgers | Ambergris