# 2021-03-01
 8:00 pm | Bob's Burgers | Synchronized Swimming
 8:30 pm | Bob's Burgers | Burgerboss
 9:00 pm | American Dad | News Glance with Genevieve Vavance
 9:30 pm | American Dad | The Longest Distance Relationship
10:00 pm | Rick and Morty | Raising Gazorpazorp
10:30 pm | Rick and Morty | Rixty Minutes
11:00 pm | Family Guy | Long John Peter
11:30 pm | Family Guy | Love Blactually
12:00 am | Lazor Wulf | Unoccupied Lane
12:15 am | Lazor Wulf | The End Is High
12:30 am | Eric Andre Show | Sinbad
12:45 am | Eric Andre Show | J-Mo
 1:00 am | Aqua Teen | Der Inflatable Fuhrer
 1:15 am | Aqua Teen | The Last Last One Forever and Ever
 1:30 am | Family Guy | Long John Peter
 2:00 am | Family Guy | Love Blactually
 2:30 am | American Dad | News Glance with Genevieve Vavance
 3:00 am | Rick and Morty | Raising Gazorpazorp
 3:30 am | Lazor Wulf | Unoccupied Lane
 3:45 am | Lazor Wulf | The End Is High
 4:00 am | Tim & Eric's Bedtime Stories | Roommates
 4:30 am | Eric Andre Show | Sinbad
 4:45 am | Eric Andre Show | J-Mo
 5:00 am | Bob's Burgers | Synchronized Swimming
 5:30 am | Bob's Burgers | Burgerboss