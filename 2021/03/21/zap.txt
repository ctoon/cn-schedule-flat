# 2021-03-21
 6:00 am | Teen Titans Go! | Record Book
 6:15 am | Teen Titans Go! | We're Off to Get Awards
 6:30 am | Bakugan: Armored Alliance | The Awesome Brawlers vs. Haavik; Their Fights
 7:00 am | Bakugan: Armored Alliance | Haavik's Final Show; Miracle Planet
 7:30 am | Teen Titans Go! | Bat Scouts
 7:45 am | Teen Titans Go! | Rain on Your Wedding Day
 8:00 am | Teen Titans Go! | Magic Man
 8:15 am | Teen Titans Go! | Egg Hunt
 8:30 am | Teen Titans Go! | The Titans Go Casual
 8:45 am | Teen Titans Go! | Real Art
 9:00 am | Teen Titans Go! | TV Knight 6
 9:15 am | Teen Titans Go! | Kryponite
 9:30 am | Teen Titans Go! | Thumb War
 9:45 am | Teen Titans Go! | The Cast
10:00 am | Craig of the Creek | The Invitation
10:15 am | Craig of the Creek | Big Pinchy
10:30 am | Craig of the Creek | Vulture's Nest
10:45 am | Craig of the Creek | The Kid From 3030
11:00 am | Craig of the Creek | Kelsey Quest
11:15 am | Craig of the Creek | Power Punchers
11:30 am | Craig of the Creek | JPony
11:45 am | Craig of the Creek | Creek Cart Racers
12:00 pm | Amazing World of Gumball | The Fraud; The Void
12:30 pm | Amazing World of Gumball | The Boss; The Move
 1:00 pm | Amazing World of Gumball | The Law; The Allergy
 1:30 pm | Amazing World of Gumball | The Mothers; The Password
 2:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:30 pm | Amazing World of Gumball | The Mirror; The Burden
 3:00 pm | Victor and Valentino | Lonely Haunts Club
 3:15 pm | Victor and Valentino | Los Cadejos
 3:30 pm | Victor and Valentino | The Dark Room
 3:45 pm | Victor and Valentino | Cuddle Monster
 4:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 5:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 5:30 pm | Teen Titans Go! | Toddler Titans...Yay!
 5:45 pm | Teen Titans Go! | Starfire the Terrible
 6:00 pm | Amazing World of Gumball | The Bros; The Man
 6:30 pm | Amazing World of Gumball | The Pizza; The Lie
 7:00 pm | Amazing World of Gumball | The Butterfly; The Question
 7:30 pm | Amazing World of Gumball | The Oracle; The Safety