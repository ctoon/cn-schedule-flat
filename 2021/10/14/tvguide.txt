# 2021-10-14
 6:00 am | Baby Looney Tunes | Mr. Mc Stuffles; Picture This!
 6:30 am | Baby Looney Tunes | A Brave Little Tweety; The Puddle Olympics Lot Like Lola
 7:05 am | Caillou | Little Bird; Thelights Outcaillou's Check Upcalling Dr. Caillou
 7:35 am | Caillou | Pumpkin Patch Kid; Caillou's Got Rhythm; Do It Myself; Caillou's Big Sale
 8:00 am | Pocoyo | Elly's Shoes; Duck Stuck; Scary Noises; Umbrella; Umbrella
 8:30 am | Pocoyo | Drum Roll Please Swept Away #104 Who's on the Phone?
 9:00 am | Thomas & Friends: All Engines Go | Rules of the Game; Music Is Everywhere
 9:30 am | Thomas & Friends: All Engines Go | Kana Goes Slow; Dragon Run
 9:55 am | Bing | Dressing up; Bake; Swing
10:25 am | Bing | Fireworks; Smoothie; Picnic; Frog
11:00 am | Pocoyo | Elly's Shoes; Duck Stuck; Scary Noises
11:30 am | Pocoyo | Drum Roll Please Swept Away #104 Who's on the Phone?
12:00 pm | Caillou | Little Bird; Thelights Outcaillou's Check Upcalling Dr. Caillou
12:30 pm | Caillou | Pumpkin Patch Kid; Caillou's Got Rhythm; Do It Myself; Caillou's Big Sale
 1:00 pm | Mush Mush and the Mushables | Race the Beast; Fool the Forest
 1:30 pm | Mush Mush and the Mushables | Bird Call; Who's Eating Mushton
 2:00 pm | Amazing World of Gumball | The One; The Parents
 2:30 pm | Amazing World of Gumball | The Vegging; The; Founder
 3:00 pm | Amazing World of Gumball | The Father; The; Schooling
 3:30 pm | Amazing World of Gumball | The Cringe; The; Intelligence
 4:00 pm | Craig of the Creek | Trick or Creek
 4:30 pm | Craig of the Creek | Rise and Fall of the Green Poncho; The Fan or Foe
 5:00 pm | Craig of the Creek | I Don't Need a Hat New Jersey
 5:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 6:00 pm | Teen Titans Go! | The Return of Slade; More of the Same
 6:30 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 7:00 pm | Apple & Onion | Drone Shoes; Falafel's Fun Day
 7:30 pm | Apple & Onion | Panamanian Night Monkey Keep It Fresh
 8:00 pm | Apple & Onion | Za; 4 on 1
 8:30 pm | Apple & Onion | Broccoli; Cousin's Day