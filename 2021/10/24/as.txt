# 2021-10-24
 9:00 pm | Bob's Burgers | Full Bars
 9:30 pm | Bob's Burgers | Fort Night
10:00 pm | American Dad | Poltergasm
10:30 pm | American Dad | The Witches of Langley
11:00 pm | Rick and Morty | Lawnmower Dog
11:30 pm | Rick and Morty | Look Who's Purging Now
12:00 am | Teenage Euthanasia | Nobody Beats the Baba
12:30 am | Tuca & Bertie | Sleepovers
 1:00 am | Robot Chicken | May Cause a Whole Lotta Scabs			
 1:15 am | Robot Chicken | May Cause Light Cannibalism			
 1:30 am | American Dad | Poltergasm
 2:00 am | American Dad | The Witches of Langley
 2:30 am | Teenage Euthanasia | Nobody Beats the Baba
 3:00 am | Rick and Morty | Lawnmower Dog
 3:30 am | Rick and Morty | Look Who's Purging Now
 4:00 am | Robot Chicken | May Cause a Whole Lotta Scabs			
 4:15 am | Robot Chicken | May Cause Light Cannibalism			
 4:30 am | Tuca & Bertie | Sleepovers
 5:00 am | Bob's Burgers | Full Bars
 5:30 am | Bob's Burgers | Fort Night