# 2021-10-12
 6:00 am | Baby Looney Tunes | The Harder They Fall
 6:15 am | Baby Looney Tunes | Business As Unusual
 6:30 am | Baby Looney Tunes | Sylvester the Pester
 6:41 am | Baby Looney Tunes | Cat-Taz-Trophy
 6:52 am | Baby Looney Tunes | Mine!
 7:05 am | Caillou | Sunday Brunch; Caillou to the Rescue; Caillou's Top Bunk; Recipe for Fun
 7:35 am | Caillou | New Kids on the Block; Caillou Goes to School; Caillou's Kitchen; Caillou's Sea Adventure
 8:00 am | Pocoyo | Guess What?
 8:07 am | Pocoyo | All for One
 8:14 am | Pocoyo | Band of Friends
 8:21 am | Pocoyo | Upside Down
 8:30 am | Pocoyo | The Messy Guest
 8:37 am | Pocoyo | New on the Planet
 8:44 am | Pocoyo | Mad Mix Machine
 8:51 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | Thomas' Promise
 9:15 am | Thomas & Friends: All Engines Go | License to Deliver
 9:30 am | Thomas & Friends: All Engines Go | Backwards Day
 9:42 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
 9:55 am | Bing | Fireworks
10:05 am | Bing | Balloon
10:15 am | Bing | Train
10:25 am | Bing | Bye Bye
10:33 am | Bing | Ducks
10:41 am | Bing | Atchoo!
10:49 am | Bing | Picnic
11:00 am | Pocoyo | Guess What?
11:10 am | Pocoyo | All for One
11:20 am | Pocoyo | Band of Friends
11:30 am | Pocoyo | The Messy Guest
11:40 am | Pocoyo | New on the Planet
11:50 am | Pocoyo | Mad Mix Machine
12:00 pm | Caillou | Sunday Brunch; Caillou to the Rescue; Caillou's Top Bunk; Recipe for Fun
12:30 pm | Caillou | New Kids on the Block; Caillou Goes to School; Caillou's Kitchen; Caillou's Sea Adventure
 1:00 pm | Mush-Mush and the Mushables | Mushpot's Treasure
 1:15 pm | Mush-Mush and the Mushables | Save the Fun Tree
 1:30 pm | Mush-Mush and the Mushables | Get the Egg Home
 1:45 pm | Mush-Mush and the Mushables | The Missing Mushlet
 2:00 pm | Amazing World of Gumball | The Weirdo
 2:15 pm | Amazing World of Gumball | The Ad
 2:30 pm | Amazing World of Gumball | The Line
 2:45 pm | Amazing World of Gumball | The Faith
 3:00 pm | Amazing World of Gumball | The Singing
 3:15 pm | Amazing World of Gumball | The Candidate
 3:30 pm | Amazing World of Gumball | The Puppets
 3:45 pm | Amazing World of Gumball | The Anybody
 4:00 pm | Craig of the Creek | The Other Side: The Tournament
 4:30 pm | Craig of the Creek | Pencil Break Mania
 4:45 pm | Craig of the Creek | Alternate Creekiverse
 5:00 pm | Craig of the Creek | The Last Game of Summer
 5:15 pm | Craig of the Creek | Snow Day
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 6:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 7:00 pm | Victor and Valentino | Miguelito the Mosquito
 7:15 pm | Victor and Valentino | Carmelita
 7:30 pm | Victor and Valentino | Kindred Spirits
 7:45 pm | Victor and Valentino | Los Perdidos
 8:00 pm | Victor and Valentino | Journey to Maiz Mountains, Part 1
 8:30 pm | Victor and Valentino | Decoding Valentino
 8:45 pm | Victor and Valentino | Folk Art Friends