# 2021-10-18
 6:00 am | Baby Looney Tunes | Wise Quacker; Yours, Mine...and Mine, Mine!
 6:30 am | Baby Looney Tunes | Pastime for Petunia; A Pouting Match Takers Keepers
 7:05 am | Caillou | Caillou's Teddy Shirtcaillou Meets Robbiepinata; Thecaillou's Promise
 7:35 am | Caillou | Caillou's Suitcasetry; Try Againnew Leaf; Thehappy New Year
 8:00 am | Pocoyo | Scooter Madness; Detective Pocoyo; Mystery Most Puzzling; a; Hush
 8:30 am | Pocoyo | Double Bubble; Key to It All; the; Keep Going Pocoyo
 9:00 am | Thomas & Friends: All Engines Go | Capture the Flag; Nia's Balloon Blunder
 9:30 am | Thomas & Friends: All Engines Go | Dragon Run; Rules of the Game
 9:55 am | Bing | Knack; Something for Sula; Say Goodbye
10:25 am | Bing | Parking Lot; Musical Statues; Blocks; Voo Voo
11:00 am | Pocoyo | A Scooter Madness; Detective Pocoyo; Mystery Most Puzzling
11:30 am | Pocoyo | Double Bubble; Key to It All; the; Keep Going Pocoyo
12:00 pm | Caillou | Caillou's Teddy Shirtcaillou Meets Robbiepinata; Thecaillou's Promise
12:30 pm | Caillou | Caillou's Suitcasetry; Try Againnew Leaf; Thehappy New Year
 1:00 pm | Mush Mush and the Mushables | Mush-Mush the Predictor; Get the Egg Home
 1:30 pm | Mush Mush and the Mushables | The Stuck in the Mud; Hero Trap
 2:00 pm | Amazing World of Gumball | The Ad; The Revolt
 2:30 pm | Amazing World of Gumball | The Slip; The Mess
 3:00 pm | Amazing World of Gumball | The Drama; The Possession
 3:30 pm | Amazing World of Gumball | The Buddy; The; Heart
 4:00 pm | Craig of the Creek | Under the Overpass; Big Pinchy
 4:30 pm | Craig of the Creek | Trick or Creek
 5:00 pm | Craig of the Creek | Vulture's Nest; Power Punchers
 5:30 pm | Teen Titans Go! | Croissant; Bbbday!
 6:00 pm | Teen Titans Go! | The Spice Game; Squash & Stretch
 6:30 pm | Teen Titans Go! | I'm the Sauce; Jam
 7:00 pm | Amazing World of Gumball | The Master; The Ghouls
 7:30 pm | Amazing World of Gumball | The Silence; The Bffs
 8:00 pm | Amazing World of Gumball | The Halloween Mirror
 8:30 pm | Amazing World of Gumball | The Decisions, The Web.