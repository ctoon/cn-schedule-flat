# 2021-10-18
 6:00 am | Baby Looney Tunes | Wise Quacker
 6:15 am | Baby Looney Tunes | Yours, Mine...and Mine, Mine!
 6:30 am | Baby Looney Tunes | A Pastime for Petunia
 6:41 am | Baby Looney Tunes | Pouting Match
 6:52 am | Baby Looney Tunes | Takers Keepers
 7:05 am | Caillou | Caillou's Teddy Shirt; Caillou Meets Robbie; The Pinata; Caillou's Promise
 7:35 am | Caillou | Caillou's Suitcase; Try, Try Again; The New Leaf; Happy New Year
 8:00 am | Pocoyo | Scooter Madness
 8:07 am | Pocoyo | Detective Pocoyo
 8:14 am | Pocoyo | A Mystery Most Puzzling
 8:21 am | Pocoyo | Hush
 8:30 am | Pocoyo | Double Bubble
 8:37 am | Pocoyo | The Key to It All
 8:44 am | Pocoyo | Keep Going Pocoyo
 8:51 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | Capture the Flag
 9:15 am | Thomas & Friends: All Engines Go | Nia's Balloon Blunder
 9:30 am | Thomas & Friends: All Engines Go | Dragon Run
 9:42 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:55 am | Bing | Knack
10:05 am | Bing | Something for Sula
10:15 am | Bing | Say Goodbye
10:25 am | Bing | Car Park
10:33 am | Bing | Musical Statues
10:41 am | Bing | Blocks
10:49 am | Bing | Voo Voo
11:00 am | Pocoyo | Scooter Madness
11:10 am | Pocoyo | Detective Pocoyo
11:20 am | Pocoyo | A Mystery Most Puzzling
11:30 am | Pocoyo | Double Bubble
11:40 am | Pocoyo | The Key to It All
11:50 am | Pocoyo | Keep Going Pocoyo
12:00 pm | Caillou | Caillou's Teddy Shirt; Caillou Meets Robbie; The Pinata; Caillou's Promise
12:30 pm | Caillou | Caillou's Suitcase; Try, Try Again; The New Leaf; Happy New Year
 1:00 pm | Mush-Mush and the Mushables | Mush-Mush the Predictor
 1:15 pm | Mush-Mush and the Mushables | Get the Egg Home
 1:30 pm | Mush-Mush and the Mushables | Stuck in the Mud
 1:45 pm | Mush-Mush and the Mushables | The Hero Trap
 2:00 pm | Amazing World of Gumball | The Ad
 2:15 pm | Amazing World of Gumball | The Revolt
 2:30 pm | Amazing World of Gumball | The Slip
 2:45 pm | Amazing World of Gumball | The Mess
 3:00 pm | Amazing World of Gumball | The Drama
 3:15 pm | Amazing World of Gumball | The Possession
 3:30 pm | Amazing World of Gumball | The Buddy
 3:45 pm | Amazing World of Gumball | The Heart
 4:00 pm | Craig of the Creek | Under the Overpass
 4:15 pm | Craig of the Creek | Big Pinchy
 4:30 pm | Craig of the Creek | Trick or Creek
 5:00 pm | Craig of the Creek | Vulture's Nest
 5:15 pm | Craig of the Creek | Power Punchers
 5:30 pm | Teen Titans Go! | The Croissant
 5:45 pm | Teen Titans Go! | BBBDay!
 6:00 pm | Teen Titans Go! | The Spice Game
 6:15 pm | Teen Titans Go! | Squash & Stretch
 6:30 pm | Teen Titans Go! | I'm the Sauce
 6:45 pm | Teen Titans Go! | Jam
 7:00 pm | Amazing World of Gumball | The Master
 7:15 pm | Amazing World of Gumball | The Ghouls
 7:30 pm | Amazing World of Gumball | The Silence
 7:45 pm | Amazing World of Gumball | The BFFS
 8:00 pm | Amazing World of Gumball | Halloween
 8:15 pm | Amazing World of Gumball | The Mirror
 8:30 pm | Amazing World of Gumball | The Decisions
 8:45 pm | Amazing World of Gumball | The Web