# 2021-10-23
 6:00 am | Baby Looney Tunes | Picture This!
 6:15 am | Baby Looney Tunes | Mr. McStuffles
 6:30 am | Baby Looney Tunes | Daffy Did It!
 6:45 am | Baby Looney Tunes | Pig Who Cried Wolf
 7:00 am | Lucas the Spider | Sliding
 7:10 am | Lucas the Spider | Lonesome Lucas
 7:20 am | Lucas the Spider | I Can't See You
 7:30 am | Esme & Roy | Hugo, We Have a Problem; A New Chapter
 8:00 am | Amazing World of Gumball | The Responsible; The Dress
 8:30 am | Amazing World of Gumball | The Laziest; The Ghost
 9:00 am | Teen Titans Go! | DC
 9:15 am | Teen Titans Go! | Jam
 9:30 am | Jellystone! | Catanooga Cheese Explosion
 9:45 am | Jellystone! | Squish or Miss
10:00 am | Total DramaRama | The Big Bangs Theory
10:15 am | Total DramaRama | Trousering Inferno
10:30 am | Victor and Valentino | Plan-De-Monium
10:45 am | Victor and Valentino | Patolli
11:00 am | Total DramaRama | Chews Wisely
11:15 am | Total DramaRama | A Dingo Ate My Duncan
11:30 am | Total DramaRama | Erase Yer Head
11:45 am | Total DramaRama | Teacher, Soldier, Chef, Spy
12:00 pm | Total DramaRama | Thingameroo
12:15 pm | Total DramaRama | CodE.T.
12:30 pm | Total DramaRama | Quiche It Goodbye
12:40 pm | Total DramaRama | Ice Guys Finish Last
12:50 pm | Total DramaRama | A Tall Tale
 1:00 pm | Total DramaRama | The Big Bangs Theory
 1:15 pm | Total DramaRama | Trousering Inferno
 1:30 pm | Teen Titans Go! | Manor and Mannerisms
 1:45 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 2:00 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 2:15 pm | Teen Titans Go! | EEbows
 2:30 pm | Teen Titans Go! | Batman's Birthday Gift
 2:45 pm | Teen Titans Go! | What a Boy Wonders
 3:00 pm | Teen Titans Go! | Doomsday Preppers
 3:15 pm | Teen Titans Go! | Fat Cats
 3:30 pm | Teen Titans Go! | DC
 3:40 pm | Teen Titans Go! | Jam
 3:50 pm | Teen Titans Go! | Creative Geniuses
 4:00 pm | Jellystone! | Must Be Jelly!
 4:15 pm | Jellystone! | Cats Do Dance
 4:30 pm | Jellystone! | VIP Baby You Know Me
 4:45 pm | Jellystone! | El Kabong's Kabong Is Gone
 5:00 pm | Jellystone! | Mr. Flabby Dabby Wabby Jabby
 5:15 pm | Jellystone! | Ice Ice Daddy
 5:30 pm | Jellystone! | DNA, A-OK!
 5:45 pm | Jellystone! | Face of the Town!
 6:00 pm | Jellystone! | Catanooga Cheese Explosion
 6:10 pm | Jellystone! | Squish or Miss
 6:20 pm | Jellystone! | 
 6:30 pm | Craig of the Creek | The Other Side: The Tournament
 7:00 pm | Craig of the Creek | The Ground Is Lava!
 7:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 7:30 pm | Craig of the Creek | The Bike Thief
 7:40 pm | Craig of the Creek | Craig of the Beach
 7:50 pm | Craig of the Creek | Brother Builder
 8:00 pm | Craig of the Creek | Plush Kingdom
 8:15 pm | Craig of the Creek | The Ice Pop Trio
 8:30 pm | Craig of the Creek | Pencil Break Mania
 8:45 pm | Craig of the Creek | The Last Game of Summer