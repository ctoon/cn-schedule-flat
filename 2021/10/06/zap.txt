# 2021-10-06
 6:00 am | Baby Looney Tunes | New Cat in Town
 6:15 am | Baby Looney Tunes | The Magic of Spring
 6:30 am | Baby Looney Tunes | Daffy Did It!
 6:41 am | Baby Looney Tunes | Pig Who Cried Wolf
 6:52 am | Baby Looney Tunes | School Daze
 7:05 am | Caillou | Three's a Crowd; Get Well Soon; Shadow Play; A New Member of the Family
 7:35 am | Caillou | Caillou and the Tooth Fairy; I Want to Grow Up; Caillou's Big Chill; Leo's Hamster
 8:00 am | Pocoyo | Duck Stuck
 8:07 am | Pocoyo | Elly's Shoes
 8:14 am | Pocoyo | Bat and Ball
 8:21 am | Pocoyo | Elly Spots
 8:30 am | Pocoyo | Up, up and Away
 8:40 am | Pocoyo | A Surprise for Pocoyo
 8:50 am | Pocoyo | Having a Ball
 9:00 am | Thomas & Friends: All Engines Go | Chasing Rainbows
 9:15 am | Thomas & Friends: All Engines Go | Thomas' Promise
 9:30 am | Thomas & Friends: All Engines Go | A Quiet Delivery
 9:42 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
 9:55 am | Bing | Dressing Up
10:05 am | Bing | Smoothie
10:15 am | Bing | Bake
10:25 am | Bing | Fireworks
10:33 am | Bing | Swing
10:41 am | Bing | Voo Voo
10:49 am | Bing | Growing
11:00 am | Pocoyo | Duck Stuck
11:07 am | Pocoyo | Elly's Shoes
11:14 am | Pocoyo | Bat and Ball
11:21 am | Pocoyo | 
11:30 am | Pocoyo | Up, up and Away
11:40 am | Pocoyo | A Surprise for Pocoyo
11:50 am | Pocoyo | Having a Ball
12:00 pm | Caillou | Three's a Crowd; Get Well Soon; Shadow Play; A New Member of the Family
12:30 pm | Caillou | Caillou and the Tooth Fairy; I Want to Grow Up; Caillou's Big Chill; Leo's Hamster
 1:00 pm | Mush-Mush and the Mushables | Mushpot's Treasure
 1:15 pm | Mush-Mush and the Mushables | The Hero Trap
 1:30 pm | Mush-Mush and the Mushables | Let it Bee
 1:40 pm | Mush-Mush and the Mushables | Oh My Compost
 1:50 pm | Mush-Mush and the Mushables | 
 2:00 pm | Amazing World of Gumball | The Watch; The Bet
 2:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 3:00 pm | Amazing World of Gumball | The Pony; The Storm
 3:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 4:00 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 4:15 pm | Craig of the Creek | Capture the Flag Part 2: The King
 4:30 pm | Craig of the Creek | Capture the Flag Part 3: The Legend
 4:45 pm | Craig of the Creek | Capture the Flag Part 4: The Plan
 5:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 5:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 6:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:15 pm | Teen Titans Go! | Cy & Beasty
 6:30 pm | Teen Titans Go! | Double Trouble; The Date
 7:00 pm | We Bare Bears | Bear Flu
 7:15 pm | We Bare Bears | Baby Bears on a Plane
 7:30 pm | We Bare Bears | Chicken and Waffles
 7:45 pm | We Bare Bears | Yuri and the Bear
 8:00 pm | We Bare Bears | The Audition
 8:15 pm | We Bare Bears | Icy Nights
 8:30 pm | We Bare Bears | Captain Craboo