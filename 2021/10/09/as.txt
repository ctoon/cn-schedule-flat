# 2021-10-09
 9:00 pm | Final Space | The Devil's Den
 9:30 pm | Final Space | … and Into the Fire
10:00 pm | American Dad | Casino Normale
10:30 pm | American Dad | Bazooka Steve
11:00 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
11:30 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
12:00 am | Fena: Pirate Princess | The Curtain Rises on the Climax
12:30 am | My Hero Academia | Tenko Shimura: Origin
 1:00 am | Yashahime: Princess Half-Demon | Farewell Under the Lunar Eclipse
 1:30 am | Food Wars! | Your Best Side
 2:00 am | Black Clover | The Devil-Binding Ritual
 2:30 am | Black Clover | The Faraway Future
 3:00 am | Dr. Stone | Prison Break
 3:30 am | Dragon Ball Super | A Run-Through For the Competition! Who Are the Last Two Members?!
 4:00 am | Black Dynamite | A "Apocalypse, This!" or "For the Pity of Fools" a/k/"Flashbacks Are Forever"
 4:30 am | Black Dynamite | "Honky Kong" or "White Apes Can't Hump"
 5:00 am | Home Movies | The Party
 5:30 am | Home Movies | Impressions