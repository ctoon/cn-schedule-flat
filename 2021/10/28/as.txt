# 2021-10-28
 9:00 pm | Bob's Burgers | Full Bars
 9:30 pm | Bob's Burgers | Fort Night
10:00 pm | American Dad | Best Little Horror House in Langley Falls
10:30 pm | American Dad | Hot Water
11:00 pm | American Dad | Death by Dinner Party
11:30 pm | Rick and Morty | The Rickshank Rickdemption
12:00 am | Robot Chicken | The Robot Chicken Walking Dead Special: Look Who's Walking
12:30 am | Teenage Euthanasia | Nobody Beats the Baba
 1:00 am | The Venture Brothers | Very Venture Halloween
 1:30 am | Rick and Morty | The Wedding Squanchers
 2:00 am | Rick and Morty | The Rickshank Rickdemption
 2:30 am | Robot Chicken | The Robot Chicken Walking Dead Special: Look Who's Walking
 3:00 am | Teenage Euthanasia | Nobody Beats the Baba
 3:30 am | The Venture Brothers | Very Venture Halloween
 4:00 am | Tim & Eric's Bedtime Stories | The Demotion
 4:15 am | Tim & Eric's Bedtime Stories | Squat
 4:30 am | Home Movies | Broken Dreams
 5:00 am | Bob's Burgers | Full Bars
 5:30 am | Bob's Burgers | Fort Night