# 2021-10-28
 6:00 am | Baby Looney Tunes | War of the Weirds
 6:15 am | Baby Looney Tunes | The Harder They Fall
 6:30 am | Baby Looney Tunes | Business As Unusual
 6:41 am | Baby Looney Tunes | Mr. McStuffles
 6:52 am | Baby Looney Tunes | Picture This!
 7:05 am | Caillou | Surprise Party; Caillou's Bad Dream; Caillou Computes; Caillou's Big Discovery
 7:35 am | Caillou | Pumpkin Patch Kid; Caillou's Got Rhythm; Do It Myself; Caillou's Big Sale
 8:00 am | Pocoyo | Band of Friends
 8:07 am | Pocoyo | Upside Down
 8:14 am | Pocoyo | Mad Mix Machine
 8:21 am | Pocoyo | The Messy Guest
 8:30 am | Pocoyo | New on the Planet
 8:40 am | Pocoyo | Pocoyo's Present
 8:50 am | Pocoyo | Elly's Ballet Class
 9:00 am | Thomas & Friends: All Engines Go | Music Is Everywhere
 9:15 am | Thomas & Friends: All Engines Go | Backwards Day
 9:30 am | Thomas & Friends: All Engines Go | Chasing Rainbows
 9:42 am | Thomas & Friends: All Engines Go | Nia's Balloon Blunder
 9:55 am | Bing | Knack
10:05 am | Bing | Growing
10:15 am | Bing | Something for Sula
10:25 am | Bing | Fireworks
10:33 am | Bing | Bye Bye
10:41 am | Bing | Blocks
10:49 am | Bing | Ducks
11:00 am | Pocoyo | Band of Friends
11:10 am | Pocoyo | Upside Down
11:20 am | Pocoyo | Mad Mix Machine
11:30 am | Pocoyo | New on the Planet
11:40 am | Pocoyo | Pocoyo's Present
11:50 am | Pocoyo | Elly's Ballet Class
12:00 pm | Caillou | Surprise Party; Caillou's Bad Dream; Caillou Computes; Caillou's Big Discovery
12:30 pm | Caillou | Pumpkin Patch Kid; Caillou's Got Rhythm; Do It Myself; Caillou's Big Sale
 1:00 pm | Mush-Mush and the Mushables | Shine, Lilit, Shine
 1:15 pm | Mush-Mush and the Mushables | Save the Fun Tree
 1:30 pm | Mush-Mush and the Mushables | Mushpot's Treasure
 1:45 pm | Mush-Mush and the Mushables | Mushroomates
 2:00 pm | We Bare Bears | Charlie
 2:15 pm | We Bare Bears | Brother Up
 2:30 pm | We Bare Bears | Charlie and the Snake
 2:45 pm | We Bare Bears | Charlie Ball
 3:00 pm | We Bare Bears | Chicken and Waffles
 3:15 pm | We Bare Bears | Creature Mysteries
 3:30 pm | We Bare Bears | Ralph
 3:45 pm | We Bare Bears | Charlie's Big Foot
 4:00 pm | We Bare Bears | Panda's Art
 4:15 pm | We Bare Bears | Private Lake
 4:30 pm | We Bare Bears | Ice Cave
 4:45 pm | We Bare Bears | Hurricane Hal
 5:00 pm | We Bare Bears | More Everyone's Tube
 5:15 pm | We Bare Bears | Rescue Ranger
 5:30 pm | We Bare Bears | El Oso
 5:45 pm | We Bare Bears | Bubble
 6:00 pm | We Bare Bears | Tabes & Charlie
 6:15 pm | We Bare Bears | Charlie's Halloween Thing
 6:30 pm | We Bare Bears | Charlie's Halloween Thing 2
 7:00 pm | Apple & Onion | Eyesore a Sunset
 7:15 pm | Apple & Onion | The Eater
 7:30 pm | Apple & Onion | Apple's in Trouble
 7:45 pm | Apple & Onion | One Hit Wonder
 8:00 pm | Apple & Onion | A Matter of Pride
 8:15 pm | Apple & Onion | Nothing Can Stop Us
 8:30 pm | Apple & Onion | Eyesore a Sunset
 8:45 pm | Apple & Onion | The Eater