# 2021-10-05
 6:00 am | Baby Looney Tunes | Tea and Basketball
 6:15 am | Baby Looney Tunes | Taz You Like It
 6:30 am | Baby Looney Tunes | A Secret Tweet
 6:41 am | Baby Looney Tunes | Comfort Level
 6:52 am | Baby Looney Tunes | Like a Duck to Water
 7:05 am | Caillou | Star Light, Star Bright; All in a Day's Work; The Cat's Meow; Caillou in Space
 7:35 am | Caillou | The Treasure Chest; A Camping We Will Go; Chopsticks; A Special Dog
 8:00 am | Pocoyo | Mystery Footprints
 8:07 am | Pocoyo | Magical Watering Can
 8:14 am | Pocoyo | Table for Fun
 8:21 am | Pocoyo | Twinkle Twinkle
 8:30 am | Pocoyo | Hiccup
 8:37 am | Pocoyo | Pato's Postal Service
 8:44 am | Pocoyo | Puppy Love
 8:51 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | Kana Goes Slow
 9:15 am | Thomas & Friends: All Engines Go | Counting Cows
 9:30 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:42 am | Thomas & Friends: All Engines Go | Percy's Lucky Bell
 9:55 am | Bing | Car Park
10:05 am | Bing | Here I Go
10:15 am | Bing | Frog
10:25 am | Bing | Shadow
10:33 am | Bing | Picnic
10:41 am | Bing | Blocks
10:49 am | Bing | Musical Statues
11:00 am | Pocoyo | Mystery Footprints
11:10 am | Pocoyo | Magical Watering Can
11:20 am | Pocoyo | Table for Fun
11:30 am | Pocoyo | Hiccup
11:37 am | Pocoyo | Pato's Postal Service
11:44 am | Pocoyo | Puppy Love
11:51 am | Pocoyo | 
12:00 pm | Caillou | Star Light, Star Bright; All in a Day's Work; The Cat's Meow; Caillou in Space
12:30 pm | Caillou | The Treasure Chest; A Camping We Will Go; Chopsticks; A Special Dog
 1:00 pm | Mush-Mush and the Mushables | Grasshopper Chep
 1:15 pm | Mush-Mush and the Mushables | Mushroomates
 1:30 pm | Mush-Mush and the Mushables | Mushlers to the Rescue
 1:45 pm | Mush-Mush and the Mushables | Fly, Chase, Fly
 2:00 pm | Amazing World of Gumball | The Awkwardness
 2:15 pm | Amazing World of Gumball | The Code
 2:30 pm | Amazing World of Gumball | The Nest
 2:45 pm | Amazing World of Gumball | The Test
 3:00 pm | Amazing World of Gumball | The Points
 3:15 pm | Amazing World of Gumball | The Slide
 3:30 pm | Amazing World of Gumball | The Bus
 3:45 pm | Amazing World of Gumball | The Loophole
 4:00 pm | Craig of the Creek | Ancients of the Creek
 4:15 pm | Craig of the Creek | Snow Place Like Home
 4:30 pm | Craig of the Creek | The Haunted Dollhouse
 4:45 pm | Craig of the Creek | Winter Creeklympics
 5:00 pm | Craig of the Creek | Mortimor to the Rescue
 5:15 pm | Craig of the Creek | Crisis at Elder Rock
 5:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 6:00 pm | Teen Titans Go! | Caramel Apples; Halloween
 6:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 7:00 pm | Victor and Valentino | The Bodyguard
 7:15 pm | Victor and Valentino | The Guest
 7:30 pm | Victor and Valentino | Victor the Predictor
 7:45 pm | Victor and Valentino | My Fair Achi
 8:00 pm | Victor and Valentino | I... am Vampier
 8:15 pm | Victor and Valentino | Oddities
 8:30 pm | Victor and Valentino | Lonely Haunts Club 3: La Llorona
 8:45 pm | Victor and Valentino | Lords of Ghost Town