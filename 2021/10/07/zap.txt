# 2021-10-07
 6:00 am | Baby Looney Tunes | Band Together
 6:15 am | Baby Looney Tunes | War of the Weirds
 6:30 am | Baby Looney Tunes | Things That Go Bugs in the Night
 6:41 am | Baby Looney Tunes | The Creature From The Chocolate Chip
 6:52 am | Baby Looney Tunes | Card Bored Box
 7:05 am | Caillou | Caillou Beats the Heat; Back Seat Driver; Lost and Found; Holiday Magic
 7:35 am | Caillou | Downhill from Here; Next Stop Fun; Under Sail; Farmer Caillou
 7:47 am | Caillou | 
 8:00 am | Pocoyo | Super Pocoyo
 8:07 am | Pocoyo | Let's Go Camping
 8:14 am | Pocoyo | Pocoyo Pocoyo
 8:21 am | Pocoyo | Elly's Big Chase
 8:30 am | Pocoyo | Picture This
 8:40 am | Pocoyo | Whale's Birthday
 8:50 am | Pocoyo | Pocoyo's Little Friend
 9:00 am | Thomas & Friends: All Engines Go | License to Deliver
 9:15 am | Thomas & Friends: All Engines Go | The Biggest Adventure Club
 9:30 am | Thomas & Friends: All Engines Go | Sandy's Sandy Shipment
 9:42 am | Thomas & Friends: All Engines Go | Backwards Day
 9:55 am | Bing | Hide and Seek
10:05 am | Bing | Lost
10:15 am | Bing | Ducks
10:25 am | Bing | Atchoo!
10:33 am | Bing | Bye Bye
10:41 am | Bing | Car Park
10:49 am | Bing | Frog
11:00 am | Pocoyo | Super Pocoyo
11:10 am | Pocoyo | Let's Go Camping
11:20 am | Pocoyo | Pocoyo Pocoyo
11:30 am | Pocoyo | Picture This
11:37 am | Pocoyo | Whale's Birthday
11:44 am | Pocoyo | 
11:51 am | Pocoyo | 
12:00 pm | Caillou | Caillou Beats the Heat; Back Seat Driver; Lost and Found; Holiday Magic
12:30 pm | Caillou | Downhill from Here; Next Stop Fun; Under Sail; Farmer Caillou
 1:00 pm | Mush-Mush and the Mushables | The Snake Dance
 1:15 pm | Mush-Mush and the Mushables | The Missing Mushlet
 1:30 pm | Mush-Mush and the Mushables | Stuck in the Mud
 1:45 pm | Mush-Mush and the Mushables | The Staff of Wisdom
 2:00 pm | Amazing World of Gumball | The Night
 2:15 pm | Amazing World of Gumball | The Fuss
 2:30 pm | Amazing World of Gumball | The Misunderstandings
 2:45 pm | Amazing World of Gumball | The News
 3:00 pm | Amazing World of Gumball | The Roots
 3:15 pm | Amazing World of Gumball | The Vase
 3:30 pm | Amazing World of Gumball | The Blame
 3:45 pm | Amazing World of Gumball | The Ollie
 4:00 pm | Craig of the Creek | Secret in a Bottle
 4:15 pm | Craig of the Creek | Kelsey the Worthy
 4:30 pm | Craig of the Creek | Trading Day
 4:45 pm | Craig of the Creek | The End Was Here
 5:00 pm | Craig of the Creek | The Children of the Dog
 5:15 pm | Craig of the Creek | Fan or Foe
 5:30 pm | Teen Titans Go! | Witches Brew
 5:45 pm | Teen Titans Go! | Ghost With the Most
 6:00 pm | Teen Titans Go! | The Mask; Slumber Party
 6:30 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
 7:00 pm | Apple & Onion | Slobbery
 7:15 pm | Apple & Onion | Microwave's Dance Club
 7:30 pm | Apple & Onion | The Eater
 7:45 pm | Apple & Onion | All Work and No Play
 8:00 pm | Apple & Onion | Ferekh
 8:15 pm | Apple & Onion | Pat on the Head
 8:30 pm | Apple & Onion | Falafel's Glory
 8:45 pm | Apple & Onion | The Perfect Team