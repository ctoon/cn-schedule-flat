# 2021-09-03
 6:00 am | Amazing World of Gumball | The Vegging
 6:15 am | Amazing World of Gumball | The Father
 6:30 am | Amazing World of Gumball | The Puppets
 6:45 am | Amazing World of Gumball | The Upgrade
 7:00 am | Amazing World of Gumball | The Line
 7:15 am | Amazing World of Gumball | The Singing
 7:30 am | Amazing World of Gumball | The Stink
 7:45 am | Amazing World of Gumball | The Awareness
 8:00 am | The Fungies! | What About Cool James?
 8:15 am | The Fungies! | Dino Club
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 8:45 am | Teen Titans Go! | Labor Day
 9:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 9:30 am | Teen Titans Go! | Video Game References; Cool School
10:00 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
10:15 am | Craig of the Creek | The Bike Thief
10:30 am | Craig of the Creek | Craig of the Beach
10:45 am | Craig of the Creek | Body Swap
11:00 am | Craig of the Creek | The Ice Pop Trio
11:15 am | Craig of the Creek | Pencil Break Mania
11:30 am | Craig of the Creek | The Last Game of Summer
11:45 am | Craig of the Creek | Fall Anthology
12:00 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
12:15 pm | Lucas the Spider | 
12:30 pm | Teen Titans Go! | Operation Tin Man; Nean
12:45 pm | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
 1:00 pm | Amazing World of Gumball | The Shippening
 1:15 pm | Amazing World of Gumball | The Brain
 1:30 pm | Amazing World of Gumball | The Candidate
 1:45 pm | Amazing World of Gumball | The Anybody
 2:00 pm | Amazing World of Gumball | The Pact
 2:15 pm | Amazing World of Gumball | The Faith
 2:30 pm | Amazing World of Gumball | The Cringe
 2:45 pm | Amazing World of Gumball | The Comic
 3:00 pm | Craig of the Creek | Afterschool Snackdown
 3:15 pm | Craig of the Creek | Creature Feature
 3:30 pm | Craig of the Creek | King of Camping
 3:45 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:00 pm | Teen Titans Go! | Career Day
 4:15 pm | Teen Titans Go! | Permanent Record
 4:30 pm | Teen Titans Go! | Quantum Fun
 4:45 pm | Teen Titans Go! | The Fight
 5:00 pm | Teen Titans Go! | The Groover
 5:15 pm | Teen Titans Go! | Genie President
 5:30 pm | Teen Titans Go! | The Metric System vs. Freedom
 5:45 pm | Teen Titans Go! | Labor Day
 6:00 pm | Teen Titans Go! | Books
 6:15 pm | Teen Titans Go! | Driver's Ed
 6:30 pm | Teen Titans Go! | Yearbook Madness
 6:45 pm | Teen Titans Go! | History Lesson
 7:00 pm | Teen Titans Go! | Bat Scouts
 7:15 pm | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
 7:30 pm | Tig N' Seek | Hair Today
 7:45 pm | Tig N' Seek | Who's This Guy?