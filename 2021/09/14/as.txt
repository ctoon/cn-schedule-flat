# 2021-09-14
 8:00 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 8:30 pm | Bob's Burgers | Aquaticism
 9:00 pm | American Dad | The Adventures of Twill Ongenbone and His Boy Jabari
 9:30 pm | American Dad | Blood Crieth Unto Heaven
10:00 pm | American Dad | Max Jets
10:30 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
11:00 pm | Family Guy | He's Bla-ack!
11:30 pm | Family Guy | Chap Stewie
12:00 am | Robot Chicken | May Cause Random Wolf Attacks			
12:15 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
12:30 am | Aqua Teen | The Granite Family
12:45 am | Aqua Teen | Bookie
 1:00 am | The Venture Brothers | Faking Miracles
 1:30 am | Family Guy | He's Bla-ack!
 2:00 am | Family Guy | Chap Stewie
 2:30 am | Bob's Burgers | The Grand Mama-Pest Hotel
 3:00 am | Bob's Burgers | Aquaticism
 3:30 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 4:00 am | Tender Touches | Microecosystems
 4:15 am | Tender Touches | Rock and Roll Guys
 4:30 am | Robot Chicken | May Cause Random Wolf Attacks			
 4:45 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
 5:00 am | Naruto:Shippuden | Jinchuriki vs. Jinchuriki!!
 5:30 am | Samurai Jack | XXXIV