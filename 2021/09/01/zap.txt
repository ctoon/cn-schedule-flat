# 2021-09-01
 6:00 am | Amazing World of Gumball | The Origins
 6:15 am | Amazing World of Gumball | The Origins
 6:30 am | Amazing World of Gumball | The Signature
 6:45 am | Amazing World of Gumball | The Others
 7:00 am | Amazing World of Gumball | The Gift
 7:15 am | Amazing World of Gumball | The Traitor
 7:30 am | Amazing World of Gumball | The Apprentice
 7:45 am | Amazing World of Gumball | The Advice
 8:00 am | Teen Titans Go! | No Power; Sidekick
 8:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 9:00 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:30 am | Teen Titans Go! | Waffles; Opposites
10:00 am | Craig of the Creek | The Invitation
10:15 am | Lucas the Spider | Fun With Findley: Sneak Peek
10:30 am | Craig of the Creek | Vulture's Nest
10:45 am | Craig of the Creek | The Shortcut
11:00 am | Craig of the Creek | Kelsey Quest
11:15 am | Craig of the Creek | Dibs Court
11:30 am | Craig of the Creek | JPony
11:45 am | Craig of the Creek | The Great Fossil Rush
12:00 pm | Teen Titans Go! | Dreams; Grandma Voice
12:30 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 1:00 pm | Amazing World of Gumball | The Routine
 1:15 pm | Amazing World of Gumball | The Points
 1:30 pm | Amazing World of Gumball | The Parking
 1:45 pm | Amazing World of Gumball | The Awkwardness
 2:00 pm | Amazing World of Gumball | The Upgrade
 2:15 pm | Amazing World of Gumball | The Nest
 2:30 pm | Amazing World of Gumball | The Comic
 2:45 pm | Amazing World of Gumball | The Bus
 3:00 pm | Craig of the Creek | Monster in the Garden
 3:15 pm | Craig of the Creek | Power Punchers
 3:30 pm | Craig of the Creek | The Curse
 3:45 pm | Craig of the Creek | Creek Cart Racers
 4:00 pm | Craig of the Creek | Dog Decider
 4:15 pm | Craig of the Creek | Secret Book Club
 4:30 pm | Craig of the Creek | Bring Out Your Beast
 4:45 pm | Craig of the Creek | Jextra Perrestrial
 5:00 pm | Total DramaRama | Harold Swatter and the Goblet of Flies
 5:15 pm | Pocoyo | 
 5:30 pm | Total DramaRama | Stink. Stank. Stunk
 5:45 pm | Total DramaRama | Supply Mom
 6:00 pm | Teen Titans Go! | Mr. Butt; Man Person
 6:30 pm | Teen Titans Go! | Pirates; I See You
 7:00 pm | Amazing World of Gumball | The Check
 7:15 pm | Amazing World of Gumball | The Signal
 7:30 pm | Apple & Onion | Pulling Your Weight
 7:45 pm | Apple & Onion | Whale Spotting