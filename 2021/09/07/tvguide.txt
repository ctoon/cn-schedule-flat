# 2021-09-07
 6:00 am | Amazing World Of Gumball | The Buddy; The Potion
 6:30 am | Amazing World of Gumball | The Spinoffs; The Master
 7:00 am | Amazing World of Gumball | The Transformation; The Silence
 7:30 am | Amazing World of Gumball | The Future; The Heart
 8:00 am | Teen Titans Go! | "Night Begins to Shine" Special
 9:00 am | Teen Titans Go! | Throne of Bones; The Academy
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:00 am | Craig of the Creek | Return of the Honeysuckle Rangers; Council of the Creek
10:30 am | Craig of the Creek | Fort Williams; Creek Daycare
11:00 am | Craig of the Creek | Kelsey the Elder; Sugar Smugglers
11:30 am | Craig of the Creek | Sour Candy Trials; Sleepover at Jp's
12:00 pm | Teen Titans Go! | Beast Girl; Quantum Fun
12:30 pm | Teen Titans Go! | The Fight; Tv Knight 3
 1:00 pm | Amazing World of Gumball | Darwin's Yearbook - Banana Joe - Darwin's Yearbook - Clayton
 1:30 pm | Amazing World of Gumball | Darwin's Yearbook - Carrie Darwin's Yearbook - Alan
 2:00 pm | Amazing World of Gumball | Darwin's Yearbook - Sarah Darwin's Yearbook - Teachers
 2:30 pm | Amazing World of Gumball | The Third; The Debt
 3:00 pm | Craig of the Creek | Ancients of the Creek Crisis at Elder Rock
 3:30 pm | Craig of the Creek | Mortimor to the Rescue Kelsey the Worthy
 4:00 pm | Craig of the Creek | The Secret in a Bottle End Was Here
 4:30 pm | Craig of the Creek | The Trading Day Children of the Dog
 5:00 pm | Star Wars: The Empire Strikes Back | Tu Ba or Not Tu Ba