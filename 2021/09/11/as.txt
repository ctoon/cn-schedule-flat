# 2021-09-11
 8:00 pm | American Dad | Killer Vacation
 8:30 pm | American Dad | Can I Be Frank (With You)
 9:00 pm | American Dad | American Stepdad
 9:30 pm | Family Guy | Fresh Heir
10:00 pm | Family Guy | Secondhand Spoke
10:30 pm | Family Guy | Herpe the Love Sore
11:00 pm | Rick and Morty | Forgetting Sarick Mortshall
11:30 pm | Rick and Morty | Rickmurai Jack
12:00 am | Fena: Pirate Princess | Mutiny on the Blue Giant
12:30 am | My Hero Academia | More of a Hero Than Anyone
 1:00 am | Yashahime: Princess Half-Demon | Curse of the Man-Eating Pond
 1:30 am | Food Wars! | Aim for Victory!
 2:00 am | Black Clover | Water Crusade
 2:30 am | Naruto:Shippuden | Kakashi: Shadow of the ANBU Black Ops - An Uchiha ANBU
 2:55 am | Iron Maiden: Stratego | Iron Maiden: Stratego
 3:00 am | Dr. Stone | Hot Line
 3:30 am | Dragon Ball Super | A Miraculous Conclusion! Farewell Goku! Until We Meet Again!
 4:00 am | Black Dynamite | "The Wizard of Watts" or "Oz Ain't Got S&@# on the Wiz"
 5:00 am | Home Movies | I Don't Do Well in Parent-Teacher Conferences
 5:30 am | Home Movies | The Art of the Sucker Punch