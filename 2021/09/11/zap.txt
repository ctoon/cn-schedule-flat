# 2021-09-11
 6:00 am | Amazing World of Gumball | The Coach; The Joy
 6:30 am | Amazing World of Gumball | The Recipe; The Puppy
 7:00 am | Amazing World of Gumball | The Name; The Extras
 7:30 am | Amazing World of Gumball | The Gripes; The Vacation
 7:45 am | Amazing World of Gumball | The Founder
 8:00 am | Amazing World of Gumball | The Fraud; The Void
 8:30 am | Amazing World of Gumball | The Boss; The Move
 9:00 am | Teen Titans Go! | EEbows
 9:15 am | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 9:30 am | Jellystone! | Boo Boots
 9:45 am | Jellystone! | My Doggie Dave
10:00 am | Total DramaRama | Teacher, Soldier, Chef, Spy
10:15 am | Total DramaRama | Erase Yer Head
10:30 am | Victor and Valentino | The Fog
10:45 am | Victor and Valentino | Tez Breaks Bread
11:00 am | Amazing World of Gumball | The Law; The Allergy
11:30 am | Amazing World of Gumball | The Mothers; The Password
12:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
12:30 pm | Amazing World of Gumball | The Mirror; The Burden
 1:00 pm | Craig of the Creek | The Invitation
 1:15 pm | Craig of the Creek | Power Punchers
 1:30 pm | Craig of the Creek | Vulture's Nest
 1:45 pm | Craig of the Creek | Creek Cart Racers
 2:00 pm | Craig of the Creek | Kelsey Quest
 2:15 pm | Craig of the Creek | Secret Book Club
 2:30 pm | Craig of the Creek | JPony
 2:45 pm | Craig of the Creek | Jextra Perrestrial
 3:00 pm | Teen Titans Go! | Them Soviet Boys
 3:15 pm | Teen Titans Go! | What's Opera, Titans?
 3:30 pm | Teen Titans Go! | Teen Titans Vroom
 4:00 pm | Teen Titans Go! | Stockton, CA!
 4:15 pm | Teen Titans Go! | Lil' Dimples
 4:30 pm | Teen Titans Go! | Collect Them All
 4:45 pm | Teen Titans Go! | Royal Jelly
 5:00 pm | Teen Titans Go! | Cartoon Feud
 5:15 pm | Teen Titans Go! | Strength of a Grown Man
 5:30 pm | Teen Titans Go! | Don't Be an Icarus
 5:45 pm | Teen Titans Go! | Had to Be There
 6:00 pm | Amazing World of Gumball | The Bros; The Man
 6:30 pm | Amazing World of Gumball | The Pizza; The Lie
 7:00 pm | Amazing World of Gumball | The Butterfly; The Question
 7:30 pm | Amazing World of Gumball | The Oracle; The Safety