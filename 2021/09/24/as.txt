# 2021-09-24
 8:00 pm | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 8:15 pm | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 8:30 pm | Joe Pera Talks With You | Joe Pera Has a Surprise for You
 8:45 pm | Joe Pera Talks With You | Joe Pera Helps You Write
 9:00 pm | American Dad | Now and Gwen
 9:30 pm | American Dad | LGBSteve
10:00 pm | American Dad | Morning Mimosa
10:30 pm | American Dad | My Affair Lady
11:00 pm | Rick and Morty | Amortycan Grickfitti
11:30 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
12:00 am | Brak Show | Goldfish
12:15 am | Brak Show | Time Machine
12:35 am | Brak Show | Psychoklahoma
12:50 am | Puberty | Puberty
 1:05 am | Brak Show | President Dad
 1:20 am | Brak Show | Brak Street
 1:40 am | Brak Show | The Runaway
 2:00 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 2:15 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 2:30 am | Joe Pera Talks With You | Joe Pera Has a Surprise for You
 2:45 am | Joe Pera Talks With You | Joe Pera Helps You Write
 3:00 am | Rick and Morty | Amortycan Grickfitti
 3:30 am | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
 4:00 am | Tender Touches | Rock and Roll Guys
 4:15 am | Tender Touches | The End of the World Concert
 4:30 am | Infomercials | For-Profit Online University
 4:45 am | Infomercials | Live Forever as You Are Now with Alan Resnick
 5:00 am | Naruto:Shippuden | The Risks of the Reanimation Jutsu
 5:30 am | Samurai Jack | XLII