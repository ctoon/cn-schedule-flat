# 2021-09-15
 6:00 am | Baby Looney Tunes | Mine!
 6:15 am | Baby Looney Tunes | Sylvester the Pester
 6:30 am | Baby Looney Tunes | Cat-Taz-Trophy
 6:45 am | Baby Looney Tunes | The Brave Little Tweety
 7:00 am | Caillou | Caillou's Big Slide; Caillou Walks a Dog; Caillou Watches Rosie; Caillou Is Sick; Caillou's Phone Call
 7:25 am | Caillou | Caillou Goes Camping; Caillou Learns to Skate; Caillou and Daddy
 7:45 am | Caillou | Caillou at the Beach; Caillou's New Shoes; Caillou's Snowman; Caillou Is a Clown; Caillou's Special Friend
 8:05 am | Pocoyo | 
 8:35 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | Dragon Run
 9:15 am | Thomas & Friends: All Engines Go | The Biggest Adventure Club
 9:30 am | Thomas & Friends: All Engines Go | License to Deliver
 9:45 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:55 am | Bing | 
10:25 am | Bing | 
11:00 am | Pocoyo | 
11:30 am | Pocoyo | 
12:00 pm | Caillou | Caillou's Big Slide; Caillou Walks a Dog; Caillou Watches Rosie; Caillou Is Sick; Caillou's Phone Call
12:30 pm | Caillou | Caillou Goes Camping; Caillou Learns to Skate; Caillou Makes a New Friend; Caillou and Daddy; Caillou Grows Carrots
 1:00 pm | Mush-Mush and the Mushables | A Frog of a Problem
 1:15 pm | Mush-Mush and the Mushables | Stuck in the Mud
 1:30 pm | Mush-Mush and the Mushables | The Staff of Wisdom
 1:45 pm | Mush-Mush and the Mushables | The Hero Trap
 2:00 pm | Amazing World of Gumball | The Club; The Wand
 2:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 3:00 pm | Amazing World of Gumball | The Quest; The Spoon
 3:30 pm | Amazing World of Gumball | The Car; The Curse
 4:00 pm | Craig of the Creek | The Last Game of Summer
 4:15 pm | Craig of the Creek | Winter Creeklympics
 4:30 pm | Craig of the Creek | Fall Anthology
 4:45 pm | Craig of the Creek | Welcome to Creek Street
 5:00 pm | Craig of the Creek | Afterschool Snackdown
 5:15 pm | Craig of the Creek | Breaking the Ice
 5:30 pm | Teen Titans Go! | The Mug
 5:45 pm | Teen Titans Go! | Don't Press Play
 6:00 pm | Teen Titans Go! | Hafo Safo
 6:15 pm | Teen Titans Go! | Huggbees
 6:30 pm | Teen Titans Go! | Thumb War
 6:45 pm | Teen Titans Go! | Various Modes of Transportation
 7:00 pm | We Bare Bears | Our Stuff
 7:15 pm | We Bare Bears | Jean Jacket
 7:30 pm | We Bare Bears | Viral Video
 7:45 pm | We Bare Bears | Nom Nom