# 2021-09-23
 8:00 pm | Joe Pera Talks With You | Joe Pera Waits With You
 8:15 pm | Joe Pera Talks With You | Joe Pera Guides You Through the Dark
 8:30 pm | Joe Pera Talks With You | Joe Pera Takes You to the Grocery Store
 8:45 pm | Joe Pera Talks With You | Joe Pera Goes to Dave Wojcek's Bachelor Party with You
 9:00 pm | Bob's Burgers | Sit Me Baby One More Time
 9:30 pm | Bob's Burgers | V for Valentine-detta
10:00 pm | American Dad | Blonde Ambition
10:30 pm | American Dad | CIAPOW
11:00 pm | American Dad | Scents and Sensei-bility
11:30 pm | Rick and Morty | Rickdependence Spray
12:00 am | Robot Chicken | Happy Russian Deathdog Dolloween 2 U			
12:15 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
12:30 am | Teenage Euthanasia | First Date with the Second Coming
 1:00 am | Teenage Euthanasia | Teen Eggs and Scram
 1:30 am | Bob's Burgers | Sit Me Baby One More Time
 2:00 am | Bob's Burgers | V for Valentine-detta
 2:30 am | Rick and Morty | Rickdependence Spray
 3:00 am | Teenage Euthanasia | First Date with the Second Coming
 3:30 am | Teenage Euthanasia | Teen Eggs and Scram
 4:00 am | Tender Touches | Foods of the World
 4:15 am | Tender Touches | Microecosystems
 4:30 am | Robot Chicken | Happy Russian Deathdog Dolloween 2 U			
 4:45 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 5:00 am | Naruto:Shippuden | A Will of Stone
 5:30 am | Samurai Jack | XLI