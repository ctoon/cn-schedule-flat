# 2021-09-16
 8:00 pm | Bob's Burgers | Zero Larp Thirty
 8:30 pm | Bob's Burgers | The Laser-Inth
 9:00 pm | American Dad | Steve and Snot's Test-Tubular Adventure
 9:30 pm | American Dad | Buck, Wild
10:00 pm | American Dad | Crotchwalkers
10:30 pm | Rick and Morty | Rickternal Friendshine of the Spotless Mort
11:00 pm | Family Guy | Brian the Closer
11:30 pm | Family Guy | Stewie, Chris & Brian's Excellent Adventure
12:00 am | Robot Chicken | May Cause Numb Butthole			
12:15 am | Robot Chicken | Gracie Purgatory in: That's How You Get Hemorrhoids
12:30 am | Aqua Teen | Zucotti Manicotti
12:45 am | Aqua Teen | Totem Pole
 1:00 am | The Venture Brothers | Tanks for Nuthin'
 1:30 am | Family Guy | Brian the Closer
 2:00 am | Family Guy | Stewie, Chris & Brian's Excellent Adventure
 2:30 am | Bob's Burgers | Zero Larp Thirty
 3:00 am | Bob's Burgers | The Laser-Inth
 3:30 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
 4:00 am | Tender Touches | Bamboo Floors
 4:15 am | Tender Touches | Train Floors
 4:30 am | Robot Chicken | May Cause Numb Butthole			
 4:45 am | Robot Chicken | Gracie Purgatory in: That's How You Get Hemorrhoids
 5:00 am | Naruto:Shippuden | Nine Tails
 5:30 am | Samurai Jack | XXXVI