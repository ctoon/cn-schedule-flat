# 2021-09-09
 6:00 am | Amazing World of Gumball | Darwin's Yearbook - Banana Joe - Darwin's Yearbook - Clayton
 6:30 am | Amazing World of Gumball | Darwin's Yearbook - Carrie Darwin's Yearbook - Alan
 7:00 am | Amazing World of Gumball | Darwin's Yearbook - Sarah Darwin's Yearbook - Teachers
 7:30 am | Amazing World of Gumball | The Third; The Debt
 8:00 am | Teen Titans Go! | Beast Girl; Quantum Fun
 8:30 am | Teen Titans Go! | The Fight; Tv Knight 3
 9:00 am | Teen Titans Go! | Genie President; Bro-pocalypse
 9:30 am | Teen Titans Go! | BBRBDAY ;Little Elvis
10:00 am | Craig of the Creek | Ancients of the Creek Crisis at Elder Rock
10:30 am | Craig of the Creek | Mortimor to the Rescue Kelsey the Worthy
11:00 am | Craig of the Creek | The Secret in a Bottle End Was Here
11:30 am | Craig of the Creek | The Trading Day Children of the Dog
12:00 pm | Teen Titans Go! | Hot Salad Water; Labor Day
12:30 pm | Teen Titans Go! | Brain Percentages; Ones and Zeros
 1:00 pm | Amazing World of Gumball | The Understanding; The Parents
 1:30 pm | Amazing World of Gumball | The Founder; The Ad
 2:00 pm | Amazing World Of Gumball | The Slip; The Schooling
 2:30 pm | Amazing World Of Gumball | The Drama; The Intelligence
 3:00 pm | Craig of the Creek | Alone Quest; Summer Wish
 3:30 pm | Craig of the Creek | Memories of Bobby;urning the Tables
 4:00 pm | Craig of the Creek | Jacob of the Creek; Camper on the Run
 4:30 pm | Craig of the Creek | The Mystery of the Timekeeper; Kelsey the Author
 5:00 pm | Star Wars: The Phantom Menace | 