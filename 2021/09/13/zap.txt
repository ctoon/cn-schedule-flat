# 2021-09-13
 6:00 am | Baby Looney Tunes | Taz in Toyland
 6:15 am | Baby Looney Tunes | A Secret Tweet
 6:30 am | Baby Looney Tunes | Comfort Level
 6:45 am | Baby Looney Tunes | School Daze
 7:00 am | Caillou | Caillou Makes Cookies; Caillou's Not Afraid Anymore; Caillou Hates Vegetables; Caillou's All Alone; Caillou Tidies His Toys
 7:25 am | Caillou | Caillou at Daycare; Caillou Joins the Circus; Caillou Is Afraid of the Dark; Caillou's Friends; Caillou Visits the Doctor
 7:45 am | Caillou | Caillou Learns to Drive; Caillou's Big Friend; Caillou's Colors; Caillou Mails a Letter; Caillou Learns to Swim
 8:05 am | Pocoyo | 
 8:35 am | Pocoyo | 
 9:00 am | Thomas & Friends: All Engines Go | Thomas' Promise
 9:15 am | Thomas & Friends: All Engines Go | Thomas Blasts Off
 9:30 am | Thomas & Friends: All Engines Go | License to Deliver
 9:45 am | Thomas & Friends: All Engines Go | Rules of the Game
 9:55 am | Bing | 
10:25 am | Bing | 
11:00 am | Pocoyo | 
11:30 am | Pocoyo | 
12:00 pm | Caillou | Caillou Makes Cookies; Caillou's Not Afraid Anymore; Caillou Hates Vegetables; Caillou's All Alone; Caillou Tidies His Toys
12:30 pm | Caillou | Caillou at Daycare; Caillou Joins the Circus; Caillou Is Afraid of the Dark; Caillou's Friends; Caillou Visits the Doctor
 1:00 pm | Mush-Mush and the Mushables | Save the Fun Tree
 1:15 pm | Mush-Mush and the Mushables | Get the Egg Home
 1:30 pm | Mush-Mush and the Mushables | Oh My Compost
 1:45 pm | Mush-Mush and the Mushables | Fly, Chase, Fly
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | Amazing World of Gumball | The Gi; The Kiss
 3:30 pm | Amazing World of Gumball | The Party; The Refund
 4:00 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 4:15 pm | Craig of the Creek | King of Camping
 4:30 pm | Craig of the Creek | The Bike Thief
 4:45 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 5:00 pm | Craig of the Creek | Craig of the Beach
 5:15 pm | Craig of the Creek | I Don't Need a Hat
 5:30 pm | Teen Titans Go! | Pig in a Poke
 5:45 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 7:00 pm | Amazing World of Gumball | 