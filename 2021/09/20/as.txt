# 2021-09-20
 8:00 pm | Joe Pera Talks With You | Joe Pera Shows You Iron
 8:15 pm | Joe Pera Talks With You | Joe Pera Takes You to Breakfast
 8:30 pm | Joe Pera Talks With You | Joe Pera Takes You on a Fall Drive
 8:45 pm | Joe Pera Talks With You | Joe Pera Shows You How to Dance
 9:00 pm | Bob's Burgers | Thelma and Louise Except Thelma Is Linda
 9:30 pm | Bob's Burgers | Mom, Lies, and Videotape
10:00 pm | American Dad | I Ain't No Holodeck Boy
10:30 pm | American Dad | Stan Goes on the Pill
11:00 pm | American Dad | Rubberneckers
11:30 pm | Rick and Morty | Mort Dinner Rick Andre
12:00 am | Robot Chicken | May Cause the Need for Speed			
12:15 am | Robot Chicken | Sundancer Craig in: 30% of the Way to Crying
12:30 am | Aqua Teen | Muscles
12:45 am | Aqua Teen | The Dudies
 1:00 am | The Venture Brothers | It Happening One Night
 1:30 am | Bob's Burgers | Thelma and Louise Except Thelma Is Linda
 2:00 am | Bob's Burgers | Mom, Lies, and Videotape
 2:30 am | Rick and Morty | Mort Dinner Rick Andre
 3:00 am | Aqua Teen | Muscles
 3:15 am | Aqua Teen | The Dudies
 3:30 am | The Venture Brothers | It Happening One Night
 4:00 am | Tender Touches | She's All We Had
 4:15 am | Tender Touches | Nude Bidet Ha-Ha Spray
 4:30 am | Robot Chicken | May Cause the Need for Speed			
 4:45 am | Robot Chicken | Sundancer Craig in: 30% of the Way to Crying
 5:00 am | Naruto:Shippuden | Two-Man Team
 5:30 am | Samurai Jack | XXXVIII