# 2021-09-26
 6:00 am | Baby Looney Tunes | Cat-Taz-Trophy
 6:15 am | Baby Looney Tunes | Duck! Monster! Duck!
 6:30 am | Baby Looney Tunes | The Brave Little Tweety
 6:45 am | Baby Looney Tunes | Puddle Olympics
 7:00 am | Love Monster | Show and Tell Day
 7:10 am | Love Monster | Perfect Pineapple Party Day
 7:20 am | Love Monster | Hot, Hot, Hot Day
 7:30 am | Care Bears: Unlock the Magic | A Patch of Perturbed Petunias
 7:45 am | Care Bears: Unlock the Magic | Nobody's Perfect
 8:00 am | Teen Titans Go! | Island Adventures
 9:00 am | Teen Titans Go! | The Streak, Part 1
 9:15 am | Teen Titans Go! | The Streak, Part 2
 9:30 am | Teen Titans Go! | Shrimps and Prime Rib
 9:40 am | Teen Titans Go! | Inner Beauty of a Cactus
 9:50 am | Teen Titans Go! | Rain on Your Wedding Day
10:00 am | Craig of the Creek | Afterschool Snackdown
10:15 am | Craig of the Creek | Snow Place Like Home
10:30 am | Craig of the Creek | Creature Feature
10:45 am | Craig of the Creek | Winter Creeklympics
11:00 am | Amazing World of Gumball | The Compilation
11:15 am | Amazing World of Gumball | The Vision
11:30 am | Amazing World of Gumball | The Stories
11:45 am | Amazing World of Gumball | The Choices
12:00 pm | Amazing World of Gumball | The Guy
12:15 pm | Amazing World of Gumball | The Code
12:30 pm | Amazing World of Gumball | The Boredom
12:45 pm | Amazing World of Gumball | The Test
 1:00 pm | Craig of the Creek | King of Camping
 1:15 pm | Craig of the Creek | Welcome to Creek Street
 1:30 pm | Craig of the Creek | Breaking the Ice
 1:45 pm | Craig of the Creek | The Sunflower
 2:00 pm | Craig of the Creek | Fan or Foe
 2:15 pm | Craig of the Creek | Craig World
 2:30 pm | Craig of the Creek | New Jersey
 2:45 pm | Craig of the Creek | Body Swap
 3:00 pm | Teen Titans Go! | Booby Trap House
 3:15 pm | Teen Titans Go! | Movie Night
 3:30 pm | Teen Titans Go! | Fish Water
 3:45 pm | Teen Titans Go! | Permanent Record
 4:00 pm | Teen Titans Go! | TV Knight
 4:15 pm | Teen Titans Go! | Titan Saving Time
 4:30 pm | Teen Titans Go! | BBSFBDAY!
 4:40 pm | Teen Titans Go! | Master Detective
 4:50 pm | Teen Titans Go! | A Little Help Please
 5:00 pm | Amazing World of Gumball | The Finale
 5:15 pm | Amazing World of Gumball | The Vacation
 5:30 pm | Amazing World of Gumball | The Singing
 5:45 pm | Amazing World of Gumball | The Vegging
 6:00 pm | Man of Steel | 