# 2021-09-06
 6:00 am | Teen Titans Go! | Genie President
 6:15 am | Teen Titans Go! | The Fight
 6:30 am | Teen Titans Go! | Finally a Lesson
 6:45 am | Teen Titans Go! | Lication
 7:00 am | Teen Titans Go! | Pyramid Scheme
 7:15 am | Alpha Bodies | 
 7:30 am | Teen Titans Go! | Money Grandma
 7:45 am | Teen Titans Go! | Two Bumble Bees and a Wasp
 8:00 am | Teen Titans Go! | Grube's Fairytales
 8:15 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt
 8:30 am | Teen Titans Go! | The Metric System vs. Freedom
 8:45 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 9:00 am | Teen Titans Go! | Oregon Trail
 9:15 am | Lucas the Spider | 
 9:30 am | Teen Titans Go! | Mas Y Menos
 9:45 am | Teen Titans Go! | Them Soviet Boys
10:00 am | Teen Titans Go! | Books
10:15 am | Teen Titans Go! | Driver's Ed
10:30 am | Teen Titans Go! | Yearbook Madness
10:45 am | Teen Titans Go! | Cool School
11:00 am | Teen Titans Go! | Career Day
11:15 am | Teen Titans Go! | Labor Day
11:30 am | Teen Titans Go! | Knowledge
11:45 am | Teen Titans Go! | History Lesson
12:00 pm | Amazing World of Gumball | The Slide
12:15 pm | Pocoyo | 
12:30 pm | Amazing World of Gumball | The Matchmaker
12:45 pm | Amazing World of Gumball | The Potato
 1:00 pm | Amazing World of Gumball | The Grades
 1:15 pm | Amazing World of Gumball | The Sorcerer
 1:30 pm | Amazing World of Gumball | The Singing
 1:45 pm | Amazing World of Gumball | The Best
 2:00 pm | Amazing World of Gumball | The Worst
 2:15 pm | Amazing World of Gumball | The List
 2:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 2:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 3:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 3:15 pm | The Amazing World of Gumball: Darwin's Yearbook | Alan
 3:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 3:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 4:00 pm | Amazing World of Gumball | The Silence
 4:15 pm | Amazing World of Gumball | The Revolt
 4:30 pm | Amazing World of Gumball | The Drama
 4:45 pm | Amazing World of Gumball | The Understanding
 5:00 pm | Star Wars: A New Hope | 
 6:30 pm | Amazing World of Gumball | The Line