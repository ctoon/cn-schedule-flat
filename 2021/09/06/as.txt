# 2021-09-06
 8:00 pm | Bob's Burgers | The Horse Rider-Er
 8:30 pm | Bob's Burgers | Secret Admiral-Irer
 9:00 pm | American Dad | Wheels & the Legman and the Case of Grandpa's Key
 9:30 pm | American Dad | Old Stan in the Mountain
10:00 pm | Family Guy | Finders Keepers
10:30 pm | Family Guy | Vestigial Peter
11:00 pm | Rick and Morty | Mort Dinner Rick Andre
11:30 pm | Rick and Morty | Mortyplicity
12:00 am | Robot Chicken | May Cause a Whole Lotta Scabs			
12:15 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
12:30 am | Aqua Teen | Lasagna
12:45 am | Aqua Teen | Last Dance for Napkin Lad
 1:00 am | The Venture Brothers | Momma's Boys
 1:30 am | Family Guy | Finders Keepers
 2:00 am | Family Guy | Vestigial Peter
 2:30 am | Bob's Burgers | The Horse Rider-Er
 3:00 am | Rick and Morty | Forgetting Sarick Mortshall
 3:30 am | Rick and Morty | Rickmurai Jack
 4:00 am | Tender Touches | Heated Floors
 4:15 am | Tender Touches | Bamboo Floors
 4:30 am | Robot Chicken | May Cause a Whole Lotta Scabs			
 4:45 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
 5:00 am | Naruto:Shippuden | Run, Omoi!
 5:30 am | Samurai Jack | XXIX