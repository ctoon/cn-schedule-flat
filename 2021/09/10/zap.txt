# 2021-09-10
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 6:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Anyone?
 7:00 am | Amazing World of Gumball | The Pressure; The Painting
 7:30 am | Amazing World of Gumball | The Responsible; The Dress
 8:00 am | Teen Titans Go! | The Groover
 8:15 am | Alpha Bodies | 
 8:30 am | Teen Titans Go! | The Power of Shrimps
 8:45 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 9:00 am | Teen Titans Go! | My Name Is Jose
 9:15 am | Pocoyo | 
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
10:00 am | Craig of the Creek | Jessica Shorts
10:15 am | Craig of the Creek | Beyond the Rapids
10:30 am | Craig of the Creek | Ferret Quest
10:45 am | Craig of the Creek | The Jinxening
11:00 am | Craig of the Creek | Into the Overpast
11:15 am | Lucas the Spider | Fun With Findley: Sneak Peek
11:30 am | Craig of the Creek | The Time Capsule
11:45 am | Craig of the Creek | The Ground Is Lava!
12:00 pm | Teen Titans Go! | The Night Begins to Shine Special
12:30 pm | Alpha Bodies | 
 1:00 pm | Amazing World of Gumball | The Potion
 1:15 pm | Amazing World of Gumball | The Buddy
 1:30 pm | Amazing World of Gumball | The Spinoffs
 1:45 pm | Amazing World of Gumball | The Master
 2:00 pm | Amazing World of Gumball | The Transformation
 2:15 pm | Pocoyo | 
 2:30 pm | Amazing World of Gumball | The Future
 2:45 pm | Amazing World of Gumball | The Heart
 3:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 3:15 pm | Lucas the Spider | Fun With Findley: Sneak Peek
 3:30 pm | Craig of the Creek | Fort Williams
 3:45 pm | Craig of the Creek | Creek Daycare
 4:00 pm | Craig of the Creek | Kelsey the Elder
 4:15 pm | Craig of the Creek | Sugar Smugglers
 4:30 pm | Craig of the Creek | Sour Candy Trials
 4:45 pm | Craig of the Creek | Sleepover at JP's
 5:00 pm | Star Wars: Attack of the Clones | 