# 2021-08-24
 8:00 pm | Bob's Burgers | Sliding Bobs
 8:30 pm | Bob's Burgers | The Land Ship
 9:00 pm | American Dad | A Jones for a Smith
 9:30 pm | American Dad | May the Best Stan Win
10:00 pm | American Dad | Return of the Bling
10:30 pm | Rick and Morty | Mortyplicity
11:00 pm | Family Guy | Leggo My Meg-O
11:30 pm | Family Guy | Tea Peter
12:00 am | Tuca & Bertie | Bird Mechanics
12:30 am | Robot Chicken | Two Weeks Without Food
12:45 am | Robot Chicken | But Not in That Way
 1:00 am | Aqua Teen | Monster
 1:15 am | Aqua Teen | Hands on a Hamburger
 1:30 am | Family Guy | Leggo My Meg-O
 2:00 am | Family Guy | Tea Peter
 2:30 am | Bob's Burgers | Sliding Bobs
 3:00 am | Bob's Burgers | The Land Ship
 3:30 am | Tuca & Bertie | Bird Mechanics
 4:00 am | Gemusetto: Death Beat(s) | Episode One: Asus4
 4:15 am | Gemusetto: Death Beat(s) | Episode Two: A# Minor
 4:30 am | The Venture Brothers | The Silent Partners
 5:00 am | Naruto:Shippuden | The New Akatsuki
 5:30 am | Samurai Jack | XXII