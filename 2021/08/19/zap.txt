# 2021-08-19
 6:00 am | Amazing World of Gumball | The Words; The Apology
 6:30 am | Amazing World of Gumball | The Watch; The Bet
 7:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
 7:30 am | Amazing World of Gumball | The Pony; The Storm
 8:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 9:00 am | Teen Titans Go! | Butter Wall
 9:15 am | Lucas the Spider | 
 9:30 am | Teen Titans Go! | BBRAEBDAY
 9:45 am | Teen Titans Go! | Real Art
10:00 am | Craig of the Creek | The Invitation
10:15 am | Craig of the Creek | The Great Fossil Rush
10:30 am | Craig of the Creek | Vulture's Nest
10:45 am | Craig of the Creek | The Mystery of the Timekeeper
11:00 am | Craig of the Creek | Kelsey Quest
11:15 am | Craig of the Creek | Alone Quest
11:30 am | Craig of the Creek | JPony
11:45 am | Craig of the Creek | Memories of Bobby
12:00 pm | Teen Titans Go! | Teen Titans Vroom
12:30 pm | Teen Titans Go! | TV Knight 5
12:45 pm | Teen Titans Go! | Walk Away
 1:00 pm | Amazing World of Gumball | The Mustache; The Date
 1:15 pm | Pocoyo | 
 1:30 pm | Amazing World of Gumball | The Club; The Wand
 2:00 pm | Amazing World of Gumball | The Ape; The Poltergeist
 2:30 pm | Amazing World of Gumball | The Quest; The Spoon
 3:00 pm | Craig of the Creek | Council of the Creek
 3:15 pm | Lucas the Spider | Fun With Findley: Sneak Peek
 3:30 pm | Craig of the Creek | Sparkle Cadet
 3:45 pm | Craig of the Creek | The Cardboard Identity
 4:00 pm | Craig of the Creek | Cousin of the Creek
 4:15 pm | Craig of the Creek | Ancients of the Creek
 4:30 pm | Craig of the Creek | Camper on the Run
 4:45 pm | Craig of the Creek | Mortimor to the Rescue
 5:00 pm | Total DramaRama | Sharing Is Caring
 5:15 pm | Pocoyo | 
 5:30 pm | Total DramaRama | Ant We All Just Get Along
 5:45 pm | Total DramaRama | Not Without My Fudgy Lumps
 6:00 pm | Teen Titans Go! | Record Book
 6:15 pm | Teen Titans Go! | Bucket List
 6:30 pm | Teen Titans Go! | Bat Scouts
 6:45 pm | Teen Titans Go! | TV Knight 6
 7:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 7:30 pm | Apple & Onion | Apple's Formula
 7:45 pm | Apple & Onion | Apple's Focus