# 2021-08-25
 6:00 am | Amazing World of Gumball | The Name; The Extras
 6:30 am | Amazing World of Gumball | The Gripes; The Vacation
 7:00 am | Amazing World of Gumball | The Fraud; The Void
 7:30 am | Amazing World of Gumball | The Boss; The Move
 7:45 am | Lucas the Spider | Fun With Findley: Sneak Peek
 8:00 am | Teen Titans Go! | T Is for Titans
 8:15 am | Teen Titans Go! | Cy & Beasty
 8:30 am | Teen Titans Go! | A Little Help Please
 8:45 am | Teen Titans Go! | Marv Wolfman and George Pérez
 9:00 am | Teen Titans Go! | Creative Geniuses
 9:15 am | Teen Titans Go! | Manor and Mannerisms
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 9:45 am | Teen Titans Go! | Inner Beauty of a Cactus
10:00 am | Craig of the Creek | Winter Creeklympics
10:15 am | Craig of the Creek | Copycat Carter
10:30 am | Craig of the Creek | Welcome to Creek Street
10:45 am | Craig of the Creek | Brother Builder
11:00 am | Craig of the Creek | Jessica the Intern
11:15 am | Craig of the Creek | You're It
11:30 am | Craig of the Creek | Itch to Explore
11:45 am | Lucas the Spider | Fun With Findley: Sneak Peek
12:00 pm | Teen Titans Go! | Dude Relax; Laundry Day
12:30 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
12:45 pm | Pocoyo | 
 1:00 pm | Amazing World of Gumball | The Bros; The Man
 1:30 pm | Amazing World of Gumball | The Pizza; The Lie
 2:00 pm | Amazing World of Gumball | The Butterfly; The Question
 2:30 pm | Amazing World of Gumball | The Oracle; The Safety
 3:00 pm | Craig of the Creek | Creature Feature
 3:15 pm | Craig of the Creek | The Jinxening
 3:30 pm | Craig of the Creek | The Other Side: The Tournament
 3:45 pm | Pocoyo | 
 4:00 pm | Craig of the Creek | King of Camping
 4:15 pm | Craig of the Creek | Breaking the Ice
 4:30 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:45 pm | Craig of the Creek | Fan or Foe
 5:00 pm | Total DramaRama | Toys Will Be Toys
 5:15 pm | Total DramaRama | Soother or Later
 5:30 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:45 pm | Total DramaRama | Mutt Ado About Owen
 6:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 6:30 pm | Teen Titans Go! | Girls' Night Out; You're Fired
 7:00 pm | Amazing World of Gumball | The Law; The Allergy
 7:30 pm | Apple & Onion | Falafel's in Jail
 7:45 pm | Apple & Onion | Party Popper