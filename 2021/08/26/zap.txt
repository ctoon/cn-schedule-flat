# 2021-08-26
 6:00 am | Amazing World of Gumball | The Law; The Allergy
 6:30 am | Amazing World of Gumball | The Mothers; The Password
 7:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 7:30 am | Amazing World of Gumball | The Mirror; The Burden
 8:00 am | Teen Titans Go! | Trans Oceanic Magical Cruise
 8:15 am | Pocoyo | 
 8:30 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 9:00 am | Teen Titans Go! | Driver's Ed; Dog Hand
 9:30 am | Teen Titans Go! | Double Trouble; The Date
10:00 am | Craig of the Creek | The End Was Here
10:15 am | Craig of the Creek | The Bike Thief
10:30 am | Craig of the Creek | In the Key of the Creek
10:45 am | Craig of the Creek | Craig of the Beach
11:00 am | Craig of the Creek | The Children of the Dog
11:15 am | Craig of the Creek | Plush Kingdom
11:30 am | Craig of the Creek | Jessica Shorts
11:45 am | Craig of the Creek | The Ice Pop Trio
12:00 pm | Teen Titans Go! | Just a Little Patience…Yeah…Yeah
12:15 pm | Lucas the Spider | 
12:30 pm | Teen Titans Go! | Villains in a Van Getting Gelato
12:45 pm | Teen Titans Go! | Hafo Safo
 1:00 pm | Amazing World of Gumball | The Promise; The Voice
 1:15 pm | Pocoyo | 
 1:30 pm | Amazing World of Gumball | The Boombox; The Castle
 2:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 2:30 pm | Amazing World of Gumball | The Internet; The Plan
 3:00 pm | Craig of the Creek | I Don't Need a Hat
 3:15 pm | Craig of the Creek | New Jersey
 3:30 pm | Craig of the Creek | Alternate Creekiverse
 3:45 pm | Craig of the Creek | The Sunflower
 4:00 pm | Craig of the Creek | Snow Day
 4:15 pm | Craig of the Creek | Craig World
 4:30 pm | Craig of the Creek | Snow Place Like Home
 4:45 pm | Craig of the Creek | Body Swap
 5:00 pm | Total DramaRama | Gum and Gummer
 5:15 pm | Lucas the Spider | 
 5:30 pm | Total DramaRama | Invasion of the Booger Snatchers
 5:45 pm | Total DramaRama | Mother of All Cards
 6:00 pm | Teen Titans Go! | Ghost With the Most
 6:15 pm | Teen Titans Go! | Zimdings
 6:30 pm | Teen Titans Go! | I Am Chair
 6:45 pm | Teen Titans Go! | Don't Press Play
 7:00 pm | Amazing World of Gumball | The Bros; The Man
 7:30 pm | Apple & Onion | The Music Store Thief
 7:45 pm | Apple & Onion | Lil Noodle