# 2021-08-18
 8:00 pm | Bob's Burgers | The Runway Club
 8:30 pm | Bob's Burgers | The Itty Bitty Ditty Committee
 9:00 pm | American Dad | DeLorean Story-An
 9:30 pm | American Dad | Every Which Way But Lose
10:00 pm | American Dad | Weiner of Our Discontent
10:30 pm | Rick and Morty | Gotron Jerrysis Rickvangelion
11:00 pm | Family Guy | Cool Hand Peter
11:30 pm | Family Guy | Quagmire and Meg
12:00 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
12:30 am | Robot Chicken | They Took My Thumbs
12:45 am | Robot Chicken | I'm Trapped
 1:00 am | Aqua Teen | Der Inflatable Fuhrer
 1:15 am | Aqua Teen | The Last Last One Forever and Ever
 1:30 am | Family Guy | Cool Hand Peter
 2:00 am | Family Guy | Quagmire and Meg
 2:30 am | Bob's Burgers | The Runway Club
 3:00 am | Bob's Burgers | The Itty Bitty Ditty Committee
 3:30 am | Rick and Morty | Rickternal Friendshine of the Spotless Mort
 4:00 am | Gemusetto: Machu Picchu | Chapter 4B: Break Point
 4:30 am | The Venture Brothers | Everybody Comes to Hank's
 5:00 am | Naruto:Shippuden | I'm in Hell
 5:30 am | Samurai Jack | XIX