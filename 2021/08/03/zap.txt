# 2021-08-03
 6:00 am | Amazing World of Gumball | The Worst
 6:15 am | Amazing World of Gumball | The Faith
 6:30 am | Amazing World of Gumball | The List
 6:45 am | Amazing World of Gumball | The Candidate
 7:00 am | Amazing World of Gumball | The Deal
 7:15 am | Amazing World of Gumball | The Anybody
 7:30 am | Amazing World of Gumball | The Line
 7:45 am | Amazing World of Gumball | The Shippening
 8:00 am | Teen Titans Go! | Space House
 9:00 am | Teen Titans Go! | T Is for Titans
 9:15 am | Teen Titans Go! | Cy & Beasty
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:00 am | Craig of the Creek | Pencil Break Mania
10:15 am | Craig of the Creek | Alternate Creekiverse
10:30 am | Craig of the Creek | The Last Game of Summer
10:45 am | Craig of the Creek | Snow Day
11:00 am | Craig of the Creek | Fall Anthology
11:15 am | Craig of the Creek | Snow Place Like Home
11:30 am | Craig of the Creek | Afterschool Snackdown
11:45 am | Craig of the Creek | Breaking the Ice
12:00 pm | Teen Titans Go! | The Academy
12:15 pm | Teen Titans Go! | TV Knight 3
12:30 pm | Teen Titans Go! | Throne of Bones
12:45 pm | Teen Titans Go! | Bro-Pocalypse
 1:00 pm | Amazing World of Gumball | The Cage
 1:15 pm | Amazing World of Gumball | The Founder
 1:30 pm | Amazing World of Gumball | The Rival
 1:45 pm | Amazing World of Gumball | The Schooling
 2:00 pm | Amazing World of Gumball | The One
 2:15 pm | Amazing World of Gumball | The Intelligence
 2:30 pm | Amazing World of Gumball | The Vegging
 2:45 pm | Amazing World of Gumball | The Potion
 3:00 pm | Craig of the Creek | The End Was Here
 3:15 pm | Craig of the Creek | The Jinxening
 3:30 pm | Craig of the Creek | In the Key of the Creek
 3:45 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 4:00 pm | Craig of the Creek | The Children of the Dog
 4:15 pm | Craig of the Creek | The Ground Is Lava!
 4:30 pm | Craig of the Creek | Jessica Shorts
 4:45 pm | Craig of the Creek | The Bike Thief
 5:00 pm | Total DramaRama | Simply Perfect
 5:15 pm | Total DramaRama | Bad Seed
 5:30 pm | Total DramaRama | Ghoul Spirit
 5:45 pm | Total DramaRama | A Fish Called Leshawna
 6:00 pm | Teen Titans Go! | T Is for Titans
 6:15 pm | Teen Titans Go! | Cy & Beasty
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Amazing World of Gumball | The Spinoffs
 7:15 pm | Amazing World of Gumball | The Silence
 7:30 pm | Apple & Onion | Apple's in Charge
 7:45 pm | Apple & Onion | The Perfect Team