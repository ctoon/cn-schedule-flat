# 2021-08-17
 8:00 pm | Bob's Burgers | Li'l Hard Dad
 8:30 pm | Bob's Burgers | Adventures in Chinchilla-Sitting
 9:00 pm | American Dad | Jack's Back
 9:30 pm | American Dad | Bar Mitzvah Shuffle
10:00 pm | American Dad | Wife Insurance
10:30 pm | Rick and Morty | Rick & Morty's Thanksploitation Spectacular
11:00 pm | Family Guy | It's a Trap Part 1 & 2
12:00 am | Tuca & Bertie | The Flood
12:30 am | Robot Chicken | Chirlaxx
12:45 am | Robot Chicken | Help Me
 1:00 am | Aqua Teen | 2-And-A-Half-Star Wars Out of Five
 1:15 am | Aqua Teen | Fry Legs
 1:30 am | Family Guy | It's a Trap Part 1 & 2
 2:30 am | Bob's Burgers | Li'l Hard Dad
 3:00 am | Bob's Burgers | Adventures in Chinchilla-Sitting
 3:30 am | Tuca & Bertie | The Flood
 4:00 am | Gemusetto: Machu Picchu | Chapter 4A: Break Point
 4:30 am | The Venture Brothers | Any Which Way But Zeus
 5:00 am | Naruto:Shippuden | Obito and Madara
 5:30 am | Samurai Jack | XVIII