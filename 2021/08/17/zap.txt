# 2021-08-17
 6:00 am | Amazing World of Gumball | The Car; The Curse
 6:30 am | Amazing World of Gumball | The Microwave; The Meddler
 7:00 am | Amazing World of Gumball | The Helmet; The Fight
 7:30 am | Amazing World of Gumball | The End; The DVD
 8:00 am | Teen Titans Go! | Magic Man
 8:15 am | Teen Titans Go! | Kryponite
 8:30 am | Teen Titans Go! | The Titans Go Casual
 8:45 am | Teen Titans Go! | Thumb War
 9:00 am | Teen Titans Go! | We're Off to Get Awards
 9:15 am | Teen Titans Go! | The Cast
 9:30 am | Teen Titans Go! | Rain on Your Wedding Day
 9:45 am | Teen Titans Go! | Toddler Titans...Yay!
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
12:30 pm | Pocoyo | 
 1:00 pm | Amazing World of Gumball | The Words; The Apology
 1:30 pm | Amazing World of Gumball | The Watch; The Bet
 2:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 2:30 pm | Amazing World of Gumball | The Pony; The Storm
 3:00 pm | Craig of the Creek | Ace of Squares
 3:15 pm | Craig of the Creek | Jacob of the Creek
 3:30 pm | Craig of the Creek | Doorway to Helen
 3:45 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:00 pm | Craig of the Creek | The Climb
 4:15 pm | Craig of the Creek | Kelsey the Elder
 4:30 pm | Craig of the Creek | Sour Candy Trials
 4:45 pm | Craig of the Creek | Fort Williams
 5:00 pm | Total DramaRama | Cluckwork Orange
 5:15 pm | Total DramaRama | A Ninjustice to Harold
 5:30 pm | Total DramaRama | Aquarium for a Dream
 5:45 pm | Pocoyo | 
 6:00 pm | Teen Titans Go! | Butter Wall
 6:15 pm | Teen Titans Go! | Superhero Feud
 6:30 pm | Teen Titans Go! | BBRAEBDAY
 6:45 pm | Teen Titans Go! | Real Art
 7:00 pm | Amazing World of Gumball | The Knights; The Colossus
 7:30 pm | Apple & Onion | Sausage and Sweetie Smash
 7:45 pm | Apple & Onion | Pancake's Bus Tour