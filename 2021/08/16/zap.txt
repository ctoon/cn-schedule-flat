# 2021-08-16
 6:00 am | Amazing World of Gumball | The Mustache; The Date
 6:30 am | Amazing World of Gumball | The Club; The Wand
 7:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 7:30 am | Amazing World of Gumball | The Quest; The Spoon
 8:00 am | Teen Titans Go! | Teen Titans Vroom
 8:30 am | Teen Titans Go! | TV Knight 5
 8:45 am | Teen Titans Go! | Walk Away
 9:00 am | Teen Titans Go! | Record Book
 9:15 am | Teen Titans Go! | Bucket List
 9:30 am | Teen Titans Go! | Bat Scouts
 9:45 am | Teen Titans Go! | Garage Sale
10:00 am | Craig of the Creek | The Other Side
10:30 am | Craig of the Creek | Summer Wish
10:45 am | Craig of the Creek | Creek Daycare
11:00 am | Craig of the Creek | Turning the Tables
11:15 am | Craig of the Creek | Sugar Smugglers
11:30 am | Craig of the Creek | Kelsey the Author
11:45 am | Craig of the Creek | Sleepover at JP's
12:00 pm | Teen Titans Go! | That's What's Up
 1:00 pm | Amazing World of Gumball | The Knights; The Colossus
 1:30 pm | Amazing World of Gumball | The Fridge; The Remote
 2:00 pm | Amazing World of Gumball | The Flower; The Banana
 2:15 pm | Lucas the Spider | Fun With Findley: Sneak Peek
 2:30 pm | Amazing World of Gumball | The Phone; The Job
 3:00 pm | Craig of the Creek | The Invitation
 3:15 pm | Pocoyo | 
 3:30 pm | Craig of the Creek | Vulture's Nest
 3:45 pm | Craig of the Creek | The Mystery of the Timekeeper
 4:00 pm | Craig of the Creek | Kelsey Quest
 4:15 pm | Craig of the Creek | Alone Quest
 4:30 pm | Craig of the Creek | JPony
 4:45 pm | Craig of the Creek | Memories of Bobby
 5:00 pm | Total DramaRama | The Date
 5:15 pm | Total DramaRama | That's a Wrap
 5:30 pm | Total DramaRama | Duck Duck Juice
 5:45 pm | Total DramaRama | Tiger Fail
 6:00 pm | Teen Titans Go! | Various Modes of Transportation
 6:15 pm | Teen Titans Go! | Cool Uncles
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 6:45 pm | Pocoyo | 
 7:00 pm | Amazing World of Gumball | The Car; The Curse
 7:15 pm | Lucas the Spider | Fun With Findley: Sneak Peek
 7:30 pm | Apple & Onion | Follow Your Dreams
 7:45 pm | Apple & Onion | Bottle Catch