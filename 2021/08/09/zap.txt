# 2021-08-09
 6:00 am | Amazing World of Gumball | The Slip
 6:15 am | Amazing World of Gumball | The Revolt
 6:30 am | Amazing World of Gumball | The Drama
 6:45 am | Amazing World of Gumball | The Mess
 7:00 am | Amazing World of Gumball | The Buddy
 7:15 am | Amazing World of Gumball | The Possession
 7:30 am | Amazing World of Gumball | The Master
 7:45 am | Amazing World of Gumball | The Heart
 8:00 am | Teen Titans Go! | The Fight
 8:15 am | Pocoyo | 
 8:30 am | Teen Titans Go! | Genie President
 8:45 am | Teen Titans Go! | Tall Titan Tales
 9:00 am | Teen Titans Go! | Little Elvis
 9:15 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 9:30 am | Teen Titans Go! | The Groover
 9:45 am | Teen Titans Go! | Pig in a Poke
10:00 am | Craig of the Creek | Too Many Treasures
10:15 am | Lucas the Spider | Fun With Findley: Sneak Peek
10:30 am | Craig of the Creek | Wildernessa
10:45 am | Craig of the Creek | Power Punchers
11:00 am | Craig of the Creek | Sunday Clothes
11:15 am | Craig of the Creek | Creek Cart Racers
11:30 am | Craig of the Creek | Escape From Family Dinner
11:45 am | Craig of the Creek | Secret Book Club
12:00 pm | Teen Titans Go! | Curse of the Booty Scooty
12:15 pm | Teen Titans Go! | TV Knight 4
12:30 pm | Teen Titans Go! | Them Soviet Boys
12:45 pm | Teen Titans Go! | Teen Titans Roar!
 1:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 1:15 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 1:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 1:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 2:00 pm | Amazing World of Gumball | The Third; The Debt
 2:30 pm | Amazing World of Gumball | The Pressure; The Painting
 3:00 pm | Craig of the Creek | New Jersey
 3:15 pm | Craig of the Creek | Copycat Carter
 3:30 pm | Craig of the Creek | The Sunflower
 3:45 pm | Craig of the Creek | Brother Builder
 4:00 pm | Craig of the Creek | Jessica the Intern
 4:15 pm | Lucas the Spider | Fun With Findley: Sneak Peek
 4:30 pm | Craig of the Creek | You're It
 4:45 pm | Craig of the Creek | Jessica Goes to the Creek
 5:00 pm | Total DramaRama | School District 9
 5:15 pm | Total DramaRama | Sugar & Spice & Lightning & Frights
 5:30 pm | Total DramaRama | Gumbearable
 5:45 pm | Pocoyo | 
 6:00 pm | Teen Titans Go! | Lil' Dimples
 6:15 pm | Teen Titans Go! | Communicate Openly
 6:30 pm | Teen Titans Go! | Stockton, CA!
 6:45 pm | Teen Titans Go! | Brain Percentages
 7:00 pm | Amazing World of Gumball | The Ghouls
 7:15 pm | Amazing World of Gumball | The Decisions
 7:30 pm | Apple & Onion | Not Funny
 7:45 pm | Apple & Onion | Apple's in Trouble