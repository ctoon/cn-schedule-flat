# 2021-08-10
 8:00 pm | Bob's Burgers | Father of the Bob
 8:30 pm | Bob's Burgers | Tina Tailor Soldier Spy
 9:00 pm | American Dad | Oedipal Panties
 9:30 pm | American Dad | Widowmaker
10:00 pm | American Dad | Red October Sky
10:30 pm | Rick and Morty | Rickternal Friendshine of the Spotless Mort
11:00 pm | Family Guy | And Then There Were Fewer Part 1 & 2
12:00 am | Tuca & Bertie | The Dance
12:30 am | Robot Chicken | Losin' the Wobble
12:45 am | Robot Chicken | Slaughterhouse on the Prairie
 1:00 am | Aqua Teen | Dummy Love
 1:15 am | Aqua Teen | The Marines
 1:30 am | Family Guy | And Then There Were Fewer Part 1 & 2
 2:30 am | Bob's Burgers | Father of the Bob
 3:00 am | Bob's Burgers | Tina Tailor Soldier Spy
 3:30 am | Tuca & Bertie | The Dance
 4:00 am | Gemusetto: Machu Picchu | Chapter 1B: Your Serve
 4:30 am | The Venture Brothers | The Better Man
 5:00 am | Naruto:Shippuden | Reanimation Jutsu, Release!
 5:30 am | Samurai Jack | XIV