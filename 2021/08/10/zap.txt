# 2021-08-10
 6:00 am | Amazing World of Gumball | The Ghouls
 6:15 am | Amazing World of Gumball | The Decisions
 6:30 am | Amazing World of Gumball | The BFFS
 6:45 am | Amazing World of Gumball | The Web
 7:00 am | Amazing World of Gumball | The Agent
 7:15 am | Amazing World of Gumball | The Inquisition
 7:30 am | The Amazing World of Gumball: Darwin's Yearbook | Alan
 7:45 am | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 8:00 am | Teen Titans Go! | The Power of Shrimps
 8:15 am | Lucas the Spider | 
 8:30 am | Teen Titans Go! | My Name Is Jose
 8:45 am | Teen Titans Go! | I Used to Be a Peoples
 9:00 am | Teen Titans Go! | The Real Orangins!
 9:15 am | Pocoyo | 
 9:30 am | Teen Titans Go! | The Chaff
 9:45 am | Teen Titans Go! | Don't Be an Icarus
10:00 am | Craig of the Creek | Monster in the Garden
10:15 am | Craig of the Creek | Jextra Perrestrial
10:30 am | Craig of the Creek | The Curse
10:45 am | Craig of the Creek | The Takeout Mission
11:00 am | Craig of the Creek | Dog Decider
11:15 am | Craig of the Creek | Dinner at the Creek
11:30 am | Craig of the Creek | Bring Out Your Beast
11:45 am | Craig of the Creek | Jessica's Trail
12:00 pm | Teen Titans Go! | Collect Them All
12:15 pm | Teen Titans Go! | Royal Jelly
12:30 pm | Teen Titans Go! | Cartoon Feud
12:45 pm | Teen Titans Go! | Strength of a Grown Man
 1:00 pm | Amazing World of Gumball | The Responsible; The Dress
 1:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:00 pm | Amazing World of Gumball | The Mystery; The Prank
 2:30 pm | Amazing World of Gumball | The Gi; The Kiss
 3:00 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 3:15 pm | Craig of the Creek | Capture the Flag Part 2: The King
 3:30 pm | Craig of the Creek | Capture the Flag Part 3: The Legend
 3:45 pm | Craig of the Creek | Capture the Flag Part 4: The Plan
 4:00 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 4:30 pm | Craig of the Creek | The Final Book
 4:45 pm | Craig of the Creek | Big Pinchy
 5:00 pm | Total DramaRama | Whack Mirror
 5:15 pm | Total DramaRama | I Dream of Meanie
 5:30 pm | Total DramaRama | Broken Back Kotter
 5:45 pm | Pocoyo | 
 6:00 pm | Teen Titans Go! | Super Hero Summer Camp
 7:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 7:15 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:30 pm | Apple & Onion | Positive Attitude Theory
 7:45 pm | Apple & Onion | Hotdog's Movie Premiere