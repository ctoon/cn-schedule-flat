# 2021-08-22
 6:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 6:30 am | Teen Titans Go! | Video Game References; Cool School
 7:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:15 am | Lucas the Spider | 
 7:30 am | Teen Titans Go! | Operation Tin Man; Nean
 8:00 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 8:30 am | Teen Titans Go! | Cat's Fancy
 8:45 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 9:00 am | Teen Titans Go! | Leg Day
 9:15 am | Pocoyo | 
 9:30 am | Teen Titans Go! | The Dignity of Teeth
 9:45 am | Teen Titans Go! | Zimdings
10:00 am | Craig of the Creek | Monster in the Garden
10:15 am | Craig of the Creek | Vulture's Nest
10:30 am | Craig of the Creek | Kelsey Quest
10:45 am | Craig of the Creek | Doorway to Helen
11:00 am | Craig of the Creek | JPony
11:15 am | Craig of the Creek | The Last Kid in the Creek
11:30 am | Craig of the Creek | Ace of Squares
11:45 am | Craig of the Creek | The Climb
12:00 pm | Amazing World of Gumball | The Sucker
12:15 pm | Amazing World of Gumball | The Founder
12:30 pm | Amazing World of Gumball | The Cage
12:45 pm | Amazing World of Gumball | The Schooling
 1:00 pm | Amazing World of Gumball | The Rival
 1:15 pm | Amazing World of Gumball | The Intelligence
 1:30 pm | Amazing World of Gumball | The One
 1:45 pm | Amazing World of Gumball | The Potion
 2:00 pm | Amazing World of Gumball | The Spinoffs
 2:15 pm | Amazing World of Gumball | The Buddy
 2:30 pm | Amazing World of Gumball | The Transformation
 2:45 pm | Amazing World of Gumball | The Comic
 3:00 pm | Victor and Valentino | Cleaning Day
 3:15 pm | Pocoyo | 
 3:30 pm | Victor and Valentino | The Babysitter
 3:45 pm | Victor and Valentino | Cuddle Monster
 4:00 pm | Teen Titans Go! | The Croissant
 4:15 pm | Teen Titans Go! | Grube's Fairytales
 4:30 pm | Teen Titans Go! | The Spice Game
 4:45 pm | Teen Titans Go! | A Farce
 5:00 pm | Teen Titans Go! | I'm the Sauce
 5:15 pm | Lucas the Spider | 
 5:30 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 5:45 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 6:00 pm | Amazing World of Gumball | The Understanding
 6:15 pm | Amazing World of Gumball | The Silence
 6:30 pm | Amazing World of Gumball | The Ad
 6:45 pm | Amazing World of Gumball | The Future
 7:00 pm | Amazing World of Gumball | The Slip
 7:15 pm | Amazing World of Gumball | The Wish
 7:30 pm | Amazing World of Gumball | The Drama
 7:45 pm | Amazing World of Gumball | The Factory