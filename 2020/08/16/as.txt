# 2020-08-16
 8:00 pm | Home Movies | Renaissance
 8:30 pm | Bob's Burgers | The Laser-Inth
 9:00 pm | American Dad | Ghost Dad
 9:30 pm | American Dad | Men ll Boyz
10:00 pm | American Dad | The Old Country
10:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
11:00 pm | Family Guy | The Heartbreak Dog
11:30 pm | Family Guy | Take a Letter
12:00 am | YOLO Crystal Fantasy | A Very Extremely Very YOLO Christmas: Reloaded
12:15 am | YOLO Crystal Fantasy | The Terry Cup
12:30 am | Tigtone | Tigtone and the Beautiful War
12:45 am | Tigtone | Tigtone and the Wine Crisis
 1:00 am | The Shivering Truth | Carrion My Son
 1:15 am | Momma Named Me Sheriff | Stuck
 1:30 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 2:00 am | Family Guy | The Heartbreak Dog
 2:30 am | Family Guy | Take a Letter
 3:00 am | American Dad | The Old Country
 3:30 am | YOLO Crystal Fantasy | A Very Extremely Very YOLO Christmas: Reloaded
 3:45 am | YOLO Crystal Fantasy | The Terry Cup
 4:00 am | Tigtone | Tigtone and the Beautiful War
 4:15 am | Tigtone | Tigtone and the Wine Crisis
 4:30 am | The Shivering Truth | Carrion My Son
 4:45 am | Momma Named Me Sheriff | Stuck
 5:00 am | Bob's Burgers | The Laser-Inth
 5:30 am | Home Movies | Renaissance