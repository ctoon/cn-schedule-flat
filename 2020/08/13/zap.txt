# 2020-08-13
 6:00 am | Amazing World of Gumball | The Cycle
 6:15 am | Amazing World of Gumball | The Transformation
 6:30 am | Amazing World of Gumball | The Stars
 6:45 am | Amazing World of Gumball | The Understanding
 7:00 am | Amazing World of Gumball | The Box
 7:15 am | Amazing World of Gumball | The Ad
 7:30 am | Amazing World of Gumball | The Matchmaker
 7:45 am | Amazing World of Gumball | The Slip
 8:00 am | Apple & Onion | Falafel's Fun Day
 8:15 am | Apple & Onion | Face Your Fears
 8:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 9:00 am | Teen Titans Go! | Who's Laughing Now
 9:15 am | Teen Titans Go! | Booty Scooty
 9:30 am | Teen Titans Go! | Oregon Trail
 9:45 am | Teen Titans Go! | Demon Prom
10:00 am | Craig of the Creek | Dog Decider
10:15 am | Craig of the Creek | Jacob of the Creek
10:30 am | Craig of the Creek | Bring Out Your Beast
10:45 am | Craig of the Creek | The Mystery of the Timekeeper
11:00 am | Teen Titans Go! | Super Hero Summer Camp
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro Part 4
12:15 pm | Teen Titans Go! | Rain on Your Wedding Day
12:30 pm | Mao Mao: Heroes of Pure Heart | Mao Mao's Nakey
12:45 pm | Mao Mao: Heroes of Pure Heart | Mao Mao's Bike Adventure
 1:00 pm | Amazing World of Gumball | The Deal
 1:15 pm | Amazing World of Gumball | The Future
 1:30 pm | Amazing World of Gumball | The Line
 1:45 pm | Amazing World of Gumball | The Ghouls
 2:00 pm | Amazing World of Gumball | The Singing
 2:15 pm | Amazing World of Gumball | The BFFS
 2:30 pm | Amazing World of Gumball | The Puppets
 2:45 pm | Amazing World of Gumball | The Agent
 3:00 pm | Craig of the Creek | Wildernessa
 3:15 pm | Craig of the Creek | The Shortcut
 3:30 pm | Craig of the Creek | Sunday Clothes
 3:45 pm | Craig of the Creek | Deep Creek Salvage
 4:00 pm | Total DramaRama | Sharing Is Caring
 4:15 pm | Total DramaRama | Apoca-lice Now
 4:30 pm | Total DramaRama | Ant We All Just Get Along
 4:45 pm | Total DramaRama | Gnome More Mister Nice Guy
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro Part 4
 5:15 pm | Teen Titans Go! | Rain on Your Wedding Day
 5:30 pm | Teen Titans Go! | Finally a Lesson
 5:45 pm | Teen Titans Go! | Arms Race With Legs
 6:00 pm | Apple & Onion | Hotdog's Movie Premiere
 6:15 pm | Apple & Onion | Selfish Shellfish
 6:30 pm | Amazing World of Gumball | The Nuisance
 6:45 pm | Amazing World of Gumball | The Wish
 7:00 pm | Amazing World of Gumball | The Best
 7:15 pm | Amazing World of Gumball | The Revolt
 7:30 pm | We Bare Bears | Crowbar Jones: Origins
 7:45 pm | We Bare Bears | The Gym