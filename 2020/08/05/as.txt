# 2020-08-05
 8:00 pm | Bob's Burgers | Moody Foodie
 8:30 pm | Bob's Burgers | Bad Tina
 9:00 pm | American Dad | Weiner of Our Discontent
 9:30 pm | American Dad | Daddy Queerest
10:00 pm | Rick and Morty | Get Schwifty
10:30 pm | Rick and Morty | The Ricks Must Be Crazy
11:00 pm | Family Guy | Lethal Weapons
11:30 pm | Family Guy | Kiss Seen 'Round the World
12:00 am | Robot Chicken | Ghandi Mulholland in: Plastic Doesn't Get Cancer
12:15 am | Robot Chicken | Gracie Purgatory in: That's How You Get Hemorrhoids
12:30 am | Aqua Teen | Robositter
12:45 am | Aqua Teen | The Mooninites: Final Mooning
 1:00 am | Three Busy Debras | Sleepover!
 1:15 am | Beef House | Boro
 1:30 am | Family Guy | Lethal Weapons
 2:00 am | Family Guy | Kiss Seen 'Round the World
 2:30 am | Rick and Morty | Get Schwifty
 3:00 am | Rick and Morty | The Ricks Must Be Crazy
 3:30 am | Robot Chicken | Ghandi Mulholland in: Plastic Doesn't Get Cancer
 3:45 am | Robot Chicken | Gracie Purgatory in: That's How You Get Hemorrhoids
 4:00 am | Loiter Squad | Sweet Chin Music
 4:15 am | Loiter Squad | Figure Four Leg Lock
 4:30 am | Aqua Teen | Robositter
 4:45 am | Aqua Teen | The Mooninites: Final Mooning
 5:00 am | Bob's Burgers | Moody Foodie
 5:30 am | Bob's Burgers | Bad Tina