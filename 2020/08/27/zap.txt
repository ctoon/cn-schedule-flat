# 2020-08-27
 6:00 am | Amazing World of Gumball | The Understanding
 6:15 am | Amazing World of Gumball | The Love
 6:30 am | Amazing World of Gumball | The Ad
 6:45 am | Amazing World of Gumball | The Stories
 7:00 am | Amazing World of Gumball | The Slip
 7:15 am | Amazing World of Gumball | The Awkwardness
 7:30 am | Amazing World of Gumball | The Drama
 7:45 am | Amazing World of Gumball | The Nest
 8:00 am | Unikitty | The Birthday to End All Birthdays
 8:30 am | Teen Titans Go! | Double Trouble; The Date
 9:00 am | Teen Titans Go! | Dude Relax; Laundry Day
 9:30 am | Teen Titans Go! | Ghost Boy; La Larva Amor
10:00 am | Craig of the Creek | Dinner at the Creek
10:15 am | Craig of the Creek | The Cardboard Identity
10:30 am | Craig of the Creek | Bug City
10:45 am | Craig of the Creek | Ancients of the Creek
11:00 am | Teen Titans Go! | Hey Pizza!; Gorilla
11:30 am | Teen Titans Go! | Curse of the Booty Scooty
11:45 am | Teen Titans Go! | TV Knight 5
12:00 pm | Teen Titans Go! | Them Soviet Boys
12:15 pm | Teen Titans Go! | Walk Away
12:30 pm | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
12:45 pm | Mao Mao: Heroes of Pure Heart | Orangusnake Begins
 1:00 pm | Amazing World of Gumball | The Future
 1:15 pm | Amazing World of Gumball | The Best
 1:30 pm | Amazing World of Gumball | The Ghouls
 1:45 pm | Amazing World of Gumball | The Worst
 2:00 pm | Amazing World of Gumball | The BFFS
 2:15 pm | Amazing World of Gumball | The List
 2:30 pm | Amazing World of Gumball | The Agent
 2:45 pm | Amazing World of Gumball | The Deal
 3:00 pm | Craig of the Creek | The Last Game of Summer
 3:15 pm | Craig of the Creek | Craig of the Beach
 3:30 pm | Craig of the Creek | Creek Cart Racers
 3:45 pm | Craig of the Creek | Sleepover at JP's
 4:00 pm | Total DramaRama | Dissing Cousins
 4:30 pm | Total DramaRama | Gum and Gummer
 4:45 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:00 pm | Teen Titans Go! | Girls Night In Part 1
 5:30 pm | Teen Titans Go! | Rain on Your Wedding Day
 5:45 pm | Teen Titans Go! | We're Off to Get Awards
 6:00 pm | Apple & Onion | Lil Noodle
 6:15 pm | Apple & Onion | The Inbetween
 6:30 pm | Amazing World of Gumball | The Wish
 6:45 pm | Amazing World of Gumball | The Roots
 7:00 pm | Amazing World of Gumball | The Revolt
 7:15 pm | Amazing World of Gumball | The Blame
 7:30 pm | We Bare Bears | Adopted
 7:45 pm | We Bare Bears | Chloe