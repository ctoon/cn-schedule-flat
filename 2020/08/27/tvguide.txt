# 2020-08-27
 6:00 am | Amazing World of Gumball | The Understanding; The Love
 6:30 am | Amazing World of Gumball | The Stories; The Ad
 7:00 am | Amazing World of Gumball | The Slip; The Awkwardness
 7:30 am | Amazing World of Gumball | The Drama; The Nest
 8:00 am | Unikitty | The Birthday to End All Birthdays
 8:30 am | Teen Titans Go! | Double Trouble; The Date
 9:00 am | Teen Titans Go! | Dude Relax!; Laundry Day
 9:30 am | Teen Titans Go! | Ghostboy; La Larva de Amor
10:00 am | Craig of the Creek | Dinner at the Creek; The Cardboard Identity
10:30 am | Craig of the Creek | Bug City; Ancients of the Creek
11:00 am | Teen Titans Go! | Hey Pizza!; Gorilla
11:30 am | Teen Titans Go! | Curse Of The Booty Scooty; Tv Knight 5
12:00 pm | Teen Titans Go! | Them Soviet Boys Walk Away
12:30 pm | Mao Mao, Heroes of Pure Heart | I Love You Mao Mao; Orangusnake Begins
 1:00 pm | Amazing World of Gumball | The Heart
 1:30 pm | Amazing World of Gumball | The Ghouls
 2:00 pm | Amazing World Of Gumball | The Bffs
 2:30 pm | Amazing World of Gumball | The Agent
 3:00 pm | Craig of the Creek | The Last Game of Summer
 3:15 pm | Craig of the Creek | Craig of the Beach
 3:30 pm | Craig of the Creek | Creek Cart Racers Sleepover at Jp's
 4:00 pm | Total DramaRama | Dissing Cousins
 4:30 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Gum And Gummer
 5:00 pm | Teen Titans Go! | Girls Night In
 5:30 pm | Teen Titans Go! | We're off to Get Awards Rain on Your Wedding Day
 6:00 pm | Apple & Onion | Lil Noodle; The Inbetween
 6:30 pm | Amazing World of Gumball | The Wish; The Roots
 7:00 pm | Amazing World of Gumball | The Revolt; The Blame
 7:30 pm | We Bare Bears | Adopted; Chloe