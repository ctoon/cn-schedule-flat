# 2020-08-14
 6:00 am | Teen Titans Go! | Super Summer Hero Camp
 7:00 am | Teen Titans Go! | Girls Night In
 7:30 am | Teen Titans Go! | Beast Boy's That's What's Up
 8:30 am | Teen Titans Go! | Oh Yeah!; Beast Girl
 9:00 am | Teen Titans Go! | Shrimps and Prime Rib; TV Knight 3
 9:30 am | Teen Titans Go! | Bro-Pocalypse
 9:45 am | Teen Titans Go! | Tv Knight; The Scoop
10:15 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
10:45 am | Teen Titans Go! | Night Begins to Shine 2: You're the One
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 1:00 pm | Amazing World of Gumball | The Lady; The Web
 1:30 pm | Amazing World of Gumball | The Sucker; The Inquisition
 2:00 pm | Amazing World of Gumball | The Cage; The Others
 2:30 pm | Amazing World of Gumball | The Rival; The Signature
 3:00 pm | Craig of the Creek | Dibs Court Escape From Family Dinner
 3:30 pm | Craig of the Creek | Monster in the Garden; The Great Fossil Rush
 4:00 pm | Total DramaRama | Cone in 60 Seconds; Look Who's Clocking
 4:30 pm | Total DramaRama | The Bad Guy Busters; Harold Swatter and the Goblet of Flies
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 6:00 pm | Apple & Onion | Tips Apple's Formula
 6:30 pm | Amazing World of Gumball | The Deal; The Heart
 7:00 pm | Amazing World of Gumball | The Line; The Ghouls
 7:30 pm | We Bare Bears | Bearz II Men Fire!