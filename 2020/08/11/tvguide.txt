# 2020-08-11
 6:00 am | Amazing World of Gumball | The Deal; The Heart
 6:30 am | Amazing World of Gumball | The Line; The Ghouls
 7:00 am | Amazing World of Gumball | The Singing; The Bffs
 7:30 am | Amazing World of Gumball | The Puppets; The Agent
 8:00 am | Apple & Onion | Hotdog's Movie Premiere Selfish Shellfish
 8:30 am | Teen Titans Go! | BBRAE
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice; Obinray
10:00 am | Craig of the Creek | Dibs Court Escape From Family Dinner
10:30 am | Craig of the Creek | Monster in the Garden; The Great Fossil Rush
11:00 am | Teen Titans Go! | Ones And Zeros; Wally T
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro Part 2
12:15 pm | Teen Titans Go! | The Titans Go Casual
12:30 pm | Mao Mao, Heroes of Pure Heart | Fright Wig Thumb War
 1:00 pm | Amazing World of Gumball | The Grades; The Drama
 1:30 pm | Amazing World of Gumball | The Uncle; The Master
 2:00 pm | Amazing World of Gumball | The Heist; The Silence
 2:30 pm | Amazing World of Gumball | The Petals; The Future
 3:00 pm | Craig of the Creek | Dog Decider Jacob of the Creek
 3:30 pm | Craig Of The Creek | The Mystery Of The Timekeeper; Bring Out Your Beast
 4:00 pm | Total DramaRama | Free Chili Simons Are Forever
 4:30 pm | Total DramaRama | Date; The Stop! Hamster Time
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro Part 2
 5:15 pm | Teen Titans Go! | The Titans Go Casual
 5:30 pm | Teen Titans Go! | Snuggle Time; Bbcyfshipbday
 6:00 pm | Apple & Onion | 4 on 1 Follow Your Dreams
 6:30 pm | Amazing World of Gumball | The Cycle; The Transformation
 7:00 pm | Amazing World of Gumball | The Stars; The Understanding
 7:30 pm | We Bare Bears | Vacation Christmas Movies