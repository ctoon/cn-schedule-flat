# 2020-08-17
 8:00 pm | Bob's Burgers | Broadcast Wagstaff School News
 8:30 pm | Bob's Burgers | My Fuzzy Valentine
 9:00 pm | American Dad | White Rice
 9:30 pm | American Dad | There Will Be Bad Blood
10:00 pm | Rick and Morty | The Rickchurian Mortydate
10:30 pm | YOLO Crystal Fantasy | A Very Extremely Very YOLO Christmas: Reloaded
10:45 pm | YOLO Crystal Fantasy | The Terry Cup
11:00 pm | Family Guy | The Cleveland-Loretta Quagmire
11:30 pm | Family Guy | Petarded
12:00 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
12:15 am | Robot Chicken | Musya Shakhtyorov in: Honeyboogers
12:30 am | Aqua Teen | Hand Banana
12:45 am | Aqua Teen | Party All the Time
 1:00 am | The Shivering Truth | The Burn Earner Spits
 1:15 am | JJ Villard's Fairy Tales | Boypunzel
 1:30 am | Family Guy | The Cleveland-Loretta Quagmire
 2:00 am | Family Guy | Petarded
 2:30 am | Rick and Morty | The Rickchurian Mortydate
 3:00 am | YOLO Crystal Fantasy | A Very Extremely Very YOLO Christmas: Reloaded
 3:15 am | YOLO Crystal Fantasy | The Terry Cup
 3:30 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
 3:45 am | Robot Chicken | Musya Shakhtyorov in: Honeyboogers
 4:00 am | Hot Package | Romance
 4:15 am | Mostly 4 Millennials | Distractions
 4:30 am | Aqua Teen | Hand Banana
 4:45 am | Aqua Teen | Party All the Time
 5:00 am | Bob's Burgers | Broadcast Wagstaff School News
 5:30 am | Bob's Burgers | My Fuzzy Valentine