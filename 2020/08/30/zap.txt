# 2020-08-30
 6:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 6:30 am | Bakugan: Armored Alliance | The Puppet Awesome Brawlers; The Return of Sophie
 7:00 am | Ben 10 | Tales From the Omnitrix
 7:15 am | Ben 10 | Tim Buk-TV
 7:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 8:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
 8:30 am | Teen Titans Go! | Super Robin; Tower Power
 9:00 am | Teen Titans Go! | Parasite; Starliar
 9:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
10:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
10:30 am | LEGO Ninjago | Ninjago Confidential
10:45 am | LEGO Ninjago | The Prodigal Father
11:00 am | LEGO Ninjago | The Temple of Madness
11:15 am | LEGO Ninjago | Game Over
11:30 am | Amazing World of Gumball | The Limit; The Game
12:00 pm | Amazing World of Gumball | The Promise; The Voice
12:30 pm | Amazing World of Gumball | The Boombox; The Castle
 1:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:30 pm | Amazing World of Gumball | The Internet; The Plan
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Kids; The Fan
 3:00 pm | Craig of the Creek | Plush Kingdom
 3:15 pm | Craig of the Creek | The Ice Pop Trio
 3:30 pm | Craig of the Creek | Pencil Break Mania
 3:45 pm | Craig of the Creek | The Last Game of Summer
 4:00 pm | DC Super Hero Girls | #Beeline
 4:15 pm | DC Super Hero Girls | #BurritoBucket
 4:30 pm | Amazing World of Gumball | The Name; The Extras
 5:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 5:30 pm | Amazing World of Gumball | The Fraud; The Void
 6:00 pm | Amazing World of Gumball | The Boss; The Move
 6:30 pm | Amazing World of Gumball | The Law; The Allergy
 7:00 pm | We Bare Bears | Imaginary Friend
 7:15 pm | We Bare Bears | Jean Jacket
 7:30 pm | We Bare Bears | Braces
 7:45 pm | We Bare Bears | Food Truck