# 2020-08-03
 6:00 am | Amazing World of Gumball | The Slide
 6:15 am | Amazing World of Gumball | The Pact
 6:30 am | Amazing World of Gumball | The Loophole
 6:45 am | Amazing World of Gumball | The Candidate
 7:00 am | Amazing World of Gumball | The Fuss
 7:15 am | Amazing World of Gumball | The Anybody
 7:30 am | Amazing World of Gumball | The News
 7:45 am | Amazing World of Gumball | The Shippening
 8:00 am | Apple & Onion | Hole in Roof
 8:15 am | Apple & Onion | Not Funny
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 9:30 am | Teen Titans Go! | Video Game References; Cool School
10:00 am | Craig of the Creek | In the Key of the Creek
10:15 am | Craig of the Creek | Big Pinchy
10:30 am | Craig of the Creek | Sunday Clothes
10:45 am | Craig of the Creek | Deep Creek Salvage
11:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:30 am | Teen Titans Go! | The Streak, Part 1
11:45 am | Teen Titans Go! | The Streak, Part 2
12:00 pm | Teen Titans Go! | I'm the Sauce
12:15 pm | Teen Titans Go! | The Spice Game
12:30 pm | Mao Mao: Heroes of Pure Heart | Head Chef
12:45 pm | Mao Mao: Heroes of Pure Heart | Ultraclops
 1:00 pm | Amazing World of Gumball | The Disaster
 1:15 pm | Amazing World of Gumball | The Re-Run
 1:30 pm | Amazing World of Gumball | The Stories
 1:45 pm | Amazing World of Gumball | The Cage
 2:00 pm | Amazing World of Gumball | The Friend
 2:15 pm | Amazing World of Gumball | The Rival
 2:30 pm | Amazing World of Gumball | The Boredom
 2:45 pm | Amazing World of Gumball | The One
 3:00 pm | Craig of the Creek | Craig of the Beach
 3:15 pm | Craig of the Creek | Secret Book Club
 3:30 pm | Craig of the Creek | You're It
 3:45 pm | Craig of the Creek | Jextra Perrestrial
 4:00 pm | Total DramaRama | Dissing Cousins
 4:30 pm | Total DramaRama | Supply Mom
 4:45 pm | Total DramaRama | Melter Skelter
 5:00 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 5:15 pm | Teen Titans Go! | Titan Saving Time
 5:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 5:45 pm | Teen Titans Go! | Master Detective
 6:00 pm | Apple & Onion | Champion
 6:15 pm | Apple & Onion | Onionless
 6:30 pm | Amazing World of Gumball | The Menu
 6:45 pm | Amazing World of Gumball | The Schooling
 7:00 pm | Amazing World of Gumball | The Outside
 7:15 pm | Amazing World of Gumball | The Intelligence
 7:30 pm | We Bare Bears | Spa Day
 7:45 pm | We Bare Bears | Pizza Band