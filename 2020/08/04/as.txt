# 2020-08-04
 8:00 pm | Bob's Burgers | Burgerboss
 8:30 pm | Bob's Burgers | Dr. Yap
 9:00 pm | American Dad | DeLorean Story-An
 9:30 pm | American Dad | Every Which Way But Lose
10:00 pm | Rick and Morty | Auto Erotic Assimilation
10:30 pm | Rick and Morty | Total Rickall
11:00 pm | Family Guy | And the Wiener Is...
11:30 pm | Family Guy | Death Lives
12:00 am | Robot Chicken | Petless M in: Cars are Couches of the Road
12:15 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
12:30 am | Aqua Teen | eDork
12:45 am | Aqua Teen | Little Brittle
 1:00 am | Three Busy Debras | Cartwheel Club
 1:15 am | Beef House | Prunes
 1:30 am | Family Guy | And the Wiener Is...
 2:00 am | Family Guy | Death Lives
 2:30 am | Rick and Morty | Auto Erotic Assimilation
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | Robot Chicken | Petless M in: Cars are Couches of the Road
 3:45 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
 4:00 am | Loiter Squad | Jungle Fever
 4:15 am | Loiter Squad | Roots
 4:30 am | Aqua Teen | eDork
 4:45 am | Aqua Teen | Little Brittle
 5:00 am | Bob's Burgers | Burgerboss
 5:30 am | Bob's Burgers | Dr. Yap