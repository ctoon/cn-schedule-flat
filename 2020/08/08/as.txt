# 2020-08-08
 8:00 pm | Dragon Ball Z Kai | A Bittersweet Victory! Until We Meet Again!
 8:30 pm | Dragon Ball Z Kai | Peace for the Future! The Spirit of Goku Is Forever!
 9:00 pm | American Dad | Man in the Moonbounce
 9:30 pm | American Dad | Shallow Vows
10:00 pm | American Dad | My Morning Straitjacket
10:30 pm | Rick and Morty | Anatomy Park
11:00 pm | Family Guy | Screwed the Pooch
11:30 pm | Family Guy | Peter Griffin: Husband, Father, Brother?
12:00 am | Dragon Ball Super | Bergamo the Crusher vs. Goku! Whose Strength Reaches the Wild Blue Yonder?!
12:30 am | JoJo's Bizarre Adventure: Golden Wind | Green Tea and Sanctuary Part 1
 1:00 am | Fire Force | The Blacksmith's Dream
 1:30 am | Black Clover | To the Heart Kingdom!
 2:00 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
 2:15 am | Ballmastrz: 9009 | Dance Dance Convolution?! Egos Warped by the Hair Gel of Hubris! Atonement, Now!
 2:30 am | Naruto:Shippuden | Fade into the Moonlight
 3:00 am | Samurai Jack | XCV
 3:30 am | Gemusetto Machu Picchu | Chapter 1A: Your Serve
 4:00 am | The Boondocks | The Story of Gangstalicious
 4:30 am | The Venture Brothers | Shadowman 9: In the Cradle of Destiny
 5:00 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 5:15 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 5:30 am | Joe Pera Talks With You | Joe Pera Has a Surprise for You
 5:45 am | Joe Pera Talks With You | Joe Pera Helps You Write