# 2020-08-09
 6:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 am | Bakugan: Armored Alliance | Dangerous Dealings; The Hunter Returns
 7:00 am | Ben 10 | It's Story Time; Party Poopers
 7:30 am | Teen Titans Go! | TV Knight 3; Tower Renovation
 8:00 am | Teen Titans Go! | Bro-Pocalypse; Quantum Fun
 8:30 am | Teen Titans Go! | The Scoop; The Fight
 9:00 am | Teen Titans Go! | Chicken in the Cradle Genie President
 9:30 am | Teen Titans Go! | Flashback
10:00 am | Teen Titans Go! | Kabooms
10:30 am | LEGO Ninjago | The Cliffs of Hysteria; The Maze of the Red Dragon
11:00 am | LEGO Ninjago: Masters of Spinjitzu | One Step Forward, Two Steps Back
11:15 am | LEGO Ninjago: Masters of Spinjitzu | Racer Seven
11:30 am | Amazing World of Gumball | The Line; The News
12:00 pm | Amazing World of Gumball | The Vase; The Singing
12:30 pm | Amazing World of Gumball | The Ollie; The Puppets
 1:00 pm | Amazing World of Gumball | The Lady; The Stars
 1:30 pm | Amazing World of Gumball | The Sorcerer; The Sucker
 2:00 pm | Amazing World of Gumball | The Box; The Cage
 2:30 pm | Amazing World of Gumball | The Outside; The Pact
 3:00 pm | Craig Of The Creek | Dog Decider; The Future Is Cardboard
 3:30 pm | Craig of the Creek | Bring Out Your Beast; Lost In The Sewer
 4:00 pm | DC Super Hero Girls | #DCSuperHeroBoys, Parts 1 & 2
 4:30 pm | Amazing World of Gumball | The Copycats; The Awareness
 5:00 pm | Amazing World of Gumball | The Catfish; The Candidate
 5:30 pm | Amazing World of Gumball | The Anybody; The Matchmaker
 6:00 pm | Amazing World of Gumball | The Grades; The Shippening
 6:30 pm | Amazing World of Gumball | The Diet; The Stink
 7:00 pm | We Bare Bears | Bro Brawl; Escandalosos
 7:30 pm | We Bare Bears | The Hurricane Hal Mall