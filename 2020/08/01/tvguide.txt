# 2020-08-01
 6:00 am | Teen Titans Go! | Rad Dudes With Bad Tudes; History Lesson
 6:30 am | Teen Titans Go! | The Bottle Episode Art of Ninjutsu
 7:00 am | Teen Titans Go! | Think About Your Future; Pyramid Scheme
 7:30 am | Teen Titans Go! | Finally a Lesson Booty Scooty
 8:00 am | Teen Titans Go! | Arms Race With Legs; Who's Laughing Now
 8:00 am | Teen Titans Go! | Arms Race With Legs; Who's Laughing Now
 8:15 am | Teen Titans Go! | Oregon Trail; Obinray
 8:30 am | Teen Titans Go! | Oregon Trail; Obinray
 9:00 am | Teen Titans Go! | Walk Away; Record Book
 9:30 am | Teen Titans Go! | Magic Man Bat Scouts
10:00 am | Craig of the Creek | The Jinxening; Craig of the Beach
10:30 am | Craig of the Creek | in the Key of the Creek; The Ground Is Lava!
11:00 am | Amazing World of Gumball | The Signature; The Points
11:30 am | Amazing World of Gumball | The Bus; The Gift
12:00 pm | Amazing World of Gumball | The Night; The Apprentice
12:30 pm | Amazing World of Gumball | The Misunderstanding; The Check
 1:00 pm | Amazing World of Gumball | The Pest; The Roots
 1:30 pm | Amazing World of Gumball | The Blame; The Hug
 2:00 pm | Amazing World of Gumball | The Traitor; The Stories
 2:30 pm | Amazing World of Gumball | The Detective; The Routine
 3:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice; Snuggle Time
 3:30 pm | Teen Titans Go! | Oh Yeah!; Wally T
 4:00 pm | Teen Titans Go! | Island Adventures
 5:00 pm | Craig of the Creek | The Other Side: The Tournament
 5:30 pm | Craig of the Creek | You're It; Too Many Treasures
 6:00 pm | Amazing World of Gumball | The Fury; The Parking
 6:30 pm | Amazing World of Gumball | The Compilation; The Upgrade
 7:00 pm | We Bare Bears | Tote Life; Emergency
 7:30 pm | We Bare Bears | The Perfect Tree; Paperboyz