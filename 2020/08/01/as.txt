# 2020-08-01
 8:00 pm | Dragon Ball Z Kai | A Hero's Sacrifice! Last Chance to Save the World!
 8:30 pm | Dragon Ball Z Kai | Combine Your Strength! The Final Kamehame-Ha!
 9:00 pm | American Dad | Live and Let Fry
 9:30 pm | American Dad | Roy Rodgers McFreely
10:00 pm | American Dad | Jack's Back
10:30 pm | Rick and Morty | Lawnmower Dog
11:00 pm | Family Guy | Thin White Line Part 1
11:30 pm | Family Guy | Brian Does Hollywood Part 2
12:00 am | Dragon Ball Super | Awaken Your Dormant Fighting Spirit! Gohan's Fight!!
12:30 am | JoJo's Bizarre Adventure: Golden Wind | Verso il Colosseo di Roma
 1:00 am | Mob Psycho 100 | Mob and Reigen ~A Giant Tsuchinoko Appears~
 1:30 am | Black Clover | Clues
 2:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 2:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 2:30 am | Naruto:Shippuden | The Heart's Eye
 3:00 am | Samurai Jack | XCIV
 3:30 am | The Shivering Truth | Holeways
 3:45 am | The Shivering Truth | Tow and Shell
 4:00 am | The Boondocks | The Real
 4:30 am | The Venture Brothers | The Saphrax Protocol
 5:00 am | Joe Pera Talks With You | Joe Pera Waits With You
 5:15 am | Joe Pera Talks With You | Joe Pera Guides You Through the Dark
 5:30 am | Joe Pera Talks With You | Joe Pera Takes You to the Grocery Store
 5:45 am | Joe Pera Talks With You | Joe Pera Goes to Dave Wojcek's Bachelor Party with You