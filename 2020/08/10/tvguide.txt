# 2020-08-10
 6:00 am | Amazing World of Gumball | The Nuisance; The Wish
 6:30 am | Amazing World Of Gumball | The Best; The Revolt
 7:00 am | Amazing World of Gumball | The Worst; The Mess
 7:30 am | Amazing World of Gumball | The List; The Possession
 8:00 am | Apple & Onion | Apple's in Trouble Sausage and Sweetie Smash
 8:30 am | Teen Titans Go! | Secret Garden Brain Percentages
 9:00 am | Teen Titans Go! | Bottle Episode; BL4Z3
 9:30 am | Teen Titans Go! | Pyramid Scheme; Lication
10:00 am | Craig Of The Creek | Wildernessa; The Shortcut
10:30 am | Craig of the Creek | The Ground Is Lava!
10:45 am | Teen Titans Go! | Night Begins to Shine 2: You're the One
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro Part 1
12:15 pm | Teen Titans Go! | Magic Man
12:30 pm | Mao Mao, Heroes of Pure Heart | Outfoxed Baost in Show
 1:00 pm | Amazing World of Gumball | The Cycle; The Transformation
 1:30 pm | Amazing World of Gumball | The Stars; The Understanding
 2:00 pm | Amazing World of Gumball | The Box; The Ad
 2:30 pm | Amazing World of Gumball | The Matchmaker; The Slip
 3:00 pm | Craig Of The Creek | The Curse; Alone Quest
 3:30 pm | Craig Of The Creek | Memories Of Bobby; The Future Is Cardboard
 4:00 pm | Total DramaRama | Camping Is In Tents
 4:30 pm | Total DramaRama | Cluckwork Orange Mutt Ado About Owen
 5:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro Part 1
 5:15 pm | Teen Titans Go! | Magic Man
 5:30 pm | Teen Titans Go! | Think About Your Future; Throne of Bones
 6:00 pm | Apple & Onion | Falafel's Fun Day Face Your Fears
 6:30 pm | Amazing World of Gumball | The Lady; The Web
 7:00 pm | Amazing World of Gumball | The Sucker; The Inquisition
 7:30 pm | We Bare Bears | Park; The Tabes & Charlie