# 2020-03-07
 8:00 pm | My Hero Academia | Infinite 100%
 8:30 pm | My Hero Academia | Bright Future
 9:00 pm | American Dad | Not Particularly Desperate Housewives
 9:30 pm | American Dad | Chimdale
10:00 pm | Bob's Burgers | Paraders of the Lost Float
10:30 pm | Family Guy | Emission Impossible
11:00 pm | Family Guy | To Love and Die in Dixie
11:30 pm | My Hero Academia | Smoldering Flames
12:00 am | Sword Art Online Alicization War of Underworld | Blood and Life
12:30 am | Demon Slayer: Kimetsu no Yaiba | Hinokami
 1:00 am | Food Wars! | The Battle That Follows the Seasons
 1:30 am | Black Clover | Battlefield Dancer
 3:00 am | JoJo's Bizarre Adventure: Golden Wind | Babyhead
 3:00 am | Daylight Savings Time Adjustment - Spring | 
 3:30 am | Naruto:Shippuden | One Worth Betting On
 4:00 am | Genndy Tartakovksy's Primal | River of Snakes
 4:30 am | Samurai Jack | XCII
 5:00 am | Aqua TV Show Show | Muscles
 5:15 am | Aqua TV Show Show | The Dudies
 5:30 am | Bob's Burgers | Paraders of the Lost Float