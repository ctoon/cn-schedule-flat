# 2020-03-22
 6:00 am | Amazing World of Gumball | The Agent
 6:15 am | Amazing World of Gumball | The Sale
 6:30 am | Bakugan: Battle Planet | Trouble Busters!/French Fry Wars
 7:00 am | Transformers: Cyberverse | The Loop
 7:15 am | Transformers: Cyberverse | The Dead End
 7:30 am | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 7:45 am | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
 8:00 am | Mao Mao: Heroes of Pure Heart | Enemy Mime
 8:15 am | Mao Mao: Heroes of Pure Heart | Breakup
 8:30 am | Teen Titans Go! | Cartoon Feud
 8:45 am | Teen Titans Go! | The Viewers Decide
 9:00 am | Teen Titans Go! | Bat Scouts
 9:15 am | Teen Titans Go! | Walk Away
 9:30 am | Teen Titans Go! | We're Off to Get Awards
 9:45 am | Teen Titans Go! | Record Book
10:00 am | Ben 10 | Bottomless Ben
10:15 am | Ben 10 | Tales From the Omnitrix
10:30 am | Power Players | Bowling for Ballers
10:45 am | Power Players | Countdown
11:00 am | Amazing World of Gumball | The Decisions
11:15 am | Amazing World of Gumball | The Routine
11:30 am | Amazing World of Gumball | The Web
11:45 am | Amazing World of Gumball | The Inquisition
12:00 pm | Amazing World of Gumball | The Third; The Debt
12:30 pm | Amazing World of Gumball | The Pressure; The Painting
 1:00 pm | Total DramaRama | All Up in Your Drill
 1:15 pm | Total DramaRama | Snots Landing
 1:30 pm | Total DramaRama | Snow Way Out
 1:45 pm | Total DramaRama | Paint That a Shame
 2:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 3:00 pm | Teen Titans Go! | Girls Night In
 3:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 4:00 pm | DC Super Hero Girls | #DinnerForFive
 4:15 pm | DC Super Hero Girls | #Retreat!
 4:30 pm | Unikitty | Cast Aside the Truth
 4:45 pm | Unikitty | Guardian of the Unikingdom
 5:00 pm | Amazing World of Gumball | The Responsible; The Dress
 5:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 6:00 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 6:30 pm | Teen Titans Go! | Double Trouble; The Date
 7:00 pm | Teen Titans | The Apprentice
 7:30 pm | Teen Titans | The Apprentice