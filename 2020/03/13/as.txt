# 2020-03-13
 8:00 pm | Final Space | Chapter Three
 8:30 pm | Final Space | Chapter Four
 9:00 pm | Bob's Burgers | The Hauntening
 9:30 pm | Rick and Morty | Look Who's Purging Now
10:00 pm | American Dad | The American Dad After School Special
10:30 pm | American Dad | Failure Is Not a Factory-Installed Option
11:00 pm | Family Guy | Family Guy Viewer Mail #1
11:30 pm | Family Guy | When You Wish Upon a Weinstein
12:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
12:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
12:30 am | Mike Tyson Mysteries | The Gift That Keeps on Giving
12:45 am | Mike Tyson Mysteries | Your Old Man
 1:00 am | The Jellies | Pilot
 1:15 am | Lazor Wulf | Lane Occupied
 1:30 am | Family Guy | Family Guy Viewer Mail #1
 2:00 am | Family Guy | When You Wish Upon a Weinstein
 2:30 am | American Dad | The American Dad After School Special
 3:00 am | Rick and Morty | Look Who's Purging Now
 3:30 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 3:45 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 4:00 am | As Seen on Adult Swim | As Seen On Adult Swim
 4:15 am | Digikiss | Digikiss
 4:30 am | Mike Tyson Mysteries | The Gift That Keeps on Giving
 4:45 am | Mike Tyson Mysteries | Your Old Man
 5:00 am | American Dad | Failure Is Not a Factory-Installed Option
 5:30 am | Bob's Burgers | The Hauntening