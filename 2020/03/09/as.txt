# 2020-03-09
 8:00 pm | Home Movies | Those Bitches Tried to Cheat Me
 8:30 pm | American Dad | Rough Trade
 9:00 pm | American Dad | Finances with Wolves
 9:30 pm | Rick and Morty | Total Rickall
10:00 pm | Bob's Burgers | Adventures in Chinchilla-Sitting
10:30 pm | Bob's Burgers | The Runway Club
11:00 pm | Family Guy | Screwed the Pooch
11:30 pm | Family Guy | Peter Griffin: Husband, Father, Brother?
12:00 am | The Boondocks | Granddad's Fight
12:30 am | Robot Chicken | Shall I Visit the Dinosaurs?
12:45 am | Robot Chicken | What Can You Tell Me About Butt Rashes?
 1:00 am | Tim and Eric Awesome Show, Great Job | Hamburger
 1:15 am | Tim & Eric's Bedtime Stories | Angel Man
 1:30 am | Family Guy | Screwed the Pooch
 2:00 am | Family Guy | Peter Griffin: Husband, Father, Brother?
 2:30 am | American Dad | Rough Trade
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | Robot Chicken | Shall I Visit the Dinosaurs?
 3:45 am | Robot Chicken | What Can You Tell Me About Butt Rashes?
 4:00 am | The Jellies | The Gameshow
 4:15 am | Lazor Wulf | We Good!
 4:30 am | Tim and Eric Awesome Show, Great Job | Hamburger
 4:45 am | Tim & Eric's Bedtime Stories | Angel Man
 5:00 am | Bob's Burgers | Adventures in Chinchilla-Sitting
 5:30 am | Bob's Burgers | The Runway Club