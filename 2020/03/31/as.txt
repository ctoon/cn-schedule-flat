# 2020-03-31
 8:00 pm | Home Movies | Pizza Club
 8:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 9:00 pm | Bob's Burgers | Aquaticism
 9:30 pm | Rick and Morty | Lawnmower Dog
10:00 pm | American Dad | Oedipal Panties
10:30 pm | American Dad | Widowmaker
11:00 pm | Family Guy | Mother Tucker
11:30 pm | Family Guy | Hell Comes to Quahog
12:00 am | No Data Available | 708
 1:15 am | No Data Available | SWIM
 2:15 am | No Data Available | FUN
 3:30 am | Robot Chicken | Hopefully Salt
 3:45 am | Robot Chicken | Yogurt in a Bag
 4:00 am | The Jellies | Walla Walla Wallabees B
 4:15 am | Lazor Wulf | Prolly for the Best
 4:30 am | Aqua Unit Patrol Squad 1 | Freedom Cobra
 4:45 am | Squidbillies | Vicki
 5:00 am | Bob's Burgers | The Grand Mama-Pest Hotel
 5:30 am | Bob's Burgers | Aquaticism