# 2020-03-15
 8:00 pm | Final Space | Chapter Five
 8:30 pm | Final Space | Chapter Six
 9:00 pm | American Dad | Stan & Francine & Connie & Ted
 9:30 pm | American Dad | Rabbit Ears
10:00 pm | Family Guy | The Dating Game
10:30 pm | Family Guy | Cop and a Half-Wit
11:00 pm | Rick and Morty | Rickmancing the Stone
11:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:00 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
12:15 am | Ballmastrz: 9009 | Dance Dance Convolution?! Egos Warped by the Hair Gel of Hubris! Atonement, NOW!
12:30 am | Tigtone | Tigtone and the Freaks of Love
12:45 am | The Shivering Truth | Fowl Flow
 1:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Pack a Lunch
 1:15 am | Joe Pera Talks With You | Joe Pera Talks with You on the First Day of School
 1:30 am | American Dad | Stan & Francine & Connie & Ted
 2:00 am | Family Guy | The Dating Game
 2:30 am | Family Guy | Cop and a Half-Wit
 3:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:30 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
 3:45 am | Ballmastrz: 9009 | Dance Dance Convolution?! Egos Warped by the Hair Gel of Hubris! Atonement, NOW!
 4:00 am | Tigtone | Tigtone and the Freaks of Love
 4:15 am | The Shivering Truth | Fowl Flow
 4:30 am | Joe Pera Talks With You | Joe Pera Shows You How to Pack a Lunch
 4:45 am | Joe Pera Talks With You | Joe Pera Talks with You on the First Day of School
 5:00 am | American Dad | Rabbit Ears
 5:30 am | Harvey Birdman | Trio's Company
 5:45 am | Harvey Birdman | The Devlin Made Me Do It