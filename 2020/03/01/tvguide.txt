# 2020-03-01
 6:00 am | Amazing World of Gumball | The Best; The Heist
 6:30 am | Bakugan: Battle Planet | The Mysterious Boy; A New Power
 7:00 am | Teen Titans Go! | Teen Titans Vroom
 7:30 am | Teen Titans Go! | Pyramid Scheme; Who's Laughing Now
 8:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 8:30 am | Teen Titans Go! | Secret Garden; Bottle Episode
 9:00 am | Teen Titans Go! | Garage Sale; Squash & Stretch
 9:30 am | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
10:00 am | Ben 10 | The Greatest Lake; Mud On The Run
10:30 am | Power Players | Saving Private Masko; On Your Toes
11:00 am | Amazing World of Gumball | The Stars; The Cycle
11:30 am | Amazing World Of Gumball | The Catfish; The Weirdo
12:00 pm | Amazing World Of Gumball | The Matchmaker; The Copycats
12:30 pm | Amazing World Of Gumball | The Grades; The Outside
 1:00 pm | Total DramaRama | An Egg-stremely Bad Idea; Exercising The Demons
 1:30 pm | Total DramaRama | Lie-ranosaurus Wrecked; The Tooth About Zombies
 2:00 pm | Teen Titans Go! | Walk Away; We're Off To Get Awards
 2:30 pm | Teen Titans Go! | Bat Scouts; Butt Atoms
 3:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 3:30 pm | Teen Titans Go! | How's This For A Special? Spaaaace; My Name Is Jose
 4:00 pm | DC Super Hero Girls | #HateTriangle; #AdventuresInBunnysitting
 4:30 pm | Unikitty | Bedtime Stories; Grown Up Stuff
 5:00 pm | Amazing World Of Gumball | The Diet; The Console
 5:30 pm | Amazing World of Gumball | The Ex; The Sorcerer
 6:00 pm | Teen Titans Go! | Beast Boy's That's What's Up; The Metric System Vs. Freedom
 7:00 pm | Teen Titans | Deep Six
 7:30 pm | Teen Titans | Mask