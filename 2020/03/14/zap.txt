# 2020-03-14
 6:00 am | Amazing World of Gumball | The Deal
 6:15 am | Amazing World of Gumball | The Shippening
 6:30 am | Amazing World of Gumball | The Line
 6:45 am | Amazing World of Gumball | The Brain
 7:00 am | Amazing World of Gumball | The Singing
 7:15 am | Amazing World of Gumball | The Stink
 7:30 am | Amazing World of Gumball | The Puppets
 7:45 am | Amazing World of Gumball | The Awareness
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 8:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 9:00 am | Total DramaRama | Mutt Ado About Owen
 9:15 am | Total DramaRama | Too Much of a Goo'd Thing
 9:30 am | Apple & Onion | Dragonhead
 9:45 am | Apple & Onion | The Fly
10:00 am | Teen Titans Go! | Magic Man
10:15 am | Teen Titans Go! | Record Book
10:30 am | ThunderCats Roar | Panthro Plagiarized!
10:45 am | ThunderCats Roar | Warrior Maiden Invasion
11:00 am | Teen Titans Go! | Ones and Zeroes
11:15 am | Teen Titans Go! | Demon Prom
11:30 am | Teen Titans Go! | Career Day
11:45 am | Teen Titans Go! | BBCYFSHIPBDAY
12:00 pm | Teen Titans Go! | TV Knight 2
12:15 pm | Teen Titans Go! | Mo' Money Mo' Problems
12:30 pm | Teen Titans Go! | The Academy
12:45 pm | Teen Titans Go! | Beast Girl
 1:00 pm | Amazing World of Gumball | The Lady
 1:15 pm | Amazing World of Gumball | The Parents
 1:30 pm | Amazing World of Gumball | The Sucker
 1:45 pm | Amazing World of Gumball | The Founder
 2:00 pm | Amazing World of Gumball | The Cage
 2:15 pm | Amazing World of Gumball | The Schooling
 2:30 pm | Amazing World of Gumball | The Rival
 2:45 pm | Amazing World of Gumball | The Intelligence
 3:00 pm | Teen Titans Go! | Throne of Bones
 3:15 pm | Teen Titans Go! | TV Knight 3
 3:30 pm | Teen Titans Go! | Bro-Pocalypse
 3:45 pm | Teen Titans Go! | The Scoop
 4:00 pm | Craig of the Creek | Jessica's Trail
 4:15 pm | Craig of the Creek | Dinner at the Creek
 4:30 pm | Victor and Valentino | Tree Buds
 4:45 pm | Victor and Valentino | On Nagual Hill
 5:00 pm | Total DramaRama | Soother or Later
 5:15 pm | Total DramaRama | The Never Gwending Story
 5:30 pm | Total DramaRama | Duncan Disorderly
 5:45 pm | Total DramaRama | Melter Skelter
 6:00 pm | Amazing World of Gumball | The One
 6:15 pm | Amazing World of Gumball | The Potion
 6:30 pm | Amazing World of Gumball | The Vegging
 6:45 pm | Amazing World of Gumball | The Spinoffs
 7:00 pm | Steven Universe: Future | Together Forever
 7:15 pm | Steven Universe: Future | Growing Pains
 7:30 pm | Infinity Train | The Mall Car
 7:45 pm | Infinity Train | The Wasteland