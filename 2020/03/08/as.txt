# 2020-03-08
 8:00 pm | Final Space | Chapter Three
 8:30 pm | Final Space | Chapter Four
 9:00 pm | American Dad | Fantasy Baseball
 9:30 pm | American Dad | I Am The Jeans: The Gina Lavetti Story
10:00 pm | Family Guy | Peter's Def Jam
10:30 pm | Family Guy | The Finer Strings
11:00 pm | Rick and Morty | The Rickshank Rickdemption
11:30 pm | Rick and Morty | Rattlestar Ricklactica
12:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
12:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
12:30 am | Tigtone | Tigtone and the Wizard Hunt
12:45 am | The Shivering Truth | Tow and Shell
 1:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Do Good Fashion
 1:30 am | American Dad | Fantasy Baseball
 2:00 am | Family Guy | Peter's Def Jam
 2:30 am | Family Guy | The Finer Strings
 3:00 am | Rick and Morty | Rattlestar Ricklactica
 3:30 am | Rick and Morty | The Rickshank Rickdemption
 4:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 4:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 4:30 am | Tigtone | Tigtone and the Wizard Hunt
 4:45 am | The Shivering Truth | Tow and Shell
 5:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Do Good Fashion
 5:30 am | Harvey Birdman | X, The Exterminator
 5:45 am | Harvey Birdman | Blackwatch Plaid