# 2020-03-27
 8:00 pm | Final Space | Chapter Seven
 8:30 pm | Final Space | Chapter Eight
 9:00 pm | Bob's Burgers | Ex Machtina
 9:30 pm | Rick and Morty | The Rickchurian Mortydate
10:00 pm | American Dad | 42-Year-Old Virgin
10:30 pm | American Dad | Surro-Gate
11:00 pm | Family Guy | Petergeist
11:30 pm | Family Guy | Untitled Griffin Family History
12:00 am | Ballmastrz: 9009 | Infinite Hugs: Cold Embrace of the Bloodless Progenitors!
12:15 am | Ballmastrz: 9009 | Shameful Disease of Yackety Yack! Don't Talk Back! Be Silenced Forever!
12:30 am | Ballmastrz: 9009 | Big Boy Body, Bigger Baby Boy Heart! Can You Endure the Pain of Love? Babyball, Discover It Now!
12:45 am | Ballmastrz: 9009 | Children of the Night Serenade Wet Nurse of Reprisal; Scream, Bloodsucker, Scream!
 1:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 1:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 1:30 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
 1:45 am | Ballmastrz: 9009 | Dance Dance Convolution?! Egos Warped by the Hair Gel of Hubris! Atonement, NOW!
 2:00 am | Ballmastrz: 9009 | Don't Let a Big Head Give You The Championship Blues! You Can Do It, Leptons! Try Your Best To Win!!
 2:15 am | Ballmastrz: 9009 | Onward, True Blue Friends Win Eternal; Paladin of the Heavens, Start Today!
 2:30 am | American Dad | 42-Year-Old Virgin
 3:00 am | Final Space | Chapter Seven
 3:30 am | Final Space | Chapter Eight
 4:00 am | As Seen on Adult Swim | As Seen on Adult Swim
 4:15 am | FishCenter | FishCenter
 4:30 am | Rick and Morty | The Rickchurian Mortydate
 5:00 am | American Dad | Surro-Gate
 5:30 am | Bob's Burgers | Ex Machtina