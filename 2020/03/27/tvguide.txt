# 2020-03-27
 6:00 am | Amazing World of Gumball | The Stories; The Catfish
 6:30 am | Amazing World of Gumball | The Guy; The Cycle
 7:00 am | Mao Mao, Heroes of Pure Heart | Baost in Show; All by Mao Self
 7:30 am | Mao Mao, Heroes of Pure Heart | Fright Wig; Ultraclops
 8:00 am | Teen Titans Go! | Parasite; Starliar
 8:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
 9:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 9:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
10:00 am | Craig of the Creek | Camper on the Run; Escape From Family Dinner
10:30 am | Craig of the Creek | Kelsey the Author; Monster in the Garden
11:00 am | Teen Titans Go! | TV Knight 5; The Academy
11:30 am | Teen Titans Go! | Walk Away; Bat Scouts
12:00 pm | Victor and Valentino | Cat-Pocalypse; Folk Art Foes
12:30 pm | Victor and Valentino | Band for Life; Dead Ringer
 1:00 pm | Steven Universe: The Movie | 
 3:00 pm | Steven Universe Future | Little Homeschool; Guidance
 3:30 pm | Steven Universe Future | Rose Buds; Volleyball
 4:00 pm | Steven Universe Future | Bluebird; A Very Special Episode
 4:30 pm | Steven Universe Future | Snow Day; Why So Blue?
 5:00 pm | Steven Universe Future | Little Graduation; Prickly Pair
 5:30 pm | Steven Universe Future | In Dreams; Bismuth Casual
 6:00 pm | Steven Universe Future | Together Forever; Growing Pains
 6:30 pm | Steven Universe Future | Mr. Universe; Fragments
 7:00 pm | Steven Universe Future | Homeworld Bound
 7:15 pm | Steven Universe Future | Everything's Fine
 7:30 pm | Steven Universe Future | I Am My Monster
 7:45 pm | Steven Universe Future | The Future