# 2020-03-26
 6:00 am | Amazing World of Gumball | The Origins
 6:15 am | Amazing World of Gumball | The Origins
 6:30 am | Amazing World of Gumball | The Compilation
 6:45 am | Amazing World of Gumball | The Console
 7:00 am | Mao Mao: Heroes of Pure Heart | Captured Clops
 7:15 am | Mao Mao: Heroes of Pure Heart | Enemy Mime
 7:30 am | Mao Mao: Heroes of Pure Heart | Flyaway
 7:45 am | Mao Mao: Heroes of Pure Heart | Breakup
 8:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 8:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 9:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
 9:30 am | Teen Titans Go! | Super Robin; Tower Power
10:00 am | Craig of the Creek | Summer Wish
10:15 am | Craig of the Creek | Wildernessa
10:30 am | Craig of the Creek | Turning the Tables
10:45 am | Craig of the Creek | Sunday Clothes
11:00 am | Teen Titans Go! | Girls Night In
11:30 am | Teen Titans Go! | The Great Disaster
11:45 am | Teen Titans Go! | The Viewers Decide
12:00 pm | Victor and Valentino | Welcome to the Underworld
12:30 pm | Victor and Valentino | The Great Bongo Heist
12:45 pm | Victor and Valentino | Escape From Bebe Bay
 1:00 pm | Amazing World of Gumball | The Test
 1:15 pm | Amazing World of Gumball | The Diet
 1:30 pm | Amazing World of Gumball | The Slide
 1:45 pm | Amazing World of Gumball | The Ex
 2:00 pm | Amazing World of Gumball | The Loophole
 2:15 pm | Amazing World of Gumball | The Uncle
 2:30 pm | Amazing World of Gumball | The Fuss
 2:45 pm | Amazing World of Gumball | The Heist
 3:00 pm | Total DramaRama | Having the Timeout of Our Lives
 3:15 pm | Total DramaRama | Snow Way Out
 3:30 pm | Total DramaRama | Hic Hic Hooray
 3:45 pm | Total DramaRama | All Up in Your Drill
 4:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 5:00 pm | Craig of the Creek | Fort Williams
 5:15 pm | Craig of the Creek | Cousin of the Creek
 5:30 pm | Craig of the Creek | Kelsey the Elder
 5:45 pm | Craig of the Creek | You're It
 6:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 pm | Amazing World of Gumball | The Romantic
 7:15 pm | Amazing World of Gumball | The Roots
 7:30 pm | Apple & Onion | Block Party
 7:45 pm | Apple & Onion | Gyranoid of the Future