# 2020-03-26
 6:00 am | Amazing World of Gumball | The Origins; The Origins Part 2
 6:30 am | Amazing World of Gumball | The Compilation; The Console
 7:00 am | Mao Mao, Heroes of Pure Heart | Captured Clops Enemy Mime
 7:30 am | Mao Mao, Heroes of Pure Heart | Flyaway; Breakup
 8:00 am | Teen Titans Go! | Ghostboy; La Larva de Amor
 8:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 9:00 am | Teen Titans Go! | Girl's Night Out; You're Fired
 9:30 am | Teen Titans Go! | Super Robin; Tower Power
10:00 am | Craig of the Creek | Summer Wish; Wildernessa
10:30 am | Craig of the Creek | Turning the Tables; Sunday Clothes
11:00 am | Teen Titans Go! | Girls Night In
11:30 am | Teen Titans Go! | The Viewers Decide; The Great Disaster
12:00 pm | Victor And Valentino | Welcome to the Underworld
12:30 pm | Victor And Valentino | The Great Bongo Heist; Escape From Bebe Bay
 1:00 pm | Amazing World of Gumball | The Diet; The Test
 1:30 pm | Amazing World of Gumball | The Ex; The Slide
 2:00 pm | Amazing World Of Gumball | The Loophole; The Uncle
 2:30 pm | Amazing World of Gumball | The Heist; The Fuss
 3:00 pm | Total DramaRama | Snow Way Out; Having The Timeout Of Our Lives
 3:30 pm | Total DramaRama | Hic Hic Hooray; All Up In Your Drill
 4:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 5:00 pm | Craig of the Creek | Fort Williams; Cousin of the Creek
 5:30 pm | Craig of the Creek | Kelsey the Elder; You're It
 6:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 pm | Amazing World of Gumball | The Romantic; The Roots
 7:30 pm | Apple & Onion | Block Party; Gyranoid of the Future