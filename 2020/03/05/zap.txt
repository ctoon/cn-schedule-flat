# 2020-03-05
 6:00 am | Amazing World of Gumball | The Words; The Apology
 6:30 am | Amazing World of Gumball | The Limit; The Game
 7:00 am | Amazing World of Gumball | The Watch; The Bet
 7:30 am | Amazing World of Gumball | The Bumpkin; The Flakers
 8:00 am | Teen Titans Go! | Squash & Stretch
 8:15 am | Teen Titans Go! | BBSFBDAY
 8:30 am | Teen Titans Go! | Two Parter: Part One
 8:45 am | Teen Titans Go! | Two Parter: Part Two
 9:00 am | Teen Titans Go! | History Lesson
 9:15 am | Teen Titans Go! | Who's Laughing Now
 9:30 am | Teen Titans Go! | The Art of Ninjutsu
 9:45 am | Teen Titans Go! | Oregon Trail
10:00 am | Craig of the Creek | Dog Decider
10:15 am | Craig of the Creek | Kelsey the Elder
10:30 am | Craig of the Creek | Bring Out Your Beast
10:45 am | Craig of the Creek | Sour Candy Trials
11:00 am | Teen Titans Go! | The Croissant
11:15 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
11:30 am | Teen Titans Go! | The Spice Game
11:45 am | Teen Titans Go! | Secret Garden
12:00 pm | Victor and Valentino | Cleaning Day
12:15 pm | Victor and Valentino | Cuddle Monster
12:30 pm | Victor and Valentino | The Babysitter
12:45 pm | Victor and Valentino | Boss for a Day
 1:00 pm | Amazing World of Gumball | The Club; The Wand
 1:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 2:00 pm | Amazing World of Gumball | The Quest; The Spoon
 2:30 pm | Amazing World of Gumball | The Car; The Curse
 3:00 pm | Teen Titans Go! | A Farce
 3:15 pm | Teen Titans Go! | Obinray
 3:30 pm | Teen Titans Go! | Animals: It's Just a Word!
 3:45 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 4:00 pm | Teen Titans Go! | Garage Sale
 4:15 pm | Teen Titans Go! | Wally T
 4:30 pm | Teen Titans Go! | BBBDay!
 4:45 pm | Teen Titans Go! | TV Knight
 5:00 pm | Total DramaRama | Bananas & Cheese
 5:15 pm | Total DramaRama | Snow Way Out
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Total DramaRama | All Up in Your Drill
 6:00 pm | Teen Titans Go! | Inner Beauty of a Cactus
 6:15 pm | Teen Titans Go! | Movie Night
 6:30 pm | Teen Titans Go! | The Streak, Part 1
 6:45 pm | Teen Titans Go! | The Streak, Part 2
 7:00 pm | Amazing World of Gumball | The Fridge; The Remote
 7:30 pm | Amazing World of Gumball | The Flower; The Banana