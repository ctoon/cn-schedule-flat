# 2020-03-16
 8:00 pm | Home Movies | Politics
 8:30 pm | American Dad | Dungeons and Wagons
 9:00 pm | American Dad | Iced, Iced Babies
 9:30 pm | Rick and Morty | The Wedding Squanchers
10:00 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
10:30 pm | Bob's Burgers | The Gene & Courtney Show
11:00 pm | Family Guy | Blind Ambition
11:30 pm | Family Guy | Don't Make Me Over
12:00 am | The Boondocks | The Return of the King
12:30 am | Robot Chicken | Hi.
12:45 am | Robot Chicken | Ginger Hill in: Bursting Pipes
 1:00 am | Tim and Eric Awesome Show, Great Job | Dolls
 1:15 am | Check It Out! | Food
 1:30 am | Family Guy | Blind Ambition
 2:00 am | Family Guy | Don't Make Me Over
 2:30 am | American Dad | Dungeons and Wagons
 3:00 am | Rick and Morty | The Wedding Squanchers
 3:30 am | Robot Chicken | Hi.
 3:45 am | Robot Chicken | Ginger Hill in: Bursting Pipes
 4:00 am | The Jellies | The Invasion
 4:30 am | Tim and Eric Awesome Show, Great Job | Dolls
 4:45 am | Check It Out! | Food
 5:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 5:30 am | Bob's Burgers | The Gene & Courtney Show