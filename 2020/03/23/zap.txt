# 2020-03-23
 6:00 am | Amazing World of Gumball | The Choices
 6:15 am | Amazing World of Gumball | The Matchmaker
 6:30 am | Amazing World of Gumball | The Code
 6:45 am | Amazing World of Gumball | The Grades
 7:00 am | Mao Mao: Heroes of Pure Heart | Small
 7:15 am | Mao Mao: Heroes of Pure Heart | Sleeper Sofa
 7:30 am | Mao Mao: Heroes of Pure Heart | Meet Tanya Keys
 7:45 am | Mao Mao: Heroes of Pure Heart | Not Impressed
 8:00 am | Teen Titans Go! | Girls Night In
 8:30 am | Teen Titans Go! | The Great Disaster
 8:45 am | Teen Titans Go! | The Viewers Decide
 9:00 am | Teen Titans Go! | Beast Boy's That's What's Up
10:00 am | Craig of the Creek | Fort Williams
10:15 am | Craig of the Creek | Cousin of the Creek
10:30 am | Craig of the Creek | Kelsey the Elder
10:45 am | Craig of the Creek | You're It
11:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
11:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
12:00 pm | Victor and Valentino | Love at First Bite
12:15 pm | Victor and Valentino | Tez Says
12:30 pm | Victor and Valentino | Go With the Flow
12:45 pm | Victor and Valentino | Forever Ever
 1:00 pm | Amazing World of Gumball | The Romantic
 1:15 pm | Amazing World of Gumball | The Roots
 1:30 pm | Amazing World of Gumball | The Detective
 1:45 pm | Amazing World of Gumball | The Fury
 2:00 pm | Amazing World of Gumball | The Origins
 2:15 pm | Amazing World of Gumball | The Origins
 2:30 pm | Amazing World of Gumball | The Compilation
 2:45 pm | Amazing World of Gumball | The Console
 3:00 pm | Total DramaRama | Ant We All Just Get Along
 3:15 pm | Total DramaRama | Not Without My Fudgy Lumps
 3:30 pm | Total DramaRama | Cone in 60 Seconds
 3:45 pm | Total DramaRama | Paint That a Shame
 4:00 pm | Teen Titans Go! | Double Trouble; The Date
 4:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 5:00 pm | Craig of the Creek | Sparkle Cadet
 5:15 pm | Craig of the Creek | Too Many Treasures
 5:30 pm | Craig of the Creek | Stink Bomb
 5:45 pm | Craig of the Creek | The Final Book
 6:00 pm | Teen Titans Go! | Parasite; Starliar
 6:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 7:00 pm | Amazing World of Gumball | The Boredom
 7:15 pm | Amazing World of Gumball | The Stars
 7:30 pm | Apple & Onion | A New Life
 7:45 pm | Apple & Onion | Party Popper