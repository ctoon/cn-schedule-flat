# 2020-03-21
 6:00 am | Amazing World of Gumball | The Silence
 6:15 am | Amazing World of Gumball | The Return
 6:30 am | Amazing World of Gumball | The Future
 6:45 am | Amazing World of Gumball | The Nemesis
 7:00 am | Amazing World of Gumball | The Wish
 7:15 am | Amazing World of Gumball | The Crew
 7:30 am | Amazing World of Gumball | The Code
 7:45 am | Amazing World of Gumball | The Others
 8:00 am | Teen Titans Go! | I Used to Be a Peoples
 8:15 am | Teen Titans Go! | Communicate Openly
 8:30 am | Teen Titans Go! | The Metric System vs. Freedom
 8:45 am | Teen Titans Go! | TV Knight 4
 9:00 am | Total DramaRama | Gum and Gummer
 9:15 am | Total DramaRama | From Badge to Worse
 9:30 am | Apple & Onion | Pulling Your Weight
 9:45 am | Apple & Onion | Dragonhead
10:00 am | Teen Titans Go! | The Titans Go Casual
10:15 am | Teen Titans Go! | Magic Man
10:30 am | ThunderCats Roar | Lost Sword
10:45 am | ThunderCats Roar | The Horror of Hook Mountain
11:00 am | Teen Titans Go! | The Chaff
11:15 am | Teen Titans Go! | What's Opera, Titans?
11:30 am | Teen Titans Go! | Curse of the Booty Scooty
11:45 am | Teen Titans Go! | TV Knight 5
12:00 pm | Teen Titans Go! | Them Soviet Boys
12:15 pm | Teen Titans Go! | Royal Jelly
12:30 pm | Teen Titans Go! | Lil' Dimples
12:45 pm | Teen Titans Go! | Strength of a Grown Man
 1:00 pm | Amazing World of Gumball | The Revolt
 1:15 pm | Amazing World of Gumball | The Signature
 1:30 pm | Amazing World of Gumball | The Mess
 1:45 pm | Amazing World of Gumball | The Gift
 2:00 pm | Amazing World of Gumball | The Possession
 2:15 pm | Amazing World of Gumball | The Apprentice
 2:30 pm | Amazing World of Gumball | The Heart
 2:45 pm | Amazing World of Gumball | The Check
 3:00 pm | Teen Titans Go! | Stockton, CA!
 3:15 pm | Teen Titans Go! | Had to Be There
 3:30 pm | Teen Titans Go! | Collect Them All
 3:45 pm | Teen Titans Go! | The Great Disaster
 4:00 pm | Craig of the Creek | Bug City
 4:15 pm | Craig of the Creek | The Shortcut
 4:30 pm | Victor and Valentino | Dance Reynaldo Dance
 4:45 pm | Victor and Valentino | El Silbon
 5:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:15 pm | Total DramaRama | A Licking Time Bomb
 5:30 pm | Total DramaRama | Toys Will Be Toys
 5:45 pm | Total DramaRama | Know It All
 6:00 pm | Amazing World of Gumball | The Ghouls
 6:15 pm | Amazing World of Gumball | The Pest
 6:30 pm | Amazing World of Gumball | The BFFS
 6:45 pm | Amazing World of Gumball | The Hug
 7:00 pm | Amazing World of Gumball | The News
 7:15 pm | Amazing World of Gumball | The Vase
 7:30 pm | Amazing World of Gumball | The Ollie
 7:45 pm | Amazing World of Gumball | The Potato