# 2020-03-24
 8:00 pm | Home Movies | Dad
 8:30 pm | Bob's Burgers | Flu-Ouise
 9:00 pm | Bob's Burgers | Sea Me Now
 9:30 pm | Rick and Morty | The Ricklantis Mixup
10:00 pm | American Dad | Joint Custody
10:30 pm | American Dad | The Vacation Goo
11:00 pm | Family Guy | Fat Guy Strangler
11:30 pm | Family Guy | Patriot Games
12:00 am | The Boondocks | ...Or Die Trying
12:30 am | Robot Chicken | Musya Shakhtyorov in: Honeyboogers
12:45 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 1:00 am | Tim and Eric Awesome Show, Great Job | Innernette
 1:15 am | Check It Out! | Friendship
 1:30 am | Family Guy | Fat Guy Strangler
 2:00 am | Family Guy | Patriot Games
 2:30 am | American Dad | Joint Custody
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Robot Chicken | Musya Shakhtyorov in: Honeyboogers
 3:45 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 4:00 am | The Jellies | Last Reggie on Earth
 4:15 am | Lazor Wulf | Lane Occupied
 4:30 am | Tim and Eric Awesome Show, Great Job | Innernette
 4:45 am | Check It Out! | Friendship
 5:00 am | Bob's Burgers | Flu-Ouise
 5:30 am | Bob's Burgers | Sea Me Now