# 2020-07-30
 6:00 am | Amazing World of Gumball | The Roots; The Deal
 6:30 am | Amazing World of Gumball | The Line; The Blame
 7:00 am | Amazing World of Gumball | The Cage; The Singing
 7:30 am | Amazing World of Gumball | The Puppets; The Detective
 8:00 am | Apple & Onion | Heatwave; Dragonhead
 8:30 am | Teen Titans Go! | Vegetables; Friendship
 9:00 am | Teen Titans Go! | The Mask; Slumber Party
 9:30 am | Teen Titans Go! | The Best Robin; Road Trip
10:00 am | Craig of the Creek | The Brood; The End Was Here
10:30 am | Craig of the Creek | Under the Overpass; In the Key of the Creek
11:00 am | Teen Titans Go! | Hot Garbage; Mouth Hole
11:30 am | Teen Titans Go! | Birds; Be Mine
12:00 pm | Teen Titans Go! | Brain Food; In and Out
12:30 pm | Mao Mao, Heroes of Pure Heart | Breakup; Meet Tanya Keys
 1:00 pm | Amazing World of Gumball | The Advice; The Copycats
 1:30 pm | Amazing World of Gumball | The Catfish; The Signal
 2:00 pm | Amazing World of Gumball | The Girlfriend; The Cycle
 2:30 pm | Amazing World of Gumball | The Stars; The Parasite
 3:00 pm | Craig of the Creek | Ace of Squares; The Time Capsule
 3:30 pm | Craig of the Creek | Doorway to Helen; Beyond the Rapids
 4:00 pm | Total DramaRama | Stay Goth; Poodle Girl; Stay Goth; An Egg-Stremely Bad Idea
 4:30 pm | Total DramaRama | Gum and Gummer; Exercising the Demons
 5:00 pm | Teen Titans Go! | Little Buddies; Missing
 5:30 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 6:00 pm | Apple & Onion | Champion Falafel's Passion
 6:30 pm | Amazing World of Gumball | The Ex; The Points
 7:00 pm | Amazing World of Gumball | The Best; The Bus
 7:30 pm | We Bare Bears | The Nom Nom Show; Best Bears