# 2020-07-10
 8:00 pm | Bob's Burgers | The Wolf of Wharf Street
 8:30 pm | Bob's Burgers | The Silence of the Louise
 9:00 pm | American Dad | Dungeons and Wagons
 9:30 pm | American Dad | Iced, Iced Babies
10:00 pm | Rick and Morty | Big Trouble in Little Sanchez
10:30 pm | Rick and Morty | Interdimensional Cable 2: Tempting Fate
11:00 pm | Family Guy | Scammed Yankees
11:30 pm | Family Guy | An App a Day
12:00 am | Eric Andre Show | Eric Andre Does Paris
12:15 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
12:30 am | Eric Andre Show | Chris Jericho; Roy Hibbert; Flavor Flav
12:45 am | Eric Andre Show | Dennis Rodman; Haley Joel Osment
 1:00 am | Eric Andre Show | Seth Rogen; Asa Akira
 1:15 am | Eric Andre Show | The Hannibal Buress Show
 1:30 am | Eric Andre Show | Bird Up!
 1:45 am | Eric Andre Show | Maria Menounos & Eric Balfour
 2:00 am | Eric Andre Show | Lou Ferrigno & Downtown Julie Brown
 2:15 am | Eric Andre Show | Jodie Sweetin & Vivica A. Fox
 2:30 am | Eric Andre Show | James Van Der Beek & Steve-O
 2:45 am | Eric Andre Show | Rick Fox
 3:00 am | Eric Andre Show | Evangelos
 3:15 am | Eric Andre Show | Jack
 3:30 am | Krft Punk's Political Party | Krft Punk's Political Party
 4:00 am | Quarantine Valentine | Quarantine Valentine
 4:15 am | FishCenter | FishCenter
 4:30 am | American Dad | Dungeons and Wagons
 5:00 am | Bob's Burgers | The Wolf of Wharf Street
 5:30 am | Bob's Burgers | The Silence of the Louise