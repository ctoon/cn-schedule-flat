# 2020-07-02
 8:00 pm | American Dad | Bullocks to Stan
 8:30 pm | American Dad | A Smith in the Hand
 9:00 pm | American Dad | Con Heir
 9:30 pm | Rick and Morty | Raising Gazorpazorp
10:00 pm | Bob's Burgers | A Few 'gurt Men
10:30 pm | Bob's Burgers | Like Gene for Chocolate
11:00 pm | Family Guy | Dr. C & the Women
11:30 pm | Family Guy | #JOLO
12:00 am | Robot Chicken | Max Caenen in: Why Would He Know If His Mother's A Size Queen
12:15 am | Robot Chicken | Petless M in: Cars are Couches of the Road
12:30 am | Aqua Teen | Cybernetic Ghost of Christmas Past
12:45 am | Squidbillies | Double Truckin' The Tricky Two
 1:00 am | The Boondocks | The Lovely Ebony Brown
 1:30 am | Family Guy | Dr. C & the Women
 2:00 am | Family Guy | #JOLO
 2:30 am | Bob's Burgers | A Few 'gurt Men
 3:00 am | Bob's Burgers | Like Gene for Chocolate
 3:30 am | Rick and Morty | Raising Gazorpazorp
 4:00 am | China, IL | Crow College
 4:30 am | Robot Chicken | Max Caenen in: Why Would He Know If His Mother's A Size Queen
 4:45 am | Robot Chicken | Petless M in: Cars are Couches of the Road
 5:00 am | Aqua Teen | Cybernetic Ghost of Christmas Past
 5:15 am | Squidbillies | Double Truckin' The Tricky Two
 5:30 am | My Hero Academia | One for All