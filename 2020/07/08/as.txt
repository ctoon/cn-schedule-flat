# 2020-07-08
 8:00 pm | Bob's Burgers | Mom, Lies, and Videotape
 8:30 pm | Bob's Burgers | Paraders of the Lost Float
 9:00 pm | American Dad | Camp Refoogee
 9:30 pm | American Dad | The American Dad After School Special
10:00 pm | Rick and Morty | Auto Erotic Assimilation
10:30 pm | Rick and Morty | Total Rickall
11:00 pm | Family Guy | Peter's Sister
11:30 pm | Family Guy | Hot Pocket-Dial
12:00 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
12:15 am | Aqua Teen | Super Bowl
12:30 am | 12oz Mouse | Enjoy the Arm
12:45 am | 12oz Mouse | Auraphull
 1:00 am | The Boondocks | It's Goin' Down
 1:30 am | Family Guy | Peter's Sister
 2:00 am | Family Guy | Hot Pocket-Dial
 2:30 am | Rick and Morty | Auto Erotic Assimilation
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | Robot Chicken | Buster Olive in: The Monkey Got Closer Overnight
 3:45 am | Aqua Teen | Super Bowl
 4:00 am | China, IL | Displays of Manhood
 4:30 am | 12oz Mouse | Enjoy the Arm
 4:45 am | 12oz Mouse | Auraphull
 5:00 am | Bob's Burgers | Mom, Lies, and Videotape
 5:30 am | Bob's Burgers | Paraders of the Lost Float