# 2020-07-14
 8:00 pm | Bob's Burgers | The Bleakening Part 1
 8:30 pm | Bob's Burgers | The Bleakening Part 2
 9:00 pm | American Dad | A.T. the Abusive Terrestrial
 9:30 pm | American Dad | Black Mystery Month
10:00 pm | Rick and Morty | The Rickshank Rickdemption
10:30 pm | Rick and Morty | Rickmancing the Stone
11:00 pm | Family Guy | The New Adventures of Old Tom
11:30 pm | Family Guy | Run, Chris, Run
12:00 am | Robot Chicken | A Piece of the Action
12:15 am | Aqua Teen | Super Spore
12:30 am | 12oz Mouse | Pre-Reckoning
12:45 am | 12oz Mouse | Farewell
 1:00 am | Samurai Jack | XCIII
 1:30 am | Family Guy | The New Adventures of Old Tom
 2:00 am | Family Guy | Run, Chris, Run
 2:30 am | Rick and Morty | The Rickshank Rickdemption
 3:00 am | Rick and Morty | Rickmancing the Stone
 3:30 am | Robot Chicken | A Piece of the Action
 3:45 am | Aqua Teen | Super Spore
 4:00 am | China, IL | The Perfect Lecture
 4:30 am | 12oz Mouse | Pre-Reckoning
 4:45 am | 12oz Mouse | Farewell
 5:00 am | Bob's Burgers | The Bleakening Part 1
 5:30 am | Bob's Burgers | The Bleakening Part 2