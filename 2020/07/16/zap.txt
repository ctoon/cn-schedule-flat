# 2020-07-16
 6:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 am | Amazing World of Gumball | The Mirror; The Burden
 7:00 am | Amazing World of Gumball | The Bros; The Man
 7:30 am | Amazing World of Gumball | The Pizza; The Lie
 8:00 am | Apple & Onion | 4 on 1
 8:15 am | Apple & Onion | Selfish Shellfish
 8:30 am | Teen Titans Go! | Girls Night In Part 1
 9:00 am | Teen Titans Go! | Super Hero Summer Camp
10:00 am | Craig of the Creek | Mortimor to the Rescue
10:15 am | Craig of the Creek | Ancients of the Creek
10:30 am | Craig of the Creek | Secret in a Bottle
10:45 am | Craig of the Creek | Summer Wish
11:00 am | Teen Titans Go! | Walk Away
11:15 am | Teen Titans Go! | Record Book
11:30 am | Teen Titans Go! | I Used to Be a Peoples
11:45 am | Teen Titans Go! | TV Knight 4
11:50 am | Teen Titans Go! | Brain Percentages
12:00 pm | Teen Titans Go! | The Metric System vs. Freedom
12:15 pm | Teen Titans Go! | Teen Titans Roar!
12:30 pm | Mao Mao: Heroes of Pure Heart | Zing Your Heart Out
12:45 pm | Mao Mao: Heroes of Pure Heart | Try Hard
 1:00 pm | Amazing World of Gumball | The Boombox; The Castle
 1:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 2:00 pm | Amazing World of Gumball | The Internet; The Plan
 2:30 pm | Amazing World of Gumball | The World; The Finale
 3:00 pm | Craig of the Creek | Itch to Explore
 3:15 pm | Craig of the Creek | Cousin of the Creek
 3:30 pm | Craig of the Creek | You're It
 3:45 pm | Craig of the Creek | Ancients of the Creek
 4:00 pm | Total DramaRama | Sharing Is Caring
 4:15 pm | Total DramaRama | The Price of Advice
 4:30 pm | Total DramaRama | Ant We All Just Get Along
 4:45 pm | Total DramaRama | Mother of All Cards
 5:00 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 5:30 pm | Teen Titans Go! | The Chaff
 5:45 pm | Teen Titans Go! | Communicate Openly
 6:00 pm | Apple & Onion | Tips
 6:15 pm | Apple & Onion | Positive Attitude Theory
 6:30 pm | Amazing World of Gumball | The Fraud; The Void
 7:00 pm | Amazing World of Gumball | The Boss; The Move
 7:30 pm | We Bare Bears | Panda 2
 7:45 pm | We Bare Bears | Rescue Ranger