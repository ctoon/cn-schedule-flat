# 2020-07-22
 8:00 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 8:30 pm | Bob's Burgers | Mo Mommy Mo Problems
 9:00 pm | American Dad | The Most Adequate Christmas Ever
 9:30 pm | American Dad | Franny 911
10:00 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
10:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
11:00 pm | Family Guy | Da Boom
11:30 pm | Family Guy | Brian in Love
12:00 am | 12oz Mouse | Adrift
12:15 am | Aqua Teen | Revenge of the Trees
12:30 am | Aqua Teen | Spirit Journey Formation Anniversary
12:45 am | Squidbillies | Meth O.D. To My Madness
 1:00 am | Samurai Jack | XCVIII
 1:30 am | Family Guy | Da Boom
 2:00 am | Family Guy | Brian in Love
 2:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | 12oz Mouse | Adrift
 3:45 am | Aqua Teen | Revenge of the Trees
 4:00 am | Loiter Squad | Episode 5
 4:15 am | Loiter Squad | Episode 6
 4:30 am | Aqua Teen | Spirit Journey Formation Anniversary
 4:45 am | Squidbillies | Meth O.D. To My Madness
 5:00 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 5:30 am | Bob's Burgers | Mo Mommy Mo Problems