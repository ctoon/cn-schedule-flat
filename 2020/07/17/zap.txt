# 2020-07-17
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Amazing World of Gumball | The Return
 7:15 am | Amazing World of Gumball | The Nemesis
 7:30 am | Amazing World of Gumball | The Society; The Spoiler
 8:00 am | Apple & Onion | Hotdog's Movie Premiere
 8:15 am | Apple & Onion | Apple's Formula
 8:30 am | Teen Titans Go! | Bat Scouts
 8:45 am | Teen Titans Go! | Magic Man
 9:00 am | Teen Titans Go! | Beast Boy's That's What's Up
10:00 am | Craig of the Creek | Trading Day
10:15 am | Craig of the Creek | Turning the Tables
10:30 am | Craig of the Creek | Crisis at Elder Rock
10:45 am | Craig of the Creek | Kelsey the Author
11:00 am | Teen Titans Go! | Brain Percentages
11:15 am | Teen Titans Go! | We're Off to Get Awards
11:30 am | Teen Titans Go! | Them Soviet Boys
11:45 am | Teen Titans Go! | Royal Jelly
12:00 pm | Teen Titans Go! | Lil' Dimples
12:15 pm | Teen Titans Go! | Strength of a Grown Man
12:30 pm | Mao Mao: Heroes of Pure Heart | Strange Bedfellows
12:45 pm | Mao Mao: Heroes of Pure Heart | Scared of Puppets
 1:00 pm | Amazing World of Gumball | The Kids; The Fan
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Craig of the Creek | Jessica Goes to the Creek
 3:15 pm | Craig of the Creek | Stink Bomb
 3:30 pm | Craig of the Creek | The Final Book
 3:45 pm | Craig of the Creek | The Evolution of Craig
 4:00 pm | Total DramaRama | Cone in 60 Seconds
 4:15 pm | Total DramaRama | Duncan Disorderly
 4:30 pm | Total DramaRama | The Bad Guy Busters
 4:45 pm | Total DramaRama | Soother or Later
 5:00 pm | Teen Titans Go! | Curse of the Booty Scooty
 5:15 pm | Teen Titans Go! | What's Opera, Titans?
 5:30 pm | Teen Titans Go! | Stockton, CA!
 5:45 pm | Teen Titans Go! | Had to Be There
 6:00 pm | Apple & Onion | Falafel's Fun Day
 6:15 pm | Apple & Onion | Follow Your Dreams
 6:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 7:00 pm | Amazing World of Gumball | The Mirror; The Burden
 7:30 pm | We Bare Bears | Kyle
 7:45 pm | We Bare Bears | Lil' Squid