# 2020-07-23
 8:00 pm | Bob's Burgers | Mission Impos-slug-ble
 8:30 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 9:00 pm | American Dad | Tearjerker
 9:30 pm | American Dad | Oedipal Panties
10:00 pm | Rick and Morty | Rattlestar Ricklactica
10:30 pm | Rick and Morty | Never Ricking Morty
11:00 pm | Family Guy | Love Thy Trophy
11:30 pm | Family Guy | Death Is a Bitch
12:00 am | 12oz Mouse | You Made This
12:15 am | Aqua Teen | The Shaving
12:30 am | Aqua Teen | Broodwich
12:45 am | Squidbillies | The Tiniest Princess
 1:00 am | Samurai Jack | XCIX
 1:30 am | Family Guy | Love Thy Trophy
 2:00 am | Family Guy | Death Is a Bitch
 2:30 am | Rick and Morty | Rattlestar Ricklactica
 3:00 am | Rick and Morty | Never Ricking Morty
 3:30 am | 12oz Mouse | You Made This
 3:45 am | Aqua Teen | The Shaving
 4:00 am | Loiter Squad | Episode 7
 4:15 am | Loiter Squad | Episode 8
 4:30 am | Aqua Teen | Broodwich
 4:45 am | Squidbillies | The Tiniest Princess
 5:00 am | Bob's Burgers | Mission Impos-slug-ble
 5:30 am | Bob's Burgers | Something Old, Something New, Something Bob Caters for You