# 2020-07-03
 6:00 am | Amazing World of Gumball | The Tape; The Sweaters
 6:30 am | Amazing World of Gumball | The Internet; The Plan
 7:00 am | Amazing World of Gumball | The World; The Finale
 7:30 am | Amazing World of Gumball | The Kids; The Fan
 8:00 am | Apple & Onion | Rotten Apple
 8:15 am | Apple & Onion | Not Funny
 8:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
 9:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt
 9:15 am | Teen Titans Go! | Head Fruit
 9:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
10:00 am | Craig of the Creek | The Ground Is Lava!
10:15 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
10:30 am | Craig of the Creek | The Bike Thief
10:45 am | Craig of the Creek | Craig of the Beach
11:00 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
11:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
12:00 pm | Teen Titans Go! | Video Game References; Cool School
12:30 pm | Mao Mao: Heroes of Pure Heart | Head Chef
12:45 pm | Mao Mao: Heroes of Pure Heart | Enemy Mime
 1:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 1:30 pm | Amazing World of Gumball | The Name; The Extras
 2:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 2:30 pm | Amazing World of Gumball | The Fraud; The Void
 3:00 pm | Craig of the Creek | The Ground Is Lava!
 3:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 3:30 pm | Craig of the Creek | The Bike Thief
 3:45 pm | Craig of the Creek | Craig of the Beach
 4:00 pm | Total DramaRama | Mutt Ado About Owen
 4:15 pm | Total DramaRama | Simons Are Forever
 4:30 pm | Total DramaRama | Mother of All Cards
 4:45 pm | Total DramaRama | Soother or Later
 5:00 pm | Teen Titans Go! | Let's Get Serious; Tamaranian Vacation
 5:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 6:00 pm | Apple & Onion | Bottle Catch
 6:15 pm | Apple & Onion | Pancake's Bus Tour
 6:30 pm | Amazing World of Gumball | The Boss; The Move
 7:00 pm | Amazing World of Gumball | The Law; The Allergy
 7:30 pm | We Bare Bears | Charlie's Big Foot
 7:45 pm | We Bare Bears | The Nom Nom Show