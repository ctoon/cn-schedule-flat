# 2020-07-29
 6:00 am | Amazing World of Gumball | The Points
 6:15 am | Amazing World of Gumball | The Ex
 6:30 am | Amazing World of Gumball | The Bus
 6:45 am | Amazing World of Gumball | The Best
 7:00 am | Amazing World of Gumball | The Night
 7:15 am | Amazing World of Gumball | The Worst
 7:30 am | Amazing World of Gumball | The Misunderstandings
 7:45 am | Amazing World of Gumball | The List
 8:00 am | Apple & Onion | Whale Spotting
 8:15 am | Apple & Onion | Pulling Your Weight
 8:30 am | Teen Titans Go! | Brian; Nature
 9:00 am | Teen Titans Go! | Love Monsters; Baby Hands
 9:30 am | Teen Titans Go! | Caramel Apples; Halloween
10:00 am | Craig of the Creek | The Climb
10:15 am | Craig of the Creek | The Jinxening
10:30 am | Craig of the Creek | The Other Side: The Tournament
11:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
11:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
12:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
12:30 pm | Mao Mao: Heroes of Pure Heart | Enemy Mime
12:45 pm | Mao Mao: Heroes of Pure Heart | Small
 1:00 pm | Amazing World of Gumball | The Fury
 1:15 pm | Amazing World of Gumball | The Lady
 1:30 pm | Amazing World of Gumball | The Compilation
 1:45 pm | Amazing World of Gumball | The Sucker
 2:00 pm | Amazing World of Gumball | The Origins
 2:15 pm | Amazing World of Gumball | The Origins
 2:30 pm | Amazing World of Gumball | The Disaster
 2:45 pm | Amazing World of Gumball | The Re-Run
 3:00 pm | Craig of the Creek | Kelsey Quest
 3:15 pm | Craig of the Creek | Ferret Quest
 3:30 pm | Craig of the Creek | JPony
 3:45 pm | Craig of the Creek | Into the Overpast
 4:00 pm | Total DramaRama | All Up in Your Drill
 4:15 pm | Total DramaRama | The Tooth About Zombies
 4:30 pm | Total DramaRama | From Badge to Worse; Snow Way Out
 4:45 pm | Total DramaRama | Lie-Ranosaurus Wrecked
 5:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Apple & Onion | Apple's in Charge
 6:15 pm | Apple & Onion | Rotten Apple
 6:30 pm | Amazing World of Gumball | The Love
 6:45 pm | Amazing World of Gumball | The Box
 7:00 pm | Amazing World of Gumball | The Matchmaker
 7:15 pm | Amazing World of Gumball | The Nuisance
 7:30 pm | We Bare Bears | El Oso
 7:45 pm | We Bare Bears | Go Fish