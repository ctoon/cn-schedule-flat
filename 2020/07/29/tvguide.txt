# 2020-07-29
 6:00 am | Amazing World of Gumball | The Ex; The Points
 6:30 am | Amazing World of Gumball | The Best; The Bus
 7:00 am | Amazing World of Gumball | The Worst; The Night
 7:30 am | Amazing World of Gumball | The Misunderstandings;The List
 8:00 am | Apple & Onion | Whale Spotting; Pulling Your Weight
 8:30 am | Teen Titans Go! | Brian; Nature
 9:00 am | Teen Titans Go! | Love Monsters; Baby Hands
 9:30 am | Teen Titans Go! | Caramel Apples; Halloween
10:00 am | Craig of the Creek | The Climb; The Jinxening
10:30 am | Craig of the Creek | The Other Side: The Tournament
11:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
11:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
12:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
12:30 pm | Mao Mao, Heroes of Pure Heart | Enemy Mime; Small
 1:00 pm | Amazing World of Gumball | The Fury; The Lady
 1:30 pm | Amazing World of Gumball | The Compilation; The Sucker
 2:00 pm | Amazing World of Gumball | The Origins; The Origins Part 2; Warriors for Hire
 2:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 3:00 pm | Craig of the Creek | Kelsey Quest; Ferret Quest
 3:30 pm | Craig of the Creek | Jpony; Into the Overpast
 4:00 pm | Total DramaRama | The All up in Your Drill; Tooth About Zombies
 4:30 pm | Total DramaRama | Lie-Ranosaurus Wrecked; Snow Way Out
 5:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Apple & Onion | Rotten Apple Apple's in Charge
 6:30 pm | Amazing World of Gumball | The Love; The Box
 7:00 pm | Amazing World of Gumball | The Matchmaker; The Nuisance
 7:30 pm | We Bare Bears | El Oso; Go Fish