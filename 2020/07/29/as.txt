# 2020-07-29
 8:00 pm | Bob's Burgers | Bed & Breakfast
 8:30 pm | Bob's Burgers | Art Crawl
 9:00 pm | American Dad | Escape from Pearl Bailey
 9:30 pm | American Dad | Pulling Double Booty
10:00 pm | Rick and Morty | Anatomy Park
10:30 pm | Rick and Morty | M. Night Shaym-Aliens!
11:00 pm | Family Guy | Dammit Janet
11:30 pm | Family Guy | There's Something About Paulie
12:00 am | 12oz Mouse | Here We Come
12:15 am | Aqua Teen | The Dressing
12:30 am | Aqua Teen | The The
12:45 am | Squidbillies | Burned And Reburned Again
 1:00 am | Genndy Tartakovksy's Primal | Spear and Fang
 1:30 am | Family Guy | Dammit Janet
 2:00 am | Family Guy | There's Something About Paulie
 2:30 am | Rick and Morty | Anatomy Park
 3:00 am | Rick and Morty | M. Night Shaym-Aliens!
 3:30 am | 12oz Mouse | Here We Come
 3:45 am | Aqua Teen | The Dressing
 4:00 am | Loiter Squad | Jason's Lyric
 4:15 am | Loiter Squad | How Stella Got Her Groove Back
 4:30 am | Aqua Teen | The The
 4:45 am | Squidbillies | Burned And Reburned Again
 5:00 am | Bob's Burgers | Bed & Breakfast
 5:30 am | Bob's Burgers | Art Crawl