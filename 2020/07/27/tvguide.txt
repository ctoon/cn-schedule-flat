# 2020-07-27
 6:00 am | Amazing World of Gumball | The Advice; The Copycats
 6:30 am | Amazing World of Gumball | The Catfish; The Signal
 7:00 am | Amazing World of Gumball | The Girlfriend; The Cycle
 7:30 am | Amazing World of Gumball | The Stars; The Parasite
 8:00 am | Apple & Onion | Champion Falafel's Passion
 8:30 am | Teen Titans Go! | Birds; Be Mine
 9:00 am | Teen Titans Go! | Brain Food; In and Out
 9:30 am | Teen Titans Go! | Little Buddies; Missing
10:00 am | Craig of the Creek | Kelsey Quest; Ferret Quest
10:30 am | Craig of the Creek | Jpony; Into the Overpast
11:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
11:30 am | Teen Titans Go! | Love Monsters; Baby Hands
12:00 pm | Teen Titans Go! | Caramel Apples; Halloween
12:30 pm | Mao Mao, Heroes of Pure Heart | I Love You Mao Mao; Legend of Torbaclaun
 1:00 pm | Amazing World of Gumball | The Ex; The Points
 1:30 pm | Amazing World of Gumball | The Best; The Bus
 2:00 pm | Amazing World of Gumball | The Worst; The Night
 2:30 pm | Amazing World of Gumball | The Misunderstandings;The List
 3:00 pm | Craig of the Creek | The Brood; The End Was Here
 3:30 pm | Craig of the Creek | Under the Overpass; In the Key of the Creek
 4:00 pm | Total DramaRama | Know It All Harold Swatter and the Goblet of Flies
 4:30 pm | Total DramaRama | Licking Time Bomb; A Stink. Stank. Stunk
 5:00 pm | Teen Titans Go! | Brian; Nature
 5:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:00 pm | Apple & Onion | Whale Spotting; Pulling Your Weight
 6:30 pm | Amazing World of Gumball | The Fury; The Lady
 7:00 pm | Amazing World of Gumball | The Compilation; The Sucker
 7:30 pm | We Bare Bears | Baby Bears Can't Jump; Teacher's Pet