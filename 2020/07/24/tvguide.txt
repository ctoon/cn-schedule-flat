# 2020-07-24
 6:00 am | Amazing World of Gumball | The Heist; The Romantic
 6:30 am | Amazing World of Gumball | The Sorcerer; The Uploads
 7:00 am | Amazing World of Gumball | The Wicked; The Petals
 7:30 am | Amazing World of Gumball | The Traitor; The Outside
 8:00 am | Apple & Onion | The Lil Noodle Fly
 8:30 am | Teen Titans Go! | No Power; Sidekick
 9:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 9:30 am | Teen Titans Go! | Legs; Breakfast Cheese
10:00 am | Craig of the Creek | Sunday Clothes; Creek Daycare
10:30 am | Craig of the Creek | Escape From Family Dinner; Sugar Smugglers
11:00 am | Teen Titans Go! | Waffles; Opposites
11:30 am | Teen Titans Go! | Dude Relax!; Laundry Day
12:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
12:30 pm | Mao Mao, Heroes of Pure Heart | Zing Your Heart out Strange Bedfellows
 1:00 pm | Amazing World of Gumball | The Others; The Guy
 1:30 pm | Amazing World of Gumball | The Boredom; The Signature
 2:00 pm | Amazing World of Gumball | The Gift; The Menu
 2:30 pm | Amazing World of Gumball | The Apprentice; The Choices
 3:00 pm | Craig of the Creek | Lost in the Sewer; Crisis at Elder Rock
 3:30 pm | Craig of the Creek | Future Is Cardboard; The Kelsey the Worthy
 4:00 pm | Total DramaRama | Paint That a Shame Gnome More Mister Nice Guy
 4:30 pm | Total DramaRama | Snots Landing Look Who's Clocking
 5:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 5:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 6:00 pm | Apple & Onion | Champion Falafel's Passion
 6:30 pm | Apple & Onion | Hole in Roof Patty's Law
 7:00 pm | Amazing World of Gumball | The Parking; The News
 7:30 pm | We Bare Bears | Dog Hotel Crowbar Jones: Origins