# 2020-07-05
 6:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:30 am | Bakugan: Armored Alliance | Final Battle! Dan vs. Ajit; Secrets Exposed
 7:00 am | Ben 10 | Funhouse
 7:15 am | Ben 10 | Summer Breakers
 7:30 am | Amazing World of Gumball | The Inquisition
 7:45 am | Amazing World of Gumball | The Routine
 8:00 am | Amazing World of Gumball | The Third; The Debt
 8:30 am | Amazing World of Gumball | The Pressure; The Painting
 9:00 am | Amazing World of Gumball | The Responsible; The Dress
 9:30 am | Amazing World of Gumball | The Laziest; The Ghost
10:00 am | Amazing World of Gumball | The Mystery; The Prank
10:30 am | Amazing World of Gumball | The Gi; The Kiss
11:00 am | Teen Titans Go! | The Power of Shrimps
11:15 am | Teen Titans Go! | BBRBDAY
11:30 am | Teen Titans Go! | My Name Is Jose
11:45 am | Teen Titans Go! | Tall Titan Tales
12:00 pm | Teen Titans Go! | The Real Orangins!
12:15 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
12:30 pm | Teen Titans Go! | Business Ethics Wink Wink
12:45 pm | Teen Titans Go! | Curse of the Booty Scooty
 1:00 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 1:15 pm | Teen Titans Go! | Them Soviet Boys
 1:30 pm | Teen Titans Go! | I Used to Be a Peoples
 1:45 pm | Teen Titans Go! | Lil' Dimples
 2:00 pm | Teen Titans Go! | The Metric System vs. Freedom
 2:15 pm | Teen Titans Go! | Stockton, CA!
 2:30 pm | Teen Titans Go! | The Chaff
 2:45 pm | Teen Titans Go! | Collect Them All
 3:00 pm | Craig of the Creek | The Ground Is Lava!
 3:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 3:30 pm | Craig of the Creek | The Bike Thief
 3:45 pm | Craig of the Creek | Craig of the Beach
 4:00 pm | DC Super Hero Girls | #HateTriangle
 4:15 pm | DC Super Hero Girls | #BurritoBucket
 4:30 pm | Amazing World of Gumball | The Party; The Refund
 5:00 pm | Amazing World of Gumball | The Robot; The Picnic
 5:30 pm | Amazing World of Gumball | The Goons; The Secret
 6:00 pm | Amazing World of Gumball | The Sock; The Genius
 6:30 pm | Amazing World of Gumball | The Mustache; The Date
 7:00 pm | Adventure Time | Marceline's Closet; Incendium
 7:30 pm | Adventure Time | Another Way; Paper Pete