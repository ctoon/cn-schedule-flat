# 2020-01-04
 6:00 am | Amazing World of Gumball | The Mystery; The Prank
 6:30 am | Transformers: Cyberverse | The Crossroads
 6:45 am | Teen Titans Go! | Leg Day
 7:00 am | Teen Titans Go! | No Power; Sidekick
 7:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 8:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Total DramaRama | Aquarium for a Dream
 9:15 am | Total DramaRama | Ant We All Just Get Along
 9:30 am | Apple & Onion | Baby Boi TP
 9:45 am | Apple & Onion | The Perfect Team
10:00 am | Teen Titans Go! | Pirates; I See You
10:30 am | Teen Titans Go! | Brian; Nature
11:00 am | Teen Titans Go! | Legs; Breakfast Cheese
11:30 am | Teen Titans Go! | Waffles; Opposites
12:00 pm | Teen Titans Go! | Birds; Be Mine
12:30 pm | LEGO Ninjago | Krag's Lament
12:45 pm | LEGO Ninjago | Secret of the Wolf
 1:00 pm | Amazing World of Gumball | The Sock; The Genius
 1:30 pm | Amazing World of Gumball | The Mustache; The Date
 2:00 pm | Amazing World of Gumball | The Club; The Wand
 2:30 pm | Amazing World of Gumball | The Ape; The Poltergeist
 3:00 pm | Craig of the Creek | Ace of Squares
 3:15 pm | Craig of the Creek | The Takeout Mission
 3:30 pm | Craig of the Creek | Doorway to Helen
 3:45 pm | Craig of the Creek | Dinner at the Creek
 4:00 pm | Victor and Valentino | The Dark Room
 4:15 pm | Victor and Valentino | A New Don
 4:30 pm | Victor and Valentino | Cleaning Day
 4:45 pm | Victor and Valentino | The Collector
 5:00 pm | Amazing World of Gumball | The Quest; The Spoon
 5:30 pm | Amazing World of Gumball | The Car; The Curse
 6:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 6:30 pm | Amazing World of Gumball | The Helmet; The Fight
 7:00 pm | Apple & Onion | Gyranoid of the Future
 7:15 pm | Apple & Onion | Heatwave
 7:30 pm | Apple & Onion | Lil Noodle
 7:45 pm | Apple & Onion | Party Popper