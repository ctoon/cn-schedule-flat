# 2020-01-28
 6:00 am | Amazing World of Gumball | The Tag; The Lesson
 6:30 am | Amazing World of Gumball | The Limit; The Game
 7:00 am | Amazing World of Gumball | The Promise; The Voice
 7:30 am | Amazing World of Gumball | The Boombox; The Castle
 8:00 am | Teen Titans Go! | Curse of the Booty Scooty
 8:15 am | Teen Titans Go! | The Spice Game
 8:30 am | Teen Titans Go! | Them Soviet Boys
 8:45 am | Teen Titans Go! | The Great Disaster
 9:00 am | Teen Titans Go! | Lil' Dimples
 9:15 am | Teen Titans Go! | The Viewers Decide
 9:30 am | Teen Titans Go! | Stockton, CA!
 9:45 am | Teen Titans Go! | TV Knight 5
10:00 am | Craig of the Creek | The Brood
10:15 am | Craig of the Creek | Big Pinchy
10:30 am | Craig of the Creek | Under the Overpass
10:45 am | Craig of the Creek | The Kid From 3030
11:00 am | Amazing World of Gumball | The Authority; The Virus
11:30 am | Amazing World of Gumball | The Pony; The Storm
12:00 pm | Victor and Valentino | Fistfull of Balloons
12:15 pm | Victor and Valentino | Band for Life
12:30 pm | Victor and Valentino | Love at First Bite
12:45 pm | Victor and Valentino | Tez Says
 1:00 pm | Amazing World of Gumball | The Coach; The Joy
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Teen Titans Go! | TV Knight 4
 3:15 pm | Teen Titans Go! | What's Opera, Titans?
 3:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 4:00 pm | Teen Titans Go! | Forest Pirates
 4:15 pm | Teen Titans Go! | The Bergerac
 4:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 5:00 pm | Total DramaRama | Snow Way Out
 5:15 pm | Total DramaRama | The Price of Advice
 5:30 pm | Total DramaRama | All Up in Your Drill
 5:45 pm | Total DramaRama | There Are No Hoppy Endings
 6:00 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 6:15 pm | Teen Titans Go! | Royal Jelly
 6:30 pm | Teen Titans Go! | I Used to Be a Peoples
 6:45 pm | Teen Titans Go! | Strength of a Grown Man
 7:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 7:30 pm | Amazing World of Gumball | The Hero; The Photo