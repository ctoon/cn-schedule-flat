# 2020-01-08
 6:00 am | Amazing World of Gumball | The Advice; The Signal
 6:30 am | Amazing World of Gumball | The Parasite; The Love
 7:00 am | Amazing World of Gumball | The Awkwardness; The Nest
 7:30 am | Amazing World of Gumball | The Points; The Bus
 8:00 am | Teen Titans Go! | Parasite; Starliar
 8:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
 9:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 9:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
10:00 am | Craig Of The Creek | Camper On The Run; The Kid From 3030
10:30 am | Craig Of The Creek | Cousin Of The Creek; Big Pinchy
11:00 am | Amazing World Of Gumball | The Inquisition; The Potion
11:30 am | Amazing World of Gumball | The Friend; The Saint
12:00 pm | Victor And Valentino | The Collector; Cleaning Day
12:30 pm | Victor And Valentino | Los Cadejos; The Babysitter
 1:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 1:30 pm | Amazing World of Gumball | The Boredom; The Guy
 2:00 pm | Amazing World of Gumball | The Vision; The Choices
 2:30 pm | Amazing World of Gumball | The Code; The Test
 3:00 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 3:30 pm | Teen Titans Go! | Legs; Breakfast Cheese
 4:00 pm | Teen Titans Go! | Waffles; Opposites
 4:30 pm | Teen Titans Go! | Birds; Be Mine
 5:00 pm | Total DramaRama | From Badge To Worse; Too Much Of A Goo'd Thing
 5:30 pm | Total DramaRama | Snow Way Out; The Price Of Advice
 6:00 pm | Teen Titans Go! | Master Detective; Finally a Lesson
 6:30 pm | Teen Titans Go! | Titan Saving Time; Bottle Episode
 7:00 pm | Amazing World Of Gumball | The Decisions; The Schooling
 7:30 pm | Infinity Train | The Parasite Car
 7:45 pm | Infinity Train | The Lucky Cat Car