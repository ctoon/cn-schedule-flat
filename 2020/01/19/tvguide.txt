# 2020-01-19
 6:00 am | Amazing World Of Gumball | The Web; The Stink
 6:30 am | Amazing World Of Gumball | The Agent; The Ad
 7:00 am | Bakugan: Battle Planet | Planet-Ception; United We Stand
 7:30 am | Teen Titans Go! | Finally a Lesson; Fish Water
 8:00 am | Teen Titans Go! | Arms Race With Legs; Shrimps And Prime Rib
 8:30 am | Teen Titans Go! | Obinray; Halloween Vs. Christmas
 9:00 am | Teen Titans Go! | Batman Vs. Teen Titans: Dark Injustice; Booby Trap House
 9:30 am | Teen Titans Go! | Wally T; Tv Knight
10:00 am | Ben 10 | Tokyo Fun
10:30 am | Power Players | Sand Trap; Dodge City
11:00 am | Teen Titans Go! | The Teen Titans Go Easter Holiday Classic; Bbsfbday!
11:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes; Teen Titans Save Christmas
12:00 pm | Teen Titans Go! | History Lesson; Inner Beauty Of A Cactus
12:30 pm | Teen Titans Go! | The Art Of Ninjutsu; Movie Night
 1:00 pm | Amazing World of Gumball | The Comic; The Others
 1:30 pm | Amazing World of Gumball | The Romantic; The Signature
 2:00 pm | Amazing World of Gumball | The Uploads; The Gift
 2:30 pm | Amazing World of Gumball | The Wicked; The Apprentice
 3:00 pm | Unikitty | Trapped in the Tower
 3:30 pm | DC Super Hero Girls | #ScrambledEggs; #DramaQueen
 4:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 4:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 5:00 pm | Teen Titans Go! | Video Game References; Cool School
 5:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 6:00 pm | Amazing World of Gumball | The Signal; The Points
 6:30 pm | Amazing World of Gumball | The Girlfriend; The Bus
 7:00 pm | Amazing World Of Gumball | The Parasite; The Night
 7:30 pm | Amazing World of Gumball | The Misunderstanding; The Love