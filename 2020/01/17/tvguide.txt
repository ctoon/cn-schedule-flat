# 2020-01-17
 6:00 am | Amazing World of Gumball | The Rival; The Lady
 6:30 am | Amazing World of Gumball | The Vegging; The Sucker
 7:00 am | Amazing World of Gumball | The Father; The One
 7:30 am | Amazing World of Gumball | The Cringe; The Cage
 8:00 am | Teen Titans Go! | Mouth Hole; Hot Garbage
 8:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
10:00 am | Craig Of The Creek | Alone Quest; Lost In The Sewer
10:30 am | Craig Of The Creek | The Mystery Of The Timekeeper; Bring Out Your Beast
11:00 am | Amazing World Of Gumball | The Wish; The Anybody
11:30 am | Amazing World Of Gumball | The Future; The Candidate
12:00 pm | Victor And Valentino | Guillermo's Gathering; El Silbon
12:30 pm | Victor And Valentino | Escape From Bebe Bay; Folk Art Foes
 1:00 pm | Amazing World of Gumball | The Candidate; The Faith
 1:30 pm | Amazing World of Gumball | The Anybody; The Pact
 2:00 pm | Amazing World of Gumball | The Neighbor; The Shippening
 2:30 pm | Amazing World of Gumball | The Brain; The Parents
 3:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 3:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 4:00 pm | Teen Titans Go! | Pirates; I See You
 4:30 pm | Teen Titans Go! | Brian; Nature
 5:00 pm | Total DramaRama | Stink. Stank. Stunk; Cuttin' Corners
 5:30 pm | Total DramaRama | Camping Is In Tents
 6:00 pm | Teen Titans Go! | The Overbite; Spice Game
 6:30 pm | Teen Titans Go! | Riding The Dragon; Croissant
 7:00 pm | Amazing World Of Gumball | The Possession; The Cringe
 7:30 pm | Amazing World Of Gumball | The Buddy; The Father