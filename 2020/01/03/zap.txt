# 2020-01-03
 6:00 am | Amazing World of Gumball | The Return
 6:15 am | Amazing World of Gumball | The Signature
 6:30 am | Amazing World of Gumball | The Nemesis
 6:45 am | Amazing World of Gumball | The Gift
 7:00 am | Mao Mao: Heroes of Pure Heart | Bao Bao's Revenge
 7:15 am | Mao Mao: Heroes of Pure Heart | Bobo Chan
 7:30 am | Mao Mao: Heroes of Pure Heart | The Truth Stinks
 7:45 am | Mao Mao: Heroes of Pure Heart | Small
 8:00 am | Teen Titans Go! | Two Parter: Part One
 8:15 am | Teen Titans Go! | Two Parter: Part Two
 8:30 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
 8:45 am | Teen Titans Go! | Secret Garden
 9:00 am | Teen Titans Go! | The True Meaning of Christmas
 9:15 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:30 am | Teen Titans Go! | Squash & Stretch
 9:45 am | Teen Titans Go! | Garage Sale
10:00 am | Craig of the Creek | Monster in the Garden
10:15 am | Craig of the Creek | Kelsey Quest
10:30 am | Craig of the Creek | The Curse
10:45 am | Craig of the Creek | JPony
11:00 am | Amazing World of Gumball | The Society; The Spoiler
11:30 am | Amazing World of Gumball | The Countdown; The Nobody
12:00 pm | Victor and Valentino | Lonely Haunts Club
12:15 pm | Victor and Valentino | Churro Kings
12:30 pm | Victor and Valentino | The Dark Room
12:45 pm | Victor and Valentino | Know It All
 1:00 pm | Amazing World of Gumball | The Mothers; The Password
 1:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:00 pm | Amazing World of Gumball | The Mirror; The Burden
 2:30 pm | Amazing World of Gumball | The Bros; The Man
 3:00 pm | Teen Titans Go! | How Bout Some Effort
 3:15 pm | Teen Titans Go! | Arms Race With Legs
 3:30 pm | Teen Titans Go! | Bottle Episode
 3:45 pm | Teen Titans Go! | Obinray
 4:00 pm | Teen Titans Go! | Pyramid Scheme
 4:15 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 4:30 pm | Teen Titans Go! | Finally a Lesson
 4:45 pm | Teen Titans Go! | Wally T
 5:00 pm | Total DramaRama | Sharing Is Caring
 5:15 pm | Total DramaRama | Inglorious Toddlers
 5:30 pm | Total DramaRama | Ant We All Just Get Along
 5:45 pm | Total DramaRama | Not Without My Fudgy Lumps
 6:00 pm | Teen Titans Go! | Cat's Fancy
 6:15 pm | Teen Titans Go! | The Spice Game
 6:30 pm | Teen Titans Go! | Leg Day
 6:45 pm | Teen Titans Go! | I'm the Sauce
 7:00 pm | Apple & Onion | Party Popper
 7:15 pm | Apple & Onion | Apple's in Charge
 7:30 pm | Apple & Onion | Lil Noodle
 7:45 pm | Apple & Onion | Onionless