# 2020-01-06
 6:00 am | Amazing World of Gumball | The Sale; The Pest
 6:30 am | Amazing World of Gumball | The Gift; The Parking
 7:00 am | Amazing World of Gumball | The Upgrade; The Routine
 7:30 am | Amazing World of Gumball | The Comic; The Romantic
 8:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 8:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 9:00 am | Teen Titans Go! | Double Trouble; The Date
 9:30 am | Teen Titans Go! | Dude Relax!; Laundry Day
10:00 am | Craig Of The Creek | Craig And The Kid's Table; Jextra Perrestrial
10:30 am | Craig Of The Creek | The Haunted Dollhouse; Secret Book Club
11:00 am | Amazing World of Gumball | The Triangle; The Money
11:30 am | Amazing World of Gumball | The Egg; The Downer
12:00 pm | Victor And Valentino | Folk Art Foes; Dead Ringer
12:30 pm | Victor And Valentino | Welcome to the Underworld
 1:00 pm | Amazing World of Gumball | The Advice; The Signal
 1:30 pm | Amazing World of Gumball | The Parasite; The Love
 2:00 pm | Amazing World of Gumball | The Awkwardness; The Nest
 2:30 pm | Amazing World of Gumball | The Points; The Bus
 3:00 pm | Teen Titans Go! | Parasite; Starliar
 3:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 4:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 4:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 5:00 pm | Total DramaRama | Invasion Of The Booger Snatchers; Paint That A Shame
 5:30 pm | Total DramaRama | Snots Landing; Wristy Business
 6:00 pm | Teen Titans Go! | Orangins; Rad Dudes With Bad Tudes
 6:30 pm | Teen Titans Go! | The Avogodo; Wally T
 7:00 pm | Amazing World Of Gumball | The Inquisition; The Potion
 7:30 pm | Infinity Train | The Black Market Car
 7:45 pm | Infinity Train | The Family Tree Car