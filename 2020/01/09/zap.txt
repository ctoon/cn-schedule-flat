# 2020-01-09
 6:00 am | Amazing World of Gumball | The Night
 6:15 am | Amazing World of Gumball | The Misunderstandings
 6:30 am | Amazing World of Gumball | The Roots
 6:45 am | Amazing World of Gumball | The Blame
 7:00 am | Amazing World of Gumball | The Detective
 7:15 am | Amazing World of Gumball | The Fury
 7:30 am | Amazing World of Gumball | The Compilation
 7:45 am | Amazing World of Gumball | The Stories
 8:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 8:30 am | Teen Titans Go! | Books; Lazy Sunday
 9:00 am | Teen Titans Go! | No Power; Sidekick
 9:30 am | Teen Titans Go! | Power Moves; Staring at the Future
10:00 am | Craig of the Creek | Sparkle Cadet
10:15 am | Craig of the Creek | The Climb
10:30 am | Craig of the Creek | Council of the Creek
10:45 am | Craig of the Creek | The Last Kid in the Creek
11:00 am | Amazing World of Gumball | The BFFS
11:15 am | Amazing World of Gumball | The Intelligence
11:30 am | Amazing World of Gumball | The Oracle; The Safety
12:00 pm | Victor and Valentino | Hurricane Chata
12:15 pm | Victor and Valentino | Cuddle Monster
12:30 pm | Victor and Valentino | Suerte
12:45 pm | Victor and Valentino | Boss for a Day
 1:00 pm | Amazing World of Gumball | The Slide
 1:15 pm | Amazing World of Gumball | The Loophole
 1:30 pm | Amazing World of Gumball | The Copycats
 1:45 pm | Amazing World of Gumball | The Potato
 2:00 pm | Amazing World of Gumball | The Fuss
 2:15 pm | Amazing World of Gumball | The Outside
 2:30 pm | Amazing World of Gumball | The Vase
 2:45 pm | Amazing World of Gumball | The Matchmaker
 3:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 3:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 4:00 pm | Teen Titans Go! | Double Trouble; The Date
 4:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 5:00 pm | Total DramaRama | All Up in Your Drill
 5:15 pm | Total DramaRama | There Are No Hoppy Endings
 5:30 pm | Total DramaRama | Toys Will Be Toys
 5:45 pm | Total DramaRama | Duncan Disorderly
 6:00 pm | Teen Titans Go! | Permanent Record
 6:15 pm | Teen Titans Go! | Pyramid Scheme
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Amazing World of Gumball | The Revolt
 7:15 pm | Amazing World of Gumball | The Founder
 7:30 pm | Infinity Train | The Mall Car
 7:45 pm | Infinity Train | The Wasteland