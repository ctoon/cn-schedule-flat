# 2020-01-25
 6:00 am | Amazing World Of Gumball | The Factory; The Understanding
 6:30 am | Amazing World Of Gumball | The Wish; The Transformation
 7:00 am | Teen Titans Go! | BBRAE
 7:30 am | Teen Titans Go! | Titan Saving Time; Classic Titans
 8:00 am | Teen Titans Go! | Mr. Butt; Man Person
 8:30 am | Teen Titans Go! | Ones And Zeros; Master Detective
 9:00 am | Total DramaRama | The Tooth About Zombies
 9:15 am | Total DramaRama | Robo Teacher
 9:30 am | Apple & Onion | Apple's Formula
 9:45 am | Apple & Onion | Selfish Shellfish
10:00 am | Teen Titans Go! | Beast Boy's That's What's Up
11:00 am | Teen Titans Go! | Hand Zombie; The Academy
11:30 am | Teen Titans Go! | Employee Of The Month Redux; Throne Of Bones
12:00 pm | Teen Titans Go! | Orangins; Costume Contest
12:30 pm | LEGO Ninjago | A Fragile Hope; Once and For All
 1:00 pm | Amazing World Of Gumball | The Roots; The Ollie
 1:30 pm | Amazing World of Gumball | The Blame; The Potato
 2:00 pm | Amazing World Of Gumball | The Sorcerer; The Line
 2:30 pm | Amazing World Of Gumball | The Detective; The Console
 3:00 pm | Craig of the Creek | Vulture's Nest; Kelsey Quest
 3:30 pm | Craig Of The Creek | Jpony; Ace Of Squares
 4:00 pm | Victor And Valentino | A New Don; It Grows
 4:30 pm | Victor And Valentino | Know It All; Churro Kings
 5:00 pm | Amazing World of Gumball | The Fury; The Outside
 5:30 pm | Amazing World Of Gumball | The Compilation; The Copycats
 6:00 pm | Amazing World Of Gumball | The Catfish; The Singing
 6:30 pm | Amazing World Of Gumball | The Cycle; The Puppets
 7:00 pm | Apple & Onion | Apple's Formula
 7:15 pm | Apple & Onion | Selfish Shellfish
 7:30 pm | Apple & Onion | Party Popper; Fun Proof