# 2020-01-25
 6:00 am | Amazing World of Gumball | The Factory
 6:15 am | Amazing World of Gumball | The Understanding
 6:30 am | Amazing World of Gumball | The Wish
 6:45 am | Amazing World of Gumball | The Transformation
 7:00 am | Teen Titans Go! | BBRAE
 7:30 am | Teen Titans Go! | Titan Saving Time
 7:45 am | Teen Titans Go! | Classic Titans
 8:00 am | Teen Titans Go! | The Gold Standard
 8:15 am | Teen Titans Go! | Labor Day
 8:30 am | Teen Titans Go! | Master Detective
 8:45 am | Teen Titans Go! | Ones and Zeroes
 9:00 am | Total DramaRama | The Tooth About Zombies
 9:15 am | Total DramaRama | Robo Teacher
 9:30 am | Apple & Onion | Apple's Formula
 9:45 am | Apple & Onion | Selfish Shellfish
10:00 am | Teen Titans Go! | Beast Boy's That's What's Up
11:00 am | Teen Titans Go! | Hand Zombie
11:15 am | Teen Titans Go! | The Academy
11:30 am | Teen Titans Go! | Employee of the Month Redux
11:45 am | Teen Titans Go! | Throne of Bones
12:00 pm | Teen Titans Go! | Orangins
12:15 pm | Teen Titans Go! | Costume Contest
12:30 pm | LEGO Ninjago | A Fragile Hope
12:45 pm | LEGO Ninjago | Once and for All
 1:00 pm | Amazing World of Gumball | The Roots
 1:15 pm | Amazing World of Gumball | The Ollie
 1:30 pm | Amazing World of Gumball | The Blame
 1:45 pm | Amazing World of Gumball | The Potato
 2:00 pm | Amazing World of Gumball | The Sorcerer
 2:15 pm | Amazing World of Gumball | The Line
 2:30 pm | Amazing World of Gumball | The Detective
 2:45 pm | Amazing World of Gumball | The Console
 3:00 pm | Craig of the Creek | Vulture's Nest
 3:15 pm | Craig of the Creek | Kelsey Quest
 3:30 pm | Craig of the Creek | JPony
 3:45 pm | Craig of the Creek | Ace of Squares
 4:00 pm | Victor and Valentino | It Grows
 4:15 pm | Victor and Valentino | A New Don
 4:30 pm | Victor and Valentino | Churro Kings
 4:45 pm | Victor and Valentino | Know It All
 5:00 pm | Amazing World of Gumball | The Fury
 5:15 pm | Amazing World of Gumball | The Outside
 5:30 pm | Amazing World of Gumball | The Compilation
 5:45 pm | Amazing World of Gumball | The Copycats
 6:00 pm | Amazing World of Gumball | The Catfish
 6:15 pm | Amazing World of Gumball | The Singing
 6:30 pm | Amazing World of Gumball | The Cycle
 6:45 pm | Amazing World of Gumball | The Puppets
 7:00 pm | Apple & Onion | Apple's Formula
 7:15 pm | Apple & Onion | Selfish Shellfish
 7:30 pm | Apple & Onion | Fun Proof
 7:45 pm | Apple & Onion | Party Popper