# 2020-01-24
 6:00 am | Amazing World of Gumball | The Future
 6:15 am | Amazing World of Gumball | The Wish
 6:30 am | Amazing World of Gumball | The Factory
 6:45 am | Amazing World of Gumball | The Agent
 7:00 am | Amazing World of Gumball | The Web
 7:15 am | Amazing World of Gumball | The Mess
 7:30 am | Amazing World of Gumball | The Heart
 7:45 am | Amazing World of Gumball | The Revolt
 8:00 am | Teen Titans Go! | The Fourth Wall
 8:15 am | Teen Titans Go! | Black Friday
 8:30 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 8:45 am | Teen Titans Go! | Squash & Stretch
 9:00 am | Teen Titans Go! | Grube's Fairytales
 9:30 am | Teen Titans Go! | A Farce
 9:45 am | Teen Titans Go! | Scary Figure Dance
10:00 am | Craig of the Creek | Dinner at the Creek
10:15 am | Craig of the Creek | Too Many Treasures
10:30 am | Craig of the Creek | The Takeout Mission
10:45 am | Craig of the Creek | The Final Book
11:00 am | Amazing World of Gumball | The Awareness
11:15 am | Amazing World of Gumball | The Sucker
11:30 am | Amazing World of Gumball | The Stink
11:45 am | Amazing World of Gumball | The Lady
12:00 pm | Victor and Valentino | Suerte
12:15 pm | Victor and Valentino | It Grows
12:30 pm | Victor and Valentino | Lonely Haunts Club
12:45 pm | Victor and Valentino | A New Don
 1:00 pm | Amazing World of Gumball | The Phone; The Job
 1:30 pm | Amazing World of Gumball | The Words; The Apology
 2:00 pm | Amazing World of Gumball | The Watch; The Bet
 2:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 3:00 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 3:30 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 4:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 4:30 pm | Teen Titans Go! | Ones and Zeroes
 4:45 pm | Teen Titans Go! | Career Day
 5:00 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:15 pm | Total DramaRama | Paint That a Shame
 5:30 pm | Total DramaRama | Snots Landing
 5:45 pm | Total DramaRama | Know It All
 6:00 pm | Teen Titans Go! | History Lesson
 6:15 pm | Teen Titans Go! | Jinxed
 6:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 7:00 pm | Amazing World of Gumball | The Transformation
 7:15 pm | Amazing World of Gumball | The List
 7:30 pm | Amazing World of Gumball | The Spinoffs
 7:45 pm | Amazing World of Gumball | The Line