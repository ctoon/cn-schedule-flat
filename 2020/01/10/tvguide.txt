# 2020-01-10
 6:00 am | Amazing World of Gumball | The Disaster; The Re-run
 6:30 am | Amazing World of Gumball | The Boredom; The Guy
 7:00 am | Amazing World of Gumball | The Vision; The Choices
 7:30 am | Amazing World of Gumball | The Code; The Test
 8:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 8:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:00 am | Teen Titans Go! | Waffles; Opposites
 9:30 am | Teen Titans Go! | Birds; Be Mine
10:00 am | Craig Of The Creek | Kelsey The Author; Doorway To Helen
10:30 am | Craig Of The Creek | Turning The Tables; Ace Of Squares
11:00 am | Amazing World Of Gumball | The Decisions; The Schooling
11:30 am | Amazing World of Gumball | The Butterfly; The Question
12:00 pm | Victor And Valentino | It Grows; Lonely Haunts Club
12:30 pm | Victor And Valentino | The Dark Room; A New Don
 1:00 pm | Amazing World of Gumball | The Box; The Console
 1:30 pm | Amazing World of Gumball | The Ollie; The Catfish
 2:00 pm | Amazing World of Gumball | The Cycle; The Stars
 2:30 pm | Amazing World of Gumball | The Grades; The Diet
 3:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 3:30 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 4:00 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 4:30 pm | Teen Titans Go! | Super Robin; Tower Power
 5:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Soother Or Later
 5:30 pm | Total DramaRama | Gum And Gummer; Mutt Ado About Owen
 6:00 pm | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
 6:30 pm | Teen Titans Go! | Movie Night; BBBday!
 7:00 pm | Amazing World Of Gumball | The Heart; The Parents
 7:30 pm | Infinity Train | The Tape Car
 7:45 pm | Infinity Train | The Number Car