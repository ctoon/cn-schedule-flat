# 2020-12-08
 6:00 am | Amazing World of Gumball | The Stars
 6:15 am | Amazing World of Gumball | The Father
 6:30 am | Amazing World of Gumball | The Box
 6:45 am | Amazing World of Gumball | The Cringe
 7:00 am | Amazing World of Gumball | The Matchmaker
 7:15 am | Amazing World of Gumball | The Neighbor
 7:30 am | Amazing World of Gumball | The Grades
 7:45 am | Amazing World of Gumball | The Pact
 8:00 am | Teen Titans Go! | Booty Scooty
 8:15 am | Teen Titans Go! | Who's Laughing Now
 8:30 am | Teen Titans Go! | Oregon Trail
 8:45 am | Teen Titans Go! | Snuggle Time
 9:00 am | Teen Titans Go! | Oh Yeah!
 9:15 am | Teen Titans Go! | Riding the Dragon
 9:30 am | Teen Titans Go! | The Overbite
 9:45 am | Teen Titans Go! | Baby Mouth
10:00 am | Craig of the Creek | The Kid From 3030
10:15 am | Craig of the Creek | The Ice Pop Trio
10:30 am | Craig of the Creek | Power Punchers
10:45 am | Craig of the Creek | Pencil Break Mania
11:00 am | Teen Titans Go! | The Avogodo
11:15 am | Teen Titans Go! | Orangins
11:30 am | Teen Titans Go! | Jinxed
11:45 am | Teen Titans Go! | Brain Percentages
12:00 pm | Teen Titans Go! | BL4Z3
12:15 pm | Teen Titans Go! | Hot Salad Water
12:30 pm | Teen Titans Go! | Lication
12:45 pm | Teen Titans Go! | Thumb War
 1:00 pm | Amazing World of Gumball | The Mirror; The Burden
 1:30 pm | Amazing World of Gumball | The Bros; The Man
 2:00 pm | Amazing World of Gumball | The Factory
 2:15 pm | Amazing World of Gumball | The Agent
 2:30 pm | Amazing World of Gumball | The Web
 2:45 pm | Amazing World of Gumball | The Mess
 3:00 pm | Amazing World of Gumball | The Heart
 3:15 pm | Amazing World of Gumball | The Revolt
 3:30 pm | Amazing World of Gumball | The BFFS
 3:45 pm | Amazing World of Gumball | The Inquisition
 4:00 pm | Craig of the Creek | Dinner at the Creek
 4:15 pm | Craig of the Creek | King of Camping
 4:30 pm | Craig of the Creek | Jessica's Trail
 4:45 pm | Craig of the Creek | Itch to Explore
 5:00 pm | Total DramaRama | Royal Flush
 5:15 pm | Total DramaRama | A Ninjustice to Harold
 5:30 pm | Total DramaRama | Total Eclipse of the Fart
 5:45 pm | Total DramaRama | Having the Timeout of Our Lives
 6:00 pm | Teen Titans Go! | Flashback
 6:30 pm | Teen Titans Go! | Bro-Pocalypse
 6:45 pm | Teen Titans Go! | The Scoop
 7:00 pm | Teen Titans Go! | Chicken in the Cradle
 7:15 pm | Teen Titans Go! | Tower Renovation
 7:30 pm | Teen Titans Go! | My Name Is Jose
 7:45 pm | Teen Titans Go! | Bucket List
 8:00 pm | Amazing World of Gumball | The Mothers; The Password
 8:30 pm | Amazing World of Gumball | The Procrastinators; The Shell