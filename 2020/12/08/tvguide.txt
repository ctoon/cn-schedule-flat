# 2020-12-08
 6:00 am | Amazing World of Gumball | The Stars; The Father
 6:30 am | Amazing World of Gumball | The Box; The Cringe
 7:00 am | Amazing World of Gumball | The Matchmaker; The Neighbor
 7:30 am | Amazing World of Gumball | The Grades; The Pact
 8:00 am | Teen Titans Go! | Who's Laughing Now; Booty Scooty
 8:30 am | Teen Titans Go! | Oregon Trail; Snuggle Time
 9:00 am | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 9:30 am | Teen Titans Go! | The Overbite; Shrimps and Prime Rib
10:00 am | Craig of the Creek | The Kid From 3030; The Ice Pop Trio
10:30 am | Craig of the Creek | Power Punchers; Pencil Break Mania
11:00 am | Teen Titans Go! | The Avogodo; Orangins
11:30 am | Teen Titans Go! | Jinxed; Brain Percentages
12:00 pm | Teen Titans Go! | Hot Salad Water; BL4Z3
12:30 pm | Teen Titans Go! | Labor Day; Lication
 1:00 pm | Amazing World of Gumball | The Mirror; The Burden
 1:30 pm | Amazing World of Gumball | The Man; The Bros
 2:00 pm | Amazing World Of Gumball | The Agent; The Factory
 2:30 pm | Amazing World Of Gumball | The Web; The Mess
 3:00 pm | Amazing World Of Gumball | The Heart; The Revolt
 3:30 pm | Amazing World Of Gumball | The Bffs; The Inquisition
 4:00 pm | Craig of the Creek | Dinner at the Creek; King of Camping
 4:30 pm | Craig of the Creek | Jessica's Trail; Itch to Explore
 5:00 pm | Total DramaRama | Royal Flush; A Ninjustice to Harold
 5:30 pm | Total DramaRama | Total Eclipse of the Fart Having the Timeout of Our Lives
 6:00 pm | Teen Titans Go! | Flashback
 6:30 pm | Teen Titans Go! | Bro-pocalypse; The Scoop
 7:00 pm | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
 7:30 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 8:00 pm | Amazing World of Gumball | The Mothers / The Password
 8:30 pm | Amazing World of Gumball | The Procrastinators; The Shell