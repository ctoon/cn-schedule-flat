# 2020-12-18
 6:00 am | Amazing World of Gumball | The Countdown; The Nobody
 6:30 am | Amazing World of Gumball | The Downer; The Egg
 7:00 am | Amazing World of Gumball | The Triangle; The Money
 7:30 am | Amazing World of Gumball | The Return
 7:45 am | Amazing World of Gumball | The Mess
 8:00 am | Teen Titans Go! | How's This for a Special? Spaaaace
 8:30 am | Teen Titans Go! | BBRBDAY
 8:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 9:00 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 9:15 am | Teen Titans Go! | Business Ethics Wink Wink
 9:30 am | Teen Titans Go! | Genie President
 9:45 am | Teen Titans Go! | Tall Titan Tales
10:00 am | Craig of the Creek | Jacob of the Creek
10:15 am | Craig of the Creek | The Curse
10:30 am | Craig of the Creek | Return of the Honeysuckle Rangers
10:45 am | Craig of the Creek | Dog Decider
11:00 am | Teen Titans Go! | Girls Night In Part 1
11:30 am | Teen Titans Go! | The Great Disaster
11:45 am | Teen Titans Go! | The Viewers Decide
12:00 pm | Teen Titans Go! | Cartoon Feud
12:15 pm | Teen Titans Go! | Curse of the Booty Scooty
12:30 pm | Teen Titans Go! | Collect Them All
12:45 pm | Teen Titans Go! | Butt Atoms
 1:00 pm | Amazing World of Gumball | The Signal
 1:15 pm | Amazing World of Gumball | The Ex
 1:30 pm | Amazing World of Gumball | The Parasite
 1:45 pm | Amazing World of Gumball | The Sorcerer
 2:00 pm | Amazing World of Gumball | The Parking
 2:15 pm | Amazing World of Gumball | The Copycats
 2:30 pm | Amazing World of Gumball | The Routine
 2:45 pm | Amazing World of Gumball | Christmas
 3:00 pm | Amazing World of Gumball | The Upgrade
 3:15 pm | Amazing World of Gumball | The Fuss
 3:30 pm | Amazing World of Gumball | The Comic
 3:45 pm | Amazing World of Gumball | The Outside
 4:00 pm | Craig of the Creek | Turning the Tables
 4:15 pm | Craig of the Creek | Under the Overpass
 4:30 pm | Craig of the Creek | Kelsey the Author
 4:45 pm | Craig of the Creek | The Invitation
 5:00 pm | Total DramaRama | OWW
 5:15 pm | Total DramaRama | Snow Way Out
 5:30 pm | Total DramaRama | Me, My Elf, and I
 5:45 pm | Total DramaRama | Snow Country for Old Men
 6:00 pm | Teen Titans Go! | The Streak, Part 1
 6:15 pm | Teen Titans Go! | The Streak, Part 2
 6:30 pm | Teen Titans Go! | Teen Titans Go! Titans vs. Santa
 7:30 pm | Teen Titans Go! | Beast Boy on a Shelf
 7:45 pm | Teen Titans Go! | Christmas Crusaders
 8:00 pm | Apple & Onion | Slobbery
 8:15 pm | Apple & Onion | Microwave's Dance Club
 8:30 pm | Apple & Onion | All Work and No Play
 8:45 pm | Apple & Onion | Drone Shoes