# 2020-12-02
 6:00 am | Amazing World of Gumball | The Heist
 6:15 am | Amazing World of Gumball | The Shippening
 6:30 am | Amazing World of Gumball | The Petals
 6:45 am | Amazing World of Gumball | The Brain
 7:00 am | Amazing World of Gumball | The Nuisance
 7:15 am | Amazing World of Gumball | The Stink
 7:30 am | Amazing World of Gumball | The Best
 7:45 am | Amazing World of Gumball | The Awareness
 8:00 am | Teen Titans Go! | Pyramid Scheme
 8:15 am | Teen Titans Go! | History Lesson
 8:30 am | Teen Titans Go! | Finally a Lesson
 8:45 am | Teen Titans Go! | The Art of Ninjutsu
 9:00 am | Teen Titans Go! | Brain Percentages
 9:15 am | Teen Titans Go! | Magic Man
 9:30 am | Teen Titans Go! | Bat Scouts
 9:45 am | Teen Titans Go! | Record Book
10:00 am | Craig of the Creek | Under the Overpass
10:15 am | Craig of the Creek | Beyond the Rapids
10:30 am | Craig of the Creek | The Invitation
10:45 am | Craig of the Creek | The Jinxening
11:00 am | Teen Titans Go! | Arms Race With Legs
11:15 am | Teen Titans Go! | Think About Your Future
11:30 am | Teen Titans Go! | Obinray
11:45 am | Teen Titans Go! | Booty Scooty
12:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
12:15 pm | Teen Titans Go! | Who's Laughing Now
12:30 pm | Teen Titans Go! | Wally T
12:45 pm | Teen Titans Go! | Oregon Trail
 1:00 pm | Amazing World of Gumball | The Ollie
 1:15 pm | Amazing World of Gumball | The Singing
 1:30 pm | Amazing World of Gumball | The Menu
 1:45 pm | Amazing World of Gumball | The Puppets
 2:00 pm | Amazing World of Gumball | The Sorcerer
 2:15 pm | Amazing World of Gumball | The Lady
 2:30 pm | Amazing World of Gumball | The Uncle
 2:45 pm | Amazing World of Gumball | The Sucker
 3:00 pm | Amazing World of Gumball | The Outside
 3:15 pm | Amazing World of Gumball | The Cage
 3:30 pm | Amazing World of Gumball | The Copycats
 3:45 pm | Amazing World of Gumball | The Rival
 4:00 pm | Craig of the Creek | JPony
 4:15 pm | Craig of the Creek | Council of the Creek: Operation Hive-Mind
 4:30 pm | Craig of the Creek | Ace of Squares
 4:45 pm | Craig of the Creek | The Bike Thief
 5:00 pm | Total DramaRama | Supply Mom
 5:15 pm | Total DramaRama | Sharing Is Caring
 5:30 pm | Total DramaRama | Mooshy Mon Mons
 5:45 pm | Total DramaRama | Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Shrimps and Prime Rib
 6:15 pm | Teen Titans Go! | TV Knight
 6:30 pm | Teen Titans Go! | Halloween vs. Christmas
 6:45 pm | Teen Titans Go! | BBSFBDAY
 7:00 pm | Teen Titans Go! | Booby Trap House
 7:15 pm | Teen Titans Go! | Teen Titans Save Christmas
 7:30 pm | Teen Titans Go! | Fish Water
 7:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 8:00 pm | Amazing World of Gumball | The Diet
 8:15 pm | Amazing World of Gumball | The Candidate
 8:30 pm | Amazing World of Gumball | The Ex
 8:45 pm | Amazing World of Gumball | The Anybody