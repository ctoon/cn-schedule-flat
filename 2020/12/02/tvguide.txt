# 2020-12-02
 6:00 am | Amazing World of Gumball | The Heist; The Shippening
 6:30 am | Amazing World of Gumball | The Petals; The Brain
 7:00 am | Amazing World of Gumball | The Nuisance; The Stink
 7:30 am | Amazing World of Gumball | The Best; The Awareness
 8:00 am | Teen Titans Go! | Pyramid Scheme; History Lesson
 8:30 am | Teen Titans Go! | Finally a Lesson; The Art of Ninjutsu
 9:00 am | Teen Titans Go! | Magic Man ; The Titans Go Casual
 9:30 am | Teen Titans Go! | Record Book; Bat Scouts
10:00 am | Craig of the Creek | Under the Overpass; Beyond the Rapids
10:30 am | Craig of the Creek | The Invitation; The Jinxening
11:00 am | Teen Titans Go! | Arms Race With Legs; Think About Your Future
11:30 am | Teen Titans Go! | Obinray; Booty Scooty
12:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice Who's Laughing Now
12:30 pm | Teen Titans Go! | Wally T; Oregon Trail
 1:00 pm | Amazing World of Gumball | The Singing; The Ollie
 1:30 pm | Amazing World of Gumball | The Puppets; The Menu
 2:00 pm | Amazing World of Gumball | The Lady; The Sorcerer
 2:30 pm | Amazing World of Gumball | The Uncle; The Sucker
 3:00 pm | Amazing World of Gumball | The Outside; The Cage
 3:30 pm | Amazing World of Gumball | The Copycats; The Rival
 4:00 pm | Craig of the Creek | Jpony; Council of the Creek: Operation Hive-Mind
 4:30 pm | Craig of the Creek | Ace of Squares; The Bike Thief
 5:00 pm | Total DramaRama | Supply Mom; Sharing Is Caring
 5:30 pm | Total DramaRama | Mooshy Mon Mons; Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | TV Knight; Shrimps and Prime Rib
 6:30 pm | Teen Titans Go! | Halloween vs. Christmas; Bbsfbday!
 7:00 pm | Teen Titans Go! | Booby Trap House; Teen Titans Save Christmas
 7:30 pm | Teen Titans Go! | Fish Water; Inner Beauty of a Cactus
 8:00 pm | Amazing World of Gumball | The Candidate; The Diet
 8:30 pm | Amazing World of Gumball | The Ex; The Anybody