# 2020-12-12
 6:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 am | Teen Titans Go! | Double Trouble; The Date
 7:30 am | Teen Titans Go! | Bucket List; TV Knight 6
 8:00 am | Teen Titans Go! | Kryponite; Thumb War
 8:30 am | Teen Titans Go! | Toddler Titans...Yay! Huggbees
 9:00 am | Teen Titans Go! | The Cast
 9:15 am | Teen Titans Go! | Baby Mouth
 9:30 am | Total DramaRama | Jelly Aches
 9:45 am | Total DramaRama | Simply Perfect
10:00 am | Craig of the Creek | The Pencil Break Mania Last Game of Summer
10:30 am | Amazing World Of Gumball | The Singing; The Stink
11:00 am | Amazing World Of Gumball | The Puppets; The Awareness
11:30 am | Amazing World Of Gumball | The Lady; The Parents
12:00 pm | Amazing World Of Gumball | The Sucker; The Founder
12:30 pm | Amazing World Of Gumball | The Cage; The Schooling
 1:00 pm | Amazing World Of Gumball | The Rival; The Intelligence
 1:30 pm | Amazing World Of Gumball | The One; The Potion
 2:00 pm | Amazing World of Gumball | The Vegging; The Spinoffs
 2:30 pm | Amazing World Of Gumball | The Father; The Transformation
 3:00 pm | Teen Titans Go! | Dude Relax!; Laundry Day
 3:30 pm | Teen Titans Go! | Ghostboy; La Larva de Amor
 4:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 4:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 5:00 pm | Teen Titans Go! | Titans vs. Santa Special
 6:00 pm | Elf: Buddy's Musical Christmas | 
 7:00 pm | Paddington | 