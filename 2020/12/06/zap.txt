# 2020-12-06
 6:00 am | Teen Titans Go! | Had to Be There
 6:15 am | Teen Titans Go! | TV Knight 5
 6:30 am | Bakugan: Armored Alliance | The Past Revealed; Run! A Storm's on the Way
 7:00 am | Teen Titans Go! | Girls Night In Part 1
 7:30 am | Teen Titans Go! | Walk Away
 7:45 am | Teen Titans Go! | Magic Man
 8:00 am | Teen Titans Go! | Record Book
 8:15 am | Teen Titans Go! | Brain Percentages
 8:30 am | Teen Titans Go! | Bat Scouts
 8:45 am | Teen Titans Go! | We're Off to Get Awards
 9:00 am | Teen Titans Go! | The Streak, Part 1
 9:15 am | Teen Titans Go! | The Streak, Part 2
 9:30 am | Teen Titans Go! | Teen Titans Go! Titans vs. Santa
10:00 am | Teen Titans Go! | Kryponite
10:30 am | Teen Titans Go! | Beast Boy on a Shelf
10:45 am | Teen Titans Go! | Christmas Crusaders
11:00 am | Amazing World of Gumball | The Loophole
11:15 am | Amazing World of Gumball | The Copycats
11:30 am | Amazing World of Gumball | The Fuss
11:45 am | Amazing World of Gumball | The Outside
12:00 pm | Amazing World of Gumball | The Vase
12:15 pm | Amazing World of Gumball | The Matchmaker
12:30 pm | Amazing World of Gumball | The Ollie
12:45 pm | Amazing World of Gumball | The Catfish
 1:00 pm | Amazing World of Gumball | The Diet
 1:15 pm | Amazing World of Gumball | The Ex
 1:30 pm | Amazing World of Gumball | The Menu
 1:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 2:00 pm | Amazing World of Gumball | The Uncle
 2:15 pm | Amazing World of Gumball | The Deal
 2:30 pm | Amazing World of Gumball | The Heist
 2:45 pm | Amazing World of Gumball | The Line
 3:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 4:00 pm | Teen Titans Go! | Super Hero Summer Camp
 5:00 pm | Paddington | 
 7:00 pm | Paddington 2 | 