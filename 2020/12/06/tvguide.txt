# 2020-12-06
 6:00 am | Teen Titans Go! | Had to Be There; TV Knight 5
 6:30 am | Bakugan: Armored Alliance | The Past Revealed; Run! A Storm's On The Way
 7:00 am | Teen Titans Go! | Girls Night In
 7:30 am | Teen Titans Go! | Walk Away Magic Man
 8:00 am | Teen Titans Go! | Record Book Titans Go Casual
 8:30 am | Teen Titans Go! | We're Off To Get Awards; Bat Scouts
 9:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 9:30 am | Teen Titans Go! | Titans vs. Santa Special
10:30 am | Teen Titans Go! | Beast Boy On A Shelf; Christmas Crusaders
11:00 am | Amazing World of Gumball | The Copycats; The Loophole
11:30 am | Amazing World of Gumball | The Fuss; The Outside
12:00 pm | Amazing World of Gumball | The Vase; The Matchmaker
12:30 pm | Amazing World of Gumball | The Ollie; The Catfish
 1:00 pm | Amazing World of Gumball | The Diet; The Ex
 1:30 pm | Amazing World of Gumball | The Menu; The List
 2:00 pm | Amazing World of Gumball | The Deal; The Uncle
 2:30 pm | Amazing World of Gumball | The Line; The Heist
 3:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 4:00 pm | Teen Titans Go! | Super Summer Hero Camp
 5:00 pm | The LEGO Batman Movie | 
 7:00 pm | The LEGO Movie | 