# 2020-12-11
 6:00 am | Amazing World Of Gumball | The Agent; The Factory
 6:30 am | Amazing World Of Gumball | The Web; The Mess
 7:00 am | Amazing World Of Gumball | The Heart; The Revolt
 7:30 am | Amazing World Of Gumball | The Bffs; The Inquisition
 8:00 am | Teen Titans Go! | The Avogodo; Orangins
 8:30 am | Teen Titans Go! | Jinxed; Brain Percentages
 9:00 am | Teen Titans Go! | Hot Salad Water; BL4Z3
 9:30 am | Teen Titans Go! | Labor Day; Lication
10:00 am | Craig of the Creek | Dinner at the Creek; King of Camping
10:30 am | Craig of the Creek | Jessica's Trail; Itch to Explore
11:00 am | Teen Titans Go! | Flashback
11:30 am | Teen Titans Go! | Bro-pocalypse; The Scoop
12:00 pm | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
12:30 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 1:00 pm | Amazing World of Gumball | The Christmas Others
 1:30 pm | Amazing World of Gumball | The Signature; The Choices
 2:00 pm | Amazing World of Gumball | The Mothers / The Password
 2:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 3:00 pm | Amazing World of Gumball | The Mirror; The Burden
 3:30 pm | Amazing World of Gumball | The Man; The Bros
 4:00 pm | Craig of the Creek | Great Fossil Rush; The Wildernessa
 4:30 pm | Craig of the Creek | The Mystery of the Timekeeper; Sunday Clothes
 5:00 pm | Total DramaRama | Dream Worriers Not Without My Fudgy Lumps
 5:30 pm | Total DramaRama | Grody to the Maximum; Paint That a Shame
 6:00 pm | Teen Titans Go! | The Metric System Vs. Freedom; I Used To Be A Peoples
 6:30 pm | Teen Titans Go! | The Chaff; Them Soviet Boys
 7:00 pm | Teen Titans Go! | Little Elvis; Tv Knight 4
 7:30 pm | Teen Titans Go! | Don't Be An Icarus; Lil' Dimples
 8:00 pm | Amazing World of Gumball | The Lie; The Nemesis
 8:30 pm | Amazing World of Gumball | The Crew; The Boredom