# 2020-12-03
 6:00 am | Amazing World of Gumball | The Nest
 6:15 am | Amazing World of Gumball | The Guy
 6:30 am | Amazing World of Gumball | The Points
 6:45 am | Amazing World of Gumball | The Boredom
 7:00 am | Amazing World of Gumball | The Bus
 7:15 am | Amazing World of Gumball | The Loophole
 7:30 am | Amazing World of Gumball | The Night
 7:45 am | Amazing World of Gumball | The Choices
 8:00 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 8:15 am | Teen Titans Go! | Who's Laughing Now
 8:30 am | Teen Titans Go! | Wally T
 8:45 am | Teen Titans Go! | Oregon Trail
 9:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 9:30 am | Teen Titans Go! | Brain Percentages
10:00 am | Craig of the Creek | Vulture's Nest
10:15 am | Craig of the Creek | In the Key of the Creek
10:30 am | Craig of the Creek | Kelsey Quest
10:45 am | Craig of the Creek | The Ground Is Lava!
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:30 am | Teen Titans Go! | Snuggle Time
11:45 am | Teen Titans Go! | Oh Yeah!
12:00 pm | Teen Titans Go! | Island Adventures
 1:00 pm | Amazing World of Gumball | The Catfish
 1:15 pm | Amazing World of Gumball | The One
 1:30 pm | Amazing World of Gumball | The Cycle
 1:45 pm | Amazing World of Gumball | The Vegging
 2:00 pm | Amazing World of Gumball | The Stars
 2:15 pm | Amazing World of Gumball | The Father
 2:30 pm | Amazing World of Gumball | The Box
 2:45 pm | Amazing World of Gumball | The Cringe
 3:00 pm | Amazing World of Gumball | The Matchmaker
 3:15 pm | Amazing World of Gumball | The Neighbor
 3:30 pm | Amazing World of Gumball | The Grades
 3:45 pm | Amazing World of Gumball | The Pact
 4:00 pm | Craig of the Creek | Dog Decider
 4:15 pm | Craig of the Creek | Jessica Shorts
 4:30 pm | Craig of the Creek | Bring Out Your Beast
 4:45 pm | Craig of the Creek | Ferret Quest
 5:00 pm | Total DramaRama | Student Becomes the Teacher
 5:15 pm | Total DramaRama | Cone in 60 Seconds
 5:30 pm | Total DramaRama | Beth and the Beanstalk
 5:45 pm | Total DramaRama | The Bad Guy Busters
 6:00 pm | Teen Titans Go! | The Fourth Wall
 6:15 pm | Teen Titans Go! | Squash & Stretch
 6:30 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 6:45 pm | Teen Titans Go! | Garage Sale
 7:00 pm | Teen Titans Go! | Grube's Fairytales
 7:15 pm | Teen Titans Go! | Secret Garden
 7:30 pm | Teen Titans Go! | A Farce
 7:45 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 8:00 pm | Amazing World of Gumball | The Disaster
 8:15 pm | Amazing World of Gumball | The Re-Run
 8:30 pm | Amazing World of Gumball | The Awkwardness
 8:45 pm | Amazing World of Gumball | The Stories