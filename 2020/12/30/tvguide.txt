# 2020-12-30
 6:00 am | Amazing World of Gumball | The Bus; The Singing
 6:30 am | Amazing World of Gumball | The Night; The Best
 7:00 am | Amazing World of Gumball | The Misunderstandings; The Worst
 7:30 am | Amazing World of Gumball | The Roots; The Deal
 8:00 am | Teen Titans Go! | Baby Mouth; Huggbees
 8:30 am | Teen Titans Go! | The Cast; Superhero Feud
 9:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 9:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
10:00 am | Craig of the Creek | Tea Timer's Ball; The Kid From 3030
10:30 am | Craig of the Creek | Cardboard Identity; The Power Punchers
11:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
11:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
12:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
12:30 pm | Teen Titans Go! | Books; Lazy Sunday
 1:00 pm | Amazing World of Gumball | The Catfish; The Parents
 1:30 pm | Amazing World of Gumball | The Cycle; The Founder
 2:00 pm | Amazing World of Gumball | The Sucker; The Boredom
 2:30 pm | Amazing World of Gumball | The Box; The Vegging
 3:00 pm | Amazing World of Gumball | The Choices; The One
 3:30 pm | Amazing World of Gumball | The Code; The Father
 4:00 pm | Craig of the Creek | Crisis at Elder Rock; Dinner at the Creek
 4:30 pm | Craig of the Creek | Kelsey the Worthy; Jessica's Trail
 5:00 pm | Total DramaRama | Cone in 60 Seconds; Driving Miss Crazy
 5:30 pm | Total DramaRama | The Bad Guy Busters; Weiner Takes All
 6:00 pm | Monsters vs. Aliens | 