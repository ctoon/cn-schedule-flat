# 2020-12-01
 9:00 pm | Bob's Burgers | The Unbearable Like-Likeness of Gene
 9:30 pm | Bob's Burgers | Mother Daughter Laser Razor
10:00 pm | American Dad | Deacon Stan, Jesus Man
10:30 pm | American Dad | Bullocks to Stan
11:00 pm | Family Guy | Secondhand Spoke
11:30 pm | Family Guy | Herpe the Love Sore
12:00 am | Rick and Morty | Auto Erotic Assimilation
12:30 am | Eric Andre Show | The A$AP Ferg Show
12:45 am | Eric Andre Show | Blannibal Quits
 1:00 am | Aqua Teen | Meatzone
 1:15 am | Aqua Teen | Super Trivia
 1:30 am | Family Guy | Secondhand Spoke
 2:00 am | Family Guy | Herpe the Love Sore
 2:30 am | American Dad | Deacon Stan, Jesus Man
 3:00 am | Rick and Morty | Auto Erotic Assimilation
 3:30 am | Eric Andre Show | The A$AP Ferg Show
 3:45 am | Eric Andre Show | Blannibal Quits
 4:00 am | 11 Minute Whambam 90's Ride | 11 Minute Whambam 90s Ride!
 4:15 am | Infomercials | Piggy
 4:30 am | Aqua Teen | Meatzone
 4:45 am | Aqua Teen | Super Trivia
 5:00 am | Bob's Burgers | The Unbearable Like-Likeness of Gene
 5:30 am | Bob's Burgers | Mother Daughter Laser Razor