# 2020-12-29
 6:00 am | Amazing World of Gumball | The Love
 6:15 am | Amazing World of Gumball | The Menu
 6:30 am | Amazing World of Gumball | The Awkwardness
 6:45 am | Amazing World of Gumball | The Uncle
 7:00 am | Amazing World of Gumball | The Nest
 7:15 am | Amazing World of Gumball | The Weirdo
 7:30 am | Amazing World of Gumball | The Points
 7:45 am | Amazing World of Gumball | The Heist
 8:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 9:00 am | Teen Titans Go! | TV Knight 6
 9:15 am | Teen Titans Go! | Brain Percentages
 9:30 am | Teen Titans Go! | Thumb War
 9:45 am | Teen Titans Go! | Toddler Titans...Yay!
10:00 am | Craig of the Creek | I Don't Need a Hat
10:15 am | Craig of the Creek | The Rise and Fall of the Green Poncho
10:30 am | Craig of the Creek | The Bike Thief
10:45 am | Craig of the Creek | Craig of the Beach
11:00 am | Monsters vs. Aliens | 
 1:00 pm | Amazing World of Gumball | The Vase
 1:15 pm | Amazing World of Gumball | The Pact
 1:30 pm | Amazing World of Gumball | The Matchmaker
 1:45 pm | Amazing World of Gumball | The Neighbor
 2:00 pm | Amazing World of Gumball | The Stories
 2:15 pm | Amazing World of Gumball | The List
 2:30 pm | Amazing World of Gumball | The News
 2:45 pm | Amazing World of Gumball | The Disaster
 3:00 pm | Amazing World of Gumball | The Re-Run
 3:15 pm | Amazing World of Gumball | The Rival
 3:30 pm | Amazing World of Gumball | The Guy
 3:45 pm | Amazing World of Gumball | The Lady
 4:00 pm | Craig of the Creek | I Don't Need a Hat
 4:15 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 4:30 pm | Craig of the Creek | Pencil Break Mania
 4:45 pm | Craig of the Creek | The Last Game of Summer
 5:00 pm | Total DramaRama | Sharing Is Caring
 5:15 pm | Total DramaRama | Simons Are Forever
 5:30 pm | Total DramaRama | Ant We All Just Get Along
 5:45 pm | Total DramaRama | Stop! Hamster Time
 6:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 6:30 pm | Teen Titans Go! | Waffles; Opposites
 7:00 pm | Amazing World of Gumball | The Fuss
 7:15 pm | Amazing World of Gumball | The Candidate
 7:30 pm | Amazing World of Gumball | The Outside
 7:45 pm | Amazing World of Gumball | The Anybody