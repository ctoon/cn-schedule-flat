# 2020-12-23
 6:00 am | Amazing World of Gumball | The Parking; The Copycats
 6:30 am | Amazing World Of Gumball | The Future; The Authority
 7:00 am | Amazing World of Gumball | The Upgrade; The Fuss
 7:30 am | Amazing World of Gumball | The Comic; The Outside
 8:00 am | Teen Titans Go! | Girls Night In
 8:30 am | Teen Titans Go! | The Viewers Decide; The Great Disaster
 9:00 am | Teen Titans Go! | Cartoon Feud; Curse Of The Booty Scooty
 9:30 am | Teen Titans Go! | Collect Them All; Butt Atoms
10:00 am | Craig of the Creek | Turning the Tables; Under the Overpass
10:30 am | Craig of the Creek | Kelsey the Author; The Invitation
11:00 am | Teen Titans Go! | Teen Titans Roar! Bucket List
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
12:00 pm | Teen Titans Go! | Titans vs. Santa Special
 1:00 pm | Amazing World of Gumball | The Nuisance; The Fury
 1:30 pm | Amazing World of Gumball | The Line; The Compilation
 2:00 pm | Amazing World of Gumball | The Grades; The Girlfriend
 2:30 pm | Amazing World of Gumball | The Advice; The Diet
 3:00 pm | Amazing World of Gumball | The Ex; The Signal
 3:30 pm | Amazing World of Gumball | The Sorcerer; The Parasite
 4:00 pm | Teen Titans Go! | Toddler Titans...Yay! Baby Mouth
 4:30 pm | Teen Titans Go! | The Superhero Feud Cast
 5:00 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 5:30 pm | Teen Titans Go! | Titans vs. Santa Special
 6:30 pm | Teen Titans Go! | Beast Boy On A Shelf; Christmas Crusaders
 7:00 pm | LEGO DC Shazam: Magic & Monsters | 