# 2020-12-23
 6:00 am | Amazing World of Gumball | The Parking
 6:15 am | Amazing World of Gumball | The Copycats
 6:30 am | Amazing World of Gumball | The Routine
 6:45 am | Amazing World of Gumball | Christmas
 7:00 am | Amazing World of Gumball | The Upgrade
 7:15 am | Amazing World of Gumball | The Fuss
 7:30 am | Amazing World of Gumball | The Comic
 7:45 am | Amazing World of Gumball | The Outside
 8:00 am | Teen Titans Go! | Girls Night In Part 1
 8:30 am | Teen Titans Go! | The Great Disaster
 8:45 am | Teen Titans Go! | The Viewers Decide
 9:00 am | Teen Titans Go! | Cartoon Feud
 9:15 am | Teen Titans Go! | Curse of the Booty Scooty
 9:30 am | Teen Titans Go! | Collect Them All
 9:45 am | Teen Titans Go! | Butt Atoms
10:00 am | Craig of the Creek | Turning the Tables
10:15 am | Craig of the Creek | Under the Overpass
10:30 am | Craig of the Creek | Kelsey the Author
10:45 am | Craig of the Creek | The Invitation
11:00 am | Teen Titans Go! | Teen Titans Roar!
11:15 am | Teen Titans Go! | Bucket List
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
12:00 pm | Teen Titans Go! | Teen Titans Go! Titans vs. Santa
 1:00 pm | Amazing World of Gumball | The Fury
 1:15 pm | Amazing World of Gumball | The Nuisance
 1:30 pm | Amazing World of Gumball | The Compilation
 1:45 pm | Amazing World of Gumball | The Line
 2:00 pm | Amazing World of Gumball | The Girlfriend
 2:15 pm | Amazing World of Gumball | The Grades
 2:30 pm | Amazing World of Gumball | The Advice
 2:45 pm | Amazing World of Gumball | The Diet
 3:00 pm | Amazing World of Gumball | The Signal
 3:15 pm | Amazing World of Gumball | The Ex
 3:30 pm | Amazing World of Gumball | The Parasite
 3:45 pm | Amazing World of Gumball | The Sorcerer
 4:00 pm | Teen Titans Go! | Toddler Titans...Yay!
 4:15 pm | Teen Titans Go! | Baby Mouth
 4:30 pm | Teen Titans Go! | Superhero Feud
 4:45 pm | Teen Titans Go! | The Cast
 5:00 pm | Teen Titans Go! | The Streak, Part 1
 5:15 pm | Teen Titans Go! | The Streak, Part 2
 5:30 pm | Teen Titans Go! | Teen Titans Go! Titans vs. Santa
 6:30 pm | Teen Titans Go! | Beast Boy on a Shelf
 6:45 pm | Teen Titans Go! | Christmas Crusaders
 7:00 pm | LEGO DC Shazam: Magic and Monsters | 
 8:00 pm | LEGO DC Shazam: Magic and Monsters | 