# 2020-12-23
 9:00 pm | American Dad | Dreaming of a White Porsche Christmas
 9:30 pm | American Dad | Ninety North, Zero West
10:00 pm | American Dad | Santa, Schmanta
10:30 pm | Family Guy | How the Griffin Stole Christmas
11:00 pm | Family Guy | Road to the North Pole Part 1 & 2
12:00 am | Rick and Morty | Anatomy Park
12:30 am | Robot Chicken | Freshly Baked: The Robot Chicken Santa Claus Pot Cookie Freakout Special: Special Edition
12:45 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 1:00 am | The Boondocks | A Huey Freeman Christmas
 1:30 am | Family Guy | Road to the North Pole Part 1 & 2
 2:30 am | Family Guy | How the Griffin Stole Christmas
 3:00 am | Rick and Morty | Anatomy Park
 3:30 am | Robot Chicken | Freshly Baked: The Robot Chicken Santa Claus Pot Cookie Freakout Special: Special Edition
 3:45 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 4:00 am | Infomercials | Danny Ketchup
 4:15 am | Infomercials | Rate the Cookie
 4:30 am | The Boondocks | A Huey Freeman Christmas
 5:00 am | American Dad | Dreaming of a White Porsche Christmas
 5:30 am | Home Movies | Breaking Up Is Hard to Do