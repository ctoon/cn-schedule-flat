# 2020-02-27
 6:00 am | Amazing World of Gumball | The Laziest; The Ghost
 6:30 am | Amazing World of Gumball | The Mystery; The Prank
 7:00 am | Amazing World of Gumball | The Gi; The Kiss
 7:30 am | Amazing World of Gumball | The Party; The Refund
 8:00 am | Teen Titans Go! | The Art of Ninjutsu
 8:15 am | Teen Titans Go! | Fish Water
 8:30 am | Teen Titans Go! | Think About Your Future
 8:45 am | Teen Titans Go! | TV Knight
 9:00 am | Teen Titans Go! | Booty Scooty
 9:15 am | Teen Titans Go! | BBSFBDAY
 9:30 am | Teen Titans Go! | Had to Be There
 9:45 am | Teen Titans Go! | Waffles
10:00 am | Craig of the Creek | The Shortcut
10:15 am | Craig of the Creek | Itch to Explore
10:30 am | Craig of the Creek | Deep Creek Salvage
10:45 am | Craig of the Creek | Jessica Goes to the Creek
11:00 am | Teen Titans Go! | Little Elvis
11:15 am | Teen Titans Go! | The Groover
11:30 am | Teen Titans Go! | The Power of Shrimps
11:45 am | Teen Titans Go! | My Name Is Jose
12:00 pm | Victor and Valentino | Go With the Flow
12:15 pm | Victor and Valentino | Forever Ever
12:30 pm | Victor and Valentino | Aluxes
12:45 pm | Victor and Valentino | Tree Buds
 1:00 pm | Amazing World of Gumball | The Mess
 1:15 pm | Amazing World of Gumball | The Return
 1:30 pm | Amazing World of Gumball | The Possession
 1:45 pm | Amazing World of Gumball | The Nemesis
 2:00 pm | Amazing World of Gumball | The Heart
 2:15 pm | Amazing World of Gumball | The Crew
 2:30 pm | Amazing World of Gumball | The Ghouls
 2:45 pm | Amazing World of Gumball | The Others
 3:00 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 3:15 pm | Teen Titans Go! | Oh Yeah!
 3:30 pm | Teen Titans Go! | How Bout Some Effort
 3:45 pm | Teen Titans Go! | Riding the Dragon
 4:00 pm | Teen Titans Go! | Bottle Episode
 4:15 pm | Teen Titans Go! | Coconut Cream Pie
 4:30 pm | Teen Titans Go! | Pyramid Scheme
 4:45 pm | Teen Titans Go! | Open Door Policy
 5:00 pm | Total DramaRama | Robo Teacher
 5:15 pm | Total DramaRama | Free Chili
 5:30 pm | Total DramaRama | Glove Glove Me Do
 5:45 pm | Total DramaRama | Cuttin' Corners
 6:00 pm | Teen Titans Go! | Shrimps and Prime Rib
 6:15 pm | Teen Titans Go! | The Great Disaster
 6:30 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 6:45 pm | Teen Titans Go! | The Viewers Decide
 7:00 pm | Amazing World of Gumball | The Inquisition
 7:15 pm | Amazing World of Gumball | The Pest
 7:30 pm | Amazing World of Gumball | The Third; The Debt