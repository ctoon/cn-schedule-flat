# 2020-02-16
 6:00 am | Teen Titans Go! | Curse of the Booty Scooty
 6:15 am | Teen Titans Go! | Tall Titan Tales
 6:30 am | Teen Titans Go! | The Chaff
 6:45 am | Teen Titans Go! | BBRBDAY
 7:00 am | Teen Titans | Car Trouble
 7:30 am | Teen Titans | How Long Is Forever?
 8:00 am | Teen Titans Go! | Beast Boy's That's What's Up
 9:00 am | Teen Titans: Trouble in Tokyo | 
10:30 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
10:45 am | Teen Titans Go! | My Name Is Jose
11:00 am | Teen Titans | Every Dog Has His Day
11:30 am | Teen Titans | Terra
12:00 pm | Teen Titans Go! | Business Ethics Wink Wink
12:15 pm | Teen Titans Go! | The Power of Shrimps
12:30 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
12:45 pm | Teen Titans Go! | The Groover
 1:00 pm | Teen Titans | Only Human
 1:30 pm | Teen Titans | Fear Itself
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 2:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 3:00 pm | Teen Titans | Date With Destiny
 3:30 pm | Teen Titans | Transformation
 4:00 pm | Teen Titans Go! | Little Elvis
 4:15 pm | Teen Titans Go! | Beast Girl
 4:30 pm | Teen Titans Go! | Genie President
 4:45 pm | Teen Titans Go! | Artful Dodgers
 5:00 pm | Teen Titans | Titan Rising
 5:30 pm | Teen Titans | Winner Take All
 6:00 pm | Teen Titans Go! | The Fight
 6:15 pm | Teen Titans Go! | TV Knight 3
 6:30 pm | Teen Titans Go! | Quantum Fun
 6:45 pm | Teen Titans Go! | The Scoop
 7:00 pm | Teen Titans | Betrayal
 7:30 pm | Teen Titans | Fractured