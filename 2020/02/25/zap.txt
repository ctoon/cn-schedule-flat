# 2020-02-25
 6:00 am | Amazing World of Gumball | The BFFS
 6:15 am | Amazing World of Gumball | The Signature
 6:30 am | Amazing World of Gumball | The Agent
 6:45 am | Amazing World of Gumball | The Gift
 7:00 am | Amazing World of Gumball | The Decisions
 7:15 am | Amazing World of Gumball | The Apprentice
 7:30 am | Amazing World of Gumball | The Web
 7:45 am | Amazing World of Gumball | The Check
 8:00 am | Teen Titans Go! | Finally a Lesson
 8:15 am | Teen Titans Go! | Baby Hands
 8:30 am | Teen Titans Go! | Arms Race With Legs
 8:45 am | Teen Titans Go! | Crazy Desire Island
 9:00 am | Teen Titans Go! | Obinray
 9:15 am | Teen Titans Go! | The Titans Show
 9:30 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 9:45 am | Teen Titans Go! | The Overbite
10:00 am | Craig of the Creek | The Takeout Mission
10:15 am | Craig of the Creek | Cousin of the Creek
10:30 am | Craig of the Creek | Jessica's Trail
10:45 am | Craig of the Creek | The Evolution of Craig
11:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
11:30 am | Teen Titans Go! | How's This for a Special? Spaaaace
12:00 pm | Victor and Valentino | Churro Kings
12:15 pm | Victor and Valentino | The Great Bongo Heist
12:30 pm | Victor and Valentino | Know It All
12:45 pm | Victor and Valentino | Cat-Pocalypse
 1:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 1:30 pm | Amazing World of Gumball | The Mystery; The Prank
 2:00 pm | Amazing World of Gumball | The Gi; The Kiss
 2:30 pm | Amazing World of Gumball | The Party; The Refund
 3:00 pm | Teen Titans Go! | The Art of Ninjutsu
 3:15 pm | Teen Titans Go! | Fish Water
 3:30 pm | Teen Titans Go! | Think About Your Future
 3:45 pm | Teen Titans Go! | TV Knight
 4:00 pm | Teen Titans Go! | Booty Scooty
 4:15 pm | Teen Titans Go! | BBSFBDAY
 4:30 pm | Teen Titans Go! | Gorilla
 4:45 pm | Teen Titans Go! | Love Monsters
 5:00 pm | Total DramaRama | Gnome More Mister Nice Guy
 5:15 pm | Total DramaRama | Duck Duck Juice
 5:30 pm | Total DramaRama | Look Who's Clocking
 5:45 pm | Total DramaRama | Cluckwork Orange
 6:00 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:15 pm | Teen Titans Go! | Oh Yeah!
 6:30 pm | Teen Titans Go! | How Bout Some Effort
 6:45 pm | Teen Titans Go! | Riding the Dragon
 7:00 pm | Amazing World of Gumball | The Mess
 7:15 pm | Amazing World of Gumball | The Return
 7:30 pm | Amazing World of Gumball | The Possession
 7:45 pm | Amazing World of Gumball | The Nemesis