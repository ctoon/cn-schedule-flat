# 2020-02-10
 6:00 am | Amazing World Of Gumball | The Points; The Sidekick
 6:30 am | Amazing World Of Gumball | The Awkwardness; The Bus
 7:00 am | Amazing World of Gumball | The Nest; The Night
 7:30 am | Amazing World Of Gumball | The Misunderstandings; The Car
 8:00 am | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 8:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:00 am | Teen Titans Go! | Waffles; Opposites
 9:30 am | Teen Titans Go! | Birds; Be Mine
10:00 am | Craig Of The Creek | The Future Is Cardboard; Bug City
10:30 am | Craig Of The Creek | Dog Decider; The Shortcut
11:00 am | Teen Titans Go! | Booby Trap House; Fish Water
11:30 am | Teen Titans Go! | TV Knight; BBSFBDAY!
12:00 pm | Victor And Valentino | Cat-pocalypse; Know It All
12:30 pm | Victor And Valentino | Fistfull Of Balloons; Band For Life
 1:00 pm | Amazing World Of Gumball | The Choices; The Potato
 1:30 pm | Amazing World Of Gumball | The Code; The Sorcerer
 2:00 pm | Amazing World Of Gumball | The Test; The Console
 2:30 pm | Amazing World Of Gumball | The Slide; The Outside
 3:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 3:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 4:00 pm | Teen Titans Go! | Pirates; I See You
 4:30 pm | Teen Titans Go! | Brian; Nature
 5:00 pm | Total DramaRama | Cone In 60 Seconds; Paint That A Shame
 5:30 pm | Total DramaRama | The Bad Guy Busters; Snots Landing
 6:00 pm | Teen Titans Go! | Friendship; Vegetables
 6:30 pm | Teen Titans Go! | The Mask; Slumber Party
 7:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 7:30 pm | Amazing World of Gumball | The Copycats; The Loophole