# 2020-02-14
 6:15 am | Amazing World of Gumball | The Potato
 6:30 am | Amazing World of Gumball | The Choices
 6:44 am | Amazing World of Gumball | The Love
 6:59 am | SPECIAL | The Amazing Dates of Gumball & Penny
 7:59 am | Teen Titans Go! | How 'Bout Some Effort
 8:14 am | Teen Titans Go! | The Power of Shrimps
 8:29 am | Teen Titans Go! | BBRAE
 8:59 am | MOVIE | Lego DC Batman: Family Matters
10:50 am | MOVIE | What's Opera, Titans?
11:00 am | Teen Titans Go! | Lication
11:15 am | Teen Titans Go! | Ones and Zeros
11:30 am | Teen Titans Go! | Career Day
11:44 am | Teen Titans Go! | TV Knight 2
12:00 pm | Victor and Valentino | Escape From Bebe Bay
12:15 pm | Victor and Valentino | Folk Art Foes
12:30 pm | Victor and Valentino | Lonely Haunts Club
12:44 pm | Victor and Valentino | Lonely Haunts Club 2: Doll Island
12:59 pm | Amazing World of Gumball | The Stories
 1:15 pm | Amazing World of Gumball | The Fuss
 1:30 pm | Amazing World of Gumball | The Choices
 1:45 pm | Amazing World of Gumball | The Love
 2:00 pm | SPECIAL | The Amazing Dates of Gumball & Penny
 2:59 pm | SPECIAL | Teen Titans Go! in Love: Beast Boy & Raven
 4:35 pm | SPECIAL | The Great Disaster
 4:45 pm | Teen Titans Go! | The Power of Shrimps
 5:00 pm | Total DramaRama | Inglorious Toddlers
 5:15 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:30 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:44 pm | Total DramaRama | Gum and Gummer
 6:00 pm | Teen Titans Go! | Salty Codgers/Knowledge
 6:30 pm | Teen Titans Go! | How 'Bout Some Effort
 6:44 pm | Teen Titans Go! | Be Mine
 7:00 pm | Steven Universe Future | Little Homeschool
 7:15 pm | Steven Universe Future | Guidance
 7:30 pm | Steven Universe Future | Rose Buds
 7:45 pm | Steven Universe Future | Volleyball