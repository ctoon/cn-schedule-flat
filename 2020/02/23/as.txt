# 2020-02-23
 8:00 pm | Bob's Burgers | Aquaticism
 8:30 pm | American Dad | Demolition Daddy
 9:00 pm | American Dad | Pride Before the Fail
 9:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel
10:00 pm | Bob's Burgers | Ex Machtina
10:30 pm | Family Guy | The Finer Strings
11:00 pm | Family Guy | High School English
11:30 pm | Rick and Morty | One Crew Over The Crewcoo's Morty
12:00 am | Ballmastrz: 9009 | Infinite Hugs: Cold Embrace of the Bloodless Progenitors!
12:15 am | Ballmastrz: 9009 | Shameful Disease of Yackety Yack! Don't Talk Back! Be Silenced Forever!
12:30 am | Tigtone | Tigtone and the Wine Crisis
12:45 am | The Shivering Truth | Ogled Inklings
 1:00 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 1:15 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 1:30 am | American Dad | Demolition Daddy
 2:00 am | Family Guy | The Finer Strings
 2:30 am | Family Guy | High School English
 3:00 am | Rick and Morty | One Crew Over The Crewcoo's Morty
 3:30 am | Ballmastrz: 9009 | Infinite Hugs: Cold Embrace of the Bloodless Progenitors!
 3:45 am | Ballmastrz: 9009 | Shameful Disease of Yackety Yack! Don't Talk Back! Be Silenced Forever!
 4:00 am | Tigtone | Tigtone and the Wine Crisis
 4:15 am | The Shivering Truth | Ogled Inklings
 4:30 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 4:45 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 5:00 am | American Dad | Pride Before the Fail
 5:30 am | Harvey Birdman | Shoyu Weenie
 5:45 am | Harvey Birdman | The Dabba Don