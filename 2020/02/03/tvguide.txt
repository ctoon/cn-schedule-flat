# 2020-02-03
 6:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 am | Amazing World of Gumball | The Mirror; The Burden
 7:00 am | Amazing World of Gumball | The Bros; The Man
 7:30 am | Amazing World of Gumball | The Pizza; The Lie
 8:00 am | Teen Titans Go! | Double Trouble; The Date
 8:30 am | Teen Titans Go! | Dude Relax!; Laundry Day
 9:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 9:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
10:00 am | Craig of the Creek | The Climb; You're It
10:30 am | Craig Of The Creek | Itch To Explore; 5 Big Pinchy
11:00 am | Amazing World of Gumball | The Nobody; The Countdown
11:30 am | Amazing World of Gumball | The Egg; The Downer
12:00 pm | Victor And Valentino | Welcome to the Underworld
12:30 pm | Victor And Valentino | Dead Ringer; The Dark Room
 1:00 pm | Amazing World of Gumball | The Gift; The Comic
 1:30 pm | Amazing World of Gumball | The Apprentice; The Romantic
 2:00 pm | Amazing World of Gumball | The Check; The Uploads
 2:30 pm | Amazing World of Gumball | The Pest; The Wicked
 3:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 3:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 4:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 4:30 pm | Teen Titans Go! | Books; Lazy Sunday
 5:00 pm | Total DramaRama | Driving Miss Crazy; Venthalla
 5:30 pm | Total DramaRama | Weiner Takes All; The Date
 6:00 pm | Teen Titans Go! | Secret Garden; Garage Sale
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:00 pm | Amazing World of Gumball | The Triangle; The Money
 7:30 pm | Amazing World of Gumball | The Return; The Hug