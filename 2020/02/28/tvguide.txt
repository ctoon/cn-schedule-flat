# 2020-02-28
 6:00 am | Amazing World of Gumball | The Robot; The Picnic
 6:30 am | Amazing World of Gumball | The Goons; The Secret
 7:00 am | Amazing World of Gumball | The Sock; The Genius
 7:30 am | Amazing World of Gumball | The Mustache; The Date
 8:00 am | Teen Titans Go! | Inner Beauty Of A Cactus; Birds
 8:30 am | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 9:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 9:30 am | Teen Titans Go! | Snuggle Time; Movie Night
10:00 am | Craig Of The Creek | Dibs Court; Too Many Treasures
10:30 am | Craig Of The Creek | The Great Fossil Rush; The Final Book
11:00 am | Teen Titans Go! | BBRBDAY;The Real Orangins
11:30 am | Teen Titans Go! | Tall Titan Tales; Nostalgia Is Not A Substitute For An Actual Story
12:00 pm | Victor And Valentino | Balloon Boys; On Nagual Hill
12:30 pm | Victor And Valentino | Lonely Haunts Club 2: Doll Island; Dance Reynaldo Dance
 1:00 pm | Amazing World Of Gumball | The Bffs; The Signature
 1:30 pm | Amazing World Of Gumball | The Agent; The Gift
 2:00 pm | Amazing World Of Gumball | The Decisions; The Apprentice
 2:30 pm | Amazing World Of Gumball | The Web; The Check
 3:00 pm | Teen Titans Go! | Finally A Lesson; Pure Protein
 3:30 pm | Teen Titans Go! | Arms Race With Legs; Crazy Desire Island
 4:00 pm | Teen Titans Go! | Obinray; The Titans Show
 4:30 pm | Teen Titans Go! | Batman Vs. Teen Titans: Dark Injustice; The Overbite
 5:00 pm | Total DramaRama | The Tooth About Zombies; Sharing Is Caring
 5:30 pm | Total DramaRama | Cone In 60 Seconds; Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Think About Your Future; Tv Knight
 6:30 pm | Teen Titans Go! | Booty Scooty; Bbsfbday!
 7:00 pm | Steven Universe Future | Bluebird; A Very Special Episode
 7:30 pm | Steven Universe Future | Snow Day; Why So Blue?