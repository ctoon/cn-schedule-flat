# 2020-02-05
 6:00 am | Amazing World of Gumball | The Countdown; The Nobody
 6:30 am | Amazing World of Gumball | The Downer; The Egg
 7:00 am | Amazing World of Gumball | The Triangle; The Money
 7:30 am | Amazing World of Gumball | The Return
 7:45 am | Amazing World of Gumball | The Hug
 8:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 8:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
 9:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 9:30 am | Teen Titans Go! | Books; Lazy Sunday
10:00 am | Craig of the Creek | The Final Book
10:15 am | Craig of the Creek | Creek Cart Racers
10:30 am | Craig of the Creek | Wildernessa
10:45 am | Craig of the Creek | Secret Book Club
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:30 am | Teen Titans Go! | Booty Scooty
11:45 am | Teen Titans Go! | Who's Laughing Now
12:00 pm | Victor and Valentino | Legend of the Hidden Skate Park
12:15 pm | Victor and Valentino | Los Cadejos
12:30 pm | Victor and Valentino | Cleaning Day
12:45 pm | Victor and Valentino | Cuddle Monster
 1:00 pm | Amazing World of Gumball | The Butterfly; The Question
 1:30 pm | Amazing World of Gumball | The Oracle; The Safety
 2:00 pm | Amazing World of Gumball | The Friend; The Saint
 2:30 pm | Amazing World of Gumball | The Society; The Spoiler
 3:00 pm | Teen Titans Go! | Secret Garden
 3:15 pm | Teen Titans Go! | Garage Sale
 3:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 4:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 5:00 pm | Total DramaRama | Look Who's Clocking
 5:15 pm | Total DramaRama | Germ Factory
 5:30 pm | Total DramaRama | Harold Swatter and the Goblet of Flies
 5:45 pm | Total DramaRama | Aquarium for a Dream
 6:00 pm | Teen Titans Go! | Girls' Night Out; You're Fired
 6:30 pm | Teen Titans Go! | Super Robin; Tower Power
 7:00 pm | Amazing World of Gumball | The Check
 7:15 pm | Amazing World of Gumball | The Uploads
 7:30 pm | Amazing World of Gumball | The Pest
 7:45 pm | Amazing World of Gumball | The Wicked