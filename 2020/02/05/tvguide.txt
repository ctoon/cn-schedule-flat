# 2020-02-05
 6:00 am | Amazing World of Gumball | The Nobody; The Countdown
 6:30 am | Amazing World of Gumball | The Egg; The Downer
 7:00 am | Amazing World of Gumball | The Triangle; The Money
 7:30 am | Amazing World of Gumball | The Return; The Hug
 8:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 8:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
 9:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 9:30 am | Teen Titans Go! | Books; Lazy Sunday
10:00 am | Craig Of The Creek | The Final Book; Creek Cart Racers
10:30 am | Craig of the Creek | Wildernessa; Secret Book Club
11:00 am | Cartoon Network Programming | 
11:30 am | Teen Titans Go! | Who's Laughing Now; Booty Scooty
12:00 pm | Victor And Valentino | Los Cadejos; Legend Of The Hidden Skate Park
12:30 pm | Victor And Valentino | Cuddle Monster; Cleaning Day
 1:00 pm | Amazing World of Gumball | The Butterfly; The Question
 1:30 pm | Amazing World of Gumball | The Oracle; The Safety
 2:00 pm | Amazing World of Gumball | The Friend; The Saint
 2:30 pm | Amazing World of Gumball | The Spoiler; The Society
 3:00 pm | Teen Titans Go! | Secret Garden; Garage Sale
 3:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 4:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 5:00 pm | Total DramaRama | Look Who's Clocking; Germ Factory
 5:30 pm | Total DramaRama | Harold Swatter And The Goblet Of Flies; Aquarium For A Dream
 6:00 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 6:30 pm | Teen Titans Go! | Super Robin; Tower Power
 7:00 pm | Amazing World of Gumball | The Check; The Uploads
 7:30 pm | Amazing World of Gumball | The Pest; The Wicked