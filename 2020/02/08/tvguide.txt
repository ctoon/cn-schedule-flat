# 2020-02-08
 6:00 am | Amazing World Of Gumball | The Transformation; The Return
 6:30 am | Amazing World Of Gumball | The Understanding; The Nemesis
 7:00 am | Amazing World Of Gumball | The Mess; The Possession
 7:30 am | Amazing World Of Gumball | The Heart; The Ghouls
 8:00 am | Teen Titans Go! | Master Detective; The Overbite
 8:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
 9:00 am | Total DramaRama | Egg-stremely Bad Idea, An
 9:15 am | Total DramaRama | Exercising The Demons
 9:30 am | Apple & Onion | Appleoni
 9:45 am | Apple & Onion | Falafel's In Jail
10:00 am | Teen Titans Go! | Shrimps And Prime Rib; Easter Creeps
10:30 am | Teen Titans Go! | The Avogodo; Hot Salad Water
11:00 am | Teen Titans Go! | Hand Zombie; Booby Trap House
11:30 am | Teen Titans Go! | Employee Of The Month Redux; Fish Water
12:00 pm | Teen Titans Go! | Orangins; Tv Knight
12:30 pm | Teen Titans Go! | BBSFBDAY!; Jinxed
 1:00 pm | Amazing World Of Gumball | The Ad; The Crew
 1:30 pm | Amazing World Of Gumball | The Slip; The Others
 2:00 pm | Amazing World Of Gumball | The Drama; The Signature
 2:30 pm | Amazing World Of Gumball | The Buddy; The Gift
 3:00 pm | Teen Titans Go! | TV Knight 2; Career Day
 3:30 pm | Teen Titans Go! | Throne of Bones; The Academy
 4:00 pm | Craig of the Creek | Big Pinchy; The Climb
 4:30 pm | Victor And Valentino | Balloon Boys; Guillermo's Gathering
 5:00 pm | Total DramaRama | An Egg-stremely Bad Idea; Exercising The Demons
 5:30 pm | Total DramaRama | Camping Is In Tents
 6:00 pm | Amazing World Of Gumball | The Future; The Authority
 6:30 pm | Amazing World Of Gumball | The Wish; The Hug
 7:00 pm | Amazing World Of Gumball | The Bffs; The Agent
 7:30 pm | Apple & Onion | Baby Boi Tp; Not Funny