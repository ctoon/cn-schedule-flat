# 2020-02-29
 6:00 am | Amazing World of Gumball | The Knights; The Colossus
 6:30 am | Amazing World of Gumball | The Fridge; The Remote
 7:00 am | Amazing World of Gumball | The News; The Vase
 7:30 am | Amazing World of Gumball | The Ollie; The Potato
 8:00 am | Teen Titans Go! | Pure Protein; The Art Of Ninjutsu
 8:30 am | Teen Titans Go! | Open Door Policy; History Lesson
 9:00 am | Total DramaRama | The Tooth About Zombies; Glove Glove Me Do
 9:30 am | Apple & Onion | The Music Store Thief
 9:45 am | Apple & Onion | Falafel's In Jail
10:00 am | Teen Titans Go! | Walk Away
10:15 am | Teen Titans Go! | We're Off To Get Awards
10:30 am | ThunderCats Roar! | The Legend of Boggy Ben
10:45 am | ThunderCats Roar! | Prank Call
11:00 am | Teen Titans Go! | Coconut Cream Pie; Rad Dudes With Bad Tudes
11:30 am | Teen Titans Go! | Riding The Dragon; Wally T
12:00 pm | Teen Titans Go! | Oh Yeah!; Obinray
12:30 pm | Teen Titans Go! | Snuggle Time; Arms Race With Legs
 1:00 pm | Amazing World of Gumball | The Flower; The Banana
 1:30 pm | Amazing World of Gumball | The Phone; The Job
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Words; The Apology
 3:00 pm | Teen Titans Go! | Bat Scouts; Business Ethics Wink Wink
 3:30 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; I Used To Be A Peoples
 4:00 pm | Craig of the Creek | Creek Cart Racers; Secret Book Club
 4:30 pm | Victor And Valentino | Cat-pocalypse; Band For Life
 5:00 pm | Total DramaRama | Weiner Takes All; Apoca-lice Now
 5:30 pm | Total DramaRama | Stop! Hamster Time; Driving Miss Crazy
 6:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 6:30 pm | Amazing World of Gumball | The Authority; The Virus
 7:00 pm | Infinity Train | The Black Market Car; The Family Tree Car
 7:30 pm | Infinity Train | The Map Car; The Toad Car