# 2020-02-29
 8:00 pm | My Hero Academia | Lemillion
 8:30 pm | My Hero Academia | Unforeseen Hope
 9:00 pm | American Dad | Stan Knows Best
 9:30 pm | Rick and Morty | Rick Potion #9
10:00 pm | The Boondocks | The Garden Party
10:30 pm | Family Guy | Wasted Talent
11:00 pm | Family Guy | Fore Father
11:30 pm | My Hero Academia | Bright Future
12:00 am | Sword Art Online Alicization War of Underworld | Stigma of the Disqualified
12:30 am | Demon Slayer: Kimetsu no Yaiba | A Forged Bond
 1:00 am | Food Wars! | Beasts That Devour Each Other
 1:30 am | Black Clover | The Battle for Clover Castle
 2:00 am | JoJo's Bizarre Adventure: Golden Wind | Thankful Death Part 2
 2:30 am | Naruto:Shippuden | Things You Can't Get Back
 3:00 am | Genndy Tartakovksy's Primal | Spear and Fang
 3:30 am | Samurai Jack | LII
 4:00 am | The Boondocks | The Garden Party
 4:30 am | The Boondocks | Guess Hoe's Coming to Dinner
 5:00 am | Aqua Something You Know Whatever | Chicken and Beans
 5:15 am | Aqua Something You Know Whatever | Zucotti Manicotti
 5:30 am | Space Ghost Coast To Coast | Spanish Translation
 5:45 am | Space Ghost Coast To Coast | Gilligan