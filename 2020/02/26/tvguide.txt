# 2020-02-26
 6:00 am | Amazing World Of Gumball | The Inquisition; The Pest
 6:30 am | Amazing World of Gumball | The Third; The Debt
 7:00 am | Amazing World of Gumball | The Pressure; The Painting
 7:30 am | Amazing World of Gumball | The Responsible; The Dress
 8:00 am | Teen Titans Go! | Wally T; Permanent Record
 8:30 am | Teen Titans Go! | Shrimps And Prime Rib; I'm The Sauce
 9:00 am | Teen Titans Go! | Accept The Next Proposition You Hear; Beast Man
 9:30 am | Teen Titans Go! | History Lesson; Booby Trap House
10:00 am | Craig Of The Creek | Dinner At The Creek; The Haunted Dollhouse
10:30 am | Craig Of The Creek | Bug City; You're It
11:00 am | Teen Titans Go! | Tower Renovation; Quantum Fun
11:30 am | Teen Titans Go! | Genie President; The Fight
12:00 pm | Victor And Valentino | Fistfull Of Balloons; Band For Life
12:30 pm | Victor And Valentino | Love At First Bite; Tez Says
 1:00 pm | Amazing World of Gumball | The Robot; The Picnic
 1:30 pm | Amazing World of Gumball | The Goons; The Secret
 2:00 pm | Amazing World of Gumball | The Sock; The Genius
 2:30 pm | Amazing World of Gumball | The Mustache; The Date
 3:00 pm | Teen Titans Go! | Inner Beauty Of A Cactus; Knowledge
 3:30 pm | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 4:00 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 4:30 pm | Teen Titans Go! | Snuggle Time; Movie Night
 5:00 pm | Total DramaRama | Harold Swatter And The Goblet Of Flies; Germ Factory
 5:30 pm | Total DramaRama | Stink. Stank. Stunk; Aquarium For A Dream
 6:00 pm | Teen Titans Go! | Finally A Lesson; Pure Protein
 6:30 pm | Teen Titans Go! | Arms Race With Legs; Crazy Desire Island
 7:00 pm | Amazing World Of Gumball | The Bffs; The Signature
 7:30 pm | Amazing World Of Gumball | The Agent; The Gift