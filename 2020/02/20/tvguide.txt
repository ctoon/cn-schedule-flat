# 2020-02-20
 6:00 am | Amazing World of Gumball | The Weirdo; The Cringe
 6:30 am | Amazing World of Gumball | The Menu; The Neighbor
 7:00 am | Amazing World of Gumball | The Pact; The Uncle
 7:30 am | Amazing World Of Gumball | The Heist; The Faith
 8:00 am | Teen Titans Go! | Video Game References; Cool School
 8:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 9:00 am | Teen Titans Go! | Operation Tin Man; Nean
 9:30 am | Teen Titans Go! | The Croissant; The Dignity of Teeth
10:00 am | Craig Of The Creek | Big Pinchy; Sparkle Cadet
10:30 am | Craig Of The Creek | The Kid From 3030; Stink Bomb
11:00 am | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
11:30 am | Teen Titans Go! | Mo' Money Mo' Problems; Beast Girl
12:00 pm | Victor And Valentino | Cuddle Monster; The Babysitter
12:30 pm | Victor And Valentino | Boss For A Day; Hurricane Chata
 1:00 pm | Amazing World Of Gumball | The Catfish; The Puppets
 1:30 pm | Amazing World Of Gumball | The Cycle; The Lady
 2:00 pm | Amazing World Of Gumball | The Stars; The Sucker
 2:30 pm | Amazing World Of Gumball | The Cage;The Founder
 3:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 3:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 4:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 4:30 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 5:00 pm | Total DramaRama | Too Much Of A Goo'd Thing; Duncan Disorderly
 5:30 pm | Total DramaRama | The Price Of Advice; Stop! Hamster Time
 6:00 pm | Teen Titans Go! | Tamaranian Vacation; Let's Get Serious
 6:30 pm | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
 7:00 pm | Teen Titans | Divide and Conquer
 7:30 pm | Teen Titans | Forces of Nature