# 2020-09-21
 6:00 am | Amazing World of Gumball | The Romantic; The Signature
 6:30 am | Amazing World of Gumball | The Uploads; The Gift
 7:00 am | Amazing World of Gumball | The Wicked; The Apprentice
 7:30 am | Amazing World of Gumball | The Check; The Traitor
 8:00 am | Apple & Onion | Apple's Focus; River of Gold
 8:30 am | Teen Titans Go! | Island Adventures
 9:30 am | Teen Titans Go! | Fish Water Movie Night
10:00 am | Craig of the Creek | The Children of the Dog; Sunday Clothes
10:30 am | Craig of the Creek | Jessica Shorts; Escape From Family Dinner
11:00 am | Teen Titans Go! | TV Knight Permanent Record
11:30 am | Teen Titans Go! | Hand Zombie; Jinxed
12:00 pm | Teen Titans Go! | Employee of the Month Redux; Brain Percentages
12:30 pm | Mao Mao, Heroes of Pure Heart | I Love You Mao Mao; Legend of Torbaclaun
 1:00 pm | Amazing World of Gumball | The Nest; The Compilation
 1:30 pm | Amazing World of Gumball | The Points; The Slide
 2:00 pm | Amazing World of Gumball | The Bus; The Loophole
 2:30 pm | Amazing World of Gumball | The Fuss; The Night
 3:00 pm | We Bare Bears | Tote Life; Grizz Helps
 3:30 pm | We Bare Bears | Charlie and the Snake; Christmas Parties
 4:00 pm | Craig of the Creek | Beyond the Rapids Jessica Goes to the Creek
 4:30 pm | Craig of the Creek | Crisis at Elder Rock Too Many Treasures
 5:00 pm | Total DramaRama | Invasion of the Booger Snatchers Supply Mom
 5:30 pm | Total DramaRama | Wristy Business Mooshy Mon Mons
 6:00 pm | Teen Titans Go! | The Avogodo; Bl4z3
 6:30 pm | Teen Titans Go! | Orangins; Hot Salad Water
 7:00 pm | Amazing World of Gumball | The Fury; The Sorcerer
 7:30 pm | Amazing World of Gumball | The Disaster; The Re-run