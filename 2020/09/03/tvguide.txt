# 2020-09-03
 6:00 am | Amazing World of Gumball | The Responsible; The Dress
 6:30 am | Amazing World of Gumball | The Laziest; The Ghost
 7:00 am | Amazing World of Gumball | The Mystery; The Prank
 7:30 am | Amazing World of Gumball | The GI; The Kiss
 8:00 am | Mao Mao, Heroes of Pure Heart | Enemy Mime Flyaway
 8:30 am | Teen Titans Go! | Little Buddies; Missing
 9:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 9:30 am | Teen Titans Go! | Dreams; Grandma Voice
10:00 am | Craig of the Creek | The Return of the Honeysuckle Rangers; Children of the Dog
10:30 am | Craig of the Creek | Fort Williams; Jessica Shorts
11:00 am | Teen Titans Go! | Mr. Butt; Man Person
11:30 am | Teen Titans Go! | Terra-ized; Artful Dodgers
12:00 pm | Teen Titans Go! | Burger vs. Burrito; Matched
12:30 pm | Mao Mao, Heroes of Pure Heart | Ultraclops; Sleeper Sofa
 1:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 1:30 pm | Amazing World of Gumball | The Helmet; The Fight
 2:00 pm | Amazing World of Gumball | The End; The DVD
 2:30 pm | Amazing World of Gumball | The Knights; The Colossus
 3:00 pm | Craig of the Creek | The Plush Kingdom Ice Pop Trio
 3:30 pm | Craig of the Creek | The Pencil Break Mania Last Game of Summer
 4:00 pm | Total DramaRama | Duncan Disorderly; Ant We All Just Get Along
 4:30 pm | Total DramaRama | Soother or Later; Cone in 60 Seconds
 5:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 5:30 pm | Teen Titans Go! | Books; Lazy Sunday
 6:00 pm | Apple & Onion | Onionless; Patty's Law
 6:30 pm | Amazing World of Gumball | The Mustache; The Date
 7:00 pm | Amazing World of Gumball | The Club; The Wand
 7:30 pm | We Bare Bears | The Fire!; Road