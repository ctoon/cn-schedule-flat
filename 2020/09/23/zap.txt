# 2020-09-23
 6:00 am | Amazing World of Gumball | The Nest
 6:15 am | Amazing World of Gumball | The Compilation
 6:30 am | Amazing World of Gumball | The Points
 6:45 am | Amazing World of Gumball | The Slide
 7:00 am | Amazing World of Gumball | The Bus
 7:15 am | Amazing World of Gumball | The Loophole
 7:30 am | Amazing World of Gumball | The Night
 7:45 am | Amazing World of Gumball | The Fuss
 8:00 am | Apple & Onion | Fun Proof
 8:15 am | Apple & Onion | The Music Store Thief
 8:30 am | Teen Titans Go! | Hand Zombie
 8:45 am | Teen Titans Go! | Jinxed
 9:00 am | Teen Titans Go! | Employee of the Month Redux
 9:15 am | Teen Titans Go! | Brain Percentages
 9:30 am | Teen Titans Go! | The Avogodo
 9:45 am | Teen Titans Go! | BL4Z3
10:00 am | Craig of the Creek | The Jinxening
10:15 am | Craig of the Creek | The Future Is Cardboard
10:30 am | Craig of the Creek | In the Key of the Creek
10:45 am | Craig of the Creek | Dog Decider
11:00 am | Teen Titans Go! | Orangins
11:15 am | Teen Titans Go! | Hot Salad Water
11:30 am | Teen Titans Go! | TV Knight 2
11:45 am | Teen Titans Go! | Brain Percentages
12:00 pm | Teen Titans Go! | Lication
12:15 pm | Teen Titans Go! | The Academy
12:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 1:00 pm | Amazing World of Gumball | The Fury
 1:15 pm | Amazing World of Gumball | The Sorcerer
 1:30 pm | Amazing World of Gumball | The Disaster
 1:45 pm | Amazing World of Gumball | The Re-Run
 2:00 pm | Amazing World of Gumball | The Stories
 2:15 pm | Amazing World of Gumball | The Menu
 2:30 pm | Amazing World of Gumball | The Friend
 2:45 pm | Amazing World of Gumball | The Uncle
 3:00 pm | We Bare Bears | Chloe and Ice Bear
 3:15 pm | We Bare Bears | Anger Management
 3:30 pm | We Bare Bears | Cupcake Job
 3:45 pm | We Bare Bears | Panda's Friend
 4:00 pm | Craig of the Creek | The Children of the Dog
 4:15 pm | Craig of the Creek | Sunday Clothes
 4:30 pm | Craig of the Creek | Jessica Shorts
 4:45 pm | Craig of the Creek | Escape From Family Dinner
 5:00 pm | Total DramaRama | There Are No Hoppy Endings
 5:15 pm | Total DramaRama | Pinata Regatta
 5:30 pm | Total DramaRama | Too Much of a Goo'd Thing
 5:45 pm | Total DramaRama | A Dame-gerous Game
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | Amazing World of Gumball | The Pest
 7:15 pm | Amazing World of Gumball | The Advice
 7:30 pm | Amazing World of Gumball | The Hug
 7:45 pm | Amazing World of Gumball | The Signal