# 2020-09-01
 8:00 pm | Bob's Burgers | Slumber Party
 8:30 pm | Bob's Burgers | Presto Tina-O
 9:00 pm | American Dad | The Kidney Stays in the Picture
 9:30 pm | American Dad | Ricky Spanish
10:00 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
10:30 pm | Rick and Morty | Ricksy Business
11:00 pm | Family Guy | Mother Tucker
11:30 pm | Family Guy | Hell Comes to Quahog
12:00 am | Robot Chicken | Molly Lucero in: Your Friend's Boob
12:15 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
12:30 am | Aqua Teen | Gene E
12:45 am | Aqua Teen | She Creature
 1:00 am | Tender Touches | Microecosystems
 1:15 am | Tender Touches | Rock and Roll Guys
 1:30 am | Family Guy | Mother Tucker
 2:00 am | Family Guy | Hell Comes to Quahog
 2:30 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 3:00 am | Rick and Morty | Ricksy Business
 3:30 am | Robot Chicken | Molly Lucero in: Your Friend's Boob
 3:45 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
 4:00 am | Hot Package | Fabio's Photo
 4:15 am | Mostly 4 Millennials | Feelings & Emotions
 4:30 am | Aqua Teen | Gene E
 4:45 am | Aqua Teen | She Creature
 5:00 am | Bob's Burgers | Slumber Party
 5:30 am | Bob's Burgers | Presto Tina-O