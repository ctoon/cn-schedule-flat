# 2020-09-24
 6:00 am | Amazing World of Gumball | The Misunderstandings
 6:15 am | Amazing World of Gumball | The News
 6:30 am | Amazing World of Gumball | The Roots
 6:45 am | Amazing World of Gumball | The Vase
 7:00 am | Amazing World of Gumball | The Blame
 7:15 am | Amazing World of Gumball | The Ollie
 7:30 am | Amazing World of Gumball | The Detective
 7:45 am | Amazing World of Gumball | The Outside
 8:00 am | Apple & Onion | Party Popper
 8:15 am | Apple & Onion | The Fly
 8:30 am | Teen Titans Go! | The Night Begins to Shine Special
 9:30 am | Teen Titans Go! | Hot Salad Water
 9:45 am | Teen Titans Go! | Ones and Zeroes
10:00 am | Craig of the Creek | Beyond the Rapids
10:15 am | Craig of the Creek | Jessica Goes to the Creek
10:30 am | Craig of the Creek | Crisis at Elder Rock
10:45 am | Craig of the Creek | Too Many Treasures
11:00 am | Teen Titans Go! | Brain Percentages
11:15 am | Teen Titans Go! | Career Day
11:30 am | Teen Titans Go! | Island Adventures
12:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 1:00 pm | Amazing World of Gumball | The Signature
 1:15 pm | Amazing World of Gumball | The Romantic
 1:30 pm | Amazing World of Gumball | The Gift
 1:45 pm | Amazing World of Gumball | The Uploads
 2:00 pm | Amazing World of Gumball | The Apprentice
 2:15 pm | Amazing World of Gumball | The Wicked
 2:30 pm | Amazing World of Gumball | The Check
 2:45 pm | Amazing World of Gumball | The Traitor
 3:00 pm | We Bare Bears | Hibernation
 3:15 pm | We Bare Bears | $100
 3:30 pm | We Bare Bears | Neighbors
 3:45 pm | We Bare Bears | Road Trip
 4:00 pm | Craig of the Creek | Ferret Quest
 4:15 pm | Craig of the Creek | Monster in the Garden
 4:30 pm | Craig of the Creek | Into the Overpast
 4:45 pm | Craig of the Creek | The Curse
 5:00 pm | Total DramaRama | The Price of Advice
 5:15 pm | Total DramaRama | Royal Flush
 5:30 pm | Total DramaRama | Mother of All Cards
 5:45 pm | Total DramaRama | Total Eclipse of the Fart
 6:00 pm | Teen Titans Go! | Fish Water
 6:15 pm | Teen Titans Go! | Movie Night
 6:30 pm | Teen Titans Go! | TV Knight
 6:45 pm | Teen Titans Go! | Permanent Record
 7:00 pm | Amazing World of Gumball | The Nest
 7:15 pm | Amazing World of Gumball | The Compilation
 7:30 pm | Amazing World of Gumball | The Points
 7:45 pm | Amazing World of Gumball | The Slide