# 2020-09-30
 9:00 pm | Bob's Burgers | Housetrap
 9:30 pm | Bob's Burgers | Hawk & Chick
10:00 pm | American Dad | A Star Is Reborn
10:30 pm | American Dad | Manhattan Magical Murder Mystery Tour
11:00 pm | Family Guy | Padre de Familia
11:30 pm | Family Guy | Back to the Woods
12:00 am | Rick and Morty | A Rickle in Time
12:30 am | Eric Andre Show | Jack
12:45 am | Eric Andre Show | Ryan Phillippe
 1:00 am | Aqua Unit Patrol Squad 1 | Allen
 1:30 am | Family Guy | Padre de Familia
 2:00 am | Family Guy | Back to the Woods
 2:30 am | American Dad | A Star Is Reborn
 3:00 am | Rick and Morty | A Rickle in Time
 3:30 am | Eric Andre Show | Jack
 3:45 am | Eric Andre Show | Ryan Phillippe
 4:00 am | Black Jesus | No Room for Jesus
 4:30 am | Aqua Unit Patrol Squad 1 | Allen
 5:00 am | Bob's Burgers | Housetrap
 5:30 am | Bob's Burgers | Hawk & Chick