# 2020-09-09
 6:00 am | Amazing World of Gumball | The Pony; The Storm
 6:30 am | Amazing World of Gumball | The Dream; The Sidekick
 7:00 am | Amazing World of Gumball | The Hero; The Photo
 7:30 am | Amazing World of Gumball | The Limit; The Game
 8:00 am | Apple & Onion | 4 on 1
 8:15 am | Apple & Onion | Positive Attitude Theory
 8:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
10:00 am | Craig of the Creek | Kelsey the Author
10:15 am | Craig of the Creek | The Ground Is Lava!
10:30 am | Craig of the Creek | Cousin of the Creek
10:45 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
11:00 am | Teen Titans Go! | Cat's Fancy
11:15 am | Teen Titans Go! | Leg Day
11:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
12:00 pm | Teen Titans Go! | Operation Tin Man; Nean
12:30 pm | Mao Mao: Heroes of Pure Heart | Bao Bao's Revenge
12:45 pm | Mao Mao: Heroes of Pure Heart | Badge-a-Fire Explosion
 1:00 pm | Amazing World of Gumball | The World; The Finale
 1:30 pm | Amazing World of Gumball | The Kids; The Fan
 2:00 pm | Amazing World of Gumball | The Parasite
 2:15 pm | Amazing World of Gumball | The Love
 2:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 3:00 pm | We Bare Bears | Food Truck
 3:15 pm | We Bare Bears | Nom Nom's Entourage
 3:30 pm | We Bare Bears | Chloe
 3:45 pm | We Bare Bears | Ranger Tabes
 4:00 pm | Craig of the Creek | Stink Bomb
 4:15 pm | Craig of the Creek | The Time Capsule
 4:30 pm | Craig of the Creek | Summer Wish
 4:45 pm | Craig of the Creek | Beyond the Rapids
 5:00 pm | Total DramaRama | A Ninjustice to Harold
 5:15 pm | Total DramaRama | Simons Are Forever
 5:30 pm | Total DramaRama | Having the Timeout of Our Lives
 5:45 pm | Total DramaRama | Stop! Hamster Time
 6:00 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 6:30 pm | Teen Titans Go! | The Dignity of Teeth
 6:45 pm | Teen Titans Go! | BBBDay!
 7:00 pm | Amazing World of Gumball | The Phone; The Job
 7:30 pm | Amazing World of Gumball | The Words; The Apology