# 2020-09-12
 8:00 pm | Dragon Ball Super | Thanks for Waiting, Lord Beerus! A Super Saiyan God Is Born at Last!
 8:30 pm | Dragon Ball Super | Show Us, Goku! The Power of a Super Saiyan God!
 9:00 pm | American Dad | Naked to the Limit, One More Time
 9:30 pm | American Dad | Seizures Suit Stanny
10:00 pm | American Dad | Spelling Bee My Baby
10:30 pm | Rick and Morty | Something Ricked This Way Comes
11:00 pm | Family Guy | The Former Life of Brian
11:30 pm | Family Guy | Long John Peter
12:00 am | Dragon Ball Super | Goku the Talent Scout - Recruit Krillin and Android 18
12:30 am | JoJo's Bizarre Adventure: Golden Wind | His Name Is Diavolo
 1:00 am | Assassination Classroom | Karma Time
 1:30 am | Black Clover | A New Resolve
 2:00 am | Fire Force | The Secrets of Pyrokinesis
 2:30 am | Naruto:Shippuden | The Fallen Castle
 3:00 am | Samurai Jack | XCVIII
 3:30 am | Gemusetto Machu Picchu | Chapter 3B: Back Spin
 4:00 am | The Boondocks | Wingmen
 4:30 am | The Venture Brothers | Dr. Quymn, Medicine Woman
 5:00 am | Joe Pera Talks With You | Joe Pera Waits With You
 5:15 am | Joe Pera Talks With You | Joe Pera Guides You Through the Dark
 5:30 am | Joe Pera Talks With You | Joe Pera Takes You to the Grocery Store
 5:45 am | Joe Pera Talks With You | Joe Pera Goes to Dave Wojcek's Bachelor Party with You