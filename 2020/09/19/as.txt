# 2020-09-19
 8:00 pm | Dragon Ball Super | Let's Keep Going Lord Beerus! The Battle of Gods!
 8:30 pm | Dragon Ball Super | The Universe Will Shatter? Clash! Destroyer vs. Super Saiyan God!
 9:00 pm | American Dad | Vision: Impossible
 9:30 pm | American Dad | Familyland
10:00 pm | American Dad | Cock of the Sleepwalk
10:30 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
11:00 pm | Family Guy | Not All Dogs Go to Heaven
11:30 pm | Family Guy | Stew-roids
12:00 am | Dragon Ball Super | The Universes Go into Action - Each with Their Own Motives
12:30 am | JoJo's Bizarre Adventure: Golden Wind | The Requiem Quietly Plays, Part 1
 1:00 am | Assassination Classroom | Grown-Up Time
 1:30 am | Black Clover | The Lion Awakens
 2:00 am | Fire Force | Into the Nether
 2:30 am | Naruto:Shippuden | Prologue of Road to Ninja
 3:00 am | Samurai Jack | XCIX
 3:30 am | Gemusetto Machu Picchu | Chapter 4A: Break Point
 4:00 am | The Boondocks | The Passion of Reverend Ruckus
 4:30 am | The Venture Brothers | What Goes Down Must Come Up
 5:00 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 5:15 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 5:30 am | Joe Pera Talks With You | Joe Pera Has a Surprise for You
 5:45 am | Joe Pera Talks With You | Joe Pera Helps You Write