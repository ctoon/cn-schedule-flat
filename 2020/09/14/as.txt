# 2020-09-14
 8:00 pm | Bob's Burgers | Friends with Burger-Fits
 8:30 pm | Bob's Burgers | Best Burger
 9:00 pm | American Dad | The Missing Kink
 9:30 pm | American Dad | The Boring Identity
10:00 pm | Rick and Morty | The ABC's of Beth
10:30 pm | Rick and Morty | The Rickchurian Mortydate
11:00 pm | Family Guy | Love Blactually
11:30 pm | Family Guy | I Dream of Jesus
12:00 am | YOLO Crystal Fantasy | Bush Doof
12:15 am | Robot Chicken | Cake Pillow
12:30 am | Eric Andre Show | Pauly D; Rick Springfield
12:45 am | Eric Andre Show | Bird Up!
 1:00 am | Aqua Teen | Rabbot Redux
 1:15 am | 12oz Mouse | Because They Could
 1:30 am | Family Guy | Love Blactually
 2:00 am | Family Guy | I Dream of Jesus
 2:30 am | Rick and Morty | The ABC's of Beth
 3:00 am | Rick and Morty | The Rickchurian Mortydate
 3:30 am | YOLO Crystal Fantasy | Bush Doof
 3:45 am | Robot Chicken | Cake Pillow
 4:00 am | Black Jesus | Smokin', Drinkin', and Chillin'
 4:30 am | Eric Andre Show | Pauly D; Rick Springfield
 4:45 am | Eric Andre Show | Bird Up!
 5:00 am | Bob's Burgers | Friends with Burger-Fits
 5:30 am | Bob's Burgers | Best Burger