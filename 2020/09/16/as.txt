# 2020-09-16
 8:00 pm | Bob's Burgers | Speakeasy Rider
 8:30 pm | Bob's Burgers | Late Afternoon in the Garden of Bob and Louise
 9:00 pm | American Dad | Da Flippity Flop
 9:30 pm | American Dad | Steve and Snot's Test-Tubular Adventure
10:00 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
10:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
11:00 pm | Family Guy | Ocean's Three and a Half
11:30 pm | Family Guy | Family Gay
12:00 am | YOLO Crystal Fantasy | Enter Bushworld Part One
12:15 am | Robot Chicken | The Robot Chicken Christmas Special: X-Mas United
12:30 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
12:45 am | Eric Andre Show | Tichina Arnold; Steve Schirrpa
 1:00 am | Aqua Teen | Eggball
 1:15 am | 12oz Mouse | Prime Time Nursery Rhyme
 1:30 am | Family Guy | Ocean's Three and a Half
 2:00 am | Family Guy | Family Gay
 2:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | YOLO Crystal Fantasy | Enter Bushworld Part One
 3:45 am | Robot Chicken | The Robot Chicken Christmas Special: X-Mas United
 4:00 am | Black Jesus | The S*** Heist
 4:30 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
 4:45 am | Eric Andre Show | Tichina Arnold; Steve Schirrpa
 5:00 am | Bob's Burgers | Speakeasy Rider
 5:30 am | Bob's Burgers | Late Afternoon in the Garden of Bob and Louise