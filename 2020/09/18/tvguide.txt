# 2020-09-18
 6:00 am | Amazing World of Gumball | The Return; The Routine
 6:30 am | Amazing World of Gumball | The Parking; The Nemesis
 7:00 am | Ben 10 | Rekoil
 7:15 am | Ben 10 | Buktu The Future
 7:30 am | Amazing World of Gumball | The Comic; The Others
 8:00 am | Apple & Onion | Block Party; Appleoni
 8:30 am | Teen Titans Go! | Booty Scooty; Riding the Dragon
 9:00 am | Teen Titans Go! | The Overbite; Teen Titans Roar!
 9:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
10:00 am | Craig of the Creek | Sleepover at Jp's Plush Kingdom
10:30 am | Craig of the Creek | The Evolution of Craig; The Ice Pop Trio
11:00 am | Teen Titans Go! | Booby Trap House; Shrimps and Prime Rib
11:30 am | Teen Titans Go! | Bottle Episode; Hey You, Don't Forget About Me in Your Memory
12:00 pm | Teen Titans Go! | Pyramid Scheme; The Fourth Wall
12:30 pm | Mao Mao, Heroes of Pure Heart | Weapon of Choice; Strange Bedfellows
 1:00 pm | Amazing World of Gumball | The Allergy; The Law
 1:30 pm | Amazing World of Gumball | The Mothers / The Password
 2:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:30 pm | Amazing World of Gumball | The Mirror; The Burden
 3:00 pm | We Bare Bears | The Road; Creature Mysteries
 3:30 pm | We Bare Bears | Emergency; The Library
 4:00 pm | Craig Of The Creek | The Other Side
 4:30 pm | Craig of the Creek | Summer Wish ;Turning the Tables
 5:00 pm | Total DramaRama | For a Few Duncans More He Who Wears the Clown
 5:30 pm | Total DramaRama | Us 'r' Toys; Dream Worriers
 6:00 pm | Total DramaRama | Grody to the Maximum Wiggin' Out
 6:30 pm | Total DramaRama | The Upside of Hunger; Fire in the Hole
 7:00 pm | Amazing World of Gumball | The Spoiler; The Society
 7:30 pm | Amazing World of Gumball | The Nobody; The Countdown