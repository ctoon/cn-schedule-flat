# 2020-06-22
 6:00 am | Amazing World of Gumball | The Stars; The Line
 6:30 am | Amazing World of Gumball | The Singing; The Candidate
 7:00 am | Amazing World of Gumball | The Matchmaker; The Puppets
 7:30 am | Amazing World of Gumball | The Grades; The Lady
 8:00 am | Apple & Onion | Apple's Focus Selfish Shellfish
 8:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 9:00 am | Teen Titans Go! | Vegetables; Friendship
 9:30 am | Teen Titans Go! | The Mask; Slumber Party
10:00 am | Craig of the Creek | The Other Side: The Tournament
10:30 am | Victor and Valentino | Lonely Haunts Club Balloon Boys
11:00 am | Teen Titans Go! | The Best Robin; Road Trip
11:30 am | Teen Titans Go! | Hot Garbage; Mouth Hole
12:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:30 pm | Mao Mao, Heroes of Pure Heart | Small Meet; Tanya Keys
 1:00 pm | Amazing World of Gumball | The Vegging; The Stink
 1:30 pm | Amazing World of Gumball | The Father; The Awareness
 2:00 pm | Amazing World of Gumball | The Cringe; The Parents
 2:30 pm | Amazing World of Gumball | The Neighbor; The Founder
 3:00 pm | Craig Of The Creek | Council Of The Creek; Deep Creek Salvage
 3:30 pm | Craig Of The Creek | Sparkle Cadet; The Shortcut
 4:00 pm | Total DramaRama | Soother or Later Driving Miss Crazy
 4:30 pm | Total DramaRama | Weiner Takes All Lie-Ranosaurus Wrecked
 5:00 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 5:30 pm | Teen Titans Go! | Cool School; Video Game References
 6:00 pm | Apple & Onion | Gyranoid of the Future Appleoni
 6:30 pm | Amazing World Of Gumball | The Wish; The Transformation
 7:00 pm | Amazing World of Gumball | The Understanding; The Revolt
 7:30 pm | We Bare Bears | Neighbors Charlie's Halloween Thing