# 2020-06-18
 8:00 pm | American Dad | The Long Bomb
 8:30 pm | American Dad | Kloger
 9:00 pm | American Dad | Garbage Stan
 9:30 pm | Rick and Morty | Pickle Rick
10:00 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
10:30 pm | Bob's Burgers | The Gene & Courtney Show
11:00 pm | Family Guy | Grimm Job
11:30 pm | Family Guy | Brian's a Bad Father
12:00 am | Tender Touches | Rock and Roll Guys
12:15 am | JJ Villard's Fairy Tales | Snow White
12:30 am | Aqua Teen | Dumber Dolls
12:45 am | Squidbillies | This Show Is Called Squidbillies
 1:00 am | The Boondocks | It's a Black President, Huey Freeman
 1:30 am | Family Guy | Grimm Job
 2:00 am | Family Guy | Brian's a Bad Father
 2:30 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 3:00 am | Bob's Burgers | The Gene & Courtney Show
 3:30 am | Rick and Morty | Pickle Rick
 4:00 am | China, IL | China-Man Begins
 4:30 am | Tender Touches | Rock and Roll Guys
 4:45 am | JJ Villard's Fairy Tales | Snow White
 5:00 am | Aqua Teen | Dumber Dolls
 5:15 am | Squidbillies | This Show Is Called Squidbillies
 5:30 am | My Hero Academia | Game Start