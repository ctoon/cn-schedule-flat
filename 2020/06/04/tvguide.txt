# 2020-06-04
 6:00 am | Amazing World of Gumball | The Mirror; The Burden
 6:30 am | Amazing World of Gumball | The Man; The Bros
 7:00 am | Amazing World of Gumball | The Lie; The Pizza
 7:30 am | Amazing World of Gumball | The Butterfly; The Question
 8:00 am | Apple & Onion | The Music Store Thief; The Inbetween
 8:30 am | Teen Titans Go! | The Groover; Bbrbday
 9:00 am | Teen Titans Go! | The Power Of Shrimps; Tall Titan Tales
 9:30 am | Teen Titans Go! | My Name Is Jose; Nostalgia Is Not a Substitute for an Actual Story
10:00 am | Craig of the Creek | Sugar Smugglers; Trading Day
10:30 am | Victor and Valentino | Lords of Ghost Town; Kindred Spirits
11:00 am | Teen Titans Go! | Real Orangins! ; The Business Ethics Wink Wink
11:30 am | Teen Titans Go! | Flashback
12:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:30 pm | Mao Mao, Heroes of Pure Heart | Breakup; Meet Tanya Keys
 1:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:30 pm | Amazing World of Gumball | The Internet; The Plan
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Kids; The Fan
 3:00 pm | Craig of the Creek | Itch to Explore; Lost in the Sewer
 3:30 pm | Craig Of The Creek | You're It; The 1 Future Is Cardboard
 4:00 pm | Total DramaRama | Royal Flush Total Eclipse of the Fart
 4:30 pm | Total DramaRama | Dissing Cousins
 5:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 6:00 pm | Apple & Onion | Appleoni; Dragonhead
 6:30 pm | Amazing World of Gumball | The Move; The Boss
 7:00 pm | Amazing World of Gumball | The Allergy; The Law
 7:30 pm | We Bare Bears | Chicken and Waffles; Anger Management