# 2020-06-07
 6:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 6:30 am | Bakugan: Armored Alliance | Kung Fu Master; A Battle of Popularity
 7:00 am | Transformers: Cyberverse | Silent Strike
 7:15 am | Transformers: Cyberverse | The Other One
 7:30 am | Power Players | Grin and Bear It
 7:45 am | Power Players | Green With Envy
 8:00 am | Ben 10 | A Sticky Situation
 8:15 am | Ben 10 | What Rhymes With Omnitrix?
 8:30 am | Amazing World of Gumball | The Crew
 8:45 am | Amazing World of Gumball | The Others
 9:00 am | Amazing World of Gumball | The Signature
 9:15 am | Amazing World of Gumball | The Wicked
 9:30 am | Amazing World of Gumball | The Origins
 9:45 am | Amazing World of Gumball | The Origins
10:00 am | Amazing World of Gumball | The Apprentice
10:15 am | Amazing World of Gumball | The Gift
10:30 am | Amazing World of Gumball | The Agent
10:45 am | Amazing World of Gumball | The List
11:00 am | Amazing World of Gumball | The Pest
11:15 am | Amazing World of Gumball | The Traitor
11:30 am | Amazing World of Gumball | The Hug
11:45 am | Amazing World of Gumball | The Advice
12:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
12:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 1:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 1:30 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 2:00 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 2:30 pm | Teen Titans Go! | Video Game References; Cool School
 2:45 pm | Teen Titans Go! | Movie Night
 3:00 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 3:30 pm | Teen Titans Go! | Let's Get Serious; Tamaranian Vacation
 4:00 pm | DC Super Hero Girls | #TweenTitans
 4:15 pm | DC Super Hero Girls | #EmperorPenguin
 4:30 pm | Amazing World of Gumball | The Signal
 4:45 pm | Amazing World of Gumball | The Stink
 5:00 pm | Amazing World of Gumball | The Routine
 5:15 pm | Amazing World of Gumball | The Girlfriend
 5:30 pm | Amazing World of Gumball | The Parking
 5:45 pm | Amazing World of Gumball | The Parasite
 6:00 pm | Amazing World of Gumball | The Upgrade
 6:15 pm | Amazing World of Gumball | The Love
 6:30 pm | Amazing World of Gumball | The Comic
 6:45 pm | Amazing World of Gumball | The Awkwardness
 7:00 pm | Adventure Time | Guardians of Sunshine
 7:15 pm | Adventure Time | Video Makers
 7:30 pm | Adventure Time | BMO Noire
 7:45 pm | Adventure Time | Time Sandwich