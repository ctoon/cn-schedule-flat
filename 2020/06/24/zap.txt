# 2020-06-24
 6:00 am | Amazing World of Gumball | The Vegging
 6:15 am | Amazing World of Gumball | The Stink
 6:30 am | Amazing World of Gumball | The Father
 6:45 am | Amazing World of Gumball | The Awareness
 7:00 am | Amazing World of Gumball | The Cringe
 7:15 am | Amazing World of Gumball | The Parents
 7:30 am | Amazing World of Gumball | The Neighbor
 7:45 am | Amazing World of Gumball | The Founder
 8:00 am | Apple & Onion | Gyranoid of the Future
 8:15 am | Apple & Onion | Appleoni
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 9:30 am | Teen Titans Go! | Video Game References; Cool School
10:00 am | Craig of the Creek | The Shortcut
10:15 am | Craig of the Creek | Sparkle Cadet
10:30 am | Victor and Valentino | The Dark Room
10:45 am | Victor and Valentino | Escape From Bebe Bay
11:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:30 am | Teen Titans Go! | Operation Tin Man; Nean
12:00 pm | Teen Titans Go! | The Fourth Wall
12:15 pm | Teen Titans Go! | Secret Garden
12:30 pm | Mao Mao: Heroes of Pure Heart | Weapon of Choice
12:45 pm | Mao Mao: Heroes of Pure Heart | Enemy Mime
 1:00 pm | Amazing World of Gumball | The Transformation
 1:15 pm | Amazing World of Gumball | The Wish
 1:30 pm | Amazing World of Gumball | The Understanding
 1:45 pm | Amazing World of Gumball | The Revolt
 2:00 pm | Amazing World of Gumball | The Ad
 2:15 pm | Amazing World of Gumball | The Mess
 2:30 pm | Amazing World of Gumball | The Slip
 2:45 pm | Amazing World of Gumball | The Possession
 3:00 pm | Craig of the Creek | The Mystery of the Timekeeper
 3:15 pm | Craig of the Creek | Stink Bomb
 3:30 pm | Craig of the Creek | Alone Quest
 3:45 pm | Craig of the Creek | The Evolution of Craig
 4:00 pm | Total DramaRama | Look Who's Clocking
 4:15 pm | Total DramaRama | Pudding the Planet First
 4:30 pm | Total DramaRama | Harold Swatter and the Goblet of Flies
 4:45 pm | Total DramaRama | Supply Mom
 5:00 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 5:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 5:30 pm | Teen Titans Go! | Grube's Fairytales
 5:45 pm | Teen Titans Go! | Finally a Lesson
 6:00 pm | Apple & Onion | Whale Spotting
 6:15 pm | Apple & Onion | Falafel's in Jail
 6:30 pm | Amazing World of Gumball | The Grades
 6:45 pm | Amazing World of Gumball | The Lady
 7:00 pm | Amazing World of Gumball | The Ex
 7:15 pm | Amazing World of Gumball | The Cage
 7:30 pm | We Bare Bears | $100
 7:45 pm | We Bare Bears | Dog Hotel