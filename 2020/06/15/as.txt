# 2020-06-15
 8:00 pm | American Dad | Whole Slotta Love
 8:30 pm | American Dad | A Nice Night for a Drive
 9:00 pm | American Dad | Casino Normale
 9:30 pm | Rick and Morty | The Wedding Squanchers
10:00 pm | Bob's Burgers | The Oeder Games
10:30 pm | Bob's Burgers | Sliding Bobs
11:00 pm | Family Guy | A Fistful of Meg
11:30 pm | Family Guy | Boopa-Dee Bappa Dee
12:00 am | Tender Touches | That's A Wrap
12:15 am | Robot Chicken | Shall I Visit the Dinosaurs?
12:30 am | Aqua Teen | Ol Drippy
12:45 am | Squidbillies | Meth O.D. To My Madness
 1:00 am | The Boondocks | The S Word
 1:30 am | Family Guy | A Fistful of Meg
 2:00 am | Family Guy | Boopa-Dee Bappa Dee
 2:30 am | Bob's Burgers | The Oeder Games
 3:00 am | Bob's Burgers | Sliding Bobs
 3:30 am | Rick and Morty | The Wedding Squanchers
 4:00 am | China, IL | Do You Know Who You Look Like?
 4:30 am | Tender Touches | That's A Wrap
 4:45 am | Robot Chicken | Shall I Visit the Dinosaurs?
 5:00 am | Aqua Teen | Ol Drippy
 5:15 am | Squidbillies | Meth O.D. To My Madness
 5:30 am | My Hero Academia | Stripping the Varnish