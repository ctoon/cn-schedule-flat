# 2020-06-28
 6:00 am | Teen Titans Go! | Girls Night In Part 1
 6:30 am | Bakugan: Armored Alliance | The Best Friend; Thief and Brawler
 7:00 am | Ben 10 | Gentle Ben
 7:15 am | Ben 10 | Funhouse
 7:30 am | Amazing World of Gumball | The Code
 7:45 am | Amazing World of Gumball | The Night
 8:00 am | Amazing World of Gumball | The List
 8:15 am | Amazing World of Gumball | The Nemesis
 8:30 am | Amazing World of Gumball | The Sock; The Genius
 9:00 am | Amazing World of Gumball | The Mustache; The Date
 9:30 am | Amazing World of Gumball | The Club; The Wand
10:00 am | Amazing World of Gumball | The Ape; The Poltergeist
10:30 am | Amazing World of Gumball | The Quest; The Spoon
11:00 am | Amazing World of Gumball | The Society; The Spoiler
11:30 am | Amazing World of Gumball | The Microwave; The Meddler
12:00 pm | Teen Titans Go! | Record Book
12:15 pm | Teen Titans Go! | Brain Percentages
12:30 pm | Teen Titans Go! | Walk Away
12:45 pm | Teen Titans Go! | We're Off to Get Awards
 1:00 pm | Teen Titans Go! | Bat Scouts
 1:15 pm | Teen Titans Go! | Rain on Your Wedding Day
 1:30 pm | Teen Titans Go! | Beast Boy's That's What's Up
 2:00 pm | Teen Titans Go! | Brain Percentages
 2:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 3:00 pm | Craig of the Creek | Sour Candy Trials
 3:15 pm | Craig of the Creek | Council of the Creek
 3:30 pm | Craig of the Creek | Sparkle Cadet
 3:45 pm | Craig of the Creek | Stink Bomb
 4:00 pm | DC Super Hero Girls | #AdventuresInBunnysitting
 4:15 pm | DC Super Hero Girls | #HateTriangle
 4:30 pm | Amazing World of Gumball | The Helmet; The Fight
 5:00 pm | Amazing World of Gumball | The End; The DVD
 5:30 pm | Amazing World of Gumball | The Knights; The Colossus
 6:00 pm | Amazing World of Gumball | The Fridge; The Remote
 6:30 pm | Amazing World of Gumball | The Flower; The Banana
 7:00 pm | Adventure Time | Football
 7:15 pm | Adventure Time | Five More Short Graybles
 7:30 pm | Adventure Time | The More You Moe; The Moe You Know