# 2020-06-28
 8:00 pm | Final Space | Chapter Two
 8:30 pm | American Dad | Shark?!
 9:00 pm | American Dad | Cheek to Cheek: A Stripper's Story
 9:30 pm | American Dad | Game Night
10:00 pm | Family Guy | A House Full of Peters
10:30 pm | Family Guy | Guy, Robot
11:00 pm | Rick and Morty | Pickle Rick
11:30 pm | Rick and Morty | The Vat of Acid Episode
12:00 am | Robot Chicken | Max Caenen in: Why Would He Know If His Mother's A Size Queen
12:15 am | Robot Chicken | Petless M in: Cars are Couches of the Road
12:30 am | The Shivering Truth | Carrion My Son
12:45 am | JJ Villard's Fairy Tales | The Goldilox Massacre
 1:00 am | Mr. Pickles | S.H.O.E.S
 1:15 am | Mr. Pickles | Telemarketers Are the Devil
 1:30 am | American Dad | Game Night
 2:00 am | Family Guy | A House Full of Peters
 2:30 am | Family Guy | Guy, Robot
 3:00 am | Rick and Morty | The Vat of Acid Episode
 3:30 am | Rick and Morty | Pickle Rick
 4:00 am | Robot Chicken | Max Caenen in: Why Would He Know If His Mother's A Size Queen
 4:15 am | Robot Chicken | Petless M in: Cars are Couches of the Road
 4:30 am | The Shivering Truth | Carrion My Son
 4:45 am | JJ Villard's Fairy Tales | The Goldilox Massacre
 5:00 am | Joe Pera Talks With You | Joe Pera Takes You on a Fall Drive
 5:15 am | Joe Pera Talks With You | Joe Pera Shows You How to Dance
 5:30 am | American Dad | Cheek to Cheek: A Stripper's Story