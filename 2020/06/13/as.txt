# 2020-06-13
 8:00 pm | Dragon Ball Z Kai | Vegeta's Final Push! Defeat the Invincible Cell!
 8:30 pm | Dragon Ball Z Kai | The Strongest Super Saiyan! Trunks' Power Unleashed!
 9:00 pm | American Dad | Ninety North, Zero West
 9:30 pm | American Dad | Roger 'n Me
10:00 pm | Bob's Burgers | Housetrap
10:30 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
11:00 pm | Family Guy | Vestigial Peter
11:30 pm | Family Guy | Quagmire's Quagmire
12:00 am | My Hero Academia | One for All
12:30 am | Paranoia Agent | Happy Family Planning
 1:00 am | Mob Psycho 100 | Discord ~To Become One~
 1:30 am | Black Clover | Three Problems
 2:00 am | Ballmastrz: 9009 | Very Special Balls
 2:15 am | Ballmastrz: 9009 | To Catch a Princess
 2:30 am | Naruto:Shippuden | The Mizukage, the Giant Clam, and the Mirage
 3:00 am | Gemusetto Machu Picchu | Chapter 1B: Your Serve
 3:30 am | The Shivering Truth | The Diff
 3:45 am | The Shivering Truth | ConstaDeath
 4:00 am | The Boondocks | Stinkmeaner: Begun the Clone War Has
 4:30 am | The Venture Brothers | Arrears in Science
 5:00 am | Aqua TV Show Show | Merlo Sauvignon Blanco
 5:15 am | Aqua TV Show Show | Working Stiffs
 5:30 am | Bob's Burgers | Housetrap