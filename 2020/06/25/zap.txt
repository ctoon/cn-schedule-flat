# 2020-06-25
 6:00 am | Amazing World of Gumball | The Pact
 6:15 am | Amazing World of Gumball | The Schooling
 6:30 am | Amazing World of Gumball | The Intelligence
 6:45 am | Amazing World of Gumball | The Master
 7:00 am | Amazing World of Gumball | The Potion
 7:15 am | Amazing World of Gumball | The Silence
 7:30 am | Amazing World of Gumball | The Spinoffs
 7:45 am | Amazing World of Gumball | The Future
 8:00 am | Apple & Onion | Fun Proof
 8:15 am | Apple & Onion | River of Gold
 8:30 am | Teen Titans Go! | The Dignity of Teeth
 8:45 am | Teen Titans Go! | The Croissant
 9:00 am | Teen Titans Go! | The Spice Game
 9:15 am | Teen Titans Go! | I'm the Sauce
 9:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
10:00 am | Craig of the Creek | The Great Fossil Rush
10:15 am | Craig of the Creek | Vulture's Nest
10:30 am | Victor and Valentino | The Collector
10:45 am | Victor and Valentino | Band for Life
11:00 am | Teen Titans Go! | Accept the Next Proposition You Hear
11:15 am | Teen Titans Go! | Squash & Stretch
11:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
11:45 am | Teen Titans Go! | Garage Sale
12:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
12:30 pm | Mao Mao: Heroes of Pure Heart | Legend of Torbaclaun
12:45 pm | Mao Mao: Heroes of Pure Heart | Breakup
 1:00 pm | Amazing World of Gumball | The Stars
 1:15 pm | Amazing World of Gumball | The Line
 1:30 pm | Amazing World of Gumball | The Singing
 1:45 pm | Amazing World of Gumball | The Candidate
 2:00 pm | Amazing World of Gumball | The Matchmaker
 2:15 pm | Amazing World of Gumball | The Puppets
 2:30 pm | Amazing World of Gumball | The Grades
 2:45 pm | Amazing World of Gumball | The Lady
 3:00 pm | Craig of the Creek | Memories of Bobby
 3:15 pm | Craig of the Creek | Creek Daycare
 3:30 pm | Craig of the Creek | Jacob of the Creek
 3:45 pm | Craig of the Creek | Sugar Smugglers
 4:00 pm | Total DramaRama | Stink. Stank. Stunk
 4:15 pm | Total DramaRama | Mooshy Mon Mons
 4:30 pm | Total DramaRama | Robo Teacher
 4:45 pm | Total DramaRama | Student Becomes the Teacher
 5:00 pm | Teen Titans Go! | Friendship; Vegetables
 5:30 pm | Teen Titans Go! | The Mask; Slumber Party
 6:00 pm | Apple & Onion | Apple's Focus
 6:15 pm | Apple & Onion | Selfish Shellfish
 6:30 pm | Amazing World of Gumball | The Vegging
 6:45 pm | Amazing World of Gumball | The Stink
 7:00 pm | Amazing World of Gumball | The Father
 7:15 pm | Amazing World of Gumball | The Awareness
 7:30 pm | We Bare Bears | The Fair
 7:45 pm | We Bare Bears | Dance Lessons