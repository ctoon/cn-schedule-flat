# 2020-06-01
 8:00 pm | American Dad | LGBSteve
 8:30 pm | American Dad | The Shrink
 9:00 pm | American Dad | Holy S***, Jeff's Back!
 9:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
10:00 pm | Bob's Burgers | The Kids Run Away
10:30 pm | Bob's Burgers | Gene It On
11:00 pm | Family Guy | Fighting Irish
11:30 pm | Family Guy | Take My Wife
12:00 am | Robot Chicken | Junk in the Trunk
12:15 am | Robot Chicken | Nutcracker Sweet
12:30 am | Aqua Teen | Supermodel
12:45 am | Squidbillies | Swayze Crazy
 1:00 am | The Boondocks | Mr. Medicinal
 1:30 am | Family Guy | Fighting Irish
 2:00 am | Family Guy | Take My Wife
 2:30 am | Bob's Burgers | The Kids Run Away
 3:00 am | Bob's Burgers | Gene It On
 3:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 4:00 am | Brad Neely's Harg Nallin' Sclopio Peepio | For Charlize
 4:15 am | China, IL | The Dream Reamer
 4:30 am | Robot Chicken | Junk in the Trunk
 4:45 am | Robot Chicken | Nutcracker Sweet
 5:00 am | Aqua Teen | Supermodel
 5:15 am | Squidbillies | Swayze Crazy
 5:30 am | My Hero Academia | Time to Pick Some Names