# 2020-06-23
 6:00 am | Amazing World of Gumball | The Diet
 6:15 am | Amazing World of Gumball | The Sucker
 6:30 am | Amazing World of Gumball | The Ex
 6:45 am | Amazing World of Gumball | The Cage
 7:00 am | Amazing World of Gumball | The Rival
 7:15 am | Amazing World of Gumball | The Anybody
 7:30 am | Amazing World of Gumball | The One
 7:45 am | Amazing World of Gumball | The Shippening
 8:00 am | Apple & Onion | Lil Noodle
 8:15 am | Apple & Onion | Apple's Formula
 8:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
10:00 am | Craig of the Creek | Jacob of the Creek
10:15 am | Craig of the Creek | Sugar Smugglers
10:30 am | Victor and Valentino | Suerte
10:45 am | Victor and Valentino | The Great Bongo Heist
11:00 am | Teen Titans Go! | Cat's Fancy
11:15 am | Teen Titans Go! | Leg Day
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Teen Titans Go! | The Dignity of Teeth
12:15 pm | Teen Titans Go! | The Croissant
12:30 pm | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
12:45 pm | Mao Mao: Heroes of Pure Heart | Sugar Berry Fever
 1:00 pm | Amazing World of Gumball | The Pact
 1:15 pm | Amazing World of Gumball | The Schooling
 1:30 pm | Amazing World of Gumball | The Intelligence
 1:45 pm | Amazing World of Gumball | The Master
 2:00 pm | Amazing World of Gumball | The Potion
 2:15 pm | Amazing World of Gumball | The Silence
 2:30 pm | Amazing World of Gumball | The Spinoffs
 2:45 pm | Amazing World of Gumball | The Future
 3:00 pm | Craig of the Creek | Dibs Court
 3:15 pm | Craig of the Creek | Cousin of the Creek
 3:30 pm | Craig of the Creek | The Great Fossil Rush
 3:45 pm | Craig of the Creek | The Invitation
 4:00 pm | Total DramaRama | Apoca-lice Now
 4:15 pm | Total DramaRama | An Egg-stremely Bad Idea
 4:30 pm | Total DramaRama | Gnome More Mister Nice Guy
 4:45 pm | Total DramaRama | Exercising the Demons
 5:00 pm | Teen Titans Go! | The Spice Game
 5:15 pm | Teen Titans Go! | I'm the Sauce
 5:30 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 6:00 pm | Apple & Onion | Fun Proof
 6:15 pm | Apple & Onion | River of Gold
 6:30 pm | Amazing World of Gumball | The Stars
 6:45 pm | Amazing World of Gumball | The Line
 7:00 pm | Amazing World of Gumball | The Singing
 7:15 pm | Amazing World of Gumball | The Candidate
 7:30 pm | We Bare Bears | Crowbar Jones
 7:45 pm | We Bare Bears | The Kitty