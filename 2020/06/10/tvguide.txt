# 2020-06-10
 6:00 am | Amazing World of Gumball | The Apprentice; The Loophole
 6:30 am | Amazing World of Gumball | The Awkwardness; The Check
 7:00 am | Amazing World of Gumball | The Pest; The Nest
 7:30 am | Amazing World of Gumball | The Hug; The Points
 8:00 am | Apple & Onion | Tips; Not Funny
 8:30 am | Teen Titans Go! | TV Knight 5; Bat Scouts
 9:00 am | Teen Titans Go! | Magic Man; Record Book
 9:30 am | Teen Titans Go! | Titans Go Casual Walk Away
10:00 am | Craig of the Creek | Into The Overpast
10:15 am | Craig of the Creek | The Time Capsule
10:30 am | Victor and Valentino | Dead Ringer; Churro Kings
11:00 am | Teen Titans Go! | We're off to Get Awards Rain on Your Wedding Day
11:30 am | Teen Titans Go! | Girls Night In
12:00 pm | Teen Titans Go! | Super Summer Hero Camp
12:30 pm | Mao Mao, Heroes of Pure Heart | Not Impressed; Orangusnake Begins
 1:00 pm | Amazing World of Gumball | The Blame; The Comic
 1:30 pm | Amazing World of Gumball | The Fuss; The Romantic
 2:00 pm | Amazing World of Gumball | The Detective; The Uploads
 2:30 pm | Amazing World of Gumball | The Wicked; The Fury
 3:00 pm | Craig of the Creek | Monster in the Garden; Creek Cart Racers
 3:30 pm | Craig of the Creek | The Curse; Secret Book Club
 4:00 pm | Total DramaRama | Cuttin' Corners; From Badge To Worse
 4:30 pm | Total DramaRama | Sharing Is Caring; Toys Will Be Toys
 5:00 pm | Teen Titans Go! | The Metric System Vs. Freedom; Tv Knight 4
 5:30 pm | Teen Titans Go! | Chaff; The Teen Titans Roar!
 6:00 pm | Apple & Onion | Apple's in Trouble; Party Popper
 6:30 pm | Craig of the Creek | Crisis at Elder Rock; Tea Timer's Ball
 7:00 pm | Amazing World of Gumball | The Girlfriend; The Others
 7:30 pm | We Bare Bears | Everyone's Tube; Bunnies