# 2020-06-29
 6:00 am | Amazing World of Gumball | The Drama
 6:15 am | Amazing World of Gumball | The Future
 6:30 am | Amazing World of Gumball | The Ghouls
 6:45 am | Amazing World of Gumball | The Check
 7:00 am | Amazing World of Gumball | The BFFS
 7:15 am | Amazing World of Gumball | The Pest
 7:30 am | Amazing World of Gumball | The Agent
 7:45 am | Amazing World of Gumball | The Hug
 8:00 am | Apple & Onion | Heatwave
 8:15 am | Apple & Onion | The Music Store Thief
 8:30 am | Teen Titans Go! | Two Parter: Part One
 8:45 am | Teen Titans Go! | Two Parter: Part Two
 9:00 am | Teen Titans Go! | BBBDay!
 9:15 am | Teen Titans Go! | Arms Race With Legs
 9:30 am | Teen Titans Go! | Obinray
 9:45 am | Teen Titans Go! | Who's Laughing Now
10:00 am | Craig of the Creek | The Ground Is Lava!
10:15 am | Craig of the Creek | Trading Day
10:30 am | Victor and Valentino | The Collector
10:45 am | Victor and Valentino | The Boy Who Cried Lechuza
11:00 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
11:15 am | Teen Titans Go! | Oregon Trail
11:30 am | Teen Titans Go! | Wally T
11:45 am | Teen Titans Go! | Snuggle Time
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 3:15 pm | Craig of the Creek | Sleepover at JP's
 3:30 pm | Craig of the Creek | Kelsey the Elder
 3:45 pm | Craig of the Creek | Tea Timer's Ball
 4:00 pm | Total DramaRama | Glove Glove Me Do
 4:15 pm | Total DramaRama | Beth and the Beanstalk
 4:30 pm | Total DramaRama | The Tooth About Zombies
 4:45 pm | Total DramaRama | Pinata Regatta
 5:00 pm | Teen Titans Go! | Booby Trap House
 5:15 pm | Teen Titans Go! | BBSFBDAY
 5:30 pm | Teen Titans Go! | Fish Water
 5:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 6:00 pm | Apple & Onion | Dragonhead
 6:15 pm | Apple & Onion | Burger's Trampoline
 6:30 pm | Amazing World of Gumball | The Gi; The Kiss
 7:00 pm | Amazing World of Gumball | The Party; The Refund
 7:30 pm | We Bare Bears | Professor Lampwick
 7:45 pm | We Bare Bears | More Everyone's Tube