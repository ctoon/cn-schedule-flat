# 2020-06-29
 6:00 am | Amazing World of Gumball | The Drama; The Heart
 6:30 am | Amazing World of Gumball | The Ghouls; The Check
 7:00 am | Amazing World of Gumball | The Bffs; The Pest
 7:30 am | Amazing World of Gumball | The Agent; The Hug
 8:00 am | Apple & Onion | The Heatwave Music Store Thief
 8:30 am | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
 9:00 am | Teen Titans Go! | Bbbday! Arms Race With Legs
 9:30 am | Teen Titans Go! | Obinray Who's Laughing Now
10:00 am | Craig of the Creek | The Ground Is Lava!
10:15 am | Craig of the Creek | Trading Day
10:30 am | Victor And Valentino | The Boy Who Cried Lechuza; The Collector
11:00 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice Oregon Trail
11:30 am | Teen Titans Go! | Snuggle Time; Wally T
12:00 pm | Teen Titans Go! | Booty Scooty Shrimps and Prime Rib
12:30 pm | Mao Mao, Heroes of Pure Heart | Bobo Chan All by Mao Self
 1:00 pm | Amazing World of Gumball | The Pressure; The Painting
 1:30 pm | Amazing World of Gumball | The Responsible; The Dress
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers Sleepover at Jp's
 3:30 pm | Craig of the Creek | Kelsey the Elder Tea Timer's Ball
 4:00 pm | Total DramaRama | Glove Glove Me Do Beth and the Beanstalk
 4:30 pm | Total DramaRama | Tooth About Zombies; The Pinata Regatta
 5:00 pm | Teen Titans Go! | Booby Trap House; BBSFBDAY!
 5:30 pm | Teen Titans Go! | Fish Water; Inner Beauty of a Cactus
 6:00 pm | Apple & Onion | Dragonhead Burger's Trampoline
 6:30 pm | Amazing World of Gumball | The GI; The Kiss
 7:00 pm | Amazing World of Gumball | The Party; The Refund
 7:30 pm | We Bare Bears | Professor Lampwick More Everyone's Tube