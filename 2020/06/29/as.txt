# 2020-06-29
 8:00 pm | American Dad | Fleabiscuit
 8:30 pm | American Dad | The Future Is Borax
 9:00 pm | American Dad | Pilot
 9:30 pm | Rick and Morty | Anatomy Park
10:00 pm | Bob's Burgers | Large Brother, Where Fart Thou?
10:30 pm | Bob's Burgers | The Quirk-Ducers
11:00 pm | Family Guy | Stewie, Chris & Brian's Excellent Adventure
11:30 pm | Family Guy | Our Idiot Brian
12:00 am | Robot Chicken | Fila Ogden in: Maggie's Got a Full Load
12:15 am | Robot Chicken | Hermie Nursery in: Seafood Sensation
12:30 am | Aqua Teen | Interfection
12:45 am | Squidbillies | Office Politics Trouble
 1:00 am | The Boondocks | The Fund-Raiser
 1:30 am | Family Guy | Stewie, Chris & Brian's Excellent Adventure
 2:00 am | Family Guy | Our Idiot Brian
 2:30 am | Bob's Burgers | Large Brother, Where Fart Thou?
 3:00 am | Bob's Burgers | The Quirk-Ducers
 3:30 am | Rick and Morty | Anatomy Park
 4:00 am | China, IL | A Gentleman's Bet
 4:30 am | Robot Chicken | Fila Ogden in: Maggie's Got a Full Load
 4:45 am | Robot Chicken | Hermie Nursery in: Seafood Sensation
 5:00 am | Aqua Teen | Interfection
 5:15 am | Squidbillies | Office Politics Trouble
 5:30 am | My Hero Academia | From Iida to Midoriya