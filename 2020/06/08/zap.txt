# 2020-06-08
 6:00 am | Amazing World of Gumball | The Downer; The Egg
 6:30 am | Amazing World of Gumball | The Triangle; The Money
 7:00 am | Amazing World of Gumball | The Return
 7:15 am | Amazing World of Gumball | The Traitor
 7:30 am | Amazing World of Gumball | The Nemesis
 7:45 am | Amazing World of Gumball | The Advice
 8:00 am | Apple & Onion | A New Life
 8:15 am | Apple & Onion | Burger's Trampoline
 8:30 am | Teen Titans Go! | History Lesson
 8:45 am | Teen Titans Go! | The Art of Ninjutsu
 9:00 am | Teen Titans Go! | The Metric System vs. Freedom
 9:15 am | Teen Titans Go! | TV Knight 4
 9:30 am | Teen Titans Go! | The Chaff
 9:45 am | Teen Titans Go! | Teen Titans Roar!
10:00 am | Craig of the Creek | In the Key of the Creek
10:15 am | Craig of the Creek | The Children of the Dog
10:30 am | Victor and Valentino | Journey to Maiz Mountains, Part 1
11:00 am | Teen Titans Go! | Curse of the Booty Scooty
11:15 am | Teen Titans Go! | What's Opera, Titans?
11:30 am | Teen Titans Go! | Them Soviet Boys
11:45 am | Teen Titans Go! | Royal Jelly
12:00 pm | Teen Titans Go! | TV Knight 5
12:15 pm | Teen Titans Go! | Bat Scouts
12:30 pm | Mao Mao: Heroes of Pure Heart | Ultraclops
12:45 pm | Mao Mao: Heroes of Pure Heart | Head Chef
 1:00 pm | Amazing World of Gumball | The Apprentice
 1:15 pm | Amazing World of Gumball | The Loophole
 1:30 pm | Amazing World of Gumball | The Check
 1:45 pm | Amazing World of Gumball | The Awkwardness
 2:00 pm | Amazing World of Gumball | The Pest
 2:15 pm | Amazing World of Gumball | The Nest
 2:30 pm | Amazing World of Gumball | The Hug
 2:45 pm | Amazing World of Gumball | The Points
 3:00 pm | Craig of the Creek | Too Many Treasures
 3:15 pm | Craig of the Creek | The Climb
 3:30 pm | Craig of the Creek | Wildernessa
 3:45 pm | Craig of the Creek | Big Pinchy
 4:00 pm | Total DramaRama | Cluckwork Orange
 4:15 pm | Total DramaRama | Paint That a Shame
 4:30 pm | Total DramaRama | Free Chili
 4:45 pm | Total DramaRama | Snots Landing
 5:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 6:00 pm | Apple & Onion | Tips
 6:15 pm | Apple & Onion | Not Funny
 6:30 pm | Amazing World of Gumball | The Comic
 6:45 pm | Amazing World of Gumball | The Blame
 7:00 pm | Amazing World of Gumball | The Romantic
 7:15 pm | Amazing World of Gumball | The Fuss
 7:30 pm | We Bare Bears | The Audition
 7:45 pm | We Bare Bears | Planet Bears