# 2020-06-11
 6:00 am | Amazing World of Gumball | The Sale; The Bus
 6:30 am | Amazing World of Gumball | The Routine; The Night
 7:00 am | Amazing World of Gumball | The Parking; The Misunderstanding
 7:30 am | Amazing World of Gumball | The Roots; The Upgrade
 8:00 am | Apple & Onion | Falafel's Fun Day; Onionless
 8:30 am | Teen Titans Go! | Beast Boy's That's What's Up
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
10:00 am | Craig of the Creek | Beyond the Rapids
10:15 am | Craig of the Creek | The Jinxening
10:30 am | Victor and Valentino | Brotherly Love Know It All
11:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
11:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
12:00 pm | Teen Titans Go! | Communicate Openly; Lil' Dimples
12:30 pm | Mao Mao, Heroes of Pure Heart | Bao Bao's Revenge Captured Clops
 1:00 pm | Amazing World of Gumball | The Egg; The Downer
 1:30 pm | Amazing World of Gumball | The Money; The Triangle
 2:00 pm | Amazing World of Gumball | The Return; The Traitor
 2:30 pm | Amazing World of Gumball | The Advice; The Nemesis
 3:00 pm | Craig of the Creek | Dog Decider Jextra Perrestrial
 3:30 pm | Craig of the Creek | The Bring out Your Beast Takeout Mission
 4:00 pm | Total DramaRama | Ant We All Just Get Along All up in Your Drill
 4:30 pm | Total DramaRama | Snow Way Out; Cone In 60 Seconds
 5:00 pm | Teen Titans Go! | Stockton; Ca! Strength of a Grown Man
 5:30 pm | Teen Titans Go! | Collect Them All Had to Be There
 6:00 pm | Apple & Onion | A New Life; Burger's Trampoline
 6:30 pm | Amazing World of Gumball | The Apprentice; The Loophole
 7:00 pm | Amazing World of Gumball | The Awkwardness; The Check
 7:30 pm | We Bare Bears | Library; The Panda 2