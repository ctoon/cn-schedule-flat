# 2020-05-31
 6:00 am | Teen Titans Go! | Super Robin; Tower Power
 6:30 am | Bakugan | Bakugan Battle League Begins; Theriot vs. Ajit
 7:00 am | Transformers Cyberverse | Rack N' Ruin N' Ratchet; Dweller in the Depths
 7:30 am | Power Players | To Tame the Perilous Skies!; Four Color Fallout
 8:00 am | Ben 10 | Heat Of The Moment; Vin Diagram
 8:30 am | Amazing World of Gumball | The Tag; The Lesson
 9:00 am | Amazing World of Gumball | The World; The Finale
 9:30 am | Amazing World of Gumball | The Kids; The Fan
10:00 am | Amazing World of Gumball | The Procrastinators; The Shell
10:30 am | Amazing World of Gumball | The Recipe; The Puppy
11:00 am | Amazing World of Gumball | The Name; The Extras
11:30 am | Amazing World of Gumball | The Gripes; The Vacation
12:00 pm | Teen Titans Go! | Parasite; Starliar
12:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 1:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 1:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 2:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 2:30 pm | Teen Titans Go! | Books; Lazy Sunday
 3:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 3:30 pm | Teen Titans Go! | No Power; Sidekick
 4:00 pm | Dc Super Hero Girls | #LivingTheNightmare;#Allaboutzee
 4:30 pm | Amazing World of Gumball | The Internet; The Plan
 5:00 pm | Amazing World of Gumball | The Limit; The Game
 5:30 pm | Amazing World of Gumball | The Promise; The Voice
 6:00 pm | Amazing World of Gumball | The Boombox; The Castle
 6:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 7:00 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | The Duke; Donny
 7:30 pm | Adventure Time | Henchman; Dungeon