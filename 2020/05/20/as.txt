# 2020-05-20
 8:00 pm | American Dad | American Stepdad
 8:30 pm | American Dad | The Adventures of Twill Ongenbone and His Boy Jabari
 9:00 pm | American Dad | My Affair Lady
 9:30 pm | Rick and Morty | M. Night Shaym-Aliens!
10:00 pm | Bob's Burgers | Seaplane!
10:30 pm | Bob's Burgers | My Big Fat Greek Bob
11:00 pm | Family Guy | Back to the Pilot
11:30 pm | Family Guy | Thanksgiving
12:00 am | Robot Chicken | Not Enough Women
12:15 am | Robot Chicken | The Angelic Sounds of Mike Giggling
12:30 am | Aqua Teen Hunger Force Forever | Brain Fairy
12:45 am | Squidbillies | Cephalo-ectomy
 1:00 am | The Boondocks | The Itis
 1:30 am | Family Guy | Back to the Pilot
 2:00 am | Family Guy | Thanksgiving
 2:30 am | Bob's Burgers | Seaplane!
 3:00 am | Bob's Burgers | My Big Fat Greek Bob
 3:30 am | Rick and Morty | M. Night Shaym-Aliens!
 4:00 am | Spooky Dreams | Spooky Dreams
 4:15 am | Star Boat | Star Boat
 4:30 am | Robot Chicken | Not Enough Women
 4:45 am | Robot Chicken | The Angelic Sounds of Mike Giggling
 5:00 am | Aqua Teen Hunger Force Forever | Brain Fairy
 5:15 am | Squidbillies | Cephalo-ectomy
 5:30 am | My Hero Academia | Cavalry Battle Finale