# 2020-05-30
 6:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 7:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:00 am | Amazing World of Gumball | The Dream; The Sidekick
10:30 am | Amazing World of Gumball | The Watch; The Bet
11:00 am | Amazing World of Gumball | The Bumpkin; The Flakers; Adventure Academy: Sluggernaut
11:30 am | Amazing World of Gumball | The Hero; The Photo
12:00 pm | Amazing World of Gumball | The Pony; The Storm
12:30 pm | Amazing World of Gumball | The Words; The Apology
 1:00 pm | Amazing World of Gumball | The Flower; The Banana
 1:30 pm | Amazing World of Gumball | The Phone; The Job
 2:00 pm | Teen Titans Go! | Waffles; Opposites
 2:30 pm | Teen Titans Go! | Birds; Be Mine
 3:00 pm | Teen Titans Go! | Ghostboy; La Larva de Amor
 3:30 pm | Teen Titans Go! | Dude Relax!; Laundry Day
 4:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 4:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 5:00 pm | Teen Titans Go! | Double Trouble; The Date
 5:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 6:00 pm | Amazing World of Gumball | The Fraud; The Void
 6:30 pm | Amazing World of Gumball | The Move; The Boss
 7:00 pm | Amazing World of Gumball | The Allergy; The Law
 7:30 pm | Amazing World of Gumball | The Mothers / The Password