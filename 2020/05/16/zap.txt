# 2020-05-16
 6:00 am | Amazing World of Gumball | The Agent
 6:15 am | Amazing World of Gumball | The Decisions
 6:30 am | Amazing World of Gumball | The Web
 6:45 am | Amazing World of Gumball | The Inquisition
 7:00 am | Teen Titans Go! | The Real Orangins!
 7:15 am | Teen Titans Go! | Business Ethics Wink Wink
 7:30 am | Teen Titans Go! | BBRBDAY
 7:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 8:00 am | Teen Titans Go! | Tall Titan Tales
 8:15 am | Teen Titans Go! | I Used to Be a Peoples
 8:30 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 8:45 am | Teen Titans Go! | The Metric System vs. Freedom
 9:00 am | Craig of the Creek | Trading Day
 9:15 am | Craig of the Creek | Crisis at Elder Rock
 9:30 am | Victor and Valentino | Kindred Spirits
 9:45 am | Victor and Valentino | Decoding Valentino
10:00 am | Total DramaRama | Total Eclipse of the Fart
10:15 am | Total DramaRama | A Dame-gerous Game
10:30 am | ThunderCats Roar | Lion-S
10:45 am | ThunderCats Roar | Snarf's Day Off
11:00 am | Teen Titans Go! | How's This for a Special? Spaaaace
11:30 am | Teen Titans Go! | The Chaff
11:45 am | Teen Titans Go! | Teen Titans Roar!
12:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
12:30 pm | Teen Titans Go! | Books; Lazy Sunday
 1:00 pm | Amazing World of Gumball | The Third; The Debt
 1:30 pm | Amazing World of Gumball | The Pressure; The Painting
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 3:30 pm | Teen Titans Go! | Power Moves; Staring at the Future
 4:00 pm | Teen Titans Go! | Teen Titans Vroom
 4:30 pm | Teen Titans Go! | Them Soviet Boys
 4:45 pm | Teen Titans Go! | Curse of the Booty Scooty
 5:00 pm | Teen Titans Go! | No Power; Sidekick
 5:30 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 6:00 pm | Amazing World of Gumball | The Mystery; The Prank
 6:30 pm | Amazing World of Gumball | The Gi; The Kiss
 7:00 pm | Amazing World of Gumball | The Party; The Refund
 7:30 pm | Amazing World of Gumball | The Robot; The Picnic