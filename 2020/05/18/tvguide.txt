# 2020-05-18
 6:00 am | Amazing World of Gumball | The Mystery; The Prank
 6:30 am | Amazing World of Gumball | The GI; The Kiss
 7:00 am | Amazing World of Gumball | The Party; The Refund
 7:30 am | Amazing World of Gumball | The Robot; The Picnic
 8:00 am | Apple & Onion | Baby Boi Tp Pulling Your Weight
 8:30 am | Apple & Onion | Not Funny Rotten Apple
 9:00 am | Teen Titans Go! | Cool School; Video Game References
 9:30 am | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
10:00 am | Craig of the Creek | Trading Day
10:15 am | Craig of the Creek | Crisis at Elder Rock
10:30 am | Craig of the Creek | Future Is Cardboard; The Jessica's Trail
11:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
11:30 am | Teen Titans Go! | Friendship; Vegetables
12:00 pm | Victor and Valentino | Kindred Spirits
12:15 pm | Victor and Valentino | Decoding Valentino
12:30 pm | Victor and Valentino | Escape From Bebe Bay el Silbon
 1:00 pm | Amazing World of Gumball | The Third; The Debt
 1:30 pm | Amazing World of Gumball | The Pressure; The Painting
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Craig Of The Creek | The Shortcut; Lost In The Sewer
 3:30 pm | Craig of the Creek | Brood; The Deep Creek Salvage
 4:00 pm | Total DramaRama | Gnome More Mister Nice Guy Mooshy Mon Mons
 4:30 pm | Total DramaRama | Look Who's Clocking Student Becomes the Teacher
 5:00 pm | Teen Titans Go! | The Best Robin; Road Trip
 5:30 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
 6:00 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 6:30 pm | Teen Titans Go! | Real Boy Adventures; Smile Bones
 7:00 pm | Amazing World of Gumball | The Sock; The Genius
 7:30 pm | We Bare Bears | Emergency; The Road