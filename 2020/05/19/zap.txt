# 2020-05-19
 6:00 am | Amazing World of Gumball | The Goons; The Secret
 6:30 am | Amazing World of Gumball | The Sock; The Genius
 7:00 am | Amazing World of Gumball | The Mustache; The Date
 7:30 am | Amazing World of Gumball | The Club; The Wand
 8:00 am | Apple & Onion | Positive Attitude Theory
 8:15 am | Apple & Onion | The Inbetween
 8:30 am | Apple & Onion | Apple's Short
 8:45 am | Apple & Onion | Election Day
 9:00 am | Teen Titans Go! | Pirates; I See You
 9:30 am | Teen Titans Go! | Brian; Nature
10:00 am | Craig of the Creek | Dog Decider
10:15 am | Craig of the Creek | Dinner at the Creek
10:30 am | Craig of the Creek | Bring Out Your Beast
10:45 am | Craig of the Creek | Bug City
11:00 am | Teen Titans Go! | The Mask; Slumber Party
11:30 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
12:00 pm | Victor and Valentino | The Great Bongo Heist
12:15 pm | Victor and Valentino | Ener-G-Shoes
12:30 pm | Victor and Valentino | Cat-Pocalypse
12:45 pm | Victor and Valentino | La Cucarachita
 1:00 pm | Amazing World of Gumball | The Mystery; The Prank
 1:30 pm | Amazing World of Gumball | The Gi; The Kiss
 2:00 pm | Amazing World of Gumball | The Party; The Refund
 2:30 pm | Amazing World of Gumball | The Robot; The Picnic
 3:00 pm | Craig of the Creek | Under the Overpass
 3:15 pm | Craig of the Creek | Dibs Court
 3:30 pm | Craig of the Creek | The Invitation
 3:45 pm | Craig of the Creek | The Great Fossil Rush
 4:00 pm | Total DramaRama | Harold Swatter and the Goblet of Flies
 4:15 pm | Total DramaRama | Venthalla
 4:30 pm | Total DramaRama | Stink. Stank. Stunk
 4:45 pm | Total DramaRama | The Date
 5:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 6:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 7:00 pm | Amazing World of Gumball | The Quest; The Spoon
 7:30 pm | We Bare Bears | Tote Life
 7:45 pm | We Bare Bears | Charlie and the Snake