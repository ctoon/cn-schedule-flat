# 2020-05-06
 8:00 pm | American Dad | Jenny Fromdabloc
 8:30 pm | American Dad | The Scarlett Getter
 9:00 pm | American Dad | Rubberneckers
 9:30 pm | Rick and Morty | The ABC's of Beth
10:00 pm | Bob's Burgers | The Deepening
10:30 pm | Bob's Burgers | Tina-Rannosaurus Wrecks
11:00 pm | Family Guy | April in Quahog
11:30 pm | Family Guy | Brian and Stewie
12:00 am | Robot Chicken | Molly Lucero in: Your Friend's Boob
12:15 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
12:30 am | Aqua Unit Patrol Squad 1 | The Creditor
12:45 am | Squidbillies | Blue Lives Battered
 1:00 am | The Boondocks | The Trial of Robert Kelly
 1:30 am | Family Guy | April in Quahog
 2:00 am | Family Guy | Brian and Stewie
 2:30 am | Bob's Burgers | The Deepening
 3:00 am | Bob's Burgers | Tina-Rannosaurus Wrecks
 3:30 am | Rick and Morty | The ABC's of Beth
 4:00 am | Infomercials | Skeleton Landlord
 4:15 am | Infomercials | When Panties Fly
 4:30 am | Robot Chicken | Molly Lucero in: Your Friend's Boob
 4:45 am | Robot Chicken | Spike Fraser in: Should I Happen to Back into a Horse
 5:00 am | Aqua Unit Patrol Squad 1 | The Creditor
 5:15 am | Squidbillies | Blue Lives Battered
 5:30 am | My Hero Academia | Bakugo's Starting Line