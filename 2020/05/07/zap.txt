# 2020-05-07
 6:00 am | Amazing World of Gumball | The Deal
 6:15 am | Amazing World of Gumball | The Founder
 6:30 am | Amazing World of Gumball | The Line
 6:45 am | Amazing World of Gumball | The Schooling
 7:00 am | Amazing World of Gumball | The Singing
 7:15 am | Amazing World of Gumball | The Intelligence
 7:30 am | Amazing World of Gumball | The Puppets
 7:45 am | Amazing World of Gumball | The Potion
 8:00 am | Apple & Onion | A New Life
 8:15 am | Apple & Onion | Not Funny
 8:30 am | Apple & Onion | Bottle Catch
 8:45 am | Apple & Onion | Positive Attitude Theory
 9:00 am | Teen Titans Go! | What's Opera, Titans?
 9:15 am | Teen Titans Go! | Communicate Openly
 9:30 am | Teen Titans Go! | Walk Away
 9:45 am | Teen Titans Go! | Record Book
10:00 am | Craig of the Creek | Sleepover at JP's
10:15 am | Craig of the Creek | Vulture's Nest
10:30 am | Craig of the Creek | The Evolution of Craig
10:45 am | Craig of the Creek | Kelsey Quest
11:00 am | Teen Titans Go! | Beast Boy's That's What's Up
12:00 pm | Victor and Valentino | Los Cadejos
12:15 pm | Victor and Valentino | Aluxes
12:30 pm | Victor and Valentino | Cuddle Monster
12:45 pm | Victor and Valentino | Balloon Boys
 1:00 pm | Amazing World of Gumball | The Nuisance
 1:15 pm | Amazing World of Gumball | The Shippening
 1:30 pm | Amazing World of Gumball | The Best
 1:45 pm | Amazing World of Gumball | The Stink
 2:00 pm | Amazing World of Gumball | The Worst
 2:15 pm | Amazing World of Gumball | The Awareness
 2:30 pm | Amazing World of Gumball | The List
 2:45 pm | Amazing World of Gumball | The Parents
 3:00 pm | Craig of the Creek | The Cardboard Identity
 3:15 pm | Craig of the Creek | Ancients of the Creek
 3:30 pm | Craig of the Creek | Camper on the Run
 3:45 pm | Craig of the Creek | Bring Out Your Beast
 4:00 pm | Total DramaRama | Beth and the Beanstalk
 4:15 pm | Total DramaRama | Pinata Regatta
 4:30 pm | Total DramaRama | Camping Is in Tents
 5:00 pm | Teen Titans Go! | TV Knight 4
 5:15 pm | Teen Titans Go! | Teen Titans Roar!
 5:30 pm | Teen Titans Go! | Teen Titans Vroom
 6:00 pm | Teen Titans Go! | Super Hero Summer Camp
 7:00 pm | Amazing World of Gumball | The Console
 7:15 pm | Amazing World of Gumball | The Sucker
 7:30 pm | We Bare Bears | Food Truck
 7:45 pm | We Bare Bears | Chloe