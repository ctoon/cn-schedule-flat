# 2020-05-08
 6:00 am | Amazing World of Gumball | The Lady; The Sorcerer
 6:30 am | Amazing World of Gumball | The Console; The Sucker
 7:00 am | Amazing World of Gumball | The Outside; The Cage
 7:30 am | Amazing World of Gumball | The Copycats; The Rival
 8:00 am | Apple & Onion | Pancake's Bus Tour ;Apple's Short
 8:30 am | Apple & Onion | Face Your Fears; Block Party
 9:00 am | Teen Titans Go! | Magic Man; Rain on Your Wedding Day
 9:30 am | Teen Titans Go! | Titans Go Casual Leg Day
10:00 am | Craig of the Creek | Tea Timer's Ball ;Jpony
10:30 am | Craig of the Creek | You're It; Ace of Squares
11:00 am | Total DramaRama | Melter Skelter; Weiner Takes All
11:30 am | Total DramaRama | Never Gwending Story; The Apoca-Lice Now
12:00 pm | Victor And Valentino | Boss For a day; Lonely Haunts Club 2: Doll Island
12:30 pm | Victor And Valentino | Guillermo's Gathering; A New Don
 1:00 pm | Amazing World of Gumball | The Deal; The Founder
 1:30 pm | Amazing World of Gumball | The Line; The Schooling
 2:00 pm | Amazing World of Gumball | The Singing; The Intelligence
 2:30 pm | Amazing World of Gumball | The Puppets; The Potion
 3:00 pm | Craig of the Creek | Kelsey the Author ;Lost in the Sewer
 3:30 pm | Craig of the Creek | Cousin of the Creek;The Brood
 4:00 pm | Total DramaRama | All Up In Your Drill; Snow Way Out
 4:30 pm | Total DramaRama | Toys Will Be Toys ;Mutt Ado About Owen
 5:00 pm | Teen Titans Go! | The Royal Jelly; Great Disaster
 5:30 pm | Teen Titans Go! | The Strength of a Grown Man ;Viewers Decide
 6:00 pm | Teen Titans Go! | Had to Be There; TV Knight 5
 6:30 pm | Teen Titans Go! | Girls Night In
 7:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 7:30 pm | Amazing World of Gumball | The Catfish; The One