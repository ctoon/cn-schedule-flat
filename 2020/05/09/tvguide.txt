# 2020-05-09
 6:00 am | Amazing World of Gumball | The Singing; The Schooling
 6:30 am | Amazing World of Gumball | The Puppets; The Intelligence
 7:00 am | Teen Titans Go! | Bro-pocalypse; The Scoop
 7:30 am | Teen Titans Go! | Flashback
 8:00 am | Teen Titans Go! | Kabooms
 8:30 am | Teen Titans Go! | Magic Man ; The Titans Go Casual
 9:00 am | Craig of the Creek | Mortimor to the Rescue
 9:15 am | Craig of the Creek | Secret in a Bottle
 9:30 am | Victor and Valentino | I......Am Vampier
 9:45 am | Victor and Valentino | Victor the Predictor
10:00 am | Total DramaRama | A Dame-Gerous Game
10:15 am | Total DramaRama | Royal Flush
10:30 am | ThunderCats Roar! | Ratar-O
10:45 am | ThunderCats Roar! | Prince Starling's Quest
10:55 am | Scoob! Sneak Peek: Adventure Calling | 
11:00 am | Teen Titans Go! | Tower Renovation; Quantum Fun
11:30 am | Teen Titans Go! | Genie President; The Fight
12:00 pm | Teen Titans Go! | Girl's Night Out; You're Fired
12:30 pm | Teen Titans Go! | Super Robin; Tower Power
 1:00 pm | Amazing World of Gumball | The Lady; The Potion
 1:30 pm | Amazing World of Gumball | The Sucker; The Spinoffs
 2:00 pm | Amazing World of Gumball | The Cage; The Transformation
 2:30 pm | Amazing World of Gumball | The Rival; The Understanding
 3:00 pm | Teen Titans Go! | Parasite; Starliar
 3:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 4:00 pm | Teen Titans Go! | Little Elvis; The Groover
 4:30 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 5:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 5:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 6:00 pm | Amazing World of Gumball | The One; The Ad
 6:30 pm | Amazing World Of Gumball | The Slip; The Vegging
 7:00 pm | Amazing World of Gumball | The Father; The Drama
 7:30 pm | Amazing World of Gumball | The Cringe; The Master