# 2020-05-09
 6:00 am | Amazing World of Gumball | The Singing
 6:15 am | Amazing World of Gumball | The Schooling
 6:30 am | Amazing World of Gumball | The Puppets
 6:45 am | Amazing World of Gumball | The Intelligence
 7:00 am | Teen Titans Go! | Bro-Pocalypse
 7:15 am | Teen Titans Go! | The Scoop
 7:30 am | Teen Titans Go! | Flashback
 8:00 am | Teen Titans Go! | Kabooms
 8:30 am | Teen Titans Go! | Magic Man
 8:45 am | Teen Titans Go! | The Titans Go Casual
 9:00 am | Craig of the Creek | Mortimor to the Rescue
 9:15 am | Craig of the Creek | Secret in a Bottle
 9:30 am | Victor and Valentino | I... am Vampier
 9:45 am | Victor and Valentino | Victor the Predictor
10:00 am | Total DramaRama | A Dame-gerous Game
10:15 am | Total DramaRama | Royal Flush
10:30 am | ThunderCats Roar | Ratar-O
10:45 am | ThunderCats Roar | Prince Starling's Quest
10:55 am | Scoob! Sneak Peek: Adventure Calling | 
11:00 am | Teen Titans Go! | Tower Renovation
11:15 am | Teen Titans Go! | Quantum Fun
11:30 am | Teen Titans Go! | The Fight
11:45 am | Teen Titans Go! | Genie President
12:00 pm | Teen Titans Go! | Girls' Night Out; You're Fired
12:30 pm | Teen Titans Go! | Super Robin; Tower Power
 1:00 pm | Amazing World of Gumball | The Lady
 1:15 pm | Amazing World of Gumball | The Potion
 1:30 pm | Amazing World of Gumball | The Sucker
 1:45 pm | Amazing World of Gumball | The Spinoffs
 2:00 pm | Amazing World of Gumball | The Cage
 2:15 pm | Amazing World of Gumball | The Transformation
 2:30 pm | Amazing World of Gumball | The Rival
 2:45 pm | Amazing World of Gumball | The Understanding
 3:00 pm | Teen Titans Go! | Parasite; Starliar
 3:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 4:00 pm | Teen Titans Go! | Little Elvis
 4:15 pm | Teen Titans Go! | The Groover
 4:30 pm | Teen Titans Go! | The Power of Shrimps
 4:45 pm | Teen Titans Go! | My Name Is Jose
 5:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 5:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 6:00 pm | Amazing World of Gumball | The One
 6:15 pm | Amazing World of Gumball | The Ad
 6:30 pm | Amazing World of Gumball | The Vegging
 6:45 pm | Amazing World of Gumball | The Slip
 7:00 pm | Amazing World of Gumball | The Father
 7:15 pm | Amazing World of Gumball | The Drama
 7:30 pm | Amazing World of Gumball | The Cringe
 7:45 pm | Amazing World of Gumball | The Master