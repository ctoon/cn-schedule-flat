# 2020-05-27
 8:00 pm | American Dad | For Black Eyes Only
 8:30 pm | American Dad | Steve and Snot's Test-Tubular Adventure
 9:00 pm | American Dad | Dreaming of a White Porsche Christmas
 9:30 pm | Rick and Morty | Rixty Minutes
10:00 pm | Bob's Burgers | Mazel Tina
10:30 pm | Bob's Burgers | Uncle Teddy
11:00 pm | Family Guy | You Can't Do That on Television, Peter
11:30 pm | Family Guy | Mr. and Mrs. Stewie
12:00 am | Relaxing Old Footage with Joe Pera | Relaxing Old Footage With Joe Pera
12:30 am | Aqua Teen Hunger Force Forever | Rabbit, not Rabbot
12:45 am | Squidbillies | Tortuga de Mentiras
 1:00 am | The Boondocks | The Block Is Hot
 1:30 am | Family Guy | You Can't Do That on Television, Peter
 2:00 am | Family Guy | Mr. and Mrs. Stewie
 2:30 am | Bob's Burgers | Mazel Tina
 3:00 am | Bob's Burgers | Uncle Teddy
 3:30 am | Rick and Morty | Rixty Minutes
 4:00 am | Brad Neely's Harg Nallin' Sclopio Peepio | For Sarandon
 4:15 am | China, IL | Chinese New Year
 4:30 am | Robot Chicken | Mr. Mozzarella's Hamburger Skateboard Depot
 4:45 am | Robot Chicken | Strummy Strummy Sad Sad
 5:00 am | Aqua Teen Hunger Force Forever | Rabbit, not Rabbot
 5:15 am | Squidbillies | Tortuga de Mentiras
 5:30 am | My Hero Academia | Shoto Todoroki: Origin