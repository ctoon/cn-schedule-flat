# 2020-05-01
 6:00 am | Amazing World of Gumball | The Boss; The Move
 6:30 am | Amazing World of Gumball | The Law; The Allergy
 7:00 am | Amazing World of Gumball | The Boredom
 7:15 am | Amazing World of Gumball | The Ollie
 7:30 am | Amazing World of Gumball | The Vision
 7:45 am | Amazing World of Gumball | The Potato
 8:00 am | Apple & Onion | Heatwave
 8:15 am | Apple & Onion | Gyranoid of the Future
 8:30 am | Apple & Onion | Rotten Apple
 8:45 am | Apple & Onion | Apple's in Charge
 9:00 am | Teen Titans Go! | Curse of the Booty Scooty
 9:15 am | Teen Titans Go! | Cartoon Feud
 9:30 am | Teen Titans Go! | Them Soviet Boys
 9:45 am | Teen Titans Go! | Don't Be an Icarus
10:00 am | Craig of the Creek | Fort Williams
10:15 am | Craig of the Creek | The Final Book
10:30 am | Craig of the Creek | Kelsey the Elder
10:45 am | Craig of the Creek | Wildernessa
11:00 am | Total DramaRama | Cuttin' Corners
11:15 am | Total DramaRama | A Ninjustice to Harold
11:30 am | Total DramaRama | Sharing Is Caring
11:45 am | Total DramaRama | Having the Timeout of Our Lives
12:00 pm | Victor and Valentino | The Babysitter
12:15 pm | Victor and Valentino | Cuddle Monster
12:30 pm | Victor and Valentino | Hurricane Chata
12:45 pm | Victor and Valentino | Boss for a Day
 1:00 pm | Amazing World of Gumball | The Society; The Spoiler
 1:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:00 pm | Amazing World of Gumball | The Downer; The Egg
 2:30 pm | Amazing World of Gumball | The Triangle; The Money
 3:00 pm | Total DramaRama | Venthalla
 3:15 pm | Total DramaRama | Ant We All Just Get Along
 3:30 pm | Total DramaRama | Duck Duck Juice
 3:45 pm | Total DramaRama | Apoca-lice Now
 4:00 pm | Teen Titans Go! | Flashback
 4:30 pm | Teen Titans Go! | Kabooms
 5:00 pm | Craig of the Creek | Summer Wish
 5:15 pm | Craig of the Creek | The Future Is Cardboard
 5:30 pm | Craig of the Creek | Turning the Tables
 5:45 pm | Craig of the Creek | Dog Decider
 6:00 pm | Teen Titans Go! | The Real Orangins!
 6:15 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 pm | Apple & Onion | Whale Spotting
 7:15 pm | Apple & Onion | Falafel's in Jail
 7:30 pm | Amazing World of Gumball | The Gripes; The Vacation