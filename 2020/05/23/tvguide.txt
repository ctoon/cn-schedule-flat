# 2020-05-23
 6:00 am | Amazing World of Gumball | The Knights; The Colossus
 6:30 am | Amazing World of Gumball | The Fridge; The Remote
 7:00 am | Teen Titans Go! | Beast Boy's That's What's Up
 8:00 am | Teen Titans Go! | Walk Away; Record Book
 8:30 am | Teen Titans Go! | Bat Scouts Titans Go Casual
 9:00 am | Craig of the Creek | Kelsey the Worthy
 9:15 am | Craig of the Creek | The End Was Here
 9:30 am | Victor and Valentino | Journey to Maiz Mountains, Part 1
 9:45 am | Victor and Valentino | Journey to Maiz Mountains, Part 2
10:00 am | Total DramaRama | Dissing Cousins
10:30 am | Thundercats Roar! | Mumm-Ra of Plun-Darr
11:00 am | Teen Titans Go! Vs. Teen Titans | 
12:45 pm | Teen Titans Go! | Magic Man
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 2:00 pm | Teen Titans Go! | We're off to Get Awards Rain on Your Wedding Day
 2:30 pm | Teen Titans Go! | Demon Prom; Ones And Zeros
 3:00 pm | Teen Titans Go! | Career Day; Bbcyfshipbday
 3:30 pm | Teen Titans Go! | Tv Knight 2; Mo' Money Mo' Problems
 4:00 pm | Teen Titans Go! | The Academy; Beast Girl
 4:30 pm | Teen Titans Go! | Throne Of Bones; Tv Knight 3
 5:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:00 pm | Teen Titans Go! | Bro-pocalypse; The Scoop
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Teen Titans Go! | Kabooms
 7:30 pm | Teen Titans Go! | Chicken In The Cradle; Tower Renovation