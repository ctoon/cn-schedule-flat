# 2020-05-23
 8:00 pm | Dragon Ball Z Kai | Power Unknown! Android 16 Breaks His Silence!
 8:30 pm | Dragon Ball Z Kai | Tien's Desperate Attack! Save Your Friends, Goku!
 9:00 pm | American Dad | The Missing Kink
 9:30 pm | American Dad | Max Jets
10:00 pm | Bob's Burgers | Christmas in the Car
10:30 pm | Rick and Morty | The Vat of Acid Episode
11:00 pm | Family Guy | The Blind Side
11:30 pm | Family Guy | Livin' on a Prayer
12:00 am | My Hero Academia | Deku vs. Gentle Criminal
12:30 am | Paranoia Agent | The Holy Warrior
 1:00 am | Mob Psycho 100 | An Invite to a Meeting ~Simply Put, I Just Want to Be Popular~
 1:30 am | Black Clover | A Reunion Across Time and Space
 2:00 am | JoJo's Bizarre Adventure: Golden Wind | Emperor Crimson vs. Metallic
 2:30 am | Naruto:Shippuden | A Father's Hope, A Mother's Love
 3:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 3:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 3:30 am | The Shivering Truth | Carrion My Son
 3:45 am | The Shivering Truth | The Nurple Rainbow
 4:00 am | The Boondocks | Granddad Dates a Kardashian
 4:30 am | The Venture Brothers | A Party for Tarzan
 5:00 am | Aqua Unit Patrol Squad 1 | Last Dance for Napkin Lad
 5:15 am | Aqua Something You Know Whatever | Big Bro
 5:30 am | Bob's Burgers | Christmas in the Car