# 2020-05-21
 6:00 am | Amazing World of Gumball | The Helmet; The Fight
 6:30 am | Amazing World of Gumball | The End; The DVD
 7:00 am | Amazing World of Gumball | The Knights; The Colossus
 7:30 am | Amazing World of Gumball | The Fridge; The Remote
 8:00 am | Apple & Onion | Sausage and Sweetie Smash 4 on 1
 8:30 am | Apple & Onion | Selfish Shellfish Apple's in Trouble
 9:00 am | Teen Titans Go! | The Best Robin; Road Trip
 9:30 am | Teen Titans Go! | Hot Garbage; Mouth Hole
10:00 am | Craig of the Creek | Under the Overpass Dibs Court
10:30 am | Craig of the Creek | The Invitation; The Great Fossil Rush
11:00 am | Teen Titans Go! | Crazy Day; Robin Backwards
11:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
12:00 pm | Victor and Valentino | Forever Ever Chata's Quinta Quinceaera
12:30 pm | Victor and Valentino | Tree Buds Brotherly Love
 1:00 pm | Amazing World of Gumball | The Ape; The Poltergeist
 1:30 pm | Amazing World of Gumball | The Quest; The Spoon
 2:00 pm | Amazing World of Gumball | The Car; The Curse
 2:30 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:00 pm | Craig of the Creek | Trading Day
 3:15 pm | Craig of the Creek | Crisis at Elder Rock
 3:30 pm | Craig of the Creek | Future Is Cardboard; The Jessica's Trail
 4:00 pm | Total DramaRama | Total Eclipse of the Fart
 4:15 pm | Total DramaRama | A Dame-Gerous Game
 4:30 pm | Total DramaRama | Driving Miss Crazy Exercising the Demons
 5:00 pm | Teen Titans Go! | Pirates; I See You
 5:30 pm | Teen Titans Go! | Brian; Nature
 6:00 pm | Teen Titans Go! | Teen Titans Roar! Don't Be an Icarus
 6:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 7:00 pm | Amazing World of Gumball | The Pressure; The Painting
 7:30 pm | We Bare Bears | Chloe and Ice Bear; Cupcake Job