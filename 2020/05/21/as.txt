# 2020-05-21
 8:00 pm | American Dad | Naked to the Limit, One More Time
 8:30 pm | American Dad | Spelling Bee My Baby
 9:00 pm | American Dad | Big Stan on Campus
 9:30 pm | Rick and Morty | Meeseeks and Destroy
10:00 pm | Bob's Burgers | Turkey in a Can
10:30 pm | Bob's Burgers | Purple Rain-Union
11:00 pm | Family Guy | Amish Guy
11:30 pm | Family Guy | Cool Hand Peter
12:00 am | The Shivering Truth | Carrion My Son
12:15 am | JJ Villard's Fairy Tales | The Goldilox Massacre
12:30 am | Aqua Unit Patrol Squad 1 | Jumpy George
12:45 am | Squidbillies | Greener Pastor
 1:00 am | The Boondocks | Let's Nab Oprah
 1:30 am | Family Guy | Amish Guy
 2:00 am | Family Guy | Cool Hand Peter
 2:30 am | Bob's Burgers | Turkey in a Can
 3:00 am | Bob's Burgers | Purple Rain-Union
 3:30 am | Rick and Morty | Meeseeks and Destroy
 4:00 am | Spooky Dreams | Spooky Dreams
 4:15 am | Space Man | Space Man
 4:30 am | The Shivering Truth | Carrion My Son
 4:45 am | JJ Villard's Fairy Tales | The Goldilox Massacre
 5:00 am | Aqua Unit Patrol Squad 1 | Jumpy George
 5:15 am | Squidbillies | Greener Pastor
 5:30 am | My Hero Academia | The Boy Born with Everything