# 2020-05-28
 6:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
 6:30 am | Amazing World of Gumball | The Pony; The Storm
 7:00 am | Amazing World of Gumball | The Dream; The Sidekick
 7:30 am | Amazing World of Gumball | The Hero; The Photo
 8:00 am | Apple & Onion | Dragonhead
 8:15 am | Apple & Onion | Apple's Focus
 8:30 am | Teen Titans Go! | Double Trouble; The Date
 9:00 am | Teen Titans Go! | The Spice Game
 9:15 am | Teen Titans Go! | Bottle Episode
 9:30 am | Teen Titans Go! | I'm the Sauce
 9:45 am | Teen Titans Go! | Pyramid Scheme
10:00 am | Craig of the Creek | Big Pinchy
10:15 am | Craig of the Creek | Kelsey the Elder
10:30 am | Victor and Valentino | It Grows
10:45 am | Victor and Valentino | The Dark Room
11:00 am | Teen Titans Go! | The Mask; Slumber Party
11:30 am | Teen Titans Go! | Cartoon Feud
11:45 am | Teen Titans Go! | Collect Them All
12:00 pm | Teen Titans Go! | The Dignity of Teeth
12:15 pm | Teen Titans Go! | Garage Sale
12:30 pm | Mao Mao: Heroes of Pure Heart | Baost in Show
12:45 pm | Mao Mao: Heroes of Pure Heart | Orangusnake Begins
 1:00 pm | Amazing World of Gumball | The Tag; The Lesson
 1:30 pm | Amazing World of Gumball | The Limit; The Game
 2:00 pm | Amazing World of Gumball | The Promise; The Voice
 2:30 pm | Amazing World of Gumball | The Boombox; The Castle
 3:00 pm | Craig of the Creek | Kelsey the Worthy
 3:15 pm | Craig of the Creek | The End Was Here
 3:30 pm | Craig of the Creek | Creek Cart Racers
 3:45 pm | Craig of the Creek | Sparkle Cadet
 4:00 pm | Total DramaRama | Dissing Cousins
 4:30 pm | Total DramaRama | Lie-Ranosaurus Wrecked
 4:45 pm | Total DramaRama | Free Chili
 5:00 pm | Teen Titans Go! | Cat's Fancy
 5:15 pm | Teen Titans Go! | BBBDay!
 5:30 pm | Teen Titans Go! | Leg Day
 5:45 pm | Teen Titans Go! | Squash & Stretch
 6:00 pm | Apple & Onion | Pulling Your Weight
 6:15 pm | Apple & Onion | Burger's Trampoline
 6:30 pm | Amazing World of Gumball | The Flower; The Banana
 7:00 pm | Amazing World of Gumball | The Phone; The Job
 7:30 pm | We Bare Bears | Ranger Tabes
 7:45 pm | We Bare Bears | Rooms