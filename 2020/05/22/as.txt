# 2020-05-22
 8:00 pm | American Dad | The Boring Identity
 8:30 pm | American Dad | Blood Crieth Unto Heaven
 9:00 pm | American Dad | Scents and Sensei-bility
 9:30 pm | Rick and Morty | The Vat of Acid Episode
10:00 pm | Rick and Morty | Auto Erotic Assimilation
10:30 pm | Bob's Burgers | Bob and Deliver
11:00 pm | Family Guy | Grumpy Old Man
11:30 pm | Family Guy | Quagmire and Meg
12:00 am | Relaxing Old Footage with Joe Pera | Relaxing Old Footage With Joe Pera
12:30 am | Joe Pera Helps Find You The Perfect Christmas Tree | Joe Pera Helps You Find the Perfect Christmas Tree
 1:00 am | Joe Pera Talks With You | Joe Pera Takes You to Breakfast
 1:15 am | Joe Pera Talks With You | Joe Pera Takes You on a Fall Drive
 1:30 am | Joe Pera Talks With You | Joe Pera Talks You Back to Sleep
 1:45 am | Joe Pera Talks With You | Joe Pera Reads You The Church Announcements
 2:00 am | Joe Pera Talks With You | Joe Pera Lights Up the Night With You
 2:15 am | Joe Pera Talks With You | Joe Pera Answers Your Questions About Cold Weather Sports
 2:45 am | Joe Pera Talks With You | Joe Pera Talks With You About Beans
 3:00 am | Joe Pera Talks With You | Joe Pera Waits With You
 3:15 am | Joe Pera Talks With You | Joe Pera Takes You to the Grocery Store
 3:30 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 3:45 am | Infomercials | Joe Pera Talks You to Sleep
 4:00 am | As Seen on Adult Swim | As Seen on Adult Swim
 4:15 am | Quarantine Valentine | Quarantine Valentine
 4:30 am | Rick and Morty | The Vat of Acid Episode
 5:00 am | Family Guy | Quagmire and Meg
 5:30 am | My Hero Academia | Victory or Defeat