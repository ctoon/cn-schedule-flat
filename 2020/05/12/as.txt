# 2020-05-12
 8:00 pm | American Dad | Hot Water
 8:30 pm | American Dad | The Wrestler
 9:00 pm | American Dad | Permanent Record Wrecker
 9:30 pm | Rick and Morty | The Rickchurian Mortydate
10:00 pm | Bob's Burgers | Lindapendent Woman
10:30 pm | Bob's Burgers | O.T. the Outside Toilet
11:00 pm | Family Guy | Road to the North Pole Part 1 & 2
12:00 am | Robot Chicken | Fridge Smell
12:15 am | Robot Chicken | Western Hay Batch
12:30 am | Aqua TV Show Show | Freda
12:45 am | Squidbillies | Debased Ball
 1:00 am | The Boondocks | A Date with the Health Inspector
 1:30 am | Family Guy | Road to the North Pole Part 1 & 2
 2:30 am | Bob's Burgers | Lindapendent Woman
 3:00 am | Bob's Burgers | O.T. the Outside Toilet
 3:30 am | Rick and Morty | The Rickchurian Mortydate
 4:00 am | Brad Neely's Harg Nallin' Sclopio Peepio | For Knowles
 4:15 am | China, IL | Dean vs. Mayor
 4:30 am | Robot Chicken | Fridge Smell
 4:45 am | Robot Chicken | Western Hay Batch
 5:00 am | Aqua TV Show Show | Freda
 5:15 am | Squidbillies | Debased Ball
 5:30 am | My Hero Academia | All Might