# 2020-10-20
 6:00 am | Amazing World of Gumball | The Name; The Extras
 6:30 am | Amazing World of Gumball | The Gripes; The Vacation
 7:00 am | Amazing World of Gumball | The Fraud; The Void
 7:30 am | Amazing World of Gumball | The Move; The Boss
 8:00 am | Victor and Valentino | Villainy in Monte Macabre
 8:15 am | Victor and Valentino | Starry Night
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Teen Titans Go! | Pirates; I See You
 9:30 am | Teen Titans Go! | Brian; Nature
10:00 am | Craig of the Creek | Kelsey Quest Memories of Bobby
10:30 am | Craig of the Creek | Jacob Of The Creek; Jpony
11:00 am | Teen Titans Go! | Knowledge; Salty Codgers
11:30 am | Teen Titans Go! | The Mask; Slumber Party
12:00 pm | Teen Titans Go! | The Halloween vs. Christmas Cruel Giggling Ghoul
12:30 pm | Teen Titans Go! | Teen Titans Vroom
 1:00 pm | Total DramaRama | Cluckwork Orange; Duncan Disorderly
 1:30 pm | Total DramaRama | Aquarium for a Dream; Soother or Later
 2:00 pm | Amazing World of Gumball | The Man; The Bros
 2:30 pm | Amazing World of Gumball | The Lie; The Pizza
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Oracle; The Safety
 4:00 pm | Craig of the Creek | Doorway to Helen Fort Williams
 4:30 pm | Craig of the Creek | Climb; The Kelsey the Elder
 5:00 pm | Total DramaRama | Fire in the Hole; The Never Gwending Story
 5:30 pm | Total DramaRama | Venthalla Too Much of a Goo'd Thing
 6:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 6:30 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 7:00 pm | Craig of the Creek | Fall Anthology
 7:15 pm | Craig of the Creek | Plush Kingdom
 7:30 pm | Victor and Valentino | Charlene Mania
 7:45 pm | Victor and Valentino | Fueygo Fest
 8:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 8:30 pm | Amazing World of Gumball | The Kids; The Fan