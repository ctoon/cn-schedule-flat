# 2020-10-20
 9:00 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 9:30 pm | Bob's Burgers | Aquaticism
10:00 pm | American Dad | Casino Normale
10:30 pm | American Dad | Bazooka Steve
11:00 pm | Family Guy | It's a Trap Part 1 & 2
12:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
12:30 am | Eric Andre Show | The Hannibal Buress Show
12:45 am | Eric Andre Show | Wiz Khalifa; Aubrey Peeples
 1:00 am | Aqua Something You Know Whatever | Buddy Nugget
 1:15 am | Aqua Something You Know Whatever | Zucotti Manicotti
 1:30 am | Family Guy | It's a Trap Part 1 & 2
 2:30 am | American Dad | Casino Normale
 3:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
 3:30 am | Eric Andre Show | The Hannibal Buress Show
 3:45 am | Eric Andre Show | Wiz Khalifa; Aubrey Peeples
 4:00 am | Black Jesus | The Compton Carter
 4:30 am | Aqua Something You Know Whatever | Buddy Nugget
 4:45 am | Aqua Something You Know Whatever | Zucotti Manicotti
 5:00 am | Bob's Burgers | The Grand Mama-Pest Hotel
 5:30 am | Bob's Burgers | Aquaticism