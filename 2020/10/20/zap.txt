# 2020-10-20
 6:00 am | Amazing World of Gumball | The Name; The Extras
 6:30 am | Amazing World of Gumball | The Gripes; The Vacation
 7:00 am | Amazing World of Gumball | The Fraud; The Void
 7:30 am | Amazing World of Gumball | The Boss; The Move
 8:00 am | Victor and Valentino | Villainy In Monte Macabre
 8:15 am | Victor and Valentino | Starry Night
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Teen Titans Go! | Pirates; I See You
 9:30 am | Teen Titans Go! | Brian; Nature
10:00 am | Craig of the Creek | Kelsey Quest
10:15 am | Craig of the Creek | Memories of Bobby
10:30 am | Craig of the Creek | JPony
10:45 am | Craig of the Creek | Jacob of the Creek
11:00 am | Teen Titans Go! | Salty Codgers; Knowledge
11:30 am | Teen Titans Go! | The Mask; Slumber Party
12:00 pm | Teen Titans Go! | Halloween vs. Christmas
12:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
12:30 pm | Teen Titans Go! | Teen Titans Vroom
 1:00 pm | Total DramaRama | Cluckwork Orange
 1:15 pm | Total DramaRama | Duncan Disorderly
 1:30 pm | Total DramaRama | Aquarium for a Dream
 1:45 pm | Total DramaRama | Soother or Later
 2:00 pm | Amazing World of Gumball | The Bros; The Man
 2:30 pm | Amazing World of Gumball | The Pizza; The Lie
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Oracle; The Safety
 4:00 pm | Craig of the Creek | Doorway to Helen
 4:15 pm | Craig of the Creek | Fort Williams
 4:30 pm | Craig of the Creek | The Climb
 4:45 pm | Craig of the Creek | Kelsey the Elder
 5:00 pm | Total DramaRama | Fire in the Hole
 5:15 pm | Total DramaRama | The Never Gwending Story
 5:30 pm | Total DramaRama | Venthalla
 5:45 pm | Total DramaRama | Too Much of a Goo'd Thing
 6:00 pm | Teen Titans Go! | Mouth Hole
 6:15 pm | Teen Titans Go! | Hot Garbage
 6:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 7:00 pm | Craig of the Creek | Fall Anthology
 7:15 pm | Craig of the Creek | Plush Kingdom
 7:30 pm | Victor and Valentino | Charlene Mania
 7:45 pm | Victor and Valentino | Fueygo Fest
 8:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 8:30 pm | Amazing World of Gumball | The Kids; The Fan