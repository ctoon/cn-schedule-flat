# 2020-10-21
 6:00 am | Amazing World of Gumball | The Law; The Allergy
 6:30 am | Amazing World of Gumball | The Mothers; The Password
 7:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 7:30 am | Amazing World of Gumball | The Mirror; The Burden
 8:00 am | Victor and Valentino | Charlene Mania
 8:15 am | Victor and Valentino | Fueygo Fest
 8:30 am | Teen Titans Go! | Caramel Apples; Halloween
 9:00 am | Teen Titans Go! | Love Monsters; Baby Hands
 9:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
10:00 am | Craig of the Creek | Ace of Squares
10:15 am | Craig of the Creek | The Mystery of the Timekeeper
10:30 am | Craig of the Creek | Big Pinchy
10:45 am | Craig of the Creek | Return of the Honeysuckle Rangers
11:00 am | Teen Titans Go! | Friendship; Vegetables
11:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
12:00 pm | Teen Titans Go! | Cat's Fancy
12:15 pm | Teen Titans Go! | Leg Day
12:30 pm | Teen Titans Go! | TV Knight 4
12:45 pm | Teen Titans Go! | Teen Titans Roar!
 1:00 pm | Total DramaRama | Free Chili
 1:15 pm | Total DramaRama | Mutt Ado About Owen
 1:30 pm | Total DramaRama | Cuttin' Corners
 1:45 pm | Total DramaRama | Simons Are Forever
 2:00 pm | Amazing World of Gumball | The Downer; The Egg
 2:30 pm | Amazing World of Gumball | The Society; The Spoiler
 3:00 pm | Amazing World of Gumball | Halloween; The Treasure
 3:30 pm | Amazing World of Gumball | The Triangle; The Money
 4:00 pm | Craig of the Creek | The Kid From 3030
 4:15 pm | Craig of the Creek | Sour Candy Trials
 4:30 pm | Craig of the Creek | Power Punchers
 4:45 pm | Craig of the Creek | Council of the Creek
 5:00 pm | Total DramaRama | The Date
 5:15 pm | Total DramaRama | The Price of Advice
 5:30 pm | Total DramaRama | Duck Duck Juice
 5:45 pm | Total DramaRama | There Are No Hoppy Endings
 6:00 pm | Teen Titans Go! | Rocks and Water
 6:15 pm | Teen Titans Go! | Multiple Trick Pony
 6:30 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 7:00 pm | Craig of the Creek | Afterschool Snackdown
 7:15 pm | Craig of the Creek | Fall Anthology
 7:30 pm | Victor and Valentino | Old Man Teo
 7:45 pm | Victor and Valentino | Folk Art Friends
 8:00 pm | Amazing World of Gumball | The Name; The Extras
 8:30 pm | Amazing World of Gumball | The Gripes; The Vacation