# 2020-10-03
 6:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 6:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 7:00 am | Teen Titans Go! | Waffles; Opposites
 7:30 am | Teen Titans Go! | Birds; Be Mine
 8:00 am | Teen Titans Go! | Brain Food; In and Out
 8:30 am | Teen Titans Go! | Little Buddies; Missing
 9:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 9:30 am | Teen Titans Go! | Dreams; Grandma Voice
 9:45 am | Teen Titans Go! | Record Book
10:00 am | Craig of the Creek | Wildernessa
10:15 am | Craig of the Creek | The Future Is Cardboard
10:30 am | Craig of the Creek | Sunday Clothes
10:45 am | Craig of the Creek | Dog Decider
11:00 am | Amazing World of Gumball | The Helmet; The Fight
11:30 am | Amazing World of Gumball | The End; The DVD
12:00 pm | Amazing World of Gumball | The Knights; The Colossus
12:30 pm | Amazing World of Gumball | The Fridge; The Remote
 1:00 pm | Amazing World of Gumball | The Puppets
 1:15 pm | Amazing World of Gumball | The Vegging
 1:30 pm | Amazing World of Gumball | The Lady
 1:45 pm | Amazing World of Gumball | The Father
 2:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 3:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 3:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 4:00 pm | Teen Titans Go! | Pirates; I See You
 4:30 pm | Teen Titans Go! | Brian; Nature
 4:45 pm | Teen Titans Go! | Brain Percentages
 5:00 pm | Total DramaRama | Soother or Later
 5:15 pm | Total DramaRama | Mutt Ado About Owen
 5:30 pm | Total DramaRama | Simons Are Forever
 5:45 pm | Total DramaRama | Mother of All Cards
 6:00 pm | Amazing World of Gumball | The Rival
 6:15 pm | Amazing World of Gumball | The Pact
 6:30 pm | Amazing World of Gumball | The One
 6:45 pm | Amazing World of Gumball | The Candidate
 7:00 pm | Amazing World of Gumball | The Anybody
 7:15 pm | Amazing World of Gumball | The Shippening
 7:30 pm | Amazing World of Gumball | The Stink
 7:45 pm | Amazing World of Gumball | The Silence
 8:00 pm | Apple & Onion | The Perfect Team
 8:15 pm | Apple & Onion | Falafel's Fun Day
 8:30 pm | Apple & Onion | 4 on 1
 8:45 pm | Apple & Onion | Apple's in Trouble