# 2020-10-03
 6:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 6:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 7:00 am | Teen Titans Go! | Waffles; Opposites
 7:30 am | Teen Titans Go! | Birds; Be Mine
 8:00 am | Teen Titans Go! | Brain Food; In and Out
 8:30 am | Teen Titans Go! | Little Buddies; Missing
 9:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 9:30 am | Teen Titans Go! | Dreams; Grandma Voice
10:00 am | Craig of the Creek | The Future Is Cardboard; Wildernessa
10:30 am | Victor And Valentino | Cat-pocalypse; Band For Life
11:00 am | Amazing World of Gumball | The Helmet; The Fight
11:30 am | Amazing World of Gumball | The End; The DVD
12:00 pm | Amazing World of Gumball | The Knights; The Colossus
12:30 pm | Amazing World of Gumball | The Fridge; The Remote
 1:00 pm | Amazing World of Gumball | The Puppets; The Vegging
 1:30 pm | Amazing World of Gumball | The Lady; The Father
 2:00 pm | Amazing World of Gumball | The Sucker; The Cringe
 2:30 pm | Amazing World of Gumball | The Cage; The Neighbor
 3:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 3:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 4:00 pm | Teen Titans Go! | Pirates; I See You
 4:30 pm | Teen Titans Go! | Brian; Nature
 5:00 pm | Craig of the Creek | Sunday Clothes; Dog Decider
 5:30 pm | Craig of the Creek | Escape From Family Dinner; Bring Out Your Beast
 6:00 pm | Amazing World of Gumball | The Rival; The Pact
 6:30 pm | Amazing World of Gumball | The One; The Candidate
 7:00 pm | Amazing World of Gumball | The Shippening; The Anybody
 7:30 pm | Amazing World of Gumball | The Stink; The Silence
 8:00 pm | Amazing World of Gumball | The Awareness; The Future
 8:30 pm | Regular Show | Terror Tales of the Park