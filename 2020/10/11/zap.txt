# 2020-10-11
 6:00 am | Teen Titans Go! | TV Knight 4
 6:15 am | Teen Titans Go! | Teen Titans Roar!
 6:30 am | Bakugan: Armored Alliance | The Second Stage Begins; Mechanoids Attack!
 7:00 am | LEGO Ninjago | Dungeon Party!
 7:15 am | LEGO Ninjago | Dungeon Crawl!
 7:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 8:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
 8:30 am | Teen Titans Go! | The Metric System vs. Freedom
 8:45 am | Teen Titans Go! | The Chaff
 9:00 am | Teen Titans Go! | Walk Away
 9:15 am | Teen Titans Go! | Ghost with the Most
 9:30 am | Teen Titans Go! | Bucket List
 9:45 am | Teen Titans Go! | TV Knight 6
10:00 am | Teen Titans Go! | The Cruel Giggling Ghoul
10:15 am | Teen Titans Go! | Thumb War
10:30 am | Teen Titans Go! | Who's Laughing Now
10:45 am | Teen Titans Go! | Oregon Trail
11:00 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
11:15 am | The Amazing World of Gumball: Darwin's Yearbook | Teachers
11:30 am | Amazing World of Gumball | The Robot; The Picnic
12:00 pm | Amazing World of Gumball | The Goons; The Secret
12:30 pm | Amazing World of Gumball | The Sock; The Genius
 1:00 pm | Amazing World of Gumball | The Mustache; The Date
 1:30 pm | Amazing World of Gumball | The Club; The Wand
 2:00 pm | Craig of the Creek | Under the Overpass
 2:15 pm | Craig of the Creek | The Invitation
 2:30 pm | Craig of the Creek | Vulture's Nest
 2:45 pm | Craig of the Creek | Kelsey Quest
 3:00 pm | Teen Titans Go! | Super Hero Summer Camp
 3:15 pm | Teen Titans Go! | Bucket List
 3:30 pm | Teen Titans Go! | TV Knight 6
 3:45 pm | Teen Titans Go! | Rain on Your Wedding Day
 4:00 pm | Teen Titans Go! | Booty Scooty
 4:15 pm | Teen Titans Go! | Think About Your Future
 4:30 pm | Teen Titans Go! | Ghost with the Most
 4:45 pm | Teen Titans Go! | Bucket List
 5:00 pm | Teen Titans Go! | TV Knight 6
 5:15 pm | Teen Titans Go! | Kryponite
 5:30 pm | Teen Titans Go! | Thumb War
 5:45 pm | Teen Titans Go! | We're Off to Get Awards
 6:00 pm | Amazing World of Gumball | The Deal
 6:15 pm | Amazing World of Gumball | The Line
 6:30 pm | Amazing World of Gumball | The Quest; The Spoon
 7:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 7:30 pm | Amazing World of Gumball | The Helmet; The Fight
 7:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton
 8:00 pm | Apple & Onion | Fun Proof
 8:15 pm | Apple & Onion | Party Popper
 8:30 pm | Apple & Onion | Lil Noodle
 8:45 pm | Apple & Onion | Heatwave