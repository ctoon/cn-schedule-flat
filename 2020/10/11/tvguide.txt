# 2020-10-11
 6:00 am | Teen Titans Go! | TV Knight 4 ;Teen Titans Roar!
 6:30 am | Bakugan: Armored Alliance | The Second Stage Begins; Mechanoids Attack!
 7:00 am | LEGO Ninjago: Masters of Spinjitzu | Dungeon Party
 7:15 am | LEGO Ninjago: Masters of Spinjitzu | Dungeon Crawl!
 7:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 8:00 am | Teen Titans Go! | Hose Water; Yearbook Madness
 8:30 am | Teen Titans Go! | The Chaff; The Metric System Vs. Freedom
 9:00 am | Teen Titans Go! | Walk Away
 9:15 am | Teen Titans Go! | Ghost With the Most; Bucket List
 9:45 am | Teen Titans Go! | TV Knight 6 Kryponite
10:15 am | Teen Titans Go! | Thumb War
10:30 am | Teen Titans Go! | Who's Laughing Now; Oregon Trail
11:00 am | Amazing World of Gumball | Darwin's Yearbook - Sarah Darwin's Yearbook - Teachers
11:30 am | Amazing World of Gumball | The Robot; The Picnic
12:00 pm | Amazing World of Gumball | The Goons; The Secret
12:30 pm | Amazing World of Gumball | The Sock; The Genius
 1:00 pm | Amazing World of Gumball | The Mustache; The Date
 1:30 pm | Amazing World of Gumball | The Club; The Wand
 2:00 pm | Craig of the Creek | Under the Overpass; The Invitation
 2:30 pm | Craig of the Creek | Vulture's Nest; Kelsey Quest
 3:00 pm | Teen Titans Go! | Super Summer Hero Camp
 4:00 pm | Teen Titans Go! | Think About Your Future; Booty Scooty
 4:30 pm | Teen Titans Go! | Ghost With the Most; Bucket List
 5:00 pm | Teen Titans Go! | TV Knight 6 Kryponite
 5:30 pm | Teen Titans Go! | Thumb War
 5:45 pm | Teen Titans Go! | We're Off To Get Awards
 6:00 pm | Amazing World of Gumball | The Line; The Deal
 6:30 pm | Amazing World of Gumball | The Quest; The Spoon
 7:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 7:30 pm | Amazing World of Gumball | The Helmet; The Fight
 8:00 pm | Apple & Onion | Party Popper; Fun Proof
 8:30 pm | Apple & Onion | Lil Noodle; Heatwave