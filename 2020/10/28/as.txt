# 2020-10-28
 9:00 pm | Bob's Burgers | Ear-sy Rider
 9:30 pm | Bob's Burgers | Fort Night
10:00 pm | American Dad | Poltergasm
10:30 pm | American Dad | Exquisite Corpses
11:00 pm | Family Guy | Family Guy Viewer Mail #1
11:30 pm | Family Guy | Fat Guy Strangler
12:00 am | Rick and Morty | The Rickchurian Mortydate
12:30 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
12:45 am | Eric Andre Show | Tichina Arnold; Steve Schirrpa
 1:00 am | Aqua Teen | The Shaving
 1:15 am | Aqua Unit Patrol Squad 1 | Vampirus
 1:30 am | Family Guy | Family Guy Viewer Mail #1
 2:00 am | Family Guy | Fat Guy Strangler
 2:30 am | American Dad | Poltergasm
 3:00 am | Rick and Morty | The Rickchurian Mortydate
 3:30 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
 3:45 am | Eric Andre Show | Tichina Arnold; Steve Schirrpa
 4:00 am | Black Jesus | The Compton Crusader
 4:30 am | Aqua Teen | The Shaving
 4:45 am | Aqua Unit Patrol Squad 1 | Vampirus
 5:00 am | Bob's Burgers | Ear-sy Rider
 5:30 am | Bob's Burgers | Fort Night