# 2020-10-27
 9:00 pm | Bob's Burgers | Teen-a Witch
 9:30 pm | Bob's Burgers | The Wolf of Wharf Street
10:00 pm | American Dad | Paranoid_Frandroid
10:30 pm | American Dad | Best Little Horror House in Langley Falls
11:00 pm | Family Guy | And Then There Were Fewer Part 1 & 2
12:00 am | Rick and Morty | The ABC's of Beth
12:30 am | Eric Andre Show | T.I.; Abbey Lee Miller
12:45 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 1:00 am | Aqua Teen | Little Brittle
 1:15 am | Aqua Teen | Monster
 1:30 am | Family Guy | And Then There Were Fewer Part 1 & 2
 2:30 am | American Dad | Paranoid_Frandroid
 3:00 am | Rick and Morty | The ABC's of Beth
 3:30 am | Eric Andre Show | T.I.; Abbey Lee Miller
 3:45 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 4:00 am | Black Jesus | Boonie Christ
 4:30 am | Aqua Teen | Little Brittle
 4:45 am | Aqua Teen | Monster
 5:00 am | Bob's Burgers | Teen-a Witch
 5:30 am | Bob's Burgers | The Wolf of Wharf Street