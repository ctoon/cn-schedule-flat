# 2020-10-10
 6:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
 6:10 am | Teen Titans Go! | Communicate Openly
 6:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 7:00 am | Teen Titans Go! | Ghost with the Most
 7:15 am | Teen Titans Go! | Bucket List
 7:30 am | Teen Titans Go! | TV Knight 6
 7:45 am | Teen Titans Go! | Rain on Your Wedding Day
 8:00 am | Teen Titans Go! | Thumb War
 8:15 am | Teen Titans Go! Vs. Teen Titans | 
10:00 am | Ben 10 vs. The Universe: The Movie | 
11:30 am | The Amazing World of Gumball: Darwin's Yearbook | Alan
11:45 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
12:00 pm | Amazing World of Gumball | The Third; The Debt
12:30 pm | Amazing World of Gumball | The Pressure; The Painting
 1:00 pm | Amazing World of Gumball | The Responsible; The Dress
 1:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:00 pm | Teen Titans Go! | The Art of Ninjutsu
 2:15 pm | Teen Titans Go! | Think About Your Future
 2:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 2:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 3:00 pm | Teen Titans Go! | Ghost with the Most
 3:15 pm | Teen Titans Go! | Bucket List
 3:30 pm | Teen Titans Go! | TV Knight 6
 3:45 pm | Teen Titans Go! | Kryponite
 4:00 pm | Teen Titans Go! | Thumb War
 4:15 pm | Teen Titans Go! Vs. Teen Titans | 
 6:00 pm | Ben 10 vs. The Universe: The Movie | 
 7:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 7:45 pm | Victor and Valentino | The Guest
 8:00 pm | Amazing World of Gumball | The Ad
 8:15 pm | Amazing World of Gumball | The Anybody
 8:30 pm | Amazing World of Gumball | The Awareness
 8:45 pm | Amazing World of Gumball | The BFFS