# 2020-10-24
 6:00 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 6:15 am | Teen Titans Go! | Wally T
 6:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 6:45 am | Teen Titans Go! | Who's Laughing Now
 7:00 am | Teen Titans Go! | History Lesson
 7:15 am | Teen Titans Go! | Oregon Trail
 7:30 am | Teen Titans Go! | The Art of Ninjutsu
 7:45 am | Teen Titans Go! | Snuggle Time
 8:00 am | Teen Titans Go! | Think About Your Future
 8:15 am | Teen Titans Go! | Oh Yeah!
 8:30 am | Teen Titans Go! | Booty Scooty
 8:45 am | Teen Titans Go! | Riding the Dragon
 9:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 9:30 am | Teen Titans Go! | Caramel Apples; Halloween
 9:45 am | Teen Titans Go! | Thumb War
10:00 am | Total DramaRama | Weiner Takes All
10:15 am | Total DramaRama | Apoca-lice Now
10:30 am | Total DramaRama | Gnome More Mister Nice Guy
10:45 am | Total DramaRama | Look Who's Clocking
11:00 am | Amazing World of Gumball | The Uncle
11:15 am | Amazing World of Gumball | The Heist
11:30 am | Amazing World of Gumball | The Mothers; The Password
12:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
12:30 pm | Amazing World of Gumball | The Mirror; The Burden
 1:00 pm | Amazing World of Gumball | The Bros; The Man
 1:30 pm | Amazing World of Gumball | The Pizza; The Lie
 1:45 pm | Amazing World of Gumball | Gumball Chronicles: The Curse of Elmore
 2:00 pm | Amazing World of Gumball | The Petals
 2:15 pm | Amazing World of Gumball | The Nuisance
 2:30 pm | Amazing World of Gumball | The Return
 2:45 pm | Amazing World of Gumball | The Nemesis
 3:00 pm | Amazing World of Gumball | The Agent
 3:15 pm | Amazing World of Gumball | The Inquisition
 3:30 pm | Amazing World of Gumball | The Mess
 3:45 pm | Amazing World of Gumball | The Possession
 4:00 pm | Amazing World of Gumball | The Society; The Spoiler
 4:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 5:00 pm | Teen Titans Go! | Witches Brew
 5:15 pm | Teen Titans Go! | TV Knight 5
 5:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:00 pm | Teen Titans Go! | The Overbite
 6:15 pm | Teen Titans Go! | Shrimps and Prime Rib
 6:30 pm | Teen Titans Go! | Halloween vs. Christmas
 6:45 pm | Teen Titans Go! | Movie Night
 7:00 pm | Teen Titans Go! | The Fight
 7:15 pm | Teen Titans Go! | Genie President
 7:30 pm | Teen Titans Go! | Little Elvis
 7:45 pm | Teen Titans Go! | The Groover
 8:00 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 8:15 pm | Teen Titans Go! | Wally T
 8:30 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 8:45 pm | Teen Titans Go! | Who's Laughing Now