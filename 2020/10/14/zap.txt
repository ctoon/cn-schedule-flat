# 2020-10-14
 6:00 am | Amazing World of Gumball | The Words; The Apology
 6:30 am | Amazing World of Gumball | The Internet; The Plan
 7:00 am | Amazing World of Gumball | The Watch; The Bet
 7:30 am | Amazing World of Gumball | The Bumpkin; The Flakers
 8:00 am | Victor and Valentino | Fueygo Fest
 8:15 am | Victor and Valentino | Los Pajaros
 8:30 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 9:00 am | Teen Titans Go! | Burger vs. Burrito; Matched
 9:30 am | Teen Titans Go! | Colors of Raven; The Left Leg
10:00 am | Craig of the Creek | The Future Is Cardboard
10:15 am | Craig of the Creek | Jessica's Trail
10:30 am | Craig of the Creek | Dog Decider
10:45 am | Craig of the Creek | Dinner at the Creek
11:00 am | Teen Titans Go! | Books; Lazy Sunday
11:30 am | Teen Titans Go! | Hand Zombie
11:45 am | Teen Titans Go! | Costume Contest
12:00 pm | Teen Titans Go! | Birds; Be Mine
12:30 pm | Teen Titans Go! | BBRAE
 1:00 pm | Total DramaRama | Dream Worriers
 1:15 pm | Total DramaRama | Gum and Gummer
 1:30 pm | Total DramaRama | Grody to the Maximum
 1:45 pm | Total DramaRama | Invasion of the Booger Snatchers
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Promise; The Voice
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | Craig of the Creek | The Brood
 4:15 pm | Craig of the Creek | Deep Creek Salvage
 4:30 pm | Craig of the Creek | Under the Overpass
 4:45 pm | Craig of the Creek | Dibs Court
 5:00 pm | Total DramaRama | Total Eclipse of the Fart
 5:15 pm | Total DramaRama | Snow Way Out
 5:30 pm | Total DramaRama | For a Few Duncans More
 5:45 pm | Total DramaRama | All Up in Your Drill
 6:00 pm | Teen Titans Go! | Caramel Apples
 6:15 pm | Teen Titans Go! | Halloween
 6:30 pm | Teen Titans Go! | Brain Food; In and Out
 7:00 pm | Apple & Onion | Ferekh
 7:15 pm | Apple & Onion | Apple & Onion's Booth
 7:30 pm | Victor and Valentino | Folk Art Friends
 7:45 pm | Victor and Valentino | Get Your Sea Legs
 8:00 pm | Amazing World of Gumball | The Flower; The Banana
 8:30 pm | Amazing World of Gumball | Halloween; The Treasure