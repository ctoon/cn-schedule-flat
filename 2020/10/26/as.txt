# 2020-10-26
 9:00 pm | Bob's Burgers | O.T. the Outside Toilet
 9:30 pm | Bob's Burgers | The Hauntening
10:00 pm | American Dad | Hot Water
10:30 pm | American Dad | Death by Dinner Party
11:00 pm | Family Guy | Wasted Talent
11:30 pm | Family Guy | Death Lives
12:00 am | Rick and Morty | Morty's Mind Blowers
12:30 am | Eric Andre Show | Pauly D; Rick Springfield
12:45 am | Eric Andre Show | Bird Up!
 1:00 am | Aqua Teen | Love Mummy
 1:15 am | Aqua Teen | Broodwich
 1:30 am | Family Guy | Wasted Talent
 2:00 am | Family Guy | Death Lives
 2:30 am | American Dad | Hot Water
 3:00 am | Rick and Morty | Morty's Mind Blowers
 3:30 am | Eric Andre Show | Pauly D; Rick Springfield
 3:45 am | Eric Andre Show | Bird Up!
 4:00 am | Black Jesus | Operation Shut'emdown
 4:30 am | Aqua Teen | Love Mummy
 4:45 am | Aqua Teen | Broodwich
 5:00 am | Bob's Burgers | O.T. the Outside Toilet
 5:30 am | Bob's Burgers | The Hauntening