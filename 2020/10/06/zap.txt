# 2020-10-06
 6:00 am | Amazing World of Gumball | The Transformation
 6:15 am | Amazing World of Gumball | The Revolt
 6:30 am | Amazing World of Gumball | The Understanding
 6:45 am | Amazing World of Gumball | The Mess
 7:00 am | Amazing World of Gumball | The Ad
 7:15 am | Amazing World of Gumball | The Possession
 7:30 am | Amazing World of Gumball | The Slip
 7:45 am | Amazing World of Gumball | The Future
 8:00 am | Victor and Valentino | Escaramuza
 8:15 am | Victor and Valentino | Los Perdidos
 8:30 am | Teen Titans Go! | Super Hero Summer Camp
 9:30 am | Teen Titans Go! | Royal Jelly
 9:45 am | Teen Titans Go! | The Great Disaster
10:00 am | Craig of the Creek | Pencil Break Mania
10:15 am | Craig of the Creek | Kelsey Quest
10:30 am | Craig of the Creek | The Last Game of Summer
10:45 am | Craig of the Creek | JPony
11:00 am | Teen Titans Go! | Strength of a Grown Man
11:15 am | Teen Titans Go! | The Viewers Decide
11:30 am | Teen Titans Go! | Beast Boy's That's What's Up
12:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 1:00 pm | Total DramaRama | Pudding the Planet First
 1:15 pm | Total DramaRama | Bananas & Cheese
 1:30 pm | Total DramaRama | Supply Mom
 1:45 pm | Total DramaRama | Inglorious Toddlers
 2:00 pm | Amazing World of Gumball | The Robot; The Picnic
 2:30 pm | Amazing World of Gumball | The Goons; The Secret
 3:00 pm | Amazing World of Gumball | The Sock; The Genius
 3:30 pm | Amazing World of Gumball | The Mustache; The Date
 4:00 pm | Craig of the Creek | Jessica Goes to the Creek
 4:15 pm | Craig of the Creek | The Climb
 4:30 pm | Craig of the Creek | Too Many Treasures
 4:45 pm | Craig of the Creek | Big Pinchy
 5:00 pm | Total DramaRama | The Tooth About Zombies
 5:15 pm | Total DramaRama | Tiger Fail
 5:30 pm | Total DramaRama | Lie-Ranosaurus Wrecked
 5:45 pm | Total DramaRama | A Ninjustice to Harold
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 6:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 pm | Teen Titans Go! | Bucket List
 7:15 pm | Teen Titans Go! | Ghost with the Most
 7:30 pm | Victor and Valentino | Los Pajaros
 7:45 pm | Victor and Valentino | Kindred Spirits
 8:00 pm | Amazing World of Gumball | Halloween; The Treasure
 8:30 pm | Amazing World of Gumball | The Intelligence
 8:45 pm | Amazing World of Gumball | The Silence