# 2020-10-30
 6:00 am | Amazing World of Gumball | The Love; The Blame
 6:30 am | Amazing World of Gumball | The Awkwardness; The Slap
 7:00 am | Amazing World of Gumball | The Nest; The Detective
 7:30 am | Amazing World of Gumball | The Points; The Fury
 8:00 am | Victor and Valentino | Carmelita
 8:15 am | Victor and Valentino | Poacher Patrol
 8:30 am | Teen Titans Go! | Garage Sale; Finally a Lesson
 9:00 am | Teen Titans Go! | Secret Garden Arms Race With Legs
 9:30 am | Teen Titans Go! | Bottle Episode; Obinray
10:00 am | Craig of the Creek | Deep Creek Salvage; Sugar Smugglers
10:30 am | Craig of the Creek | Dibs Court; Sleepover at Jp's
11:00 am | Teen Titans Go! | Monster Squad!; Costume Contest
11:30 am | Teen Titans Go! | Operation Tin Man; Nean
12:00 pm | Teen Titans Go! | The Croissant; The Dignity of Teeth
12:30 pm | Teen Titans Go! | Magic Man; Ghost With the Most
 1:00 pm | Total DramaRama | Way Back Wendel
 1:15 pm | Total DramaRama | Dude Where's Macaw
 1:30 pm | Total DramaRama | The Bad Guy Busters; Stop! Hamster Time
 2:00 pm | Amazing World Of Gumball | The Others; The Parking
 2:30 pm | Amazing World of Gumball | The Upgrade; The Signature
 3:00 pm | Amazing World of Gumball | The Gift; The Comic
 3:30 pm | Amazing World of Gumball | The Apprentice; The Romantic
 4:00 pm | Craig of the Creek | Creature Feature; King of Camping
 4:30 pm | Craig of the Creek | The Takeout Mission; Turning the Tables
 5:00 pm | Total DramaRama | Hic Hic Hooray; Look Who's Clocking
 5:30 pm | Total DramaRama | Dissing Cousins
 6:00 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 6:30 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 7:00 pm | Total DramaRama | Stingin' in the Rain
 7:15 pm | Total DramaRama | Way Back Wendel
 7:30 pm | Victor and Valentino | Lonely Haunts Club 3: La Llorona; in Your Own Skin
 8:00 pm | Amazing World of Gumball | Gumball Chronicles: The Curse of Elmore; Halloween
 8:30 pm | Amazing World of Gumball | The Advice; The Night