# 2020-10-29
 9:00 pm | Bob's Burgers | Tina and the Real Ghost
 9:30 pm | Bob's Burgers | The Hauntening
10:00 pm | American Dad | Hot Water
10:30 pm | American Dad | Death by Dinner Party
11:00 pm | Family Guy | And Then There Were Fewer Part 1 & 2
12:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:30 am | Eric Andre Show | A King Is Born
12:45 am | Eric Andre Show | Hannibal Quits
 1:00 am | DREAM CORP LLC | Comin' in Hot
 1:30 am | Robot Chicken | Not Enough Women
 1:45 am | Robot Chicken | The Angelic Sounds of Mike Giggling
 2:00 am | Family Guy | And Then There Were Fewer Part 1 & 2
 3:00 am | American Dad | Hot Water
 3:30 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 4:00 am | Black Jesus | Hair Tudi
 4:30 am | Eric Andre Show | A King Is Born
 4:45 am | Eric Andre Show | Hannibal Quits
 5:00 am | Bob's Burgers | Tina and the Real Ghost
 5:30 am | Bob's Burgers | The Hauntening