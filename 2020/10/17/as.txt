# 2020-10-17
 9:00 pm | Bob's Burgers | There's No Business Like Mr. Business Business
 9:30 pm | American Dad | Roger's Baby
10:00 pm | American Dad | She Swill Survive
10:30 pm | Rick and Morty | M. Night Shaym-Aliens!
11:00 pm | Family Guy | Peter Peter Caviar Eater
11:30 pm | Family Guy | Stuck Together, Torn Apart
12:00 am | SPECIAL | Adult Swim x Ben & Jerry's Present Holy Calamavote
12:50 am | Eric Andre Show | The Eric Andre Show Season Five Sneak Peak
 1:00 am | Dragon Ball Super | A Mysterious Beauty Appears! The Enigma of the Tien Shin-Style Dojo?!
 1:30 am | JoJo's Bizarre Adventure: Golden Wind | Golden Wind Requiem
 2:00 am | Assassination Classroom | School Trip Time/2nd Period
 2:30 am | Fire Force | Smile
 3:00 am | Naruto:Shippuden | Lingering Snow
 3:30 am | Demon Slayer: Kimetsu no Yaiba | Trainer Sakonji Urokodaki
 4:00 am | Genndy Tartakovsky's Primal | Plague of Madness
 4:30 am | The Venture Brothers | Orb
 5:00 am | Joe Pera Talks With You | Joe Pera Answers Your Questions About Cold Weather Sports
 5:30 am | Joe Pera Talks With You | Joe Pera Talks With You About Beans
 5:45 am | Joe Pera Talks With You | Joe Pera Takes You on a Hike