# 2020-11-10
 6:00 am | Amazing World Of Gumball | The Web; The Bffs
 6:30 am | Amazing World of Gumball | The Inquisition; The Agent
 7:00 am | Amazing World of Gumball | Darwin's Yearbook - Alan Darwin's Yearbook - Banana Joe
 7:30 am | Amazing World of Gumball | Darwin's Yearbook - Carrie Darwin's Yearbook - Clayton
 8:00 am | Thundercats Roar! | Eclipsorr
 8:15 am | Thundercats Roar! | Wizz-Ra
 8:30 am | Teen Titans Go! | Record Book Thumb War
 9:00 am | Teen Titans Go! | Magic Man ; The Titans Go Casual
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
10:00 am | Craig Of The Creek | Craig And The Kid's Table
10:30 am | Craig of the Creek | Jessica Shorts Return of the Honeysuckle Rangers
11:00 am | Teen Titans Go! | Teen Titans Roar! Rain on Your Wedding Day
11:30 am | Teen Titans Go! | Thanksgiving; Serious Business
12:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 1:00 pm | Amazing World of Gumball | The Diet; The Pact
 1:30 pm | Amazing World of Gumball | The Ex; The Candidate
 2:00 pm | Amazing World of Gumball | The Anybody; The Awareness
 2:30 pm | Amazing World of Gumball | The Shippening; The Parents
 3:00 pm | Amazing World of Gumball | The Stink; The Founder
 3:30 pm | Amazing World of Gumball | The Schooling; The Master
 4:00 pm | Craig of the Creek | Jinxening; The Sparkle Cadet
 4:30 pm | Craig of the Creek | In the Key of the Creek Stink Bomb
 5:00 pm | Total DramaRama | Snow Way out Pudding the Planet First
 5:30 pm | Total DramaRama | All up in Your Drill Supply Mom
 6:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 6:30 pm | Teen Titans Go! | Girl's Night Out; You're Fired
 7:00 pm | Teen Titans Go! | Super Robin; Tower Power
 7:30 pm | Teen Titans Go! | Parasite; Starliar
 8:00 pm | Amazing World Of Gumball | The Heart; The Slip
 8:30 pm | Amazing World of Gumball | The Drama; The Ghouls