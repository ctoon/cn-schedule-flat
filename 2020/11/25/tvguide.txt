# 2020-11-25
 6:00 am | Amazing World of Gumball | The Advice; The Pest
 6:30 am | Amazing World of Gumball | The Hug; The Signal
 7:00 am | Amazing World of Gumball | The Origins; The Origins Part 2
 7:30 am | Amazing World Of Gumball | The Girlfriend; The Compilation
 8:00 am | Teen Titans Go! | Titan Saving Time; Permanent Record
 8:30 am | Teen Titans Go! | Academy; The Accept the Next Proposition You Hear
 9:00 am | Teen Titans Go! | Teen Titans Roar! Rain on Your Wedding Day
 9:30 am | Teen Titans Go! | Curse Of The Booty Scooty; Collect Them All
10:00 am | Craig of the Creek | The Pencil Break Mania Last Game of Summer
10:30 am | Craig of the Creek | Fall Anthology; Afterschool Snackdown
11:00 am | Teen Titans Go! | Leg Day; Cat's Fancy
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:30 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 1:00 pm | Teen Titans | Final Exam
 1:30 pm | Teen Titans | Sisters
 2:00 pm | Teen Titans Go! | Master Detective; The Avogodo
 2:30 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
 3:00 pm | Teen Titans Go! | Bucket List; TV Knight 6
 3:30 pm | Teen Titans Go! | Kryponite; Thumb War
 4:00 pm | Teen Titans | Divide and Conquer
 4:30 pm | Teen Titans | Forces of Nature
 5:00 pm | Teen Titans Go! | Black Friday
 5:15 pm | Teen Titans Go! | Baby Mouth
 5:30 pm | Teen Titans Go! Vs. Teen Titans | 
 7:00 pm | Teen Titans Go! To the Movies | 
 8:30 pm | Teen Titans Go! | Thanksgiving; Thanksgetting; Black Friday