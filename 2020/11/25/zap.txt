# 2020-11-25
 6:00 am | Amazing World of Gumball | The Pest
 6:15 am | Amazing World of Gumball | The Advice
 6:30 am | Amazing World of Gumball | The Hug
 6:45 am | Amazing World of Gumball | The Signal
 7:00 am | Amazing World of Gumball | The Origins
 7:15 am | Amazing World of Gumball | The Origins
 7:30 am | Amazing World of Gumball | The Girlfriend
 7:45 am | Amazing World of Gumball | The BFFS
 8:00 am | Teen Titans Go! | Permanent Record
 8:15 am | Teen Titans Go! | Titan Saving Time
 8:30 am | Teen Titans Go! | The Academy
 8:45 am | Teen Titans Go! | Baby Mouth
 9:00 am | Teen Titans Go! | Rain on Your Wedding Day
 9:15 am | Teen Titans Go! | Teen Titans Roar!
 9:30 am | Teen Titans Go! | Collect Them All
 9:45 am | Teen Titans Go! | Curse of the Booty Scooty
10:00 am | Craig of the Creek | Pencil Break Mania
10:15 am | Craig of the Creek | The Last Game of Summer
10:30 am | Craig of the Creek | Fall Anthology
10:45 am | Craig of the Creek | King of Camping
11:00 am | Teen Titans Go! | Cat's Fancy
11:15 am | Teen Titans Go! | Leg Day
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
12:45 pm | Teen Titans Go! | Toddler Titans...Yay!
 1:00 pm | Teen Titans | Final Exam
 1:30 pm | Teen Titans | Sisters
 2:00 pm | Teen Titans Go! | Master Detective
 2:15 pm | Teen Titans Go! | The Avogodo
 2:30 pm | Teen Titans Go! | Thanksgetting
 2:45 pm | Teen Titans Go! | Huggbees
 3:00 pm | Teen Titans Go! | Bucket List
 3:15 pm | Teen Titans Go! | TV Knight 6
 3:30 pm | Teen Titans Go! | Kryponite
 3:45 pm | Teen Titans Go! | Brain Percentages
 4:00 pm | Teen Titans | Divide and Conquer
 4:30 pm | Teen Titans | Forces of Nature
 5:00 pm | Teen Titans Go! | Black Friday
 5:15 pm | Teen Titans Go! Vs. Teen Titans | 
 7:00 pm | Teen Titans GO! to the Movies | 
 8:30 pm | Teen Titans Go! | Thanksgiving
 8:45 pm | Teen Titans Go! | Black Friday