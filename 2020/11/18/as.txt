# 2020-11-18
 9:00 pm | Bob's Burgers | Bed & Breakfast
 9:30 pm | Bob's Burgers | Art Crawl
10:00 pm | American Dad | Shark?!
10:30 pm | American Dad | The Long March
11:00 pm | Family Guy | Save the Clam
11:30 pm | Family Guy | Farmer Guy
12:00 am | Rick and Morty | M. Night Shaym-Aliens!
12:30 am | Eric Andre Show | The A$AP Ferg Show
12:45 am | Eric Andre Show | Blannibal Quits
 1:00 am | Lazor Wulf | We Good!
 1:15 am | Lazor Wulf | Where You From?
 1:30 am | Family Guy | Save the Clam
 2:00 am | Family Guy | Farmer Guy
 2:30 am | American Dad | Shark?!
 3:00 am | Rick and Morty | M. Night Shaym-Aliens!
 3:30 am | Eric Andre Show | The A$AP Ferg Show
 3:45 am | Eric Andre Show | Blannibal Quits
 4:00 am | Tropical Cop Tales | The Shrimp Gang are Back
 4:15 am | Tropical Cop Tales | The Big Night In
 4:30 am | Lazor Wulf | We Good!
 4:45 am | Lazor Wulf | Where You From?
 5:00 am | Bob's Burgers | Bed & Breakfast
 5:30 am | Bob's Burgers | Art Crawl