# 2020-11-15
 6:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 am | Bakugan: Battle Planet | Champion of the Universe, Dan; Operation Babysitter
 7:00 am | Teen Titans Go! | Friendship; Vegetables
 7:30 am | Teen Titans Go! | The Mask; Slumber Party
 8:00 am | Teen Titans Go! | Serious Business; Thanksgiving
 8:30 am | Teen Titans Go! | Road Trip; The Best Robin
 9:00 am | Teen Titans Go! | Mouth Hole; Hot Garbage
 9:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:45 am | Teen Titans Go! | Black Friday
10:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
10:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
11:00 am | Amazing World of Gumball | The Recipe; The Puppy
11:30 am | Amazing World of Gumball | The Name; The Extras
12:00 pm | Amazing World of Gumball | The Gripes; The Vacation
12:30 pm | Amazing World of Gumball | The Fraud; The Void
 1:00 pm | Amazing World of Gumball | The Boss; The Move
 1:30 pm | Amazing World of Gumball | The Law; The Allergy
 2:00 pm | Amazing World of Gumball | The Mothers; The Password
 2:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 3:00 pm | Amazing World of Gumball | The Oracle; The Safety
 3:15 pm | Amazing World of Gumball | The Mess
 3:30 pm | Amazing World of Gumball | The Butterfly; The Question
 4:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 5:00 pm | Teen Titans Go! | Let's Get Serious; Tamaranian Vacation
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 6:15 pm | Teen Titans Go! | Booby Trap House
 6:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 7:00 pm | Ben 10 vs. The Universe: The Movie | 
 8:00 pm | Ben 10 vs. The Universe: The Movie | 