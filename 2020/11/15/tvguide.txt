# 2020-11-15
 6:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 am | Bakugan: Armored Alliance | Champion Of The Universe, Dan!; Operation Babysitter
 7:00 am | Teen Titans Go! | Vegetables; Friendship
 7:30 am | Teen Titans Go! | The Mask; Slumber Party
 8:00 am | Teen Titans Go! | Thanksgiving; Serious Business
 8:30 am | Teen Titans Go! | The Best Robin; Road Trip
 9:00 am | Teen Titans Go! | Hot Garbage; Mouth Hole
 9:30 am | Teen Titans Go! | Crazy Day; Robin Backwards
10:00 am | Teen Titans Go! | Real Boy Adventures; Smile Bones
10:30 am | Teen Titans Go! | Hose Water; Yearbook Madness
11:00 am | Amazing World of Gumball | The Recipe; The Puppy
11:30 am | Amazing World of Gumball | The Name; The Extras
12:00 pm | Amazing World of Gumball | The Gripes; The Vacation
12:30 pm | Amazing World of Gumball | The Fraud; The Void
 1:00 pm | Amazing World of Gumball | The Move; The Boss
 1:30 pm | Amazing World of Gumball | The Allergy; The Law
 2:00 pm | Amazing World of Gumball | The Mothers / The Password
 2:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 3:00 pm | Amazing World of Gumball | The Oracle; The Safety
 3:30 pm | Amazing World of Gumball | The Butterfly; The Question
 4:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 5:00 pm | Teen Titans Go! | Tamaranian Vacation; Let's Get Serious
 5:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 6:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 6:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 7:00 pm | Ben 10 vs. The Universe | 