# 2020-11-07
 6:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 6:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 am | Teen Titans Go! | Double Trouble; The Date
 7:30 am | Teen Titans Go! | Dude Relax; Laundry Day
 8:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 8:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 8:45 am | Teen Titans Go! | Thumb War
 9:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
 9:30 am | Total DramaRama | Camping Is in Tents
10:00 am | Total DramaRama | Dissing Cousins
10:30 am | Amazing World of Gumball | The Ad
10:45 am | Amazing World of Gumball | The Mess
11:00 am | Amazing World of Gumball | The Neighbor
11:15 am | Amazing World of Gumball | The Stink
11:30 am | Amazing World of Gumball | The Pact
11:45 am | Amazing World of Gumball | The Awareness
12:00 pm | Amazing World of Gumball | The Candidate
12:15 pm | Amazing World of Gumball | The Parents
12:30 pm | Amazing World of Gumball | The Anybody
12:45 pm | Amazing World of Gumball | The Founder
 1:00 pm | Amazing World of Gumball | The Shippening
 1:15 pm | Amazing World of Gumball | The Schooling
 1:30 pm | Amazing World of Gumball | The Intelligence
 1:45 pm | Amazing World of Gumball | The Master
 2:00 pm | Amazing World of Gumball | The Potion
 2:15 pm | Amazing World of Gumball | The Silence
 2:30 pm | Amazing World of Gumball | The Spinoffs
 2:45 pm | Amazing World of Gumball | The Future
 3:00 pm | Amazing World of Gumball | The Transformation
 3:15 pm | Amazing World of Gumball | The Wish
 3:30 pm | Amazing World of Gumball | The Understanding
 3:45 pm | Amazing World of Gumball | The Awkwardness
 4:00 pm | Teen Titans Go! | Super Robin; Tower Power
 4:30 pm | Teen Titans Go! | BL4Z3
 4:45 pm | Teen Titans Go! | Hot Salad Water
 5:00 pm | Teen Titans Go! | Night Begins to Shine 2: You're the One
 6:15 pm | Teen Titans Go! | Kryponite
 6:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 6:45 pm | Teen Titans Go! | What's Opera, Titans?
 7:00 pm | Steven Universe: The Movie | 
 8:45 pm | Amazing World of Gumball | The Crew