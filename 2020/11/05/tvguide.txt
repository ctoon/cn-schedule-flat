# 2020-11-05
 6:00 am | Amazing World of Gumball | The Advice; The Check
 6:30 am | Amazing World of Gumball | The Signal; The Pest
 7:00 am | Amazing World of Gumball | The Girlfriend; The Hug
 7:30 am | Amazing World of Gumball | The Disaster; The Re-Run; #83; The Parasite
 8:00 am | Thundercats Roar! | Pumm-Ra
 8:15 am | ThunderCats Roar! | King of the Machines
 8:30 am | Teen Titans Go! | Bbrbday; TV Knight 4
 9:00 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason; Lil' Dimples
 9:30 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story; Don't Be an Icarus
10:00 am | Craig of the Creek | Deep Creek Salvage; Mortimor to the Rescue
10:30 am | Craig Of The Creek | The Other Side
11:00 am | Teen Titans Go! | Super Summer Hero Camp
12:00 pm | Teen Titans Go! | Thanksgiving; Serious Business
12:30 pm | Teen Titans Go! | Teen Titans Vroom
 1:00 pm | Amazing World of Gumball | The Test; The Petals
 1:30 pm | Amazing World of Gumball | The Slide; The Nuisance
 2:00 pm | Amazing World of Gumball | The Loophole; The Best
 2:30 pm | Amazing World of Gumball | The Fuss; The Worst
 3:00 pm | Amazing World of Gumball | The News; The List
 3:30 pm | Amazing World of Gumball | The Vase; The Deal
 4:00 pm | Craig of the Creek | Jessica's Trail Tea Timer's Ball
 4:30 pm | Craig Of The Creek | Craig And The Kid's Table
 5:00 pm | Total DramaRama | Not Without My Fudgy Lumps; Robo Teacher
 5:30 pm | Total DramaRama | Paint That a Shame; Glove Glove Me Do
 6:00 pm | Teen Titans Go! | Little Elvis; The Viewers Decide
 6:30 pm | Teen Titans Go! | Cartoon Feud; Curse Of The Booty Scooty
 7:00 pm | Teen Titans Go! | The Chaff; Communicate Openly
 7:30 pm | Teen Titans Go! | Them Soviet Boys; The Great Disaster
 8:00 pm | Amazing World of Gumball | The Origins; The Origins Part 2
 8:30 pm | Amazing World of Gumball | The Apprentice; The Traitor