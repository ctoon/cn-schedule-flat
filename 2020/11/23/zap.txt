# 2020-11-23
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Amazing World of Gumball | The Downer; The Egg
 7:30 am | Amazing World of Gumball | The Triangle; The Money
 8:00 am | Teen Titans Go! | The Overbite
 8:15 am | Teen Titans Go! | Shrimps and Prime Rib
 8:30 am | Teen Titans Go! | Booby Trap House
 8:45 am | Teen Titans Go! | Thanksgetting
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 9:30 am | Teen Titans Go! | Had to Be There
 9:45 am | Teen Titans Go! | Strength of a Grown Man
10:00 am | Craig of the Creek | The Other Side: The Tournament
10:30 am | Craig of the Creek | Craig and the Kid's Table
11:00 am | Teen Titans Go! | Collect Them All
11:15 am | Teen Titans Go! | TV Knight 5
11:30 am | Teen Titans Go! | Serious Business; Thanksgiving
12:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Amazing World of Gumball | The Kids; The Fan
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Amazing World of Gumball | The Fraud; The Void
 3:30 pm | Amazing World of Gumball | The Boss; The Move
 4:00 pm | Teen Titans Go! | Thanksgetting
 4:15 pm | Teen Titans Go! | Beast Girl
 4:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 5:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 5:30 pm | Teen Titans Go! | Serious Business; Thanksgiving
 5:45 pm | Teen Titans Go! | Huggbees
 6:00 pm | Teen Titans Go! | Cartoon Feud
 6:15 pm | Teen Titans Go! | The Viewers Decide
 6:30 pm | Teen Titans Go! | Had to Be There
 6:45 pm | Teen Titans Go! | Strength of a Grown Man
 7:00 pm | Steven Universe: The Movie | 