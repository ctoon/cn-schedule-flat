# 2020-11-03
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny?
 6:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Leslie?
 6:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert?
 6:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Anyone?
 7:00 am | Amazing World of Gumball | The News; The List
 7:30 am | Amazing World of Gumball | The Vase; The Deal
 8:00 am | ThunderCats Roar! | Corporate Buyout
 8:15 am | ThunderCats Roar! | Hachiman
 8:30 am | Teen Titans Go! | Super Summer Hero Camp
 9:30 am | Teen Titans Go! | Thanksgiving; Serious Business
10:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny?
10:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Leslie?
10:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert?
10:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Anyone?
11:00 am | Teen Titans Go! | Genie President What's Opera; Titans?
11:30 am | Teen Titans Go! | Tall Titan Tales Royal Jelly
12:00 pm | Teen Titans Go! | I Used To Be A Peoples; Strength Of A Grown Man
12:30 pm | Teen Titans Go! | The Metric System Vs. Freedom; Had To Be There
 1:00 pm | Amazing World of Gumball | The Love; The Routine
 1:30 pm | Amazing World of Gumball | The Parking; The Misunderstanding
 2:00 pm | Amazing World of Gumball | The Upgrade; The Awkwardness
 2:30 pm | Amazing World of Gumball | The Nest; The Comic
 3:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny?
 3:15 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Leslie?
 3:30 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert?
 3:45 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Anyone?
 4:00 pm | Cartoon Network Programming | 
 4:30 pm | Cartoon Network Programming | 
 5:00 pm | Cartoon Network Programming | 
 5:15 pm | Teen Titans Go! | TV Knight
 5:30 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason Lil' Dimples
 6:00 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story Don't Be an Icarus
 6:30 pm | Teen Titans Go! | Business Ethics Wink Wink Stockton; Ca!
 7:00 pm | LEGO DC Shazam: Magic & Monsters | 