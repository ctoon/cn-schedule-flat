# 2020-11-08
 6:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 6:30 am | Bakugan: Battle Planet | The Tokyo Catastrophe; Bakugan Battle League Finale
 7:00 am | Teen Titans Go! | Books; Lazy Sunday
 7:30 am | Teen Titans Go! | Parasite; Starliar
 8:00 am | Teen Titans Go! | Meatball Party; Staff Meeting
 8:30 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 9:00 am | Teen Titans Go! | Power Moves; Staring at the Future
 9:30 am | Teen Titans Go! | No Power; Sidekick
 9:45 am | Teen Titans Go! | Thanksgetting
10:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
10:30 am | Teen Titans Go! | Legs; Breakfast Cheese
11:00 am | Amazing World of Gumball | The Drama
11:15 am | Amazing World of Gumball | The Future
11:30 am | Amazing World of Gumball | The Ghouls
11:45 am | Amazing World of Gumball | The BFFS
12:00 pm | Amazing World of Gumball | The Agent
12:15 pm | Amazing World of Gumball | The Web
12:30 pm | Amazing World of Gumball | The Inquisition
12:45 pm | Amazing World of Gumball | The Ex
 1:00 pm | Amazing World of Gumball | The Menu
 1:15 pm | Amazing World of Gumball | The Deal
 1:30 pm | Amazing World of Gumball | The Uncle
 1:45 pm | Amazing World of Gumball | The Line
 2:00 pm | Amazing World of Gumball | The Heist
 2:15 pm | Amazing World of Gumball | The Singing
 2:30 pm | Amazing World of Gumball | The Petals
 2:45 pm | Amazing World of Gumball | The Parking
 3:00 pm | Amazing World of Gumball | The Nuisance
 3:15 pm | Amazing World of Gumball | The Lady
 3:30 pm | Amazing World of Gumball | The Best
 3:45 pm | Amazing World of Gumball | The Sucker
 4:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 5:00 pm | Teen Titans Go! | Waffles; Opposites
 5:30 pm | Teen Titans Go! | Birds; Be Mine
 6:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 6:30 pm | Teen Titans Go! | Kryponite
 7:00 pm | We Bare Bears: The Movie | 
 8:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 8:45 pm | The Amazing World of Gumball: Darwin's Yearbook | Clayton