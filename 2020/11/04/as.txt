# 2020-11-04
 9:00 pm | Bob's Burgers | Sit Me Baby One More Time
 9:30 pm | Bob's Burgers | Y Tu Ga-Ga Tambien
10:00 pm | American Dad | (You Gotta) Strike for Your Right
10:30 pm | American Dad | Klaustastrophe.tv
11:00 pm | Family Guy | Forget Me Not
11:30 pm | Family Guy | You Can't Do That on Television, Peter
12:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
12:30 am | Eric Andre Show | Dennis Rodman; Haley Joel Osment
12:45 am | Eric Andre Show | Jack Black; Jennette McCurdy
 1:00 am | Aqua Teen | Interfection
 1:15 am | Aqua Teen | PDA
 1:30 am | Family Guy | Forget Me Not
 2:00 am | Family Guy | You Can't Do That on Television, Peter
 2:30 am | American Dad | (You Gotta) Strike for Your Right
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | Eric Andre Show | Dennis Rodman; Haley Joel Osment
 3:45 am | Eric Andre Show | Jack Black; Jennette McCurdy
 4:00 am | Black Jesus | The Real Jesus of Compton
 4:30 am | Aqua Teen | Interfection
 4:45 am | Aqua Teen | PDA
 5:00 am | Bob's Burgers | Sit Me Baby One More Time
 5:30 am | Bob's Burgers | Y Tu Ga-Ga Tambien