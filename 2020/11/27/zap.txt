# 2020-11-27
 6:00 am | Amazing World of Gumball | The Law; The Allergy
 6:30 am | Amazing World of Gumball | The Mothers; The Password
 7:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 7:30 am | Amazing World of Gumball | The Mirror; The Burden
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 8:30 am | Teen Titans Go! | Orangins
 8:45 am | Teen Titans Go! | Classic Titans
 9:00 am | Teen Titans Go! Vs. Teen Titans | 
10:45 am | Craig of the Creek | Secret in a Bottle
11:00 am | Teen Titans Go! | The Spice Game
11:15 am | Teen Titans Go! | I'm the Sauce
11:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
12:00 pm | Teen Titans Go! | Accept the Next Proposition You Hear
12:15 pm | Teen Titans Go! | BBBDay!
12:30 pm | Teen Titans Go! | Black Friday
12:45 pm | Teen Titans Go! | Baby Mouth
 1:00 pm | Amazing World of Gumball | The Pest
 1:15 pm | Amazing World of Gumball | The Advice
 1:30 pm | Amazing World of Gumball | The Hug
 1:45 pm | Amazing World of Gumball | The Signal
 2:00 pm | Amazing World of Gumball | The Origins
 2:15 pm | Amazing World of Gumball | The Origins
 2:30 pm | Amazing World of Gumball | The Girlfriend
 2:45 pm | Amazing World of Gumball | The Compilation
 3:00 pm | Amazing World of Gumball | The Parasite
 3:15 pm | Amazing World of Gumball | The Detective
 3:30 pm | Amazing World of Gumball | The Love
 3:45 pm | Amazing World of Gumball | The Fury
 4:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 4:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 5:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 5:30 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 5:45 pm | Teen Titans Go! | Lil' Dimples
 6:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 7:00 pm | Amazing World of Gumball | The Neighbor
 7:15 pm | Amazing World of Gumball | The Pact
 7:30 pm | Amazing World of Gumball | The Candidate
 7:45 pm | Amazing World of Gumball | The Anybody
 8:00 pm | Amazing World of Gumball | The Kids; The Fan
 8:30 pm | Amazing World of Gumball | The Recipe; The Puppy