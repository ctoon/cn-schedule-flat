# 2020-11-28
 6:00 am | Teen Titans Go! | Wally T
 6:15 am | Teen Titans Go! | Snuggle Time
 6:30 am | Teen Titans Go! | The Scoop
 6:45 am | Teen Titans Go! | Chicken in the Cradle
 7:00 am | Teen Titans Go! | Flashback
 7:30 am | Teen Titans Go! | Kabooms
 7:45 am | Teen Titans Go! | Permanent Record
 8:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 8:30 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 9:30 am | Total DramaRama | Gobble Head
 9:45 am | Total DramaRama | OWW
10:00 am | Teen Titans Go! | Teen Titans Roar!
10:15 am | Teen Titans Go! | Don't Be an Icarus
10:30 am | Teen Titans Go! | Mo' Money Mo' Problems
10:45 am | Teen Titans Go! | Master Detective
11:00 am | Amazing World of Gumball | The Goons; The Secret
11:30 am | Amazing World of Gumball | The Sock; The Genius
12:00 pm | Amazing World of Gumball | The Third; The Debt
12:30 pm | Amazing World of Gumball | The Pressure; The Painting
 1:00 pm | Amazing World of Gumball | The Responsible; The Dress
 1:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:00 pm | Amazing World of Gumball | The Mystery; The Prank
 2:30 pm | Amazing World of Gumball | The Gi; The Kiss
 3:00 pm | Teen Titans Go! | Toddler Titans...Yay!
 3:15 pm | Teen Titans Go! | The Streak, Part 1
 3:30 pm | Teen Titans Go! | The Streak, Part 2
 3:45 pm | Teen Titans Go! | Teen Titans Go! Titans vs. Santa
 4:45 pm | Teen Titans Go! | Beast Boy on a Shelf
 5:00 pm | Teen Titans Go! | Christmas Crusaders
 5:15 pm | Teen Titans Go! Vs. Teen Titans | 
 7:00 pm | The LEGO Batman Movie | 