# 2020-11-22
 6:00 am | Teen Titans Go! | Black Friday
 6:15 am | Teen Titans Go! | I'm the Sauce
 6:30 am | Bakugan: Battle Planet | Beastie Team Battle; Ajit and Lia
 7:00 am | Teen Titans Go! | Accept the Next Proposition You Hear
 7:15 am | Teen Titans Go! | Squash & Stretch
 7:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 7:45 am | Teen Titans Go! | The Fourth Wall
 8:00 am | Teen Titans Go! | Two Parter: Part One
 8:15 am | Teen Titans Go! | Two Parter: Part Two
 8:30 am | Teen Titans Go! | Garage Sale
 8:45 am | Teen Titans Go! | Secret Garden
 9:00 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:15 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 9:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:45 am | Teen Titans Go! | The Chaff
10:00 am | Teen Titans Go! | Bottle Episode
10:15 am | Teen Titans Go! | History Lesson
10:30 am | Teen Titans Go! | Pyramid Scheme
10:45 am | Teen Titans Go! | The Art of Ninjutsu
11:00 am | Amazing World of Gumball | The Points
11:15 am | Amazing World of Gumball | The Bus
11:30 am | Amazing World of Gumball | The Traitor
11:45 am | Amazing World of Gumball | The Night
12:00 pm | Amazing World of Gumball | The Advice
12:15 pm | Amazing World of Gumball | The Misunderstandings
12:30 pm | Amazing World of Gumball | The Signal
12:45 pm | Amazing World of Gumball | The Roots
 1:00 pm | Amazing World of Gumball | The Blame
 1:15 pm | Amazing World of Gumball | The Stories
 1:30 pm | Amazing World of Gumball | The Code
 1:45 pm | Amazing World of Gumball | The Test
 2:00 pm | Amazing World of Gumball | The Detective
 2:15 pm | Amazing World of Gumball | The Boredom
 2:30 pm | Amazing World of Gumball | The Fury
 2:45 pm | Amazing World of Gumball | The Slide
 3:00 pm | Amazing World of Gumball | The Compilation
 3:15 pm | Amazing World of Gumball | The Choices
 3:30 pm | Amazing World of Gumball | The Disaster
 3:45 pm | Amazing World of Gumball | The Re-Run
 4:00 pm | Teen Titans Go! | Serious Business; Thanksgiving
 4:30 pm | Teen Titans Go! | Demon Prom
 4:45 pm | Teen Titans Go! | Thanksgetting
 5:00 pm | Teen Titans Go! | Finally a Lesson
 5:15 pm | Teen Titans Go! | Think About Your Future
 5:30 pm | Teen Titans Go! | Arms Race With Legs
 5:45 pm | Teen Titans Go! | Booty Scooty
 6:00 pm | Teen Titans Go! | Obinray
 6:15 pm | Teen Titans Go! | Who's Laughing Now
 6:30 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
 6:45 pm | Teen Titans Go! | Little Elvis
 7:00 pm | LEGO DC Shazam: Magic and Monsters | 
 8:45 pm | Amazing World of Gumball | The Gumball Chronicles: Ancestor Act