# 2020-11-13
 9:00 pm | Bob's Burgers | Human Flesh
 9:30 pm | American Dad | Fleabiscuit
10:00 pm | American Dad | The Future Is Borax
10:30 pm | Rick and Morty | Pilot
11:00 pm | Family Guy | The Giggity Wife
11:30 pm | Family Guy | Valentine's Day in Quahog
12:00 am | Eric Andre Show | The Eric Andre Show: The Making of Season Five
12:15 am | Eric Andre Show | A King Is Born
12:30 am | Eric Andre Show | Hannibal Quits
12:45 am | Eric Andre Show | You Got Served
 1:00 am | Eric Andre Show | Lizzo Up
 1:15 am | Eric Andre Show | The A$AP Ferg Show
 1:30 am | Eric Andre Show | Blannibal Quits
 1:45 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 2:00 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
 2:15 am | Eric Andre Show | Tichina Arnold; Steve Schirrpa
 2:30 am | Eric Andre Show | Jesse Williams; Jillian Michaels
 2:45 am | Eric Andre Show | Warren G; Kelly Osbourne
 3:00 am | Eric Andre Show | Raymond Cruz; Amber Rose
 3:15 am | Eric Andre Show | Chris Jericho; Roy Hibbert; Flavor Flav
 3:30 am | Eric Andre Show | Dennis Rodman; Haley Joel Osment
 3:45 am | Eric Andre Show | Jack Black; Jennette McCurdy
 4:00 am | As Seen on Adult Swim | As Seen on Adult Swim
 4:15 am | Digikiss | Digikiss
 4:30 am | American Dad | Fleabiscuit
 5:00 am | American Dad | The Future Is Borax
 5:30 am | Bob's Burgers | Human Flesh