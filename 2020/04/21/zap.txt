# 2020-04-21
 6:00 am | Amazing World of Gumball | The Internet; The Plan
 6:30 am | Amazing World of Gumball | The World; The Finale
 7:00 am | Amazing World of Gumball | The Advice
 7:15 am | Amazing World of Gumball | The Signal
 7:30 am | Amazing World of Gumball | The Girlfriend
 7:45 am | Amazing World of Gumball | The Parasite
 8:00 am | Apple & Onion | Onionless
 8:15 am | Apple & Onion | Lil Noodle
 8:30 am | Apple & Onion | Baby Boi TP
 8:45 am | Apple & Onion | Not Funny
 9:00 am | Teen Titans Go! | The Streak, Part 1
 9:15 am | Teen Titans Go! | The Streak, Part 2
 9:30 am | Teen Titans Go! | BBRAE
10:00 am | Craig of the Creek | Memories of Bobby
10:15 am | Craig of the Creek | You're It
10:30 am | Craig of the Creek | Jacob of the Creek
10:45 am | Craig of the Creek | Itch to Explore
11:00 am | Total DramaRama | Duncan Disorderly
11:15 am | Total DramaRama | Stop! Hamster Time
11:30 am | Total DramaRama | Soother or Later
11:45 am | Total DramaRama | Driving Miss Crazy
12:00 pm | Victor and Valentino | Band for Life
12:15 pm | Victor and Valentino | Dead Ringer
12:30 pm | Victor and Valentino | Tez Says
12:45 pm | Victor and Valentino | Chata's Quinta Quinceañera
 1:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 1:30 pm | Amazing World of Gumball | The Crew
 1:45 pm | Amazing World of Gumball | The Others
 2:00 pm | Amazing World of Gumball | The Pony; The Storm
 2:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 3:00 pm | Total DramaRama | Apoca-lice Now
 3:15 pm | Total DramaRama | Harold Swatter and the Goblet of Flies
 3:30 pm | Total DramaRama | Gnome More Mister Nice Guy
 3:45 pm | Total DramaRama | Stink. Stank. Stunk
 4:00 pm | Teen Titans Go! | Employee of the Month Redux
 4:15 pm | Teen Titans Go! | Labor Day
 4:30 pm | Teen Titans Go! | Orangins
 4:45 pm | Teen Titans Go! | Ones and Zeroes
 5:00 pm | Craig of the Creek | Deep Creek Salvage
 5:15 pm | Craig of the Creek | Turning the Tables
 5:30 pm | Craig of the Creek | Dibs Court
 5:45 pm | Craig of the Creek | Camper on the Run
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | Apple & Onion | Apple's Focus
 7:15 pm | Apple & Onion | Face Your Fears
 7:30 pm | Amazing World of Gumball | The Boombox; The Castle