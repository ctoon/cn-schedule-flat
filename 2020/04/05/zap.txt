# 2020-04-05
 6:00 am | Amazing World of Gumball | The Boombox; The Castle
 6:30 am | Bakugan: Battle Planet | The Lady and the Queen, The/AB's Silver Screen Debut
 7:00 am | Transformers: Cyberverse | The Trial
 7:15 am | Transformers: Cyberverse | The Prisoner
 7:30 am | Mao Mao: Heroes of Pure Heart | Bao Bao's Revenge
 7:45 am | Mao Mao: Heroes of Pure Heart | The Truth Stinks
 8:00 am | Mao Mao: Heroes of Pure Heart | Outfoxed
 8:15 am | Mao Mao: Heroes of Pure Heart | Thumb War
 8:30 am | Teen Titans Go! | Love Monsters; Baby Hands
 9:00 am | Teen Titans Go! | Caramel Apples; Halloween
 9:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
10:00 am | Ben 10 | Digital Quality
10:15 am | Ben 10 | Tim Buk-TV
10:30 am | Power Players | Freeze!
10:45 am | Power Players | King Axel
11:00 am | Amazing World of Gumball | The Tape; The Sweaters
11:30 am | Amazing World of Gumball | The Internet; The Plan
12:00 pm | Amazing World of Gumball | The World; The Finale
12:30 pm | Amazing World of Gumball | The Kids; The Fan
 1:00 pm | Total DramaRama | Ant We All Just Get Along
 1:15 pm | Total DramaRama | Aquarium for a Dream
 1:30 pm | Total DramaRama | Sharing Is Caring
 1:45 pm | Total DramaRama | All Up in Your Drill
 2:00 pm | Teen Titans Go! | Friendship; Vegetables
 2:30 pm | Teen Titans Go! | The Mask; Slumber Party
 3:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 3:30 pm | Teen Titans Go! | Video Game References; Cool School
 4:00 pm | DC Super Hero Girls | #AllAboutZee
 4:15 pm | DC Super Hero Girls | #LivingTheNightmare
 4:30 pm | Unikitty | Sunken Treasure
 4:45 pm | Unikitty | Unikitty and the Ice Pop Factory
 5:00 pm | Amazing World of Gumball | The Coach; The Joy
 5:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 6:00 pm | Teen Titans Go! | Road Trip; The Best Robin
 6:30 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 7:00 pm | Adventure Time | Slumber Party Panic; Trouble in Lumpy Space
 7:30 pm | Adventure Time | The Enchiridion!; The Jiggler