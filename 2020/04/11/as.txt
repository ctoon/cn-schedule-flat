# 2020-04-11
 8:00 pm | Dragon Ball Z Kai | The Hunt for Doctor Gero! Discover the Hidden Laboratory!
 8:30 pm | Dragon Ball Z Kai | Number 17 and Number 18! The Androids Awaken!
 9:00 pm | American Dad | Every Which Way But Lose
 9:30 pm | American Dad | Weiner of Our Discontent
10:00 pm | Bob's Burgers | Cheer Up Sleepy Gene
10:30 pm | Family Guy | Stewie Kills Lois Part 1
11:00 pm | Family Guy | Lois Kills Stewie Part 2
11:30 pm | Dragon Ball Z Kai | Cell on the Verge of Defeat! Krillin, Destroy Android 18!
12:00 am | Dragon Ball Z Kai | The Battle Turns for the Worst! Cell Attacks Android 18!
12:30 am | Dragon Ball Z Kai | The Tables Are Turned! Witness the Power of Perfection!
 1:00 am | Dragon Ball Z Kai | Vegeta's Final Push! Defeat the Invincible Cell!
 1:30 am | Dragon Ball Z Kai | The Strongest Super Saiyan! Trunks' Power Unleashed!
 2:00 am | Dragon Ball Z Kai | Cell Invades the Airwaves! Announcing, "The Cell Games!"
 2:30 am | Dragon Ball Z Kai | Training Complete! Goku Sizes Up the Competition!
 3:00 am | Genndy Tartakovksy's Primal | River of Snakes
 3:30 am | Samurai Jack | XCVII
 4:00 am | The Boondocks | It's a Black President, Huey Freeman
 4:30 am | The Venture Brothers | Tanks for Nuthin'
 5:00 am | Aqua Teen Hunger Force Forever | Sweet C
 5:15 am | Aqua Teen Hunger Force Forever | Knapsack!
 5:30 am | Bob's Burgers | Cheer Up Sleepy Gene