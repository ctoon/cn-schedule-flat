# 2020-04-11
 6:00 am | Amazing World of Gumball | The Inquisition
 6:15 am | Amazing World of Gumball | The Ad
 6:30 am | Amazing World of Gumball | The Web
 6:45 am | Amazing World of Gumball | The Understanding
 7:00 am | Teen Titans Go! | Teen Titans Vroom
 7:30 am | Teen Titans Go! | We're Off to Get Awards
 7:45 am | Teen Titans Go! | Record Book
 8:00 am | Teen Titans Go! | The Titans Go Casual
 8:15 am | Teen Titans Go! | Walk Away
 8:30 am | Teen Titans Go! | Magic Man
 8:45 am | Teen Titans Go! | TV Knight 5
 9:00 am | Total DramaRama | Gnome More Mister Nice Guy
 9:15 am | Total DramaRama | Look Who's Clocking
 9:30 am | Apple & Onion | Floored
 9:45 am | Apple & Onion | Election Day
10:00 am | Teen Titans Go! | Teen Titans Roar!
10:15 am | Teen Titans Go! | Rain on Your Wedding Day
10:30 am | ThunderCats Roar | Study Time
10:45 am | ThunderCats Roar | Mumm-Ra, the Ever-Living
11:00 am | Teen Titans Go! | Easter Extravaganza
12:00 pm | Teen Titans Go! Vs. Teen Titans | 
 1:45 pm | Amazing World of Gumball | The Decisions
 2:00 pm | Amazing World of Gumball | The Agent
 2:15 pm | Amazing World of Gumball | The Spinoffs
 2:30 pm | Amazing World of Gumball | The BFFS
 2:45 pm | Amazing World of Gumball | The Potion
 3:00 pm | Amazing World of Gumball | The Ghouls
 3:15 pm | Amazing World of Gumball | The Intelligence
 3:30 pm | Amazing World of Gumball | The Heart
 3:45 pm | Amazing World of Gumball | The Schooling
 4:00 pm | Amazing World of Gumball | The Possession
 4:15 pm | Amazing World of Gumball | The Founder
 4:30 pm | Amazing World of Gumball | The Mess
 4:45 pm | Amazing World of Gumball | The Parents
 5:00 pm | Teen Titans Go! | Easter Extravaganza
 6:00 pm | Teen Titans Go! Vs. Teen Titans | 
 7:45 pm | ThunderCats Roar | Mumm-Ra, the Ever-Living