# 2020-04-04
 8:00 pm | Dragon Ball Z Kai | No Victory for Android 19! Enter Super Vegeta!
 8:30 pm | Dragon Ball Z Kai | Piccolo's Assault! Android 20 and the Twisted Future!
 9:00 pm | American Dad | One Little Word
 9:30 pm | American Dad | Roy Rodgers McFreely
10:00 pm | Bob's Burgers | The Wolf of Wharf Street
10:30 pm | Family Guy | The Tan Aquatic with Steve Zissou
11:00 pm | Family Guy | Airport '07
11:30 pm | My Hero Academia | Prepping for the School Festival Is the Funnest Part
12:00 am | Sword Art Online Alicization War of Underworld | Ray of Light
12:30 am | Demon Slayer: Kimetsu no Yaiba | Hashira Meeting
 1:00 am | Food Wars! | The Magician Returns
 1:30 am | Black Clover | Humans Who Can Be Trusted
 2:00 am | JoJo's Bizarre Adventure: Golden Wind | The Mystery of Emperor Crimson
 2:30 am | Naruto:Shippuden | Power - Episode 2
 3:00 am | Genndy Tartakovksy's Primal | Spear and Fang
 3:30 am | Samurai Jack | XCVI
 4:00 am | The Boondocks | Home Alone
 4:30 am | The Venture Brothers | Rapacity in Blue
 5:00 am | Aqua TV Show Show | Spacecadeuce
 5:15 am | Aqua Teen Hunger Force Forever | Brain Fairy
 5:30 am | Bob's Burgers | The Wolf of Wharf Street