# 2020-04-04
 6:00 am | Amazing World of Gumball | The Phone; The Job
 6:30 am | Amazing World of Gumball | The Hero; The Photo
 7:00 am | Amazing World of Gumball | The Words; The Apology
 7:30 am | Amazing World of Gumball | The Tag; The Lesson
 8:00 am | Teen Titans Go! | Brain Food; In and Out
 8:30 am | Teen Titans Go! | The Titans Go Casual
 8:45 am | Teen Titans Go! | The Groover
 9:00 am | Total DramaRama | Lie-Ranosaurus Wrecked
 9:15 am | Total DramaRama | The Tooth About Zombies
 9:30 am | Apple & Onion | Floored
 9:45 am | Apple & Onion | Rotten Apple
10:00 am | Teen Titans Go! | Teen Titans Roar!
10:15 am | Teen Titans Go! | Rain on Your Wedding Day
10:30 am | ThunderCats Roar | Mandora -- The Evil Chaser
10:45 am | ThunderCats Roar | Dr. Dometome
11:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
11:30 am | Teen Titans Go! | Dreams; Grandma Voice
12:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
12:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 1:00 pm | Amazing World of Gumball | The Watch; The Bet
 1:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 2:00 pm | Amazing World of Gumball | The Authority; The Virus
 2:30 pm | Amazing World of Gumball | The Pony; The Storm
 3:00 pm | Teen Titans Go! | Brian; Nature
 3:30 pm | Teen Titans Go! | Salty Codgers; Knowledge
 4:00 pm | Craig of the Creek | The Great Fossil Rush
 4:15 pm | Craig of the Creek | Alone Quest
 4:30 pm | Victor and Valentino | Chata's Quinta Quinceañera
 4:45 pm | Victor and Valentino | Brotherly Love
 5:00 pm | Total DramaRama | The Bad Guy Busters
 5:15 pm | Total DramaRama | Cuttin' Corners
 5:30 pm | Total DramaRama | Cone in 60 Seconds
 5:45 pm | Total DramaRama | Free Chili
 6:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 6:30 pm | Amazing World of Gumball | The Limit; The Game
 7:00 pm | Amazing World of Gumball | The Promise; The Voice
 7:30 pm | Apple & Onion | Rotten Apple
 7:45 pm | Apple & Onion | Floored