# 2020-04-13
 6:00 am | Amazing World of Gumball | The Mustache; The Date
 6:30 am | Amazing World of Gumball | The Club; The Wand
 7:00 am | Amazing World of Gumball | The Return; The Nemesis
 7:30 am | Amazing World of Gumball | The Crew; The Others
 8:00 am | Apple & Onion | Inbetween; The Election Day
 8:30 am | Apple & Onion | Apple's Formula; Party Popper
 9:00 am | Teen Titans Go! | Obinray; Spice Game
 9:30 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice; I'm the Sauce
10:00 am | Craig Of The Creek | Return Of The Honeysuckle Rangers; Creek Cart Racers
10:30 am | Craig of the Creek | Secret Book Club Fort Williams
11:00 am | Total DramaRama | Hic Hic Hooray; All Up In Your Drill
11:30 am | Total DramaRama | Bananas & Cheese; Snow Way Out
12:00 pm | Victor And Valentino | Guillermo's Gathering; Churro Kings
12:30 pm | Victor and Valentino | Know It All ;Escape From Bebe Bay
 1:00 pm | Amazing World of Gumball | The Third; The Debt
 1:30 pm | Amazing World of Gumball | The Pressure; The Painting
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Total DramaRama | Invasion Of The Booger Snatchers; Paint That A Shame
 3:30 pm | Total DramaRama | Snots Landing; Wristy Business
 4:00 pm | Teen Titans Go! | Animals: It's Just a Word!; Booty Scooty
 4:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 5:00 pm | Craig of the Creek | Climb; The Alone Quest
 5:30 pm | Craig of the Creek | Big Pinchy; Memories of Bobby
 6:00 pm | Teen Titans Go! | Oh Yeah!; The Cruel Giggling Ghoul
 6:30 pm | Teen Titans Go! | Bottle Episode; Riding the Dragon
 7:00 pm | Apple & Onion | Apple's In Charge; Hotdog's Movie Premiere
 7:30 pm | Amazing World of Gumball | The Goons; The Secret