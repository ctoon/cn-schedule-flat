# 2020-04-13
 6:00 am | Amazing World of Gumball | The Mustache; The Date
 6:30 am | Amazing World of Gumball | The Club; The Wand
 7:00 am | Mao Mao: Heroes of Pure Heart | Flyaway
 7:15 am | Mao Mao: Heroes of Pure Heart | No Shortcuts
 7:30 am | Mao Mao: Heroes of Pure Heart | Baost in Show
 7:45 am | Mao Mao: Heroes of Pure Heart | Not Impressed
 8:00 am | Teen Titans Go! | Two Parter: Part One
 8:15 am | Teen Titans Go! | Two Parter: Part Two
 8:30 am | Teen Titans Go! | The Croissant
 8:45 am | Teen Titans Go! | Arms Race With Legs
 9:00 am | Teen Titans Go! | The Spice Game
 9:15 am | Teen Titans Go! | Obinray
 9:30 am | Teen Titans Go! | I'm the Sauce
 9:45 am | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice
10:00 am | Total DramaRama | Hic Hic Hooray
10:15 am | Total DramaRama | All Up in Your Drill
10:30 am | Total DramaRama | Bananas & Cheese
10:45 am | Total DramaRama | Snow Way Out
11:00 am | Teen Titans Go! | Grube's Fairytales
11:15 am | Teen Titans Go! | The Art of Ninjutsu
11:30 am | Teen Titans Go! | A Farce
11:45 am | Teen Titans Go! | Think About Your Future
12:00 pm | Victor and Valentino | Churro Kings
12:15 pm | Victor and Valentino | Guillermo's Gathering
12:30 pm | Victor and Valentino | Know It All
12:45 pm | Victor and Valentino | Escape From Bebe Bay
 1:00 pm | Amazing World of Gumball | The Third; The Debt
 1:30 pm | Amazing World of Gumball | The Pressure; The Painting
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Total DramaRama | Paint That a Shame
 3:15 pm | Total DramaRama | Invasion of the Booger Snatchers
 3:30 pm | Total DramaRama | Snots Landing
 3:45 pm | Total DramaRama | Wristy Business
 4:00 pm | Teen Titans Go! | Animals: It's Just a Word!
 4:15 pm | Teen Titans Go! | Booty Scooty
 4:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 4:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 5:00 pm | Craig of the Creek | The Climb
 5:15 pm | Craig of the Creek | Alone Quest
 5:30 pm | Craig of the Creek | Big Pinchy
 5:45 pm | Craig of the Creek | Memories of Bobby
 6:00 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:15 pm | Teen Titans Go! | Oh Yeah!
 6:30 pm | Teen Titans Go! | Bottle Episode
 6:45 pm | Teen Titans Go! | Riding the Dragon
 7:00 pm | Apple & Onion | Hotdog's Movie Premiere
 7:15 pm | Apple & Onion | Apple's in Charge
 7:30 pm | Amazing World of Gumball | The Goons; The Secret