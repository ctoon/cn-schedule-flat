# 2020-04-29
 6:00 am | Amazing World of Gumball | The Downer; The Egg
 6:30 am | Amazing World of Gumball | The Triangle; The Money
 7:00 am | Amazing World of Gumball | The Disaster
 7:15 am | Amazing World of Gumball | The Re-Run
 7:30 am | Amazing World of Gumball | The Loophole
 7:45 am | Amazing World of Gumball | The Fuss
 8:00 am | Apple & Onion | 4 on 1
 8:15 am | Apple & Onion | Apple's in Trouble
 8:30 am | Apple & Onion | Block Party
 8:45 am | Apple & Onion | Apple's Focus
 9:00 am | Teen Titans Go! | The Power of Shrimps
 9:15 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 9:30 am | Teen Titans Go! | My Name Is Jose
 9:45 am | Teen Titans Go! | Business Ethics Wink Wink
10:00 am | Craig of the Creek | Summer Wish
10:15 am | Craig of the Creek | The Future Is Cardboard
10:30 am | Craig of the Creek | Turning the Tables
10:45 am | Craig of the Creek | Dog Decider
11:00 am | Total DramaRama | Cluckwork Orange
11:15 am | Total DramaRama | Cone in 60 Seconds
11:30 am | Total DramaRama | Free Chili
11:45 am | Total DramaRama | The Bad Guy Busters
12:00 pm | Victor and Valentino | Chata's Quinta Quinceañera
12:15 pm | Victor and Valentino | The Dark Room
12:30 pm | Victor and Valentino | Brotherly Love
12:45 pm | Victor and Valentino | The Boy Who Cried Lechuza
 1:00 pm | Amazing World of Gumball | The Mothers; The Password
 1:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:00 pm | Amazing World of Gumball | The Mirror; The Burden
 2:30 pm | Amazing World of Gumball | The Bros; The Man
 3:00 pm | Total DramaRama | Cuttin' Corners
 3:15 pm | Total DramaRama | A Ninjustice to Harold
 3:30 pm | Total DramaRama | Sharing Is Caring
 3:45 pm | Total DramaRama | Having the Timeout of Our Lives
 4:00 pm | Teen Titans Go! | Curse of the Booty Scooty
 4:15 pm | Teen Titans Go! | Cartoon Feud
 4:30 pm | Teen Titans Go! | Them Soviet Boys
 4:45 pm | Teen Titans Go! | Don't Be an Icarus
 5:00 pm | Craig of the Creek | Sour Candy Trials
 5:15 pm | Craig of the Creek | Sunday Clothes
 5:30 pm | Craig of the Creek | Council of the Creek
 5:45 pm | Craig of the Creek | Escape From Family Dinner
 6:00 pm | Teen Titans Go! | TV Knight 3
 6:15 pm | Teen Titans Go! | The Fight
 6:30 pm | Teen Titans Go! | Bro-Pocalypse
 6:45 pm | Teen Titans Go! | Genie President
 7:00 pm | Apple & Onion | Heatwave
 7:15 pm | Apple & Onion | Appleoni
 7:30 pm | Amazing World of Gumball | The Society; The Spoiler