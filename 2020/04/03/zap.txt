# 2020-04-03
 6:00 am | Amazing World of Gumball | The Cage
 6:15 am | Amazing World of Gumball | The Parents
 6:30 am | Amazing World of Gumball | The Rival
 6:45 am | Amazing World of Gumball | The Founder
 7:00 am | Mao Mao: Heroes of Pure Heart | All by Mao Self
 7:15 am | Mao Mao: Heroes of Pure Heart | Flyaway
 7:30 am | Mao Mao: Heroes of Pure Heart | Legend of Torbaclaun
 7:45 am | Mao Mao: Heroes of Pure Heart | Baost in Show
 8:00 am | Teen Titans Go! | Dreams; Grandma Voice
 8:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 9:00 am | Teen Titans Go! | Mr. Butt; Man Person
 9:30 am | Teen Titans Go! | Pirates; I See You
10:00 am | Craig of the Creek | Under the Overpass
10:15 am | Craig of the Creek | The Climb
10:30 am | Craig of the Creek | The Invitation
10:45 am | Craig of the Creek | Big Pinchy
11:00 am | Teen Titans Go! | No Power; Sidekick
11:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
12:00 pm | Victor and Valentino | Suerte
12:15 pm | Victor and Valentino | It Grows
12:30 pm | Victor and Valentino | Lonely Haunts Club
12:45 pm | Victor and Valentino | A New Don
 1:00 pm | Amazing World of Gumball | The Neighbor
 1:15 pm | Amazing World of Gumball | The Transformation
 1:30 pm | Amazing World of Gumball | The Pact
 1:45 pm | Amazing World of Gumball | The Understanding
 2:00 pm | Amazing World of Gumball | The Signature
 2:15 pm | Amazing World of Gumball | The Ad
 2:30 pm | Amazing World of Gumball | The Candidate
 2:45 pm | Amazing World of Gumball | The Slip
 3:00 pm | Total DramaRama | The Never Gwending Story
 3:15 pm | Total DramaRama | Mutt Ado About Owen
 3:30 pm | Total DramaRama | Too Much of a Goo'd Thing
 3:45 pm | Total DramaRama | Simons Are Forever
 4:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 4:30 pm | Teen Titans Go! | Waffles; Opposites
 5:00 pm | Craig of the Creek | Dog Decider
 5:15 pm | Craig of the Creek | JPony
 5:30 pm | Craig of the Creek | Bring Out Your Beast
 5:45 pm | Craig of the Creek | Ace of Squares
 6:00 pm | Total DramaRama | Exercising the Demons
 6:15 pm | Total DramaRama | An Egg-stremely Bad Idea
 6:30 pm | Apple & Onion | Rotten Apple
 6:45 pm | Apple & Onion | Pulling Your Weight
 7:00 pm | Teen Titans Go! | Rain on Your Wedding Day
 7:15 pm | Teen Titans Go! | The Titans Go Casual
 7:30 pm | ThunderCats Roar | ThunderSlobs
 7:45 pm | ThunderCats Roar | Working Grrrl