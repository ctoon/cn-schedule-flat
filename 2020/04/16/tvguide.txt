# 2020-04-16
 6:00 am | Amazing World of Gumball | The Responsible; The Dress
 6:30 am | Amazing World of Gumball | The Laziest; The Ghost
 7:00 am | Amazing World of Gumball | The Parking; The Upgrade
 7:30 am | Amazing World of Gumball | The Comic; The Romantic
 8:00 am | Apple & Onion | The Fly; 4 on 1
 8:30 am | Apple & Onion | Dragonhead; Apple's in Trouble
 9:00 am | Teen Titans Go! | Oregon Trail; Garage Sale
 9:30 am | Teen Titans Go! | Snuggle Time; Secret Garden
10:00 am | Craig of the Creek | Climb; The Alone Quest
10:30 am | Craig of the Creek | Big Pinchy; Memories of Bobby
11:00 am | Total DramaRama | Know It All; Melter Skelter
11:30 am | Total DramaRama | A Licking Time Bomb; The Never Gwending Story
12:00 pm | Victor and Valentino | Balloon Boys Forever Ever
12:30 pm | Victor And Valentino | Tree Buds; Lonely Haunts Club 2: Doll Island
 1:00 pm | Amazing World of Gumball | The Ape; The Poltergeist
 1:30 pm | Amazing World of Gumball | The Quest; The Spoon
 2:00 pm | Amazing World of Gumball | The Car; The Curse
 2:30 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:00 pm | Total DramaRama | Hic Hic Hooray; All Up In Your Drill
 3:30 pm | Total DramaRama | Bananas & Cheese; Snow Way Out
 4:00 pm | Teen Titans Go! | Obinray; Spice Game
 4:30 pm | Teen Titans Go! | Batman vs. Teen Titans: Dark Injustice; I'm the Sauce
 5:00 pm | Craig of the Creek | Jextra Perrestrial Kelsey the Elder
 5:30 pm | Craig of the Creek | Takeout Mission; The Sour Candy Trials
 6:00 pm | Teen Titans Go! | The Art of Ninjutsu; Grube's Fairytales
 6:30 pm | Teen Titans Go! | Think About Your Future; A Farce
 7:00 pm | Apple & Onion | Bottle Catch; Not Funny
 7:30 pm | Amazing World of Gumball | The Third; The Debt