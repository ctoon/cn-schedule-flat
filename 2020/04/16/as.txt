# 2020-04-16
 8:00 pm | Home Movies | Storm Warning
 8:30 pm | Bob's Burgers | Mission Impos-slug-ble
 9:00 pm | Bob's Burgers | Something Old, Something New, Something Bob Caters for You
 9:30 pm | Rick and Morty | Auto Erotic Assimilation
10:00 pm | American Dad | Man in the Moonbounce
10:30 pm | American Dad | Shallow Vows
11:00 pm | Family Guy | Love Blactually
11:30 pm | Family Guy | I Dream of Jesus
12:00 am | Three Busy Debras | Sleepover!
12:15 am | Beef House | Boro
12:30 am | Aqua Something You Know Whatever | Rocket Horse and Jet Chicken
12:45 am | Squidbillies | Tortuga de Mentiras
 1:00 am | The Venture Brothers | The High Cost of Loathing
 1:30 am | Family Guy | Love Blactually
 2:00 am | Family Guy | I Dream of Jesus
 2:30 am | American Dad | Man in the Moonbounce
 3:00 am | Rick and Morty | Auto Erotic Assimilation
 3:30 am | Three Busy Debras | Sleepover!
 3:45 am | Beef House | Boro
 4:00 am | Space Man | Space Man
 4:15 am | Hot Streets | Camp Hot Streets
 4:30 am | Aqua Something You Know Whatever | Rocket Horse and Jet Chicken
 4:45 am | Squidbillies | Tortuga de Mentiras
 5:00 am | Bob's Burgers | Mission Impos-slug-ble
 5:30 am | Bob's Burgers | Something Old, Something New, Something Bob Caters for You