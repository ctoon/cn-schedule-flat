# 2020-04-30
 6:00 am | Amazing World of Gumball | The Recipe; The Puppy
 6:30 am | Amazing World of Gumball | The Name; The Extras
 7:00 am | Amazing World of Gumball | The Stories; The News
 7:30 am | Amazing World of Gumball | The Guy; The Vase
 8:00 am | Apple & Onion | Burger's Trampoline/Fun Proof
 8:30 am | Apple & Onion | Party Popper; Lil Noodle
 9:00 am | Teen Titans Go! | How's This For A Special? Spaaaace
 9:30 am | Teen Titans Go! | I Used to Be a Peoples; Lil' Dimples
10:00 am | Craig of the Creek | Mystery of the Timekeeper; The Jessica Goes to the Creek
10:30 am | Craig of the Creek | Return of the Honeysuckle Rangers; Too Many Treasures
11:00 am | Total DramaRama | Date; The That's a Wrap
11:30 am | Total DramaRama | Aquarium for a Dream Tiger Fail
12:00 pm | Victor And Valentino | Legend Of The Hidden Skate Park; The Collector
12:30 pm | Victor And Valentino | Cleaning Day; Los Cadejos
 1:00 pm | Amazing World of Gumball | The Lie; The Pizza
 1:30 pm | Amazing World of Gumball | The Butterfly; The Question
 2:00 pm | Amazing World of Gumball | The Oracle; The Safety
 2:30 pm | Amazing World of Gumball | The Friend; The Saint
 3:00 pm | Total DramaRama | Lie-ranosaurus Wrecked; The Tooth About Zombies
 3:30 pm | Total DramaRama | An Egg-stremely Bad Idea; Exercising The Demons
 4:00 pm | Teen Titans Go! | Mo' Money Mo' Problems; Tower Renovation
 4:30 pm | Teen Titans Go! | Beast Girl; Quantum Fun
 5:00 pm | Craig of the Creek | Sparkle Cadet; Monster in the Garden
 5:30 pm | Craig of the Creek | Stink Bomb; The Curse
 6:00 pm | Teen Titans Go! | BBRBDAY ;Little Elvis
 6:30 pm | Teen Titans Go! | Tall Titan Tales; The Groover
 7:00 pm | Apple & Onion | Gyranoid of the Future; River of Gold
 7:30 pm | Amazing World of Gumball | The Kids; The Fan