# 2020-04-24
 6:00 am | Amazing World of Gumball | The Pony; The Storm
 6:30 am | Amazing World of Gumball | The Dream; The Sidekick
 7:00 am | Amazing World of Gumball | The Bus; The Night
 7:30 am | Amazing World of Gumball | The Roots; The Misunderstanding
 8:00 am | Apple & Onion | Inbetween; The Election Day
 8:30 am | Apple & Onion | Follow Your Dreams; Selfish Shellfish
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
10:00 am | Craig of the Creek | Deep Creek Salvage Turning the Tables
10:30 am | Craig of the Creek | Dibs Court Camper on the Run
11:00 am | Total DramaRama | Apoca-Lice Now Harold Swatter and the Goblet of Flies
11:30 am | Total DramaRama | Gnome More Mister Nice Guy Stink. Stank. Stunk
12:00 pm | Victor and Valentino | On Nagual Hill Cleaning Day
12:30 pm | Victor and Valentino | The Dance Reynaldo Dance Babysitter
 1:00 pm | Amazing World of Gumball | The Boombox; The Castle
 1:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 2:00 pm | Amazing World of Gumball | The Internet; The Plan
 2:30 pm | Amazing World of Gumball | The World; The Finale
 3:00 pm | Total DramaRama | Duncan Disorderly Stop! Hamster Time
 3:30 pm | Total DramaRama | Soother or Later Driving Miss Crazy
 4:00 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 4:30 pm | Teen Titans Go! | BBRAE
 5:00 pm | Craig of the Creek | Memories of Bobby You're It
 5:30 pm | Craig of the Creek | Jacob of the Creek Itch to Explore
 6:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 7:00 pm | Apple & Onion | Fun Proof; Sausage and Sweetie Smash
 7:30 pm | Amazing World of Gumball | The Bumpkin; Theflakers