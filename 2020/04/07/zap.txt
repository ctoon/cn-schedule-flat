# 2020-04-07
 6:00 am | Amazing World of Gumball | The Decisions
 6:15 am | Amazing World of Gumball | The Wicked
 6:30 am | Amazing World of Gumball | The Web
 6:45 am | Amazing World of Gumball | The Traitor
 7:00 am | Mao Mao: Heroes of Pure Heart | Meet Tanya Keys
 7:15 am | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 7:30 am | Mao Mao: Heroes of Pure Heart | Trading Day
 7:45 am | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
 8:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 8:30 am | Teen Titans Go! | Friendship; Vegetables
 9:00 am | Teen Titans Go! | The Mask; Slumber Party
 9:30 am | Teen Titans Go! | Road Trip; The Best Robin
10:00 am | Total DramaRama | Cluckwork Orange
10:15 am | Total DramaRama | Cone in 60 Seconds
10:30 am | Total DramaRama | Free Chili
10:45 am | Total DramaRama | The Bad Guy Busters
11:00 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
11:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:00 pm | Victor and Valentino | The Collector
12:15 pm | Victor and Valentino | Fistfull of Balloons
12:30 pm | Victor and Valentino | Los Cadejos
12:45 pm | Victor and Valentino | Love at First Bite
 1:00 pm | Amazing World of Gumball | The Future
 1:15 pm | Amazing World of Gumball | The Check
 1:30 pm | Amazing World of Gumball | The Wish
 1:45 pm | Amazing World of Gumball | The Pest
 2:00 pm | Amazing World of Gumball | The Inquisition
 2:15 pm | Amazing World of Gumball | The Hug
 2:30 pm | Amazing World of Gumball | The Revolt
 2:45 pm | Amazing World of Gumball | The Sale
 3:00 pm | Total DramaRama | Cuttin' Corners
 3:15 pm | Total DramaRama | A Ninjustice to Harold
 3:30 pm | Total DramaRama | Sharing Is Caring
 3:45 pm | Total DramaRama | Having the Timeout of Our Lives
 4:00 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 4:30 pm | Teen Titans Go! | Video Game References; Cool School
 5:00 pm | Craig of the Creek | Vulture's Nest
 5:15 pm | Craig of the Creek | Dinner at the Creek
 5:30 pm | Craig of the Creek | Kelsey Quest
 5:45 pm | Craig of the Creek | Bug City
 6:00 pm | Teen Titans Go! | Brian; Nature
 6:30 pm | Teen Titans Go! | Salty Codgers; Knowledge
 7:00 pm | Apple & Onion | Falafel's Fun Day
 7:15 pm | Apple & Onion | Heatwave
 7:30 pm | Amazing World of Gumball | The BFFS
 7:45 pm | Amazing World of Gumball | The Romantic