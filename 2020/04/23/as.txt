# 2020-04-23
 8:00 pm | Home Movies | Coffins and Cradles
 8:30 pm | Bob's Burgers | Spaghetti Western and Meatballs
 9:00 pm | Bob's Burgers | Burger Wars
 9:30 pm | Rick and Morty | Look Who's Purging Now
10:00 pm | American Dad | An Incident at Owl Creek
10:30 pm | American Dad | Great Space Roaster
11:00 pm | Family Guy | We Love You, Conrad
11:30 pm | Family Guy | Three Kings
12:00 am | Three Busy Debras | Barbra
12:15 am | Beef House | Beaver in the Beef House
12:30 am | Aqua Something You Know Whatever | Buddy Nugget
12:45 am | Squidbillies | Forever Autumn
 1:00 am | The Venture Brothers | The Terminus Mandate
 1:30 am | Family Guy | We Love You, Conrad
 2:00 am | Family Guy | Three Kings
 2:30 am | American Dad | An Incident at Owl Creek
 3:00 am | Rick and Morty | Look Who's Purging Now
 3:30 am | Three Busy Debras | Barbra
 3:45 am | Beef House | Beaver in the Beef House
 4:00 am | Hot Streets | Super Agent
 4:15 am | Hot Streets | The Egg
 4:30 am | Aqua Something You Know Whatever | Buddy Nugget
 4:45 am | Squidbillies | Forever Autumn
 5:00 am | Bob's Burgers | Spaghetti Western and Meatballs
 5:30 am | Bob's Burgers | Burger Wars