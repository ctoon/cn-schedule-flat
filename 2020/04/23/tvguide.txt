# 2020-04-23
 6:00 am | Amazing World of Gumball | The Words; The Apology
 6:30 am | Amazing World of Gumball | The Watch; The Bet
 7:00 am | Amazing World of Gumball | The Love; The Awkwardness
 7:30 am | Amazing World of Gumball | The Nest; The Points
 8:00 am | Apple & Onion | Positive Attitude Theory; Apple's Short
 8:30 am | Apple & Onion | Face Your Fears; Sausage and Sweetie Smash
 9:00 am | Teen Titans Go! | Employee of the Month Redux Labor Day
 9:30 am | Teen Titans Go! | Orangins Ones and Zeros
10:00 am | Craig of the Creek | Bug City Stink Bomb
10:30 am | Craig Of The Creek | Summer Wish; The Shortcut
11:00 am | Total DramaRama | Camping Is In Tents
11:30 am | Total DramaRama | Weiner Takes All Look Who's Clocking
12:00 pm | Victor and Valentino | Forever Ever Brotherly Love
12:30 pm | Victor and Valentino | Tree Buds Legend of the Hidden Skate Park
 1:00 pm | Amazing World of Gumball | The Hero; The Photo
 1:30 pm | Amazing World of Gumball | The Tag; The Lesson
 2:00 pm | Amazing World of Gumball | The Limit; The Game
 2:30 pm | Amazing World of Gumball | The Promise; The Voice
 3:00 pm | Total DramaRama | Price of Advice; The Mutt Ado About Owen
 3:30 pm | Total DramaRama | Simons Are Forever; Mother Of All Cards
 4:00 pm | Teen Titans Go! | Booby Trap House; Inner Beauty Of A Cactus
 4:30 pm | Teen Titans Go! | Fish Water Movie Night
 5:00 pm | Craig Of The Creek | Kelsey The Author; The Great Fossil Rush
 5:30 pm | Craig of the Creek | Alone Quest Cousin of the Creek
 6:00 pm | Teen Titans Go! | TV Knight Permanent Record
 6:30 pm | Teen Titans Go! | Bbsfbday! Titan Saving Time
 7:00 pm | Apple & Onion | Burger's Trampoline; Follow Your Dreams
 7:30 pm | Amazing World of Gumball | The Flower; The Banana