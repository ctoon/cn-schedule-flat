# 2020-04-18
 6:00 am | Amazing World of Gumball | The Name; The Extras
 6:30 am | Amazing World of Gumball | The Gripes; The Vacation
 7:00 am | Teen Titans Go! | Hose Water; Yearbook Madness
 7:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 8:00 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 8:30 am | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 9:00 am | Craig of the Creek | Creek Daycare
 9:15 am | Craig of the Creek | Sugar Smugglers
 9:30 am | Victor and Valentino | The Guest
 9:45 am | Victor And Valentino | Ener-G-Shoes
10:00 am | Total DramaRama | Pudding the Planet First
10:15 am | Total DramaRama | Supply Mom
10:30 am | Thundercats Roar! | Berserkers
10:45 am | Thundercats Roar! | Jaga History
11:00 am | Teen Titans Go! | Cool School; Video Game References
11:30 am | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
12:00 pm | Victor And Valentino | Legend Of The Hidden Skate Park; Cleaning Day
12:30 pm | Victor And Valentino | The Babysitter; Hurricane Chata
 1:00 pm | Amazing World of Gumball | The Fraud; The Void
 1:30 pm | Amazing World of Gumball | The Move; The Boss
 2:00 pm | Amazing World of Gumball | The Allergy; The Law
 2:30 pm | Amazing World of Gumball | The Mothers / The Password
 3:00 pm | Total DramaRama | The Bad Guy Busters; Cone In 60 Seconds
 3:30 pm | Total DramaRama | Tiger Fail; That's A Wrap
 4:00 pm | Teen Titans Go! | The Return of Slade; More of the Same
 4:30 pm | Teen Titans Go! | Tamaranian Vacation; Let's Get Serious
 5:00 pm | Craig of the Creek | Memories Of Bobby; Jacob Of The Creek
 5:30 pm | Craig of the Creek | Return Of The Honeysuckle Rangers; The Mystery Of The Timekeeper
 6:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 pm | Amazing World of Gumball | The Mirror; The Burden
 7:00 pm | Amazing World of Gumball | The Man; The Bros
 7:30 pm | Amazing World of Gumball | The Lie; The Pizza