# 2020-04-09
 6:00 am | Amazing World of Gumball | The Inquisition; The Hug
 6:30 am | Amazing World of Gumball | The Revolt; The Sale
 7:00 am | Mao Mao, Heroes of Pure Heart | Orangusnake Begins All by Mao Self
 7:30 am | Mao Mao, Heroes of Pure Heart | Captured Clops Ultraclops
 8:00 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 9:30 am | Teen Titans Go! | Cool School; Video Game References
10:00 am | Total DramaRama | A Ninjustice To Harold; Cuttin' Corners
10:30 am | Total DramaRama | Sharing Is Caring Having the Timeout of Our Lives
11:00 am | Teen Titans Go! | Brian; Nature
11:30 am | Teen Titans Go! | Knowledge; Salty Codgers
12:00 pm | Victor and Valentino | It Grows Balloon Boys
12:30 pm | Victor And Valentino | Lonely Haunts Club 2: Doll Island; A New Don
 1:00 pm | Amazing World of Gumball | The Bffs; The Romantic
 1:30 pm | Amazing World of Gumball | The Agent; The Uploads
 2:00 pm | Amazing World of Gumball | The Decisions; The Wicked
 2:30 pm | Amazing World Of Gumball | The Web; The Traitor
 3:00 pm | Total DramaRama | Cluckwork Orange Cone in 60 Seconds
 3:30 pm | Total DramaRama | Free Chili; The Bad Guy Busters
 4:00 pm | Teen Titans Go! | Love Monsters; Baby Hands
 4:30 pm | Teen Titans Go! | Caramel Apples; Halloween
 5:00 pm | Craig of the Creek | Dibs Court Escape From Family Dinner
 5:30 pm | Craig of the Creek | The Doorway to Helen Great Fossil Rush
 6:00 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
 6:30 pm | Teen Titans Go! | Crazy Day; Robin Backwards
 7:00 pm | Apple & Onion | Whale Spotting; Apple's In Trouble
 7:30 pm | Amazing World of Gumball | The Future; The Check