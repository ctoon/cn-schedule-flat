# 2020-04-06
 8:00 pm | Home Movies | Breaking Up Is Hard to Do
 8:30 pm | American Dad | Phantom of the Telethon
 9:00 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 9:30 pm | Rick and Morty | The Old Man And The Seat
10:00 pm | Rick and Morty | One Crew Over The Crewcoo's Morty
10:30 pm | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
11:00 pm | Rick and Morty | Rattlestar Ricklactica
11:30 pm | Rick and Morty | Pickle Rick
12:00 am | Rick and Morty | Meeseeks and Destroy
12:30 am | Rick and Morty | Rixty Minutes
 1:00 am | Rick and Morty | Ricksy Business
 1:30 am | Rick and Morty | Mortynight Run
 2:00 am | Rick and Morty | Get Schwifty
 2:30 am | Rick and Morty | The Wedding Squanchers
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Rick and Morty | The Rickchurian Mortydate
 4:00 am | Hot Streets | Got a Minute for Love?
 4:15 am | Hot Streets | Snake Island
 4:30 am | Aqua Unit Patrol Squad 1 | Wi-tri
 4:45 am | Squidbillies | Squash B'Gosh
 5:00 am | Bob's Burgers | Brunchsquatch
 5:30 am | Bob's Burgers | Thanks-hoarding