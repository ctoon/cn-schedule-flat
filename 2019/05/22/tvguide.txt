# 2019-05-22
 6:00 am | Steven Universe | Earthlings
 6:15 am | Teen Titans Go! | The Fight
 6:30 am | Teen Titans Go! | The Groover; Bbrbday
 7:00 am | Craig of the Creek | Under The Overpass; The Mystery Of The Timekeeper
 7:30 am | Amazing World of Gumball | The Safety; The Oracle
 8:00 am | Amazing World of Gumball | The Friend; The Saint
 8:30 am | Amazing World of Gumball | The Deal; The Worst
 9:00 am | Total DramaRama | Inglorious Toddlers; The Date
 9:30 am | Victor And Valentino | The Collector
 9:45 am | Teen Titans Go! | Serious Business
10:00 am | Teen Titans Go! | Animals: It's Just a Word; Bbb Day!
10:30 am | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
11:00 am | Teen Titans Go! | The Mask; Slumber Party
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Mega Man: Fully Charged | The Gauntlet Part 1
12:15 pm | Amazing World of Gumball | The Shippening
12:30 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 1:00 pm | Amazing World of Gumball | The Girlfriend; The Traitor
 1:30 pm | Total DramaRama | From Badge To Worse; Cone In 60 Seconds
 2:00 pm | Teen Titans Go! | I Used To Be A Peoples; Tall Titan Tales
 2:30 pm | Teen Titans Go! | The Chaff; The Metric System Vs. Freedom
 3:00 pm | Ben 10 | Buggy Out
 3:15 pm | Craig of the Creek | Dog Decider
 3:30 pm | Craig of the Creek | The Curse; Jessica's Trail
 4:00 pm | Amazing World of Gumball | The Awareness; The Slip
 4:30 pm | Amazing World of Gumball | The Drama; The Buddy
 5:00 pm | Total DramaRama | Gum And Gummer; Bananas & Cheese
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Victor And Valentino | Folk Art Foes
 6:00 pm | Teen Titans Go! | Pyramid Scheme; Bottle Episode
 6:30 pm | Teen Titans Go! | Finally a Lesson; Arms Race With Legs
 7:00 pm | Amazing World of Gumball | The Awkwardness; The Nest
 7:30 pm | We Bare Bears | Escandalosos; Snake Babies