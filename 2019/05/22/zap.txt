# 2019-05-22
 6:00 am | Steven Universe | Earthlings
 6:15 am | Teen Titans Go! | The Fight
 6:30 am | Teen Titans Go! | The Groover
 6:45 am | Teen Titans Go! | BBRBDAY
 7:00 am | Craig of the Creek | Under the Overpass
 7:15 am | Craig of the Creek | The Mystery of the Timekeeper
 7:30 am | Amazing World of Gumball | The Oracle; The Safety
 8:00 am | Amazing World of Gumball | The Friend; The Saint
 8:30 am | Amazing World of Gumball | The Worst
 8:45 am | Amazing World of Gumball | The Deal
 9:00 am | Total DramaRama | Inglorious Toddlers
 9:15 am | Total DramaRama | The Date
 9:30 am | Victor and Valentino | The Collector
 9:45 am | Teen Titans Go! | Serious Business
10:00 am | Teen Titans Go! | Animals: It's Just a Word!
10:15 am | Teen Titans Go! | BBBDay!
10:30 am | Teen Titans Go! | Two Parter: Part One
10:45 am | Teen Titans Go! | Two Parter: Part Two
11:00 am | Teen Titans Go! | The Mask; Slumber Party
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Mega Man: Fully Charged | The Gauntlet, Part 1
12:15 pm | Amazing World of Gumball | The Shippening
12:30 pm | Amazing World of Gumball | The Origins
12:45 pm | Amazing World of Gumball | The Origins
 1:00 pm | Amazing World of Gumball | The Traitor
 1:15 pm | Amazing World of Gumball | The Girlfriend
 1:30 pm | Total DramaRama | From Badge to Worse
 1:45 pm | Total DramaRama | Cone in 60 Seconds
 2:00 pm | Teen Titans Go! | Tall Titan Tales
 2:15 pm | Teen Titans Go! | I Used to Be a Peoples
 2:30 pm | Teen Titans Go! | The Metric System vs. Freedom
 2:45 pm | Teen Titans Go! | The Chaff
 3:00 pm | Ben 10 | Buggy Out
 3:15 pm | Craig of the Creek | Dog Decider
 3:30 pm | Craig of the Creek | The Curse
 3:45 pm | Craig of the Creek | Jessica's Trail
 4:00 pm | Amazing World of Gumball | The Awareness
 4:15 pm | Amazing World of Gumball | The Slip
 4:30 pm | Amazing World of Gumball | The Drama
 4:45 pm | Amazing World of Gumball | The Buddy
 5:00 pm | Total DramaRama | Gum and Gummer
 5:15 pm | Total DramaRama | Bananas & Cheese
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Victor and Valentino | Folk Art Foes
 6:00 pm | Teen Titans Go! | Pyramid Scheme
 6:15 pm | Teen Titans Go! | Bottle Episode
 6:30 pm | Teen Titans Go! | Finally a Lesson
 6:45 pm | Teen Titans Go! | Arms Race With Legs
 7:00 pm | Amazing World of Gumball | The Awkwardness
 7:15 pm | Amazing World of Gumball | The Nest
 7:30 pm | We Bare Bears | Escandalosos
 7:45 pm | We Bare Bears | Snake Babies