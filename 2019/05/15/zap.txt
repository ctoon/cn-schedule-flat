# 2019-05-15
 6:00 am | Steven Universe | Alone at Sea
 6:15 am | Teen Titans Go! | Throne of Bones
 6:30 am | Teen Titans Go! | Demon Prom
 6:45 am | Teen Titans Go! | BBCYFSHIPBDAY
 7:00 am | Craig of the Creek | The Kid From 3030
 7:15 am | Craig of the Creek | The Final Book
 7:30 am | Amazing World of Gumball | The Name; The Extras
 8:00 am | Amazing World of Gumball | The Gripes; The Vacation
 8:30 am | Amazing World of Gumball | The Copycats
 8:45 am | Amazing World of Gumball | The Potato
 9:00 am | Total DramaRama | Germ Factory
 9:15 am | Total DramaRama | Gum and Gummer
 9:30 am | Victor and Valentino | Hurricane Chata
 9:45 am | Teen Titans Go! | Dreams
10:00 am | Teen Titans Go! | The Return of Slade; More of the Same
10:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:30 am | Mighty Magiswords | Hiccup the Volume
11:45 am | Mighty Magiswords | Ghosthaste
12:00 pm | Amazing World of Gumball | The Heist
12:15 pm | Amazing World of Gumball | The Best
12:30 pm | Amazing World of Gumball | The Crew
12:45 pm | Amazing World of Gumball | The Others
 1:00 pm | Amazing World of Gumball | The Signature
 1:15 pm | Amazing World of Gumball | The Check
 1:30 pm | Total DramaRama | Having the Timeout of Our Lives
 1:45 pm | Total DramaRama | The Price of Advice
 2:00 pm | Teen Titans Go! | The Scoop
 2:15 pm | Teen Titans Go! | Chicken in the Cradle
 2:30 pm | Teen Titans Go! | Tower Renovation
 2:45 pm | Teen Titans Go! | My Name Is Jose
 3:00 pm | Ben 10 | Which Watch
 3:15 pm | Craig of the Creek | Ace of Squares
 3:30 pm | Craig of the Creek | JPony
 3:45 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:00 pm | Amazing World of Gumball | The Vegging
 4:15 pm | Amazing World of Gumball | The One
 4:30 pm | Amazing World of Gumball | The Father
 4:45 pm | Amazing World of Gumball | The Cringe
 5:00 pm | Total DramaRama | Paint That a Shame
 5:15 pm | Total DramaRama | Cuttin' Corners
 5:30 pm | Total DramaRama | Sharing Is Caring
 5:45 pm | Victor and Valentino | Suerte
 6:00 pm | Teen Titans Go! | The Spice Game
 6:15 pm | Teen Titans Go! | I'm the Sauce
 6:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:45 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 7:00 pm | Amazing World of Gumball | The Routine
 7:15 pm | Amazing World of Gumball | The Upgrade
 7:30 pm | We Bare Bears | Crowbar Jones: Origins
 7:45 pm | We Bare Bears | Baby Orphan Ninja Bears