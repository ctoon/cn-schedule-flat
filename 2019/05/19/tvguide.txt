# 2019-05-19
 6:00 am | Teen Titans Go! | Bl4z3; I Used To Be A Peoples
 6:30 am | Mega Man: Fully Charged | Hide And Secrets
 6:45 am | Teen Titans Go! | Hot Salad Water
 7:00 am | Teen Titans Go! | Lication; The Metric System Vs. Freedom
 7:30 am | Teen Titans Go! | Ones And Zeros; The Chaff
 8:00 am | Teen Titans Go! | Stockton, Ca!; Career Day
 8:30 am | Victor And Valentino | Boss For A Day; Legend Of The Hidden Skate Park
 9:00 am | Total DramaRama | All Up In Your Drill; Bananas & Cheese
 9:30 am | Teen Titans Go! | Tv Knight 2; Them Soviet Boys
10:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star; Little Elvis
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition; Tv Knight 4
11:00 am | Teen Titans Go! | The Academy; Lil' Dimples
11:30 am | Teen Titans Go! | Stockton, Ca!; Throne Of Bones
12:00 pm | Summer Camp Island | It's My Party; Moon Problems
12:30 pm | Amazing World of Gumball | The Wish; The Future
 1:00 pm | Amazing World of Gumball | The Deal; The Comic
 1:30 pm | Amazing World of Gumball | The Nuisance; The Apprentice
 2:00 pm | Amazing World of Gumball | The Puppets; The Uploads
 2:30 pm | Amazing World of Gumball | The Petals; The Romantic
 3:00 pm | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
 3:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:00 pm | DC Super Hero Girls | #SuperWho?
 4:15 pm | DC Super Hero Girls | #Beeline
 4:30 pm | OK K.O.! Let's Be Heroes | Soda Genie
 4:45 pm | OK K.O.! Let's Be Heroes | Plaza Alone
 5:00 pm | OK K.O.! Let's Be Heroes | Monster Party; You're Everybody's Sidekick
 5:30 pm | Amazing World of Gumball | The Wish; The Best
 6:00 pm | Amazing World of Gumball | The Singing; The Routine
 6:30 pm | Amazing World of Gumball | The Heist; The Parking
 7:00 pm | Total Drama Island | Who Can You Trust?
 7:30 pm | Total Drama Island | Basic Straining