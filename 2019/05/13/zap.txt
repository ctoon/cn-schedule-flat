# 2019-05-13
 6:00 am | Steven Universe | Kiki's Pizza Delivery Service
 6:15 am | Teen Titans Go! | BL4Z3
 6:30 am | Teen Titans Go! | Lication
 6:45 am | Teen Titans Go! | Ones and Zeroes
 7:00 am | Craig of the Creek | Jextra Perrestrial
 7:15 am | Craig of the Creek | Escape From Family Dinner
 7:30 am | Amazing World of Gumball | The World; The Finale
 8:00 am | Amazing World of Gumball | The Kids; The Fan
 8:30 am | Amazing World of Gumball | The Guy
 8:45 am | Amazing World of Gumball | The Boredom
 9:00 am | Total DramaRama | Cuttin' Corners
 9:15 am | Total DramaRama | From Badge to Worse
 9:30 am | Victor and Valentino | Cleaning Day
 9:45 am | Teen Titans Go! | Birds
10:00 am | Teen Titans Go! | Video Game References; Cool School
10:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:00 am | Teen Titans Go! | Kabooms
11:30 am | Mighty Magiswords | A Mind Is a Terrible Thing
11:45 am | Mighty Magiswords | Bewitched, Bothered and Bothered Some More!
12:00 pm | Amazing World of Gumball | The Cycle
12:15 pm | Amazing World of Gumball | The Stars
12:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 1:00 pm | Amazing World of Gumball | The Downer; The Egg
 1:30 pm | Total DramaRama | The Bad Guy Busters
 1:45 pm | Total DramaRama | Wristy Business
 2:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:30 pm | Teen Titans Go! | Beast Girl
 2:45 pm | Teen Titans Go! | Bro-Pocalypse
 3:00 pm | Ben 10 | Franken-Fight
 3:15 pm | Craig of the Creek | Big Pinchy
 3:30 pm | Craig of the Creek | The Climb
 3:45 pm | Craig of the Creek | You're It
 4:00 pm | Amazing World of Gumball | The Petals
 4:15 pm | Amazing World of Gumball | The Nuisance
 4:30 pm | Amazing World of Gumball | The Line
 4:45 pm | Amazing World of Gumball | The List
 5:00 pm | Total DramaRama | Duncan Disorderly
 5:15 pm | Total DramaRama | Mother of All Cards
 5:30 pm | Victor and Valentino | The Boy Who Cried Lechuza
 5:45 pm | Victor and Valentino | The Collector
 6:00 pm | Teen Titans Go! | Stockton, CA!
 6:15 pm | Teen Titans Go! | Don't Be an Icarus
 6:30 pm | Teen Titans Go! | Tall Titan Tales
 6:45 pm | Teen Titans Go! | I Used to Be a Peoples
 7:00 pm | Amazing World of Gumball | The Wish
 7:15 pm | Amazing World of Gumball | The Future
 7:30 pm | We Bare Bears | Tabes & Charlie
 7:45 pm | We Bare Bears | Band of Outsiders