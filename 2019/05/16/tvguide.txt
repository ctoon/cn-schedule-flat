# 2019-05-16
 6:00 am | Steven Universe | Greg the Babysitter
 6:15 am | Teen Titans Go! | Beast Girl
 6:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 am | Craig of the Creek | The Climb; You're It
 7:30 am | Amazing World of Gumball | The Fraud; The Void
 8:00 am | Amazing World of Gumball | The Move; The Boss
 8:30 am | Amazing World of Gumball | The Vase; The Matchmaker
 9:00 am | Total DramaRama | The Bad Guy Busters; Wristy Business
 9:30 am | Victor And Valentino | The Boy Who Cried Lechuza
 9:45 am | Teen Titans Go! | Mr. Butt
10:00 am | Teen Titans Go! | Stockton, Ca!; Don't Be An Icarus
10:30 am | Teen Titans Go! | I Used To Be A Peoples; Tall Titan Tales
11:00 am | Teen Titans Go! | Flashback
11:30 am | Mighty Magiswords | Hangry Hangry Hoppus; Pachydermus Packard And The Camp Of Fantasy
12:00 pm | Amazing World of Gumball | The List; The Line
12:30 pm | Amazing World of Gumball | The Wish; The Future
 1:00 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 1:30 pm | Total DramaRama | Duncan Disorderly; Mother Of All Cards
 2:00 pm | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
 2:30 pm | Teen Titans Go! | The Power Of Shrimps; The Real Orangins!
 3:00 pm | Ben 10 | Baby Buktu
 3:15 pm | Craig of the Creek | Kelsey Quest
 3:30 pm | Craig of the Creek | Vulture's Nest; Memories Of Bobby
 4:00 pm | Amazing World of Gumball | The Faith; The Cage
 4:30 pm | Amazing World of Gumball | The Anybody; The Candidate
 5:00 pm | Total DramaRama | Know It All; Ant We All Just Get Along
 5:30 pm | Total DramaRama | Germ Factory
 5:45 pm | Victor And Valentino | The Dark Room
 6:00 pm | Teen Titans Go! | The Fourth Wall; 40%, 40%, 20%
 6:30 pm | Teen Titans Go! | Grube's Fairytales; A Farce
 7:00 pm | Amazing World of Gumball | The Uploads; The Apprentice
 7:30 pm | We Bare Bears | Hot Sauce; Fire!