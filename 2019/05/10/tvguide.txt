# 2019-05-10
 6:00 am | Steven Universe | Beach City Drift
 6:15 am | Teen Titans Go! | Orangins
 6:30 am | Teen Titans Go! | Jinxed; Brain Percentages
 7:00 am | Craig of the Creek | Dinner At The Creek; The Curse
 7:30 am | Amazing World of Gumball | The Tape; The Sweaters
 8:00 am | Amazing World of Gumball | The Internet; The Plan
 8:30 am | Amazing World of Gumball | The Compilation; The Stories
 9:00 am | Total DramaRama | The Date; Know It All
 9:30 am | Victor And Valentino | Legend of the Hidden Skate Park
 9:45 am | Teen Titans Go! | Pie Bros
10:00 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
10:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
11:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
11:30 am | Mighty Magiswords | Sorry For Party Oching, Forever A Fishstick
12:00 pm | Amazing World of Gumball | The Box; The Console
12:30 pm | Amazing World of Gumball | The Friend; The Saint
 1:00 pm | Amazing World of Gumball | The Spoiler; The Society
 1:30 pm | Total DramaRama | Germ Factory; Gum And Gummer
 2:00 pm | Teen Titans Go! | Throne of Bones; The Academy
 2:30 pm | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
 3:00 pm | Ben 10 | Billy Bajillions
 3:15 pm | Craig of the Creek | Power Punchers
 3:30 pm | Craig of the Creek | The Final Book; The Kid From 3030
 4:00 pm | Amazing World of Gumball | The Uncle; The Menu
 4:30 pm | Amazing World of Gumball | The Best; The Heist
 5:00 pm | Total DramaRama | Having The Timeout Of Our Lives; The Price Of Advice
 5:30 pm | Total DramaRama | Hic Hic Hooray
 5:45 pm | Victor And Valentino | Hurricane Chata
 6:00 pm | Teen Titans Go! | How's This For A Special? Spaaaace
 6:30 pm | Craig of the Creek | Fort Williams
 6:45 pm | Craig of the Creek | Sour Candy Trials
 7:00 pm | Amazing World of Gumball | The Others; The Crew
 7:30 pm | We Bare Bears | Family Troubles; The Gym