# 2019-05-08
 6:00 am | Steven Universe | Too Short to Ride
 6:15 am | Teen Titans Go! | Movie Night
 6:30 am | Teen Titans Go! | BBRAE
 7:00 am | Craig of the Creek | The Shortcut
 7:15 am | Craig of the Creek | The Future Is Cardboard
 7:30 am | Amazing World of Gumball | The Tag; The Lesson
 8:00 am | Amazing World of Gumball | The Limit; The Game
 8:30 am | Amazing World of Gumball | The Points
 8:45 am | Amazing World of Gumball | The Bus
 9:00 am | Total DramaRama | Venthalla
 9:15 am | Total DramaRama | Inglorious Toddlers
 9:30 am | Victor and Valentino | Chata's Quinta Quinceañera
 9:45 am | Teen Titans Go! | No Power
10:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
10:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
11:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
11:30 am | Mighty Magiswords | Manlier Fish the Fishlier Man
11:45 am | Mighty Magiswords | The Lanolion Sleeps Tonight
12:00 pm | Amazing World of Gumball | The Code
12:15 pm | Amazing World of Gumball | The Test
12:30 pm | Amazing World of Gumball | The Butterfly; The Question
 1:00 pm | Amazing World of Gumball | The Oracle; The Safety
 1:30 pm | Total DramaRama | Cuttin' Corners
 1:45 pm | Total DramaRama | From Badge to Worse
 2:00 pm | Teen Titans Go! | BL4Z3
 2:15 pm | Teen Titans Go! | Hot Salad Water
 2:30 pm | Teen Titans Go! | Lication
 2:45 pm | Teen Titans Go! | Ones and Zeroes
 3:00 pm | Ben 10 | Beach Heads
 3:15 pm | Craig of the Creek | The Takeout Mission
 3:30 pm | Craig of the Creek | Jextra Perrestrial
 3:45 pm | Craig of the Creek | Escape From Family Dinner
 4:00 pm | Amazing World of Gumball | The Ollie
 4:15 pm | Amazing World of Gumball | The Catfish
 4:30 pm | Amazing World of Gumball | The Cycle
 4:45 pm | Amazing World of Gumball | The Stars
 5:00 pm | Total DramaRama | The Bad Guy Busters
 5:15 pm | Total DramaRama | Wristy Business
 5:30 pm | Total DramaRama | Melter Skelter
 5:45 pm | Victor and Valentino | Cleaning Day
 6:00 pm | Teen Titans Go! | Video Game References; Cool School
 6:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 7:30 pm | We Bare Bears | Lil' Squid
 7:45 pm | We Bare Bears | Tunnels