# 2019-11-17
 6:00 am | Teen Titans Go! | Kabooms
 6:30 am | Teen Titans Go! | Secret Garden; Chicken In The Cradle
 7:00 am | Bakugan: Battle Planet | In My Room/An Army of Their Own
 7:30 am | Teen Titans Go! | Garage Sale; The Scoop
 8:00 am | Teen Titans Go! | Tv Knight 3; Squash & Stretch
 8:30 am | Teen Titans Go! | Them Soviet Boys; Oregon Trail
 9:00 am | Total DramaRama | Stop! Hamster Time
 9:15 am | Total DramaRama | Driving Miss Crazy
 9:30 am | Apple & Onion | Heatwave; Whale Spotting
10:00 am | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
10:30 am | Teen Titans Go! | Mo' Money Mo' Problems; Bro-pocalypse
11:00 am | Teen Titans Go! | Flashback
11:30 am | Teen Titans Go! | Black Friday; Beast Girl
12:00 pm | Teen Titans Go! | BBCYFSHIPBDAY;BBBDay!
12:30 pm | Teen Titans Go! | Teen Titans Vroom
 1:00 pm | Amazing World Of Gumball | The Signal; The Stars
 1:30 pm | Amazing World Of Gumball | The Parasite; The Grades
 2:00 pm | Amazing World Of Gumball | The Love; The Diet
 2:30 pm | Amazing World Of Gumball | The Awkwardness; The Ex
 3:00 pm | Craig Of The Creek | Dog Decider; Big Pinchy
 3:30 pm | Craig Of The Creek | Bring Out Your Beast; The Kid From 3030
 4:00 pm | Victor And Valentino | The Great Bongo Heist; The Dark Room
 4:30 pm | Victor And Valentino | Balloon Boys; Suerte
 5:00 pm | Amazing World of Gumball | The Password; The Mothers
 5:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 6:00 pm | Amazing World of Gumball | The Mirror; The Burden
 6:30 pm | Amazing World of Gumball | The Bros; The Man
 7:00 pm | Apple & Onion | Baby Boi Tp; Not Funny
 7:30 pm | Apple & Onion | Apple's Focus; Lil Noodle
 8:00 pm | We Bare Bears | Panda's Date; Charlie
 8:30 pm | We Bare Bears | Everyday Bears; Brother Up