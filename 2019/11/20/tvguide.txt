# 2019-11-20
 6:00 am | Amazing World of Gumball | The Points; The Bus
 6:30 am | Amazing World of Gumball | The Night; The Misunderstandings
 7:00 am | Amazing World of Gumball | The Roots; The Blame
 7:30 am | Amazing World of Gumball | The Dream; The Sidekick
 8:00 am | Teen Titans Go! | Lication; Hot Salad Water
 8:30 am | Teen Titans Go! | Career Day; Ones and Zeros
 9:00 am | Teen Titans Go! | The Academy; TV Knight 2
 9:30 am | Teen Titans Go! | Had To Be There; Strength Of A Grown Man
10:00 am | Craig Of The Creek | The Other Side
10:30 am | Craig of the Creek | Creek Cart Racers; Power Punchers
11:00 am | Amazing World of Gumball | The Hero; The Photo
11:30 am | Amazing World of Gumball | The Tag; The Lesson
12:00 pm | Ben 10 | The Night Ben Tennyson Came to Town
12:15 pm | Victor And Valentino | The Babysitter
12:30 pm | Victor And Valentino | Lonely Haunts Club; Hurricane Chata
 1:00 pm | Teen Titans Go! | Girls Night In
 1:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
 2:00 pm | Teen Titans Go! | The Fourth Wall; 40%, 40%, 20%
 2:30 pm | Teen Titans Go! | Grube's Fairytales; A Farce
 3:00 pm | Amazing World of Gumball | The Slide; The Loophole
 3:30 pm | Amazing World of Gumball | The Copycats; The Potato
 4:00 pm | Amazing World of Gumball | The Fuss; The Outside
 4:30 pm | Amazing World of Gumball | The Wish; The Future
 5:00 pm | Total DramaRama | Stop! Hamster Time; Driving Miss Crazy
 5:30 pm | Total DramaRama | Melter Skelter; The Bad Guy Busters
 6:00 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 6:30 pm | Teen Titans Go! | Double Trouble; The Date
 7:00 pm | Amazing World Of Gumball | The Agent; The Factory
 7:30 pm | Amazing World Of Gumball | The Web; The Mess
 8:00 pm | We Bare Bears | Money Man; Spa Day
 8:30 pm | We Bare Bears | More Everyone's Tube; Ice Cave