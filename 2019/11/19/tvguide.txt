# 2019-11-19
 6:00 am | Amazing World of Gumball | The Advice; The Signal
 6:30 am | Amazing World of Gumball | The Parasite; The Love
 7:00 am | Amazing World of Gumball | The Awkwardness; The Nest
 7:30 am | Amazing World of Gumball | The Bumpkin; The Flakers
 8:00 am | Teen Titans Go! | Brain Percentages; Bl4z3
 8:30 am | Teen Titans Go! | "Night Begins to Shine" Special
 9:30 am | Teen Titans Go! | The Bergerac; Snot And Tears
10:00 am | Craig Of The Creek | Jextra Perrestrial; You're It
10:30 am | Craig Of The Creek | Secret Book Club; Itch To Explore
11:00 am | Amazing World of Gumball | The Authority; The Virus
11:30 am | Amazing World of Gumball | The Pony; The Storm
12:00 pm | Ben 10 | What Rhymes with Omnitrix?
12:15 pm | Victor And Valentino | Chata's Quinta Quinceañera
12:30 pm | Victor And Valentino | Legend Of The Hidden Skate Park; Cleaning Day
 1:00 pm | Teen Titans Go! | What We Learned At Camp; Campfire!
 1:30 pm | Teen Titans Go! | Leg Day; Cat's Fancy
 2:00 pm | Teen Titans Go! | The Croissant; The Dignity of Teeth
 2:30 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 3:00 pm | Amazing World of Gumball | The Boredom; The Guy
 3:30 pm | Amazing World of Gumball | The Vision; The Choices
 4:00 pm | Amazing World of Gumball | The Code; The Test
 4:30 pm | Amazing World of Gumball | The Slip; The Drama
 5:00 pm | Total DramaRama | Too Much Of A Goo'd Thing; A Ninjustice To Harold
 5:30 pm | Total DramaRama | There Are No Hoppy Endings; Tiger Fail
 6:00 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
 6:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 7:00 pm | Amazing World of Gumball | The Possession; The Buddy
 7:30 pm | Amazing World of Gumball | The Silence; The Master
 8:00 pm | We Bare Bears | Escandalosos; Pigeons
 8:30 pm | We Bare Bears | Rescue Ranger; Bunnies