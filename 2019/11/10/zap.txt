# 2019-11-10
 6:00 am | Teen Titans Go! | The Chaff
 6:15 am | Teen Titans Go! | Who's Laughing Now
 6:30 am | Teen Titans Go! | The Metric System vs. Freedom
 6:45 am | Teen Titans Go! | Booty Scooty
 7:00 am | Bakugan: Battle Planet | Stormy Weather; Who Can It Be Now
 7:30 am | Teen Titans Go! | I Used to Be a Peoples
 7:45 am | Teen Titans Go! | Think About Your Future
 8:00 am | Teen Titans Go! | Tall Titan Tales
 8:15 am | Teen Titans Go! | The Art of Ninjutsu
 8:30 am | Teen Titans Go! | Teen Titans Vroom
 9:00 am | Total DramaRama | Mutt Ado About Owen
 9:15 am | Total DramaRama | Simons Are Forever
 9:30 am | Apple & Onion | Lil Noodle
 9:45 am | Apple & Onion | Apple's Focus
10:00 am | Apple & Onion | Fun Proof
10:15 am | Apple & Onion | Gyranoid of the Future
10:30 am | Teen Titans Go! | Genie President
10:45 am | Teen Titans Go! | History Lesson
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:30 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
11:45 am | Teen Titans Go! | Business Ethics Wink Wink
12:00 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
12:15 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
12:30 pm | Teen Titans Go! | BBRBDAY
12:45 pm | Teen Titans Go! | Wally T
 1:00 pm | Amazing World of Gumball | The Uploads
 1:15 pm | Amazing World of Gumball | The Vase
 1:30 pm | Amazing World of Gumball | The Apprentice
 1:45 pm | Amazing World of Gumball | The Matchmaker
 2:00 pm | Amazing World of Gumball | The Hug
 2:15 pm | Amazing World of Gumball | The Box
 2:30 pm | Amazing World of Gumball | The Wicked
 2:45 pm | Amazing World of Gumball | The Console
 3:00 pm | Craig of the Creek | Sunday Clothes
 3:15 pm | Craig of the Creek | Ace of Squares
 3:30 pm | Craig of the Creek | Escape From Family Dinner
 3:45 pm | Craig of the Creek | Doorway to Helen
 4:00 pm | Victor and Valentino | Forever Ever
 4:15 pm | Victor and Valentino | Cuddle Monster
 4:30 pm | Victor and Valentino | Tez Says
 4:45 pm | Victor and Valentino | Boss for a Day
 5:00 pm | Alvin and the Chipmunks: The Road Chip | 
 7:00 pm | Apple & Onion | Whale Spotting
 7:15 pm | Apple & Onion | Heatwave
 7:30 pm | Apple & Onion | Apple's in Charge
 7:45 pm | Apple & Onion | Burger's Trampoline
 8:00 pm | We Bare Bears | Food Truck
 8:15 pm | We Bare Bears | Shush Ninjas
 8:30 pm | We Bare Bears | Chloe
 8:45 pm | We Bare Bears | My Clique