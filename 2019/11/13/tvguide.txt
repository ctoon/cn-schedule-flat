# 2019-11-13
 6:00 am | Amazing World of Gumball | The Triangle; The Money
 6:30 am | Amazing World of Gumball | The Return; The Nemesis
 7:00 am | Amazing World of Gumball | The Crew; The Others
 7:30 am | Amazing World of Gumball | The Ape; The Poltergeist
 8:00 am | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 8:30 am | Teen Titans Go! | Oh Yeah!; Snuggle Time
 9:00 am | Teen Titans Go! | Riding the Dragon; The Overbite
 9:30 am | Teen Titans Go! | Quantum Fun; The Fight
10:00 am | Craig Of The Creek | The Great Fossil Rush; Dog Decider
10:30 am | Craig Of The Creek | Dibs Court; The Curse
11:00 am | Amazing World of Gumball | The Quest; The Spoon
11:30 am | Amazing World of Gumball | The Car; The Curse
12:00 pm | Ben 10 | Vin Diagram
12:15 pm | Victor And Valentino | Band For Life
12:30 pm | Victor And Valentino | Tez Says; Forever Ever
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 1:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 2:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 2:30 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 3:00 pm | Amazing World of Gumball | The Hug; The Wicked
 3:30 pm | Amazing World of Gumball | The Traitor; The Origins
 4:00 pm | Amazing World of Gumball | The Origins Part 2; The Girlfriend
 4:30 pm | Amazing World of Gumball | The Vegging; The Sucker
 5:00 pm | Total DramaRama | Mutt Ado About Owen; Simons Are Forever
 5:30 pm | Total DramaRama | Snots Landing; Duck Duck Juice
 6:00 pm | Teen Titans Go! | Tv Knight 4; Lil' Dimples
 6:30 pm | Teen Titans Go! | Stockton, Ca!; Don't Be An Icarus
 7:00 pm | Amazing World of Gumball | The Father; The One
 7:30 pm | Amazing World of Gumball | The Cringe; The Cage
 8:00 pm | We Bare Bears | Ramen; Vacation
 8:30 pm | We Bare Bears | Tunnels; Hurricane Hal