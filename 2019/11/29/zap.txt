# 2019-11-29
 6:00 am | Amazing World of Gumball | The Stink
 6:15 am | Amazing World of Gumball | The Misunderstandings
 6:30 am | Amazing World of Gumball | The Origins
 6:45 am | Amazing World of Gumball | The Origins
 7:00 am | Amazing World of Gumball | The Ad
 7:15 am | Amazing World of Gumball | The Night
 7:30 am | Amazing World of Gumball | The Understanding
 7:45 am | Amazing World of Gumball | The Bus
 8:00 am | Alvin and the Chipmunks: The Road Chip | 
10:00 am | Amazing World of Gumball | The Transformation
10:15 am | Amazing World of Gumball | The Points
10:30 am | Amazing World of Gumball | The Spinoffs
10:45 am | Amazing World of Gumball | The Nest
11:00 am | Amazing World of Gumball | The Potion
11:15 am | Amazing World of Gumball | The Awkwardness
11:30 am | Amazing World of Gumball | The Intelligence
11:45 am | Amazing World of Gumball | The Love
12:00 pm | Apple & Onion | Heatwave
12:15 pm | Apple & Onion | 4 on 1
12:30 pm | Apple & Onion | Apple's in Charge
12:45 pm | Apple & Onion | Hotdog's Movie Premiere
 1:00 pm | Amazing World of Gumball | The Society; The Spoiler
 1:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:00 pm | Amazing World of Gumball | The Schooling
 2:15 pm | Amazing World of Gumball | The Parasite
 2:30 pm | Amazing World of Gumball | The Founder
 2:45 pm | Amazing World of Gumball | The Signal
 3:00 pm | Amazing World of Gumball | The Parents
 3:15 pm | Amazing World of Gumball | The Advice
 3:30 pm | Amazing World of Gumball | The Brain
 3:45 pm | Amazing World of Gumball | The Girlfriend
 4:00 pm | Amazing World of Gumball | The Shippening
 4:15 pm | Amazing World of Gumball | The Traitor
 4:30 pm | Amazing World of Gumball | The Neighbor
 4:45 pm | Amazing World of Gumball | The Wicked
 5:00 pm | Amazing World of Gumball | The Downer; The Egg
 5:30 pm | Amazing World of Gumball | The Triangle; The Money
 6:00 pm | Apple & Onion | Burger's Trampoline
 6:15 pm | Apple & Onion | Bottle Catch
 6:30 pm | Apple & Onion | Baby Boi TP
 6:45 pm | Apple & Onion | Pancake's Bus Tour
 7:00 pm | Amazing World of Gumball | The Inquisition
 7:15 pm | Amazing World of Gumball | The Potato
 7:30 pm | Amazing World of Gumball | The BFFS
 7:45 pm | Amazing World of Gumball | The Copycats
 8:00 pm | Amazing World of Gumball | The Decisions
 8:15 pm | Amazing World of Gumball | The Loophole
 8:30 pm | Amazing World of Gumball | The Revolt
 8:45 pm | Amazing World of Gumball | The Slide