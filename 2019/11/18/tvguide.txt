# 2019-11-18
 6:00 am | Amazing World of Gumball | The Hug; The Wicked
 6:30 am | Amazing World of Gumball | The Traitor; The Origins
 7:00 am | Amazing World of Gumball | The Origins Part 2; The Girlfriend
 7:30 am | Amazing World of Gumball | The Phone; The Job
 8:00 am | Teen Titans Go! | Master Detective; Hand Zombie
 8:30 am | Teen Titans Go! | The Avogodo; Employee of the Month Redux
 9:00 am | Teen Titans Go! | Orangins; Jinxed
 9:30 am | Teen Titans Go! | Tv Knight 4; Lil' Dimples
10:00 am | Craig Of The Creek | Dinner At The Creek; Too Many Treasures
10:30 am | Craig Of The Creek | The Takeout Mission; Jessica Goes To The Creek
11:00 am | Amazing World of Gumball | The Words; The Apology
11:30 am | Amazing World of Gumball | The Watch; The Bet
12:00 pm | Ben 10 | A Sticky Situation
12:15 pm | Victor And Valentino | Folk Art Foes
12:30 pm | Victor And Valentino | Dead Ringer; Brotherly Love
 1:00 pm | Teen Titans Go! | Stockton, Ca!; Don't Be An Icarus
 1:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 2:00 pm | Teen Titans Go! | More of the Same; The Return of Slade
 2:30 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 3:00 pm | Amazing World of Gumball | The Detective; The Fury
 3:30 pm | Amazing World of Gumball | The Compilation; The Stories
 4:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 4:30 pm | Amazing World of Gumball | The Spinoffs; The Transformation
 5:00 pm | Total DramaRama | Weiner Takes All; Apoca-lice Now
 5:30 pm | Total DramaRama | Having The Timeout Of Our Lives; The Price Of Advice
 6:00 pm | Teen Titans Go! | Cartoon Feud; Curse Of The Booty Scooty
 6:30 pm | Teen Titans Go! | Collect Them All; Butt Atoms
 7:00 pm | Amazing World of Gumball | The Understanding; The Ad
 7:30 pm | Amazing World of Gumball | The Stink; The Awareness
 8:00 pm | We Bare Bears | Adopted; Tubin'
 8:30 pm | We Bare Bears | Pizza Band; Panda 2