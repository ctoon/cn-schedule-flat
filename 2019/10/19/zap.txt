# 2019-10-19
 6:00 am | Teen Titans Go! | Who's Laughing Now
 6:15 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 6:30 am | Transformers: Cyberverse | Parley
 6:45 am | Teen Titans Go! | Oregon Trail
 7:00 am | Teen Titans Go! | Snuggle Time
 7:15 am | Teen Titans Go! | Business Ethics Wink Wink
 7:30 am | Teen Titans Go! | Oh Yeah!
 7:45 am | Teen Titans Go! | Genie President
 8:00 am | Teen Titans Go! | Riding the Dragon
 8:15 am | Teen Titans Go! | Tall Titan Tales
 8:30 am | Teen Titans Go! | The Overbite
 8:45 am | Teen Titans Go! | I Used to Be a Peoples
 9:00 am | Victor and Valentino | Tree Buds
 9:15 am | Victor and Valentino | Band for Life
 9:30 am | Victor and Valentino | Tez Says
 9:45 am | Victor and Valentino | Forever Ever
10:00 am | The LEGO Batman Movie | 
12:00 pm | Ben 10 | And Xingo Was His Name-O
12:15 pm | Ben 10 | Fear in the Family
12:30 pm | Power Players | All Trick No Treat
12:45 pm | Power Players | Iced Out
 1:00 pm | Amazing World of Gumball | The Guy
 1:15 pm | Amazing World of Gumball | The Spinoffs
 1:30 pm | Amazing World of Gumball | The Boredom
 1:45 pm | Amazing World of Gumball | The Transformation
 2:00 pm | Amazing World of Gumball | The Vision
 2:15 pm | Amazing World of Gumball | The Understanding
 2:30 pm | Amazing World of Gumball | The Choices
 2:45 pm | Amazing World of Gumball | The Ad
 3:00 pm | Craig of the Creek | Cousin of the Creek
 3:15 pm | Craig of the Creek | Camper on the Run
 3:30 pm | Craig of the Creek | Council of the Creek
 3:45 pm | Craig of the Creek | Sparkle Cadet
 4:00 pm | Total DramaRama | Camping Is in Tents
 4:30 pm | Total DramaRama | Duncan Disorderly
 4:45 pm | Total DramaRama | Soother or Later
 5:00 pm | Amazing World of Gumball | The Code
 5:15 pm | Amazing World of Gumball | The Ghouls
 5:30 pm | Amazing World of Gumball | The Scam
 5:45 pm | Amazing World of Gumball | The Stink
 6:00 pm | Amazing World of Gumball | The Test
 6:15 pm | Amazing World of Gumball | The Awareness
 6:30 pm | Amazing World of Gumball | The Slide
 6:45 pm | Amazing World of Gumball | The Slip
 7:00 pm | Amazing World of Gumball | The Loophole
 7:15 pm | Amazing World of Gumball | The Drama
 7:30 pm | Amazing World of Gumball | The Copycats
 7:45 pm | Amazing World of Gumball | The Buddy
 8:00 pm | Steven Universe | Sadie Killer
 8:15 pm | Steven Universe | Kevin Party
 8:30 pm | Steven Universe | 