# 2019-10-27
 6:00 am | Teen Titans Go! | Orangins
 6:15 am | Teen Titans Go! | Hot Salad Water
 6:30 am | Teen Titans Go! | Jinxed
 6:45 am | Teen Titans Go! | BL4Z3
 7:00 am | Bakugan: Battle Planet | Too Much; Who Are You?
 7:30 am | Teen Titans Go! | Witches Brew
 7:45 am | Teen Titans Go! | Monster Squad!
 8:00 am | Teen Titans Go! | Costume Contest
 8:15 am | Teen Titans Go! | Halloween vs. Christmas
 8:30 am | Teen Titans Go! | Scary Figure Dance
 8:45 am | Teen Titans Go! | Halloween
 9:00 am | Victor and Valentino | Welcome to the Underworld
 9:30 am | Victor and Valentino | Lonely Haunts Club
 9:45 am | Victor and Valentino | Lonely Haunts Club 2: Doll Island
10:00 am | Teen Titans Go! | Brain Percentages
10:15 am | Teen Titans Go! | Permanent Record
10:30 am | Teen Titans Go! | BBRAE
11:00 am | Teen Titans Go! | Cartoon Feud
11:15 am | Teen Titans Go! | Curse of the Booty Scooty
11:30 am | Teen Titans Go! | Collect Them All
11:45 am | Teen Titans Go! | Lication
12:00 pm | Teen Titans Go! | Butt Atoms
12:15 pm | Teen Titans Go! | Ones and Zeroes
12:30 pm | Teen Titans Go! | TV Knight 5
12:45 pm | Teen Titans Go! | Career Day
 1:00 pm | Amazing World of Gumball | The Diet
 1:15 pm | Amazing World of Gumball | The BFFS
 1:30 pm | Amazing World of Gumball | The Ex
 1:45 pm | Amazing World of Gumball | The Inquisition
 2:00 pm | Amazing World of Gumball | The Ghouls
 2:15 pm | Amazing World of Gumball | The Scam
 2:30 pm | Amazing World of Gumball | Halloween
 2:45 pm | Amazing World of Gumball | The Mirror
 3:00 pm | Craig of the Creek | The Other Side
 3:30 pm | Craig of the Creek | The Haunted Dollhouse
 4:00 pm | Total DramaRama | A Ninjustice to Harold
 4:15 pm | Total DramaRama | Invasion of the Booger Snatchers
 4:30 pm | Total DramaRama | Having the Timeout of Our Lives
 4:45 pm | Total DramaRama | Wristy Business
 5:00 pm | Amazing World of Gumball | The Menu
 5:15 pm | Amazing World of Gumball | The Return
 5:30 pm | Amazing World of Gumball | The Uncle
 5:45 pm | Amazing World of Gumball | The Nemesis
 6:00 pm | Amazing World of Gumball | The Ghouls
 6:15 pm | Amazing World of Gumball | The Scam
 6:30 pm | Amazing World of Gumball | Halloween
 6:45 pm | Amazing World of Gumball | The Mirror
 7:00 pm | Amazing World of Gumball | The Sorcerer
 7:15 pm | Amazing World of Gumball | The Potion
 7:30 pm | Amazing World of Gumball | The Vacation
 7:45 pm | Amazing World of Gumball | The Ghost
 8:00 pm | Regular Show | Terror Tales of the Park III
 8:30 pm | Regular Show | Terror Tales of the Park IV