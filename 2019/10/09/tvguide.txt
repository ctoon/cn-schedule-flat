# 2019-10-09
 6:00 am | Teen Titans Go! | The Fight; The Groover
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 am | Teen Titans Go! | How's This For A Special? Spaaaace
 7:30 am | Amazing World of Gumball | The Phone; The Job
 8:00 am | Amazing World of Gumball | Halloween; The Treasure
 8:30 am | Amazing World of Gumball | The Words; The Apology
 9:00 am | Teen Titans Go! | "Night Begins to Shine" Special
10:00 am | Teen Titans Go! | Island Adventures
11:00 am | Amazing World of Gumball | The Nemesis; The Crew
11:30 am | Amazing World of Gumball | The Signature; The Others
12:00 pm | Ben 10 | LaGrange Muraille
12:15 pm | Victor And Valentino | Know It All
12:30 pm | Victor And Valentino | Fistfull Of Balloons; Love At First Bite
 1:00 pm | Teen Titans Go! | Little Buddies; Missing
 1:30 pm | Teen Titans Go! | Garage Sale; Secret Garden
 2:00 pm | Teen Titans Go! | Pyramid Scheme; The Cruel Giggling Ghoul
 2:30 pm | Teen Titans Go! | Finally a Lesson; Bottle Episode
 3:00 pm | Amazing World Of Gumball | The Ad; The Ghouls
 3:30 pm | Amazing World of Gumball | The Stink; The Awareness
 4:00 pm | Amazing World of Gumball | The Slip; The Drama
 4:30 pm | Amazing World of Gumball | The Traitor; The Wicked
 5:00 pm | Teen Titans Go! | Collect Them All; Butt Atoms
 5:30 pm | Teen Titans Go! | Cartoon Feud; Curse Of The Booty Scooty
 6:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 7:30 pm | Amazing World of Gumball | The Advice; The Girlfriend
 8:00 pm | We Bare Bears | Lazer Royale; Sandcastle
 8:30 pm | We Bare Bears | Baby Orphan Ninja Bears; Panda 2