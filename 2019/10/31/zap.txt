# 2019-10-31
 6:00 am | Victor and Valentino | El Silbon
 6:15 am | Victor and Valentino | Tree Buds
 6:30 am | Victor and Valentino | On Nagual Hill
 6:45 am | Victor and Valentino | The Boy Who Cried Lechuza
 7:00 am | Victor and Valentino | Cat-Pocalypse
 7:15 am | Victor and Valentino | The Collector
 7:30 am | Victor and Valentino | Dance Reynaldo Dance
 7:45 am | Victor and Valentino | The Dark Room
 8:00 am | Victor and Valentino | Welcome to the Underworld
 8:30 am | Victor and Valentino | Forever Ever
 8:45 am | Victor and Valentino | Suerte
 9:00 am | Victor and Valentino | Tez Says
 9:15 am | Victor and Valentino | Hurricane Chata
 9:30 am | Victor and Valentino | Band for Life
 9:45 am | Victor and Valentino | The Babysitter
10:00 am | Victor and Valentino | Lonely Haunts Club
10:15 am | Victor and Valentino | Lonely Haunts Club 2: Doll Island
10:30 am | Victor and Valentino | Escape From Bebe Bay
10:45 am | Victor and Valentino | Cleaning Day
11:00 am | Victor and Valentino | The Great Bongo Heist
11:15 am | Victor and Valentino | Legend of the Hidden Skate Park
11:30 am | Victor and Valentino | Balloon Boys
11:45 am | Victor and Valentino | Chata's Quinta Quinceañera
12:00 pm | Victor and Valentino | Welcome to the Underworld
12:30 pm | Victor and Valentino | Guillermo's Gathering
12:45 pm | Victor and Valentino | Brotherly Love
 1:00 pm | Victor and Valentino | Aluxes
 1:15 pm | Victor and Valentino | Dead Ringer
 1:30 pm | Victor and Valentino | Go With the Flow
 1:45 pm | Victor and Valentino | Folk Art Foes
 2:00 pm | Victor and Valentino | El Silbon
 2:15 pm | Victor and Valentino | Tree Buds
 2:30 pm | Victor and Valentino | Love at First Bite
 2:45 pm | Victor and Valentino | On Nagual Hill
 3:00 pm | Victor and Valentino | Fistfull of Balloons
 3:15 pm | Victor and Valentino | Cat-Pocalypse
 3:30 pm | Victor and Valentino | Know It All
 3:45 pm | Victor and Valentino | Dance Reynaldo Dance
 4:00 pm | Victor and Valentino | Welcome to the Underworld
 4:30 pm | Victor and Valentino | Churro Kings
 4:45 pm | Victor and Valentino | Forever Ever
 5:00 pm | Victor and Valentino | A New Don
 5:15 pm | Victor and Valentino | Tez Says
 5:30 pm | Victor and Valentino | It Grows
 5:45 pm | Victor and Valentino | Band for Life
 6:00 pm | Victor and Valentino | Lonely Haunts Club
 6:15 pm | Victor and Valentino | Lonely Haunts Club 2: Doll Island
 6:30 pm | Victor and Valentino | Los Cadejos
 6:45 pm | Victor and Valentino | Escape From Bebe Bay
 7:00 pm | Victor and Valentino | Cuddle Monster
 7:15 pm | Victor and Valentino | The Great Bongo Heist
 7:30 pm | Victor and Valentino | Boss for a Day
 7:45 pm | Victor and Valentino | Balloon Boys
 8:00 pm | Victor and Valentino | Welcome to the Underworld
 8:30 pm | Victor and Valentino | El Silbon
 8:45 pm | Victor and Valentino | Tree Buds