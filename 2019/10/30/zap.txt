# 2019-10-30
 6:00 am | Amazing World of Gumball | The Authority; The Virus
 6:30 am | Amazing World of Gumball | The Pony; The Storm
 7:00 am | Amazing World of Gumball | The Dream; The Sidekick
 7:30 am | Amazing World of Gumball | The Cringe
 7:45 am | Amazing World of Gumball | The Cage
 8:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
 8:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
 9:30 am | Teen Titans Go! | The Art of Ninjutsu
 9:45 am | Teen Titans Go! | Think About Your Future
10:00 am | Craig of the Creek | Dinner at the Creek
10:15 am | Craig of the Creek | Too Many Treasures
10:30 am | Craig of the Creek | The Takeout Mission
10:45 am | Craig of the Creek | The Final Book
11:00 am | Amazing World of Gumball | The Faith
11:15 am | Amazing World of Gumball | The Candidate
11:30 am | Amazing World of Gumball | The Anybody
11:45 am | Amazing World of Gumball | The Pact
12:00 pm | Victor and Valentino | On Nagual Hill
12:15 pm | Victor and Valentino | The Dark Room
12:30 pm | Victor and Valentino | The Collector
12:45 pm | Victor and Valentino | The Boy Who Cried Lechuza
 1:00 pm | Teen Titans Go! | Booty Scooty
 1:15 pm | Teen Titans Go! | Who's Laughing Now
 1:30 pm | Teen Titans Go! | BBRBDAY
 1:45 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 2:00 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 2:15 pm | Teen Titans Go! | Business Ethics Wink Wink
 2:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 3:00 pm | Amazing World of Gumball | The Internet; The Plan
 3:30 pm | Amazing World of Gumball | The World; The Finale
 4:00 pm | Amazing World of Gumball | The Kids; The Fan
 4:30 pm | Amazing World of Gumball | The Ghouls
 4:45 pm | Amazing World of Gumball | The Stink
 5:00 pm | Total DramaRama | Camping Is in Tents
 5:30 pm | Total DramaRama | Soother or Later
 5:45 pm | Total DramaRama | Not Without My Fudgy Lumps
 6:00 pm | Teen Titans Go! | BBRAE
 6:30 pm | Teen Titans Go! | Permanent Record
 6:45 pm | Teen Titans Go! | Titan Saving Time
 7:00 pm | Amazing World of Gumball | The Awareness
 7:15 pm | Amazing World of Gumball | The Slip
 7:30 pm | Amazing World of Gumball | The Drama
 7:45 pm | Amazing World of Gumball | The Buddy
 8:00 pm | We Bare Bears | Bunnies
 8:15 pm | We Bare Bears | Ramen
 8:30 pm | We Bare Bears | The Fair
 8:45 pm | We Bare Bears | Imaginary Friend