# 2019-10-13
 6:00 am | Teen Titans Go! | Finally A Lesson; Monster Squad!
 6:30 am | Teen Titans Go! | Arms Race With Legs; The Real Orangins!
 7:00 am | Bakugan: Battle Planet | The Mask Of Pride; The Graveyard Of Courage
 7:30 am | Teen Titans Go! | Obinray; Quantum Fun
 8:00 am | Teen Titans Go! | Wally T; The Fight
 8:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes; The Groover
 9:00 am | Victor And Valentino | Go With The Flow; It Grows
 9:30 am | Victor And Valentino | Aluxes; A New Don
10:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
11:00 am | Teen Titans Go! | How's This For A Special? Spaaaace
11:30 am | Teen Titans Go! | The Art of Ninjutsu; History Lesson
12:00 pm | Teen Titans Go! | Think About Your Future; Bbrbday
12:30 pm | Teen Titans Go! | Booty Scooty; Slapping Butts And Celebrating For No Reason
 1:00 pm | Amazing World of Gumball | The Anybody; The Night
 1:30 pm | Amazing World of Gumball | The Pact; The Misunderstandings
 2:00 pm | Amazing World of Gumball | The Neighbor; The Roots
 2:30 pm | Amazing World of Gumball | The Shippening; The Blame
 3:00 pm | Craig Of The Creek | Council Of The Creek; The Mystery Of The Timekeeper
 3:30 pm | Craig Of The Creek | Sparkle Cadet; Alone Quest
 4:00 pm | Total DramaRama | Cuttin' Corners; Know It All
 4:30 pm | Total DramaRama | A Licking Time Bomb; Sharing Is Caring
 5:00 pm | Amazing World of Gumball | The Brain; The Detective
 5:30 pm | Amazing World of Gumball | The Parents; The Fury
 6:00 pm | Amazing World of Gumball | The Founder; The Compilation
 6:30 pm | Amazing World of Gumball | The Schooling; The Stories
 7:00 pm | Amazing World Of Gumball | The Intelligence; The Disaster
 7:30 pm | Amazing World Of Gumball | The Re-run; The Potion
 8:00 pm | We Bare Bears | Go Fish; Googs
 8:30 pm | We Bare Bears | Bear Squad; I, Butler