# 2019-10-13
 6:00 am | Teen Titans Go! | Finally a Lesson
 6:15 am | Teen Titans Go! | Monster Squad!
 6:30 am | Teen Titans Go! | Arms Race With Legs
 6:45 am | Teen Titans Go! | The Real Orangins!
 7:00 am | Bakugan: Battle Planet | The Mask of Pride; The Graveyard of Courage
 7:30 am | Teen Titans Go! | Obinray
 7:45 am | Teen Titans Go! | Quantum Fun
 8:00 am | Teen Titans Go! | Wally T
 8:15 am | Teen Titans Go! | The Fight
 8:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 8:45 am | Teen Titans Go! | The Groover
 9:00 am | Victor and Valentino | Go With the Flow
 9:15 am | Victor and Valentino | It Grows
 9:30 am | Victor and Valentino | Aluxes
 9:45 am | Victor and Valentino | A New Don
10:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
10:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
11:00 am | Teen Titans Go! | How's This for a Special? Spaaaace
11:30 am | Teen Titans Go! | The Art of Ninjutsu
11:45 am | Teen Titans Go! | History Lesson
12:00 pm | Teen Titans Go! | Think About Your Future
12:15 pm | Teen Titans Go! | BBRBDAY
12:30 pm | Teen Titans Go! | Booty Scooty
12:45 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 1:00 pm | Amazing World of Gumball | The Night
 1:15 pm | Amazing World of Gumball | The Anybody
 1:30 pm | Amazing World of Gumball | The Misunderstandings
 1:45 pm | Amazing World of Gumball | The Pact
 2:00 pm | Amazing World of Gumball | The Roots
 2:15 pm | Amazing World of Gumball | The Neighbor
 2:30 pm | Amazing World of Gumball | The Blame
 2:45 pm | Amazing World of Gumball | The Shippening
 3:00 pm | Craig of the Creek | Council of the Creek
 3:15 pm | Craig of the Creek | The Mystery of the Timekeeper
 3:30 pm | Craig of the Creek | Sparkle Cadet
 3:45 pm | Craig of the Creek | Alone Quest
 4:00 pm | Total DramaRama | Cuttin' Corners
 4:15 pm | Total DramaRama | Know It All
 4:30 pm | Total DramaRama | Sharing Is Caring
 4:45 pm | Total DramaRama | A Licking Time Bomb
 5:00 pm | Amazing World of Gumball | The Detective
 5:15 pm | Amazing World of Gumball | The Brain
 5:30 pm | Amazing World of Gumball | The Fury
 5:45 pm | Amazing World of Gumball | The Parents
 6:00 pm | Amazing World of Gumball | The Compilation
 6:15 pm | Amazing World of Gumball | The Founder
 6:30 pm | Amazing World of Gumball | The Stories
 6:45 pm | Amazing World of Gumball | The Schooling
 7:00 pm | Amazing World of Gumball | The Intelligence
 7:15 pm | Amazing World of Gumball | The Disaster
 7:30 pm | Amazing World of Gumball | The Re-Run
 7:45 pm | Amazing World of Gumball | The Potion
 8:00 pm | We Bare Bears | Go Fish
 8:15 pm | We Bare Bears | Googs
 8:30 pm | We Bare Bears | Bear Squad
 8:45 pm | We Bare Bears | I, Butler