# 2019-10-08
 6:00 am | Amazing World of Gumball | The News
 6:15 am | Amazing World of Gumball | The Rival
 6:30 am | Amazing World of Gumball | The Lady
 6:45 am | Amazing World of Gumball | The Sucker
 7:00 am | Amazing World of Gumball | The Vegging
 7:15 am | Amazing World of Gumball | The One
 7:30 am | Amazing World of Gumball | The Countdown; The Nobody
 8:00 am | Teen Titans Go! | Tower Renovation
 8:15 am | Teen Titans Go! | My Name Is Jose
 8:30 am | Teen Titans Go! | The Power of Shrimps
 8:45 am | Teen Titans Go! | Monster Squad!
 9:00 am | Teen Titans Go! | The Real Orangins!
 9:15 am | Teen Titans Go! | Quantum Fun
 9:30 am | Teen Titans Go! | Legs; Breakfast Cheese
10:00 am | Craig of the Creek | Stink Bomb
10:15 am | Craig of the Creek | Power Punchers
10:30 am | Craig of the Creek | Camper on the Run
10:45 am | Craig of the Creek | The Kid From 3030
11:00 am | Amazing World of Gumball | The Downer; The Egg
11:30 am | Amazing World of Gumball | The Triangle; The Money
12:00 pm | Ben 10 | Big Ben 10
12:15 pm | Victor and Valentino | It Grows
12:30 pm | Victor and Valentino | A New Don
12:45 pm | Victor and Valentino | Churro Kings
 1:00 pm | Teen Titans Go! | Waffles; Opposites
 1:30 pm | Teen Titans Go! | Scary Figure Dance
 1:45 pm | Teen Titans Go! | Animals: It's Just a Word!
 2:00 pm | Teen Titans Go! | BBBDay!
 2:15 pm | Teen Titans Go! | Two Parter: Part One
 2:30 pm | Teen Titans Go! | Two Parter: Part Two
 2:45 pm | Teen Titans Go! | Squash & Stretch
 3:00 pm | Amazing World of Gumball | The Schooling
 3:15 pm | Amazing World of Gumball | The Intelligence
 3:30 pm | Amazing World of Gumball | The Potion
 3:45 pm | Amazing World of Gumball | The Spinoffs
 4:00 pm | Amazing World of Gumball | The Transformation
 4:15 pm | Amazing World of Gumball | The Understanding
 4:30 pm | Amazing World of Gumball | The Upgrade
 4:45 pm | Amazing World of Gumball | The Comic
 5:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:15 pm | Total DramaRama | Ant We All Just Get Along
 5:30 pm | Total DramaRama | Snow Way Out
 5:45 pm | Total DramaRama | Sharing Is Caring
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 pm | Amazing World of Gumball | The Romantic
 7:15 pm | Amazing World of Gumball | The Uploads
 7:30 pm | Amazing World of Gumball | The Apprentice
 7:45 pm | Amazing World of Gumball | The Hug
 8:00 pm | We Bare Bears | Baby Bears Can't Jump
 8:15 pm | We Bare Bears | The Mummy's Curse
 8:30 pm | We Bare Bears | Bearz II Men
 8:45 pm | We Bare Bears | Band of Outsiders