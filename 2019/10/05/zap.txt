# 2019-10-05
 6:00 am | Teen Titans Go! | Cat's Fancy
 6:15 am | Teen Titans Go! | The Academy
 6:30 am | Transformers: Cyberverse | Trials
 6:45 am | Teen Titans Go! | Leg Day
 7:00 am | Teen Titans Go! | The Dignity of Teeth
 7:15 am | Teen Titans Go! | Costume Contest
 7:30 am | Teen Titans Go! | The Croissant
 7:45 am | Teen Titans Go! | Throne of Bones
 8:00 am | Teen Titans Go! | Cartoon Feud
 8:15 am | Teen Titans Go! | Curse of the Booty Scooty
 8:30 am | Teen Titans Go! | Collect Them All
 8:45 am | Teen Titans Go! | Butt Atoms
 9:00 am | Victor and Valentino | The Great Bongo Heist
 9:15 am | Victor and Valentino | Escape From Bebe Bay
 9:30 am | Victor and Valentino | Band for Life
 9:45 am | Victor and Valentino | Tez Says
10:00 am | Mao Mao: Heroes of Pure Heart | Flyaway
10:15 am | Mao Mao: Heroes of Pure Heart | Baost in Show
10:30 am | Mao Mao: Heroes of Pure Heart | Sugar Berry Fever
10:45 am | Mao Mao: Heroes of Pure Heart | Captured Clops
11:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
11:30 am | Teen Titans Go! | I'm the Sauce
11:45 am | Teen Titans Go! | The Spice Game
12:00 pm | Ben 10 | The Bentathlon
12:15 pm | Ben 10 | Prey or Play
12:30 pm | Power Players | Access Denied
12:45 pm | Power Players | Swing Set Jet Set
 1:00 pm | Amazing World of Gumball | The Return
 1:15 pm | Amazing World of Gumball | The Cycle
 1:30 pm | Amazing World of Gumball | The Nemesis
 1:45 pm | Amazing World of Gumball | The Stars
 2:00 pm | Amazing World of Gumball | The Crew
 2:15 pm | Amazing World of Gumball | The Grades
 2:30 pm | Amazing World of Gumball | The Others
 2:45 pm | Amazing World of Gumball | The Diet
 3:00 pm | Craig of the Creek | Sour Candy Trials
 3:15 pm | Craig of the Creek | Bug City
 3:30 pm | Craig of the Creek | Fort Williams
 3:45 pm | Craig of the Creek | Deep Creek Salvage
 4:00 pm | Total DramaRama | Venthalla
 4:15 pm | Total DramaRama | Hic Hic Hooray
 4:30 pm | Total DramaRama | Duck Duck Juice
 4:45 pm | Total DramaRama | Bananas & Cheese
 5:00 pm | Amazing World of Gumball | The Signature
 5:15 pm | Amazing World of Gumball | The Ex
 5:30 pm | Amazing World of Gumball | The Check
 5:45 pm | Amazing World of Gumball | The Sorcerer
 6:00 pm | Amazing World of Gumball | The Pest
 6:15 pm | Amazing World of Gumball | The Menu
 6:30 pm | Amazing World of Gumball | The Sale
 6:45 pm | Amazing World of Gumball | The Uncle
 7:00 pm | Amazing World of Gumball | The Gift
 7:15 pm | Amazing World of Gumball | The Weirdo
 7:30 pm | Amazing World of Gumball | The Parking
 7:45 pm | Amazing World of Gumball | The Heist
 8:00 pm | Steven Universe | 