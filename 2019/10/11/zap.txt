# 2019-10-11
 6:00 am | Amazing World of Gumball | The Schooling
 6:15 am | Amazing World of Gumball | The Intelligence
 6:30 am | Amazing World of Gumball | The Potion
 6:45 am | Amazing World of Gumball | The Spinoffs
 7:00 am | Amazing World of Gumball | The Transformation
 7:15 am | Amazing World of Gumball | The Understanding
 7:30 am | Amazing World of Gumball | The Upgrade
 7:45 am | Amazing World of Gumball | The Comic
 8:00 am | Teen Titans Go! | I Used to Be a Peoples
 8:15 am | Teen Titans Go! | The Metric System vs. Freedom
 8:30 am | Teen Titans Go! | The Chaff
 8:45 am | Teen Titans Go! | Them Soviet Boys
 9:00 am | Teen Titans Go! | Cartoon Feud
 9:15 am | Teen Titans Go! | Curse of the Booty Scooty
 9:30 am | Teen Titans Go! | Collect Them All
 9:45 am | Teen Titans Go! | Butt Atoms
10:00 am | Amazing World of Gumball | The Dream; The Sidekick
10:30 am | Amazing World of Gumball | The Hero; The Photo
11:00 am | Amazing World of Gumball | The Romantic
11:15 am | Amazing World of Gumball | The Uploads
11:30 am | Amazing World of Gumball | The Apprentice
11:45 am | Amazing World of Gumball | The Hug
12:00 pm | Ben 10 | The Claws of the Cat
12:15 pm | Victor and Valentino | Balloon Boys
12:30 pm | Victor and Valentino | Folk Art Foes
12:45 pm | Victor and Valentino | Dead Ringer
 1:00 pm | Teen Titans Go! | Brian; Nature
 1:30 pm | Teen Titans Go! | Island Adventures
 2:30 pm | Teen Titans Go! | History Lesson
 2:45 pm | Teen Titans Go! | The Art of Ninjutsu
 3:00 pm | Amazing World of Gumball | The Factory
 3:15 pm | Amazing World of Gumball | The Agent
 3:30 pm | Amazing World of Gumball | The Web
 3:45 pm | Amazing World of Gumball | The Mess
 4:00 pm | Amazing World of Gumball | The Heart
 4:15 pm | Amazing World of Gumball | The Revolt
 4:30 pm | Amazing World of Gumball | The Bus
 4:45 pm | Amazing World of Gumball | The Night
 5:00 pm | Teen Titans Go! | Super Hero Summer Camp
 6:00 pm | The LEGO Movie | 
 8:00 pm | DC Super Hero Girls | #Frenemies
 8:30 pm | Teen Titans Go! | Girls Night In