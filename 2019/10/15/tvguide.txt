# 2019-10-15
 6:00 am | Teen Titans Go! | Communicate Openly; Royal Jelly
 6:30 am | Teen Titans Go! | Had To Be There; Strength Of A Grown Man
 7:00 am | Teen Titans Go! | Girls Night In
 7:30 am | Amazing World of Gumball | The Boombox; The Castle
 8:00 am | Amazing World of Gumball | The Tape; The Sweaters
 8:30 am | Amazing World of Gumball | The Internet; The Plan
 9:00 am | Tom & Jerry Show | You Are What You Eat;  Not My Tyke;  Everyone Into The Pool
 9:30 am | Tom & Jerry Show | A Head For Science; Cat Cop; Double Dog Trouble
10:00 am | New Looney Tunes | Then Things Got Weird; Duck Duck Ghost; Acme Instant; When Marvin Comes Martian In
10:30 am | New Looney Tunes | The Knight Time Is the Right Time; The Pepe Le Pew Affair; Hamsters; Bugs Baked
11:00 am | Amazing World of Gumball | The Love; The Awkwardness
11:30 am | Amazing World of Gumball | The Nest; The Points
12:00 pm | Ben 10 | Roundabout Part 1
12:15 pm | Victor And Valentino | Cleaning Day
12:30 pm | Victor And Valentino | The Babysitter; Hurricane Chata
 1:00 pm | Teen Titans Go! | The Mask; Slumber Party
 1:30 pm | Teen Titans Go! | Riding the Dragon; The Overbite
 2:00 pm | Teen Titans Go! | Halloween vs. Christmas; Shrimps and Prime Rib
 2:30 pm | Teen Titans Go! | Booby Trap House; Fish Water
 3:00 pm | Amazing World of Gumball | The Responsible; The Dress
 3:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 4:00 pm | Amazing World of Gumball | The Mystery; The Prank
 4:30 pm | Amazing World of Gumball | The Stories; The Guy
 5:00 pm | The LEGO Batman Movie | 
 7:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 7:30 pm | Amazing World of Gumball | The Vision; The Boredom
 8:00 pm | We Bare Bears | Family Troubles; Baby Bears On A Plane
 8:30 pm | We Bare Bears | Lil' Squid; The Island