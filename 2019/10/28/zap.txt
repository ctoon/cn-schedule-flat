# 2019-10-28
 6:00 am | Amazing World of Gumball | The Flower; The Banana
 6:30 am | Amazing World of Gumball | The Phone; The Job
 7:00 am | Amazing World of Gumball | Halloween; The Treasure
 7:30 am | Amazing World of Gumball | The Petals
 7:45 am | Amazing World of Gumball | The Puppets
 8:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 8:30 am | Teen Titans Go! | Friendship; Vegetables
 9:00 am | Teen Titans Go! | The Mask; Slumber Party
 9:30 am | Teen Titans Go! | Secret Garden
 9:45 am | Teen Titans Go! | The Cruel Giggling Ghoul
10:00 am | Craig of the Creek | The Shortcut
10:15 am | Craig of the Creek | Monster in the Garden
10:30 am | Craig of the Creek | Deep Creek Salvage
10:45 am | Craig of the Creek | Escape From Family Dinner
11:00 am | Amazing World of Gumball | The Nuisance
11:15 am | Amazing World of Gumball | The Line
11:30 am | Amazing World of Gumball | The List
11:45 am | Amazing World of Gumball | The News
12:00 pm | Ben 10 | Xingo Nation
12:15 pm | Victor and Valentino | Legend of the Hidden Skate Park
12:30 pm | Victor and Valentino | Cleaning Day
12:45 pm | Victor and Valentino | The Babysitter
 1:00 pm | Teen Titans Go! | Pyramid Scheme
 1:15 pm | Teen Titans Go! | Bottle Episode
 1:30 pm | Teen Titans Go! | TV Knight 3
 1:45 pm | Teen Titans Go! | Chicken in the Cradle
 2:00 pm | Teen Titans Go! | Tower Renovation
 2:15 pm | Teen Titans Go! | My Name Is Jose
 2:30 pm | Teen Titans Go! | The Power of Shrimps
 2:45 pm | Teen Titans Go! | Monster Squad!
 3:00 pm | Amazing World of Gumball | The Hero; The Photo
 3:30 pm | Amazing World of Gumball | The Tag; The Lesson
 4:00 pm | Amazing World of Gumball | The Limit; The Game
 4:30 pm | Amazing World of Gumball | The Neighbor
 4:45 pm | Amazing World of Gumball | The Shippening
 5:00 pm | Total DramaRama | Free Chili
 5:15 pm | Total DramaRama | A Licking Time Bomb
 5:30 pm | Total DramaRama | Cluckwork Orange
 5:45 pm | Total DramaRama | Know It All
 6:00 pm | Teen Titans Go! | Oh Yeah!
 6:15 pm | Teen Titans Go! | Riding the Dragon
 6:30 pm | Teen Titans Go! | The Overbite
 6:45 pm | Teen Titans Go! | Shrimps and Prime Rib
 7:00 pm | Amazing World of Gumball | The Brain
 7:15 pm | Amazing World of Gumball | The Parents
 7:30 pm | Amazing World of Gumball | The Founder
 7:45 pm | Amazing World of Gumball | The Schooling
 8:00 pm | We Bare Bears | Baby Bears Can't Jump
 8:15 pm | We Bare Bears | The Mummy's Curse
 8:30 pm | We Bare Bears | Bearz II Men
 8:45 pm | We Bare Bears | Band of Outsiders