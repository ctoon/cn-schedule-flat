# 2019-02-03
 6:00 am | Teen Titans Go! | Bbrbday
 6:30 am | Mega Man: Fully Charged | Watt's Happening?!
 6:45 am | Teen Titans Go! | The Scoop
 7:00 am | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
 7:30 am | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
 8:00 am | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 8:30 am | Teen Titans Go! | The Real Orangins!; Quantum Fun
 9:00 am | Total DramaRama | Having The Timeout Of Our Lives; Hic Hic Hooray
 9:30 am | Total DramaRama | A Licking Time Bomb; A Ninjustice To Harold
10:00 am | Teen Titans Go! | "Night Begins to Shine" Special
11:00 am | Teen Titans Go! | Island Adventures
12:00 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; 40%, 40%, 20%
12:30 pm | Teen Titans Go! | The Fight; The Groover
 1:00 pm | Craig of the Creek | Bug City; Doorway To Helen
 1:30 pm | Craig of the Creek | The Last Kid in the Creek; The Climb
 2:00 pm | Amazing World of Gumball | The Phone; The Job
 2:30 pm | Amazing World of Gumball | The Flower; The Banana
 3:00 pm | Amazing World of Gumball | The Fridge; The Remote
 3:30 pm | Amazing World of Gumball | The Knights
 3:45 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason
 4:00 pm | Total DramaRama | Tiger Fail; That's A Wrap
 4:30 pm | Total DramaRama | A Licking Time Bomb; The Bad Guy Busters
 5:00 pm | Amazing World of Gumball | The Signature; The Compilation
 5:30 pm | Amazing World of Gumball | The Check; The Stories
 6:00 pm | Amazing World of Gumball | The Pest; The Disaster
 6:30 pm | Amazing World of Gumball | The Re-run; The Sale
 7:00 pm | Total Drama Island | Camp Castaways
 7:30 pm | Total Drama Island | Are We There, Yeti?