# 2019-02-06
 6:00 am | Steven Universe | Lion 3: Straight to Video
 6:15 am | Teen Titans Go! | Oh Yeah!
 6:30 am | Teen Titans Go! | Riding the Dragon; The Overbite
 7:00 am | Total Drama Island | No So Happy Campers
 7:30 am | Total Drama Island | Not So Happy Campers
 8:00 am | Amazing World of Gumball | The Mirror; The Burden
 8:30 am | Amazing World of Gumball | The Bros; The Man
 9:00 am | Craig of the Creek | Dog Decider; Power Punchers
 9:30 am | Teen Titans Go! | How Bout Some Effort; Pyramid Scheme
10:00 am | Teen Titans Go! | Finally a Lesson; Bottle Episode
10:30 am | Total DramaRama | Free Chili; A Ninjustice To Harold
11:00 am | Amazing World of Gumball | The Code; The Test
11:30 am | Amazing World of Gumball | The Slide; The Loophole
12:00 pm | Unikitty | Batkitty; Pool Duel
12:30 pm | Teen Titans Go! | BBSFBDAY!
 1:00 pm | Teen Titans Go! | The Streak: Part 2; Inner Beauty of a Cactus
 1:30 pm | We Bare Bears | Crowbar Jones: Origins; Bear Lift
 2:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 2:30 pm | Teen Titans Go! | The Art of Ninjutsu; History Lesson
 3:00 pm | Ben 10 | Super-villain Team-up
 3:15 pm | Amazing World of Gumball | The Oracle
 3:30 pm | Amazing World of Gumball | The Friend; The Saint
 4:00 pm | Total Drama Island | Not Quite Famous
 4:30 pm | Total Drama Island | The Sucky Outdoors
 5:00 pm | Craig of the Creek | Lost In The Sewer; Secret Book Club
 5:30 pm | Unikitty | Roadtrip Rukus
 5:45 pm | Unikitty | Memory Amok
 6:00 pm | Teen Titans Go! | Titan Saving Time; Master Detective
 6:30 pm | Teen Titans Go! | Employee of the Month Redux; Hand Zombie
 7:00 pm | We Bare Bears | Mom App; Pigeons
 7:30 pm | Total DramaRama | Aquarium For A Dream; Hic Hic Hooray