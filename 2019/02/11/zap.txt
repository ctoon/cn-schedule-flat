# 2019-02-11
 6:00 am | Steven Universe | The Test
 6:15 am | Teen Titans Go! | Movie Night
 6:30 am | Teen Titans Go! | BBRAE
 7:00 am | Total Drama Island | Phobia Factor
 7:30 am | Total Drama Island | Up the Creek
 8:00 am | Amazing World of Gumball | The Society; The Spoiler
 8:30 am | Amazing World of Gumball | The Countdown; The Nobody
 9:00 am | Craig of the Creek | Jextra Perrestrial
 9:15 am | Craig of the Creek | The Future Is Cardboard
 9:30 am | Teen Titans Go! | Think About Your Future
 9:45 am | Teen Titans Go! | Booty Scooty
10:00 am | Teen Titans Go! | Who's Laughing Now
10:15 am | Teen Titans Go! | Oregon Trail
10:30 am | Total DramaRama | Cuttin' Corners
10:45 am | Total DramaRama | Bananas & Cheese
11:00 am | Amazing World of Gumball | The Ollie
11:15 am | Amazing World of Gumball | The Catfish
11:30 am | Amazing World of Gumball | The Cycle
11:45 am | Amazing World of Gumball | The Stars
12:00 pm | Unikitty | Election Day
12:15 pm | Unikitty | No Sleep Sleep Over
12:30 pm | Teen Titans Go! | The Avogodo
12:45 pm | Teen Titans Go! | Orangins
 1:00 pm | Teen Titans Go! | Jinxed
 1:15 pm | Teen Titans Go! | Brain Percentages
 1:30 pm | We Bare Bears | Rescue Ranger
 1:45 pm | We Bare Bears | Panda 2
 2:00 pm | Teen Titans Go! | Shrimps and Prime Rib
 2:15 pm | Teen Titans Go! | Booby Trap House
 2:30 pm | Teen Titans Go! | Fish Water
 2:45 pm | Teen Titans Go! | TV Knight
 3:00 pm | Ben 10 | Xingo's Back
 3:15 pm | Amazing World of Gumball | The Return
 3:30 pm | Amazing World of Gumball | The Crew
 3:45 pm | Amazing World of Gumball | The Others
 4:00 pm | Total Drama Island | Who Can You Trust
 4:30 pm | Total Drama Island | Basic Straining
 5:00 pm | Craig of the Creek | The Shortcut
 5:15 pm | Craig of the Creek | Deep Creek Salvage
 5:30 pm | Unikitty | Kitty & Hawk
 5:45 pm | Unikitty | Roadtrip Rukus
 6:00 pm | Teen Titans Go! | Business Ethics Wink Wink
 6:15 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 6:30 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 6:45 pm | Teen Titans Go! | BBRBDAY
 7:00 pm | We Bare Bears | Tunnels
 7:15 pm | We Bare Bears | The Mall
 7:30 pm | Total DramaRama | From Badge to Worse
 7:45 pm | Total DramaRama | A Licking Time Bomb