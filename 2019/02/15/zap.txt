# 2019-02-15
 6:00 am | Steven Universe | Winter Forecast
 6:15 am | Teen Titans Go! | Career Day
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:00 am | Total Drama Island | No Pain, No Game
 7:30 am | Total Drama Island | Search and Do Not Destroy
 8:00 am | Amazing World of Gumball | The Gift
 8:15 am | Amazing World of Gumball | The Parking
 8:30 am | Amazing World of Gumball | The Routine
 8:45 am | Amazing World of Gumball | The Upgrade
 9:00 am | Amazing World of Gumball | The Slide
 9:15 am | Amazing World of Gumball | The Choices
 9:30 am | Teen Titans Go! | BBRAE
10:00 am | Teen Titans Go! | Movie Night
10:15 am | Teen Titans Go! | Permanent Record
10:30 am | Teen Titans Go! | The Avogodo
10:45 am | Teen Titans Go! | Orangins
11:00 am | Amazing World of Gumball | The Line
11:15 am | Amazing World of Gumball | The List
11:30 am | Amazing World of Gumball | The News
11:45 am | Amazing World of Gumball | The Rival
12:00 pm | Amazing World of Gumball | The Code
12:15 pm | Amazing World of Gumball | The Test
12:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 1:00 pm | Teen Titans Go! | Beast Girl
 1:15 pm | Teen Titans Go! | Bro-Pocalypse
 1:30 pm | Teen Titans Go! | Jinxed
 1:45 pm | Teen Titans Go! | Brain Percentages
 2:00 pm | Amazing World of Gumball | The Potato
 2:15 pm | Amazing World of Gumball | The Fuss
 2:30 pm | Amazing World of Gumball | The Loophole
 2:45 pm | Amazing World of Gumball | The Copycats
 3:00 pm | Amazing World of Gumball | The Hug
 3:15 pm | Amazing World of Gumball | The Wicked
 3:30 pm | Amazing World of Gumball | The Origins
 3:45 pm | Amazing World of Gumball | The Origins
 4:00 pm | Total Drama Island | Hook, Line and Screamer
 4:30 pm | Total Drama Island | Wawanakwa Gone Wild
 5:00 pm | Craig of the Creek | Jessica Goes to the Creek
 5:15 pm | Craig of the Creek | JPony
 5:30 pm | Unikitty | P.L.O.T. Device
 5:45 pm | Unikitty | BatKitty
 6:00 pm | The LEGO Movie | 