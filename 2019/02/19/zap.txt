# 2019-02-19
 6:00 am | Steven Universe | Marble Madness
 6:15 am | Teen Titans Go! | Beast Girl
 6:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 am | Total Drama Island | Camp Castaways
 7:30 am | Amazing World of Gumball | The Coach; The Joy
 8:00 am | Amazing World of Gumball | The Hug
 8:15 am | Amazing World of Gumball | The Wicked
 8:30 am | Amazing World of Gumball | The Origins
 8:45 am | Amazing World of Gumball | The Origins
 9:00 am | Amazing World of Gumball | The Potato
 9:15 am | Amazing World of Gumball | The Fuss
 9:30 am | Teen Titans Go! | The Avogodo
 9:45 am | Teen Titans Go! | Orangins
10:00 am | Teen Titans Go! | Jinxed
10:15 am | Teen Titans Go! | Brain Percentages
10:30 am | Teen Titans Go! | Career Day
10:45 am | Teen Titans Go! | TV Knight 2
11:00 am | Alvin and the Chipmunks: The Road Chip | 
 1:00 pm | Teen Titans Go! | Kabooms
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:00 pm | Amazing World of Gumball | The Console
 2:15 pm | Amazing World of Gumball | The Ollie
 2:30 pm | Amazing World of Gumball | The Catfish
 2:45 pm | Amazing World of Gumball | The Cycle
 3:00 pm | Ben 10 | Creature Feature
 3:15 pm | Amazing World of Gumball | The Parasite
 3:30 pm | Amazing World of Gumball | The Awkwardness
 3:45 pm | Amazing World of Gumball | The Nest
 4:00 pm | Total Drama Island | Camp Castaways
 4:30 pm | Total Drama Island | Are We There Yeti?
 5:00 pm | Craig of the Creek | Too Many Treasures
 5:15 pm | Craig of the Creek | Doorway to Helen
 5:30 pm | Unikitty | Rainbow Race
 5:45 pm | Unikitty | Memory Amok
 6:00 pm | Teen Titans Go! | Quantum Fun
 6:15 pm | Teen Titans Go! | The Fight
 6:30 pm | Teen Titans Go! | The Groover
 6:45 pm | Teen Titans Go! | BBRBDAY
 7:00 pm | We Bare Bears | Food Truck
 7:15 pm | We Bare Bears | I Am Ice Bear
 7:30 pm | Total DramaRama | A Ninjustice to Harold
 7:45 pm | Total DramaRama | Cluckwork Orange