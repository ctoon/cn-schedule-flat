# 2019-02-09
 6:00 am | Teen Titans Go! | How Bout Some Effort; Pyramid Scheme
 6:30 am | Teen Titans Go! | Bottle Episode; Cat's Fancy
 7:00 am | Bakugan: Battle Planet | Fathers And Friends; Midsummer Nightmare
 7:30 am | Teen Titans Go! | Finally a Lesson; Arms Race With Legs
 8:00 am | Teen Titans Go! | Obinray; Wally T
 8:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 9:00 am | Total DramaRama | From Badge To Worse
 9:15 am | Total DramaRama | A Licking Time Bomb
 9:30 am | Total DramaRama | Germ Factory; Cone In 60 Seconds; The Lego Movie 2: Everything Is Awesome Music Video
10:00 am | Teen Titans Go! | Nostalgia Is Not A Substitute For An Actual Story; History Lesson
10:30 am | Teen Titans Go! | The Art of Ninjutsu; Think About Your Future
11:00 am | Teen Titans Go! | Who's Laughing Now; Booty Scooty
11:30 am | Teen Titans Go! | Oregon Trail; Snuggle Time
12:00 pm | Ben 10 | Ben Again and Again
12:15 pm | Teen Titans Go! | Oh Yeah!
12:30 pm | Teen Titans Go! | Riding the Dragon; The Overbite
 1:00 pm | We Bare Bears | The Mall; Crowbar Jones: Origins
 1:30 pm | We Bare Bears | Hot Sauce; Mom App
 2:00 pm | Amazing World of Gumball | The Gift; The Guy
 2:30 pm | Amazing World of Gumball | The Parking; The Boredom
 3:00 pm | Amazing World of Gumball | The Routine; The Vision
 3:30 pm | Amazing World of Gumball | The Upgrade; The Choices
 4:00 pm | Total DramaRama | From Badge To Worse; Ant We All Just Get Along
 4:30 pm | Total DramaRama | Cuttin' Corners; Sharing Is Caring
 5:00 pm | The Lego 2: Everything Is Awesome Music Video | 
 7:00 pm | Total Drama Island | Camp Castaways
 7:30 pm | Total Drama Island | Are We There, Yeti?