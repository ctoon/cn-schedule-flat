# 2019-02-09
 6:00 am | Teen Titans Go! | How Bout Some Effort
 6:15 am | Teen Titans Go! | Pyramid Scheme
 6:30 am | Teen Titans Go! | Bottle Episode
 6:45 am | Teen Titans Go! | Cat's Fancy
 7:00 am | Bakugan: Battle Planet | Fathers and Friends; Midsummer Nightmare
 7:30 am | Teen Titans Go! | Finally a Lesson
 7:45 am | Teen Titans Go! | Arms Race With Legs
 8:00 am | Teen Titans Go! | Obinray
 8:15 am | Teen Titans Go! | Wally T
 8:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 8:45 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 9:00 am | Total DramaRama | From Badge to Worse
 9:15 am | Total DramaRama | A Licking Time Bomb
 9:30 am | Total DramaRama | Germ Factory
 9:45 am | Total DramaRama | Cone in 60 Seconds
10:00 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
10:15 am | Teen Titans Go! | History Lesson
10:30 am | Teen Titans Go! | The Art of Ninjutsu
10:45 am | Teen Titans Go! | Think About Your Future
11:00 am | Teen Titans Go! | Booty Scooty
11:15 am | Teen Titans Go! | Who's Laughing Now
11:30 am | Teen Titans Go! | Oregon Trail
11:45 am | Teen Titans Go! | Snuggle Time
12:00 pm | Ben 10 | Ben Again & Again
12:15 pm | Teen Titans Go! | Oh Yeah!
12:30 pm | Teen Titans Go! | Riding the Dragon
12:45 pm | Teen Titans Go! | The Overbite
 1:00 pm | We Bare Bears | The Mall
 1:15 pm | We Bare Bears | Crowbar Jones: Origins
 1:30 pm | We Bare Bears | Hot Sauce
 1:45 pm | We Bare Bears | Mom App
 2:00 pm | Amazing World of Gumball | The Gift
 2:15 pm | Amazing World of Gumball | The Guy
 2:30 pm | Amazing World of Gumball | The Parking
 2:45 pm | Amazing World of Gumball | The Boredom
 3:00 pm | Amazing World of Gumball | The Routine
 3:15 pm | Amazing World of Gumball | The Vision
 3:30 pm | Amazing World of Gumball | The Upgrade
 3:45 pm | Amazing World of Gumball | The Choices
 4:00 pm | Total DramaRama | From Badge to Worse
 4:15 pm | Total DramaRama | Ant We All Just Get Along
 4:30 pm | Total DramaRama | Cuttin' Corners
 4:45 pm | Total DramaRama | Sharing Is Caring
 5:00 pm | The LEGO Movie | 
 7:00 pm | Total Drama Island | Camp Castaways
 7:30 pm | Total Drama Island | Are We There Yeti?