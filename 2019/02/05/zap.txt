# 2019-02-05
 6:00 am | Steven Universe | Watermelon Steven
 6:15 am | Teen Titans Go! | Booty Scooty
 6:30 am | Teen Titans Go! | Who's Laughing Now
 6:45 am | Teen Titans Go! | Oregon Trail
 7:00 am | Total Drama Island | I Triple Dog Dare You
 7:30 am | Total Drama Island | The Very Last Episode, Really!
 8:00 am | Amazing World of Gumball | The Mothers; The Password
 8:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 9:00 am | Craig of the Creek | The Kid From 3030
 9:15 am | Craig of the Creek | The Curse
 9:30 am | Teen Titans Go! | Squash & Stretch
 9:45 am | Teen Titans Go! | Garage Sale
10:00 am | Teen Titans Go! | Secret Garden
10:15 am | Teen Titans Go! | The Cruel Giggling Ghoul
10:30 am | Total DramaRama | Cluckwork Orange
10:45 am | Total DramaRama | Tiger Fail
11:00 am | Amazing World of Gumball | The Guy
11:15 am | Amazing World of Gumball | The Boredom
11:30 am | Amazing World of Gumball | The Vision
11:45 am | Amazing World of Gumball | The Choices
12:00 pm | Unikitty | Super Amazing Raft Adventure
12:15 pm | Unikitty | Wishing Well
12:30 pm | Teen Titans Go! | Shrimps and Prime Rib
12:45 pm | Teen Titans Go! | Booby Trap House
 1:00 pm | Teen Titans Go! | Fish Water
 1:15 pm | Teen Titans Go! | TV Knight
 1:30 pm | We Bare Bears | Best Bears
 1:45 pm | We Bare Bears | Dog Hotel
 2:00 pm | Teen Titans Go! | Arms Race With Legs
 2:15 pm | Teen Titans Go! | Obinray
 2:30 pm | Teen Titans Go! | Wally T
 2:45 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 3:00 pm | Ben 10 | Can I Keep It?
 3:15 pm | Amazing World of Gumball | The Pizza
 3:30 pm | Amazing World of Gumball | The Butterfly; The Question
 4:00 pm | Total Drama Island | The Big Sleep
 4:30 pm | Total Drama Island | Dodgebrawl
 5:00 pm | Craig of the Creek | Creek Cart Racers
 5:15 pm | Craig of the Creek | Bring Out Your Beast
 5:30 pm | Unikitty | Tooth Trouble
 5:45 pm | Unikitty | This Spells Disaster
 6:00 pm | Teen Titans Go! | BBRAE
 6:30 pm | Teen Titans Go! | Movie Night
 6:45 pm | Teen Titans Go! | Permanent Record
 7:00 pm | We Bare Bears | Hot Sauce
 7:15 pm | We Bare Bears | Bunnies
 7:30 pm | Total DramaRama | The Date
 7:45 pm | Total DramaRama | Having the Timeout of Our Lives