# 2019-02-25
 6:00 am | Steven Universe | 
 6:15 am | Teen Titans Go! | 
 6:30 am | Teen Titans Go! | 
 6:45 am | Teen Titans Go! | 
 7:00 am | Total Drama Island | 
 7:30 am | Total Drama Island | 
 8:00 am | Amazing World of Gumball | 
 8:15 am | Amazing World of Gumball | 
 8:30 am | Amazing World of Gumball | 
 8:45 am | Amazing World of Gumball | 
 9:00 am | Amazing World of Gumball | 
 9:15 am | Amazing World of Gumball | 
 9:30 am | Teen Titans Go! | 
 9:45 am | Teen Titans Go! | 
10:00 am | Teen Titans Go! | 
10:15 am | Teen Titans Go! | 
10:30 am | Teen Titans Go! | 
10:45 am | Teen Titans Go! | 
11:00 am | Amazing World of Gumball | 
11:15 am | Amazing World of Gumball | 
11:30 am | Amazing World of Gumball | 
11:45 am | Amazing World of Gumball | 
12:00 pm | Amazing World of Gumball | 
12:15 pm | Amazing World of Gumball | 
12:30 pm | Teen Titans Go! | 
12:45 pm | Teen Titans Go! | 
 1:00 pm | Teen Titans Go! | 
 1:15 pm | Teen Titans Go! | 
 1:30 pm | Teen Titans Go! | 
 1:45 pm | Teen Titans Go! | 
 2:00 pm | Amazing World of Gumball | 
 2:15 pm | Amazing World of Gumball | 
 2:30 pm | Amazing World of Gumball | 
 2:45 pm | Amazing World of Gumball | 
 3:00 pm | Ben 10 | 
 3:15 pm | Amazing World of Gumball | 
 3:30 pm | Amazing World of Gumball | 
 3:45 pm | Amazing World of Gumball | 
 4:00 pm | Total Drama Island | 
 4:30 pm | Total Drama Island | 
 5:00 pm | Craig of the Creek | 
 5:15 pm | Craig of the Creek | 
 5:30 pm | Unikitty | 
 5:45 pm | Unikitty | 
 6:00 pm | Teen Titans Go! | 
 6:15 pm | Teen Titans Go! | 
 6:30 pm | Teen Titans Go! | 
 6:45 pm | Teen Titans Go! | 
 7:00 pm | We Bare Bears | 
 7:15 pm | We Bare Bears | 
 7:30 pm | Total DramaRama | 
 7:45 pm | Total DramaRama | 