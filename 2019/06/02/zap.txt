# 2019-06-02
 6:00 am | Teen Titans Go! | Forest Pirates
 6:15 am | Teen Titans Go! | Permanent Record
 6:30 am | Teen Titans Go! | A Farce
 6:45 am | Teen Titans Go! | Titan Saving Time
 7:00 am | Teen Titans Go! | Animals: It's Just a Word!
 7:15 am | Teen Titans Go! | Master Detective
 7:30 am | Teen Titans Go! | BBBDay!
 7:45 am | Teen Titans Go! | Hand Zombie
 8:00 am | Teen Titans Go! | Forest Pirates
 8:15 am | Teen Titans Go! | Two Parter: Part One
 8:30 am | Teen Titans Go! | Two Parter: Part Two
 8:45 am | Teen Titans Go! | Garage Sale
 9:00 am | Total DramaRama | Mother of All Cards
 9:15 am | Total DramaRama | Paint That a Shame
 9:30 am | Victor and Valentino | The Dark Room
 9:45 am | Victor and Valentino | Suerte
10:00 am | Teen Titans Go! | BBRAE
10:30 am | Teen Titans Go! | Secret Garden
10:45 am | Teen Titans Go! | Employee of the Month Redux
11:00 am | Teen Titans Go! | The Cruel Giggling Ghoul
11:15 am | Teen Titans Go! | The Avogodo
11:30 am | Teen Titans Go! | Forest Pirates
11:45 am | Teen Titans Go! | Pyramid Scheme
12:00 pm | Summer Camp Island | Time Traveling Quick Pants
12:15 pm | Summer Camp Island | It's My Party
12:30 pm | Teen Titans Go! | Jinxed
12:45 pm | Teen Titans Go! | Orangins
 1:00 pm | Teen Titans Go! | Finally a Lesson
 1:15 pm | Teen Titans Go! | Brain Percentages
 1:30 pm | Teen Titans Go! | Arms Race With Legs
 1:45 pm | Teen Titans Go! | BL4Z3
 2:00 pm | Teen Titans Go! | Obinray
 2:15 pm | Teen Titans Go! | Hot Salad Water
 2:30 pm | Teen Titans Go! | Wally T
 2:45 pm | Teen Titans Go! | Lication
 3:00 pm | Amazing World of Gumball | The Gift
 3:15 pm | Amazing World of Gumball | The Rival
 3:30 pm | Amazing World of Gumball | The Parking
 3:45 pm | Amazing World of Gumball | The Lady
 4:00 pm | DC Super Hero Girls | SheMightBeGiant
 4:15 pm | DC Super Hero Girls | ShockIttoMe
 4:30 pm | OK K.O.! Let's Be Heroes | Sidekick Scouts
 4:45 pm | OK K.O.! Let's Be Heroes | Whacky Jaxxyz
 5:00 pm | OK K.O.! Let's Be Heroes | Mystery Science Fair 201X
 5:15 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 5:30 pm | Amazing World of Gumball | The Routine
 5:45 pm | Amazing World of Gumball | The Sucker
 6:00 pm | Amazing World of Gumball | The Upgrade
 6:15 pm | Amazing World of Gumball | The Vegging
 6:30 pm | Amazing World of Gumball | The Comic
 6:45 pm | Amazing World of Gumball | The One
 7:00 pm | Craig of the Creek | Kelsey the Elder
 7:15 pm | Craig of the Creek | Deep Creek Salvage
 7:30 pm | Victor and Valentino | Los Cadejos
 7:45 pm | Victor and Valentino | Legend of the Hidden Skate Park