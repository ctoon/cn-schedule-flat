# 2019-06-01
 6:00 am | Steven Universe | Onion Gang
 6:15 am | Teen Titans Go! | Cat's Fancy
 6:30 am | Teen Titans Go! | Leg Day; Shrimps and Prime Rib
 7:00 am | Teen Titans Go! | Dignity of Teeth; Booby Trap House
 7:30 am | Teen Titans Go! | Croissant; Fish Water
 8:00 am | Teen Titans Go! | Spice Game; TV Knight
 8:30 am | Teen Titans Go! | I'm the Sauce,BBSFBDAY!,Oregon Trail
 9:00 am | Total DramaRama | Camping Is In Tents
 9:30 am | Victor And Valentino | Los Cadejos
 9:45 am | Mao Mao: Heroes Of Pure Heart | Sneak Peek
10:00 am | Teen Titans Go! | Forest Pirates; What's Opera, Titans?
10:30 am | Amazing World of Gumball | The Agent,The Factory,Career Day
11:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
11:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
12:00 pm | Ben 10 | Omni-copped; This One Goes To 11
12:30 pm | Victor And Valentino | Boss For A Day; Hurricane Chata
 1:00 pm | Teen Titans Go! | Forest Pirates; Inner Beauty Of A Cactus
 1:30 pm | Teen Titans Go! | 40%, 40%, 20%
 2:00 pm | Amazing World of Gumball | The Return; The Best
 2:30 pm | Amazing World of Gumball | The Nemesis; The Worst
 3:00 pm | Amazing World of Gumball | The Crew; The Deal
 3:30 pm | Amazing World of Gumball | The Others; The Petals
 4:00 pm | Amazing World Of Gumball | The Signature; The Nuisance
 4:30 pm | Victor And Valentino | The Boy Who Cried Lechuza; The Babysitter
 5:00 pm | Total DramaRama | Duncan Disorderly; Snots Landing
 5:30 pm | Amazing World Of Gumball | The Check; The Line
 6:00 pm | Amazing World Of Gumball | The Pest; The List
 6:30 pm | Amazing World Of Gumball | The Sale; The News
 7:00 pm | Craig Of The Creek | Sour Candy Trials; The Shortcut
 7:30 pm | Victor And Valentino | The Collector; Cleaning Day