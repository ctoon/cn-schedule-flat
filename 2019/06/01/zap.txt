# 2019-06-01
 6:00 am | Steven Universe | Onion Gang
 6:15 am | Teen Titans Go! | Cat's Fancy
 6:30 am | Teen Titans Go! | Leg Day
 6:45 am | Teen Titans Go! | Shrimps and Prime Rib
 7:00 am | Teen Titans Go! | The Dignity of Teeth
 7:15 am | Teen Titans Go! | Booby Trap House
 7:30 am | Teen Titans Go! | The Croissant
 7:45 am | Teen Titans Go! | Fish Water
 8:00 am | Teen Titans Go! | The Spice Game
 8:15 am | Teen Titans Go! | TV Knight
 8:30 am | Teen Titans Go! | I'm the Sauce
 8:45 am | Teen Titans Go! | BBSFBDAY
 9:00 am | Total DramaRama | Camping Is in Tents
 9:30 am | Victor and Valentino | Los Cadejos
 9:45 am | Mao Mao: Heroes of Pure Heart | Sneak Peek
10:00 am | Teen Titans Go! | Forest Pirates
10:15 am | Teen Titans Go! | What's Opera, Titans?
10:30 am | Amazing World of Gumball | The Agent
10:45 am | Amazing World of Gumball | The Factory
11:00 am | Teen Titans Go! | The Streak, Part 1
11:15 am | Teen Titans Go! | The Streak, Part 2
11:30 am | Teen Titans Go! | Accept the Next Proposition You Hear
11:45 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
12:00 pm | Ben 10 | Omni-Copped
12:15 pm | Ben 10 | This One Goes to 11
12:30 pm | Victor and Valentino | Boss for a Day
12:45 pm | Victor and Valentino | Hurricane Chata
 1:00 pm | Teen Titans Go! | Forest Pirates
 1:15 pm | Teen Titans Go! | Inner Beauty of a Cactus
 1:30 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 1:45 pm | Teen Titans Go! | Movie Night
 2:00 pm | Amazing World of Gumball | The Return
 2:15 pm | Amazing World of Gumball | The Best
 2:30 pm | Amazing World of Gumball | The Nemesis
 2:45 pm | Amazing World of Gumball | The Worst
 3:00 pm | Amazing World of Gumball | The Crew
 3:15 pm | Amazing World of Gumball | The Deal
 3:30 pm | Amazing World of Gumball | The Others
 3:45 pm | Amazing World of Gumball | The Petals
 4:00 pm | Amazing World of Gumball | The Signature
 4:15 pm | Amazing World of Gumball | The Nuisance
 4:30 pm | Victor and Valentino | The Boy Who Cried Lechuza
 4:45 pm | Victor and Valentino | The Babysitter
 5:00 pm | Total DramaRama | Duncan Disorderly
 5:15 pm | Total DramaRama | Snots Landing
 5:30 pm | Amazing World of Gumball | The Check
 5:45 pm | Amazing World of Gumball | The Line
 6:00 pm | Amazing World of Gumball | The Pest
 6:15 pm | Amazing World of Gumball | The List
 6:30 pm | Amazing World of Gumball | The Sale
 6:45 pm | Amazing World of Gumball | The News
 7:00 pm | Craig of the Creek | Sour Candy Trials
 7:15 pm | Craig of the Creek | The Shortcut
 7:30 pm | Victor and Valentino | The Collector
 7:45 pm | Victor and Valentino | Cleaning Day