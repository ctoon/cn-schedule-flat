# 2019-06-26
 6:00 am | Steven Universe | Dewey Wins
 6:15 am | Teen Titans Go! | BL4Z3
 6:30 am | Teen Titans Go! | Hot Salad Water
 6:45 am | Teen Titans Go! | Lication
 7:00 am | Teen Titans Go! | Video Game References; Cool School
 7:30 am | Amazing World of Gumball | The Revolt
 7:45 am | Amazing World of Gumball | The Decisions
 8:00 am | Amazing World of Gumball | The Slide
 8:15 am | Amazing World of Gumball | The Loophole
 8:30 am | Amazing World of Gumball | The Downer; The Egg
 9:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 9:30 am | Teen Titans Go! | Don't Be an Icarus
 9:45 am | Teen Titans Go! | Stockton, CA!
10:00 am | Teen Titans Go! | What's Opera, Titans?
10:15 am | Teen Titans Go! | Forest Pirates
10:30 am | We Bare Bears | The Fair
10:45 am | We Bare Bears | Imaginary Friend
11:00 am | Amazing World of Gumball | The Countdown; The Nobody
11:30 am | Amazing World of Gumball | The Triangle; The Money
12:00 pm | Total DramaRama | Cluckwork Orange
12:15 pm | Total DramaRama | Know It All
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 1:30 pm | Teen Titans Go! | Campfire!
 1:45 pm | Teen Titans Go! | Throne of Bones
 2:00 pm | Teen Titans Go! | Cat's Fancy
 2:15 pm | Teen Titans Go! | Leg Day
 2:30 pm | Ben 10 | Mutiny for the Bounty
 2:45 pm | Craig of the Creek | Secret Book Club
 3:00 pm | Amazing World of Gumball | The Matchmaker
 3:15 pm | Amazing World of Gumball | The Vase
 3:30 pm | Amazing World of Gumball | The Console
 3:45 pm | Amazing World of Gumball | The Box
 4:00 pm | Amazing World of Gumball | The Oracle; The Safety
 4:30 pm | Amazing World of Gumball | The Friend; The Saint
 5:00 pm | Total DramaRama | Soother or Later
 5:15 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:30 pm | Victor and Valentino | Suerte
 5:45 pm | Victor and Valentino | Legend of the Hidden Skate Park
 6:00 pm | Teen Titans Go! | Tall Titan Tales
 6:15 pm | Teen Titans Go! | I Used to Be a Peoples
 6:30 pm | Teen Titans Go! | The Metric System vs. Freedom
 6:45 pm | Teen Titans Go! | The Chaff
 7:00 pm | Amazing World of Gumball | The Possession
 7:15 pm | Amazing World of Gumball | The Master
 7:30 pm | Amazing World of Gumball | The Silence
 7:45 pm | Amazing World of Gumball | The Future