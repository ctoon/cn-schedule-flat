# 2019-06-30
 6:00 am | Teen Titans Go! | Stockton, CA!
 6:15 am | Teen Titans Go! | BBBDay!
 6:30 am | Teen Titans Go! | What's Opera, Titans?
 6:45 am | Teen Titans Go! | Squash & Stretch
 7:00 am | Teen Titans Go! | Cat's Fancy
 7:15 am | Teen Titans Go! | Garage Sale
 7:30 am | Teen Titans Go! | Leg Day
 7:45 am | Teen Titans Go! | The Dignity of Teeth
 8:00 am | Total DramaRama | From Badge to Worse
 8:15 am | Total DramaRama | Sharing Is Caring
 8:30 am | Total DramaRama | A Licking Time Bomb
 8:45 am | Total DramaRama | Cuttin' Corners
 9:00 am | Craig of the Creek | The Other Side
 9:30 am | Victor and Valentino | Welcome to the Underworld
10:00 am | Teen Titans Go! | The Cruel Giggling Ghoul
10:15 am | Teen Titans Go! | Two Parter: Part One
10:30 am | Teen Titans Go! | Two Parter: Part Two
10:45 am | Teen Titans Go! | Forest Pirates
11:00 am | Teen Titans Go! | The Bergerac
11:15 am | Teen Titans Go! | Snot and Tears
11:30 am | Teen Titans Go! | Campfire!
11:45 am | Teen Titans Go! | What We Learned at Camp
12:00 pm | Summer Camp Island | I Heart Heartforde
12:15 pm | Summer Camp Island | Space Invasion
12:30 pm | Summer Camp Island | Mom Soon
12:45 pm | Summer Camp Island | Sneeze Guard
 1:00 pm | Teen Titans Go! | The Croissant
 1:15 pm | Teen Titans Go! | Secret Garden
 1:30 pm | Teen Titans Go! | The Spice Game
 1:45 pm | Teen Titans Go! | Pyramid Scheme
 2:00 pm | Amazing World of Gumball | The BFFS
 2:15 pm | Amazing World of Gumball | The Inquisition
 2:30 pm | Amazing World of Gumball | The Revolt
 2:45 pm | Amazing World of Gumball | The Agent
 3:00 pm | Amazing World of Gumball | The Ex
 3:15 pm | Amazing World of Gumball | The Uploads
 3:30 pm | Amazing World of Gumball | The Sorcerer
 3:45 pm | Amazing World of Gumball | The Apprentice
 4:00 pm | DC Super Hero Girls | #Beeline
 4:15 pm | DC Super Hero Girls | #SuperWho?
 4:30 pm | OK K.O.! Let's Be Heroes | K.O.'s Health Week
 4:45 pm | OK K.O.! Let's Be Heroes | Rad's Alien Sickness
 5:00 pm | OK K.O.! Let's Be Heroes | Dark Plaza
 5:30 pm | Amazing World of Gumball | The Menu
 5:45 pm | Amazing World of Gumball | The Hug
 6:00 pm | Amazing World of Gumball | The Uncle
 6:15 pm | Amazing World of Gumball | The Wicked
 6:30 pm | Amazing World of Gumball | The Heist
 6:45 pm | Amazing World of Gumball | The Traitor
 7:00 pm | Craig of the Creek | Summer Wish
 7:15 pm | Craig of the Creek | Power Punchers
 7:30 pm | Victor and Valentino | A New Don
 7:45 pm | Victor and Valentino | Dead Ringer