# 2019-06-22
 6:00 am | Steven Universe | The Trial
 6:15 am | Teen Titans Go! | My Name is Jose
 6:30 am | Teen Titans Go! | The Power Of Shrimps; Tall Titan Tales
 7:00 am | Teen Titans Go! | The Real Orangins!; I Used To Be A Peoples
 7:30 am | Teen Titans Go! | Quantum Fun; The Metric System Vs. Freedom
 8:00 am | Total DramaRama | Invasion Of The Booger Snatchers; Tiger Fail
 8:30 am | Total DramaRama | Camping Is In Tents
 9:00 am | Craig Of The Creek | The Other Side
 9:30 am | Victor And Valentino | Welcome to the Underworld
10:00 am | Teen Titans Go! | Campfire!; Snot And Tears
10:30 am | Amazing World Of Gumball | The Revolt; The Decisions
11:00 am | Teen Titans Go! | "Night Begins to Shine" Special
12:00 pm | Ben 10 | Moor Fogg
12:15 pm | Ben 10 | King of the Castle
12:30 pm | Ninjago: Masters of Spinjitzu | True Potential Wasted
12:45 pm | Ninjago: Masters of Spinjitzu | Questing For Quests
 1:00 pm | Teen Titans Go! | The Fight; The Chaff
 1:30 pm | Teen Titans Go! | The Groover; Them Soviet Boys
 2:00 pm | Amazing World Of Gumball | The Boredom; The Master
 2:30 pm | Amazing World Of Gumball | The Vision; The Silence
 3:00 pm | Amazing World Of Gumball | The Choices; The Buddy
 3:30 pm | Amazing World Of Gumball | The Code; The Future
 4:00 pm | Amazing World Of Gumball | The Test; The Wish
 4:30 pm | Victor And Valentino | Boss For A Day; Hurricane Chata
 5:00 pm | Total DramaRama | Gum And Gummer; That's A Wrap
 5:30 pm | Amazing World Of Gumball | The Slide; The Factory
 6:00 pm | Amazing World Of Gumball | The Loophole; The Agent
 6:30 pm | Amazing World Of Gumball | The Copycats; The Return
 7:00 pm | Craig Of The Creek | The Mystery Of The Timekeeper; Jextra Perrestrial
 7:30 pm | Victor And Valentino | The Boy Who Cried Lechuza; The Babysitter