# 2019-06-23
 6:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:30 am | Teen Titans Go! | How's This For A Special? Spaaaace
 7:00 am | Teen Titans Go! | BBRBDAY ;Little Elvis
 7:30 am | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Tv Knight 4
 8:00 am | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; The Bad Guy Busters
 8:30 am | Total DramaRama | Snow Way Out; Cone In 60 Seconds
 9:00 am | Craig Of The Creek | The Great Fossil Rush; Secret Book Club
 9:30 am | Victor And Valentino | The Collector; Cleaning Day
10:00 am | Teen Titans Go! | Nostalgia Is Not A Substitute For An Actual Story; Lil' Dimples
10:30 am | Teen Titans Go! | Business Ethics Wink Wink; Don't Be An Icarus
11:00 am | Teen Titans Go! | Island Adventures
12:00 pm | Summer Camp Island | Cosmic Bupkiss
12:15 pm | Summer Camp Island | Radio Silence
12:30 pm | Summer Camp Island | Director's Cut
12:45 pm | Summer Camp Island | The Haunted Campfire
 1:00 pm | Mao Mao: Heroes Of Pure Heart | Sneak Peek
 1:15 pm | Teen Titans Go! | Campfire!
 1:30 pm | Teen Titans Go! | Tall Titan Tales; What's Opera, Titans?
 2:00 pm | Amazing World Of Gumball | The Revolt; The Decisions
 2:30 pm | Amazing World of Gumball | The Mess, The Factory
 3:00 pm | Amazing World Of Gumball | The Potato; The Nemesis
 3:30 pm | Amazing World Of Gumball | The Fuss; The Crew
 4:00 pm | DC Super Hero Girls | MeetTheCheetah;BurritoBucket
 4:30 pm | Ok K.o.! Let's Be Heroes | Beach Episode
 4:45 pm | Ok K.o.! Let's Be Heroes | Ok A.u.!
 5:00 pm | Ok K.o.! Let's Be Heroes | Let's Watch The Boxmore Show; Let's Watch The Pilot
 5:30 pm | Amazing World Of Gumball | The Outside; The Others
 6:00 pm | Amazing World Of Gumball | The Vase; The Signature
 6:30 pm | Amazing World Of Gumball | The Matchmaker; The Check
 7:00 pm | Craig Of The Creek | The Other Side
 7:30 pm | Victor And Valentino | Welcome to the Underworld