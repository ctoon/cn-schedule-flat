# 2019-06-14
 6:00 am | Steven Universe | Room for Ruby
 6:15 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:30 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 6:45 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 7:00 am | Teen Titans Go! | Pirates; I See You
 7:30 am | Amazing World of Gumball | The Comic
 7:45 am | Amazing World of Gumball | The Romantic
 8:00 am | Amazing World of Gumball | The Uploads
 8:15 am | Amazing World of Gumball | The Apprentice
 8:30 am | Amazing World of Gumball | The Web
 8:45 am | Amazing World of Gumball | The Line
 9:00 am | Teen Titans Go! | Brian; Nature
 9:30 am | Teen Titans Go! | The Academy
 9:45 am | Teen Titans Go! | Throne of Bones
10:00 am | Teen Titans Go! | Demon Prom
10:15 am | Teen Titans Go! | BBCYFSHIPBDAY
10:30 am | We Bare Bears | Lil' Squid
10:45 am | We Bare Bears | The Island
11:00 am | Amazing World of Gumball | The Buddy
11:15 am | Amazing World of Gumball | The Spinoffs
11:30 am | Amazing World of Gumball | The News
11:45 am | Amazing World of Gumball | The Rival
12:00 pm | Total DramaRama | Cone in 60 Seconds
12:15 pm | Total DramaRama | Invasion of the Booger Snatchers
12:30 pm | Total DramaRama | Ant We All Just Get Along
12:45 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 1:00 pm | Teen Titans Go! | The Bergerac
 1:15 pm | Mao Mao: Heroes of Pure Heart | Sneak Peek
 1:30 pm | Teen Titans Go! | Oh Yeah!
 1:45 pm | Teen Titans Go! | Riding the Dragon
 2:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 2:30 pm | Ben 10 | Billy Bajillions
 2:45 pm | Craig of the Creek | Dibs Court
 3:00 pm | Amazing World of Gumball | The Traitor
 3:15 pm | Amazing World of Gumball | The Girlfriend
 3:30 pm | Amazing World of Gumball | The Advice
 3:45 pm | Amazing World of Gumball | The Signal
 4:00 pm | Amazing World of Gumball | The Disaster
 4:15 pm | Amazing World of Gumball | The Re-Run
 4:30 pm | Amazing World of Gumball | The Origins
 4:45 pm | Amazing World of Gumball | The Origins
 5:00 pm | Total DramaRama | All Up in Your Drill
 5:15 pm | Total DramaRama | Cuttin' Corners
 5:30 pm | Victor and Valentino | Dead Ringer
 5:45 pm | Victor and Valentino | Lonely Haunts Club
 6:00 pm | Teen Titans Go! | Flashback
 6:30 pm | Teen Titans Go! | Mo' Money Mo' Problems
 6:45 pm | Teen Titans Go! | TV Knight 3
 7:00 pm | Amazing World of Gumball | The Father
 7:15 pm | Amazing World of Gumball | The Cringe
 7:30 pm | Amazing World of Gumball | The Cage
 7:45 pm | Amazing World of Gumball | The Faith