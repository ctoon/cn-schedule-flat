# 2019-06-11
 6:00 am | Steven Universe | Storm in the Room
 6:15 am | Teen Titans Go! | Two Parter: Part 1
 6:30 am | Teen Titans Go! | Two Parter: Part 2; Garage Sale
 7:00 am | Teen Titans Go! | Brain Food; In and Out
 7:30 am | Amazing World of Gumball | The Return; The Nemesis
 8:00 am | Amazing World of Gumball | The Crew; The Others
 8:30 am | Amazing World of Gumball | The Uncle; The Menu
 9:00 am | Teen Titans Go! | The Bergerac; Little Buddies
 9:30 am | Teen Titans Go! | Brain Percentages; Bl4z3
10:00 am | Teen Titans Go! | Lication; Hot Salad Water
10:30 am | We Bare Bears | Adopted; $100
11:00 am | Amazing World Of Gumball | The Factory; The Slip
11:30 am | Amazing World Of Gumball | The Web; The Best
12:00 pm | Total DramaRama | Tiger Fail; The Never Gwending Story
12:30 pm | Total DramaRama | The Bad Guy Busters; Wristy Business
 1:00 pm | Teen Titans Go! | Finally a Lesson; Arms Race With Legs
 1:30 pm | Teen Titans Go! | Obinray; Wally T
 2:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 2:30 pm | Ben 10 | Bridge Out
 2:45 pm | Craig of the Creek | Alone Quest
 3:00 pm | Amazing World of Gumball | The Gift; The Parking
 3:30 pm | Amazing World of Gumball | The Upgrade; The Routine
 4:00 pm | Total Drama Island | Trial by Tri-Armed Triathlon
 4:30 pm | Total Drama Island | After the Dock of Shame
 5:00 pm | Total DramaRama | Germ Factory; Gum And Gummer
 5:30 pm | Victor And Valentino | The Babysitter; The Collector
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | Career Day
 6:45 pm | Mao Mao: Heroes Of Pure Heart | Sneak Peek
 7:00 pm | Amazing World of Gumball | The Deal; The Worst
 7:30 pm | Amazing World of Gumball | The Petals; The Nuisance