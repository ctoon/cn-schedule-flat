# 2019-06-10
 6:00 am | Steven Universe | The New Crystal Gems
 6:15 am | Teen Titans Go! | A Farce
 6:30 am | Teen Titans Go! | Animals: It's Just a Word!
 6:45 am | Teen Titans Go! | BBBDay!
 7:00 am | Teen Titans Go! | Waffles; Opposites
 7:30 am | Amazing World of Gumball | The Silence
 7:45 am | Amazing World of Gumball | The Future
 8:00 am | Amazing World of Gumball | The Wish
 8:15 am | Amazing World of Gumball | The Factory
 8:30 am | Amazing World of Gumball | The Grades
 8:45 am | Amazing World of Gumball | The Diet
 9:00 am | Teen Titans Go! | Birds; Be Mine
 9:30 am | Teen Titans Go! | Employee of the Month Redux
 9:45 am | Teen Titans Go! | The Avogodo
10:00 am | Teen Titans Go! | Orangins
10:15 am | Teen Titans Go! | Jinxed
10:30 am | We Bare Bears | Imaginary Friend
10:45 am | We Bare Bears | The Fair
11:00 am | Amazing World of Gumball | The Web
11:15 am | Amazing World of Gumball | The Agent
11:30 am | Amazing World of Gumball | The Ex
11:45 am | Amazing World of Gumball | The Sorcerer
12:00 pm | Total DramaRama | A Ninjustice to Harold
12:15 pm | Total DramaRama | There Are No Hoppy Endings
12:30 pm | Total DramaRama | That's a Wrap
12:45 pm | Total DramaRama | Melter Skelter
 1:00 pm | Teen Titans Go! | Secret Garden
 1:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 1:30 pm | Teen Titans Go! | Pyramid Scheme
 1:45 pm | Teen Titans Go! | Bottle Episode
 2:00 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 2:30 pm | Ben 10 | Welcome to Zombozo-Zone!
 2:45 pm | Craig of the Creek | Memories of Bobby
 3:00 pm | Amazing World of Gumball | The Signature
 3:15 pm | Amazing World of Gumball | The Check
 3:30 pm | Amazing World of Gumball | The Pest
 3:45 pm | Amazing World of Gumball | The Sale
 4:00 pm | Total Drama Island | Hook, Line and Screamer
 4:30 pm | Total Drama Island | Wawanakwa Gone Wild
 5:00 pm | Total DramaRama | Invasion of the Booger Snatchers
 5:15 pm | Total DramaRama | Cone in 60 Seconds
 5:30 pm | Victor and Valentino | It Grows
 5:45 pm | Victor and Valentino | Los Cadejos
 6:00 pm | Teen Titans Go! | Snot and Tears
 6:15 pm | Teen Titans Go! | The Bergerac
 6:30 pm | Teen Titans Go! | The Metric System vs. Freedom
 6:45 pm | Teen Titans Go! | The Chaff
 7:00 pm | Amazing World of Gumball | The Mess
 7:15 pm | Amazing World of Gumball | The Heart
 7:30 pm | Amazing World of Gumball | The Web
 7:45 pm | Amazing World of Gumball | The Agent