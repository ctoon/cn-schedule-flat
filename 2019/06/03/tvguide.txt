# 2019-06-03
 6:00 am | Steven Universe | Three Gems and a Baby
 6:15 am | Teen Titans Go! | Two Bumble Bees and a Wasp
 6:30 am | Teen Titans Go! | Video Game References; Cool School
 7:00 am | Teen Titans Go! | Parasite; Starliar
 7:30 am | Amazing World of Gumball | The Neighbor; The Pact
 8:00 am | Amazing World of Gumball | The Shippening; The Brain
 8:30 am | Amazing World of Gumball | The Code; The Test
 9:00 am | Teen Titans Go! | Meatball Party; Staff Meeting
 9:30 am | Teen Titans Go! | The Overbite; Shrimps and Prime Rib
10:00 am | Teen Titans Go! | Booby Trap House; Fish Water
10:30 am | We Bare Bears | Band Of Outsiders; Baby Bears Can't Jump
11:00 am | Amazing World of Gumball | The Name; The Extras
11:30 am | Amazing World of Gumball | The Slide; The Loophole
12:00 pm | Total DramaRama | Soother Or Later; Not Without My Fudgy Lumps
12:30 pm | Total DramaRama | Mother Of All Cards; Bananas & Cheese
 1:00 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 1:30 pm | Teen Titans Go! | Leg Day; Cat's Fancy
 2:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 2:30 pm | Ben 10 | Omni-Copped
 2:45 pm | Craig of the Creek | Fort Williams
 3:00 pm | Amazing World of Gumball | The Potion; The Spinoffs
 3:30 pm | Amazing World of Gumball | The Transformation; The Understanding
 4:00 pm | Total Drama Island | Paintball Deer Hunter
 4:30 pm | Total Drama Island | If You Can't Take the Heat
 5:00 pm | Total DramaRama | Too Much Of A Good Thing; Having The Timeout Of Our Lives
 5:30 pm | Victor And Valentino | Los Cadejos; Cuddle Monster
 6:00 pm | Teen Titans Go! | The Bergerac
 6:15 pm | Teen Titans Go! | Forest Pirates
 6:30 pm | Teen Titans Go! | Them Soviet Boys; What's Opera, Titans?
 7:00 pm | Amazing World of Gumball | The Internet
 7:15 pm | Mao Mao: Heroes Of Pure Heart | Sneak Peek
 7:30 pm | Amazing World of Gumball | The Silence; The Future