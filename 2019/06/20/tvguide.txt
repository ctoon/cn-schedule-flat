# 2019-06-20
 6:00 am | Steven Universe | I Am My Mom
 6:15 am | Teen Titans Go! | BBSFBDAY!
 6:30 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 7:00 am | Teen Titans Go! | Snot And Tears; Road Trip
 7:30 am | Amazing World of Gumball | The Points; The Bus
 8:00 am | Amazing World of Gumball | The Night; The Misunderstandings
 8:30 am | Amazing World of Gumball | The Anybody; The Candidate
 9:00 am | Teen Titans Go! | Mouth Hole; Hot Garbage
 9:30 am | Teen Titans Go! | Kabooms
10:00 am | Teen Titans Go! | The Scoop; Chicken in the Cradle
10:30 am | We Bare Bears | Bearz Ii Men; The Mummy's Curse
11:00 am | Amazing World Of Gumball | The Possession; The Transformation
11:30 am | Amazing World of Gumball | The Neighbor; The Pact
12:00 pm | Total DramaRama | Cuttin' Corners; All Up In Your Drill
12:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 1:00 pm | Teen Titans Go! | Permanent Record; Titan Saving Time
 1:30 pm | Teen Titans Go! | Master Detective; Hand Zombie
 2:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 2:30 pm | Ben 10 | Baby Buktu
 2:45 pm | Craig of the Creek | Jessica's Trail
 3:00 pm | Amazing World of Gumball | The Boredom; The Guy
 3:30 pm | Amazing World of Gumball | The Choices; The Vision
 4:00 pm | Amazing World of Gumball | The Move; The Boss
 4:30 pm | Amazing World of Gumball | The Law; The Allergy
 5:00 pm | Total DramaRama | Know It All; Cluckwork Orange
 5:30 pm | Victor And Valentino | The Boy Who Cried Lechuza; Hurricane Chata
 6:00 pm | Teen Titans Go! | Quantum Fun; The Fight
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 pm | Amazing World of Gumball | The Schooling; The Intelligence
 7:30 pm | Amazing World of Gumball | The Potion; The Spinoffs