# 2019-06-04
 6:00 am | Steven Universe | Steven's Dream
 6:15 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt
 6:30 am | Teen Titans Go! | Operation Tin Man; Nean
 7:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 7:30 am | Amazing World of Gumball | The Parents; The Founder
 8:00 am | Amazing World of Gumball | The Schooling; The Intelligence
 8:30 am | Amazing World of Gumball | The Copycats; The Potato
 9:00 am | Teen Titans Go! | Forest Pirates; Burger Vs. Burrito
 9:30 am | Teen Titans Go! | TV Knight; BBSFBDAY!
10:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
10:30 am | We Bare Bears | The Mummy's Curse; Bearz Ii Men
11:00 am | Amazing World of Gumball | The Name; The Extras
11:30 am | Amazing World Of Gumball | The Agent; The Outside
12:00 pm | Total DramaRama | Duncan Disorderly; Inglorious Toddlers
12:30 pm | Total DramaRama | The Price Of Advice; Hic Hic Hooray
 1:00 pm | Teen Titans Go! | The Croissant; The Dignity of Teeth
 1:30 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 2:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 2:30 pm | Ben 10 | This One Goes to 11
 2:45 pm | Craig of the Creek | Sour Candy Trials
 3:00 pm | Amazing World of Gumball | The Stink; The Ad
 3:30 pm | Amazing World of Gumball | The Awareness; The Slip
 4:00 pm | Total Drama Island | Who Can You Trust?
 4:30 pm | Total Drama Island | Basic Straining
 5:00 pm | Total DramaRama | There Are No Hoppy Endings; A Ninjustice To Harold
 5:30 pm | Victor And Valentino | Lonely Haunts Club; Dead Ringer
 6:00 pm | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Amazing World of Gumball | The Vase; The Matchmaker
 7:30 pm | Amazing World of Gumball | The Box; The Console