# 2019-07-09
 6:00 am | Steven Universe | Pool Hopping
 6:15 am | Teen Titans Go! | Slapping Butts And Celebrating For No Reason
 6:30 am | Teen Titans Go! | How's This For A Special? Spaaaace
 7:00 am | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 7:30 am | Amazing World of Gumball | The Cringe; The Father
 8:00 am | Amazing World of Gumball | The Faith; The Cage
 8:30 am | Amazing World Of Gumball | The Revolt; The Decisions
 9:00 am | Teen Titans Go! | Communicate Openly; Dignity Of Teeth
 9:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
10:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
10:30 am | We Bare Bears | Adopted; Baby Bears On A Plane
11:00 am | Amazing World of Gumball | The Uploads; The Romantic
11:30 am | Amazing World of Gumball | The Hug; The Apprentice
12:00 pm | Total DramaRama | Melter Skelter; That's A Wrap
12:30 pm | Mao Mao, Heroes Of Pure Heart | No Shortcuts; Ultraclops
 1:00 pm | Teen Titans Go! | Little Elvis; them Soviet Boys
 1:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 2:00 pm | Teen Titans Go! | Video Game References; Cool School
 2:30 pm | Ben 10 | Beach Heads
 2:45 pm | Craig of the Creek | Deep Creek Salvage
 3:00 pm | Amazing World of Gumball | The Shippening; The Parents
 3:30 pm | Amazing World of Gumball | The Founder; The Schooling
 4:00 pm | Amazing World of Gumball | The Pest; The Check
 4:30 pm | Amazing World of Gumball | The Sale; The Gift
 5:00 pm | Total DramaRama | Invasion Of The Booger Snatchers; Cone In 60 Seconds
 5:30 pm | Victor And Valentino | Los Cadejos; Hurricane Chata
 6:00 pm | Teen Titans Go! | Garage Sale; Squash & Stretch
 6:30 pm | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
 7:00 pm | Amazing World of Gumball | The Advice; The Signal
 7:30 pm | Amazing World of Gumball | The Parasite; The Love