# 2019-07-10
 6:00 am | Steven Universe | Letters to Lars
 6:15 am | Teen Titans Go! | Business Ethics Wink Wink
 6:30 am | Teen Titans Go! | Tall Titan Tales
 6:45 am | Teen Titans Go! | I Used to Be a Peoples
 7:00 am | Teen Titans Go! | The Croissant
 7:15 am | Teen Titans Go! | The Spice Game
 7:30 am | Amazing World of Gumball | The Candidate
 7:45 am | Amazing World of Gumball | The Anybody
 8:00 am | Amazing World of Gumball | The Pact
 8:15 am | Amazing World of Gumball | The Neighbor
 8:30 am | Amazing World of Gumball | The Kids; The Fan
 9:00 am | Teen Titans Go! | I'm the Sauce
 9:15 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 9:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
10:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
10:30 am | We Bare Bears | Captain Craboo
11:00 am | Amazing World of Gumball | The Wicked
11:15 am | Amazing World of Gumball | The Traitor
11:30 am | Amazing World of Gumball | The Origins
11:45 am | Amazing World of Gumball | The Origins
12:00 pm | Total DramaRama | Wristy Business
12:15 pm | Total DramaRama | The Bad Guy Busters
12:30 pm | Teen Titans Go! | Communicate Openly
12:45 pm | Teen Titans Go! | Lil' Dimples
 1:00 pm | Teen Titans Go! | Don't Be an Icarus
 1:15 pm | Teen Titans Go! | Stockton, CA!
 1:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 2:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 2:30 pm | Ben 10 | Charm School's Out
 2:45 pm | Craig of the Creek | The Shortcut
 3:00 pm | Amazing World of Gumball | The Intelligence
 3:15 pm | Amazing World of Gumball | The Potion
 3:30 pm | Amazing World of Gumball | The Spinoffs
 3:45 pm | Amazing World of Gumball | The Transformation
 4:00 pm | Amazing World of Gumball | The Parking
 4:15 pm | Amazing World of Gumball | The Routine
 4:30 pm | Amazing World of Gumball | The Upgrade
 4:45 pm | Amazing World of Gumball | The Comic
 5:00 pm | Total DramaRama | Gum and Gummer
 5:15 pm | Total DramaRama | Germ Factory
 5:30 pm | Victor and Valentino | Cuddle Monster
 5:45 pm | Victor and Valentino | The Babysitter
 6:00 pm | Teen Titans Go! | Pyramid Scheme
 6:15 pm | Teen Titans Go! | Bottle Episode
 6:30 pm | Teen Titans Go! | Finally a Lesson
 6:45 pm | Teen Titans Go! | Arms Race With Legs
 7:00 pm | Amazing World of Gumball | The Awkwardness
 7:15 pm | Amazing World of Gumball | The Nest
 7:30 pm | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 7:45 pm | Mao Mao: Heroes of Pure Heart | The Perfect Adventure