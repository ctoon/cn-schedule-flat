# 2019-07-16
 6:00 am | Steven Universe | What's Your Problem?
 6:15 am | Teen Titans Go! | Campfire!
 6:30 am | Teen Titans Go! | Communicate Openly; Legendary Sandwich
 7:00 am | Teen Titans Go! | Pyramid Scheme; The Cruel Giggling Ghoul
 7:30 am | Amazing World of Gumball | The Slip; The Drama
 8:00 am | Amazing World of Gumball | The Possession; The Buddy
 8:30 am | Amazing World of Gumball | The Password; The Mothers
 9:00 am | Teen Titans Go! | Royal Jelly; Finally A Lesson
 9:30 am | Teen Titans Go! | Birds; Be Mine
10:00 am | Teen Titans Go! | Brain Food; In and Out
10:30 am | We Bare Bears | Hot Sauce; Bear Cleanse
11:00 am | Amazing World of Gumball | The Blame; The Detective
11:30 am | Amazing World of Gumball | The Fury; The Compilation
12:00 pm | Total DramaRama | Snow Way Out; Sharing Is Caring
12:30 pm | Mao Mao, Heroes Of Pure Heart | Mao Mao's Bike Adventure; Breakup
 1:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 1:30 pm | Teen Titans Go! | Animals: It's Just a Word; A Farce
 2:00 pm | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 2:30 pm | Ben 10 | Which Watch
 2:45 pm | Craig of the Creek | The Mystery Of The Timekeeper
 3:00 pm | Amazing World Of Gumball | The Agent; The Factory
 3:30 pm | Amazing World Of Gumball | The Web; The Mess
 4:00 pm | Amazing World of Gumball | The Love; The Awkwardness
 4:30 pm | Amazing World of Gumball | The Nest; The Points
 5:00 pm | Total DramaRama | Toys Will Be Toys; Aquarium For A Dream
 5:30 pm | Victor And Valentino | Welcome to the Underworld
 6:00 pm | Teen Titans Go! | Think About Your Future; Booty Scooty
 6:30 pm | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 7:00 pm | Amazing World of Gumball | The Vision; The Boredom
 7:30 pm | Amazing World of Gumball | The Choices; The Code