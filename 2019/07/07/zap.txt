# 2019-07-07
 6:00 am | Teen Titans Go! | Tall Titan Tales
 6:15 am | Teen Titans Go! | I Used to Be a Peoples
 6:30 am | Teen Titans Go! | Genie President
 6:45 am | Teen Titans Go! | Rad Dudes With Bad Tudes
 7:00 am | Teen Titans Go! | Business Ethics Wink Wink
 7:15 am | Teen Titans Go! | Wally T
 7:30 am | Teen Titans Go! | Communicate Openly
 7:45 am | Teen Titans Go! | What We Learned at Camp
 8:00 am | Total DramaRama | Duncan Disorderly
 8:15 am | Total DramaRama | A Licking Time Bomb
 8:30 am | Total DramaRama | Mother of All Cards
 8:45 am | Total DramaRama | Know It All
 9:00 am | Craig of the Creek | Fort Williams
 9:15 am | Craig of the Creek | Memories of Bobby
 9:30 am | Victor and Valentino | It Grows
 9:45 am | Victor and Valentino | Lonely Haunts Club
10:00 am | Teen Titans Go! | Two Parter: Part One
10:15 am | Teen Titans Go! | Two Parter: Part Two
10:30 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
10:45 am | Teen Titans Go! | Arms Race With Legs
11:00 am | Teen Titans Go! | BBRBDAY
11:15 am | Teen Titans Go! | Finally a Lesson
11:30 am | Teen Titans Go! | Communicate Openly
11:45 am | Teen Titans Go! | What We Learned at Camp
12:00 pm | Summer Camp Island | Susie's Fantastical Scavenger Hunt
12:15 pm | Summer Camp Island | Mop Forever
12:30 pm | Summer Camp Island | Pajamas Party
12:45 pm | Summer Camp Island | The Soundhouse
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition (Alternate)
 1:30 pm | Teen Titans Go! | The Groover
 1:45 pm | Teen Titans Go! | Bottle Episode
 2:00 pm | Amazing World of Gumball | The Future
 2:15 pm | Amazing World of Gumball | The Wish
 2:30 pm | Amazing World of Gumball | The Silence
 2:45 pm | Amazing World of Gumball | The Stories
 3:00 pm | Amazing World of Gumball | The Master
 3:15 pm | Amazing World of Gumball | The Compilation
 3:30 pm | Amazing World of Gumball | The Possession
 3:45 pm | Amazing World of Gumball | The Fury
 4:00 pm | Amazing World of Gumball | The Buddy
 4:15 pm | Amazing World of Gumball | The Detective
 4:30 pm | OK K.O.! Let's Be Heroes | We Are Heroes
 4:45 pm | OK K.O.! Let's Be Heroes | K.O., Rad and Enid!
 5:00 pm | OK K.O.! Let's Be Heroes | Dark Plaza
 5:30 pm | Amazing World of Gumball | The Revolt
 5:45 pm | Amazing World of Gumball | The Decisions
 6:00 pm | Amazing World of Gumball | The BFFS
 6:15 pm | Amazing World of Gumball | The Inquisition
 6:30 pm | Teen Titans Go! | Super Hero Summer Camp
 7:45 pm | Teen Titans Go! | Communicate Openly