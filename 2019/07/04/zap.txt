# 2019-07-04
 6:00 am | Steven Universe | Jungle Moon
 6:15 am | Teen Titans Go! | My Name Is Jose
 6:30 am | Teen Titans Go! | Kabooms
 7:00 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 7:30 am | Amazing World of Gumball | The Worst
 7:45 am | Amazing World of Gumball | The Deal
 8:00 am | Amazing World of Gumball | The Petals
 8:15 am | Amazing World of Gumball | The Nuisance
 8:30 am | Amazing World of Gumball | The Drama
 8:45 am | Amazing World of Gumball | The Buddy
 9:00 am | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 9:15 am | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
 9:30 am | Teen Titans Go! | Hot Salad Water
 9:45 am | Teen Titans Go! | Dude Relax
10:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
10:30 am | We Bare Bears | Baby Orphan Ninja Bears
10:45 am | We Bare Bears | Coffee Cave
11:00 am | Amazing World of Gumball | The Nemesis
11:15 am | Amazing World of Gumball | The Crew
11:30 am | Amazing World of Gumball | The Others
11:45 am | Amazing World of Gumball | The Signature
12:00 pm | Total DramaRama | Too Much of a Goo'd Thing
12:15 pm | Total DramaRama | Having the Timeout of Our Lives
12:30 pm | Teen Titans Go! | Super Hero Summer Camp
 1:45 pm | Teen Titans Go! | Hot Salad Water
 2:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 2:30 pm | Ben 10 | Show Don't Tell
 2:45 pm | Craig of the Creek | Jessica's Trail
 3:00 pm | Amazing World of Gumball | The Lady
 3:15 pm | Amazing World of Gumball | The Sucker
 3:30 pm | Amazing World of Gumball | The Vegging
 3:45 pm | Amazing World of Gumball | The One
 4:00 pm | Amazing World of Gumball | The Society; The Spoiler
 4:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 5:00 pm | Total DramaRama | The Never Gwending Story
 5:15 pm | Total DramaRama | Tiger Fail
 5:30 pm | Victor and Valentino | Lonely Haunts Club
 5:45 pm | Victor and Valentino | Folk Art Foes
 6:00 pm | Teen Titans Go! | Hot Salad Water
 6:15 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 6:30 pm | Teen Titans Go! | Grube's Fairytales
 6:45 pm | Teen Titans Go! | A Farce
 7:00 pm | Amazing World of Gumball | The Routine
 7:15 pm | Amazing World of Gumball | The Upgrade
 7:30 pm | Amazing World of Gumball | The Comic
 7:45 pm | Amazing World of Gumball | The Romantic