# 2019-07-06
 6:00 am | Teen Titans Go! | Stockton, CA!
 6:15 am | Teen Titans Go! | Oh Yeah!
 6:30 am | Teen Titans Go! | Don't Be an Icarus
 6:45 am | Teen Titans Go! | Snuggle Time
 7:00 am | Teen Titans Go! | Lil' Dimples
 7:15 am | Teen Titans Go! | Oregon Trail
 7:30 am | Teen Titans Go! | TV Knight 4
 7:45 am | Teen Titans Go! | Who's Laughing Now
 8:00 am | Total DramaRama | Camping Is in Tents
 8:30 am | Total DramaRama | Soother or Later
 8:45 am | Total DramaRama | From Badge to Worse
 9:00 am | Craig of the Creek | Turning the Tables
 9:15 am | Craig of the Creek | Summer Wish
 9:30 am | Victor and Valentino | Churro Kings
 9:45 am | Victor and Valentino | A New Don
10:00 am | Teen Titans Go! | Communicate Openly
10:15 am | Teen Titans Go! | What We Learned at Camp
10:30 am | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
10:45 am | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
11:00 am | Teen Titans Go! | Them Soviet Boys
11:15 am | Teen Titans Go! | Think About Your Future
11:30 am | Teen Titans Go! | The Chaff
11:45 am | Teen Titans Go! | The Art of Ninjutsu
12:00 pm | Ben 10 | Big in Japan
12:15 pm | Ben 10 | Cyber Slammers
12:30 pm | LEGO Ninjago | Booby-Traps, and How to Survive Them
12:45 pm | LEGO Ninjago | The News Never Sleeps!
 1:00 pm | Teen Titans Go! | The Metric System vs. Freedom
 1:15 pm | Teen Titans Go! | History Lesson
 1:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 1:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 2:00 pm | Amazing World of Gumball | The Inquisition
 2:15 pm | Amazing World of Gumball | The Copycats
 2:30 pm | Amazing World of Gumball | The BFFS
 2:45 pm | Amazing World of Gumball | The Loophole
 3:00 pm | Amazing World of Gumball | The Decisions
 3:15 pm | Amazing World of Gumball | The Slide
 3:30 pm | Amazing World of Gumball | The Revolt
 3:45 pm | Amazing World of Gumball | The Test
 4:00 pm | Amazing World of Gumball | The Heart
 4:15 pm | Amazing World of Gumball | The Code
 4:30 pm | Amazing World of Gumball | The Mess
 4:45 pm | Amazing World of Gumball | The Choices
 5:00 pm | Amazing World of Gumball | The Web
 5:15 pm | Amazing World of Gumball | The Vision
 5:30 pm | Amazing World of Gumball | The Agent
 5:45 pm | Amazing World of Gumball | The Boredom
 6:00 pm | Amazing World of Gumball | The Factory
 6:15 pm | Amazing World of Gumball | The Guy
 6:30 pm | Amazing World of Gumball | The Disaster
 6:45 pm | Amazing World of Gumball | The Re-Run
 7:00 pm | Craig of the Creek | The Other Side
 7:30 pm | Victor and Valentino | Welcome to the Underworld