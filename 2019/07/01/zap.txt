# 2019-07-01
 6:00 am | Steven Universe | Sadie Killer
 6:15 am | Teen Titans Go! | BBCYFSHIPBDAY
 6:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
 7:30 am | Amazing World of Gumball | The Ollie
 7:45 am | Amazing World of Gumball | The Catfish
 8:00 am | Amazing World of Gumball | The Cycle
 8:15 am | Amazing World of Gumball | The Stars
 8:30 am | Amazing World of Gumball | The Parents
 8:45 am | Amazing World of Gumball | The Founder
 9:00 am | Teen Titans Go! | Road Trip; The Best Robin
 9:30 am | Teen Titans Go! | Them Soviet Boys
 9:45 am | Teen Titans Go! | Little Elvis
10:00 am | Teen Titans Go! | TV Knight 4
10:15 am | Teen Titans Go! | Lil' Dimples
10:30 am | We Bare Bears | Band of Outsiders
10:45 am | We Bare Bears | Dog Hotel
11:00 am | Amazing World of Gumball | The Oracle; The Safety
11:30 am | Amazing World of Gumball | The Friend; The Saint
12:00 pm | Total DramaRama | Duncan Disorderly
12:15 pm | Total DramaRama | Inglorious Toddlers
12:30 pm | Teen Titans Go! | Mo' Money Mo' Problems
12:45 pm | Teen Titans Go! | TV Knight 3
 1:00 pm | Teen Titans Go! | The Scoop
 1:15 pm | Teen Titans Go! | Chicken in the Cradle
 1:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 2:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 2:30 pm | Ben 10 | This One Goes to 11
 2:45 pm | Craig of the Creek | Summer Wish
 3:00 pm | Amazing World of Gumball | The Menu
 3:15 pm | Amazing World of Gumball | The Uncle
 3:30 pm | Amazing World of Gumball | The Heist
 3:45 pm | Amazing World of Gumball | The Best
 4:00 pm | Amazing World of Gumball | The Mirror; The Burden
 4:30 pm | Amazing World of Gumball | The Bros; The Man
 5:00 pm | Total DramaRama | The Price of Advice
 5:15 pm | Total DramaRama | Hic Hic Hooray
 5:30 pm | Victor and Valentino | A New Don
 5:45 pm | Victor and Valentino | It Grows
 6:00 pm | Teen Titans Go! | Communicate Openly
 6:10 pm | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 6:28 pm | Mao Mao: Heroes of Pure Heart | The Perfect Adventure
 6:45 pm | Teen Titans Go! | Super Hero Summer Camp