# 2019-07-08
 6:00 am | Steven Universe | The Big Show
 6:15 am | Teen Titans Go! | BBRBDAY
 6:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:30 am | Amazing World of Gumball | The Sucker; The Lady
 8:00 am | Amazing World of Gumball | The One; The Vegging
 8:30 am | Amazing World Of Gumball | The Web; The Agent
 9:00 am | Teen Titans Go! | Operation Tin Man
 9:15 am | Teen Titans Go! | Super Summer Hero Camp
10:30 am | Scooby-doo And Guess Who? | Revenge Of The Swamp Monster!
11:00 am | Amazing World of Gumball | The Routine; The Parking
11:30 am | Amazing World of Gumball | The Comic; The Upgrade
12:00 pm | Total DramaRama | Tiger Fail; The Never Gwending Story
12:30 pm | Teen Titans Go! | Genie President; Business Ethics Wink Wink
 1:00 pm | Teen Titans Go! | I Used To Be A Peoples; Tall Titan Tales
 1:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 2:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 2:30 pm | Ben 10 | Bridge Out
 2:45 pm | Craig of the Creek | Turning the Tables
 3:00 pm | Amazing World of Gumball | The Anybody; The Candidate
 3:30 pm | Amazing World of Gumball | The Neighbor; The Pact
 4:00 pm | Amazing World of Gumball | The Nemesis; The Crew
 4:30 pm | Amazing World of Gumball | The Signature; The Others
 5:00 pm | Total DramaRama | The Bad Guy Busters; Wristy Business
 5:30 pm | Victor And Valentino | Churro Kings; A New Don
 6:00 pm | Teen Titans Go! | Royal Jelly
 6:10 pm | Mao Mao, Heroes of Pure Heart | No Shortcuts
 6:28 pm | Mao Mao, Heroes of Pure Heart | Ultraclops
 6:45 pm | Teen Titans | Titans East
 7:45 pm | Teen Titans Go! | Communicate Openly