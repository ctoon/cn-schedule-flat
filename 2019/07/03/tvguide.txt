# 2019-07-03
 6:00 am | Steven Universe | Lars Of The Stars
 6:15 am | Teen Titans Go! | Mo' Money Mo' Problems
 6:30 am | Teen Titans Go! | The Scoop; Chicken in the Cradle
 7:00 am | Teen Titans Go! | What We Learned At Camp; Smile Bones
 7:30 am | Amazing World of Gumball | The Uncle; The Menu
 8:00 am | Amazing World of Gumball | The Best; The Heist
 8:30 am | Amazing World of Gumball | The Stink; The Ad
 9:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
 9:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
10:00 am | Teen Titans Go! | Double Trouble; The Date
10:30 am | We Bare Bears | Sandcastle; The Fair
11:00 am | Amazing World of Gumball | The Egg; The Downer
11:30 am | Amazing World of Gumball | The Triangle; The Return
12:00 pm | Total DramaRama | The Price Of Advice; Hic Hic Hooray
12:30 pm | Teen Titans Go! | The Power Of Shrimps; The Real Orangins!
 1:00 pm | Teen Titans Go! | Quantum Fun; The Fight
 1:30 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
 2:00 pm | Teen Titans Go! | Road Trip; The Best Robin
 2:30 pm | Ben 10 | Poles Apart
 2:45 pm | Craig of the Creek | Dinner At the Creek
 3:00 pm | Amazing World of Gumball | The List; The Line
 3:30 pm | Amazing World of Gumball | The News; The Rival
 4:00 pm | Amazing World of Gumball | The Oracle; The Safety
 4:30 pm | Amazing World of Gumball | The Friend; The Saint
 5:00 pm | Total DramaRama | There Are No Hoppy Endings; A Ninjustice To Harold
 5:30 pm | Victor And Valentino | Suerte; Dead Ringer
 6:00 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 6:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
 7:00 pm | Amazing World of Gumball | The Sale; The Pest
 7:30 pm | Mao Mao, Heroes Of Pure Heart | I Love You Mao Mao; The Perfect Adventure