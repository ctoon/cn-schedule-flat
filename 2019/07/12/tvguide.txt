# 2019-07-12
 6:00 am | Steven Universe | A Single Pale Rose
 6:15 am | Teen Titans Go! | Lil' Dimples
 6:30 am | Teen Titans Go! | Stockton, Ca!; Don't Be An Icarus
 7:00 am | Teen Titans Go! | Animals: It's Just a Word; A Farce
 7:30 am | Amazing World of Gumball | The Intelligence; The Potion
 8:00 am | Amazing World of Gumball | The Spinoffs; The Transformation
 8:30 am | Amazing World of Gumball | The Gripes; The Vacation
 9:00 am | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 9:30 am | Teen Titans Go! | No Power; Sidekick
10:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
10:30 am | We Bare Bears | Escandalosos; Bear Flu
11:00 am | Amazing World of Gumball | The Love; The Awkwardness
11:30 am | Amazing World of Gumball | The Nest; The Points
12:00 pm | Scooby-Doo and Guess Who? | Revenge of the Swamp Monster!
12:30 pm | Teen Titans Go! | What We Learned At Camp; Campfire!
 1:00 pm | Teen Titans Go! | Communicate Openly; Legendary Sandwich
 1:30 pm | Teen Titans Go! | The Croissant; Spice Game
 2:00 pm | Teen Titans Go! | I'm the Sauce; Hey You, Don't Forget About Me In Your Memory
 2:30 pm | Ben 10 | Franken-Fight
 2:45 pm | Craig of the Creek | The Great Fossil Rush
 3:00 pm | Amazing World of Gumball | The Slip; The Drama
 3:30 pm | Amazing World of Gumball | The Possession; The Buddy
 4:00 pm | Amazing World of Gumball | The Traitor; The Wicked
 4:30 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 5:00 pm | Total DramaRama | Snow Way Out; Sharing Is Caring
 5:30 pm | Mao Mao, Heroes Of Pure Heart | No Shortcuts; Ultraclops
 6:00 pm | Teen Titans Go! | Island Adventures
 7:00 pm | Amazing World of Gumball | The Slap; The Detective
 7:30 pm | Amazing World of Gumball | The Fury; The Compilation