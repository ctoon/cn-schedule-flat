# 2019-07-12
 6:00 am | Steven Universe | A Single Pale Rose
 6:15 am | Teen Titans Go! | Lil' Dimples
 6:30 am | Teen Titans Go! | Don't Be an Icarus
 6:45 am | Teen Titans Go! | Stockton, CA!
 7:00 am | Teen Titans Go! | A Farce
 7:15 am | Teen Titans Go! | Animals: It's Just a Word!
 7:30 am | Amazing World of Gumball | The Intelligence
 7:45 am | Amazing World of Gumball | The Potion
 8:00 am | Amazing World of Gumball | The Spinoffs
 8:15 am | Amazing World of Gumball | The Transformation
 8:30 am | Amazing World of Gumball | The Gripes; The Vacation
 9:00 am | Teen Titans Go! | Two Parter: Part One
 9:15 am | Teen Titans Go! | Two Parter: Part Two
 9:30 am | Teen Titans Go! | No Power; Sidekick
10:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
10:30 am | We Bare Bears | Escandalosos
10:45 am | We Bare Bears | Bear Flu
11:00 am | Amazing World of Gumball | The Love
11:15 am | Amazing World of Gumball | The Awkwardness
11:30 am | Amazing World of Gumball | The Nest
11:45 am | Amazing World of Gumball | The Points
12:00 pm | Scooby-Doo and Guess Who? | Revenge of the Swamp Monster!
12:30 pm | Teen Titans Go! | Campfire!
12:45 pm | Teen Titans Go! | What We Learned at Camp
 1:00 pm | Teen Titans Go! | Communicate Openly
 1:15 pm | Teen Titans Go! | Legendary Sandwich
 1:30 pm | Teen Titans Go! | The Croissant
 1:45 pm | Teen Titans Go! | The Spice Game
 2:00 pm | Teen Titans Go! | I'm the Sauce
 2:15 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 2:30 pm | Ben 10 | Franken-Fight
 2:45 pm | Craig of the Creek | The Great Fossil Rush
 3:00 pm | Amazing World of Gumball | The Slip
 3:15 pm | Amazing World of Gumball | The Drama
 3:30 pm | Amazing World of Gumball | The Buddy
 3:45 pm | Amazing World of Gumball | The Possession
 4:00 pm | Amazing World of Gumball | The Wicked
 4:15 pm | Amazing World of Gumball | The Traitor
 4:30 pm | Amazing World of Gumball | The Origins
 4:45 pm | Amazing World of Gumball | The Origins
 5:00 pm | Total DramaRama | Snow Way Out
 5:15 pm | Total DramaRama | Sharing Is Caring
 5:30 pm | Mao Mao: Heroes of Pure Heart | No Shortcuts
 5:45 pm | Mao Mao: Heroes of Pure Heart | Ultraclops
 6:00 pm | Teen Titans Go! | Island Adventures
 7:00 pm | Amazing World of Gumball | The Slap
 7:15 pm | Amazing World of Gumball | The Detective
 7:30 pm | Amazing World of Gumball | The Fury
 7:45 pm | Amazing World of Gumball | The Compilation