# 2019-07-14
 6:00 am | Teen Titans Go! | Chicken In The Cradle; A Farce
 6:30 am | Teen Titans Go! | The Scoop; Grube's Fairytales
 7:00 am | Teen Titans Go! | Tv Knight 3; 40%, 40%, 20%
 7:30 am | Teen Titans Go! | Royal Jelly; Communicate Openly
 8:00 am | Total DramaRama | There Are No Hoppy Endings; Not Without My Fudgy Lumps
 8:30 am | Total DramaRama | The Never Gwending Story; Inglorious Toddlers
 9:00 am | Craig Of The Creek | The Other Side
 9:30 am | Victor And Valentino | Welcome to the Underworld
10:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
10:30 am | Teen Titans Go! | Bro-pocalypse; Accept The Next Proposition You Hear
11:00 am | Teen Titans Go! | Flashback
11:30 am | Teen Titans Go! | Royal Jelly; Communicate Openly
12:00 pm | Summer Camp Island | Puff Paint
12:15 pm | Summer Camp Island | Susie Appreciation Day
12:30 pm | Summer Camp Island | Campers Above the Bed
12:45 pm | Summer Camp Island | Midnight Quittance
 1:00 pm | Teen Titans Go! | Beast Girl; I'm The Sauce
 1:30 pm | Teen Titans Go! | BBCYFSHIPBDAY;Spice Game
 2:00 pm | Amazing World Of Gumball | The Brain; The Origins
 2:30 pm | Amazing World Of Gumball | The Origins Part 2; The Shippening
 3:00 pm | Amazing World Of Gumball | The Neighbor; The Traitor
 3:30 pm | Amazing World Of Gumball | The Pact; The Wicked
 4:00 pm | Amazing World Of Gumball | The Anybody; The Hug
 4:30 pm | Ok K.o.! Let's Be Heroes | Tko Rules!
 4:45 pm | Ok K.o.! Let's Be Heroes | Chip's Damage
 5:00 pm | Ok K.o.! Let's Be Heroes | Tko's House; Final Exams
 5:30 pm | Amazing World Of Gumball | The Candidate; The Apprentice
 6:00 pm | Amazing World Of Gumball | The Faith; The Uploads
 6:30 pm | Amazing World Of Gumball | The Cage; The Romantic
 7:00 pm | Craig Of The Creek | Kelsey The Author; Dibs Court
 7:30 pm | Victor And Valentino | Boss For A Day; Cleaning Day