# 2019-07-21
 6:00 am | Steven Universe | Steven's Birthday; Steven Floats
 6:30 am | Steven Universe | Storm In The Room; Familiar
 7:00 am | Teen Titans Go! | Hot Salad Water; The Hive Five
 7:30 am | Teen Titans Go! | Strength Of A Grown Man; Royal Jelly
 8:00 am | Total DramaRama | Invasion Of The Booger Snatchers; Having The Timeout Of Our Lives
 8:30 am | Total DramaRama | Gum And Gummer; A Ninjustice To Harold
 9:00 am | Craig Of The Creek | Return Of The Honeysuckle Rangers; The Shortcut
 9:30 am | Victor And Valentino | The Boy Who Cried Lechuza; Legend Of The Hidden Skate Park
10:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
10:30 am | Teen Titans Go! | Brain Percentages
10:45 am | Teen Titans Go! | Super Summer Hero Camp
12:00 pm | Summer Camp Island | The Great Elf Invention Convention
12:15 pm | Summer Camp Island | Twelve Angry Hedgehogs
12:30 pm | Summer Camp Island | Spell Crushers
12:45 pm | Summer Camp Island | The Library
 1:00 pm | Teen Titans Go! | Jinxed; Nean
 1:30 pm | Teen Titans Go! | Orangins; Operation Tin Man
 2:00 pm | Amazing World Of Gumball | The Nuisance; The Crew
 2:30 pm | Amazing World Of Gumball | The Puppets; The Egg
 3:00 pm | Amazing World Of Gumball | The Petals; The Nemesis
 3:30 pm | Amazing World Of Gumball | The Deal; The Money
 4:00 pm | Amazing World Of Gumball | The Worst; The Triangle
 4:30 pm | Ok K.o.! Let's Be Heroes | K.o. Vs Fink
 4:45 pm | Ok K.o.! Let's Be Heroes | The K.o. Trap
 5:00 pm | Ok K.o.! Let's Be Heroes | All In The Villainy; We're Captured
 5:30 pm | Amazing World of Gumball | The Return; The Best
 6:00 pm | Amazing World Of Gumball | The Singing; The Downer
 6:30 pm | Amazing World Of Gumball | The Heist; The Nobody
 7:00 pm | Craig Of The Creek | Council Of The Creek; Deep Creek Salvage
 7:30 pm | Victor And Valentino | The Collector; Chata's Quinta Quinceaera