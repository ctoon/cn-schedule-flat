# 2019-01-11
 8:00 pm | Samurai Jack | X
 8:30 pm | American Dad | I Ain't No Holodeck Boy
 9:00 pm | American Dad | Stan Goes on the Pill
 9:30 pm | The Jellies | Pilot
 9:45 pm | The Jellies | Mervin for Mayor
10:00 pm | Bob's Burgers | Seaplane!
10:30 pm | Family Guy | Spies Reminiscent of Us
11:00 pm | Family Guy | Brian's Got a Brand New Bag
11:30 pm | Rick and Morty | Auto Erotic Assimilation
12:00 am | Joe Pera Talks With You | Joe Pera Talks You Back to Sleep
12:15 am | Joe Pera Talks With You | Joe Pera Talks To You About The Rat Wars of Alberta, Canada, 1950 - Present Day
12:30 am | Eagleheart | Chris & Susie & Brett & Malice
12:45 am | Eagleheart | Susie's Song
 1:00 am | Tigtone | Tigtone and the Pilot
 1:15 am | The Shivering Truth | Pilot
 1:30 am | Bob's Burgers | Seaplane!
 2:00 am | Family Guy | Spies Reminiscent of Us
 2:30 am | Family Guy | Brian's Got a Brand New Bag
 3:00 am | Rick and Morty | Auto Erotic Assimilation
 3:30 am | Joe Pera Talks With You | Joe Pera Talks You Back to Sleep
 3:45 am | Joe Pera Talks With You | Joe Pera Talks To You About The Rat Wars of Alberta, Canada, 1950 - Present Day
 4:00 am | FishCenter | FishCenter
 4:15 am | Digikiss | Digikiss
 4:30 am | Tigtone | Tigtone and the Pilot
 4:45 am | The Shivering Truth | Pilot
 5:00 am | American Dad | I Ain't No Holodeck Boy
 5:30 am | American Dad | Stan Goes on the Pill