# 2019-01-09
 6:00 am | Steven Universe | Onion Trade
 6:15 am | Amazing World of Gumball | The Party
 6:30 am | Amazing World of Gumball | The Robot; The Picnic
 7:00 am | Total Drama Island | No Pain, No Game
 7:30 am | Total Drama Island | Search and Do Not Destroy
 8:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 8:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:00 am | Craig of the Creek | Sunday Clothes
 9:15 am | Craig of the Creek | The Last Kid in the Creek
 9:30 am | Amazing World of Gumball | The Mothers; The Password
10:00 am | Amazing World of Gumball | The Procrastinators; The Shell
10:30 am | Total DramaRama | Bananas & Cheese
10:45 am | Total DramaRama | Cuttin' Corners
11:00 am | Teen Titans Go! | Titan Saving Time
11:15 am | Teen Titans Go! | Animals: It's Just a Word!
11:30 am | Teen Titans Go! | Master Detective
11:45 am | Teen Titans Go! | BBBDay!
12:00 pm | Unikitty | Chair
12:15 pm | Unikitty | Crushing Defeat
12:30 pm | Amazing World of Gumball | The Rival
12:45 pm | Amazing World of Gumball | The Guy
 1:00 pm | Amazing World of Gumball | The One
 1:15 pm | Amazing World of Gumball | The Boredom
 1:30 pm | We Bare Bears | Family Troubles
 1:45 pm | We Bare Bears | Icy Nights II
 2:00 pm | Teen Titans Go! | Brain Food; In and Out
 2:30 pm | Teen Titans Go! | Little Buddies; Missing
 3:00 pm | Ben 10 | Drone On
 3:15 pm | Amazing World of Gumball | The Mustache
 3:30 pm | Amazing World of Gumball | The Club; The Wand
 4:00 pm | Total Drama Island | Trial by Tri-ArmedTriathalon
 4:30 pm | Total Drama Island | Haute Camp-ture
 5:00 pm | Craig of the Creek | Big Pinchy
 5:15 pm | Craig of the Creek | Monster in the Garden
 5:30 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:45 pm | Total DramaRama | Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Two Parter: Part Two
 6:15 pm | Teen Titans Go! | Employee of the Month Redux
 6:30 pm | Teen Titans Go! | Orangins
 6:45 pm | Teen Titans Go! | Garage Sale
 7:00 pm | We Bare Bears | Crowbar Jones: Origins
 7:15 pm | We Bare Bears | Bunnies
 7:30 pm | Amazing World of Gumball | The Neighbor
 7:45 pm | Amazing World of Gumball | The Test