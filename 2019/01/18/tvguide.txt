# 2019-01-18
 6:00 am | Steven Universe | Steven and the Stevens
 6:15 am | Amazing World of Gumball | The Flower
 6:30 am | Amazing World of Gumball | The Phone; The Job
 7:00 am | Total Drama Island | The Big Sleep
 7:30 am | Total Drama Island | Dodgebrawl
 8:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 8:30 am | Teen Titans Go! | Friendship; Vegetables
 9:00 am | Craig of the Creek | The Future Is Cardboard; Jextra Perrestrial
 9:30 am | Amazing World of Gumball | The Uploads; The Crew
10:00 am | Amazing World of Gumball | The Wicked; The Others
10:30 am | Total DramaRama | Tiger Fail; Duck Duck Juice
11:00 am | Teen Titans Go! | Rad Dudes With Bad Tudes; Career Day
11:30 am | Teen Titans Go! | History Lesson; TV Knight 2
12:00 pm | Unikitty | Tragic Magic; Birthday Blowout
12:30 pm | Amazing World of Gumball | The Parents; The Console
 1:00 pm | Amazing World of Gumball | The Founder; The Outside
 1:30 pm | We Bare Bears | Pizza Band; Vacation
 2:00 pm | Teen Titans Go! | Road Trip; The Best Robin
 2:30 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 3:00 pm | Ben 10 | All Koiled Up
 3:15 pm | Amazing World of Gumball | The Bumpkin
 3:30 pm | Amazing World of Gumball | The Authority; The Virus
 4:00 pm | Total Drama Island | Paintball Deer Hunter
 4:30 pm | Total Drama Island | If You Can't Take the Heat
 5:00 pm | Craig of the Creek | Under The Overpass; Dinner At The Creek
 5:30 pm | Total DramaRama | Having The Timeout Of Our Lives; Free Chili
 6:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 7:00 pm | We Bare Bears | Wingmen; The Park
 7:30 pm | Amazing World of Gumball | The Spinoffs; The Stars