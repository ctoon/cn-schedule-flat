# 2019-01-16
 6:00 am | Steven Universe | Coach Steven
 6:15 am | Amazing World of Gumball | The Helmet
 6:30 am | Amazing World of Gumball | The End; The DVD
 7:00 am | Total Drama Island | I Triple Dog Dare You
 7:30 am | Total Drama Island | The Very Last Episode, Really!
 8:00 am | Teen Titans Go! | Pirates; I See You
 8:30 am | Teen Titans Go! | Brian; Nature
 9:00 am | Craig of the Creek | Bring Out Your Beast; Creek Cart Racers
 9:30 am | Amazing World of Gumball | The Egg; The Downer
10:00 am | Amazing World of Gumball | The Triangle; The Money
10:30 am | Total DramaRama | Know It All; The Bad Guy Busters
11:00 am | Teen Titans Go! | Finally a Lesson; Lication
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:00 pm | Unikitty | Beach Daze; Pet Pet
12:30 pm | Amazing World of Gumball | The Shippening; The Vase
 1:00 pm | Amazing World of Gumball | The Brain; The Ollie
 1:30 pm | We Bare Bears | Rescue Ranger; Bearz Ii Men
 2:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 2:30 pm | Teen Titans Go! | Friendship; Vegetables
 3:00 pm | Ben 10 | The Charm Offensive
 3:15 pm | Amazing World of Gumball | The Flower
 3:30 pm | Amazing World of Gumball | The Phone; The Job
 4:00 pm | Total Drama Island | Not Quite Famous
 4:30 pm | Total Drama Island | The Sucky Outdoors
 5:00 pm | Craig of the Creek | The Future Is Cardboard; Jextra Perrestrial
 5:30 pm | Total DramaRama | Tiger Fail; Duck Duck Juice
 6:00 pm | Teen Titans Go! | Rad Dudes With Bad Tudes; Career Day
 6:30 pm | Teen Titans Go! | History Lesson; TV Knight 2
 7:00 pm | We Bare Bears | Pizza Band; Vacation
 7:30 pm | Amazing World of Gumball | The Founder; The Outside