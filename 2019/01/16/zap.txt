# 2019-01-16
 6:00 am | Steven Universe | Coach Steven
 6:15 am | Amazing World of Gumball | The Helmet
 6:30 am | Amazing World of Gumball | The End; The DVD
 7:00 am | Total Drama Island | I Triple Dog Dare You
 7:30 am | Total Drama Island | The Very Last Episode, Really!
 8:00 am | Teen Titans Go! | Pirates; I See You
 8:30 am | Teen Titans Go! | Brian; Nature
 9:00 am | Craig of the Creek | Bring Out Your Beast
 9:15 am | Craig of the Creek | Creek Cart Racers
 9:30 am | Amazing World of Gumball | The Downer; The Egg
10:00 am | Amazing World of Gumball | The Triangle; The Money
10:30 am | Total DramaRama | Know It All
10:45 am | Total DramaRama | The Bad Guy Busters
11:00 am | Teen Titans Go! | Lication
11:15 am | Teen Titans Go! | Finally a Lesson
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:00 pm | Unikitty | Beach Daze
12:15 pm | Unikitty | Pet Pet
12:30 pm | Amazing World of Gumball | The Shippening
12:45 pm | Amazing World of Gumball | The Vase
 1:00 pm | Amazing World of Gumball | The Brain
 1:15 pm | Amazing World of Gumball | The Ollie
 1:30 pm | We Bare Bears | Rescue Ranger
 1:45 pm | We Bare Bears | Bearz II Men
 2:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 2:30 pm | Teen Titans Go! | Friendship; Vegetables
 3:00 pm | Ben 10 | The Charm Offensive
 3:15 pm | Amazing World of Gumball | The Flower
 3:30 pm | Amazing World of Gumball | The Phone; The Job
 4:00 pm | Total Drama Island | Not Quite Famous
 4:30 pm | Total Drama Island | Sucky Outdoors
 5:00 pm | Craig of the Creek | Jextra Perrestrial
 5:15 pm | Craig of the Creek | The Future Is Cardboard
 5:30 pm | Total DramaRama | Tiger Fail
 5:45 pm | Total DramaRama | Duck Duck Juice
 6:00 pm | Teen Titans Go! | Career Day
 6:15 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 6:30 pm | Teen Titans Go! | TV Knight 2
 6:45 pm | Teen Titans Go! | History Lesson
 7:00 pm | We Bare Bears | Pizza Band
 7:15 pm | We Bare Bears | Vacation
 7:30 pm | Amazing World of Gumball | The Founder
 7:45 pm | Amazing World of Gumball | The Outside