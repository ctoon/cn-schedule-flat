# 2019-01-06
 6:00 am | Ben 10 | Past Aliens Present; Vote Zombozo!
 6:30 am | Mega Man: Fully Charged | Swish
 6:45 am | Teen Titans Go! | Dude Relax!
 7:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 7:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 8:00 am | Teen Titans Go! | The Groover; Booby Trap House
 8:30 am | Teen Titans Go! | Shrimps And Prime Rib; The Fight
 9:00 am | Craig of the Creek | Jessica's Trail; Big Pinchy
 9:30 am | Craig of the Creek | Dinner At The Creek; The Climb
10:00 am | Teen Titans Go! | Quantum Fun; The Overbite
10:30 am | Teen Titans Go! | Tower Renovation; Riding The Dragon
11:00 am | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
11:30 am | Teen Titans Go! | Oh Yeah!; Snuggle Time
12:00 pm | Teen Titans Go! | Oregon Trail; Chicken in the Cradle
12:30 pm | Teen Titans Go! | Who's Laughing Now; The Scoop
 1:00 pm | Total DramaRama | Inglorious Toddlers; Cuttin' Corners
 1:30 pm | Total DramaRama | Bananas & Cheese; Aquarium For A Dream
 2:00 pm | Amazing World of Gumball | The Friend; The Saint
 2:30 pm | Amazing World of Gumball | The Oracle; The Safety
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Pizza; The Lie
 4:00 pm | Total DramaRama | Hic Hic Hooray; The Date
 4:30 pm | Total DramaRama | Having The Timeout Of Our Lives; Free Chili
 5:00 pm | Amazing World of Gumball | The Stink; The Best
 5:30 pm | Amazing World of Gumball | The Ad; The Heist
 6:00 pm | Amazing World of Gumball | The Understanding; The Uncle
 6:30 pm | Amazing World of Gumball | The Transformation; The Menu
 7:00 pm | Total Drama Island | No Pain, No Game
 7:30 pm | Total Drama Island | Search and Do Not Destroy