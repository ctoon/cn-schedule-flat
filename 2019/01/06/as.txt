# 2019-01-06
 8:00 pm | Ranger Smith | Boo Boo Runs Wild
 8:30 pm | Harvey Birdman | Bannon Custody Battle
 8:45 pm | Harvey Birdman | Very Personal Injury
 9:00 pm | Mike Tyson Mysteries | The End
 9:15 pm | Mike Tyson Mysteries | Ultimate Judgment Day
 9:30 pm | Bob's Burgers | Sleeping with the Frenemy
10:00 pm | American Dad | Paranoid_Frandroid
10:30 pm | Family Guy | Fighting Irish
11:00 pm | Family Guy | Take My Wife
11:30 pm | Rick and Morty | Look Who's Purging Now
12:00 am | Tigtone | Tigtone and the Pilot
12:15 am | The Shivering Truth | Pilot
12:30 am | Venture Brothers  | The Saphrax Protocol
 1:00 am | DREAM CORP LLC | 88's Weekly
 1:15 am | DREAM CORP LLC | Staff Infection
 1:30 am | Bob's Burgers | Sleeping with the Frenemy
 2:00 am | Family Guy | Fighting Irish
 2:30 am | Family Guy | Take My Wife
 3:00 am | Rick and Morty | Look Who's Purging Now
 3:30 am | Tigtone | Tigtone and the Pilot
 3:45 am | The Shivering Truth | Pilot
 4:00 am | Venture Brothers  | The Saphrax Protocol
 4:30 am | DREAM CORP LLC | 88's Weekly
 4:45 am | DREAM CORP LLC | Staff Infection
 5:00 am | Harvey Birdman | Bannon Custody Battle
 5:15 am | Harvey Birdman | Very Personal Injury
 5:30 am | Home Movies | Curses