# 2019-01-21
 8:00 pm | Samurai Jack | XVI
 8:30 pm | American Dad | CIAPOW
 9:00 pm | American Dad | Scents and Sensei-bility
 9:30 pm | Bob's Burgers | The Kids Rob a Train
10:00 pm | Bob's Burgers | I Get Psy-Chic Out of You
10:30 pm | Family Guy | The Splendid Source
11:00 pm | Family Guy | Excellence in Broadcasting
11:30 pm | Rick and Morty | Look Who's Purging Now
12:00 am | Robot Chicken | Immortal
12:15 am | Robot Chicken | Born Again Virgin Christmas Special
12:30 am | Aqua Teen | Moonajuana
12:45 am | Squidbillies | Velvet Messiah
 1:00 am | Mr. Pickles | Cops and Robbers
 1:15 am | Superjail | Mayhem Donor
 1:30 am | American Dad | CIAPOW
 2:00 am | Family Guy | The Splendid Source
 2:30 am | Family Guy | Excellence in Broadcasting
 3:00 am | Rick and Morty | Look Who's Purging Now
 3:30 am | Robot Chicken | Immortal
 3:45 am | Robot Chicken | Born Again Virgin Christmas Special
 4:00 am | Greatest Event In Television History  | Simon and Simon
 4:15 am | Paid Programming: Icelandic Ultrablue | Icelandic Ultrablue
 4:30 am | Aqua Teen | Moonajuana
 4:45 am | Squidbillies | Velvet Messiah
 5:00 am | Bob's Burgers | The Kids Rob a Train
 5:30 am | Bob's Burgers | I Get Psy-Chic Out of You