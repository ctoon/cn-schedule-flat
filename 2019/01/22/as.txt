# 2019-01-22
 8:00 pm | Samurai Jack | XVII
 8:30 pm | American Dad | Big Stan on Campus
 9:00 pm | American Dad | Now and Gwen
 9:30 pm | Bob's Burgers | Equestranauts
10:00 pm | Bob's Burgers | Ambergris
10:30 pm | Family Guy | Blue Harvest Part 1 & 2
11:30 pm | Rick and Morty | The Wedding Squanchers
12:00 am | Robot Chicken | G.I. Gogurt
12:15 am | Robot Chicken | Link's Sausages
12:30 am | Aqua Teen | Bart Oats
12:45 am | Squidbillies | The Big E
 1:00 am | Mr. Pickles | Serial Killers
 1:15 am | Superjail | Lord Stingray Crash Party
 1:30 am | American Dad | Big Stan on Campus
 2:00 am | Family Guy | Blue Harvest Part 1 & 2
 3:00 am | Rick and Morty | The Wedding Squanchers
 3:30 am | Robot Chicken | G.I. Gogurt
 3:45 am | Robot Chicken | Link's Sausages
 4:00 am | Greatest Event In Television History  | Hart to Hart
 4:30 am | Aqua Teen | Bart Oats
 4:45 am | Squidbillies | The Big E
 5:00 am | Bob's Burgers | Equestranauts
 5:30 am | Bob's Burgers | Ambergris