# 2019-01-28
 6:00 am | Steven Universe | Space Race
 6:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 7:00 am | Total Drama Island | X-treme Torture
 7:30 am | Total Drama Island | Brunch of Disgustingness
 8:00 am | Amazing World of Gumball | The Boombox; The Castle
 8:30 am | Amazing World of Gumball | The Tape; The Sweaters
 9:00 am | Craig of the Creek | JPony
 9:15 am | Craig of the Creek | Jessica Goes to the Creek
 9:30 am | Teen Titans Go! | Video Game References; Cool School
10:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
10:30 am | Total DramaRama | Not Without My Fudgy Lumps
10:45 am | Total DramaRama | Sharing Is Caring
11:00 am | Amazing World of Gumball | The Points
11:15 am | Amazing World of Gumball | The Return
11:30 am | Amazing World of Gumball | The Bus
11:45 am | Amazing World of Gumball | The Nemesis
12:00 pm | Unikitty | Dinner Apart-y
12:15 pm | Unikitty | Action Forest
12:30 pm | Teen Titans Go! | Flashback
 1:00 pm | Teen Titans Go! | The Overbite
 1:15 pm | Teen Titans Go! | Shrimps and Prime Rib
 1:30 pm | We Bare Bears | Teacher's Pet
 1:45 pm | We Bare Bears | Coffee Cave
 2:00 pm | Teen Titans Go! | Cat's Fancy
 2:15 pm | Teen Titans Go! | Leg Day
 2:30 pm | Teen Titans Go! | The Dignity of Teeth
 2:45 pm | Teen Titans Go! | The Croissant
 3:00 pm | The LEGO Movie | 
 5:00 pm | Craig of the Creek | Bug City
 5:15 pm | Craig of the Creek | Jessica's Trail
 5:30 pm | Total DramaRama | Snots Landing
 5:45 pm | Total DramaRama | Germ Factory
 6:00 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 6:15 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 7:00 pm | We Bare Bears | Imaginary Friend
 7:15 pm | We Bare Bears | Braces
 7:30 pm | Amazing World of Gumball | The Blame
 7:45 pm | Amazing World of Gumball | The Check