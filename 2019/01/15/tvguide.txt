# 2019-01-15
 6:00 am | Steven Universe | Rose's Room
 6:15 am | Amazing World of Gumball | The Car
 6:30 am | Amazing World of Gumball | The Microwave; The Meddler
 7:00 am | Total Drama Island | Camp Castaways
 7:30 am | Total Drama Island | Are We There, Yeti?
 8:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Craig of the Creek | Dog Decider; Power Punchers
 9:30 am | Amazing World of Gumball | The Spoiler; The Society
10:00 am | Amazing World of Gumball | The Nobody; The Countdown
10:30 am | Total DramaRama | Snots Landing; Cone In 60 Seconds
11:00 am | Teen Titans Go! | Brain Percentages; Bottle Episode
11:30 am | Teen Titans Go! | Bl4z3
12:00 pm | Unikitty | Brawl Bot; Little Prince Puppycorn
12:30 pm | Amazing World of Gumball | The Candidate; The Fuss
 1:00 pm | Amazing World of Gumball | The Anybody; The News
 1:30 pm | We Bare Bears | Mom App; Tubin'
 2:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
 2:30 pm | Teen Titans Go! | Baby Hands; Love Monsters
 3:00 pm | Ben 10 | Double Hex
 3:15 pm | Amazing World of Gumball | The Knights
 3:30 pm | Amazing World of Gumball | The Fridge; The Remote
 4:00 pm | Total Drama Island | The Big Sleep
 4:30 pm | Total Drama Island | Dodgebrawl
 5:00 pm | Craig of the Creek | Lost In The Sewer; Secret Book Club
 5:30 pm | Total DramaRama | That's A Wrap; Venthalla
 6:00 pm | Teen Titans Go! | Obinray; Arms Race with Legs
 6:30 pm | Teen Titans Go! | Ones And Zeros; Wally T
 7:00 pm | We Bare Bears | Escandalosos; Hurricane Hal
 7:30 pm | Amazing World of Gumball | The Awareness; The Sorcerer