# 2019-01-10
 6:00 am | Steven Universe | Steven the Sword Fighter
 6:15 am | Amazing World of Gumball | The Goons
 6:30 am | Amazing World of Gumball | The Sock; The Genius
 7:00 am | Total Drama Island | Hide and Be Sneaky
 7:30 am | Total Drama Island | That's Off the Chain!
 8:00 am | Teen Titans Go! | Waffles; Opposites
 8:30 am | Teen Titans Go! | Birds; Be Mine
 9:00 am | Craig of the Creek | Escape From Family Dinner
 9:15 am | Craig of the Creek | The Climb
 9:30 am | Amazing World of Gumball | The Mirror; The Burden
10:00 am | Amazing World of Gumball | The Bros; The Man
10:30 am | Total DramaRama | Inglorious Toddlers
10:45 am | Total DramaRama | Sharing Is Caring
11:00 am | Teen Titans Go! | The Avogodo
11:15 am | Teen Titans Go! | Squash & Stretch
11:30 am | Teen Titans Go! | Hand Zombie
11:45 am | Teen Titans Go! | Two Parter: Part One
12:00 pm | Unikitty | Kickflip McPuppycorn
12:15 pm | Unikitty | Wishing Well
12:30 pm | Amazing World of Gumball | The Vegging
12:45 pm | Amazing World of Gumball | The Vision
 1:00 pm | Amazing World of Gumball | The Father
 1:15 pm | Amazing World of Gumball | The Choices
 1:30 pm | We Bare Bears | Best Bears
 1:45 pm | We Bare Bears | Dog Hotel
 2:00 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 2:30 pm | Teen Titans Go! | Dreams; Grandma Voice
 3:00 pm | Ben 10 | Vote Zombozo
 3:15 pm | Amazing World of Gumball | The Ape
 3:30 pm | Amazing World of Gumball | The Quest; The Spoon
 4:00 pm | Total Drama Island | Camp Castaways
 4:30 pm | Total Drama Island | Are We There Yeti?
 5:00 pm | Craig of the Creek | The Kid From 3030
 5:15 pm | Craig of the Creek | The Curse
 5:30 pm | Total DramaRama | Paint That a Shame
 5:45 pm | Total DramaRama | Germ Factory
 6:00 pm | Teen Titans Go! | Jinxed
 6:15 pm | Teen Titans Go! | Secret Garden
 6:30 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:45 pm | Teen Titans Go! | Hot Salad Water
 7:00 pm | We Bare Bears | Hot Sauce
 7:15 pm | We Bare Bears | Panda 2
 7:30 pm | Amazing World of Gumball | The Faith
 7:45 pm | Amazing World of Gumball | The Loophole