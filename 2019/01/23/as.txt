# 2019-01-23
 8:00 pm | Samurai Jack | XVIII
 8:30 pm | American Dad | LGBSteve
 9:00 pm | American Dad | Morning Mimosa
 9:30 pm | Bob's Burgers | The Kids Run Away
10:00 pm | Bob's Burgers | Gene It On
10:30 pm | Family Guy | Welcome Back, Carter
11:00 pm | Family Guy | Baby, You Knock Me Out
11:30 pm | Rick and Morty | The Rickshank Rickdemption
12:00 am | Robot Chicken | Secret of the Booze
12:15 am | Robot Chicken | Rebel Appliance
12:30 am | Aqua Teen | Antenna
12:45 am | Squidbillies | Keeping it in the Family Way
 1:00 am | Mr. Pickles | Sh?venpucker
 1:15 am | Superjail | Hotchick
 1:30 am | American Dad | LGBSteve
 2:00 am | Family Guy | Welcome Back, Carter
 2:30 am | Family Guy | Baby, You Knock Me Out
 3:00 am | Rick and Morty | The Rickshank Rickdemption
 3:30 am | Robot Chicken | Secret of the Booze
 3:45 am | Robot Chicken | Rebel Appliance
 4:00 am | Greatest Event In Television History  | Too Close for Comfort
 4:30 am | Aqua Teen | Antenna
 4:45 am | Squidbillies | Keeping it in the Family Way
 5:00 am | Bob's Burgers | The Kids Run Away
 5:30 am | Bob's Burgers | Gene It On