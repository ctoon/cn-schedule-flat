# 2019-01-07
 8:00 pm | Samurai Jack | VI
 8:30 pm | American Dad | Buck, Wild
 9:00 pm | American Dad | Crotchwalkers
 9:30 pm | Bob's Burgers | Two for Tina
10:00 pm | Bob's Burgers | It Snakes a Village
10:30 pm | Family Guy | Not All Dogs Go to Heaven
11:00 pm | Family Guy | Episode 420
11:30 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
12:00 am | Robot Chicken | Punctured Jugular
12:15 am | Robot Chicken | Poisoned by Relatives
12:30 am | Aqua Teen | Carl
12:45 am | Squidbillies | Young, Dumb and Full of Gums
 1:00 am | Mr. Pickles | The Cheeseman
 1:15 am | Superjail | Cold-Blooded
 1:30 am | American Dad | Buck, Wild
 2:00 am | Family Guy | Not All Dogs Go to Heaven
 2:30 am | Family Guy | Episode 420
 3:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 3:30 am | Robot Chicken | Punctured Jugular
 3:45 am | Robot Chicken | Poisoned by Relatives
 4:00 am | Chuck Deuce | Pilot
 4:15 am | Trap Universe | Pilot
 4:30 am | Aqua Teen | Carl
 4:45 am | Squidbillies | Young, Dumb and Full of Gums
 5:00 am | Bob's Burgers | Two for Tina
 5:30 am | Bob's Burgers | It Snakes a Village