# 2019-01-19
 6:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 6:30 am | Mega Man: Fully Charged | A Bot and His Dog
 6:45 am | Teen Titans Go! | Books
 7:00 am | Bakugan: Battle Planet | Rubbed the Wrong Way; Babysitting Bedlam
 7:30 am | Teen Titans Go! | Power Moves; Staring at the Future
 8:00 am | Teen Titans Go! | Two Parter: Part One
 8:15 am | Teen Titans Go! | Two Parter: Part Two
 8:30 am | Teen Titans Go! | Lication
 8:45 am | Teen Titans Go! | Garage Sale
 9:00 am | Total DramaRama | Cuttin' Corners
 9:15 am | Total DramaRama | Bananas & Cheese
 9:30 am | Total DramaRama | Aquarium for a Dream
 9:45 am | Total DramaRama | Hic Hic Hooray
10:00 am | Teen Titans Go! | BL4Z3
10:15 am | Teen Titans Go! | Squash & Stretch
10:30 am | Teen Titans Go! | Brain Percentages
10:45 am | Teen Titans Go! | BBBDay!
11:00 am | Teen Titans Go! | Hot Salad Water
11:15 am | Teen Titans Go! | Animals: It's Just a Word!
11:30 am | Teen Titans Go! | Jinxed
11:45 am | Teen Titans Go! | A Farce
12:00 pm | Ben 10 | Reststop Roustabout
12:15 pm | Teen Titans Go! | Orangins
12:30 pm | Teen Titans Go! | Employee of the Month Redux
12:45 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 1:00 pm | We Bare Bears | Escandalosos
 1:15 pm | We Bare Bears | Paperboyz
 1:30 pm | We Bare Bears | Rescue Ranger
 1:45 pm | We Bare Bears | Googs
 2:00 pm | Amazing World of Gumball | The Neighbor
 2:15 pm | Amazing World of Gumball | The Console
 2:30 pm | Amazing World of Gumball | The Pact
 2:45 pm | Amazing World of Gumball | The Box
 3:00 pm | Amazing World of Gumball | The Anybody
 3:15 pm | Amazing World of Gumball | The Matchmaker
 3:30 pm | Amazing World of Gumball | The Candidate
 3:45 pm | Amazing World of Gumball | The Vase
 4:00 pm | Total DramaRama | The Date
 4:15 pm | Total DramaRama | Having the Timeout of Our Lives
 4:30 pm | Total DramaRama | Free Chili
 4:45 pm | Total DramaRama | A Ninjustice to Harold
 5:00 pm | Amazing World of Gumball | The Name; The Extras
 5:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 6:00 pm | Amazing World of Gumball | The Coach; The Joy
 6:30 pm | Amazing World of Gumball | The Kids; The Fan
 7:00 pm | Total Drama Island | Hide and Be Sneaky
 7:30 pm | Total Drama Island | That's Off the Chain!