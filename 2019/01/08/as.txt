# 2019-01-08
 8:00 pm | Samurai Jack | VII
 8:30 pm | American Dad | Independent Movie
 9:00 pm | American Dad | Faking Bad
 9:30 pm | Bob's Burgers | Family Fracas
10:00 pm | Bob's Burgers | The Kids Run the Restaurant
10:30 pm | Family Guy | Stew-roids
11:00 pm | Family Guy | We Love You, Conrad
11:30 pm | Rick and Morty | Ricksy Business
12:00 am | Robot Chicken | Hurtled from a Helicopter into a Speeding Train
12:15 am | Robot Chicken | Disemboweled by an Orphan
12:30 am | Aqua Teen | Dirtfoot
12:45 am | Squidbillies | Holodeck Redneck
 1:00 am | Mr. Pickles | Dead Man's Curve
 1:15 am | Superjail | Negaton
 1:30 am | American Dad | Independent Movie
 2:00 am | Family Guy | Stew-roids
 2:30 am | Family Guy | We Love You, Conrad
 3:00 am | Rick and Morty | Ricksy Business
 3:30 am | Robot Chicken | Hurtled from a Helicopter into a Speeding Train
 3:45 am | Robot Chicken | Disemboweled by an Orphan
 4:00 am | Ole Bud's ANU Football Weekly | Pilot
 4:15 am | The Hindenburg Explodes! | Pilot
 4:30 am | Aqua Teen | Dirtfoot
 4:45 am | Squidbillies | Holodeck Redneck
 5:00 am | Bob's Burgers | Family Fracas
 5:30 am | Bob's Burgers | The Kids Run the Restaurant