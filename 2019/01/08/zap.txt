# 2019-01-08
 6:00 am | Steven Universe | Escapism
 6:15 am | Amazing World of Gumball | The Mystery
 6:30 am | Amazing World of Gumball | The Gi; The Kiss
 7:00 am | Total Drama Island | X-treme Torture
 7:30 am | Total Drama Island | Brunch of Disgustingness
 8:00 am | Teen Titans Go! | Power Moves; Staring at the Future
 8:30 am | Teen Titans Go! | No Power; Sidekick
 9:00 am | Craig of the Creek | Wildernessa
 9:15 am | Craig of the Creek | Doorway to Helen
 9:30 am | Amazing World of Gumball | The Boss; The Move
10:00 am | Amazing World of Gumball | The Law; The Allergy
10:30 am | Total DramaRama | Hic Hic Hooray
10:45 am | Total DramaRama | Aquarium for a Dream
11:00 am | Teen Titans Go! | BBRAE
11:30 am | Teen Titans Go! | A Farce
11:45 am | Teen Titans Go! | Grube's Fairytales
12:00 pm | Unikitty | Bugging Out
12:15 pm | Unikitty | Kitchen Chaos
12:30 pm | Amazing World of Gumball | The Sucker
12:45 pm | Amazing World of Gumball | The Lady
 1:00 pm | Amazing World of Gumball | The Cage
 1:15 pm | Amazing World of Gumball | The Stories
 1:30 pm | We Bare Bears | I, Butler
 1:45 pm | We Bare Bears | Crowbar Jones
 2:00 pm | Teen Titans Go! | Waffles; Opposites
 2:30 pm | Teen Titans Go! | Birds; Be Mine
 3:00 pm | Ben 10 | Safari Sa'Bad
 3:15 pm | Amazing World of Gumball | The Goons
 3:30 pm | Amazing World of Gumball | The Sock; The Genius
 4:00 pm | Total Drama Island | Hook, Line and Screamer
 4:30 pm | Total Drama Island | Wawanakwa Gone Wild
 5:00 pm | Craig of the Creek | The Climb
 5:15 pm | Craig of the Creek | Escape From Family Dinner
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Total DramaRama | Sharing Is Caring
 6:00 pm | Teen Titans Go! | The Avogodo
 6:15 pm | Teen Titans Go! | Squash & Stretch
 6:30 pm | Teen Titans Go! | Hand Zombie
 6:45 pm | Teen Titans Go! | Two Parter: Part One
 7:00 pm | We Bare Bears | Best Bears
 7:15 pm | We Bare Bears | Dog Hotel
 7:30 pm | Amazing World of Gumball | The Father
 7:45 pm | Amazing World of Gumball | The Choices