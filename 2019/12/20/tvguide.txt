# 2019-12-20
 6:00 am | Teen Titans Go! | The Scoop; TV Knight 3
 6:30 am | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
 7:00 am | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 7:30 am | Teen Titans Go! | Operation Tin Man; Nean
 8:00 am | Amazing World of Gumball | The Skull; Christmas
 8:30 am | Amazing World of Gumball | The Watch; The Bet
 9:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
 9:30 am | Amazing World of Gumball | The Code; The Test
10:00 am | Apple & Onion | Positive Attitude Theory; Apple's Short
10:30 am | Apple & Onion | Follow Your Dreams; Face Your Fears
11:00 am | Teen Titans Go! | Beast Boy's That's What's Up
12:00 pm | Dc Super Hero Girls | DramaQueen
12:15 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt
12:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 1:00 pm | Amazing World of Gumball | The Heist; The Weirdo
 1:30 pm | Amazing World of Gumball | The Line; The Nuisance
 2:00 pm | Amazing World of Gumball | The Father; The One
 2:30 pm | Amazing World of Gumball | The Promise; The Voice
 3:00 pm | Apple & Onion | Not Funny; Hotdog's Movie Premiere
 3:30 pm | Apple & Onion | 4 On 1; Baby Boi Tp
 4:00 pm | Teen Titans Go! | Genie President; Tall Titan Tales
 4:30 pm | Teen Titans Go! | I Used To Be A Peoples; The Metric System Vs. Freedom
 5:00 pm | Teen Titans Go! | The Chaff; Them Soviet Boys
 5:30 pm | Teen Titans Go! | Animals: It's Just a Word; Bbb Day!
 6:00 pm | Amazing World of Gumball | The Boombox; The Castle
 6:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 7:00 pm | Amazing World of Gumball | The Uncle; The Menu
 7:30 pm | Amazing World of Gumball | The Heist; The Weirdo
 8:00 pm | Apple & Onion | Apple's In Trouble; Burger's Trampoline
 8:30 pm | Apple & Onion | Falafel's Fun Day; Apple's In Charge