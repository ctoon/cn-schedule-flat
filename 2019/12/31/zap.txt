# 2019-12-31
 6:00 am | Amazing World of Gumball | The Mothers; The Password
 6:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 7:00 am | Mao Mao: Heroes of Pure Heart | Enemy Mime
 7:15 am | Mao Mao: Heroes of Pure Heart | Popularity Conquest
 7:30 am | Mao Mao: Heroes of Pure Heart | Breakup
 7:45 am | Mao Mao: Heroes of Pure Heart | Mao Mao's Bike Adventure
 8:00 am | Teen Titans Go! | The Fourth Wall
 8:15 am | Teen Titans Go! | Animals: It's Just a Word!
 8:30 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 8:45 am | Teen Titans Go! | Scary Figure Dance
 9:00 am | Teen Titans Go! | Grube's Fairytales
 9:15 am | Teen Titans Go! | BBBDay!
 9:30 am | Teen Titans Go! | A Farce
 9:45 am | Teen Titans Go! | Black Friday
10:00 am | Craig of the Creek | Jessica Goes to the Creek
10:15 am | Craig of the Creek | Bring Out Your Beast
10:30 am | Craig of the Creek | Too Many Treasures
10:45 am | Craig of the Creek | Lost in the Sewer
11:00 am | Amazing World of Gumball | The Gripes; The Vacation
11:30 am | Amazing World of Gumball | The Fraud; The Void
12:00 pm | Victor and Valentino | Cleaning Day
12:15 pm | Victor and Valentino | Los Cadejos
12:30 pm | Victor and Valentino | The Babysitter
12:45 pm | Victor and Valentino | Cuddle Monster
 1:00 pm | Amazing World of Gumball | The Society; The Spoiler
 1:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 2:00 pm | Amazing World of Gumball | The Downer; The Egg
 2:30 pm | Amazing World of Gumball | The Triangle; The Money
 3:00 pm | Teen Titans Go! | Cat's Fancy
 3:15 pm | Teen Titans Go! | The Spice Game
 3:30 pm | Teen Titans Go! | Leg Day
 3:45 pm | Teen Titans Go! | I'm the Sauce
 4:00 pm | Teen Titans Go! | The Dignity of Teeth
 4:15 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 4:30 pm | Teen Titans Go! | The Croissant
 4:45 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 5:00 pm | Total DramaRama | Duck Duck Juice
 5:15 pm | Total DramaRama | That's a Wrap
 5:30 pm | Total DramaRama | Cluckwork Orange
 5:45 pm | Total DramaRama | Tiger Fail
 6:00 pm | Teen Titans Go! | Squash & Stretch
 6:15 pm | Teen Titans Go! | Garage Sale
 6:30 pm | Teen Titans Go! | Two Parter: Part One
 6:45 pm | Teen Titans Go! | Two Parter: Part Two
 7:00 pm | Apple & Onion | 4 on 1
 7:15 pm | Apple & Onion | Bottle Catch
 7:30 pm | Apple & Onion | Apple's in Trouble
 7:45 pm | Apple & Onion | Pancake's Bus Tour