# 2019-12-04
 6:00 am | Amazing World of Gumball | The Cycle; The Stars
 6:30 am | Amazing World of Gumball | The Grades; The Diet
 7:00 am | Amazing World of Gumball | The Ex; The Sorcerer
 7:30 am | Amazing World of Gumball | The Move; The Boss
 8:00 am | Teen Titans Go! | Garage Sale; Secret Garden
 8:30 am | Teen Titans Go! | Pyramid Scheme; The Cruel Giggling Ghoul
 9:00 am | Teen Titans Go! | Finally a Lesson; Bottle Episode
 9:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
10:00 am | Craig Of The Creek | Vulture's Nest; Summer Wish
10:30 am | Craig Of The Creek | The Other Side
11:00 am | Amazing World of Gumball | The Law; The Allergy
11:30 am | Amazing World of Gumball | The Password; The Mothers
12:00 pm | DC Super Hero Girls | GothamCon
12:15 pm | Teen Titans Go! | Burger Vs. Burrito
12:30 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 1:00 pm | Teen Titans Go! | Who's Laughing Now; Booty Scooty
 1:30 pm | Teen Titans Go! | Oregon Trail; Snuggle Time
 2:00 pm | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 2:30 pm | Victor And Valentino | Fistfull Of Balloons; Love At First Bite
 3:00 pm | Amazing World of Gumball | The News; The List
 3:30 pm | Amazing World of Gumball | The Rival; The Lady
 4:00 pm | Amazing World of Gumball | The Vegging; The Sucker
 4:30 pm | Amazing World of Gumball | The Friend; The Saint
 5:00 pm | Total DramaRama | Apoca-lice Now; Know It All
 5:30 pm | Total DramaRama | Weiner Takes All; Snots Landing
 6:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 6:30 pm | Teen Titans Go! | Waffles; Opposites
 7:00 pm | Amazing World of Gumball | The Spoiler; The Society
 7:30 pm | Amazing World of Gumball | The Nobody; The Countdown
 8:00 pm | We Bare Bears | Lil' Squid; $100
 8:30 pm | We Bare Bears | Bear Squad; Grizzly The Movie