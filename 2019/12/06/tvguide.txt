# 2019-12-06
 6:00 am | Amazing World of Gumball | The Deal; The Worst
 6:30 am | Amazing World of Gumball | The Petals; The Puppets
 7:00 am | Amazing World of Gumball | The Line; The Nuisance
 7:30 am | Amazing World of Gumball | The Pizza; The Lie
 8:00 am | Teen Titans Go! | The Art of Ninjutsu; History Lesson
 8:30 am | Teen Titans Go! | Island Adventures
 9:30 am | Teen Titans Go! | Power Moves; Staring at the Future
10:00 am | Craig Of The Creek | The Future Is Cardboard; Kelsey The Elder
10:30 am | Craig Of The Creek | Lost In The Sewer; Return Of The Honeysuckle Rangers
11:00 am | Amazing World of Gumball | The Butterfly; The Question
11:30 am | Amazing World of Gumball | The Oracle; The Safety
12:00 pm | DC Super Hero Girls | Illusions Of Grandeur
12:15 pm | Teen Titans Go! | Caged Tiger
12:30 pm | Teen Titans Go! | No Power; Sidekick
 1:00 pm | Teen Titans Go! | Teen Titans Save Christmas; BBSFBDAY!
 1:30 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 2:00 pm | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 2:30 pm | Victor And Valentino | The Great Bongo Heist; Escape From Bebe Bay
 3:00 pm | Amazing World of Gumball | The Anybody; The Pact
 3:30 pm | Amazing World of Gumball | The Neighbor; The Shippening
 4:00 pm | Amazing World of Gumball | The Brain; The Parents
 4:30 pm | Amazing World of Gumball | The Crew; The Others
 5:00 pm | Total DramaRama | Simons Are Forever; Inglorious Toddlers
 5:30 pm | Total DramaRama | Mutt Ado About Owen; Bananas & Cheese
 6:00 pm | Teen Titans Go! | Dreams; Grandma Voice
 6:30 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:00 pm | Amazing World of Gumball | The Signature; The Check
 7:30 pm | Amazing World of Gumball | The Sale; The Pest
 8:00 pm | We Bare Bears | Teacher's Pet; Yuri And The Bear
 8:30 pm | We Bare Bears | Go Fish; Baby Bears On A Plane