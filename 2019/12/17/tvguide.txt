# 2019-12-17
 6:00 am | Amazing World Of Gumball | The Agent; The Factory
 6:30 am | Amazing World Of Gumball | The Web; The Mess
 7:00 am | Amazing World Of Gumball | The Heart; The Revolt
 7:30 am | Amazing World of Gumball | The Awkwardness; The Nest
 8:00 am | Teen Titans Go! | Career Day; Ones and Zeros
 8:30 am | Teen Titans Go! | The Academy; TV Knight 2
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
10:00 am | Craig Of The Creek | The Evolution Of Craig; Jextra Perrestrial
10:30 am | Craig Of The Creek | Stink Bomb; Secret Book Club
11:00 am | Amazing World of Gumball | The Points; The Bus
11:30 am | Amazing World of Gumball | The Night; The Misunderstandings
12:00 pm | Dc Super Hero Girls | BackInAFlash
12:15 pm | Teen Titans Go! | Road Trip
12:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 1:00 pm | Teen Titans Go! | The Scoop; TV Knight 3
 1:30 pm | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
 2:00 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 2:30 pm | Victor And Valentino | The Collector; The Dark Room
 3:00 pm | Amazing World of Gumball | The Skull; Christmas
 3:30 pm | Amazing World of Gumball | The Watch; The Bet
 4:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:30 pm | Amazing World of Gumball | The Code; The Test
 5:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Aquarium For A Dream
 5:30 pm | Total DramaRama | Snow Way Out; The Date
 6:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 6:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 7:00 pm | Amazing World of Gumball | The Slide; The Loophole
 7:30 pm | Amazing World of Gumball | The Copycats; The Potato
 8:00 pm | We Bare Bears | Dog Hotel; Emergency
 8:30 pm | We Bare Bears | Icy Nights Ii; The Road