# 2019-12-21
 6:00 am | Teen Titans Go! | Booty Scooty
 6:15 am | Teen Titans Go! | Cartoon Feud
 6:30 am | Transformers: Cyberverse | Ghost Town
 6:45 am | Teen Titans Go! | Think About Your Future
 7:00 am | Teen Titans Go! | Island Adventures
 8:00 am | Amazing World of Gumball | The Cage
 8:15 am | Amazing World of Gumball | The Cringe
 8:30 am | Amazing World of Gumball | The List
 8:45 am | Amazing World of Gumball | The News
 9:00 am | Amazing World of Gumball | The Lady
 9:15 am | Amazing World of Gumball | The Rival
 9:30 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 9:45 am | The Amazing World of Gumball: Darwin's Yearbook | Clayton
10:00 am | The Amazing World of Gumball: Darwin's Yearbook | Carrie
10:15 am | The Amazing World of Gumball: Darwin's Yearbook | Alan
10:30 am | Teen Titans Go! | Beast Boy on a Shelf
10:45 am | Teen Titans Go! | Christmas Crusaders
11:00 am | Teen Titans Go! | TTG v Santa
12:00 pm | Teen Titans Go! | Teen Titans Vroom
12:30 pm | LEGO Ninjago | An Unlikely Ally
12:45 pm | LEGO Ninjago | The Absolute Worst
 1:00 pm | Amazing World of Gumball | The Hero; The Photo
 1:30 pm | Amazing World of Gumball | The Promise; The Voice
 2:00 pm | Amazing World of Gumball | The Uploads
 2:15 pm | Amazing World of Gumball | The Petals
 2:30 pm | Amazing World of Gumball | The Romantic
 2:45 pm | Amazing World of Gumball | The Heist
 3:00 pm | Apple & Onion | Heatwave
 3:15 pm | Apple & Onion | Tips
 3:30 pm | Apple & Onion | Whale Spotting
 3:45 pm | Apple & Onion | The Perfect Team
 4:00 pm | Teen Titans Go! | Beast Boy on a Shelf
 4:15 pm | Teen Titans Go! | Christmas Crusaders
 4:30 pm | Teen Titans Go! | TTG v Santa
 5:30 pm | Teen Titans Go! | History Lesson
 5:45 pm | Teen Titans Go! | The Great Disaster
 6:00 pm | Amazing World of Gumball | The Comic
 6:15 pm | Amazing World of Gumball | The Uncle
 6:30 pm | Amazing World of Gumball | The Upgrade
 6:45 pm | Amazing World of Gumball | The Menu
 7:00 pm | Amazing World of Gumball | The Routine
 7:15 pm | Amazing World of Gumball | The Weirdo
 7:30 pm | Amazing World of Gumball | The Parking
 7:45 pm | Amazing World of Gumball | The Ex
 8:00 pm | Steven Universe: Future | Snow Day
 8:15 pm | Steven Universe: Future | Why So Blue?
 8:30 pm | Steven Universe: Future | Bluebird
 8:45 pm | Steven Universe: Future | A Very Special Episode