# 2019-12-01
 6:00 am | Amazing World of Gumball | The Petals
 6:15 am | Amazing World of Gumball | The Inquisition
 6:30 am | Amazing World of Gumball | The Deal
 6:45 am | Amazing World of Gumball | The BFFS
 7:00 am | Bakugan: Battle Planet | Girl Power; Return to the Fold
 7:30 am | Amazing World of Gumball | The Worst
 7:45 am | Amazing World of Gumball | The Decisions
 8:00 am | Amazing World of Gumball | The Best
 8:15 am | Amazing World of Gumball | The Revolt
 8:30 am | Amazing World of Gumball | The Singing
 8:45 am | Amazing World of Gumball | The Heart
 9:00 am | Amazing World of Gumball | The Heist
 9:15 am | Amazing World of Gumball | The Mess
 9:30 am | Amazing World of Gumball | The Weirdo
 9:45 am | Amazing World of Gumball | The Web
10:00 am | Amazing World of Gumball | The Uncle
10:15 am | Amazing World of Gumball | The Agent
10:30 am | Amazing World of Gumball | The Menu
10:45 am | Amazing World of Gumball | The Factory
11:00 am | Apple & Onion | Party Popper
11:15 am | Apple & Onion | Lil Noodle
11:30 am | Apple & Onion | Gyranoid of the Future
11:45 am | Apple & Onion | The Perfect Team
12:00 pm | Amazing World of Gumball | The Sorcerer
12:15 pm | Amazing World of Gumball | The Wish
12:30 pm | Amazing World of Gumball | The Ex
12:45 pm | Amazing World of Gumball | The Future
 1:00 pm | Amazing World of Gumball | The Diet
 1:15 pm | Amazing World of Gumball | The Silence
 1:30 pm | Amazing World of Gumball | The Grades
 1:45 pm | Amazing World of Gumball | The Master
 2:00 pm | Amazing World of Gumball | The Stars
 2:15 pm | Amazing World of Gumball | The Possession
 2:30 pm | Amazing World of Gumball | The Cycle
 2:45 pm | Amazing World of Gumball | The Buddy
 3:00 pm | Amazing World of Gumball | The Catfish
 3:15 pm | Amazing World of Gumball | The Drama
 3:30 pm | Amazing World of Gumball | The Ollie
 3:45 pm | Amazing World of Gumball | The Slip
 4:00 pm | Apple & Onion | Apple's Short
 4:15 pm | Apple & Onion | Face Your Fears
 4:30 pm | Apple & Onion | Fun Proof
 4:45 pm | Apple & Onion | Tips
 5:00 pm | Amazing World of Gumball | The Console
 5:15 pm | Amazing World of Gumball | The Awareness
 5:30 pm | Amazing World of Gumball | The Box
 5:45 pm | Amazing World of Gumball | The Stink
 6:00 pm | Amazing World of Gumball | The Matchmaker
 6:15 pm | Amazing World of Gumball | The Ad
 6:30 pm | Amazing World of Gumball | The Vase
 6:45 pm | Amazing World of Gumball | The Understanding
 7:00 pm | Amazing World of Gumball | The Potato
 7:15 pm | Amazing World of Gumball | The Spinoffs
 7:30 pm | Amazing World of Gumball | The Copycats
 7:45 pm | Amazing World of Gumball | The Potion
 8:00 pm | Amazing World of Gumball | The Outside
 8:15 pm | Amazing World of Gumball | The Intelligence
 8:30 pm | Amazing World of Gumball | The Fuss
 8:45 pm | Amazing World of Gumball | The Transformation