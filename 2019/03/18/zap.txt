# 2019-03-18
 6:00 am | Steven Universe | Cry for Help
 6:15 am | Teen Titans Go! | Terra-ized
 6:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
 7:00 am | Total Drama Action | Dial M for Merger
 7:30 am | Amazing World of Gumball | The Copycats
 7:45 am | Amazing World of Gumball | The Potato
 8:00 am | Amazing World of Gumball | The Schooling
 8:15 am | Amazing World of Gumball | The Intelligence
 8:30 am | Amazing World of Gumball | The Potion
 8:45 am | Amazing World of Gumball | The Spinoffs
 9:00 am | Total DramaRama | Ant We All Just Get Along
 9:15 am | Total DramaRama | A Licking Time Bomb
 9:30 am | Total DramaRama | Duck Duck Juice
 9:45 am | Total DramaRama | Hic Hic Hooray
10:00 am | Teen Titans Go! | Who's Laughing Now
10:15 am | Teen Titans Go! | Oregon Trail
10:30 am | Teen Titans Go! | Think About Your Future
10:45 am | Teen Titans Go! | Booty Scooty
11:00 am | Teen Titans Go! | Orangins
11:15 am | Teen Titans Go! | Jinxed
11:30 am | Teen Titans Go! | Brain Percentages
11:45 am | Teen Titans Go! | BL4Z3
12:00 pm | Amazing World of Gumball | The Disaster
12:15 pm | Amazing World of Gumball | The Re-Run
12:30 pm | Amazing World of Gumball | The Boss; The Move
 1:00 pm | Amazing World of Gumball | The Law; The Allergy
 1:30 pm | Total DramaRama | Cuttin' Corners
 1:45 pm | Total DramaRama | Snots Landing
 2:00 pm | Teen Titans Go! | BBSFBDAY
 2:15 pm | Teen Titans Go! | The Streak, Part 1
 2:30 pm | Teen Titans Go! | The Streak, Part 2
 2:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 3:00 pm | Ben 10 | King Koil
 3:15 pm | Amazing World of Gumball | The Pizza
 3:30 pm | Amazing World of Gumball | The Butterfly; The Question
 4:00 pm | Total Drama Action | Chefshank Redemption
 4:30 pm | Total Drama Action | One Flew Over the Cuckoos
 5:00 pm | Craig of the Creek | Memories of Bobby
 5:15 pm | Craig of the Creek | Jacob of the Creek
 5:30 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:45 pm | Total DramaRama | Gum and Gummer
 6:00 pm | Teen Titans Go! | The Chaff
 6:15 pm | Teen Titans Go! | The Metric System vs. Freedom
 6:30 pm | Teen Titans Go! | I Used to Be a Peoples
 6:45 pm | Teen Titans Go! | Tall Titan Tales
 7:00 pm | We Bare Bears | Fire!
 7:15 pm | We Bare Bears | Ranger Norm
 7:30 pm | Amazing World of Gumball | The Fuss
 7:45 pm | Amazing World of Gumball | The Outside