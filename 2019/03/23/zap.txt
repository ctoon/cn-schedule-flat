# 2019-03-23
 6:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 6:30 am | Teen Titans Go! | History Lesson
 6:45 am | Teen Titans Go! | The Art of Ninjutsu
 7:00 am | Bakugan: Battle Planet | Shun Shine; Ronin Son
 7:30 am | Teen Titans Go! | Think About Your Future
 7:45 am | Teen Titans Go! | Booty Scooty
 8:00 am | Teen Titans Go! | Who's Laughing Now
 8:15 am | Teen Titans Go! | Oregon Trail
 8:30 am | Teen Titans Go! | Snuggle Time
 8:45 am | Teen Titans Go! | Oh Yeah!
 9:00 am | Total DramaRama | Invasion of the Booger Snatchers
 9:15 am | Total DramaRama | Gum and Gummer
 9:30 am | Total DramaRama | Having the Timeout of Our Lives
 9:45 am | Total DramaRama | Snots Landing
10:00 am | Teen Titans Go! | Riding the Dragon
10:15 am | Teen Titans Go! | The Overbite
10:30 am | Teen Titans Go! | Shrimps and Prime Rib
10:45 am | Teen Titans Go! | Booby Trap House
11:00 am | Teen Titans Go! | The Night Begins to Shine Special
12:00 pm | Ben 10 | Charm School's Out
12:15 pm | Ben 10 | Billy Bajillions
12:30 pm | Teen Titans Go! | BBRAE
 1:00 pm | We Bare Bears | Fire!
 1:15 pm | We Bare Bears | Ranger Norm
 1:30 pm | Amazing World of Gumball | The Heist
 1:45 pm | Amazing World of Gumball | The Hug
 2:00 pm | Amazing World of Gumball | The Cringe
 2:15 pm | Amazing World of Gumball | The Misunderstandings
 2:30 pm | Amazing World of Gumball | The Father
 2:45 pm | Amazing World of Gumball | The Night
 3:00 pm | Amazing World of Gumball | The One
 3:15 pm | Amazing World of Gumball | The Bus
 3:30 pm | Amazing World of Gumball | The Vegging
 3:45 pm | Amazing World of Gumball | The Points
 4:00 pm | Total DramaRama | Invasion of the Booger Snatchers
 4:15 pm | Total DramaRama | Paint That a Shame
 4:30 pm | Total DramaRama | A Ninjustice to Harold
 4:45 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth
 5:15 pm | Total DramaRama | From Badge to Worse
 5:30 pm | Total DramaRama | Snow Way Out
 5:45 pm | Total DramaRama | A Licking Time Bomb
 6:00 pm | Total DramaRama | All Up in Your Drill
 6:15 pm | Total DramaRama | Know It All
 6:30 pm | Total DramaRama | Toys Will Be Toys
 6:45 pm | Total DramaRama | Inglorious Toddlers
 7:00 pm | Celebrity Manhunt's Total Drama Action Reunion Show | 