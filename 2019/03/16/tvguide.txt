# 2019-03-16
 6:00 am | Teen Titans Go! | Animals: It's Just a Word; Bbb Day!
 6:30 am | Teen Titans Go! | Garage Sale; Squash & Stretch
 7:00 am | Bakugan: Battle Planet | Subterranean Homesick Blues; Honey Struck
 7:30 am | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
 8:00 am | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and it's Bad; Pyramid Scheme
 8:30 am | Teen Titans Go! | Finally a Lesson; Bottle Episode
 9:00 am | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Gum And Gummer
 9:30 am | Total DramaRama | Inglorious Toddlers; All Up In Your Drill
10:00 am | Teen Titans Go! | Arms Race With Legs; Obinray
10:30 am | Teen Titans Go! | Wally T; Rad Dudes With Bad Tudes
11:00 am | Teen Titans Go! | Island Adventures
12:00 pm | Ben 10 | Bridge Out; Beach Heads
12:30 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 1:00 pm | We Bare Bears | Baby Orphan Ninja Bears; Wingmen
 1:30 pm | Amazing World of Gumball | The Cage; The Roots
 2:00 pm | Amazing World of Gumball | The Understanding; The Loophole
 2:30 pm | Amazing World of Gumball | The Transformation; The Slide
 3:00 pm | Amazing World of Gumball | The Spinoffs; The Test
 3:30 pm | Amazing World of Gumball | The Potion; The Code
 4:00 pm | Total DramaRama | Gum And Gummer; Toys Will Be Toys
 4:30 pm | Total DramaRama | Bananas & Cheese; From Badge To Worse
 5:00 pm | Amazing World of Gumball | The Intelligence; The Choices
 5:30 pm | Amazing World of Gumball | The Schooling; The Vision
 6:00 pm | Amazing World of Gumball | The Founder; The Boredom
 6:30 pm | Amazing World of Gumball | The Parents; The Guy
 7:00 pm | Total Drama Action | Ocean's Eight-or-Nine
 7:30 pm | Total Drama Action | One Million Bucks BC