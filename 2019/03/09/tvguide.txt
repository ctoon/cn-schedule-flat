# 2019-03-09
 6:00 am | Teen Titans Go! | Nostalgia Is Not A Substitute For An Actual Story; Business Ethics Wink Wink
 6:30 am | Teen Titans Go! | Genie President; Tall Titan Tales
 7:00 am | Bakugan: Battle Planet | Matter Of The Mind; Core Cell
 7:30 am | Teen Titans Go! | Leg Day; Cat's Fancy
 8:00 am | Teen Titans Go! | The Croissant; The Dignity of Teeth
 8:30 am | Teen Titans Go! | The Spice Game; I'm the Sauce
 9:00 am | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Snow Way Out
 9:30 am | Total DramaRama | Snots Landing; Free Chili
10:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
10:30 am | Teen Titans Go! | The Fourth Wall; 40%, 40%, 20%
11:00 am | Teen Titans Go! | Grube's Fairytales; A Farce
11:30 am | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
12:00 pm | Ben 10 | Show Don't Tell; Welcome To Zombozo-zone!
12:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 1:00 pm | We Bare Bears | Bubble; Imaginary Friend
 1:30 pm | Amazing World of Gumball | The Ad; The Copycats
 2:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 2:30 pm | Amazing World of Gumball | The Pony; The Storm
 3:00 pm | Amazing World of Gumball | The Authority; The Virus
 3:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:00 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Cluckwork Orange
 4:30 pm | Total DramaRama | Paint That A Shame; Duck Duck Juice
 5:00 pm | Amazing World of Gumball | The Watch; The Bet
 5:30 pm | Amazing World of Gumball | The Words; The Apology
 6:00 pm | Amazing World of Gumball | The Phone; The Job
 6:30 pm | Amazing World of Gumball | The Flower; The Banana
 7:00 pm | Total Drama Action | The Sand Witch Project
 7:30 pm | Total Drama Action | Masters of Disasters