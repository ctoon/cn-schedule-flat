# 2019-03-22
 6:00 am | Steven Universe | Friend Ship
 6:15 am | Teen Titans Go! | Legs
 6:30 am | Teen Titans Go! | Waffles; Opposites
 7:00 am | Total Drama Action | Get a Clue
 7:30 am | Amazing World of Gumball | The Uncle; The Menu
 8:00 am | Amazing World of Gumball | The Recipe; The Puppy
 8:30 am | Amazing World of Gumball | The Name; The Extras
 9:00 am | Total DramaRama | The Date; Not Without My Fudgy Lumps
 9:30 am | Total DramaRama | All Up In Your Drill; The Bad Guy Busters
10:00 am | Teen Titans Go! | Permanent Record; Movie Night
10:30 am | Teen Titans Go! | BBRAE
11:00 am | Teen Titans Go! | Beast Girl; BBCYFSHIPBDAY
11:30 am | Teen Titans Go! | Flashback
12:00 pm | Amazing World of Gumball | The Box; The Console
12:30 pm | Amazing World of Gumball | The Safety; The Oracle
 1:00 pm | Amazing World of Gumball | The Friend; The Saint
 1:30 pm | Total DramaRama | Cluckwork Orange; Bananas & Cheese
 2:00 pm | Teen Titans Go! | Lication; Hot Salad Water
 2:30 pm | Teen Titans Go! | Career Day; Ones and Zeros
 3:00 pm | Ben 10 | Super-villain Team-up
 3:15 pm | Amazing World of Gumball | The Nemesis
 3:30 pm | Amazing World of Gumball | The Others; The Crew
 4:00 pm | Total Drama Action | Million Dollar Babies
 4:30 pm | Total Drama Action | Dial M for Merger
 5:00 pm | Craig of the Creek | Vulture's Nest; Deep Creek Salvage
 5:30 pm | Total DramaRama | Cuttin' Corners; Snots Landing
 6:00 pm | Teen Titans Go! | Chicken In The Cradle; Tower Renovation
 6:30 pm | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
 7:00 pm | We Bare Bears | Tunnels; Baby Bears On A Plane
 7:30 pm | Amazing World of Gumball | The Best; The Heist