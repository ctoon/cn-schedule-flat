# 2019-03-17
 6:00 am | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
 6:30 am | Mega Man: Fully Charged | Too Much Is Never Enough
 6:45 am | Teen Titans Go! | Think About Your Future
 7:00 am | Teen Titans Go! | The Power Of Shrimps; The Real Orangins!
 7:30 am | Teen Titans Go! | Quantum Fun; The Fight
 8:00 am | Teen Titans Go! | The Croissant; The Dignity of Teeth
 8:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:00 am | Total DramaRama | Hic Hic Hooray; A Licking Time Bomb
 9:30 am | Total DramaRama | Gum And Gummer; Know It All
10:00 am | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
10:30 am | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Nostalgia Is Not A Substitute For An Actual Story
11:00 am | Teen Titans Go! | Genie President; Business Ethics Wink Wink
11:30 am | Teen Titans Go! | Leg Day; Cat's Fancy
12:00 pm | Teen Titans Go! | "Night Begins to Shine" Special
 1:00 pm | Craig of the Creek | Alone Quest; Dinner At The Creek
 1:30 pm | Amazing World of Gumball | The Parents; The Guy
 2:00 pm | Amazing World of Gumball | The Shippening; The Brain
 2:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 3:00 pm | Amazing World of Gumball | The Neighbor; The Stories
 3:30 pm | Amazing World of Gumball | The Pact; The Compilation
 4:00 pm | DC Super Hero Girls | #adventuresinbunnysitting
 4:15 pm | Teen Titans Go! | TTG v PPG
 4:30 pm | The Powerpuff Girls | Oh, Daisy!; Brain Freeze
 5:00 pm | Amazing World of Gumball | The Anybody; The Fury
 5:30 pm | Amazing World of Gumball | The Candidate; The Detective
 6:00 pm | Amazing World of Gumball | The Faith; The Blame
 6:30 pm | Amazing World of Gumball | The Cage; The Roots
 7:00 pm | Total Drama Action | Million Dollar Babies
 7:30 pm | Total Drama Action | Dial M for Merger