# 2019-03-05
 6:00 am | Steven Universe | Story for Steven
 6:15 am | Teen Titans Go! | BBBDay!
 6:30 am | Teen Titans Go! | Two Parter: Part One
 6:45 am | Teen Titans Go! | Two Parter: Part Two
 7:00 am | Total Drama Action | Chefshank Redemption
 7:30 am | Amazing World of Gumball | The Uploads
 7:45 am | Amazing World of Gumball | The Apprentice
 8:00 am | Amazing World of Gumball | The Ollie
 8:15 am | Amazing World of Gumball | The Catfish
 8:30 am | Amazing World of Gumball | The Cycle
 8:45 am | Amazing World of Gumball | The Stars
 9:00 am | Craig of the Creek | Creek Cart Racers
 9:15 am | Craig of the Creek | The Curse
 9:30 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 9:45 am | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
10:00 am | Teen Titans Go! | Business Ethics Wink Wink
10:15 am | Teen Titans Go! | Genie President
10:30 am | Total DramaRama | Hic Hic Hooray
10:45 am | Total DramaRama | The Date
11:00 am | Amazing World of Gumball | The Father
11:15 am | Amazing World of Gumball | The Cringe
11:30 am | Amazing World of Gumball | The Cage
11:45 am | Amazing World of Gumball | The Faith
12:00 pm | Unikitty | Perfect Moment
12:15 pm | Unikitty | Pool Duel
12:30 pm | Teen Titans Go! | The Cruel Giggling Ghoul
12:45 pm | Teen Titans Go! | Pyramid Scheme
 1:00 pm | Teen Titans Go! | Bottle Episode
 1:15 pm | Teen Titans Go! | Finally a Lesson
 1:30 pm | We Bare Bears | Pizza Band
 1:45 pm | We Bare Bears | Lil' Squid
 2:00 pm | Amazing World of Gumball | The Worst
 2:15 pm | Amazing World of Gumball | The Deal
 2:30 pm | Amazing World of Gumball | The Petals
 2:45 pm | Amazing World of Gumball | The Nuisance
 3:00 pm | Ben 10 | The Sound and the Furry
 3:15 pm | Amazing World of Gumball | The Intelligence
 3:30 pm | Amazing World of Gumball | The Potion
 3:45 pm | Amazing World of Gumball | The Spinoffs
 4:00 pm | Total Drama Action | Super Hero-Id
 4:30 pm | Total Drama Action | The Aftermath: O-wen or Lose
 5:00 pm | Craig of the Creek | Jextra Perrestrial
 5:15 pm | Craig of the Creek | Bring Out Your Beast
 5:30 pm | Total DramaRama | Inglorious Toddlers
 5:45 pm | Total DramaRama | Cuttin' Corners
 6:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:15 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 6:30 pm | Teen Titans Go! | History Lesson
 6:45 pm | Teen Titans Go! | The Art of Ninjutsu
 7:00 pm | We Bare Bears | Best Bears
 7:15 pm | We Bare Bears | Emergency
 7:30 pm | Amazing World of Gumball | The Hug
 7:45 pm | Amazing World of Gumball | The Wicked