# 2019-03-15
 6:00 am | Steven Universe | Chille Tid
 6:15 am | Teen Titans Go! | Parasite
 6:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
 7:00 am | Total Drama Action | Million Dollar Babies
 7:30 am | Amazing World of Gumball | The Code; The Test
 8:00 am | Amazing World of Gumball | The Shippening; The Brain
 8:30 am | Amazing World of Gumball | The Parents; The Founder
 9:00 am | Total DramaRama | Germ Factory; From Badge To Worse
 9:30 am | Total DramaRama | Cluckwork Orange; Bananas & Cheese
10:00 am | Teen Titans Go! | The Art of Ninjutsu; History Lesson
10:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
11:00 am | Teen Titans Go! | Titan Saving Time; Master Detective
11:30 am | Teen Titans Go! | Employee of the Month Redux; Hand Zombie
12:00 pm | Mega Man: Fully Charged | Too Much Is Never Enough
12:15 pm | Amazing World of Gumball | The Gripes
12:30 pm | Amazing World of Gumball | The Fraud; The Void
 1:00 pm | Amazing World of Gumball | The Detective; The Fury
 1:30 pm | Total DramaRama | Sharing Is Caring; Know It All
 2:00 pm | Teen Titans Go! | Booby Trap House; Shrimps and Prime Rib
 2:30 pm | Teen Titans Go! | Fish Water; TV Knight
 3:00 pm | Ben 10 | Double Hex
 3:15 pm | Amazing World of Gumball | The Mirror
 3:30 pm | Amazing World of Gumball | The Bros; The Man
 4:00 pm | Total Drama Action | 3:10 to Crazy Town
 4:30 pm | Total Drama Action | The Aftermath: Trent's Descent
 5:00 pm | Craig of the Creek | The Last Kid In The Creek; Too Many Treasures
 5:30 pm | Total DramaRama | The Bad Guy Busters; Toys Will Be Toys
 6:00 pm | Teen Titans Go! | BL4Z3,Hot Salad Water
 6:30 pm | Teen Titans Go! | Lication; Ones and Zeros
 7:00 pm | We Bare Bears | Adopted; Losing Ice
 7:30 pm | Amazing World of Gumball | The Slide; The Loophole