# 2019-03-01
 6:00 am | Steven Universe | Joy Ride
 6:15 am | Teen Titans Go! | I'm the Sauce
 6:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:45 am | Teen Titans Go! | Accept the Next Proposition You Hear
 7:00 am | Total Drama Action | 3:10 to Crazytown
 7:30 am | Amazing World of Gumball | The Gift
 7:45 am | Amazing World of Gumball | The Parking
 8:00 am | Amazing World of Gumball | The Copycats
 8:15 am | Amazing World of Gumball | The Potato
 8:30 am | Amazing World of Gumball | The Fuss
 8:45 am | Amazing World of Gumball | The Outside
 9:00 am | Craig of the Creek | Jextra Perrestrial
 9:15 am | Craig of the Creek | Bring Out Your Beast
 9:30 am | Teen Titans Go! | Quantum Fun
 9:45 am | Teen Titans Go! | The Fight
10:00 am | Teen Titans Go! | The Groover
10:15 am | Teen Titans Go! | BBRBDAY
10:30 am | Total DramaRama | Inglorious Toddlers
10:45 am | Total DramaRama | Cuttin' Corners
11:00 am | Amazing World of Gumball | The Line
11:15 am | Amazing World of Gumball | The List
11:30 am | Amazing World of Gumball | The News
11:45 am | Amazing World of Gumball | The Rival
12:00 pm | Unikitty | P.L.O.T. Device
12:15 pm | Unikitty | This Spells Disaster
12:30 pm | Teen Titans Go! | A Farce
12:45 pm | Teen Titans Go! | Animals: It's Just a Word!
 1:00 pm | Teen Titans Go! | Two Parter: Part One
 1:15 pm | Teen Titans Go! | Two Parter: Part Two
 1:30 pm | We Bare Bears | Wingmen
 1:45 pm | We Bare Bears | Family Troubles
 2:00 pm | Amazing World of Gumball | The Grades
 2:15 pm | Amazing World of Gumball | The Diet
 2:30 pm | Amazing World of Gumball | The Ex
 2:45 pm | Amazing World of Gumball | The Sorcerer
 3:00 pm | Ben 10 | That's the Stuff
 3:15 pm | Amazing World of Gumball | The Anybody
 3:30 pm | Amazing World of Gumball | The Pact
 3:45 pm | Amazing World of Gumball | The Neighbor
 4:00 pm | Total Drama Action | Oceans 8... or 9
 4:30 pm | Total Drama Action | Million Bucks BC
 5:00 pm | Craig of the Creek | Dinner at the Creek
 5:15 pm | Craig of the Creek | The Future Is Cardboard
 5:30 pm | Unikitty | Asteroid Blues
 5:45 pm | Unikitty | The Big Trip
 6:00 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:15 pm | Teen Titans Go! | Pyramid Scheme
 6:30 pm | Teen Titans Go! | Bottle Episode
 6:45 pm | Teen Titans Go! | Finally a Lesson
 7:00 pm | We Bare Bears | I, Butler
 7:15 pm | We Bare Bears | Panda's Sneeze
 7:30 pm | Total DramaRama | Paint That a Shame
 7:45 pm | Total DramaRama | Ant We All Just Get Along