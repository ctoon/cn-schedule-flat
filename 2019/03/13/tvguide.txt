# 2019-03-13
 6:00 am | Steven Universe | Keeping it Together
 6:15 am | Teen Titans Go! | Ghost Boy
 6:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 7:00 am | Total Drama Action | Ocean's Eight-or-Nine
 7:30 am | Amazing World of Gumball | The Compilation; The Stories
 8:00 am | Amazing World of Gumball | The Cringe; The Father
 8:30 am | Amazing World of Gumball | The Faith; The Cage
 9:00 am | Total DramaRama | The Bad Guy Busters; Toys Will Be Toys
 9:30 am | Total DramaRama | The Date; Not Without My Fudgy Lumps
10:00 am | Teen Titans Go! | Finally a Lesson; Bottle Episode
10:30 am | Teen Titans Go! | Pyramid Scheme; The Cruel Giggling Ghoul
11:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2; BBSFBDAY!
11:30 am | Teen Titans Go! | The Streak: Part 2; Inner Beauty of a Cactus
12:00 pm | Mega Man: Fully Charged | More More More
12:15 pm | Amazing World of Gumball | The Kids
12:30 pm | Amazing World of Gumball | The Coach; The Joy
 1:00 pm | Amazing World of Gumball | The Awkwardness; The Nest
 1:30 pm | Total DramaRama | Germ Factory; From Badge To Worse
 2:00 pm | Teen Titans Go! | Think About Your Future; Booty Scooty
 2:30 pm | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 3:00 pm | Ben 10 | Ben Again and Again
 3:15 pm | Amazing World of Gumball | The Boss
 3:30 pm | Amazing World of Gumball | The Allergy; The Law
 4:00 pm | Total Drama Action | Monster Cash
 4:30 pm | Total Drama Action | Alien Resurr-eggtion
 5:00 pm | Craig of the Creek | Big Pinchy; Sunday Clothes
 5:30 pm | Total DramaRama | Tiger Fail; Duck Duck Juice
 6:00 pm | Teen Titans Go! | Titan Saving Time; Master Detective
 6:30 pm | Teen Titans Go! | Employee of the Month Redux; Hand Zombie
 7:00 pm | We Bare Bears | Escandalosos; Bear Cleanse
 7:30 pm | Amazing World of Gumball | The Disaster; The Re-run