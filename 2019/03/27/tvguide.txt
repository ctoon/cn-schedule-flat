# 2019-03-27
 6:00 am | Steven Universe | Catch and Release
 6:15 am | Teen Titans Go! | Dreams
 6:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:00 am | Total Drama Action | 2008: A Space Owen
 7:30 am | Amazing World of Gumball | The Sucker; The Lady
 8:00 am | Amazing World of Gumball | The Password; The Mothers
 8:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 9:00 am | Total DramaRama | Duck Duck Juice; Hic Hic Hooray
 9:30 am | Total DramaRama | Ant We All Just Get Along; A Licking Time Bomb
10:00 am | Teen Titans Go! | Career Day; Ones and Zeros
10:30 am | Teen Titans Go! | Lication; Hot Salad Water
11:00 am | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
11:30 am | Teen Titans Go! | Quantum Fun; The Real Orangins!
12:00 pm | Amazing World of Gumball | The Best; The Heist
12:30 pm | Amazing World of Gumball | The Return; The Nemesis
 1:00 pm | Amazing World of Gumball | The Others; The Crew
 1:30 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; A Ninjustice To Harold
 2:00 pm | Teen Titans Go! | BBCYFSHIPBDAY, Beast Girl
 2:30 pm | Teen Titans Go! | Flashback
 3:00 pm | Ben 10 | Xingo's Back
 3:15 pm | Amazing World of Gumball | The Comic
 3:30 pm | Amazing World of Gumball | The Uploads; The Apprentice
 4:00 pm | Total Drama Action | Rock 'n Rule
 4:30 pm | Total Drama Action | Crouching Courtney, Hidden Owen
 5:00 pm | Craig of the Creek | Dinner At the Creek; The Brood
 5:30 pm | Total DramaRama | Free Chili; Inglorious Toddlers
 6:00 pm | Teen Titans Go! | How's This For A Special? Spaaaace
 6:30 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Bbrbday
 7:00 pm | We Bare Bears | Food Truck; Grizz Helps
 7:30 pm | Amazing World of Gumball | The One; The Vegging