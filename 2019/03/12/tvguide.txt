# 2019-03-12
 6:00 am | Steven Universe | Rising Tides, Crashing Skies
 6:15 am | Teen Titans Go! | Double Trouble
 6:30 am | Teen Titans Go! | Dude Relax!; Laundry Day
 7:30 am | Amazing World of Gumball | The Roots; The Blame
 8:00 am | Amazing World of Gumball | The Sucker; The Lady
 8:30 am | Amazing World of Gumball | The One; The Vegging
 9:00 am | Total DramaRama | That's A Wrap; Venthalla
 9:30 am | Total DramaRama | Aquarium For A Dream; Paint That A Shame
10:00 am | Teen Titans Go! | Garage Sale; Secret Garden
10:30 am | Teen Titans Go! | BBB Day!; Squash & Stretch
11:00 am | Teen Titans Go! | Booby Trap House; Shrimps and Prime Rib
11:30 am | Teen Titans Go! | Fish Water; TV Knight
12:00 pm | Mega Man: Fully Charged | All Play And No Work...
12:15 pm | Amazing World of Gumball | The Slip
12:30 pm | Amazing World of Gumball | The Drama; The Buddy
 1:00 pm | Amazing World of Gumball | The Advice; The Signal
 1:30 pm | Total DramaRama | All Up In Your Drill; Cone In 60 Seconds
 2:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 2:30 pm | Teen Titans Go! | The Art of Ninjutsu; History Lesson
 3:00 pm | Ben 10 | Vote Zombozo
 3:15 pm | Amazing World of Gumball | The Gripes
 3:30 pm | Amazing World of Gumball | The Fraud; The Void
 4:00 pm | Total Drama Action | Mutiny on the Soundstage
 4:30 pm | Total Drama Action | Who Wants To Pick A Millionaire?
 5:00 pm | Craig of the Creek | The Kid From 3030; Escape From Family Dinner
 5:30 pm | Total DramaRama | A Ninjustice To Harold; Cluckwork Orange
 6:00 pm | Teen Titans Go! | BBRAE
 6:30 pm | Teen Titans Go! | Permanent Record; Movie Night
 7:00 pm | We Bare Bears | Rescue Ranger; Charlie Ball
 7:30 pm | Amazing World of Gumball | The Detective; The Fury