# 2019-03-04
 6:00 am | Steven Universe | Say Uncle
 6:15 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 6:30 am | Teen Titans Go! | Grube's Fairytales
 6:45 am | Teen Titans Go! | A Farce
 7:00 am | Total Drama Action | The Aftermath 1: Trent's Descent
 7:30 am | Amazing World of Gumball | The Routine
 7:45 am | Amazing World of Gumball | The Upgrade
 8:00 am | Amazing World of Gumball | The Vase
 8:15 am | Amazing World of Gumball | The Matchmaker
 8:30 am | Amazing World of Gumball | The Box
 8:45 am | Amazing World of Gumball | The Console
 9:00 am | Craig of the Creek | Secret Book Club
 9:15 am | Craig of the Creek | Dog Decider
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
10:00 am | Teen Titans Go! | How's This for a Special? Spaaaace
10:30 am | Total DramaRama | Bananas & Cheese
10:45 am | Total DramaRama | Aquarium for a Dream
11:00 am | Amazing World of Gumball | The Lady
11:15 am | Amazing World of Gumball | The Sucker
11:30 am | Amazing World of Gumball | The Vegging
11:45 am | Amazing World of Gumball | The One
12:00 pm | Unikitty | Hawkodile Sensei
12:15 pm | Unikitty | Tooth Trouble
12:30 pm | Teen Titans Go! | BBBDay!
12:45 pm | Teen Titans Go! | Squash & Stretch
 1:00 pm | Teen Titans Go! | Garage Sale
 1:15 pm | Teen Titans Go! | Secret Garden
 1:30 pm | We Bare Bears | Adopted
 1:45 pm | We Bare Bears | I, Butler
 2:00 pm | Amazing World of Gumball | The Menu
 2:15 pm | Amazing World of Gumball | The Uncle
 2:30 pm | Amazing World of Gumball | The Heist
 2:45 pm | Amazing World of Gumball | The Best
 3:00 pm | Ben 10 | Reststop Roustabout
 3:15 pm | Amazing World of Gumball | The Shippening
 3:30 pm | Amazing World of Gumball | The Parents
 3:45 pm | Amazing World of Gumball | The Founder
 4:00 pm | Total Drama Action | Million Dollar Babies
 4:30 pm | Total Drama Action | Dial M for Merger
 5:00 pm | Craig of the Creek | The Mystery of the Timekeeper
 5:15 pm | Craig of the Creek | The Great Fossil Rush
 5:30 pm | Total DramaRama | All Up in Your Drill; Toys Will Be Toys
 5:45 pm | Total DramaRama | Snow Way Out
 6:00 pm | Teen Titans Go! | I Used to Be a Peoples
 6:15 pm | Teen Titans Go! | Tall Titan Tales
 6:30 pm | Teen Titans Go! | Genie President
 6:45 pm | Teen Titans Go! | Business Ethics Wink Wink
 7:00 pm | We Bare Bears | Bubble
 7:15 pm | We Bare Bears | The Gym
 7:30 pm | Amazing World of Gumball | The Comic
 7:45 pm | Amazing World of Gumball | The Romantic