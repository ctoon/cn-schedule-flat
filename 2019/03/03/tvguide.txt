# 2019-03-03
 6:00 am | Teen Titans Go! | Riding the Dragon; The Overbite
 6:30 am | Mega Man: Fully Charged | All Good In The Wood
 6:45 am | Teen Titans Go! | Shrimps and Prime Rib
 7:00 am | Teen Titans Go! | Booby Trap House; Fish Water
 7:30 am | Teen Titans Go! | TV Knight; BBSFBDAY!
 8:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 8:30 am | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 9:00 am | Total DramaRama | A Licking Time Bomb; Sharing Is Caring
 9:30 am | Total DramaRama | Snow Way Out; Cuttin' Corners
10:00 am | Teen Titans Go! | BBRAE
10:30 am | Teen Titans Go! | Permanent Record; Hand Zombie
11:00 am | Teen Titans Go! | Tall Titan Tales; Titan Saving Time
11:30 am | Teen Titans Go! | The Avogodo; Employee of the Month Redux
12:00 pm | Teen Titans Go! | Orangins; Jinxed
12:30 pm | Teen Titans Go! | Brain Percentages; Bl4z3
 1:00 pm | Craig of the Creek | The Great Fossil Rush; The Shortcut
 1:30 pm | Amazing World of Gumball | The Kids; The Fan
 2:00 pm | Amazing World of Gumball | The World; The Finale
 2:30 pm | Amazing World of Gumball | The Internet; The Plan
 3:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | Total DramaRama | Know It All; Aquarium For A Dream
 4:30 pm | Total DramaRama | Snow Way Out; The Date
 5:00 pm | Amazing World of Gumball | The Promise; The Voice
 5:30 pm | Amazing World of Gumball | The Limit; The Game
 6:00 pm | Amazing World of Gumball | The Tag; The Lesson
 6:30 pm | Amazing World of Gumball | The Hero; The Photo
 7:00 pm | Total Drama Action | The Chefshank Redemption
 7:30 pm | Total Drama Action | One Flu Over the Cuckoos