# 2019-03-28
 6:00 am | Steven Universe | When It Rains
 6:15 am | Teen Titans Go! | Mr. Butt
 6:30 am | Teen Titans Go! | Pirates; I See You
 7:00 am | Total Drama Action | Top Dog
 7:30 am | Amazing World of Gumball | The Father
 7:45 am | Amazing World of Gumball | The Cringe
 8:00 am | Amazing World of Gumball | The Mirror; The Burden
 8:30 am | Amazing World of Gumball | The Bros; The Man
 9:00 am | Total DramaRama | Venthalla
 9:15 am | Total DramaRama | Having the Timeout of Our Lives
 9:30 am | Total DramaRama | Know It All
 9:45 am | Total DramaRama | Sharing Is Caring
10:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:30 am | Teen Titans Go! | TV Knight 2
10:45 am | Teen Titans Go! | The Academy
11:00 am | Teen Titans Go! | The Fight
11:15 am | Teen Titans Go! | The Groover
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition (Alternate)
12:00 pm | Amazing World of Gumball | The Petals
12:15 pm | Amazing World of Gumball | The Nuisance
12:30 pm | Amazing World of Gumball | The Signature
12:45 pm | Amazing World of Gumball | The Check
 1:00 pm | Amazing World of Gumball | The Pest
 1:15 pm | Amazing World of Gumball | The Sale
 1:30 pm | Total DramaRama | Snow Way Out
 1:45 pm | Total DramaRama | Tiger Fail
 2:00 pm | Teen Titans Go! | Bro-Pocalypse
 2:15 pm | Teen Titans Go! | Mo' Money Mo' Problems
 2:30 pm | Teen Titans Go! | TV Knight 3
 2:45 pm | Teen Titans Go! | The Scoop
 3:00 pm | Ben 10 | Half-Sies
 3:15 pm | Amazing World of Gumball | The Wicked
 3:30 pm | Amazing World of Gumball | The Origins
 3:45 pm | Amazing World of Gumball | The Origins
 4:00 pm | Total Drama Action | 2008: A Space Owen
 4:30 pm | Total Drama Action | Top Dog
 5:00 pm | Craig of the Creek | The Future Is Cardboard
 5:15 pm | Craig of the Creek | The Takeout Mission
 5:30 pm | Total DramaRama | Cluckwork Orange
 5:45 pm | Total DramaRama | Bananas & Cheese
 6:00 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 6:15 pm | Teen Titans Go! | Business Ethics Wink Wink
 6:30 pm | Teen Titans Go! | Genie President
 6:45 pm | Teen Titans Go! | Tall Titan Tales
 7:00 pm | We Bare Bears | Everyday Bears
 7:15 pm | We Bare Bears | Grizzly the Movie
 7:30 pm | Amazing World of Gumball | The Cage
 7:45 pm | Amazing World of Gumball | The Faith