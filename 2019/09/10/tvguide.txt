# 2019-09-10
 6:00 am | Steven Universe | Future Vision
 6:15 am | Teen Titans Go! | La Larva Amor
 6:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 7:00 am | Teen Titans Go! | Video Game References; Cool School
 7:30 am | Amazing World of Gumball | The Possession; The Buddy
 8:00 am | Amazing World of Gumball | The Heist; The Weirdo
 8:30 am | Amazing World of Gumball | The Best; The Singing
 9:00 am | Tom & Jerry Show | Cattyshack; Drone Sweet Drone; Home Away From Home
 9:30 am | Tom & Jerry Show | From Riches To Rags; Chew Toy; Live And Let Diet
10:00 am | New Looney Tunes | Love it or Survivalist It; The Porklight; Best Bugs; Lewis & Pork
10:30 am | New Looney Tunes | Daffy the Stowaway; Superscooter 3000; Hoggin' the Road; Timmmbugs
11:00 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
11:30 am | Teen Titans Go! | BBRAE
12:00 pm | Ben 10 | The Chupaca-bro
12:15 pm | Teen Titans Go! | Leg Day
12:30 pm | Teen Titans Go! | The Croissant; The Dignity of Teeth
 1:00 pm | Victor And Valentino | The Babysitter; Know It All
 1:30 pm | Victor And Valentino | Cleaning Day; Churro Kings
 2:00 pm | Total DramaRama | Snots Landing; Duck Duck Juice
 2:30 pm | Total DramaRama | Paint That A Shame; Venthalla
 3:00 pm | Amazing World of Gumball | The Crew; The Others
 3:30 pm | Amazing World of Gumball | The Signature; The Check
 4:00 pm | Amazing World Of Gumball | The Decisions; The Bffs
 4:30 pm | Amazing World Of Gumball | The Inquisition; The Kids
 5:00 pm | Total DramaRama | The Price Of Advice; Hic Hic Hooray
 5:30 pm | Total DramaRama | Too Much Of A Good Thing; Having The Timeout Of Our Lives
 6:00 pm | Teen Titans Go! | Jinxed; Brain Percentages
 6:30 pm | Teen Titans Go! | Hot Salad Water; BL4Z3
 7:00 pm | Amazing World of Gumball | The Rival; The Lady
 7:30 pm | Amazing World of Gumball | The Vegging; The Sucker