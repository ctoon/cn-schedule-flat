# 2019-09-16
 6:00 am | Steven Universe | Marble Madness
 6:15 am | Teen Titans Go! | You're Fired
 6:30 am | Teen Titans Go! | The Spice Game; I'm the Sauce
 7:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
 7:30 am | Amazing World of Gumball | The Coach; The Joy
 8:00 am | Amazing World of Gumball | The Father; The One
 8:30 am | Amazing World of Gumball | The Cringe; The Cage
 9:00 am | Tom & Jerry Show | Tic-tyke-d'oh; Missing In Traction; Hair Today; Gone Tomorrow
 9:30 am | Tom & Jerry Show | Funnel Face; Wing Nuts; Cat-titude Adjustment
10:00 am | New Looney Tunes | Ear! We! Go!; Hare Band; Bugs In The Petting Zoo; Hawaiian Ice
10:30 am | New Looney Tunes | Quiet The Undertaking; Bugs Bunny?; Wet Feet; There's A Soccer Born Every Minute
11:00 am | Teen Titans Go! | "Night Begins to Shine" Special
12:00 pm | Ben 10 | Moor Fogg
12:15 pm | Teen Titans Go! | Garage Sale
12:30 pm | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
 1:00 pm | Victor And Valentino | Balloon Boys; The Boy Who Cried Lechuza
 1:30 pm | Victor And Valentino | Guillermo's Gathering; The Collector
 2:00 pm | Total DramaRama | There Are No Hoppy Endings; A Ninjustice To Harold
 2:30 pm | Total DramaRama | Tiger Fail; The Never Gwending Story
 3:00 pm | Amazing World of Gumball | The Traitor; The Origins
 3:30 pm | Amazing World of Gumball | The Origins Part 2; The Girlfriend
 4:00 pm | Amazing World of Gumball | The Law; The Allergy
 4:30 pm | Amazing World of Gumball | The Password; The Mothers
 5:00 pm | Total DramaRama | Ant We All Just Get Along; Stay Goth, Poodle Girl, Stay Goth
 5:30 pm | Total DramaRama | Snow Way Out; Sharing Is Caring
 6:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 pm | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
 7:00 pm | Amazing World of Gumball | The Founder; The Schooling
 7:30 pm | Amazing World of Gumball | The Intelligence; The Potion