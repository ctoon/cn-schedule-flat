# 2019-09-27
 6:00 am | Steven Universe | Story for Steven
 6:15 am | Teen Titans Go! | Burger Vs. Burrito
 6:30 am | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 7:00 am | Teen Titans Go! | The Overbite; Shrimps and Prime Rib
 7:30 am | Amazing World of Gumball | The Return; The Nemesis
 8:00 am | Amazing World Of Gumball | The Decisions; The Bffs
 8:30 am | Amazing World Of Gumball | The Inquisition; The Kids
 9:00 am | Scooby Doo & the Samurai Sword | 
10:30 am | Teen Titans Go! | The Art of Ninjutsu; Think About Your Future
11:00 am | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Bbrbday
11:30 am | Teen Titans Go! | Nostalgia Is Not A Substitute For An Actual Story; Business Ethics Wink Wink
12:00 pm | Ben 10 | The Claws Of The Cat
12:15 pm | Teen Titans Go! | Permanent Record
12:30 pm | Teen Titans Go! | Titan Saving Time; Master Detective
 1:00 pm | Victor And Valentino | Love At First Bite; Lonely Haunts Club
 1:30 pm | Victor And Valentino | Hurricane Chata
 2:00 pm | Total DramaRama | Mother Of All Cards; Bananas & Cheese
 2:30 pm | Total DramaRama | The Price Of Advice; Hic Hic Hooray
 3:00 pm | Amazing World of Gumball | The Box; The Console
 3:30 pm | Amazing World of Gumball | The Ollie; The Catfish
 4:00 pm | Amazing World of Gumball | The Comic; The Romantic
 4:30 pm | Amazing World of Gumball | The Uploads; The Apprentice
 5:00 pm | Total DramaRama | The Bad Guy Busters; Wristy Business
 5:30 pm | Total DramaRama | Invasion Of The Booger Snatchers; Cone In 60 Seconds
 6:00 pm | Teen Titans Go! | Stockton, Ca!; Don't Be An Icarus
 6:30 pm | Teen Titans Go! | What's Opera, Titans?; Forest Pirates
 7:00 pm | Amazing World of Gumball | The Fraud; The Void
 7:30 pm | Amazing World of Gumball | The Move; The Boss