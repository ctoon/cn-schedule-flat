# 2019-09-11
 6:00 am | Steven Universe | On the Run
 6:15 am | Teen Titans Go! | Hey Pizza!
 6:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:00 am | Teen Titans Go! | Operation Tin Man; Nean
 7:30 am | Amazing World of Gumball | The Future
 7:45 am | Amazing World of Gumball | The Wish
 8:00 am | Amazing World of Gumball | The Worst
 8:15 am | Amazing World of Gumball | The Deal
 8:30 am | Amazing World of Gumball | The Petals
 8:45 am | Amazing World of Gumball | The Puppets
 9:00 am | The Tom and Jerry Show | Auntie Social
 9:10 am | The Tom and Jerry Show | A Snootful
 9:20 am | The Tom and Jerry Show | Lame Duck
 9:30 am | The Tom and Jerry Show | It's All Relative
 9:40 am | The Tom and Jerry Show | Vegged Out
 9:50 am | The Tom and Jerry Show | Faux Hunt
10:00 am | New Looney Tunes | Easter Bunny Imposter; Easter Tweets
10:15 am | New Looney Tunes | Hoarder Up; Cougar, Cougar
10:30 am | New Looney Tunes | The Wedding Quacksher; The Food Notwork
10:45 am | New Looney Tunes | A Duck in the Aquarium; The BreezeHammer
11:00 am | Teen Titans Go! | Movie Night
11:15 am | Teen Titans Go! | Permanent Record
11:30 am | Teen Titans Go! | Titan Saving Time
11:45 am | Teen Titans Go! | Master Detective
12:00 pm | Ben 10 | Buggy Out
12:15 pm | Teen Titans Go! | I'm the Sauce
12:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
12:45 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 1:00 pm | Victor and Valentino | Legend of the Hidden Skate Park
 1:15 pm | Victor and Valentino | A New Don
 1:30 pm | Victor and Valentino | Welcome to the Underworld
 2:00 pm | Total DramaRama | Not Without My Fudgy Lumps
 2:15 pm | Total DramaRama | Soother or Later
 2:30 pm | Total DramaRama | Inglorious Toddlers
 2:45 pm | Total DramaRama | Duncan Disorderly
 3:00 pm | Amazing World of Gumball | The Pest
 3:15 pm | Amazing World of Gumball | The Sale
 3:30 pm | Amazing World of Gumball | The Gift
 3:45 pm | Amazing World of Gumball | The Parking
 4:00 pm | Amazing World of Gumball | The Coach; The Joy
 4:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 5:00 pm | Total DramaRama | A Ninjustice to Harold
 5:15 pm | Total DramaRama | There Are No Hoppy Endings
 5:30 pm | Total DramaRama | Tiger Fail
 5:45 pm | Total DramaRama | The Never Gwending Story
 6:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 7:00 pm | Amazing World of Gumball | The One
 7:15 pm | Amazing World of Gumball | The Father
 7:30 pm | Amazing World of Gumball | The Cringe
 7:45 pm | Amazing World of Gumball | The Cage