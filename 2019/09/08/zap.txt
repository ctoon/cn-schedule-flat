# 2019-09-08
 6:00 am | Teen Titans Go! | Video Game References; Cool School
 6:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 7:00 am | Bakugan: Battle Planet | Seek and Hide; Family First
 7:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 8:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
 8:30 am | Teen Titans Go! | Operation Tin Man; Nean
 9:00 am | Teen Titans Go! | Super Robin; Tower Power
 9:30 am | Teen Titans Go! | Campfire Stories; The Hive Five
10:00 am | Mao Mao: Heroes of Pure Heart | Bobo Chan
10:15 am | Mao Mao: Heroes of Pure Heart | He's the Sheriff
10:30 am | Mao Mao: Heroes of Pure Heart | All by Mao Self
10:45 am | Mao Mao: Heroes of Pure Heart | Thumb War
11:00 am | Teen Titans Go! | Parasite; Starliar
11:30 am | Teen Titans Go! | The Return of Slade; More of the Same
12:00 pm | Teen Titans Go! | Meatball Party; Staff Meeting
12:30 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 1:00 pm | Amazing World of Gumball | The Fuss
 1:15 pm | Amazing World of Gumball | The Outside
 1:30 pm | Amazing World of Gumball | The Fraud; The Void
 2:00 pm | Amazing World of Gumball | The Vase
 2:15 pm | Amazing World of Gumball | The Matchmaker
 2:30 pm | Amazing World of Gumball | The Boss; The Move
 3:00 pm | Craig of the Creek | Camper on the Run
 3:15 pm | Craig of the Creek | The Great Fossil Rush
 3:30 pm | Craig of the Creek | Cousin of the Creek
 3:45 pm | Craig of the Creek | Dibs Court
 4:00 pm | Total DramaRama | Duncan Disorderly
 4:15 pm | Total DramaRama | A Licking Time Bomb
 4:30 pm | Total DramaRama | Mother of All Cards
 4:45 pm | Total DramaRama | Know It All
 5:00 pm | Amazing World of Gumball | The Box
 5:15 pm | Amazing World of Gumball | The Console
 5:30 pm | Amazing World of Gumball | The Law; The Allergy
 6:00 pm | Amazing World of Gumball | The Ollie
 6:15 pm | Amazing World of Gumball | The Catfish
 6:30 pm | Amazing World of Gumball | The Mothers; The Password
 7:00 pm | Amazing World of Gumball | The Cycle
 7:15 pm | Amazing World of Gumball | The Stars
 7:30 pm | Amazing World of Gumball | The Procrastinators; The Shell