# 2019-09-07
 6:00 am | Steven Universe | Alone Together
 6:15 am | Teen Titans Go! | Yearbook Madness
 6:30 am | Transformers Cyberverse | Sea Of Tranquility
 6:45 am | Teen Titans Go! | Hose Water
 7:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 7:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 8:00 am | Teen Titans Go! | Driver's Ed; Dog Hand
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | Super Summer Hero Camp
10:00 am | Mao Mao, Heroes Of Pure Heart | Small
10:15 am | Mao Mao, Heroes Of Pure Heart | Legend Of Torbaclaun
10:30 am | Mao Mao, Heroes Of Pure Heart | Meet Tanya Keys
10:45 am | Mao Mao, Heroes Of Pure Heart | Weapon Of Choice
11:00 am | Teen Titans Go! | Double Trouble; The Date
11:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
12:00 pm | Ben 10 | Lickety Split
12:15 pm | Teen Titans Go! | Dude Relax!
12:30 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 1:00 pm | Amazing World of Gumball | The Kids; The Fan
 1:30 pm | Amazing World of Gumball | The Vision; The Choices
 2:00 pm | Amazing World of Gumball | The Coach; The Joy
 2:30 pm | Amazing World of Gumball | The Code; The Test
 3:00 pm | Ok K.o.! Let's Be Heroes | Dendy's Video Channel
 3:15 pm | Ok K.o.! Let's Be Heroes | Let's Fight To The End
 3:45 pm | Ok K.o.! Let's Be Heroes | Thank You For Watching The Show
 4:00 pm | Total DramaRama | Camping Is In Tents
 4:30 pm | Total DramaRama | Soother Or Later; From Badge To Worse
 5:00 pm | Amazing World Of Gumball | The Bffs; The Inquisition
 5:30 pm | Steven Universe: The Movie | 
 7:30 pm | DC Super Hero Girls | DCSuperHeroBoys