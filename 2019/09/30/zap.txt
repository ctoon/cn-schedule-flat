# 2019-09-30
 6:00 am | Teen Titans Go! | Permanent Record
 6:15 am | Teen Titans Go! | Titan Saving Time
 6:30 am | Teen Titans Go! | Master Detective
 6:45 am | Teen Titans Go! | Hand Zombie
 7:00 am | Teen Titans Go! | Employee of the Month Redux
 7:15 am | Teen Titans Go! | The Avogodo
 7:30 am | Amazing World of Gumball | The Third; The Debt
 8:00 am | Amazing World of Gumball | The Pressure; The Painting
 8:30 am | Amazing World of Gumball | The Responsible; The Dress
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
10:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
10:30 am | Teen Titans Go! | Flashback
11:00 am | Amazing World of Gumball | The Recipe; The Puppy
11:30 am | Amazing World of Gumball | The Name; The Extras
12:00 pm | Ben 10 | Moor Fogg
12:15 pm | Victor and Valentino | Folk Art Foes
12:30 pm | Victor and Valentino | Dead Ringer
12:45 pm | Victor and Valentino | Brotherly Love
 1:00 pm | Madagascar 3: Europe's Most Wanted | 
 3:00 pm | Amazing World of Gumball | The Stars
 3:15 pm | Amazing World of Gumball | The Grades
 3:30 pm | Amazing World of Gumball | The Diet
 3:45 pm | Amazing World of Gumball | The Ex
 4:00 pm | Amazing World of Gumball | The Sorcerer
 4:15 pm | Amazing World of Gumball | The Menu
 4:30 pm | Amazing World of Gumball | The Law; The Allergy
 5:00 pm | Teen Titans Go! | Kabooms
 5:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 6:00 pm | Teen Titans Go! | Two Parter: Part One
 6:15 pm | Teen Titans Go! | Two Parter: Part Two
 6:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 6:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 7:00 pm | Amazing World of Gumball | The Mothers; The Password
 7:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 8:00 pm | We Bare Bears | The Mummy's Curse
 8:15 pm | We Bare Bears | Baby Bears Can't Jump
 8:30 pm | We Bare Bears | Band of Outsiders
 8:45 pm | We Bare Bears | Bearz II Men