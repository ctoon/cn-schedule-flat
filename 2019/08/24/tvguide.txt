# 2019-08-24
 6:00 am | Steven Universe | House Guest
 6:15 am | Teen Titans Go! | The Groover
 6:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 7:00 am | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Bbrbday
 7:30 am | Teen Titans Go! | Friendship; Vegetables
 8:00 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
 8:30 am | Teen Titans Go! | I Used To Be A Peoples; The Metric System Vs. Freedom
 9:00 am | Craig Of The Creek | The Evolution Of Craig
 9:15 am | Craig Of The Creek | Stink Bomb
 9:30 am | Victor And Valentino | Balloon Boys
 9:45 am | Victor And Valentino | Guillermo's Gathering
10:00 am | Teen Titans Go! | The Viewers Decide; The Great Disaster
10:30 am | Teen Titans Go! | Nostalgia Is Not A Substitute For An Actual Story; Business Ethics Wink Wink
11:00 am | Teen Titans Go! | The Mask; Slumber Party
11:30 am | Teen Titans Go! | Genie President; Tall Titan Tales
12:00 pm | Ben 10 | Lickety Split; The Claws Of The Cat
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Amazing World Of Gumball | The Master; The Vase
 1:30 pm | Amazing World Of Gumball | The Silence; The Matchmaker
 2:00 pm | Amazing World Of Gumball | The Future; The Box
 2:30 pm | Amazing World Of Gumball | The Wish; The Console
 3:00 pm | Amazing World Of Gumball | The Possession; The Outside
 3:30 pm | Total DramaRama | Snow Way Out; Ant We All Just Get Along
 4:00 pm | Total DramaRama | All Up In Your Drill; Sharing Is Caring
 4:30 pm | Amazing World of Gumball | The Spoiler; The Society
 5:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 5:30 pm | Amazing World of Gumball | The Factory; The Ollie
 6:00 pm | Amazing World Of Gumball | The Agent; The Catfish
 6:30 pm | Amazing World Of Gumball | The Web; The Cycle
 7:00 pm | Steven Universe | Steven Universe Celebration 11