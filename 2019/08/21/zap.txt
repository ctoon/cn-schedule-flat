# 2019-08-21
 6:00 am | Steven Universe | An Indirect Kiss
 6:15 am | Teen Titans Go! | Sandwich Thief
 6:30 am | Teen Titans Go! | Friendship; Vegetables
 7:00 am | Teen Titans Go! | Throne of Bones
 7:15 am | Teen Titans Go! | Demon Prom
 7:30 am | Amazing World of Gumball | The Butterfly; The Question
 8:00 am | Amazing World of Gumball | The Oracle; The Safety
 8:30 am | Amazing World of Gumball | The Pact
 8:45 am | Amazing World of Gumball | The Neighbor
 9:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 9:30 am | Teen Titans Go! | Lil' Dimples
 9:45 am | Teen Titans Go! | Don't Be an Icarus
10:00 am | Teen Titans Go! | Stockton, CA!
10:15 am | Teen Titans Go! | What's Opera, Titans?
10:30 am | Amazing World of Gumball | The Shippening
10:45 am | Amazing World of Gumball | The Brain
11:00 am | Amazing World of Gumball | The Uploads
11:15 am | Amazing World of Gumball | The Apprentice
11:30 am | Amazing World of Gumball | The Hug
11:45 am | Amazing World of Gumball | The Wicked
12:00 pm | Ben 10 | LaGrange Muraille
12:15 pm | Teen Titans Go! | Animals: It's Just a Word!
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 1:30 pm | Teen Titans Go! | Bro-Pocalypse
 1:45 pm | Teen Titans Go! | Mo' Money Mo' Problems
 2:00 pm | Total DramaRama | Tiger Fail
 2:15 pm | Total DramaRama | A Ninjustice to Harold
 2:30 pm | Total DramaRama | Know It All
 2:45 pm | Total DramaRama | Duck Duck Juice
 3:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 3:30 pm | Amazing World of Gumball | The Downer; The Egg
 4:00 pm | Amazing World of Gumball | The Potion
 4:15 pm | Amazing World of Gumball | The Spinoffs
 4:30 pm | Amazing World of Gumball | The Transformation
 4:45 pm | Amazing World of Gumball | The Understanding
 5:00 pm | Total DramaRama | Camping Is in Tents
 5:30 pm | Victor and Valentino | Churro Kings
 5:45 pm | Victor and Valentino | Hurricane Chata
 6:00 pm | Teen Titans Go! | The Spice Game
 6:15 pm | Teen Titans Go! | I'm the Sauce
 6:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:45 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 7:00 pm | Amazing World of Gumball | The Advice
 7:15 pm | Amazing World of Gumball | The Signal
 7:30 pm | Amazing World of Gumball | The Parasite
 7:45 pm | Amazing World of Gumball | The Love