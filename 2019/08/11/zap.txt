# 2019-08-11
 6:00 am | Steven Universe | 
 7:00 am | Bakugan: Battle Planet | In the Wild; Call Me Old Brakken
 7:30 am | Teen Titans Go! | Career Day
 7:45 am | Teen Titans Go! | The Academy
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 8:30 am | Teen Titans Go! | Girls Night In
 9:00 am | Victor and Valentino | Aluxes
 9:15 am | Victor and Valentino | Go With the Flow
 9:30 am | Victor and Valentino | Love at First Bite
 9:45 am | Victor and Valentino | Fistfull of Balloons
10:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
10:30 am | Teen Titans Go! | Throne of Bones
10:45 am | Teen Titans Go! | Demon Prom
11:00 am | Teen Titans Go! | Legs; Breakfast Cheese
11:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
12:00 pm | Summer Camp Island | Susie's Fantastical Scavenger Hunt
12:15 pm | Summer Camp Island | Mop Forever
12:30 pm | Summer Camp Island | Pajamas Party
12:45 pm | Summer Camp Island | The Soundhouse
 1:00 pm | Total DramaRama | The Never Gwending Story
 1:15 pm | Total DramaRama | A Ninjustice to Harold
 1:30 pm | Total DramaRama | Melter Skelter
 1:45 pm | Total DramaRama | Tiger Fail
 2:00 pm | Amazing World of Gumball | The Oracle; The Safety
 2:30 pm | Amazing World of Gumball | The Friend; The Saint
 3:00 pm | Amazing World of Gumball | The Candidate
 3:15 pm | Amazing World of Gumball | The Misunderstandings
 3:30 pm | Amazing World of Gumball | The Anybody
 3:45 pm | Amazing World of Gumball | The Roots
 4:00 pm | Amazing World of Gumball | The Pact
 4:15 pm | Amazing World of Gumball | The Blame
 4:30 pm | OK K.O.! Let's Be Heroes | Big Reveal
 4:45 pm | OK K.O.! Let's Be Heroes | Radical Rescue
 5:00 pm | OK K.O.! Let's Be Heroes | Let's Meet Sonic
 5:15 pm | OK K.O.! Let's Be Heroes | Let's Take A Moment
 5:30 pm | Amazing World of Gumball | The Neighbor
 5:45 pm | Amazing World of Gumball | The Detective
 6:00 pm | Amazing World of Gumball | The Shippening
 6:15 pm | Amazing World of Gumball | The Fury
 6:30 pm | Amazing World of Gumball | The Brain
 6:45 pm | Amazing World of Gumball | The Compilation
 7:00 pm | Mao Mao: Heroes of Pure Heart | Popularity Conquest
 7:15 pm | Mao Mao: Heroes of Pure Heart | Sick Mao
 7:30 pm | Mao Mao: Heroes of Pure Heart | Mao Mao's Bike Adventure
 7:45 pm | Mao Mao: Heroes of Pure Heart | Breakup