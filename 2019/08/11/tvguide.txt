# 2019-08-11
 6:00 am | Steven Universe | Steven Universe Celebration 8
 7:00 am | Bakugan: Battle Planet | In The Wild; Call Me Old Brakken
 7:30 am | Teen Titans Go! | The Academy; Career Day
 8:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 8:30 am | Teen Titans Go! | Girls Night In
 9:00 am | Victor And Valentino | Aluxes; Go With The Flow
 9:30 am | Victor And Valentino | Love At First Bite; Fistfull Of Balloons
10:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
10:30 am | Teen Titans Go! | Demon Prom; Throne of Bones
11:00 am | Teen Titans Go! | Legs; Breakfast Cheese
11:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
12:00 pm | Summer Camp Island | Susie's Fantastical Scavenger Hunt; Mop Forever; Pajamas Party; The Soundhouse
 1:00 pm | Total DramaRama | The Never Gwending Story; A Ninjustice To Harold
 1:30 pm | Total DramaRama | Melter Skelter; Tiger Fail
 2:00 pm | Amazing World of Gumball | The Oracle; The Safety
 2:30 pm | Amazing World of Gumball | The Friend; The Saint
 3:00 pm | Amazing World of Gumball | The Candidate; The Misunderstandings
 3:30 pm | Amazing World of Gumball | The Anybody; The Roots
 4:00 pm | Amazing World of Gumball | The Pact; The Blame
 4:30 pm | Ok K.o.! Let's Be Heroes | Big Reveal
 4:45 pm | Ok K.o.! Let's Be Heroes | Radical Rescue
 5:00 pm | Ok K.o.! Let's Be Heroes | Let's Meet Sonic; Let's Take A Moment
 5:30 pm | Amazing World of Gumball | The Neighbor; The Detective
 6:00 pm | Amazing World of Gumball | The Shippening; The Fury
 6:30 pm | Amazing World of Gumball | The Brain; The Compilation
 7:00 pm | Mao Mao, Heroes Of Pure Heart | Popularity Conquest; Sick Mao
 7:30 pm | Mao Mao, Heroes Of Pure Heart | Mao Mao's Bike Adventure; Breakup