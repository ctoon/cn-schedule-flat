# 2019-08-30
 6:00 am | Steven Universe | Fusion Cuisine
 6:15 am | Teen Titans Go! | Pie Bros
 6:30 am | Teen Titans Go! | Brian; Nature
 7:00 am | Teen Titans Go! | Salty Codgers; Knowledge
 7:30 am | Amazing World of Gumball | The Recipe; The Puppy
 8:00 am | Amazing World of Gumball | The Name; The Extras
 8:30 am | Amazing World of Gumball | The Father; The One
 9:00 am | Teen Titans Go! | Pyramid Scheme; Bottle Episode
 9:30 am | Teen Titans Go! | Finally a Lesson; Arms Race With Legs
10:00 am | Teen Titans Go! | Ghost Boy; La Larva Amor
10:30 am | Amazing World of Gumball | The Cringe; The Cage
11:00 am | Amazing World of Gumball | The Disaster; The Re-run
11:30 am | Amazing World of Gumball | The Boredom; The Guy
12:00 pm | Ben 10 | Franken-Fight
12:15 pm | Teen Titans Go! | Boys vs. Girls
12:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 1:00 pm | Victor And Valentino | A New Don; Legend Of The Hidden Skate Park
 1:30 pm | Victor And Valentino | Welcome to the Underworld
 2:00 pm | Total DramaRama | Too Much Of A Good Thing; Having The Timeout Of Our Lives
 2:30 pm | Total DramaRama | There Are No Hoppy Endings; A Ninjustice To Harold
 3:00 pm | Amazing World of Gumball | The Password; The Mothers
 3:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 4:00 pm | Amazing World of Gumball | The Founder; The Schooling
 4:30 pm | Amazing World of Gumball | The Intelligence; The Potion
 5:00 pm | Total DramaRama | Germ Factory; Gum And Gummer
 5:30 pm | Total DramaRama | Stay Goth, Poodle Girl, Stay Goth; Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Island Adventures
 7:00 pm | Amazing World of Gumball | The Fuss; The Outside
 7:30 pm | Amazing World of Gumball | The Vase; The Matchmaker