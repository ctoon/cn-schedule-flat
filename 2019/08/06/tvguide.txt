# 2019-08-06
 6:00 am | Steven Universe | Arcade Mania
 6:15 am | Teen Titans Go! | Parasite
 6:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
 7:00 am | Teen Titans Go! | Operation Tin Man; Nean
 7:30 am | Amazing World of Gumball | The Pony; The Storm
 8:00 am | Amazing World of Gumball | The Dream; The Sidekick
 8:30 am | Amazing World of Gumball | The Vision; The Choices
 9:00 am | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 9:30 am | Teen Titans Go! | Beast Girl; Bro-Pocalypse
10:00 am | Teen Titans Go! | Mo' Money Mo' Problems; TV Knight 3
10:30 am | Amazing World of Gumball | The Code; The Test
11:00 am | Amazing World of Gumball | The Shippening; The Brain
11:30 am | Amazing World of Gumball | The Parents; The Founder
12:00 pm | Ben 10 | The Chupaca-bro
12:15 pm | Teen Titans Go! | Leg Day
12:30 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 1:00 pm | Teen Titans Go! | Books; Lazy Sunday
 1:30 pm | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 2:00 pm | Teen Titans Go! | The Overbite; Shrimps and Prime Rib
 2:30 pm | Total DramaRama | The Never Gwending Story; That's A Wrap
 3:00 pm | Amazing World of Gumball | The Limit; The Game
 3:30 pm | Amazing World of Gumball | The Promise; The Voice
 4:00 pm | Amazing World of Gumball | The Fuss; The Outside
 4:30 pm | Amazing World of Gumball | The Vase; The Matchmaker
 5:00 pm | Total DramaRama | Wristy Business; Cone In 60 Seconds
 5:30 pm | Victor And Valentino | It Grows; Cleaning Day
 6:00 pm | Teen Titans Go! | What's Opera, Titans?; Forest Pirates
 6:30 pm | Teen Titans Go! | The Bergerac; Snot And Tears
 7:00 pm | Amazing World Of Gumball | The Web; The Bffs
 7:30 pm | Infinity Train | The Corgi Car
 7:45 pm | Infinity Train | The Crystal Car