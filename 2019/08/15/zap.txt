# 2019-08-15
 6:00 am | Steven Universe | Rose's Room
 6:15 am | Teen Titans Go! | Uncle Jokes
 6:30 am | Teen Titans Go! | Dreams; Grandma Voice
 7:00 am | Teen Titans Go! | Orangins
 7:15 am | Teen Titans Go! | Jinxed
 7:30 am | Amazing World of Gumball | The Gripes; The Vacation
 8:00 am | Amazing World of Gumball | The Fraud; The Void
 8:30 am | Amazing World of Gumball | The Petals
 8:45 am | Amazing World of Gumball | The Nuisance
 9:00 am | Teen Titans Go! | Brain Percentages
 9:15 am | Teen Titans Go! | BL4Z3
 9:30 am | Teen Titans Go! | The Real Orangins!
 9:45 am | Teen Titans Go! | Quantum Fun
10:00 am | Teen Titans Go! | The Fight
10:15 am | Teen Titans Go! | The Groover
10:30 am | Amazing World of Gumball | The Line
10:45 am | Amazing World of Gumball | The List
11:00 am | Amazing World of Gumball | The Silence
11:15 am | Amazing World of Gumball | The Future
11:30 am | Amazing World of Gumball | The Return
11:45 am | Amazing World of Gumball | The Nemesis
12:00 pm | Ben 10 | Don't Touch
12:15 pm | Teen Titans Go! | The Fourth Wall
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Teen Titans Go! | Brian; Nature
 1:30 pm | Teen Titans Go! | Hot Salad Water
 1:45 pm | Teen Titans Go! | Lication
 2:00 pm | Teen Titans Go! | Ones and Zeroes
 2:15 pm | Teen Titans Go! | Career Day
 2:30 pm | Total DramaRama | All Up in Your Drill
 2:45 pm | Total DramaRama | Aquarium for a Dream
 3:00 pm | Amazing World of Gumball | The Mothers; The Password
 3:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 4:00 pm | Amazing World of Gumball | The Vegging
 4:15 pm | Amazing World of Gumball | The One
 4:30 pm | Amazing World of Gumball | The Father
 4:45 pm | Amazing World of Gumball | The Cringe
 5:00 pm | Total DramaRama | From Badge to Worse
 5:15 pm | Total DramaRama | Free Chili
 5:30 pm | Victor and Valentino | Love at First Bite
 5:45 pm | Victor and Valentino | The Dark Room
 6:00 pm | Teen Titans Go! | Genie President
 6:15 pm | Teen Titans Go! | Tall Titan Tales
 6:30 pm | Teen Titans Go! | I Used to Be a Peoples
 6:45 pm | Teen Titans Go! | The Metric System vs. Freedom
 7:00 pm | Amazing World of Gumball | The Pest
 7:15 pm | Amazing World of Gumball | The Sale
 7:30 pm | Amazing World of Gumball | The Gift
 7:45 pm | Amazing World of Gumball | The Parking