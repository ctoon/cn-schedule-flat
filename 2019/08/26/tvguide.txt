# 2019-08-26
 6:00 am | Steven Universe | Space Race
 6:15 am | Teen Titans Go! | Little Elvis
 6:30 am | Teen Titans Go! | Birds; Be Mine
 7:00 am | Teen Titans Go! | Brain Food; In and Out
 7:30 am | Amazing World of Gumball | The Silence; The Future
 8:00 am | Amazing World of Gumball | The Factory; The Wish
 8:30 am | Amazing World of Gumball | The Heist; The Weirdo
 9:00 am | Teen Titans Go! | The Croissant; The Dignity of Teeth
 9:30 am | Teen Titans Go! | The Spice Game; I'm the Sauce
10:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
10:30 am | Amazing World of Gumball | The Best; The Singing
11:00 am | Amazing World of Gumball | The Advice; The Signal
11:30 am | Amazing World of Gumball | The Parasite; The Love
12:00 pm | Ben 10 | Bridge Out
12:15 pm | Teen Titans Go! | Mr. Butt
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Victor And Valentino | Balloon Boys; The Boy Who Cried Lechuza
 1:30 pm | Victor And Valentino | Guillermo's Gathering; The Collector
 2:00 pm | Total DramaRama | Know It All; Cluckwork Orange
 2:30 pm | Total DramaRama | Snots Landing; Duck Duck Juice
 3:00 pm | Amazing World of Gumball | The Kids; The Fan
 3:30 pm | Amazing World of Gumball | The Coach; The Joy
 4:00 pm | Amazing World of Gumball | The Rival; The Lady
 4:30 pm | Amazing World of Gumball | The Vegging; The Sucker
 5:00 pm | Total DramaRama | Mother Of All Cards; Bananas & Cheese
 5:30 pm | Total DramaRama | The Price Of Advice; Hic Hic Hooray
 6:00 pm | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 6:30 pm | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
 7:00 pm | Amazing World of Gumball | The Detective; The Fury
 7:30 pm | Amazing World of Gumball | The Compilation; The Stories