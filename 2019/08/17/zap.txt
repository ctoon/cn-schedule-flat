# 2019-08-17
 6:00 am | Steven Universe | Joking Victim
 6:15 am | Teen Titans Go! | Bro-Pocalypse
 6:30 am | Teen Titans Go! | Brain Food; In and Out
 7:00 am | Teen Titans Go! | Flashback
 7:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:00 am | Teen Titans Go! | Kabooms
 8:30 am | Teen Titans Go! | The Great Disaster
 8:45 am | Teen Titans Go! | Girls Night In Part 2
 9:00 am | Craig of the Creek | Stink Bomb
 9:15 am | Craig of the Creek | Camper on the Run
 9:30 am | Victor and Valentino | Guillermo's Gathering
 9:45 am | Victor and Valentino | Aluxes
10:00 am | Teen Titans Go! | The Scoop
10:15 am | Teen Titans Go! | Chicken in the Cradle
10:30 am | Teen Titans Go! | Mo' Money Mo' Problems
10:45 am | Teen Titans Go! | TV Knight 3
11:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
11:30 am | Teen Titans Go! | Girls Night In
11:45 am | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
12:00 pm | Ben 10 | Big Ben 10
12:15 pm | Ben 10 | LaGrange Muraille
12:30 pm | Teen Titans Go! | Dreams; Grandma Voice
 1:00 pm | Amazing World of Gumball | The Founder
 1:15 pm | Amazing World of Gumball | The Disaster
 1:30 pm | Amazing World of Gumball | The Re-Run
 1:45 pm | Amazing World of Gumball | The Schooling
 2:00 pm | Amazing World of Gumball | The Intelligence
 2:15 pm | Amazing World of Gumball | The Guy
 2:30 pm | Amazing World of Gumball | The Potion
 2:45 pm | Amazing World of Gumball | The Boredom
 3:00 pm | Amazing World of Gumball | The Parents
 3:15 pm | Amazing World of Gumball | The Stories
 3:30 pm | Total DramaRama | That's a Wrap
 3:45 pm | Total DramaRama | Wristy Business
 4:00 pm | Total DramaRama | The Bad Guy Busters
 4:15 pm | Total DramaRama | Invasion of the Booger Snatchers
 4:30 pm | Amazing World of Gumball | The Downer
 4:45 pm | Amazing World of Gumball | The Egg
 5:00 pm | Amazing World of Gumball | The Triangle
 5:15 pm | Amazing World of Gumball | The Money
 5:30 pm | Amazing World of Gumball | The Spinoffs
 5:45 pm | Amazing World of Gumball | The Vision
 6:00 pm | Amazing World of Gumball | The Transformation
 6:15 pm | Amazing World of Gumball | The Choices
 6:30 pm | Amazing World of Gumball | The Understanding
 6:45 pm | Amazing World of Gumball | The Code
 7:00 pm | Steven Universe | 