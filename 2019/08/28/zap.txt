# 2019-08-28
 6:00 am | Steven Universe | Island Adventure
 6:15 am | Teen Titans Go! | What's Opera, Titans?
 6:30 am | Teen Titans Go! | Dreams; Grandma Voice
 7:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:30 am | Amazing World of Gumball | The Revolt
 7:45 am | Amazing World of Gumball | The Decisions
 8:00 am | Amazing World of Gumball | The BFFS
 8:15 am | Amazing World of Gumball | The Inquisition
 8:30 am | Amazing World of Gumball | The Nuisance
 8:45 am | Amazing World of Gumball | The Line
 9:00 am | Teen Titans Go! | Grube's Fairytales
 9:15 am | Teen Titans Go! | A Farce
 9:30 am | Teen Titans Go! | Animals: It's Just a Word!
 9:45 am | Teen Titans Go! | BBBDay!
10:00 am | Teen Titans Go! | Double Trouble; The Date
10:30 am | Amazing World of Gumball | The List
10:45 am | Amazing World of Gumball | The News
11:00 am | Amazing World of Gumball | The Misunderstandings
11:15 am | Amazing World of Gumball | The Night
11:30 am | Amazing World of Gumball | The Roots
11:45 am | Amazing World of Gumball | The Blame
12:00 pm | Ben 10 | Charm School's Out
12:15 pm | Teen Titans Go! | Love Monsters
12:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 1:00 pm | Victor and Valentino | Love at First Bite
 1:15 pm | Victor and Valentino | Lonely Haunts Club
 1:30 pm | Victor and Valentino | Fistfull of Balloons
 1:45 pm | Victor and Valentino | Hurricane Chata
 2:00 pm | Total DramaRama | Soother or Later
 2:15 pm | Total DramaRama | Not Without My Fudgy Lumps
 2:30 pm | Total DramaRama | Duncan Disorderly
 2:45 pm | Total DramaRama | Inglorious Toddlers
 3:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:30 pm | Amazing World of Gumball | The Fraud; The Void
 4:00 pm | Amazing World of Gumball | The Faith
 4:15 pm | Amazing World of Gumball | The Candidate
 4:30 pm | Amazing World of Gumball | The Anybody
 4:45 pm | Amazing World of Gumball | The Pact
 5:00 pm | Total DramaRama | The Never Gwending Story
 5:15 pm | Total DramaRama | Tiger Fail
 5:30 pm | Total DramaRama | Melter Skelter
 5:45 pm | Total DramaRama | That's a Wrap
 6:00 pm | Teen Titans Go! | Obinray
 6:15 pm | Teen Titans Go! | Wally T
 6:30 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 6:45 pm | Teen Titans Go! | History Lesson
 7:00 pm | Amazing World of Gumball | The Vision
 7:15 pm | Amazing World of Gumball | The Choices
 7:30 pm | Amazing World of Gumball | The Code
 7:45 pm | Amazing World of Gumball | The Test