# 2019-08-23
 6:00 am | Steven Universe | Ocean Gem
 6:15 am | Teen Titans Go! | Road Trip
 6:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
 7:00 am | Teen Titans Go! | Bro-Pocalypse
 7:15 am | Teen Titans Go! | Mo' Money Mo' Problems
 7:30 am | Amazing World of Gumball | The Countdown; The Nobody
 8:00 am | Amazing World of Gumball | The Downer; The Egg
 8:30 am | Amazing World of Gumball | The Potion
 8:45 am | Amazing World of Gumball | The Spinoffs
 9:00 am | Teen Titans Go! | TV Knight 3
 9:15 am | Teen Titans Go! | The Scoop
 9:30 am | Teen Titans Go! | The Spice Game
 9:45 am | Teen Titans Go! | I'm the Sauce
10:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
10:15 am | Teen Titans Go! | Accept the Next Proposition You Hear
10:30 am | Amazing World of Gumball | The Transformation
10:45 am | Amazing World of Gumball | The Understanding
11:00 am | Amazing World of Gumball | The Advice
11:15 am | Amazing World of Gumball | The Signal
11:30 am | Amazing World of Gumball | The Parasite
11:45 am | Amazing World of Gumball | The Love
12:00 pm | Ben 10 | The Claws of the Cat
12:15 pm | Teen Titans Go! | Secret Garden
12:30 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 1:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 1:30 pm | Teen Titans Go! | My Name Is Jose
 1:45 pm | Teen Titans Go! | The Power of Shrimps
 2:00 pm | Total DramaRama | Bananas & Cheese
 2:15 pm | Total DramaRama | Inglorious Toddlers
 2:30 pm | Total DramaRama | Camping Is in Tents
 3:00 pm | Amazing World of Gumball | The Crew
 3:15 pm | Amazing World of Gumball | The Others
 3:30 pm | Amazing World of Gumball | The Signature
 3:45 pm | Amazing World of Gumball | The Check
 4:00 pm | Amazing World of Gumball | The Drama
 4:15 pm | Amazing World of Gumball | The Buddy
 4:30 pm | Amazing World of Gumball | The Possession
 4:45 pm | Amazing World of Gumball | The Master
 5:00 pm | Total DramaRama | Not Without My Fudgy Lumps
 5:15 pm | Total DramaRama | Duncan Disorderly
 5:30 pm | Victor and Valentino | Welcome to the Underworld
 6:00 pm | Teen Titans Go! | Animals: It's Just a Word!
 6:15 pm | Teen Titans Go! | BBBDay!
 6:30 pm | Teen Titans Go! | Squash & Stretch
 6:45 pm | Teen Titans Go! | Garage Sale
 7:00 pm | Amazing World of Gumball | The Night
 7:15 pm | Amazing World of Gumball | The Misunderstandings
 7:30 pm | Amazing World of Gumball | The Roots
 7:45 pm | Amazing World of Gumball | The Blame