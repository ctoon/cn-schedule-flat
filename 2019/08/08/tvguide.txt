# 2019-08-08
 6:00 am | Steven Universe | So Many Birthdays
 6:15 am | Teen Titans Go! | Colors of Raven
 6:30 am | Teen Titans Go! | Books; Lazy Sunday
 7:00 am | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 7:30 am | Amazing World of Gumball | The Limit; The Game
 8:00 am | Amazing World of Gumball | The Promise; The Voice
 8:30 am | Amazing World of Gumball | The Fuss; The Outside
 9:00 am | Teen Titans Go! | The Overbite; Shrimps and Prime Rib
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
10:00 am | Teen Titans Go! | How's This For A Special? Spaaaace
10:30 am | Amazing World of Gumball | The Vase; The Matchmaker
11:00 am | Amazing World Of Gumball | The Web; The Bffs
11:30 am | Amazing World of Gumball | The Potion; The Spinoffs
12:00 pm | Ben 10 | Introducing Kevin 11
12:15 pm | Teen Titans Go! | Croissant
12:30 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 1:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
 1:30 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 2:00 pm | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 2:30 pm | Total DramaRama | Wristy Business; Cone In 60 Seconds
 3:00 pm | Amazing World of Gumball | The Internet; The Plan
 3:30 pm | Amazing World of Gumball | The World; The Finale
 4:00 pm | Amazing World of Gumball | The Cycle; The Stars
 4:30 pm | Amazing World of Gumball | The Grades; The Diet
 5:00 pm | Total DramaRama | Gum And Gummer; Ant We All Just Get Along
 5:30 pm | Victor And Valentino | Cuddle Monster; Chata's Quinta Quinceaera
 6:00 pm | Teen Titans Go! | Had To Be There; Strength Of A Grown Man
 6:30 pm | Teen Titans Go! | Girls Night In
 7:00 pm | Amazing World Of Gumball | The Factory; The Revolt
 7:30 pm | Infinity Train | The Chrome Car
 7:45 pm | Infinity Train | The Ball Pit Car