# 2019-08-04
 6:00 am | Steven Universe | 
 7:00 am | Bakugan: Battle Planet | The Forbidden Isle; New World Orders
 7:30 am | Teen Titans Go! | The Streak, Part 1
 7:45 am | Teen Titans Go! | The Streak, Part 2
 8:00 am | Teen Titans Go! | Movie Night
 8:15 am | Teen Titans Go! | Permanent Record
 8:30 am | Teen Titans Go! | Strength of a Grown Man
 8:45 am | Teen Titans Go! | Had to Be There
 9:00 am | Victor and Valentino | Go With the Flow
 9:15 am | Victor and Valentino | Love at First Bite
 9:30 am | Victor and Valentino | Fistfull of Balloons
 9:45 am | Victor and Valentino | Know It All
10:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
10:30 am | Teen Titans Go! | BBRAE
11:00 am | Teen Titans Go! | Super Robin; Tower Power
11:30 am | Teen Titans Go! | Titan Saving Time
11:45 am | Teen Titans Go! | Master Detective
12:00 pm | Summer Camp Island | I Heart Heartforde
12:15 pm | Summer Camp Island | Space Invasion
12:30 pm | Summer Camp Island | Mom Soon
12:45 pm | Summer Camp Island | Sneeze Guard
 1:00 pm | Total DramaRama | Mother of All Cards
 1:15 pm | Total DramaRama | Inglorious Toddlers
 1:30 pm | Total DramaRama | The Price of Advice
 1:45 pm | Total DramaRama | Bananas & Cheese
 2:00 pm | Amazing World of Gumball | The Pizza; The Lie
 2:30 pm | Amazing World of Gumball | The Butterfly; The Question
 3:00 pm | Amazing World of Gumball | The Puppets
 3:15 pm | Amazing World of Gumball | The Wicked
 3:30 pm | Amazing World of Gumball | The Nuisance
 3:45 pm | Amazing World of Gumball | The Traitor
 4:00 pm | Amazing World of Gumball | The Origins
 4:15 pm | Amazing World of Gumball | The Origins
 4:30 pm | OK K.O.! Let's Be Heroes | Deep Space Vacation
 4:45 pm | OK K.O.! Let's Be Heroes | Let's Meet Sonic
 5:00 pm | OK K.O.! Let's Be Heroes | Planet X
 5:15 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 5:30 pm | Amazing World of Gumball | The List
 5:45 pm | Amazing World of Gumball | The Line
 6:00 pm | Amazing World of Gumball | The News
 6:15 pm | Amazing World of Gumball | The Girlfriend
 6:30 pm | Amazing World of Gumball | The Rival
 6:45 pm | Amazing World of Gumball | The Advice
 7:00 pm | Mao Mao: Heroes of Pure Heart | Outfoxed
 7:15 pm | Mao Mao: Heroes of Pure Heart | Bao Bao's Revenge
 7:30 pm | Mao Mao: Heroes of Pure Heart | I Love You Mao Mao
 7:45 pm | Mao Mao: Heroes of Pure Heart | The Perfect Adventure