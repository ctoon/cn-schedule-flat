# 2019-04-19
 6:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
 6:30 am | Teen Titans Go! | Books; Lazy Sunday
 7:00 am | Total Drama: Revenge of the Island | Truth or Laser Shark
 7:30 am | Amazing World of Gumball | The Slide
 7:45 am | Amazing World of Gumball | The Loophole
 8:00 am | Amazing World of Gumball | The Parents
 8:15 am | Amazing World of Gumball | The Founder
 8:30 am | Amazing World of Gumball | The Schooling
 8:45 am | Amazing World of Gumball | The Intelligence
 9:00 am | Total DramaRama | Cone in 60 Seconds
 9:15 am | Total DramaRama | All Up in Your Drill
 9:30 am | Total DramaRama | Germ Factory
 9:45 am | Total DramaRama | Toys Will Be Toys
10:00 am | Teen Titans Go! | The Streak, Part 1
10:15 am | Teen Titans Go! | The Streak, Part 2
10:30 am | Teen Titans Go! | Inner Beauty of a Cactus
10:45 am | Teen Titans Go! | Movie Night
11:00 am | Teen Titans Go! | Caged Tiger; Nose Mouth
11:30 am | Teen Titans Go! | Legs; Breakfast Cheese
12:00 pm | Amazing World of Gumball | The Pony; The Storm
12:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 1:00 pm | Amazing World of Gumball | The Ollie
 1:15 pm | Amazing World of Gumball | The Catfish
 1:30 pm | Total DramaRama | Hic Hic Hooray
 1:45 pm | Total DramaRama | Melter Skelter
 2:00 pm | Teen Titans Go! | Orangins
 2:15 pm | Teen Titans Go! | Jinxed
 2:30 pm | Teen Titans Go! | Brain Percentages
 2:45 pm | Teen Titans Go! | BL4Z3
 3:00 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 3:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition (Alternate)
 4:00 pm | Amazing World of Gumball | The Knights; The Colossus
 4:30 pm | Amazing World of Gumball | The Possession
 4:45 pm | Amazing World of Gumball | The Potion
 5:00 pm | Total DramaRama | There Are No Hoppy Endings
 5:15 pm | Total DramaRama | The Never Gwending Story
 5:30 pm | Teen Titans Go! | Booty Eggs
 5:45 pm | Teen Titans Go! | Easter Creeps
 6:00 pm | Ninjago: Masters of Spinjitzu: March of the Oni | March of the Oni