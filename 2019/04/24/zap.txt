# 2019-04-24
 6:00 am | Steven Universe | Steven's Birthday
 6:15 am | Teen Titans Go! | Caged Tiger
 6:30 am | Teen Titans Go! | Legs; Breakfast Cheese
 7:00 am | Victor and Valentino | The Babysitter
 7:15 am | Victor and Valentino | Hurricane Chata
 7:30 am | Amazing World of Gumball | The Cycle
 7:45 am | Amazing World of Gumball | The Stars
 8:00 am | Amazing World of Gumball | The Drama
 8:15 am | Amazing World of Gumball | The Buddy
 8:30 am | Amazing World of Gumball | The Knights; The Colossus
 9:00 am | Total DramaRama | The Date
 9:15 am | Total DramaRama | Paint That a Shame
 9:30 am | Total DramaRama | Free Chili
 9:45 am | Total DramaRama | Not Without My Fudgy Lumps
10:00 am | Teen Titans Go! | Orangins
10:15 am | Teen Titans Go! | Jinxed
10:30 am | Teen Titans Go! | Brain Percentages
10:45 am | Teen Titans Go! | BL4Z3
11:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
11:30 am | Teen Titans Go! | Dreams; Grandma Voice
12:00 pm | Amazing World of Gumball | The Boombox; The Castle
12:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:00 pm | Amazing World of Gumball | The Worst
 1:15 pm | Amazing World of Gumball | The Deal
 1:30 pm | Total DramaRama | Cone in 60 Seconds
 1:45 pm | Total DramaRama | All Up in Your Drill
 2:00 pm | Teen Titans Go! | Throne of Bones
 2:15 pm | Teen Titans Go! | Demon Prom
 2:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 3:00 pm | Ben 10 | Bridge Out
 3:15 pm | Craig of the Creek | Ace of Squares
 3:30 pm | Craig of the Creek | JPony
 3:45 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 4:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:30 pm | Amazing World of Gumball | The Kids; The Fan
 5:00 pm | Total DramaRama | Wristy Business
 5:15 pm | Total DramaRama | A Ninjustice to Harold
 5:30 pm | Victor and Valentino | Legend of the Hidden Skate Park
 5:45 pm | Victor and Valentino | Cleaning Day
 6:00 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 6:30 pm | Teen Titans Go! | Brian; Nature
 7:00 pm | Amazing World of Gumball | The Coach; The Joy
 7:30 pm | We Bare Bears | Baby Bears Can't Jump
 7:45 pm | We Bare Bears | Go Fish