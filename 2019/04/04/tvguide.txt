# 2019-04-04
 6:00 am | Teen Titans Go! | Island Adventures
 7:00 am | Total Drama Island | Dodgebrawl
 7:30 am | Amazing World of Gumball | The Others; The Crew
 8:00 am | Amazing World of Gumball | The Slide; The Loophole
 8:30 am | Amazing World of Gumball | The Copycats; The Potato
 9:00 am | Total DramaRama | Melter Skelter; Having The Timeout Of Our Lives
 9:30 am | Total DramaRama | Wristy Business; A Ninjustice To Harold
10:00 am | Teen Titans Go! | Leg Day; Cat's Fancy
10:30 am | Teen Titans Go! | The Croissant; The Dignity of Teeth
11:00 am | Teen Titans Go! | Little Elvis; them Soviet Boys
11:30 am | Teen Titans Go! | The Chaff; The Metric System Vs. Freedom
12:00 pm | Amazing World of Gumball | The Neighbor; The Pact
12:30 pm | Amazing World of Gumball | The Anybody; The Candidate
 1:00 pm | Amazing World of Gumball | The Comic; The Romantic
 1:30 pm | Total DramaRama | Snots Landing; Aquarium For A Dream
 2:00 pm | Teen Titans Go! | Animals: It's Just a Word; Bbb Day!
 2:30 pm | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
 3:00 pm | Ben 10 | Poles Apart
 3:15 pm | Amazing World of Gumball | The Uploads
 3:30 pm | Amazing World of Gumball | The Cycle; The Stars
 4:00 pm | Amazing World of Gumball | The Grades; The Diet
 4:30 pm | Amazing World of Gumball | The Transformation; The Understanding
 5:00 pm | Craig of the Creek | Memories Of Bobby; Dibs Court
 5:30 pm | Total DramaRama | Toys Will Be Toys; Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 6:30 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Bbrbday
 7:00 pm | We Bare Bears | Cousin Jon; Ranger Norm
 7:30 pm | Amazing World of Gumball | The Stink; The Ad