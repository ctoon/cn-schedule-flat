# 2019-04-14
 6:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 am | Mega Man: Fully Charged | Enemy of My Enemy
 6:45 am | Teen Titans Go! | Friendship; Vegetables
 7:00 am | Teen Titans Go! | The Mask; Slumber Party
 7:30 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
 8:00 am | Teen Titans Go! | Two Parter: Part One
 8:15 am | Teen Titans Go! | Two Parter: Part Two
 8:30 am | Victor and Valentino | Folk Art Foes
 8:45 am | Victor and Valentino | Dead Ringer
 9:00 am | Total DramaRama | From Badge to Worse
 9:15 am | Total DramaRama | The Bad Guy Busters
 9:30 am | Teen Titans Go! | Flashback
10:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
10:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
11:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Summer Camp Island | Chocolate Money Badgers
12:15 pm | Summer Camp Island | Saxophone Come Home
12:30 pm | Craig of the Creek | Jacob of the Creek
12:45 pm | Craig of the Creek | Return of the Honeysuckle Rangers
 1:00 pm | Amazing World of Gumball | The Love
 1:15 pm | Amazing World of Gumball | The Founder
 1:30 pm | Amazing World of Gumball | The Awkwardness
 1:45 pm | Amazing World of Gumball | The Schooling
 2:00 pm | Amazing World of Gumball | The Nest
 2:15 pm | Amazing World of Gumball | The Intelligence
 2:30 pm | Amazing World of Gumball | The Points
 2:45 pm | Amazing World of Gumball | The Potion
 3:00 pm | Amazing World of Gumball | The Bus
 3:15 pm | Amazing World of Gumball | The Spinoffs
 3:30 pm | Amazing World of Gumball | The Night
 3:45 pm | Amazing World of Gumball | The Transformation
 4:00 pm | DC Super Hero Girls | SweetJustice Part 1
 4:15 pm | DC Super Hero Girls | SweetJustice Part 2
 4:30 pm | The Powerpuff Girls | Rebel Rebel
 4:45 pm | The Powerpuff Girls | Our Brand Is Chaos
 5:00 pm | Amazing World of Gumball | The Misunderstandings
 5:15 pm | Amazing World of Gumball | The Understanding
 5:30 pm | Amazing World of Gumball | The Roots
 5:45 pm | Amazing World of Gumball | The Ad
 6:00 pm | Amazing World of Gumball | The Blame
 6:15 pm | Amazing World of Gumball | The Stink
 6:30 pm | Victor and Valentino | The Babysitter
 6:45 pm | Victor and Valentino | Hurricane Chata
 7:00 pm | Total Drama: Revenge of the Island | Backstabbers Ahoy
 7:30 pm | Total Drama: Revenge of the Island | Runaway Model