# 2019-04-26
 6:00 am | Steven Universe | Message Received
 6:15 am | Teen Titans Go! | Brain Food
 6:30 am | Teen Titans Go! | Little Buddies; Missing
 7:00 am | Victor and Valentino | Brotherly Love
 7:15 am | Victor and Valentino | Chata's Quinta Quinceañera
 7:30 am | Amazing World of Gumball | The Heist
 7:45 am | Amazing World of Gumball | The Best
 8:00 am | Amazing World of Gumball | The Phone; The Job
 8:30 am | Amazing World of Gumball | The Words; The Apology
 9:00 am | Total DramaRama | Venthalla
 9:15 am | Total DramaRama | Hic Hic Hooray
 9:30 am | Total DramaRama | Melter Skelter
 9:45 am | Total DramaRama | Having the Timeout of Our Lives
10:00 am | Teen Titans Go! | TV Knight 2
10:15 am | Teen Titans Go! | The Academy
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
11:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
11:30 am | Teen Titans Go! | Mr. Butt; Man Person
12:00 pm | Amazing World of Gumball | The Internet; The Plan
12:30 pm | Amazing World of Gumball | The World; The Finale
 1:00 pm | Amazing World of Gumball | The Lady
 1:15 pm | Amazing World of Gumball | The Sucker
 1:30 pm | Total DramaRama | Cuttin' Corners
 1:45 pm | Total DramaRama | Know It All
 2:00 pm | Teen Titans Go! | Kabooms
 2:30 pm | Teen Titans Go! | Bro-Pocalypse
 2:45 pm | Teen Titans Go! | Mo' Money Mo' Problems
 3:00 pm | Ben 10 | Charm School's Out
 3:15 pm | Craig of the Creek | The Brood
 3:30 pm | Craig of the Creek | The Future Is Cardboard
 3:45 pm | Craig of the Creek | The Mystery of the Timekeeper
 4:00 pm | Amazing World of Gumball | The Hero; The Photo
 4:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 5:00 pm | Total DramaRama | Snow Way Out
 5:15 pm | Total DramaRama | Cone in 60 Seconds
 5:30 pm | Victor and Valentino | Dead Ringer
 5:45 pm | Victor and Valentino | Folk Art Foes
 6:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 pm | Teen Titans Go! | BBRBDAY
 6:45 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 7:00 pm | Amazing World of Gumball | The Fraud; The Void
 7:30 pm | We Bare Bears | Cousin Jon
 7:45 pm | We Bare Bears | Lord of the Poppies