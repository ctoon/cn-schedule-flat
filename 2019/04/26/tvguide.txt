# 2019-04-26
 6:00 am | Steven Universe | Message Received
 6:15 am | Teen Titans Go! | Brain Food
 6:30 am | Teen Titans Go! | Little Buddies; Missing
 7:00 am | Victor And Valentino | Brotherly Love; Chata's Quinta Quinceañera
 7:30 am | Amazing World of Gumball | The Best; The Heist
 8:00 am | Amazing World of Gumball | The Phone; The Job
 8:30 am | Amazing World of Gumball | The Words; The Apology
 9:00 am | Total DramaRama | Venthalla; Hic Hic Hooray
 9:30 am | Victor And Valentino | The Babysitter; Hurricane Chata
10:00 am | Teen Titans Go! | The Academy; TV Knight 2
10:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
11:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
11:30 am | Teen Titans Go! | Mr. Butt; Man Person
12:00 pm | Amazing World of Gumball | The Internet; The Plan
12:30 pm | Amazing World of Gumball | The World; The Finale
 1:00 pm | Amazing World of Gumball | The Sucker; The Lady
 1:30 pm | Total DramaRama | Cuttin' Corners; Know It All
 2:00 pm | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
 2:30 pm | Teen Titans Go! | Mo' Money Mo' Problems; Bro-pocalypse
 3:00 pm | Ben 10 | Charm School's Out
 3:15 pm | Craig of the Creek | The Brood
 3:30 pm | Craig of the Creek | The Future Is Cardboard; The Mystery Of The Timekeeper
 4:00 pm | Amazing World of Gumball | The Hero; The Photo
 4:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 5:00 pm | Total DramaRama | Snow Way Out; Cone In 60 Seconds
 5:30 pm | Victor And Valentino | Folk Art Foes; Dead Ringer
 6:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 pm | Teen Titans Go! | Slapping Butts And Celebrating For No Reason; Bbrbday
 7:00 pm | Amazing World of Gumball | The Fraud; The Void
 7:30 pm | We Bare Bears | Cousin Jon; Lord Of The Poppies