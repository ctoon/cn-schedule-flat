# 2019-04-29
 6:00 am | Steven Universe | Super Watermelon Island
 6:15 am | Teen Titans Go! | The Cruel Giggling Ghoul
 6:30 am | Teen Titans Go! | Pyramid Scheme
 6:45 am | Teen Titans Go! | Bottle Episode
 7:00 am | Craig of the Creek | Creek Cart Racers
 7:15 am | Craig of the Creek | Sunday Clothes
 7:30 am | Amazing World of Gumball | The Awareness
 7:45 am | Amazing World of Gumball | The Slip
 8:00 am | Amazing World of Gumball | The Drama
 8:15 am | Amazing World of Gumball | The Buddy
 8:30 am | Amazing World of Gumball | The Return
 8:45 am | Amazing World of Gumball | The Nemesis
 9:00 am | Total DramaRama | Snow Way Out
 9:15 am | Total DramaRama | That's a Wrap
 9:30 am | Victor and Valentino | Hurricane Chata
 9:45 am | Teen Titans Go! | Double Trouble
10:00 am | Teen Titans Go! | Pirates; I See You
10:30 am | Teen Titans Go! | Brian; Nature
11:00 am | Teen Titans Go! | Dude Relax; Laundry Day
11:30 am | Amazing World of Gumball | The Uploads
11:45 am | Amazing World of Gumball | The Apprentice
12:00 pm | Amazing World of Gumball | The Hug
12:15 pm | Amazing World of Gumball | The Wicked
12:30 pm | Amazing World of Gumball | The Internet; The Plan
 1:00 pm | Amazing World of Gumball | The World; The Finale
 1:30 pm | Total DramaRama | Invasion of the Booger Snatchers
 1:45 pm | Total DramaRama | Having the Timeout of Our Lives
 2:00 pm | Teen Titans Go! | The Art of Ninjutsu
 2:15 pm | Teen Titans Go! | Think About Your Future
 2:30 pm | Teen Titans Go! | Booty Scooty
 2:45 pm | Teen Titans Go! | Who's Laughing Now
 3:00 pm | Ben 10 | Omni-Copped
 3:15 pm | Craig of the Creek | Sour Candy Trials
 3:30 pm | Craig of the Creek | Kelsey the Elder
 3:45 pm | Craig of the Creek | Doorway to Helen
 4:00 pm | Amazing World of Gumball | The Awkwardness
 4:15 pm | Amazing World of Gumball | The Nest
 4:30 pm | Amazing World of Gumball | The Points
 4:45 pm | Amazing World of Gumball | The Bus
 5:00 pm | Total DramaRama | The Price of Advice
 5:15 pm | Total DramaRama | Too Much of a Goo'd Thing
 5:30 pm | Victor and Valentino | The Dark Room
 5:45 pm | Victor and Valentino | Suerte
 6:00 pm | Teen Titans Go! | Lil' Dimples
 6:15 pm | Teen Titans Go! | TV Knight 4
 6:30 pm | Teen Titans Go! | Slapping Butts and Celebrating for No Reason
 6:45 pm | Teen Titans Go! | Nostalgia Is Not a Substitute for an Actual Story
 7:00 pm | Amazing World of Gumball | The Silence
 7:15 pm | Amazing World of Gumball | The Master
 7:30 pm | We Bare Bears | The Mummy's Curse
 7:45 pm | We Bare Bears | Sandcastle