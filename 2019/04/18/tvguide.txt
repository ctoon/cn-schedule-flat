# 2019-04-18
 6:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 6:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
 7:00 am | Total Drama Island | Brunch of Disgustingness
 7:30 am | Amazing World of Gumball | The Vision; The Choices
 8:00 am | Amazing World of Gumball | The Neighbor; The Pact
 8:30 am | Amazing World of Gumball | The Shippening; The Brain
 9:00 am | Total DramaRama | That's A Wrap; Stay Goth, Poodle Girl, Stay Goth
 9:30 am | Total DramaRama | The Bad Guy Busters; Snow Way Out
10:00 am | Teen Titans Go! | Booby Trap House; Fish Water
10:30 am | Teen Titans Go! | TV Knight; BBSFBDAY!
11:00 am | Teen Titans Go! | Booty Eggs; Little Elvis
11:30 am | Teen Titans Go! | Easter Creeps; The Teen Titans Go Easter Holiday Classic
12:00 pm | Amazing World of Gumball | The Awareness; The Slip
12:30 pm | Amazing World of Gumball | The Possession; The Buddy
 1:00 pm | Amazing World of Gumball | The Vase; The Matchmaker
 1:30 pm | Total DramaRama | Inglorious Toddlers; Duck Duck Juice
 2:00 pm | Teen Titans Go! | Master Detective; Hand Zombie
 2:30 pm | Teen Titans Go! | The Avogodo; Employee of the Month Redux
 3:00 pm | Ben 10 | Poles Apart
 3:15 pm | Craig of the Creek | Secret Book Club
 3:30 pm | Craig of the Creek | Creek Cart Racers; Wildernessa
 4:00 pm | Amazing World of Gumball | The Rival; The One
 4:30 pm | Amazing World of Gumball | The Limit; The Game
 5:00 pm | Total DramaRama | Cuttin' Corners; Know It All
 5:30 pm | Total DramaRama | Snots Landing; Aquarium For A Dream
 6:00 pm | Teen Titans Go! | Brain Food; In and Out
 6:30 pm | Teen Titans Go! | The Chaff; Them Soviet Boys
 7:00 pm | Amazing World of Gumball | The Promise; The Voice
 7:30 pm | We Bare Bears | Lil' Squid; I, Butler