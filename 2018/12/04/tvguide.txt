# 2018-12-04
 6:00 am | Amazing World of Gumball | The Watch; The Bet
 6:30 am | Amazing World of Gumball | The Bumpkin; The Flakers
 7:00 am | Amazing World of Gumball | The Authority; The Virus
 7:30 am | Amazing World of Gumball | The Pony; The Storm
 8:00 am | Teen Titans Go! | Dreams; Grandma Voice
 8:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 9:00 am | Teen Titans Go! | Mr. Butt; Man Person
 9:30 am | Teen Titans Go! | Pirates; I See You
10:00 am | Amazing World of Gumball | The Vision; The Choices
10:30 am | Amazing World of Gumball | The Code; The Test
11:00 am | Amazing World of Gumball | The Slide; The Loophole
11:30 am | Amazing World of Gumball | The Copycats; The Potato
12:00 pm | Teen Titans Go! | Friendship; Vegetables
12:30 pm | Teen Titans Go! | The Mask; Slumber Party
 1:00 pm | Amazing World of Gumball | The Promise; The Voice
 1:30 pm | Amazing World of Gumball | The Boombox; The Castle
 2:00 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
 2:30 pm | Teen Titans Go! | Road Trip; The Best Robin
 3:00 pm | Ben 10 | The Sound and the Furry
 3:15 pm | Amazing World of Gumball | The Tape
 3:30 pm | Amazing World of Gumball | The Internet; The Plan
 4:00 pm | Amazing World of Gumball | The Cycle; The Stars
 4:30 pm | Amazing World of Gumball | The Grades; The Diet
 5:00 pm | Craig of the Creek | Creek Cart Racers; Dog Decider
 5:30 pm | Elf Pets: Santa's St. Bernards Save Christmas | 
 6:00 pm | Teen Titans Go! | Teen Titans Save Christmas; Mo' Money Mo' Problems
 6:30 pm | Teen Titans Go! | Shrimps And Prime Rib; Bro-pocalypse
 7:00 pm | We Bare Bears | Mom App; The Kitty
 7:30 pm | Total DramaRama | Venthalla; Germ Factory
 8:00 pm | Amazing World of Gumball | The Ex; The Sorcerer
 8:30 pm | Amazing World of Gumball | The Uncle; The Menu