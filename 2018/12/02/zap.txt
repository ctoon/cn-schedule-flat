# 2018-12-02
 6:00 am | Ben 10 | Past Aliens Present
 6:15 am | Ben 10 | Double Hex
 6:30 am | Teen Titans Go! | Mo' Money Mo' Problems
 6:45 am | Teen Titans Go! | Oregon Trail
 7:00 am | Teen Titans Go! | Bro-Pocalypse
 7:15 am | Teen Titans Go! | Who's Laughing Now
 7:30 am | Teen Titans Go! | Booty Scooty
 7:45 am | Teen Titans Go! | The Art of Ninjutsu
 8:00 am | Total DramaRama | Inglorious Toddlers
 8:15 am | Total DramaRama | The Bad Guy Busters
 8:30 am | Total DramaRama | Cone in 60 Seconds
 8:45 am | Total DramaRama | Germ Factory
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 9:30 am | Teen Titans Go! | Flashback
10:00 am | Teen Titans Go! | Beast Girl
10:15 am | Teen Titans Go! | History Lesson
10:30 am | Teen Titans Go! | BBCYFSHIPBDAY
10:45 am | Teen Titans Go! | Rad Dudes With Bad Tudes
11:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
11:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:45 am | Teen Titans Go! | Operation Dude Rescue: Part 2
12:00 pm | Teen Titans Go! | Demon Prom
12:15 pm | Teen Titans Go! | Wally T
12:30 pm | Teen Titans Go! | Throne of Bones
12:45 pm | Teen Titans Go! | Obinray
 1:00 pm | Total DramaRama | Inglorious Toddlers
 1:15 pm | Total DramaRama | Ant We All Just Get Along
 1:30 pm | Total DramaRama | Sharing Is Caring
 1:45 pm | Total DramaRama | Cuttin' Corners
 2:00 pm | Amazing World of Gumball | The Intelligence
 2:15 pm | Amazing World of Gumball | The Guy
 2:30 pm | Amazing World of Gumball | The Schooling
 2:45 pm | Amazing World of Gumball | The Founder
 3:00 pm | Amazing World of Gumball | The Disaster
 3:15 pm | Amazing World of Gumball | The Re-Run
 3:30 pm | Amazing World of Gumball | The Parents
 3:45 pm | Amazing World of Gumball | The Stories
 4:00 pm | Total DramaRama | Aquarium for a Dream
 4:15 pm | Total DramaRama | The Date
 4:30 pm | Total DramaRama | Free Chili
 4:45 pm | Total DramaRama | Cluckwork Orange
 5:00 pm | Lego DC Comics Super Heroes: The Flash | 
 7:00 pm | Amazing World of Gumball | The Brain
 7:15 pm | Amazing World of Gumball | The Compilation
 7:30 pm | Amazing World of Gumball | The Shippening
 7:45 pm | Amazing World of Gumball | The Fury
 8:00 pm | Amazing World of Gumball | The Neighbor
 8:15 pm | Amazing World of Gumball | The Detective
 8:30 pm | Amazing World of Gumball | The Pact
 8:45 pm | Amazing World of Gumball | The Blame