# 2018-12-03
 6:00 am | Amazing World of Gumball | The Flower/The Banana
 6:30 am | Amazing World of Gumball | The Phone/The Job
 7:00 am | Amazing World of Gumball | The Words/The Apology
 7:30 am | Amazing World of Gumball | The Skull/Christmas
 8:00 am | Teen Titans Go! | Birds/Be Mine
 8:30 am | Teen Titans Go! | Brain Food/In and Out
 9:00 am | Teen Titans Go! | Little Buddies/Missing
 9:30 am | Teen Titans Go! | Uncle Jokes/Mas y Menos
10:00 am | Amazing World of Gumball | Detective/The Fury
10:30 am | Amazing World of Gumball | Compilation/The Stories
11:00 am | Amazing World of Gumball | Disaster/The Re-Run
11:30 am | Amazing World of Gumball | Guy/The Boredom
12:00 pm | Teen Titans Go! | Brian/Nature
12:30 pm | Teen Titans Go! | Salty Codgers/Knowledge
 1:00 pm | Amazing World of Gumball | The Dream/The Sidekick
 1:30 pm | Amazing World of Gumball | The Hero/The Photo
 2:00 pm | Teen Titans Go! | Love Monsters/Baby Hands
 2:30 pm | Teen Titans Go! | Sandwich Thief/Money Grandma
 3:00 pm | Ben 10 | Reststop Roustabout
 3:15 pm | Amazing World of Gumball | The Tag
 3:30 pm | Amazing World of Gumball | The Limit/The Game
 4:00 pm | Amazing World of Gumball | Fuss/The Outside
 4:30 pm | Amazing World of Gumball | Vase/The Matchmaker
 5:00 pm | Craig of the Creek | Secret Book Club/Bring Out Your Beast
 5:30 pm | Total DramaRama | Inglorious Toddlers/That's a Wrap
 6:00 pm | Teen Titans Go! | Teen Titans Save Christmas/The True Meaning of Christmas
 6:30 pm | SPECIAL | Elf Pets: Santa's St. Bernards Save Christmas
 7:00 pm | We Bare Bears | Perfect Tree/Christmas Parties
 7:30 pm | Total DramaRama | Cone in 60 Seconds/Duck Duck Juice