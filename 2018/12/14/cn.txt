# 2018-12-14
 6:00 am | Amazing World of Gumball | The Triangle/The Money
 6:30 am | Amazing World of Gumball | Return/The Nemesis
 7:00 am | Amazing World of Gumball | Crew/The Others
 7:30 am | Amazing World of Gumball | Signature/The Check
 8:00 am | Teen Titans Go! | True Meaning of Christmas/Squash & Stretch
 8:30 am | Teen Titans Go! | Garage Sale/Secret Garden
 9:00 am | Teen Titans Go! | Cruel Giggling Ghoul/Pyramid Scheme
 9:30 am | Teen Titans Go! | Bottle Episode/Finally a Lesson
10:00 am | Amazing World of Gumball | The Coach/The Joy
10:30 am | Amazing World of Gumball | The Recipe/The Puppy
11:00 am | Amazing World of Gumball | The Name/The Extras
11:30 am | Amazing World of Gumball | The Gripes/The Vacation
12:00 pm | Teen Titans Go! | Think About Your Future/Booty Scooty
12:30 pm | Teen Titans Go! | Who's Laughing Now/Oregon Trail
 1:00 pm | Amazing World of Gumball | Uploads/The Apprentice
 1:30 pm | Amazing World of Gumball | Hug/The Wicked
 2:00 pm | Teen Titans Go! | Snuggle Time/Oh Yeah!
 2:30 pm | Teen Titans Go! | Riding the Dragon/The Overbite
 3:00 pm | Ben 10 | The Charm Offensive
 3:15 pm | Amazing World of Gumball | The Origins
 3:30 pm | Amazing World of Gumball | Origins Part 2/The Girlfriend
 4:00 pm | Amazing World of Gumball | The Procrastinators/The Shell
 4:30 pm | Amazing World of Gumball | The Mirror/The Burden
 5:00 pm | Craig of the Creek | JPony/Jessica Goes to the Creek
 5:30 pm | Total DramaRama | Free Chili/That's a Wrap
 6:00 pm | Teen Titans Go! | Lication/Obinray
 6:30 pm | Teen Titans Go! | Hot Salad Water/Arms Race with Legs
 7:00 pm | We Bare Bears | Paperboyz/Yuri and the Bear
 7:30 pm | Total DramaRama | Duck Duck Juice/Cone in 60 Seconds