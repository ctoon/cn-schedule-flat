# 2018-12-12
 6:00 am | Amazing World of Gumball | The Bros/The Man
 6:30 am | Amazing World of Gumball | The Pizza/The Lie
 7:00 am | Amazing World of Gumball | The Butterfly/The Question
 7:30 am | Amazing World of Gumball | The Oracle/The Safety
 8:00 am | Teen Titans Go! | Cat's Fancy/Leg Day
 8:30 am | Teen Titans Go! | Dignity of Teeth/Croissant
 9:00 am | Teen Titans Go! | Spice Game/I'm the Sauce
 9:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory/Accept the Next Proposition You Hear
10:00 am | Amazing World of Gumball | Founder/The Schooling
10:30 am | Amazing World of Gumball | Intelligence/The Potion
11:00 am | Amazing World of Gumball | Spinoffs/The Transformation
11:30 am | Amazing World of Gumball | Understanding/The Ad
12:00 pm | Teen Titans Go! | True Meaning of Christmas/Squash & Stretch
12:30 pm | Teen Titans Go! | Garage Sale/Secret Garden
 1:00 pm | Amazing World of Gumball | The Triangle/The Money
 1:30 pm | Amazing World of Gumball | Return/The Nemesis
 2:00 pm | Teen Titans Go! | Cruel Giggling Ghoul/Pyramid Scheme
 2:30 pm | Teen Titans Go! | Bottle Episode/Finally a Lesson
 3:00 pm | Ben 10 | Ye Olde Laser Duel
 3:15 pm | Amazing World of Gumball | The Others
 3:30 pm | Amazing World of Gumball | Signature/The Check
 4:00 pm | Amazing World of Gumball | The Coach/The Joy
 4:30 pm | Amazing World of Gumball | The Recipe/The Puppy
 5:00 pm | Craig of the Creek | Doorway to Helen/Too Many Treasures
 5:30 pm | Total DramaRama | Aquarium for a Dream/A Ninjustice to Harold
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:30 pm | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
 7:00 pm | We Bare Bears | Lil' Squid/Christmas Parties
 7:30 pm | Total DramaRama | Free Chili/That's a Wrap