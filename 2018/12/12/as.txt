# 2018-12-12
 8:00 pm | Amazing World of Gumball | The Name/The Extras
 8:30 pm | Amazing World of Gumball | The Gripes/The Vacation
 9:00 pm | American Dad | The Wrestler
 9:30 pm | American Dad | Dr. Klaustus
10:00 pm | Bob's Burgers | Burger Wars
10:30 pm | Bob's Burgers | Weekend at Morts
11:00 pm | Family Guy | Road to Rupert
11:30 pm | Family Guy | Peter's Two Dads
12:00 am | Rick and Morty | The Whirly Dirly Conspiracy
12:30 am | Robot Chicken | Big Trouble in Little Clerks 2
12:45 am | Robot Chicken | Kramer vs. Showgirls
 1:00 am | Aqua Teen | Video Ouija
 1:15 am | Squidbillies | Lerm
 1:30 am | Bob's Burgers | Burger Wars
 2:00 am | Bob's Burgers | Weekend at Morts
 2:30 am | Family Guy | Road to Rupert
 3:00 am | Family Guy | Peter's Two Dads
 3:30 am | Rick and Morty | The Whirly Dirly Conspiracy
 4:00 am | Infomercials | Pervert Everything
 4:15 am | Infomercials | NewsHits
 4:30 am | Aqua Teen | Video Ouija
 4:45 am | Squidbillies | Lerm
 5:00 am | American Dad | The Wrestler
 5:30 am | American Dad | Dr. Klaustus