# 2018-12-01
 8:00 pm | Amazing World of Gumball | Brain/The Compilation
 8:30 pm | Amazing World of Gumball | Shippening/The Fury
 9:00 pm | Dragon Ball Z Kai | Sit Tight, Chiaotzu! Tien's Screaming Tri-Beam!
 9:30 pm | My Hero Academia | Midoriya and Shigaraki
10:00 pm | Naruto:Shippuden | Fight! Rock Lee!
10:30 pm | Boruto: Naruto Next Generations | Proof of Oneself
11:00 pm | Dragon Ball Super | A Mysterious Beauty Appears! The Enigma of the Tien Shin-Style Dojo?!
11:30 pm | Mob Psycho 100 | OCHIMUSHA ~Psychic Powers and Me~
12:00 am | Attack on Titan | Pain
12:30 am | JoJo's Bizarre Adventure: Diamond Is Unbreakable | Let's Go to the Manga Artist's House Part 1
 1:00 am | Black Clover | Despair vs. Hope
 1:30 am | Hunter x Hunter | Defeat x and x Dignity
 2:00 am | FLCL: Alternative | Full Flat
 2:30 am | Pop Team Epic | The Documentary
 3:00 am | Lupin The 3rd Part 4 | World Dissection Part 1
 3:30 am | Samurai Jack | XI
 4:00 am | Boondocks  | The Passion of Reverend Ruckus
 4:30 am | Apollo Gauntlet | Lunacy
 4:45 am | Apollo Gauntlet | Dinner Party
 5:00 am | Venture Brothers  | The Unicorn in Captivity
 5:30 am | Home Movies | Storm Warning