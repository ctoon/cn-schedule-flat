# 2018-12-01
 6:00 am | Teen Titans Go! | The Streak, Part 1
 6:15 am | Teen Titans Go! | The Streak, Part 2
 6:30 am | Transformers: Cyberverse | Siloed
 6:45 am | Teen Titans Go! | The Fight
 7:00 am | Teen Titans Go! | Quantum Fun
 7:15 am | Teen Titans Go! | The Groover
 7:30 am | Teen Titans Go! | The Power of Shrimps
 7:45 am | Teen Titans Go! | Teen Titans Save Christmas
 8:00 am | Amazing World of Gumball | The Buddy
 8:15 am | Amazing World of Gumball | The Fuss
 8:30 am | Amazing World of Gumball | The Drama
 8:45 am | Amazing World of Gumball | The Potato
 9:00 am | Total DramaRama | Inglorious Toddlers
 9:15 am | Total DramaRama | Bananas & Cheese
 9:30 am | Total DramaRama | Hic Hic Hooray
 9:45 am | Total DramaRama | Having the Timeout of Our Lives
10:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition (Alternate)
10:30 am | Teen Titans Go! | My Name Is Jose
10:45 am | Teen Titans Go! | Booby Trap House
11:00 am | Teen Titans Go! | Tower Renovation
11:15 am | Teen Titans Go! | Halloween vs. Christmas
11:30 am | Teen Titans Go! | Kabooms
12:00 pm | Ben 10 | Dreamtime
12:15 pm | Teen Titans Go! | The Overbite
12:30 pm | Teen Titans Go! | Chicken in the Cradle
12:45 pm | Teen Titans Go! | Riding the Dragon
 1:00 pm | Teen Titans Go! | The Scoop
 1:15 pm | Teen Titans Go! | Oh Yeah!
 1:30 pm | Teen Titans Go! | TV Knight 3
 1:45 pm | Teen Titans Go! | Snuggle Time
 2:00 pm | Amazing World of Gumball | The Slip
 2:15 pm | Amazing World of Gumball | The Copycats
 2:30 pm | Amazing World of Gumball | The Awareness
 2:45 pm | Amazing World of Gumball | The Loophole
 3:00 pm | Amazing World of Gumball | The Stink
 3:15 pm | Amazing World of Gumball | The Slide
 3:30 pm | Amazing World of Gumball | The Ad
 3:45 pm | Amazing World of Gumball | The Test
 4:00 pm | Total DramaRama | Inglorious Toddlers
 4:15 pm | Total DramaRama | A Ninjustice to Harold
 4:30 pm | Total DramaRama | Tiger Fail
 4:45 pm | Total DramaRama | That's a Wrap
 5:00 pm | Amazing World of Gumball | The Understanding
 5:15 pm | Amazing World of Gumball | The Code
 5:30 pm | Amazing World of Gumball | The Transformation
 5:45 pm | Amazing World of Gumball | The Choices
 6:00 pm | Amazing World of Gumball | The Spinoffs
 6:15 pm | Amazing World of Gumball | The Vision
 6:30 pm | Amazing World of Gumball | The Potion
 6:45 pm | Amazing World of Gumball | The Boredom
 7:00 pm | Lego DC Comics Super Heroes: The Flash | 