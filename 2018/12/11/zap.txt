# 2018-12-11
 6:00 am | Amazing World of Gumball | The Law; The Allergy
 6:30 am | Amazing World of Gumball | The Mothers; The Password
 7:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 7:30 am | Amazing World of Gumball | The Mirror; The Burden
 8:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 8:30 am | Teen Titans Go! | Operation Tin Man; Nean
 9:00 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 9:30 am | Teen Titans Go! | The Fight
 9:45 am | Teen Titans Go! | The Groover
10:00 am | Amazing World of Gumball | The Faith
10:15 am | Amazing World of Gumball | The Candidate
10:30 am | Amazing World of Gumball | The Anybody
10:45 am | Amazing World of Gumball | The Pact
11:00 am | Amazing World of Gumball | The Neighbor
11:15 am | Amazing World of Gumball | The Shippening
11:30 am | Amazing World of Gumball | The Brain
11:45 am | Amazing World of Gumball | The Parents
12:00 pm | Teen Titans Go! | The Fourth Wall
12:15 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
12:30 pm | Teen Titans Go! | Grube's Fairytales
12:45 pm | Teen Titans Go! | A Farce
 1:00 pm | Amazing World of Gumball | The Friend; The Saint
 1:30 pm | Amazing World of Gumball | The Society; The Spoiler
 2:00 pm | Teen Titans Go! | Animals: It's Just a Word!
 2:15 pm | Teen Titans Go! | BBBDay!
 2:30 pm | Teen Titans Go! | Two Parter: Part One
 2:45 pm | Teen Titans Go! | Two Parter: Part Two
 3:00 pm | Ben 10 | Ben Again & Again
 3:15 pm | Amazing World of Gumball | The Countdown
 3:30 pm | Amazing World of Gumball | The Downer; The Egg
 4:00 pm | Amazing World of Gumball | The Stink
 4:15 pm | Amazing World of Gumball | The Awareness
 4:30 pm | Amazing World of Gumball | The Slip
 4:45 pm | Amazing World of Gumball | The Drama
 5:00 pm | Craig of the Creek | The Last Kid in the Creek
 5:15 pm | Craig of the Creek | Wildernessa
 5:30 pm | Total DramaRama | Cuttin' Corners
 5:45 pm | Total DramaRama | Having the Timeout of Our Lives
 6:00 pm | Teen Titans Go! | The Academy
 6:15 pm | Teen Titans Go! | The Art of Ninjutsu
 6:30 pm | Teen Titans Go! | History Lesson
 6:45 pm | Teen Titans Go! | TV Knight 2
 7:00 pm | We Bare Bears | I, Butler
 7:15 pm | We Bare Bears | Grizzly the Movie
 7:30 pm | Total DramaRama | The Date
 7:45 pm | Total DramaRama | Tiger Fail
 8:00 pm | Amazing World of Gumball | The Buddy
 8:15 pm | Amazing World of Gumball | The Return
 8:30 pm | Amazing World of Gumball | The Kids; The Fan