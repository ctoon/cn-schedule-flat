# 2018-12-27
 6:00 am | Steven Universe | Arcade Mania
 6:15 am | Amazing World of Gumball | The Line
 6:30 am | Amazing World of Gumball | List/The News
 7:00 am | Bakugan: Battle Planet | Local Heroes/Pegatrix
 7:30 am | Teen Titans Go! | Girls' Night Out/You're Fired
 8:00 am | Teen Titans Go! | Power of Shrimps/The Real Orangins!
 8:30 am | Teen Titans Go! | Quantum Fun/The Fight
 9:00 am | Craig of the Creek | Lost in the Sewer/Secret Book Club
 9:30 am | Amazing World of Gumball | Detective/The Fury
10:00 am | Amazing World of Gumball | Compilation/The Stories
10:30 am | Total DramaRama | A Ninjustice to Harold/Aquarium for a Dream
11:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
11:30 am | Teen Titans Go! | Legendary Sandwich/Pie Bros
12:00 pm | Unikitty | Landlord Lord/Beach Daze
12:30 pm | Amazing World of Gumball | Brain/The Parents
 1:00 pm | Amazing World of Gumball | Neighbor/The Shippening
 1:30 pm | We Bare Bears | Adopted/Pizza Band
 2:00 pm | Teen Titans Go! | Super Robin/Tower Power
 2:30 pm | Teen Titans Go! | Parasite/Starliar
 3:00 pm | Amazing World of Gumball | Schooling/The Founder
 3:30 pm | Amazing World of Gumball | Intelligence/The Potion
 4:00 pm | Total Drama Island | Not Quite Famous
 4:30 pm | Total Drama Island | The Sucky Outdoors
 5:00 pm | Craig of the Creek | Dog Decider/Power Punchers
 5:30 pm | Total DramaRama | That's a Wrap/Free Chili
 6:00 pm | Teen Titans Go! | Night Begins to Shine Special
 7:00 pm | We Bare Bears | Bearz II Men/Hibernation
 7:30 pm | Total DramaRama | Snots Landing/Paint That a Shame