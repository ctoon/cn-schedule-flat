# 2018-12-20
 6:00 am | Steven Universe | Frybo
 6:15 am | Amazing World of Gumball | The Misunderstandings
 6:30 am | Amazing World of Gumball | The Roots
 6:45 am | Amazing World of Gumball | The Blame
 7:00 am | Amazing World of Gumball | The Detective
 7:15 am | Amazing World of Gumball | The Fury
 7:30 am | Amazing World of Gumball | The Compilation
 7:45 am | Amazing World of Gumball | The Stories
 8:00 am | Teen Titans Go! | The Streak, Part 1
 8:15 am | Teen Titans Go! | The Streak, Part 2
 8:30 am | Teen Titans Go! | BBRAE
 9:00 am | Teen Titans Go! | Movie Night
 9:15 am | Teen Titans Go! | Permanent Record
 9:30 am | Teen Titans Go! | Titan Saving Time
 9:45 am | Teen Titans Go! | Master Detective
10:00 am | Amazing World of Gumball | The Countdown; The Nobody
10:30 am | Amazing World of Gumball | The Downer; The Egg
11:00 am | Amazing World of Gumball | The Triangle; The Money
11:30 am | Amazing World of Gumball | The Return
11:45 am | Amazing World of Gumball | The Nemesis
12:00 pm | Teen Titans Go! | Lication
12:15 pm | Teen Titans Go! | Ones and Zeroes
12:30 pm | Teen Titans Go! | Career Day
12:45 pm | Teen Titans Go! | TV Knight 2
 1:00 pm | Amazing World of Gumball | The Slide
 1:15 pm | Amazing World of Gumball | The Loophole
 1:30 pm | Amazing World of Gumball | The Copycats
 1:45 pm | Amazing World of Gumball | The Potato
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:30 pm | Teen Titans Go! | The Academy
 2:45 pm | Teen Titans Go! | Throne of Bones
 3:00 pm | Ben 10 | Can I Keep It?
 3:15 pm | Amazing World of Gumball | The Fuss
 3:30 pm | Amazing World of Gumball | The Vase
 3:45 pm | Amazing World of Gumball | The Matchmaker
 4:00 pm | Amazing World of Gumball | The Routine
 4:15 pm | Amazing World of Gumball | The Upgrade
 4:30 pm | Amazing World of Gumball | The Comic
 4:45 pm | Amazing World of Gumball | The Romantic
 5:00 pm | Craig of the Creek | Under the Overpass
 5:15 pm | Craig of the Creek | Dinner at the Creek
 5:30 pm | Total DramaRama | Bananas & Cheese
 5:45 pm | Total DramaRama | Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | 
 7:00 pm | We Bare Bears | Baby Bears Can't Jump
 7:15 pm | We Bare Bears | The Island
 7:30 pm | Total DramaRama | Paint That a Shame
 7:45 pm | Total DramaRama | Having the Timeout of Our Lives
 8:00 pm | Amazing World of Gumball | The Uploads
 8:15 pm | Amazing World of Gumball | The Apprentice
 8:30 pm | Amazing World of Gumball | The Hug
 8:45 pm | Amazing World of Gumball | The Wicked