# 2018-12-17
 6:00 am | Steven Universe | Cheeseburger Backpack
 6:15 am | Amazing World of Gumball | The Sale
 6:30 am | Amazing World of Gumball | Gift/The Parking
 7:00 am | Amazing World of Gumball | Routine/The Upgrade
 7:30 am | Amazing World of Gumball | Comic/The Romantic
 8:00 am | Teen Titans Go! | Arms Race with Legs/Obinray
 8:30 am | Teen Titans Go! | Wally T/Rad Dudes with Bad Tudes
 9:00 am | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
 9:30 am | Teen Titans Go! | History Lesson/The Art of Ninjutsu
10:00 am | Amazing World of Gumball | The Fraud/The Void
10:30 am | Amazing World of Gumball | The Boss/The Move
11:00 am | Amazing World of Gumball | The Law/The Allergy
11:30 am | Amazing World of Gumball | The Mothers/The Password
12:00 pm | Teen Titans Go! | Shrimps and Prime Rib/The Fight
12:30 pm | Teen Titans Go! | Booby Trap House/Fish Water
 1:00 pm | Amazing World of Gumball | Advice/The Signal
 1:30 pm | Amazing World of Gumball | Parasite/The Love
 2:00 pm | Teen Titans Go! | TV Knight/Teen Titans Save Christmas
 2:30 pm | Teen Titans Go! | BBSFBDAY!/Inner Beauty of a Cactus
 3:00 pm | Ben 10 | King Koil
 3:15 pm | Amazing World of Gumball | The Nest
 3:30 pm | Amazing World of Gumball | Points/The Bus
 4:00 pm | Amazing World of Gumball | The Butterfly/The Question
 4:30 pm | Amazing World of Gumball | The Oracle/The Safety
 5:00 pm | Craig of the Creek | Kelsey Quest/You're It
 5:30 pm | Total DramaRama | Paint That a Shame/The Bad Guy Busters
 6:00 pm | Teen Titans Go! | BL4Z3/Finally a Lesson
 6:30 pm | SPECIAL | An Elf's Story: The Elf on the Shelf
 7:00 pm | We Bare Bears | Christmas Movies/The Perfect Tree
 7:30 pm | Steven Universe | Legs from Here to Homeworld
 7:45 pm | Amazing World of Gumball | The Lie