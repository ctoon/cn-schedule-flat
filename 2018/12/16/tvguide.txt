# 2018-12-16
 6:00 am | Steven Universe | Laser Light Cannon
 6:15 am | Ben 10 | King Koil
 6:30 am | Teen Titans Go! | Oregon Trail; The Scoop
 7:00 am | Teen Titans Go! | Who's Laughing Now; Tv Knight 3
 7:30 am | Teen Titans Go! | Booty Scooty; Mo' Money Mo' Problems
 8:00 am | Teen Titans Go! | Titans Vs. Santa
 9:00 am | Total DramaRama | Paint That A Shame; Duck Duck Juice
 9:30 am | Total DramaRama | Venthalla; Hic Hic Hooray
10:00 am | Teen Titans Go! | The Art of Ninjutsu; History Lesson
10:30 am | Teen Titans Go! | Beast Girl; BBCYFSHIPBDAY
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
11:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
12:00 pm | Teen Titans Go! | Wally T; Rad Dudes With Bad Tudes
12:30 pm | Teen Titans Go! | Obinray; Demon Prom
 1:00 pm | Total DramaRama | Paint That A Shame; Having The Timeout Of Our Lives
 1:30 pm | Total DramaRama | A Ninjustice To Harold; Tiger Fail
 2:00 pm | Amazing World of Gumball | The Ollie; The Signature
 2:30 pm | Amazing World of Gumball | The Console; The Others
 3:00 pm | Amazing World of Gumball | The Box; The Crew
 3:30 pm | The Elf on the Shelf: An Elf's Story | 
 4:00 pm | The Powerpuff Girls | The Gift
 4:15 pm | OK K.O.! Let's Be Heroes | Super Black Friday
 4:30 pm | Alvin and the Chipmunks: The Road Chip | 
 6:30 pm | Amazing World of Gumball | The Matchmaker; The Nemesis
 7:00 pm | Total Drama Island | Paintball Deer Hunter
 7:30 pm | Total Drama Island | If You Can't Take the Heat
 8:00 pm | Amazing World of Gumball | The Fuss; The Drama
 8:30 pm | Amazing World of Gumball | The Potato; The Slip