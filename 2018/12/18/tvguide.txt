# 2018-12-18
 6:00 am | Steven Universe | Legs From Here to Homeworld
 6:15 am | Amazing World of Gumball | The Apprentice
 6:30 am | Amazing World of Gumball | The Hug; The Wicked
 7:00 am | Amazing World of Gumball | The Traitor; The Origins
 7:30 am | Amazing World of Gumball | The Origins Part 2; The Girlfriend
 8:00 am | Teen Titans Go! | Think About Your Future; Booty Scooty
 8:30 am | Teen Titans Go! | Who's Laughing Now; Oregon Trail
 9:00 am | Teen Titans Go! | Oh Yeah!; Snuggle Time
 9:30 am | Teen Titans Go! | Riding the Dragon; The Overbite
10:00 am | Amazing World of Gumball | The Procrastinators; The Shell
10:30 am | Amazing World of Gumball | The Mirror; The Burden
11:00 am | Amazing World of Gumball | The Bros; The Man
11:30 am | Amazing World of Gumball | The Pizza; The Lie
12:00 pm | Teen Titans Go! | Titans Vs. Santa
 1:00 pm | Amazing World of Gumball | The Night; The Misunderstandings
 1:30 pm | Amazing World of Gumball | The Roots; The Blame
 2:00 pm | Teen Titans Go! | Permanent Record; Movie Night
 2:30 pm | Teen Titans Go! | Titan Saving Time; Master Detective
 3:00 pm | Ben 10 | All Koiled Up
 3:15 pm | Amazing World of Gumball | The Fury
 3:30 pm | Amazing World of Gumball | The Compilation; The Stories
 4:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 4:30 pm | Amazing World of Gumball | The Egg; The Downer
 5:00 pm | Craig of the Creek | Vulture's Nest; Itch To Explore
 5:30 pm | Total DramaRama | Cone In 60 Seconds; Duck Duck Juice
 6:00 pm | Teen Titans Go! | Jinxed; Pyramid Scheme
 6:30 pm | Teen Titans Go! | Orangins; The Cruel Giggling Ghoul
 7:00 pm | We Bare Bears | Teacher's Pet; The Audition
 7:30 pm | Total DramaRama | Bananas & Cheese; Ant We All Just Get Along
 8:00 pm | Amazing World of Gumball | The Triangle; The Money
 8:30 pm | Amazing World of Gumball | The Return; The Nemesis