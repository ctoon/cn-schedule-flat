# 2018-12-30
 6:00 am | Steven Universe | Lars and the Cool Kids
 6:15 am | Ben 10 | All Koiled Up
 6:30 am | Teen Titans Go! | Snuggle Time/Oh Yeah!
 7:00 am | Teen Titans Go! | Chicken in the Cradle/Oregon Trail
 7:30 am | Teen Titans Go! | Scoop/Who's Laughing Now
 8:00 am | Teen Titans Go! | TV Knight 3/Booty Scooty
 8:30 am | Teen Titans Go! | Mo' Money Mo' Problems/Think About Your Future
 9:00 am | Craig of the Creek | Jessica's Trail/The Takeout Mission
 9:30 am | Craig of the Creek | Dinner at the Creek/Jextra Perrestrial
10:00 am | Teen Titans Go! | Bro-Pocalypse/The Art of Ninjutsu
10:30 am | Teen Titans Go! | Flashback
11:00 am | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
11:30 am | Teen Titans Go! | Beast Girl/History Lesson
12:00 pm | Teen Titans Go! | BBCYFSHIPBDAY/Rad Dudes with Bad Tudes
12:30 pm | Teen Titans Go! | More of the Same/Wally T/The Groover
 1:00 pm | Total DramaRama | Know it All/The Bad Guy Busters
 1:30 pm | Total DramaRama | Cone in 60 Seconds/Germ Factory
 2:00 pm | Amazing World of Gumball | Origins/The Origins Part 2
 2:30 pm | Amazing World of Gumball | Petals/The Line
 3:00 pm | Amazing World of Gumball | Traitor/The Deal
 3:30 pm | Amazing World of Gumball | Wicked/The Worst
 4:00 pm | Total DramaRama | Aquarium for a Dream/The Date
 4:30 pm | Total DramaRama | Free Chili/Cluckwork Orange
 5:00 pm | Amazing World of Gumball | The Society/The Spoiler
 5:30 pm | Amazing World of Gumball | The Countdown/The Nobody
 6:00 pm | Amazing World of Gumball | The Downer/The Egg
 6:30 pm | Amazing World of Gumball | The Triangle/The Money
 7:00 pm | Total Drama Island | X-treme Torture
 7:30 pm | Total Drama Island | Brunch of Disgustingness