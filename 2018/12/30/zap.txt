# 2018-12-30
 6:00 am | Steven Universe | Lars and the Cool Kids
 6:15 am | Ben 10 | All Koiled Up
 6:30 am | Teen Titans Go! | Snuggle Time
 6:45 am | Teen Titans Go! | Oh Yeah!
 7:00 am | Teen Titans Go! | Chicken in the Cradle
 7:15 am | Teen Titans Go! | Oregon Trail
 7:30 am | Teen Titans Go! | The Scoop
 7:45 am | Teen Titans Go! | Who's Laughing Now
 8:00 am | Teen Titans Go! | TV Knight 3
 8:15 am | Teen Titans Go! | Booty Scooty
 8:30 am | Teen Titans Go! | Mo' Money Mo' Problems
 8:45 am | Teen Titans Go! | Think About Your Future
 9:00 am | Craig of the Creek | Jessica's Trail
 9:15 am | Craig of the Creek | The Takeout Mission
 9:30 am | Craig of the Creek | Dinner at the Creek
 9:45 am | Craig of the Creek | Jextra Perrestrial
10:00 am | Teen Titans Go! | Bro-Pocalypse
10:15 am | Teen Titans Go! | The Art of Ninjutsu
10:30 am | Teen Titans Go! | Flashback
11:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:30 am | Teen Titans Go! | Beast Girl
11:45 am | Teen Titans Go! | History Lesson
12:00 pm | Teen Titans Go! | BBCYFSHIPBDAY
12:15 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
12:30 pm | Teen Titans Go! | More of the Same
12:45 pm | Teen Titans Go! | Wally T
 1:00 pm | Total DramaRama | Know It All
 1:15 pm | Total DramaRama | The Bad Guy Busters
 1:30 pm | Total DramaRama | Cone in 60 Seconds
 1:45 pm | Total DramaRama | Germ Factory
 2:00 pm | Amazing World of Gumball | The Origins
 2:15 pm | Amazing World of Gumball | The Origins
 2:30 pm | Amazing World of Gumball | The Petals
 2:45 pm | Amazing World of Gumball | The Line
 3:00 pm | Amazing World of Gumball | The Traitor
 3:15 pm | Amazing World of Gumball | The Deal
 3:30 pm | Amazing World of Gumball | The Wicked
 3:45 pm | Amazing World of Gumball | The Worst
 4:00 pm | Total DramaRama | Aquarium for a Dream
 4:15 pm | Total DramaRama | The Date
 4:30 pm | Total DramaRama | Free Chili
 4:45 pm | Total DramaRama | Cluckwork Orange
 5:00 pm | Amazing World of Gumball | The Society; The Spoiler
 5:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 6:00 pm | Amazing World of Gumball | The Downer; The Egg
 6:30 pm | Amazing World of Gumball | The Triangle; The Money
 7:00 pm | Total Drama Island | X-treme Torture
 7:30 pm | Total Drama Island | Brunch of Disgustingness
 8:00 pm | Amazing World of Gumball | The Uploads
 8:15 pm | Amazing World of Gumball | The Uncle
 8:30 pm | Amazing World of Gumball | The Romantic
 8:45 pm | Amazing World of Gumball | The Menu