# 2018-12-23
 6:00 am | Steven Universe | Serious Steven
 6:15 am | Ben 10 | The Feels
 6:30 am | Teen Titans Go! | Bbbday!; Orangins
 7:00 am | Teen Titans Go! | Titans Vs. Santa
 8:00 am | Dorothy and the Wizard of Oz | Cooking Up Some Magic; Snow Place Like Home
 8:30 am | Unikitty | Top Of The Naughty List; No Day Like Snow
 9:00 am | Total DramaRama | Snots Landing; Aquarium For A Dream
 9:30 am | Total DramaRama | The Date; Free Chili
10:00 am | Teen Titans Go! | 40%, 40%, 20%
10:30 am | Teen Titans Go! | The Fourth Wall; Permanent Record
11:00 am | Teen Titans Go! | BBRAE
11:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
12:00 pm | Teen Titans Go! | I'm The Sauce; Movie Night
12:30 pm | Teen Titans Go! | Spice Game; Inner Beauty Of A Cactus
 1:00 pm | Total DramaRama | Snots Landing; Cluckwork Orange
 1:30 pm | Total DramaRama | Duck Duck Juice; Venthalla
 2:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 2:30 pm | Amazing World of Gumball | The Stories; The Brain
 3:00 pm | Amazing World of Gumball | The Compilation; The Shippening
 3:30 pm | Amazing World of Gumball | The Fury; The Neighbor
 4:00 pm | Total DramaRama | Inglorious Toddlers; Bananas & Cheese
 4:30 pm | Total DramaRama | Having The Timeout Of Our Lives; Hic Hic Hooray
 5:00 pm | Amazing World of Gumball | The Pizza; The Lie
 5:30 pm | Amazing World of Gumball | The Butterfly; The Question
 6:00 pm | Amazing World of Gumball | The Oracle; The Safety
 6:30 pm | Teen Titans Go! | How's This For A Special? Spaaaace
 7:00 pm | Bakugan: Battle Planet | Origin Of Species Part 1; Origin Of Species Part 2; Burger Run; Monkey See, Monkey Don't
 8:00 pm | Amazing World of Gumball | The Roots; The Candidate
 8:30 pm | Amazing World of Gumball | The Misunderstandings; The Faith