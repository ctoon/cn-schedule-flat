# 2018-12-23
 6:00 am | Steven Universe | Serious Steven
 6:15 am | Ben 10 | The Feels
 6:30 am | Teen Titans Go! | BBBDay!
 6:45 am | Teen Titans Go! | Orangins
 7:00 am | Teen Titans Go! | 
 8:00 am | Dorothy and the Wizard of Oz | Snow Place Like Home
 8:15 am | Dorothy and the Wizard of Oz | Cooking Up Some Magic
 8:30 am | Unikitty | Top of the Naughty List
 8:45 am | Unikitty | No Day Like Snow Day
 9:00 am | Total DramaRama | Snots Landing
 9:15 am | Total DramaRama | Aquarium for a Dream
 9:30 am | Total DramaRama | The Date
 9:45 am | Total DramaRama | Free Chili
10:00 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
10:15 am | Teen Titans Go! | Titan Saving Time
10:30 am | Teen Titans Go! | The Fourth Wall
10:45 am | Teen Titans Go! | Permanent Record
11:00 am | Teen Titans Go! | BBRAE
11:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
11:45 am | Teen Titans Go! | Accept the Next Proposition You Hear
12:00 pm | Teen Titans Go! | I'm the Sauce
12:15 pm | Teen Titans Go! | Movie Night
12:30 pm | Teen Titans Go! | The Spice Game
12:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 1:00 pm | Total DramaRama | Snots Landing
 1:15 pm | Total DramaRama | Cluckwork Orange
 1:30 pm | Total DramaRama | Duck Duck Juice
 1:45 pm | Total DramaRama | Venthalla
 2:00 pm | Amazing World of Gumball | The Disaster
 2:15 pm | Amazing World of Gumball | The Re-Run
 2:30 pm | Amazing World of Gumball | The Stories
 2:45 pm | Amazing World of Gumball | The Brain
 3:00 pm | Amazing World of Gumball | The Compilation
 3:15 pm | Amazing World of Gumball | The Shippening
 3:30 pm | Amazing World of Gumball | The Fury
 3:45 pm | Amazing World of Gumball | The Neighbor
 4:00 pm | Total DramaRama | Inglorious Toddlers
 4:15 pm | Total DramaRama | Bananas & Cheese
 4:30 pm | Total DramaRama | Hic Hic Hooray
 4:45 pm | Total DramaRama | Having the Timeout of Our Lives
 5:00 pm | Amazing World of Gumball | The Pizza; The Lie
 5:30 pm | Amazing World of Gumball | The Butterfly; The Question
 6:00 pm | Amazing World of Gumball | The Oracle; The Safety
 6:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 7:00 pm | Bakugan: Battle Planet | Origin of Species Part 1; Origin of Species Part 2
 7:30 pm | Bakugan: Battle Planet | Burger Run; Monkey See, Monkey Don't
 8:00 pm | Amazing World of Gumball | The Roots
 8:15 pm | Amazing World of Gumball | The Candidate
 8:30 pm | Amazing World of Gumball | The Misunderstandings
 8:45 pm | Amazing World of Gumball | The Faith