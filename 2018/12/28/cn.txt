# 2018-12-28
 6:00 am | Steven Universe | Giant Woman
 6:15 am | Amazing World of Gumball | The Father
 6:30 am | Amazing World of Gumball | Cringe/The Cage
 7:00 am | Bakugan: Battle Planet | Frenemies/Strata's Fear
 7:30 am | Teen Titans Go! | Terra-ized/Artful Dodgers
 8:00 am | Teen Titans Go! | Driver's Ed/Dog Hand
 8:30 am | Teen Titans Go! | Double Trouble/The Date
 9:00 am | Craig of the Creek | Bring Out Your Beast/Creek Cart Racers
 9:30 am | Amazing World of Gumball | Choices/The Vision
10:00 am | Amazing World of Gumball | Code/The Test
10:30 am | Total DramaRama | Tiger Fail/The Date
11:00 am | Teen Titans Go! | Dude Relax!/Laundry Day
11:30 am | Teen Titans Go! | Ghostboy/La Larva de Amor
12:00 pm | Unikitty | Dancer Danger/Brawl Bot
12:30 pm | Amazing World of Gumball | Understanding/The Ad
 1:00 pm | Amazing World of Gumball | Spinoffs/The Transformation
 1:30 pm | We Bare Bears | Wingmen/Braces
 2:00 pm | Teen Titans Go! | Burger vs. Burrito/Matched
 2:30 pm | Teen Titans Go! | Colors of Raven/The Left Leg
 3:00 pm | Amazing World of Gumball | Awareness/The Stink
 3:30 pm | Amazing World of Gumball | Slip/The Drama
 4:00 pm | Total Drama Island | Phobia Factor
 4:30 pm | Total Drama Island | Up the Creek
 5:00 pm | Craig of the Creek | Curse/The Kid From 3030
 5:30 pm | Total DramaRama | Bad Guy Busters/Cluckwork Orange
 6:00 pm | Teen Titans Go! | Kabooms
 6:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace
 7:00 pm | We Bare Bears | Cupcake Job/Fashion Bears
 7:30 pm | Total DramaRama | Inglorious Toddlers/Bananas & Cheese