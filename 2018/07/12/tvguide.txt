# 2018-07-12
 6:00 am | Steven Universe | Chille Tid
 6:15 am | Ben 10 | Half-Sies
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Craig of the Creek | Jpony
 7:15 am | Amazing World of Gumball | The Stories
 7:30 am | Amazing World of Gumball | The Boredom; The Guy
 8:00 am | Craig of the Creek | Under the Overpass
 8:15 am | Teen Titans Go! | The Cape
 8:30 am | Teen Titans Go! | Booby Trap House; Shrimps and Prime Rib
 9:00 am | Craig of the Creek | The Invitation
 9:15 am | Teen Titans Go! | Mr. Butt
 9:30 am | Teen Titans Go! | Pirates; I See You
10:00 am | Craig of the Creek | Vulture's Nest
10:15 am | Amazing World of Gumball | The Hug
10:30 am | Amazing World of Gumball | The Sale; The Routine
11:00 am | Craig of the Creek | Kelsey Quest
11:15 am | Amazing World of Gumball | The Puppets
11:30 am | Amazing World of Gumball | The Sucker; The Lady
12:00 pm | Craig of the Creek | Jpony
12:15 pm | Teen Titans Go! | TV Knight
 1:00 pm | Craig of the Creek | Under the Overpass
 1:15 pm | Unikitty | Dinner Apart-y
 1:30 pm | Unikitty | Film Fest; Unikitty News!
 2:00 pm | Craig of the Creek | The Invitation
 2:15 pm | We Bare Bears | Panda's Sneeze
 2:30 pm | We Bare Bears | Brother Up; Viral Video
 3:00 pm | Craig of the Creek | Vulture's Nest
 3:15 pm | Summer Camp Island | The Basketball Liaries
 3:30 pm | Summer Camp Island | Popular Banana Split; Time Traveling Quick Pants
 4:00 pm | Craig of the Creek | Kelsey Quest
 4:15 pm | Teen Titans Go! | BBSFBDAY!
 4:30 pm | Teen Titans Go! | BBRAE
 5:00 pm | Craig of the Creek | Jpony
 5:15 pm | Amazing World of Gumball | The Vegging
 5:30 pm | Amazing World of Gumball | The Cringe; The Father
 6:00 pm | Craig of the Creek | Under the Overpass
 6:15 pm | Teen Titans Go! | The Scoop
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Craig of the Creek | The Invitation
 7:15 pm | We Bare Bears | The Audition
 7:30 pm | We Bare Bears | Yuri And The Bear; Crowbar Jones