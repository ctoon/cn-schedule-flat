# 2018-07-23
 6:00 am | Steven Universe | Sadie's Song
 6:15 am | Ben 10 | All Koiled Up
 6:30 am | Amazing World of Gumball | The Flower; The Banana
 7:00 am | Unikitty | Brawl Bot
 7:15 am | Amazing World of Gumball | The Box
 7:30 am | Amazing World of Gumball | The Matchmaker
 7:45 am | Amazing World of Gumball | The Grades
 8:00 am | Unikitty | Beach Daze
 8:15 am | Teen Titans Go! | Teen Titans Go! Hollywood Special
 9:00 am | Unikitty | Big Pup, Little Problem
 9:15 am | Teen Titans Go! | 
10:00 am | Unikitty | Tragic Magic
10:15 am | Amazing World of Gumball | The Night
10:30 am | Amazing World of Gumball | The Misunderstandings
10:45 am | Amazing World of Gumball | The Roots
11:00 am | Unikitty | Dancer Danger
11:15 am | Summer Camp Island | Ice Cream Headache
11:30 am | Summer Camp Island | Pepper's Blanket Is Missing
11:45 am | Summer Camp Island | Hedgehog Werewolf
12:00 pm | Unikitty | Brawl Bot
12:15 pm | Teen Titans Go! | Obinray
12:30 pm | Teen Titans Go! | Wally T
12:45 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 1:00 pm | Unikitty | Beach Daze
 1:15 pm | Teen Titans Go! | 
 2:00 pm | Unikitty | Big Pup, Little Problem
 2:15 pm | We Bare Bears | Planet Bears
 2:30 pm | We Bare Bears | Bunnies
 2:45 pm | We Bare Bears | Panda 2
 3:00 pm | Unikitty | Tragic Magic
 3:15 pm | Summer Camp Island | The Basketball Liaries
 3:30 pm | Summer Camp Island | Popular Banana Split
 3:45 pm | Summer Camp Island | Time Traveling Quick Pants
 4:00 pm | Unikitty | Dancer Danger
 4:15 pm | Craig of the Creek | Dog Decider
 4:30 pm | Craig of the Creek | Bring Out Your Beast
 4:45 pm | Craig of the Creek | Lost in the Sewer
 5:00 pm | Unikitty | Brawl Bot
 5:15 pm | Amazing World of Gumball | The Potato
 5:30 pm | Amazing World of Gumball | The Sorcerer
 5:45 pm | Amazing World of Gumball | The Console
 6:00 pm | Unikitty | Beach Daze
 6:15 pm | Teen Titans Go! | 
 7:00 pm | Unikitty | Big Pup, Little Problem
 7:15 pm | We Bare Bears | 