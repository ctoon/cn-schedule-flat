# 2018-07-26
 8:00 pm | Dragon Ball Super | Despair Redux! The Return of the Evil Emperor, Frieza!
 8:30 pm | American Dad | Faking Bad
 9:00 pm | Cleveland Show  | The Curious Case of Jr. Working at the Stool
 9:30 pm | American Dad | Minstrel Krampus
10:00 pm | Bob's Burgers | Best Burger
10:30 pm | Bob's Burgers | Father of the Bob
11:00 pm | Family Guy | Spies Reminiscent of Us
11:30 pm | Family Guy | Brian's Got a Brand New Bag
12:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
12:30 am | Venture Brothers  | Maybe No Go
 1:00 am | Robot Chicken | Casablankman
 1:15 am | Mostly 4 Millennials | Empowerment
 1:30 am | Bob's Burgers | Best Burger
 2:00 am | Bob's Burgers | Father of the Bob
 2:30 am | Family Guy | Spies Reminiscent of Us
 3:00 am | Family Guy | Brian's Got a Brand New Bag
 3:30 am | American Dad | Faking Bad
 4:00 am | Decker: Unsealed | Promises Kept
 4:15 am | Check It Out! | Pleasure
 4:30 am | Rick and Morty | Vindicators 3: The Return of Worldender
 5:00 am | Robot Chicken | In Bed Surrounded by Loved Ones
 5:15 am | Robot Chicken | Choked on Multi-Colored Scarves
 5:30 am | Cleveland Show  | The Curious Case of Jr. Working at the Stool