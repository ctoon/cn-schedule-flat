# 2018-07-17
 6:00 am | Steven Universe | Onion Friend
 6:15 am | Ben 10 | Fear The Fogg
 6:30 am | Amazing World of Gumball | The Helmet; The Fight
 7:00 am | Amazing World of Gumball | The Understanding; The Fuss
 7:30 am | Amazing World of Gumball | The News; The Vase
 8:00 am | Amazing World of Gumball | The Ad
 8:15 am | Teen Titans Go! | Movie Night
 8:30 am | Teen Titans Go! | BBRAE
 9:00 am | Amazing World of Gumball | The Potion
 9:15 am | Teen Titans Go! | Sandwich Thief
 9:30 am | Teen Titans Go! | Friendship; Vegetables
10:00 am | Amazing World of Gumball | The Spinoffs
10:15 am | Amazing World of Gumball | The Origins
10:30 am | Amazing World of Gumball | The Origins Part 2; The Traitor
11:00 am | Amazing World of Gumball | The Transformation
11:15 am | Summer Camp Island | Saxophone Come Home
11:30 am | Summer Camp Island | Pajama Pajimjams; Oscar & Hedgehog's Melody
12:00 pm | Amazing World of Gumball | The Understanding
12:15 pm | Teen Titans Go! | 40%, 40%, 20%
12:30 pm | Teen Titans Go! | Grube's Fairytales; A Farce
 1:00 pm | Amazing World of Gumball | The Ad
 1:15 pm | Unikitty | Little Prince Puppycorn
 1:30 pm | Unikitty | Stuck Together; Hide N' Seek
 2:00 pm | Amazing World of Gumball | The Potion
 2:15 pm | We Bare Bears | The Island
 2:30 pm | We Bare Bears | Bear Flu; Baby Bears On A Plane
 3:00 pm | Amazing World of Gumball | The Spinoffs
 3:15 pm | Summer Camp Island | Mr. Softball
 3:30 pm | Summer Camp Island | Fuzzy Pink Time Babies; Time Traveling Quick Pants
 4:00 pm | Amazing World of Gumball | The Transformation
 4:15 pm | Craig of the Creek | Kelsey Quest
 4:30 pm | Craig of the Creek | Jpony; Itch To Explore
 5:00 pm | Amazing World of Gumball | The Understanding; The Parents
 5:30 pm | Amazing World of Gumball | The Parents; The Candidate
 6:00 pm | Amazing World of Gumball | The Ad
 6:15 pm | Teen Titans Go! | Dude Relax!
 6:30 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 7:00 pm | Amazing World of Gumball | The Potion
 7:15 pm | We Bare Bears | Bunnies
 7:30 pm | We Bare Bears | Panda 2; Icy Nights II