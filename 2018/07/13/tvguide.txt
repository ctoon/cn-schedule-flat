# 2018-07-13
 6:00 am | Steven Universe | Cry for Help
 6:15 am | Ben 10 | Xingo's Back
 6:30 am | Amazing World of Gumball | The Car; The Curse
 7:00 am | Craig of the Creek | Vulture's Nest
 7:15 am | Amazing World of Gumball | The Vision
 7:30 am | Amazing World of Gumball | The Choices; The Code
 8:00 am | Craig of the Creek | Kelsey Quest
 8:15 am | Teen Titans Go! | Fish Water
 8:30 am | Teen Titans Go! | TV Knight; BBSFBDAY!
 9:00 am | Craig of the Creek | Jpony
 9:15 am | Teen Titans Go! | Brian
 9:30 am | Teen Titans Go! | Salty Codgers; Knowledge
10:00 am | Craig of the Creek | Under the Overpass
10:15 am | Amazing World of Gumball | The Parking
10:30 am | Amazing World of Gumball | The Comic; The Upgrade
11:00 am | Craig of the Creek | The Invitation
11:15 am | Amazing World of Gumball | The Cage
11:30 am | Amazing World of Gumball | The Rival; The One
12:00 pm | Craig of the Creek | Vulture's Nest
12:15 pm | Teen Titans Go! | The Croissant
12:30 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 1:00 pm | Craig of the Creek | Kelsey Quest
 1:15 pm | Unikitty | Too Many Unikittys
 1:30 pm | Unikitty | The Zone; Lab Cat
 2:00 pm | Craig of the Creek | Jpony
 2:15 pm | We Bare Bears | Hibernation
 2:30 pm | We Bare Bears | Bear Cleanse/Our Stuff
 3:00 pm | Craig of the Creek | Under the Overpass
 3:15 pm | Summer Camp Island | It's My Party
 3:30 pm | Summer Camp Island | Moon Problems; Monster Visit
 4:00 pm | Craig of the Creek | The Invitation
 4:15 pm | Teen Titans Go! | TV Knight
 5:00 pm | Craig of the Creek | Vulture's Nest
 5:15 pm | Amazing World of Gumball | The Neighbor
 5:30 pm | Amazing World of Gumball | The Pact; The Faith
 6:00 pm | Craig of the Creek | Kelsey Quest
 6:15 pm | Teen Titans Go! | Chicken in the Cradle
 6:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 7:00 pm | Craig of the Creek | Jpony
 7:15 pm | We Bare Bears | $100
 7:30 pm | We Bare Bears | The Fair; Grizzly the Movie