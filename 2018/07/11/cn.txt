# 2018-07-11
 6:00 am | Steven Universe | We Need to Talk
 6:15 am | Ben 10 | High Stress Express
 6:30 am | Amazing World of Gumball | Ape/The Poltergeist
 7:00 am | Craig of the Creek | The Invitation
 7:15 am | Amazing World of Gumball | The Compilation
 7:30 am | Amazing World of Gumball | Disaster/The Re-Run
 8:00 am | Craig of the Creek | Vulture's Nest
 8:15 am | SPECIAL | Teen Titans Go!: TV Knight
 9:00 am | Craig of the Creek | Kelsey Quest
 9:15 am | Teen Titans Go! | Dreams
 9:30 am | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
10:00 am | Craig of the Creek | JPony
10:15 am | Amazing World of Gumball | The Apprentice
10:30 am | Amazing World of Gumball | Check/The Pest
11:00 am | Craig of the Creek | Under the Overpass
11:15 am | Amazing World of Gumball | The Deal
11:30 am | Amazing World of Gumball | Line/The Singing
12:00 pm | Craig of the Creek | The Invitation
12:15 pm | Teen Titans Go! | The Return of Slade
12:30 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob/Some of Their Parts
 1:00 pm | Craig of the Creek | Vulture's Nest
 1:15 pm | Unikitty | Buggin' Out
 1:30 pm | Unikitty | Chair/R & Arr
 2:00 pm | Craig of the Creek | Kelsey Quest
 2:15 pm | SPECIAL | We Bare Bears: The Stack Life
 3:00 pm | Craig of the Creek | JPony
 3:15 pm | Summer Camp Island | Feeling Spacey
 3:30 pm | Summer Camp Island | Ghost the Boy/Computer Vampire
 4:00 pm | Craig of the Creek | Under the Overpass
 4:15 pm | Teen Titans Go! | The Scoop
 4:30 pm | Teen Titans Go! | Streak Part 1/The Streak Part 2
 5:00 pm | Craig of the Creek | The Invitation
 5:15 pm | Amazing World of Gumball | The Cage
 5:30 pm | Amazing World of Gumball | Rival/The One
 6:00 pm | Craig of the Creek | Vulture's Nest
 6:15 pm | SPECIAL | Teen Titans Go!: TV Knight
 7:00 pm | Craig of the Creek | Kelsey Quest
 7:15 pm | We Bare Bears | Icy Nights
 7:30 pm | We Bare Bears | Captain Craboo