# 2018-07-20
 8:00 pm | Dragon Ball Super | Valiant Hercule, Work a Miracle! A Challenge from Outer Space
 8:30 pm | Cleveland Show  | Cleveland Jr.'s Cherry Bomb
 9:00 pm | Cleveland Show  | Ladies' Night
 9:30 pm | American Dad | Lost in Space
10:00 pm | American Dad | Da Flippity Flop
10:30 pm | Bob's Burgers | Gene It On
11:00 pm | Family Guy | The Juice Is Loose
11:30 pm | Family Guy | Fox-y Lady
12:00 am | Rick and Morty | The Wedding Squanchers
12:30 am | Venture Brothers  | Bot Seeks Bot
 1:00 am | Robot Chicken | No Country for Old Dogs
 1:15 am | Mostly 4 Millennials | Interfacing
 1:30 am | Bob's Burgers | Gene It On
 2:00 am | Family Guy | The Juice Is Loose
 2:30 am | Family Guy | Fox-y Lady
 3:00 am | American Dad | Lost in Space
 3:30 am | American Dad | Da Flippity Flop
 4:00 am | FishCenter | Williams Stream 262
 4:15 am | Digikiss | Williams Stream 263
 4:30 am | Rick and Morty | The Wedding Squanchers
 5:00 am | Robot Chicken | The Godfather of the Bride II
 5:15 am | Robot Chicken | Casablankman II
 5:30 am | Cleveland Show  | Ladies' Night