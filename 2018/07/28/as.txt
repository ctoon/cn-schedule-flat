# 2018-07-28
 8:00 pm | Cleveland Show  | Gone with the Wind
 8:30 pm | Family Guy | Jerome Is the New Black
 9:00 pm | Rick and Morty | Rick Potion #9
 9:30 pm | Rick and Morty | Raising Gazorpazorp
10:00 pm | Family Guy | Dog Gone
10:30 pm | Dragon Ball Super | Gohan's Plight! The Preposterous Great Saiyaman Film Adaptation?!
11:00 pm | My Hero Academia | All Might
11:30 pm | FLCL: Progressive | Stone Skipping
12:00 am | Pop Team Epic | Donca * Sis
12:30 am | JoJo's Bizarre Adventure: Stardust Crusaders | Dio's World Part 2
 1:00 am | Hunter x Hunter | Resolve x and x Awakening
 1:30 am | Black Clover | Three-Leaf Sprouts
 2:00 am | Naruto:Shippuden | Sakura's Resolve
 2:30 am | Space Dandy | Dandy's Day in Court, Baby
 3:00 am | Cowboy Bebop | Black Dog Serenade
 3:30 am | Lupin The 3rd Part 4 | Welcome to the Haunted Hotel
 4:00 am | Rick and Morty | Rick Potion #9
 4:30 am | Rick and Morty | Raising Gazorpazorp
 5:00 am | Cleveland Show  | Brotherly Love
 5:30 am | Cleveland Show  | Gone with the Wind