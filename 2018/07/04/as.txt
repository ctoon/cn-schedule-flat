# 2018-07-04
 8:00 pm | Dragon Ball Super | Where Does the Dream Pick Up?! Find the Super Saiyan God!
 8:30 pm | American Dad | The Wrestler
 9:00 pm | Cleveland Show  | 'Tis The Cleveland To Be Sorry
 9:30 pm | American Dad | Dr. Klaustus
10:00 pm | Bob's Burgers | Boyz 4 Now
10:30 pm | Bob's Burgers | Carpe Museum
11:00 pm | Family Guy | Boys Do Cry
11:30 pm | Family Guy | No Chris Left Behind
12:00 am | Rick and Morty | Something Ricked This Way Comes
12:30 am | Venture Brothers  | Everybody Comes to Hank's
 1:00 am | Robot Chicken | Please Do Not Notify Our Contractors
 1:15 am | Hot Package | Pilot
 1:30 am | Bob's Burgers | Boyz 4 Now
 2:00 am | Bob's Burgers | Carpe Museum
 2:30 am | Family Guy | Boys Do Cry
 3:00 am | Family Guy | No Chris Left Behind
 3:30 am | American Dad | The Wrestler
 4:00 am | Infomercials | Food: The Source of Life
 4:15 am | Infomercials | Live at the Necropolis: Lords of Synth
 4:30 am | Rick and Morty | Something Ricked This Way Comes
 5:00 am | Robot Chicken | Vegetable Fun Fest
 5:15 am | Robot Chicken | The Deep End
 5:30 am | Cleveland Show  | 'Tis The Cleveland To Be Sorry