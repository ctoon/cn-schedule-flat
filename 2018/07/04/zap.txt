# 2018-07-04
 6:00 am | Steven Universe | Love Letters
 6:15 am | Ben 10 | Screamcatcher
 6:30 am | Amazing World of Gumball | The Robot; The Picnic
 7:00 am | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
 7:15 am | Amazing World of Gumball | The Advice
 7:30 am | Amazing World of Gumball | The Signal
 7:45 am | Amazing World of Gumball | The Girlfriend
 8:00 am | OK K.O.! Let's Be Heroes | Bittersweet Rivals
 8:15 am | Teen Titans Go! | Pyramid Scheme
 8:30 am | Teen Titans Go! | Finally a Lesson
 8:45 am | Teen Titans Go! | Arms Race With Legs
 9:00 am | OK K.O.! Let's Be Heroes | Are You Ready for Some Megafootball?!
 9:15 am | Teen Titans Go! | Starfire the Terrible
 9:30 am | Teen Titans Go! | Power Moves; Staring at the Future
10:00 am | OK K.O.! Let's Be Heroes | Mystery Sleepover
10:15 am | Amazing World of Gumball | The Oracle
10:30 am | Amazing World of Gumball | The Friend; The Saint
11:00 am | OK K.O.! Let's Be Heroes | Special Delivery
11:15 am | Amazing World of Gumball | The Cycle
11:30 am | Amazing World of Gumball | The Stars
11:45 am | Amazing World of Gumball | The Box
12:00 pm | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
12:15 pm | Teen Titans Go! | Robin Backwards
12:30 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 1:00 pm | OK K.O.! Let's Be Heroes | Bittersweet Rivals
 1:15 pm | Unikitty | Tasty Heist
 1:30 pm | Unikitty | Lab Cat
 1:45 pm | Unikitty | A Rock Friend Indeed
 2:00 pm | OK K.O.! Let's Be Heroes | Are You Ready for Some Megafootball?!
 2:15 pm | We Bare Bears | Beehive
 2:30 pm | We Bare Bears | Baby Bears Can't Jump
 2:45 pm | We Bare Bears | Tubin
 3:00 pm | OK K.O.! Let's Be Heroes | Mystery Sleepover
 3:15 pm | Teen Titans Go! | TV Knight 3
 3:30 pm | Teen Titans Go! | BBCYFSHIPBDAY
 3:45 pm | Teen Titans Go! | Throne of Bones
 4:00 pm | OK K.O.! Let's Be Heroes | Special Delivery
 4:15 pm | Craig of the Creek | Escape From Family Dinner
 4:30 pm | Craig of the Creek | Sunday Clothes
 4:45 pm | Craig of the Creek | Monster in the Garden
 5:00 pm | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
 5:15 pm | Amazing World of Gumball | The Ex
 5:30 pm | Amazing World of Gumball | The Weirdo
 5:45 pm | Amazing World of Gumball | The Menu
 6:00 pm | OK K.O.! Let's Be Heroes | Bittersweet Rivals
 6:15 pm | Teen Titans Go! | BL4Z3
 6:30 pm | Teen Titans Go! | Lication
 6:45 pm | Teen Titans Go! | Classic Titans
 7:00 pm | OK K.O.! Let's Be Heroes | Are You Ready for Some Megafootball?!
 7:15 pm | OK K.O.! Let's Be Heroes | TKO's House
 7:30 pm | Steven Universe | The Question
 7:45 pm | Steven Universe | Cat Fingers