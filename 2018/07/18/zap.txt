# 2018-07-18
 6:00 am | Steven Universe | Historical Friction
 6:15 am | Ben 10 | Super-Villain Team-Up
 6:30 am | Amazing World of Gumball | The End; The DVD
 7:00 am | Amazing World of Gumball | The Spinoffs
 7:15 am | Amazing World of Gumball | The Ollie
 7:30 am | Amazing World of Gumball | The Potato
 7:45 am | Amazing World of Gumball | The Sorcerer
 8:00 am | Amazing World of Gumball | The Transformation
 8:15 am | Teen Titans Go! | Permanent Record
 8:30 am | Teen Titans Go! | Titan Saving Time
 8:45 am | Teen Titans Go! | Master Detective
 9:00 am | Amazing World of Gumball | The Understanding
 9:15 am | Teen Titans Go! | Serious Business
 9:30 am | Teen Titans Go! | The Mask; Slumber Party
10:00 am | Amazing World of Gumball | The Ad
10:15 am | Amazing World of Gumball | The Advice
10:30 am | Amazing World of Gumball | The Signal
10:45 am | Amazing World of Gumball | The Girlfriend
11:00 am | Amazing World of Gumball | The Potion
11:15 am | Summer Camp Island | Feeling Spacey
11:30 am | Summer Camp Island | Ghost the Boy
11:45 am | Summer Camp Island | Computer Vampire
12:00 pm | Amazing World of Gumball | The Spinoffs
12:15 pm | Teen Titans Go! | Animals: It's Just a Word!
12:30 pm | Teen Titans Go! | BBB Day!
12:45 pm | Teen Titans Go! | Squash & Stretch
 1:00 pm | Amazing World of Gumball | The Transformation
 1:15 pm | Unikitty | Wishing Well
 1:30 pm | Unikitty | Crushing Defeat
 1:45 pm | Unikitty | Kitchen Chaos
 2:00 pm | Amazing World of Gumball | The Understanding
 2:15 pm | We Bare Bears | The Audition
 2:30 pm | We Bare Bears | Captain Craboo
 3:00 pm | Amazing World of Gumball | The Ad
 3:15 pm | Summer Camp Island | The First Day
 3:30 pm | Summer Camp Island | Monster Babies
 3:45 pm | Summer Camp Island | Chocolate Money Badgers
 4:00 pm | Amazing World of Gumball | The Potion
 4:15 pm | Craig of the Creek | You're It
 4:30 pm | Craig of the Creek | Jessica Goes to the Creek
 4:45 pm | Craig of the Creek | The Final Book
 5:00 pm | Amazing World of Gumball | The Spinoffs
 5:15 pm | Amazing World of Gumball | The Founder
 5:30 pm | Amazing World of Gumball | The Code
 5:45 pm | Amazing World of Gumball | The Test
 6:00 pm | Amazing World of Gumball | The Transformation
 6:15 pm | Teen Titans Go! | Hey Pizza!
 6:30 pm | Teen Titans Go! | Girls' Night Out; You're Fired
 7:00 pm | Amazing World of Gumball | The Understanding
 7:15 pm | We Bare Bears | The Kitty
 7:30 pm | We Bare Bears | Dog Hotel
 7:45 pm | We Bare Bears | Beehive