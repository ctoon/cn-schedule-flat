# 2018-07-05
 6:00 am | Steven Universe | Reformed
 6:15 am | Ben 10 | Creature Feature
 6:30 am | Amazing World of Gumball | Goons/The Secret
 7:00 am | OK K.O.! Let's Be Heroes | Mystery Sleepover
 7:15 am | Amazing World of Gumball | The Parasite
 7:30 am | Amazing World of Gumball | Love/The Awkwardness
 8:00 am | OK K.O.! Let's Be Heroes | Special Delivery
 8:15 am | Teen Titans Go! | Obinray
 8:30 am | Teen Titans Go! | Wally T/Rad Dudes with Bad Tudes
 9:00 am | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
 9:15 am | Teen Titans Go! | No Power
 9:30 am | Teen Titans Go! | Caged Tiger/Nose Mouth
10:00 am | OK K.O.! Let's Be Heroes | Bittersweet Rivals
10:15 am | Amazing World of Gumball | The Society
10:30 am | Amazing World of Gumball | Countdown/The Nobody
11:00 am | OK K.O.! Let's Be Heroes | Are You Ready for Some Megafootball?!
11:15 am | Amazing World of Gumball | The Matchmaker
11:30 am | Amazing World of Gumball | Grades/The Diet
12:00 pm | OK K.O.! Let's Be Heroes | Mystery Sleepover
12:15 pm | Teen Titans Go! | Yearbook Madness
12:30 pm | Teen Titans Go! | Rocks and Water/Multiple Trick Pony
 1:00 pm | OK K.O.! Let's Be Heroes | Special Delivery
 1:15 pm | Unikitty | Buggin' Out
 1:30 pm | Unikitty | Birthday Blowout/Fire & Nice
 2:00 pm | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
 2:15 pm | We Bare Bears | Bro Brawl
 2:30 pm | We Bare Bears | Hurricane Hal/The Park
 3:00 pm | OK K.O.! Let's Be Heroes | Bittersweet Rivals
 3:15 pm | Teen Titans Go! | Mo' Money Mo' Problems
 3:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 4:00 pm | OK K.O.! Let's Be Heroes | Are You Ready for Some Megafootball?!
 4:15 pm | Craig of the Creek | Dog Decider
 4:30 pm | Craig of the Creek | Curse/Bring Out Your Beast
 5:00 pm | OK K.O.! Let's Be Heroes | Mystery Sleepover
 5:15 pm | Amazing World of Gumball | The Uncle
 5:30 pm | Amazing World of Gumball | Heist/The Petals
 6:00 pm | OK K.O.! Let's Be Heroes | Special Delivery
 6:15 pm | Teen Titans Go! | Ones and Zeros
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:00 pm | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma/Red Action to the Future
 7:30 pm | Steven Universe | Made of Honor
 7:45 pm | Steven Universe | Serious Steven