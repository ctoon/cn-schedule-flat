# 2018-07-24
 6:00 am | Steven Universe | Catch and Release
 6:15 am | Ben 10 | King Koil
 6:30 am | Amazing World of Gumball | Phone/The Job
 7:00 am | Unikitty | Tragic Magic
 7:15 am | Amazing World of Gumball | The Diet
 7:30 am | Amazing World of Gumball | Ex/The Weirdo
 8:00 am | Unikitty | Dancer Danger
 8:15 am | Teen Titans Go! | Ones and Zeros
 8:30 am | Teen Titans Go! | Flashback
 9:00 am | Unikitty | Brawl Bot
 9:15 am | SPECIAL | The Teen Titans Go! Hollywood Special
10:00 am | Unikitty | Beach Daze
10:15 am | Amazing World of Gumball | The Blame
10:30 am | Amazing World of Gumball | Detective/The Fury
11:00 am | Unikitty | Big Pup, Little Problem
11:15 am | Summer Camp Island | Mr. Softball
11:30 am | Summer Camp Island | Fuzzy Pink Time Babies/The First Day
12:00 pm | Unikitty | Tragic Magic
12:15 pm | SPECIAL | Teen Titans Go!: League of Legs
 1:00 pm | Unikitty | Dancer Danger
 1:15 pm | Teen Titans Go! | Mo' Money Mo' Problems
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:00 pm | Unikitty | Brawl Bot
 2:15 pm | SPECIAL | We Bare Bears: Cave Chaos
 3:00 pm | Unikitty | Beach Daze
 3:15 pm | Summer Camp Island | It's My Party
 3:30 pm | Summer Camp Island | Moon Problems/Monster Visit
 4:00 pm | Unikitty | Big Pup, Little Problem
 4:15 pm | Craig of the Creek | The Future is Cardboard
 4:30 pm | Craig of the Creek | Under the Overpass/The Brood
 5:00 pm | Unikitty | Tragic Magic
 5:15 pm | SPECIAL | The Amazing World of Gumball: Flashback
 6:00 pm | Unikitty | Dancer Danger
 6:15 pm | SPECIAL | Teen Titans Go!: Makin' That Money
 7:00 pm | Unikitty | Brawl Bot
 7:15 pm | SPECIAL | The Teen Titans Go! Hollywood Special