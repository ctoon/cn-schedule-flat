# 2018-07-31
 6:00 am | Steven Universe | Steven's Birthday
 6:15 am | Ben 10 | All Koiled Up
 6:30 am | Amazing World of Gumball | The Authority
 6:45 am | Amazing World of Gumball | The Virus
 7:00 am | We Bare Bears | Lil' Squid
 7:15 am | Amazing World of Gumball | The Rival
 7:30 am | Amazing World of Gumball | The One
 7:45 am | Amazing World of Gumball | The Vegging
 8:00 am | We Bare Bears | I, Butler
 8:15 am | Teen Titans Go! | Mo' Money Mo' Problems
 8:30 am | Teen Titans Go! | The Streak, Part 1
 8:45 am | Teen Titans Go! | The Streak, Part 2
 9:00 am | We Bare Bears | Family Troubles
 9:15 am | Teen Titans Go! | The Scoop
 9:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:45 am | Teen Titans Go! | Operation Dude Rescue: Part 2
10:00 am | We Bare Bears | Go Fish
10:15 am | Amazing World of Gumball | The Copycats
10:30 am | Amazing World of Gumball | The Potato
10:45 am | Amazing World of Gumball | The Fuss
11:00 am | We Bare Bears | Teacher's Pet
11:15 am | Summer Camp Island | Computer Vampire
11:30 am | Summer Camp Island | The Basketball Liaries
11:45 am | Summer Camp Island | Popular Banana Split
12:00 pm | We Bare Bears | Googs
12:15 pm | Teen Titans Go! | Teen Titans Go! Hollywood Special
 1:00 pm | We Bare Bears | Paperboyz
 1:15 pm | Unikitty | Tragic Magic
 1:30 pm | Unikitty | Super Amazing Raft Adventure
 1:45 pm | Unikitty | R & Arr
 2:00 pm | We Bare Bears | Bear Squad
 2:15 pm | Teen Titans Go! | Boys vs. Girls
 2:30 pm | Teen Titans Go! | Road Trip
 2:45 pm | Teen Titans Go! | The Best Robin
 3:00 pm | We Bare Bears | Lil' Squid
 3:15 pm | OK K.O.! Let's Be Heroes | Wisdom, Strength, and Charisma
 3:30 pm | OK K.O.! Let's Be Heroes | My Fair Carol
 3:45 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 4:00 pm | We Bare Bears | I, Butler
 4:15 pm | Teen Titans Go! | Teen Titans Go! Hollywood Special
 5:00 pm | We Bare Bears | Family Troubles
 5:15 pm | Amazing World of Gumball | The Deal
 5:30 pm | Amazing World of Gumball | The Petals
 5:45 pm | Amazing World of Gumball | The Line
 6:00 pm | We Bare Bears | Go Fish
 6:15 pm | Teen Titans Go! | Mouth Hole
 6:30 pm | Teen Titans Go! | Robin Backwards
 6:45 pm | Teen Titans Go! | Crazy Day
 7:00 pm | We Bare Bears | Teacher's Pet
 7:15 pm | We Bare Bears | Viral Video
 7:30 pm | We Bare Bears | Our Stuff
 7:45 pm | We Bare Bears | Burrito