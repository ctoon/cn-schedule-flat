# 2018-07-31
 8:00 pm | Dragon Ball Super | Change! An Unexpected Return! His Name is Ginyu!!
 8:30 pm | American Dad | I Ain't No Holodeck Boy
 9:00 pm | Cleveland Show  | Harder, Better, Faster, Browner
 9:30 pm | American Dad | Stan Goes on the Pill
10:00 pm | Bob's Burgers | Late Afternoon in the Garden of Bob and Louise
10:30 pm | Bob's Burgers | Can't Buy Me Math
11:00 pm | Family Guy | Dial Meg for Murder
11:30 pm | Family Guy | Extra Large Medium
12:00 am | Rick and Morty | The Ricklantis Mixup
12:30 am | Venture Brothers  | Tanks for Nuthin'
 1:00 am | Robot Chicken | The Core, the Thief, His Wife and Her Lover
 1:15 am | Eric Andre Show | Stacey Dash; Jack McBrayer
 1:30 am | Bob's Burgers | Late Afternoon in the Garden of Bob and Louise
 2:00 am | Bob's Burgers | Can't Buy Me Math
 2:30 am | Family Guy | Dial Meg for Murder
 3:00 am | Family Guy | Extra Large Medium
 3:30 am | American Dad | I Ain't No Holodeck Boy
 4:00 am | Decker: Unsealed | Private Sector
 4:15 am | Check It Out! | Space
 4:30 am | Rick and Morty | The Ricklantis Mixup
 5:00 am | Robot Chicken | Robot Chicken's ATM Christmas Special
 5:15 am | Robot Chicken | Papercut to Aorta
 5:30 am | Cleveland Show  | Harder, Better, Faster, Browner