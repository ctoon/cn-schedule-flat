# 2018-07-02
 8:00 pm | Dragon Ball Super | A Peacetime Reward: Who Gets the 100,000,000 Zeni?
 8:30 pm | American Dad | The Unbrave One
 9:00 pm | Cleveland Show  | Turkey Pot Die
 9:30 pm | American Dad | Stanny Tendergrass
10:00 pm | Bob's Burgers | Two For Tina
10:30 pm | Bob's Burgers | It Snakes a Village
11:00 pm | Family Guy | The Tan Aquatic with Steve Zissou
11:30 pm | Family Guy | Airport '07
12:00 am | Rick and Morty | Raising Gazorpazorp
12:30 am | Venture Brothers  | Pomp & Circuitry
 1:00 am | Robot Chicken | The Ramblings of Maurice
 1:15 am | Hot Package | Romance
 1:30 am | Bob's Burgers | Two For Tina
 2:00 am | Bob's Burgers | It Snakes a Village
 2:30 am | Family Guy | The Tan Aquatic with Steve Zissou
 3:00 am | Family Guy | Airport '07
 3:30 am | American Dad | The Unbrave One
 4:00 am | Infomercials | Food: The Source of Life
 4:15 am | Infomercials | M.O.P.Z.
 4:30 am | Rick and Morty | Raising Gazorpazorp
 5:00 am | Robot Chicken | Junk in the Trunk
 5:15 am | Robot Chicken | Gold Dust Gasoline
 5:30 am | Cleveland Show  | Turkey Pot Die