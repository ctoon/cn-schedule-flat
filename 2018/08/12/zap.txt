# 2018-08-12
 6:00 am | Ben 10 | Bomzobo Lives
 6:15 am | Ben 10 | Animorphosis
 6:30 am | Mega Man: Fully Charged | Drilling Deep
 6:45 am | Mega Man: Fully Charged | Videodrone
 7:00 am | Teen Titans Go! | BBBDay!
 7:15 am | Teen Titans Go! | Squash & Stretch
 7:30 am | Teen Titans Go! | Love Monsters; Baby Hands
 8:00 am | Teen Titans Go! | BBRAE
 8:30 am | Teen Titans Go! | The Mask; Slumber Party
 9:00 am | Teen Titans Go! | TV Knight 2
 9:15 am | Teen Titans Go! | Garage Sale
 9:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
10:00 am | Teen Titans Go! | The Night Begins to Shine Special
11:00 am | Teen Titans Go! | Career Day
11:15 am | Teen Titans Go! | Secret Garden
11:30 am | Teen Titans Go! | Power Moves; Staring at the Future
12:00 pm | Amazing World of Gumball | The List
12:15 pm | Amazing World of Gumball | The Detective
12:30 pm | Amazing World of Gumball | The News
12:45 pm | Amazing World of Gumball | The Fury
 1:00 pm | We Bare Bears | Crowbar Jones: Origins
 1:15 pm | We Bare Bears | Hot Sauce
 1:30 pm | We Bare Bears | Mom App
 1:45 pm | Adventure Time | Diamonds and Lemons
 2:00 pm | Amazing World of Gumball | The Rival
 2:15 pm | Amazing World of Gumball | The Compilation
 2:30 pm | Amazing World of Gumball | The Lady
 2:45 pm | Amazing World of Gumball | The Stories
 3:00 pm | Summer Camp Island | Pajama Pajimjams
 3:15 pm | Summer Camp Island | Monster Visit
 3:30 pm | Summer Camp Island | Oscar & Hedghog's Melody
 3:45 pm | Summer Camp Island | Ice Cream Headache
 4:00 pm | The Powerpuff Girls | Total Eclipse of the Kart
 4:30 pm | Unikitty | Brawl Bot
 4:45 pm | Unikitty | Big Pup, Little Problem
 5:00 pm | Amazing World of Gumball | The Disaster
 5:15 pm | Amazing World of Gumball | The Re-Run
 5:30 pm | Amazing World of Gumball | The Vegging
 5:45 pm | Amazing World of Gumball | The Sucker
 6:00 pm | Craig of the Creek | Sunday Clothes
 6:15 pm | Craig of the Creek | JPony
 6:30 pm | Craig of the Creek | Escape From Family Dinner
 6:45 pm | Adventure Time | Diamonds and Lemons
 7:00 pm | We Bare Bears | Googs
 7:15 pm | We Bare Bears | Lil' Squid
 7:30 pm | We Bare Bears | Family Troubles
 7:45 pm | We Bare Bears | Best Bears