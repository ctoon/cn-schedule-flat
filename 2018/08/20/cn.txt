# 2018-08-20
 6:00 am | Steven Universe | Beach City Drift
 6:15 am | Amazing World of Gumball | The Helmet
 6:30 am | Amazing World of Gumball | The End/The DVD
 7:00 am | Amazing World of Gumball | The Butterfly/The Question
 7:30 am | Amazing World of Gumball | The Oracle/The Safety
 8:00 am | Teen Titans Go! | Pirates/I See You
 8:30 am | Teen Titans Go! | Brian/Nature
 9:00 am | Teen Titans Go! | History Lesson/BL4Z3
 9:30 am | Teen Titans Go! | Art of Ninjutsu/Hot Salad Water
10:00 am | Amazing World of Gumball | The Pressure/The Painting
10:30 am | Amazing World of Gumball | The Responsible/The Dress
11:00 am | Summer Camp Island | First Day/Monster Babies
11:30 am | Summer Camp Island | Chocolate Money Badgers/Saxophone Come Home
12:00 pm | Teen Titans Go! | Dude Relax!/Laundry Day
12:30 pm | Teen Titans Go! | Ghostboy/La Larva de Amor
 1:00 pm | We Bare Bears | Teacher's Pet/Googs
 1:30 pm | We Bare Bears | Go Fish/Best Bears
 2:00 pm | Unikitty | Buggin' Out/Crushing Defeat
 2:30 pm | Unikitty | Chair/Rock Friend
 3:00 pm | Ben 10 | Super-Villain Team-Up
 3:15 pm | OK K.O.! Let's Be Heroes | Hope This Flies
 3:30 pm | OK K.O.! Let's Be Heroes | Seasons Change/Special Delivery
 4:00 pm | Craig of the Creek | Ace of Squares/The Brood
 4:30 pm | Craig of the Creek | Itch to Explore/Under the Overpass
 5:00 pm | Amazing World of Gumball | Traitor/The Guy
 5:30 pm | Amazing World of Gumball | The Origins
 5:30 pm | Amazing World of Gumball | Wicked/The Hug
 5:30 pm | Amazing World of Gumball | The Origins Part 2
 6:00 pm | Teen Titans Go! | Permanent Record/Pyramid Scheme
 6:30 pm | Teen Titans Go! | Titan Saving Time/Bottle Episode
 7:00 pm | We Bare Bears | Lil' Squid/Crowbar Jones: Origins
 7:30 pm | We Bare Bears | Hot Sauce/Mom App