# 2018-08-23
 6:00 am | Steven Universe | Monster Reunion
 6:15 am | Amazing World of Gumball | The Treasure
 6:30 am | Amazing World of Gumball | The Words/The Apology
 7:00 am | Amazing World of Gumball | The Triangle/The Money
 7:30 am | Amazing World of Gumball | Return/The Nemesis
 8:00 am | Teen Titans Go! | Friendship/Vegetables
 8:30 am | Teen Titans Go! | The Mask/Slumber Party
 9:00 am | Teen Titans Go! | Snuggle Time/Oh Yeah!
 9:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:00 am | Amazing World of Gumball | The Robot/The Picnic
10:30 am | Amazing World of Gumball | The Goons/The Secret
11:00 am | Summer Camp Island | It's My Party/Moon Problems
11:30 am | Summer Camp Island | Monster Visit/Ice Cream Headache
12:00 pm | Teen Titans Go! | Meatball Party/Staff Meeting
12:30 pm | Teen Titans Go! | Terra-ized/Artful Dodgers
 1:00 pm | We Bare Bears | Food Truck/Primal
 1:30 pm | We Bare Bears | Everyday Bears/Nom Nom
 2:00 pm | Unikitty | Beach Daze/Birthday Blowout
 2:30 pm | Unikitty | Big Pup, Little Problem/Action Forest
 3:00 pm | Ben 10 | All Koiled Up
 3:15 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 3:30 pm | OK K.O.! Let's Be Heroes | Point to the Plaza/Are You Ready for Some Megafootball?!
 4:00 pm | Craig of the Creek | Climb/The Last Kid in the Creek
 4:30 pm | Craig of the Creek | Final Book/Kelsey Quest/Lost in the Sewer
 5:00 pm | Amazing World of Gumball | Parasite/The Slide
 5:30 pm | Amazing World of Gumball | Love/The Loophole
 6:00 pm | Teen Titans Go! | Orangins/Rad Dudes with Bad Tudes
 6:30 pm | Teen Titans Go! | Jinxed/Brain Percentages
 7:00 pm | We Bare Bears | Googs/Paperboyz
 7:30 pm | We Bare Bears | Bear Squad/Lil' Squid