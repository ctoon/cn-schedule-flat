# 2018-08-15
 8:00 pm | Dragon Ball Super | Surprise, 6th Universe! This Is Super Saiyan Goku!
 8:30 pm | American Dad | The Shrink
 9:00 pm | Cleveland Show  | Hot Cocoa Bang Bang
 9:30 pm | American Dad | Holy S***, Jeff's Back!
10:00 pm | Bob's Burgers | Sacred Couch
10:30 pm | Bob's Burgers | Lice Things Are Lice
11:00 pm | Family Guy | The Big Bang Theory
11:30 pm | Family Guy | Foreign Affairs
12:00 am | Rick and Morty | Rixty Minutes
12:30 am | Robot Chicken | Catch Me If You Kangaroo Jack
12:45 am | Robot Chicken | Beastmaster and Commander
 1:00 am | Squidbillies | Duel of the Dimwits
 1:15 am | Aqua Teen | Bible Fruit
 1:30 am | Bob's Burgers | Sacred Couch
 2:00 am | Bob's Burgers | Lice Things Are Lice
 2:30 am | Family Guy | The Big Bang Theory
 3:00 am | Family Guy | Foreign Affairs
 3:30 am | American Dad | The Shrink
 4:00 am | Apollo Gauntlet | Origin
 4:15 am | Check It Out! | Cars
 4:30 am | Rick and Morty | Rixty Minutes
 5:00 am | Robot Chicken | Walking Dead Lobster
 5:15 am | Robot Chicken | Victoria's Secret of NIMH
 5:30 am | Cleveland Show  | Hot Cocoa Bang Bang