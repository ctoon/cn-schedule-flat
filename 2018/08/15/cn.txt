# 2018-08-15
 6:00 am | Steven Universe | Mr. Greg
 6:15 am | Amazing World of Gumball | The Mustache
 6:30 am | Amazing World of Gumball | The Club/The Wand
 7:00 am | Amazing World of Gumball | The Law/The Allergy
 7:30 am | Amazing World of Gumball | The Mothers/The Password
 8:00 am | Teen Titans Go! | Brain Food/In and Out
 8:30 am | Teen Titans Go! | Little Buddies/Missing
 9:00 am | SPECIAL | Teen Titans Go!: To Work
 9:00 am | Teen Titans Go! | Hand Zombie
 9:30 am | Teen Titans Go! | Employee of the Month Redux
 9:45 am | Teen Titans Go! | Hand Zombie
10:00 am | Amazing World of Gumball | Rival/The Transformation
10:30 am | Amazing World of Gumball | Lady/The Spinoffs
11:00 am | Summer Camp Island | Computer Vampire/Chocolate Money Badgers/The Basketball Liaries
11:30 am | Summer Camp Island | Popular Banana Split/Time Traveling Quick Pants
12:00 pm | Teen Titans Go! | Classic Titans/TV Knight 3
12:30 pm | Teen Titans Go! | Ones and Zeros/The Scoop
 1:00 pm | We Bare Bears | Planet Bears/Bunnies
 1:30 pm | We Bare Bears | Panda 2/Panda's Art
 2:00 pm | Unikitty | Dancer Danger/The Zone
 2:30 pm | Unikitty | Too Many Unikittys/Stuck Together
 3:00 pm | Ben 10 | Half-Sies
 3:15 pm | OK K.O.! Let's Be Heroes | RMS & Brandon's First Episode
 3:30 pm | OK K.O.! Let's Be Heroes | T.K.O.
 4:00 pm | Craig of the Creek | Brood/Too Many Treasures
 4:30 pm | Craig of the Creek | Under the Overpass/Wildernessa
 5:00 pm | SPECIAL | The Amazing World of Gumball: Nicole's Fury
 5:00 pm | Amazing World of Gumball | Comic/The Detective
 5:30 pm | Amazing World of Gumball | Romantic/The Fury
 6:00 pm | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
 6:30 pm | Teen Titans Go! | Streak Part 1/The Streak Part 2
 7:00 pm | We Bare Bears | Rescue Ranger/Money Man
 7:30 pm | We Bare Bears | Go Fish/Teacher's Pet