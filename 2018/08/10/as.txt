# 2018-08-10
 8:00 pm | Dragon Ball Super | A Run-Through For the Competition! Who Are the Last Two Members?!
 8:30 pm | Cleveland Show  | Terry Unmarried
 9:00 pm | Cleveland Show  | The Blue and the Gray and the Brown
 9:30 pm | American Dad | Dreaming of a White Porsche Christmas
10:00 pm | American Dad | LGBSteve
10:30 pm | Bob's Burgers | Gayle Makin' Bob Sled
11:00 pm | Family Guy | Friends of Peter G.
11:30 pm | Family Guy | German Guy
12:00 am | Rick and Morty | Meeseeks and Destroy
12:30 am | Robot Chicken | Big Trouble in Little Clerks 2
12:45 am | Robot Chicken | Kramer vs. Showgirls
 1:00 am | Squidbillies | The Ballad of the Latrine Marine
 1:15 am | Aqua Teen | Laser Lenses
 1:30 am | Bob's Burgers | Gayle Makin' Bob Sled
 2:00 am | Family Guy | Friends of Peter G.
 2:30 am | Family Guy | German Guy
 3:00 am | American Dad | Dreaming of a White Porsche Christmas
 3:30 am | American Dad | LGBSteve
 4:00 am | As Seen on Adult Swim | Williams Stream 268
 4:15 am | Development Meeting | Williams Stream 269
 4:30 am | Rick and Morty | Meeseeks and Destroy
 5:00 am | Robot Chicken | Up Up and Buffet
 5:15 am | Robot Chicken | Panthropologie
 5:30 am | Cleveland Show  | The Blue and the Gray and the Brown