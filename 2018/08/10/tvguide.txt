# 2018-08-10
 6:00 am | Steven Universe | Hit the Diamond
 6:15 am | We Bare Bears | Paperboyz
 6:30 am | We Bare Bears | Vacation; Hurricane Hal
 7:00 am | Amazing World of Gumball | The Coach; The Joy
 7:30 am | Amazing World of Gumball | The Recipe; The Puppy
 8:00 am | Teen Titans Go! | Power Moves; Staring at the Future
 8:30 am | Teen Titans Go! | No Power; Sidekick
 9:00 am | Teen Titans Go! | Movie Night; Garage Sale
 9:30 am | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
10:00 am | Amazing World of Gumball | The Worst; The Shippening
10:30 am | Amazing World of Gumball | The Deal; The Brain
11:00 am | Summer Camp Island | Pepper's Blanket Is Missing; Hedgehog Werewolf
11:30 am | Summer Camp Island | Mr. Softball; Fuzzy Pink Time Babies
12:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
12:30 pm | Teen Titans Go! | Jinxed; BBCYFSHIPBDAY
 1:00 pm | We Bare Bears | Occupy Bears; Yard Sale
 1:30 pm | We Bare Bears | Charlie Ball; Bear Cleanse
 2:00 pm | Unikitty | Kickflip McPuppycorn; Hide N' Seek
 2:30 pm | Unikitty | Super Amazing Raft Adventure; Tasty Heist
 3:00 pm | Ben 10 | Screamcatcher
 3:15 pm | OK K.O.! Let's Be Heroes | Back in Red Action
 3:30 pm | OK K.O.! Let's Be Heroes | Face Your Fears; Let's Take a Moment
 4:00 pm | Craig of the Creek | Monster in the Garden; Kelsey Quest
 4:30 pm | Craig of the Creek | The Curse; Jpony
 5:00 pm | Amazing World of Gumball | The Sale; The Pest
 5:30 pm | Amazing World of Gumball | The Console; The Points
 6:00 pm | Adventure Time | Diamonds and Lemons
 6:15 pm | The Teen Titans Go! Hollywood | 
 7:00 pm | We Bare Bears | The Limo; Mom App
 7:30 pm | We Bare Bears | Family Troubles; Best Bears