# 2018-08-29
 6:00 am | Steven Universe | Letters to Lars
 6:15 am | Amazing World of Gumball | The Hero
 6:30 am | Amazing World of Gumball | The Tag; The Lesson
 7:00 am | Amazing World of Gumball | The Routine
 7:15 am | Amazing World of Gumball | The Cycle
 7:30 am | Amazing World of Gumball | The Upgrade
 7:45 am | Amazing World of Gumball | The Stars
 8:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
 8:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | The Streak, Part 1
 9:45 am | Teen Titans Go! | The Streak, Part 2
10:00 am | Amazing World of Gumball | The Microwave; The Meddler
10:30 am | Amazing World of Gumball | The Helmet; The Fight
11:00 am | Summer Camp Island | Pajama Pajimjams
11:15 am | Summer Camp Island | Monster Visit
11:30 am | Summer Camp Island | Oscar & Hedghog's Melody
11:45 am | Summer Camp Island | Ice Cream Headache
12:00 pm | Teen Titans Go! | Legs; Breakfast Cheese
12:30 pm | Teen Titans Go! | Waffles; Opposites
 1:00 pm | Adventure Time | My Two Favorite People; Memories of Boom Boom Mountain
 1:30 pm | Adventure Time | Wizard; Evicted!
 2:00 pm | Unikitty | A Rock Friend Indeed
 2:15 pm | Unikitty | Unikitty News!
 2:30 pm | Unikitty | Kitchen Chaos
 2:45 pm | Unikitty | Dinner Apart-y
 3:00 pm | Ben 10 | Ye Olde Laser Duel
 3:15 pm | OK K.O.! Let's Be Heroes | Sibling Rivalry
 3:30 pm | OK K.O.! Let's Be Heroes | I Am Dendy
 3:45 pm | OK K.O.! Let's Be Heroes | Do You Have Any More in the Back?
 4:00 pm | Amazing World of Gumball | The Roots
 4:15 pm | Amazing World of Gumball | The Box
 4:30 pm | Amazing World of Gumball | The Blame
 4:45 pm | Amazing World of Gumball | The Console
 5:00 pm | Craig of the Creek | The Last Kid in the Creek
 5:15 pm | Craig of the Creek | Doorway to Helen
 5:30 pm | Craig of the Creek | The Invitation
 5:45 pm | Craig of the Creek | Bring Out Your Beast
 6:00 pm | Teen Titans Go! | Who's Laughing Now
 6:15 pm | Teen Titans Go! | Lication
 6:30 pm | Teen Titans Go! | Oregon Trail
 6:45 pm | Teen Titans Go! | Ones and Zeroes
 7:00 pm | We Bare Bears | Bear Squad
 7:15 pm | We Bare Bears | Crowbar Jones: Origins
 7:30 pm | Amazing World of Gumball | The Line
 7:45 pm | Amazing World of Gumball | The Sucker