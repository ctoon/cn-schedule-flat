# 2018-08-29
 8:00 pm | Dragon Ball Super | Goku's Energy Is Out of Control?! The Struggle to Look After Pan
 8:30 pm | American Dad | Criss-Cross Applesauce: The Ballad of Billy Jesusworth
 9:00 pm | Cleveland Show  | Mama Drama
 9:30 pm | American Dad | Mine Struggle
10:00 pm | Bob's Burgers | Art Crawl
10:30 pm | Bob's Burgers | Spaghetti Western and Meatballs
11:00 pm | Family Guy | Tea Peter
11:30 pm | Family Guy | Family Guy Viewer Mail #2
12:00 am | Rick and Morty | Big Trouble in Little Sanchez
12:30 am | Robot Chicken | Collateral Damage in Gang Turf War
12:45 am | Robot Chicken | Eviscerated Post-Coital by Six Foot Mantis
 1:00 am | Harvey Birdman | Deadomutt, Pt. 2
 1:15 am | Aqua Teen | The Last Last One Forever and Ever
 1:30 am | Bob's Burgers | Art Crawl
 2:00 am | Bob's Burgers | Spaghetti Western and Meatballs
 2:30 am | Family Guy | Tea Peter
 3:00 am | Family Guy | Family Guy Viewer Mail #2
 3:30 am | American Dad | Criss-Cross Applesauce: The Ballad of Billy Jesusworth
 4:00 am | Off The Air | Love
 4:15 am | Off The Air | Paradise
 4:30 am | Rick and Morty | Big Trouble in Little Sanchez
 5:00 am | Robot Chicken | The Unnamed One
 5:15 am | Robot Chicken | Fridge Smell
 5:30 am | Cleveland Show  | Mama Drama