# 2018-08-29
 6:00 am | Steven Universe | Letters to Lars
 6:15 am | Amazing World of Gumball | The Hero
 6:30 am | Amazing World of Gumball | The Tag/The Lesson
 7:00 am | Amazing World of Gumball | Routine/The Cycle
 7:30 am | Amazing World of Gumball | Upgrade/The Stars
 8:00 am | Teen Titans Go! | Yearbook Madness/Hose Water
 8:30 am | Teen Titans Go! | Rocks and Water/Multiple Trick Pony
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | Streak Part 1/The Streak Part 2
10:00 am | Amazing World of Gumball | The Microwave/The Meddler
10:30 am | Amazing World of Gumball | The Helmet/The Fight
11:00 am | Summer Camp Island | Pajama Pajimjams/Monster Visit
11:30 am | Summer Camp Island | Oscar & Hedgehog's Melody/Ice Cream Headache
12:00 pm | Teen Titans Go! | Legs/Breakfast Cheese
12:30 pm | Teen Titans Go! | Waffles/Opposites
 1:00 pm | Adventure Time | My Two Favorite People/Memories of Boom Boom Mountain
 1:30 pm | Adventure Time | Wizard/Evicted!
 2:00 pm | Unikitty | Rock Friend/Unikitty News!
 2:30 pm | Unikitty | Kitchen Chaos/Dinner Apart-y
 3:00 pm | Ben 10 | Ye Olde Laser Duel
 3:15 pm | OK K.O.! Let's Be Heroes | Sibling Rivalry
 3:30 pm | OK K.O.! Let's Be Heroes | I Am Dendy/Do You Have Any More in the Back?
 4:00 pm | Amazing World of Gumball | Roots/The Box
 4:30 pm | Amazing World of Gumball | Blame/The Console
 5:00 pm | Craig of the Creek | The Last Kid in the Creek
 5:15 pm | Craig of the Creek | Doorway to Helen
 5:30 pm | Craig of the Creek | Invitation/Bring Out Your Beast
 6:00 pm | Teen Titans Go! | Who's Laughing Now/Lication
 6:30 pm | Teen Titans Go! | Oregon Trail/Ones and Zeros
 7:00 pm | We Bare Bears | Bear Squad/Crowbar Jones: Origins
 7:30 pm | Amazing World of Gumball | Line/The Sucker