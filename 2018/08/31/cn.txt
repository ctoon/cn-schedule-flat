# 2018-08-31
 6:00 am | Steven Universe | A Single Pale Rose
 6:15 am | Amazing World of Gumball | The Boombox
 6:30 am | Amazing World of Gumball | The Tape/The Sweaters
 7:00 am | Amazing World of Gumball | The Uploads/The Ex
 7:30 am | Amazing World of Gumball | The Apprentice/The Sorcerer
 8:00 am | Teen Titans Go! | Video Game References/Cool School
 8:30 am | Teen Titans Go! | Kicking a Ball and Pretending to Be Hurt/Head Fruit
 9:00 am | Teen Titans Go! | Movie Night/BBCYFSHIPBDAY
 9:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
10:00 am | Amazing World of Gumball | The Fridge/The Remote
10:30 am | Amazing World of Gumball | The Flower/The Banana
11:00 am | Summer Camp Island | Mr. Softball/Computer Vampire
11:30 am | Summer Camp Island | The Basketball Liaries/Fuzzy Pink Time Babies
12:00 pm | Teen Titans Go! | Little Buddies/Missing
12:30 pm | Teen Titans Go! | Uncle Jokes/Mas y Menos
 1:00 pm | Adventure Time | When Wedding Bells Thaw/Freak City
 1:30 pm | Adventure Time | The Duke/Donny
 2:00 pm | Unikitty | Buggin' Out/Hide N' Seek
 2:30 pm | Unikitty | Stuck Together/Chair
 3:00 pm | Ben 10 | Waterfilter
 3:15 pm | OK K.O.! Let's Be Heroes | Just Be a Pebble
 3:30 pm | OK K.O.! Let's Be Heroes | We've Got Pests/Presenting Joe Cuppa
 4:00 pm | Amazing World of Gumball | The Cycle/The Compilation
 4:30 pm | Amazing World of Gumball | The Stars/The Stories
 5:00 pm | Craig of the Creek | Big Pinchy
 5:15 pm | Craig of the Creek | The Climb
 5:30 pm | Craig of the Creek | The Curse/The Brood
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | Riding the Dragon/The Overbite
 7:00 pm | We Bare Bears | Best Bears/The Limo
 7:30 pm | Amazing World of Gumball | The One/The Deal