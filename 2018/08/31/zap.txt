# 2018-08-31
 6:00 am | Steven Universe | A Single Pale Rose
 6:15 am | Amazing World of Gumball | The Boombox
 6:30 am | Amazing World of Gumball | The Tape; The Sweaters
 7:00 am | Amazing World of Gumball | The Uploads
 7:15 am | Amazing World of Gumball | The Ex
 7:30 am | Amazing World of Gumball | The Apprentice
 7:45 am | Amazing World of Gumball | The Sorcerer
 8:00 am | Teen Titans Go! | Video Game References; Cool School
 8:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 9:00 am | Teen Titans Go! | Movie Night
 9:15 am | Teen Titans Go! | BBCYFSHIPBDAY
 9:30 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
10:00 am | Amazing World of Gumball | The Fridge; The Remote
10:30 am | Amazing World of Gumball | The Flower; The Banana
11:00 am | Summer Camp Island | Computer Vampire
11:15 am | Summer Camp Island | Mr. Softball
11:30 am | Summer Camp Island | The Basketball Liaries
11:45 am | Summer Camp Island | Fuzzy Pink Time Babies
12:00 pm | Teen Titans Go! | Little Buddies; Missing
12:30 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 1:00 pm | Adventure Time | When Wedding Bells Thaw; Freak City
 1:30 pm | Adventure Time | The Duke; Donny
 2:00 pm | Unikitty | Hide n' Seek
 2:15 pm | Unikitty | Bugging Out
 2:30 pm | Unikitty | Stuck Together
 2:45 pm | Unikitty | Chair
 3:00 pm | Ben 10 | Waterfilter
 3:15 pm | OK K.O.! Let's Be Heroes | Just Be a Pebble
 3:30 pm | OK K.O.! Let's Be Heroes | Presenting Joe Cuppa
 3:45 pm | OK K.O.! Let's Be Heroes | We've Got Pests
 4:00 pm | Amazing World of Gumball | The Compilation
 4:15 pm | Amazing World of Gumball | The Cycle
 4:30 pm | Amazing World of Gumball | The Stories
 4:45 pm | Amazing World of Gumball | The Stars
 5:00 pm | Craig of the Creek | Big Pinchy
 5:15 pm | Craig of the Creek | The Climb
 5:30 pm | Craig of the Creek | The Brood
 5:45 pm | Craig of the Creek | The Curse
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:30 pm | Teen Titans Go! | The Overbite
 6:45 pm | Teen Titans Go! | Riding the Dragon
 7:00 pm | We Bare Bears | Best Bears
 7:15 pm | We Bare Bears | The Limo
 7:30 pm | Amazing World of Gumball | The Deal
 7:45 pm | Amazing World of Gumball | The One
 7:45 pm | Amazing World of Gumball | The One