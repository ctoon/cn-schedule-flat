# 2018-08-02
 6:00 am | Steven Universe | Message Received
 6:15 am | Ben 10 | The Charm Offensive
 6:30 am | Amazing World of Gumball | The Dream; The Sidekick
 7:00 am | We Bare Bears | Family Troubles
 7:15 am | Amazing World of Gumball | The Pact
 7:30 am | Amazing World of Gumball | The Candidate; The Faith
 8:00 am | We Bare Bears | Go Fish
 8:15 am | Teen Titans Go! | Double Trouble
 8:30 am | Teen Titans Go! | Dude Relax!; Laundry Day
 9:00 am | We Bare Bears | Teacher's Pet
 9:15 am | Teen Titans Go! | The Croissant
 9:30 am | Teen Titans Go! | The Spice Game; I'm the Sauce
10:00 am | We Bare Bears | Googs
10:15 am | Amazing World of Gumball | The Box
10:30 am | Amazing World of Gumball | The Console; The Ollie
11:00 am | We Bare Bears | Paperboyz
11:15 am | Amazing World of Gumball | The Knights
11:30 am | Amazing World of Gumball | The Fridge; The Remote
12:00 pm | We Bare Bears | Bear Squad
12:15 pm | Teen Titans Go! | TV Knight
12:30 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 1:00 pm | We Bare Bears | Lil' Squid
 1:15 pm | Unikitty | Beach Daze
 1:30 pm | Unikitty | Chair; Unikitty News!
 2:00 pm | We Bare Bears | I, Butler
 2:15 pm | The Teen Titans Go! Hollywood | 
 3:00 pm | We Bare Bears | Family Troubles
 3:15 pm | Summer Camp Island | Monster Visit
 3:30 pm | Summer Camp Island | Ice Cream Headache; Pepper's Blanket Is Missing
 4:00 pm | We Bare Bears | Go Fish
 4:15 pm | Craig of the Creek | The Brood
 4:30 pm | Craig of the Creek | Under the Overpass; The Invitation
 5:00 pm | We Bare Bears | Teacher's Pet
 5:15 pm | Amazing World of Gumball | The Lady
 5:30 pm | Amazing World of Gumball | The Vegging; The Sucker
 6:00 pm | We Bare Bears | Googs
 6:15 pm | Teen Titans Go! | Video Game References
 6:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 7:00 pm | We Bare Bears | Paperboyz; Everyday Bears
 7:30 pm | We Bare Bears | Nom Nom; The Road