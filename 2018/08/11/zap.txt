# 2018-08-11
 6:00 am | Amazing World of Gumball | The Worst
 6:15 am | Amazing World of Gumball | The Night
 6:30 am | Amazing World of Gumball | The Deal
 6:45 am | Amazing World of Gumball | The Misunderstandings
 7:00 am | Teen Titans Go! | Bro-Pocalypse
 7:15 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 7:30 am | Teen Titans Go! | Dreams; Grandma Voice
 8:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 8:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 9:30 am | Teen Titans Go! | Mr. Butt; Man Person
10:00 am | Teen Titans Go! | Demon Prom
10:15 am | Teen Titans Go! | Grube's Fairytales
10:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
11:00 am | Teen Titans Go! | Throne of Bones
11:15 am | Teen Titans Go! | A Farce
11:30 am | Teen Titans Go! | Brian; Nature
12:00 pm | Ben 10 | Creature Feature
12:15 pm | Teen Titans Go! | The Academy
12:30 pm | Teen Titans Go! | Boys vs. Girls; Body Adventure
 1:00 pm | Teen Titans Go! | Chicken in the Cradle
 1:15 pm | LEGO DC Comics Superheroes: Justice League -- Gotham City Breakout | 
 3:00 pm | Adventure Time | Diamonds and Lemons
 3:15 pm | Adventure Time | Freak City
 3:30 pm | Adventure Time | The Duke; Donny
 4:00 pm | LEGO Ninjago: Masters of Spinjitzu: Hunted | Part 1
 6:00 pm | Craig of the Creek | Too Many Treasures
 6:15 pm | Craig of the Creek | Vulture's Nest
 6:30 pm | Craig of the Creek | Wildernessa
 6:45 pm | Craig of the Creek | Kelsey Quest
 7:00 pm | We Bare Bears | Crowbar Jones: Origins
 7:15 pm | We Bare Bears | Mom App
 7:30 pm | We Bare Bears | The Limo
 7:45 pm | We Bare Bears | Hot Sauce