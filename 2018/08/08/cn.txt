# 2018-08-08
 6:00 am | Steven Universe | Same Old World
 6:15 am | We Bare Bears | Teacher's Pet
 6:30 am | We Bare Bears | I Am Ice Bear/Baby Bears Can't Jump
 7:00 am | Amazing World of Gumball | The Tape/The Sweaters
 7:30 am | Amazing World of Gumball | The Internet/The Plan
 8:00 am | Teen Titans Go! | Terra-ized/Artful Dodgers
 8:30 am | Teen Titans Go! | Burger vs. Burrito/Matched
 9:00 am | Teen Titans Go! | BBBDay!/BBSFBDAY!
 9:30 am | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
10:00 am | Amazing World of Gumball | Menu/The Candidate
10:30 am | Amazing World of Gumball | Uncle/The Anybody
11:00 am | Summer Camp Island | Computer Vampire/The Basketball Liaries
11:30 am | Summer Camp Island | Popular Banana Split/Time Traveling Quick Pants
12:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:30 pm | Teen Titans Go! | Hand Zombie/Throne of Bones
 1:00 pm | We Bare Bears | Road/Shush Ninjas
 1:30 pm | We Bare Bears | Emergency/Cupcake Job
 2:00 pm | Unikitty | Rock Friend/R & Arr
 2:30 pm | Unikitty | License to Punch/Pet Pet
 3:00 pm | Ben 10 | Bon Voyage
 3:15 pm | OK K.O.! Let's Be Heroes | The Power Is Yours!
 3:30 pm | OK K.O.! Let's Be Heroes | We've Got Pests/Glory Days
 4:00 pm | Craig of the Creek | Too Many Treasures/The Brood
 4:30 pm | Craig of the Creek | Wildernessa/Under the Overpass
 5:00 pm | Amazing World of Gumball | Founder/The Schooling
 5:30 pm | Amazing World of Gumball | Intelligence/The Potion
 6:00 pm | Teen Titans Go! | Mo' Money Mo' Problems/TV Knight 3
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | We Bare Bears | Hot Sauce/Crowbar Jones: Origins
 7:30 pm | We Bare Bears | Paperboyz/Bear Squad