# 2018-08-17
 6:00 am | Steven Universe | The New Lars
 6:15 am | Amazing World of Gumball | The Car
 6:30 am | Amazing World of Gumball | The Microwave; The Meddler
 7:00 am | Amazing World of Gumball | The Bros; The Man
 7:30 am | Amazing World of Gumball | The Pizza; The Lie
 8:00 am | Teen Titans Go! | 
 8:45 am | Teen Titans Go! | Arms Race With Legs
 9:00 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:15 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 9:30 am | Teen Titans Go! | Brain Percentages
 9:45 am | Teen Titans Go! | Jinxed
10:00 am | Amazing World of Gumball | The One
10:15 am | Amazing World of Gumball | The Ad
10:30 am | Amazing World of Gumball | The Third; The Debt
11:00 am | Summer Camp Island | Feeling Spacey
11:15 am | Summer Camp Island | Monster Visit
11:30 am | Summer Camp Island | Ghost the Boy
11:45 am | Summer Camp Island | Ice Cream Headache
12:00 pm | Teen Titans Go! | Driver's Ed; Dog Hand
12:30 pm | Teen Titans Go! | Double Trouble; The Date
 1:00 pm | We Bare Bears | Bear Lift
 1:15 pm | We Bare Bears | Tubin'
 1:30 pm | We Bare Bears | Lil' Squid
 1:45 pm | We Bare Bears | Pigeons
 2:00 pm | Unikitty | Dinner Apart-y
 2:15 pm | Unikitty | Fire & Nice
 2:30 pm | Unikitty | R & Arr
 2:45 pm | Unikitty | Sparkle Matter Matters
 3:00 pm | Ben 10 | Bounty Ball
 3:15 pm | OK K.O.! Let's Be Heroes | No More Pow Cards
 3:30 pm | OK K.O.! Let's Be Heroes | Action News
 3:45 pm | OK K.O.! Let's Be Heroes | The Perfect Meal
 4:00 pm | Craig of the Creek | Kelsey Quest
 4:15 pm | Craig of the Creek | Monster in the Garden
 4:30 pm | Craig of the Creek | JPony
 4:45 pm | Craig of the Creek | The Curse
 5:00 pm | Amazing World of Gumball | The Origins
 5:15 pm | Amazing World of Gumball | The Origins
 5:30 pm | Amazing World of Gumball | The Disaster
 5:45 pm | Amazing World of Gumball | The Re-Run
 6:00 pm | Teen Titans Go! | Tower Renovation
 6:15 pm | Unikitty | Landlord Lord
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | We Bare Bears | Bear Squad
 7:15 pm | We Bare Bears | I, Butler
 7:30 pm | We Bare Bears | Family Troubles
 7:45 pm | We Bare Bears | Best Bears