# 2018-08-03
 8:00 pm | Dragon Ball Super | A Full-Throttle Battle! The Vengeful Golden Frieza
 8:30 pm | Cleveland Show  | It's the Great Pancake, Junior Brown
 9:00 pm | Cleveland Show  | Little Man on Campus
 9:30 pm | American Dad | News Glance with Genevieve Vavance
10:00 pm | American Dad | The Longest Distance Relationship
10:30 pm | Bob's Burgers | The Runway Club
11:00 pm | Family Guy | Brian and Stewie
11:30 pm | Family Guy | Quagmire's Dad
12:00 am | Rick and Morty | The Rickchurian Mortydate
12:30 am | Venture Brothers  | Red Means Stop
 1:00 am | Robot Chicken | Fool's Goldfinger
 1:15 am | Eric Andre Show | Jesse Williams; Jillian Michaels
 1:30 am | Bob's Burgers | The Runway Club
 2:00 am | Family Guy | Brian and Stewie
 2:30 am | Family Guy | Quagmire's Dad
 3:00 am | American Dad | News Glance with Genevieve Vavance
 3:30 am | American Dad | The Longest Distance Relationship
 4:00 am | As Seen on Adult Swim | Williams Stream 266
 4:15 am | FishCenter | Williams Stream 267
 4:30 am | Rick and Morty | The Rickchurian Mortydate
 5:00 am | Robot Chicken | Choked on a Bottle Cap
 5:15 am | Robot Chicken | Immortal
 5:30 am | Cleveland Show  | Little Man on Campus