# 2018-08-03
 6:00 am | Steven Universe | Log Date 7 15 2
 6:15 am | Ben 10 | Double Hex
 6:30 am | Amazing World of Gumball | The Hero; The Photo
 7:00 am | We Bare Bears | Bear Squad
 7:15 am | Amazing World of Gumball | The Brain
 7:30 am | Amazing World of Gumball | The Shippening; The Anybody
 8:00 am | We Bare Bears | Lil' Squid
 8:15 am | Teen Titans Go! | Ghost Boy
 8:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 9:00 am | We Bare Bears | I, Butler
 9:15 am | The Teen Titans Go! Hollywood | 
10:00 am | We Bare Bears | Family Troubles
10:15 am | Amazing World of Gumball | The Catfish
10:30 am | Amazing World of Gumball | The Cycle; The Stars
11:00 am | We Bare Bears | Go Fish
11:15 am | Summer Camp Island | Hedgehog Werewolf
11:30 am | Summer Camp Island | Mr. Softball; Fuzzy Pink Time Babies
12:00 pm | We Bare Bears | Teacher's Pet
12:15 pm | Teen Titans Go! | BBSFBDAY!
12:30 pm | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 1:00 pm | We Bare Bears | Googs
 1:15 pm | Unikitty | Brawl Bot
 1:30 pm | Unikitty | Buggin' Out; Film Fest
 2:00 pm | We Bare Bears | Paperboyz
 2:15 pm | Teen Titans Go! | Operation Tin Man
 2:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 3:00 pm | We Bare Bears | Bear Squad
 3:15 pm | The Teen Titans Go! Hollywood | 
 4:00 pm | We Bare Bears | Lil' Squid
 4:15 pm | Craig of the Creek | Vulture's Nest
 4:30 pm | Craig of the Creek | Kelsey Quest; Wildernessa
 5:00 pm | We Bare Bears | I, Butler
 5:15 pm | Amazing World of Gumball | The One
 5:30 pm | Amazing World of Gumball | The Cringe; The Father
 6:00 pm | We Bare Bears | Family Troubles
 6:15 pm | Teen Titans Go! | The Return of Slade
 6:30 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 7:00 pm | We Bare Bears | Go Fish
 7:15 pm | The Teen Titans Go! Hollywood | 