# 2018-08-25
 6:00 am | Amazing World of Gumball | The Shippening; The Potato
 6:30 am | Amazing World of Gumball | The Brain; The Fuss
 7:00 am | Teen Titans Go! | Master Detective; The Art of Ninjutsu
 7:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:00 am | Teen Titans Go! | Titan Saving Time; Think About Your Future
 8:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:00 am | Teen Titans Go! | Permanent Record; Booty Scooty
 9:30 am | Teen Titans Go! | Pirates; I See You
10:00 am | Teen Titans Go! | Road Trip; The Best Robin
10:30 am | Teen Titans Go! | Who's Laughing Now; Oregon Trail
11:00 am | Teen Titans Go! | Flashback
11:30 am | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
12:00 pm | Ben 10 | The 11th Alien
12:30 pm | Ninjago: Hunted Part 1 | 
 2:30 pm | Ninjago: Hunted Part 2 | 
 4:00 pm | Ninjago: Hunted Part 3 | 
 5:30 pm | Amazing World of Gumball | The Parents; The Outside
 6:00 pm | Amazing World of Gumball | The Schooling; The Matchmaker
 6:30 pm | Amazing World of Gumball | The Ad; The Transformation
 7:00 pm | Amazing World of Gumball | The Founder; The Vase
 7:30 pm | Amazing World of Gumball | The Origins; The Origins Part Two