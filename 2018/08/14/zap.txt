# 2018-08-14
 6:00 am | Steven Universe | Drop Beat Dad
 6:15 am | Amazing World of Gumball | The Goons
 6:30 am | Amazing World of Gumball | The Sock; The Genius
 7:00 am | Amazing World of Gumball | The Fraud; The Void
 7:30 am | Amazing World of Gumball | The Boss; The Move
 8:00 am | Teen Titans Go! | Waffles; Opposites
 8:30 am | Teen Titans Go! | Birds; Be Mine
 9:00 am | Teen Titans Go! | Bottle Episode
 9:15 am | Teen Titans Go! | Titan Saving Time
 9:30 am | Teen Titans Go! | Finally a Lesson
 9:45 am | Teen Titans Go! | Master Detective
10:00 am | Amazing World of Gumball | 
10:45 am | Amazing World of Gumball | The Schooling
11:00 am | Summer Camp Island | The First Day
11:15 am | Summer Camp Island | Computer Vampire
11:30 am | Summer Camp Island | Monster Babies
11:45 am | Summer Camp Island | The Basketball Liaries
12:00 pm | Teen Titans Go! | Flashback
12:30 pm | Teen Titans Go! | Lication
12:45 pm | Teen Titans Go! | Mo' Money Mo' Problems
 1:00 pm | We Bare Bears | The Fair
 1:15 pm | We Bare Bears | Grizzly the Movie
 1:30 pm | We Bare Bears | Road Trip
 1:45 pm | We Bare Bears | Coffee Cave
 2:00 pm | Unikitty | Big Pup, Little Problem
 2:15 pm | Unikitty | Birthday Blowout
 2:30 pm | Unikitty | Tragic Magic
 2:45 pm | Unikitty | Wishing Well
 3:00 pm | Ben 10 | High Stress Express
 3:15 pm | OK K.O.! Let's Be Heroes | Let's Watch the Pilot
 3:30 pm | OK K.O.! Let's Be Heroes | Second First Date
 3:45 pm | OK K.O.! Let's Be Heroes | Mystery Science Fair 201X
 4:00 pm | Craig of the Creek | Lost in the Sewer
 4:15 pm | Craig of the Creek | Jessica Goes to the Creek
 4:30 pm | Craig of the Creek | The Future Is Cardboard
 4:45 pm | Craig of the Creek | The Final Book
 5:00 pm | Amazing World of Gumball | The Routine
 5:15 pm | Amazing World of Gumball | The Roots
 5:30 pm | Amazing World of Gumball | The Upgrade
 5:45 pm | Amazing World of Gumball | The Blame
 6:00 pm | Teen Titans Go! | 
 6:45 pm | Teen Titans Go! | BBSFBDAY
 7:00 pm | We Bare Bears | Money Man
 7:15 pm | We Bare Bears | More Everyone's Tube
 7:30 pm | We Bare Bears | Mom App
 7:45 pm | We Bare Bears | The Limo