# 2018-08-01
 8:00 pm | Dragon Ball Super | Earth! Gohan! Both on the Ropes! Hurry and Get Here, Goku!!
 8:30 pm | American Dad | Honey, I'm Homeland
 9:00 pm | Cleveland Show  | Cleveland Live!
 9:30 pm | American Dad | She Swill Survive
10:00 pm | Bob's Burgers | The Millie-Churian Candidate
10:30 pm | Bob's Burgers | The Gayle Tales
11:00 pm | Family Guy | Go, Stewie, Go!
11:30 pm | Family Guy | Peter-assment
12:00 am | Rick and Morty | Morty's Mind Blowers
12:30 am | Venture Brothers  | It Happening One Night
 1:00 am | Robot Chicken | The Godfather of the Bride II
 1:15 am | Eric Andre Show | Howie Mandel; Malaysia Pargo
 1:30 am | Bob's Burgers | The Millie-Churian Candidate
 2:00 am | Bob's Burgers | The Gayle Tales
 2:30 am | Family Guy | Go, Stewie, Go!
 3:00 am | Family Guy | Peter-assment
 3:30 am | American Dad | Honey, I'm Homeland
 4:00 am | Decker: Unsealed | Same Old Glory
 4:15 am | Check It Out! | Animals
 4:30 am | Rick and Morty | Morty's Mind Blowers
 5:00 am | Robot Chicken | Caffeine-Induced Aneurysm
 5:15 am | Robot Chicken | Eaten by Cats
 5:30 am | Cleveland Show  | Cleveland Live!