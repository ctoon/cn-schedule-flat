# 2018-08-26
 8:00 pm | Cleveland Show  | American Prankster
 8:30 pm | Cleveland Show  | B.M.O.C.
 9:00 pm | Bob's Burgers | The Last Gingerbread House on the Left
 9:30 pm | American Dad | Shell Game
10:00 pm | Family Guy | Life of Brian
10:30 pm | Family Guy | In Harmony's Way
11:00 pm | Rick and Morty | Anatomy Park
11:30 pm | Robot Chicken | Why Is It Wet?
11:45 pm | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
12:00 am | Venture Brothers  | The High Cost of Loathing
12:30 am | Mike Tyson Mysteries | My Favorite Mystery
12:45 am | Hot Streets | The Egg
 1:00 am | Eric Andre Show | Jesse Williams; Jillian Michaels
 1:15 am | Mostly 4 Millennials | Interfacing
 1:30 am | Family Guy | Life of Brian
 2:00 am | Family Guy | In Harmony's Way
 2:30 am | Rick and Morty | Anatomy Park
 3:00 am | Robot Chicken | Why Is It Wet?
 3:15 am | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
 3:30 am | Venture Brothers  | The High Cost of Loathing
 4:00 am | Mike Tyson Mysteries | My Favorite Mystery
 4:15 am | Hot Streets | The Egg
 4:30 am | Eric Andre Show | Jesse Williams; Jillian Michaels
 4:45 am | Mostly 4 Millennials | Interfacing
 5:00 am | American Dad | Shell Game
 5:30 am | Bob's Burgers | The Last Gingerbread House on the Left