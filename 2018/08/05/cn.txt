# 2018-08-05
 6:00 am | Ben 10 | Ye Olde Laser Duel/Ben Again and Again
 6:30 am | Mega Man: Fully Charged | Throwing Shade Part 1/Throwing Shade Part 2
 7:00 am | Teen Titans Go! | I'm the Sauce/Hey You, Don't Forget About Me in Your Memory
 7:30 am | Teen Titans Go! | Birds/Be Mine
 8:00 am | Teen Titans Go! | Flashback
 8:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums/Video Game References
 9:00 am | Teen Titans Go! | Beast Girl/Accept the Next Proposition You Hear
 9:30 am | Teen Titans Go! | Little Buddies/Missing
10:00 am | We Bare Bears | Bear Squad/I, Butler
10:30 am | We Bare Bears | Lil' Squid/Family Troubles
11:00 am | SPECIAL | The Teen Titans Go! Hollywood Special
12:00 pm | Teen Titans | Final Exam
12:30 pm | Teen Titans | Divide and Conquer
 1:00 pm | Teen Titans | Forces of Nature
 1:30 pm | Teen Titans | Mask
 2:00 pm | Teen Titans | Apprentice Part 1
 2:30 pm | Teen Titans | Apprentice Part 2
 3:00 pm | Summer Camp Island | First Day/Popular Banana Split
 3:30 pm | Summer Camp Island | Monster Babies/Time Traveling Quick Pants
 4:00 pm | The Powerpuff Girls | Blundercup/Ragnarock and Roll
 4:30 pm | Unikitty | Tasty Heist/Beach Daze
 5:00 pm | Amazing World of Gumball | Heist/The Points
 5:30 pm | Amazing World of Gumball | Best/The Bus
 6:00 pm | Craig of the Creek | Jessica Goes to the Creek/Under the Overpass
 6:30 pm | Craig of the Creek | Final Book/The Invitation
 7:00 pm | We Bare Bears | Go Fish/Googs
 7:30 pm | We Bare Bears | Teacher's Pet/Paperboyz