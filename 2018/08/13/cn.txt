# 2018-08-13
 6:00 am | Steven Universe | Steven Floats
 6:15 am | Amazing World of Gumball | The Party
 6:30 am | Amazing World of Gumball | The Robot/The Picnic
 7:00 am | Amazing World of Gumball | The Name/The Extras
 7:30 am | Amazing World of Gumball | The Gripes/The Vacation
 8:00 am | Teen Titans Go! | Caged Tiger/Nose Mouth
 8:30 am | Teen Titans Go! | Legs/Breakfast Cheese
 9:00 am | Teen Titans Go! | BBRAE
 9:30 am | Teen Titans Go! | Pyramid Scheme/Permanent Record
10:00 am | Amazing World of Gumball | Petals/The Parents
10:30 am | Amazing World of Gumball | Line/The Founder
11:00 am | SPECIAL | Wacky Races: A Race Through Time
12:00 pm | Teen Titans Go! | Brain Percentages/Beast Girl
12:30 pm | Teen Titans Go! | BL4Z3/Hot Salad Water
 1:00 pm | We Bare Bears | Grizz Helps/Yuri and the Bear
 1:30 pm | We Bare Bears | Crowbar Jones/$100
 2:00 pm | Unikitty | Brawl Bot/Pet Pet
 2:30 pm | Unikitty | Beach Daze/Kitchen Chaos
 3:00 pm | Ben 10 | Assault on Pancake Palace
 3:15 pm | OK K.O.! Let's Be Heroes | Everybody Likes Rad?
 3:30 pm | SPECIAL | OK K.O.! Let's Be Heroes: Villains' Night
 4:00 pm | Craig of the Creek | Dog Decider/Itch to Explore
 4:30 pm | Craig of the Creek | Bring Out Your Beast/You're It
 5:00 pm | Amazing World of Gumball | Potion/The Spinoffs
 5:30 pm | Amazing World of Gumball | Understanding/The Ad
 6:00 pm | Teen Titans Go! | Tower Renovation
 6:15 pm | SPECIAL | The Teen Titans Go! Hollywood Special
 7:00 pm | We Bare Bears | More Everyone's Tube/The Limo
 7:30 pm | We Bare Bears | Crowbar Jones: Origins/Hot Sauce