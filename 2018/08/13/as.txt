# 2018-08-13
 8:00 pm | Dragon Ball Super | Off to See Master Zuno! Find Out Where the Super Dragon Balls Are!
 8:30 pm | American Dad | Morning Mimosa
 9:00 pm | Cleveland Show  | Back to Cool
 9:30 pm | American Dad | My Affair Lady
10:00 pm | Bob's Burgers | Nice-Capades
10:30 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
11:00 pm | Family Guy | Tiegs for Two
11:30 pm | Family Guy | Brothers and Sisters
12:00 am | Rick and Morty | Rick Potion #9
12:30 am | Robot Chicken | Malcolm X: Fully Loaded
12:45 am | Robot Chicken | Major League of Extraordinary Gentlemen
 1:00 am | Squidbillies | Debased Ball
 1:15 am | Aqua Teen | Dummy Love
 1:30 am | Bob's Burgers | Nice-Capades
 2:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 2:30 am | Family Guy | Tiegs for Two
 3:00 am | Family Guy | Brothers and Sisters
 3:30 am | American Dad | Morning Mimosa
 4:00 am | Decker: Mindwipe | Rock and a Hard Place
 4:15 am | Check It Out! | Children
 4:30 am | Rick and Morty | Rick Potion #9
 5:00 am | Robot Chicken | Catdog on a Stick
 5:15 am | Robot Chicken | Super Guitario Center
 5:30 am | Cleveland Show  | Back to Cool