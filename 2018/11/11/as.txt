# 2018-11-11
 8:00 pm | Amazing World of Gumball | Awareness/The Slip
 8:30 pm | Amazing World of Gumball | Drama/The Buddy
 9:00 pm | Family Guy | Road to Rupert
 9:30 pm | Bob's Burgers | The Silence of the Louise
10:00 pm | American Dad | Railroaded
10:30 pm | Family Guy | Our Idiot Brian
11:00 pm | Family Guy | This Little Piggy
11:30 pm | Rick and Morty | Mortynight Run
12:00 am | DREAM CORP LLC | Guys and Dads
12:15 am | DREAM CORP LLC | 88's Weekly
12:30 am | Venture Brothers  | Arrears in Science
 1:00 am | Squidbillies | Southern Pride and Prejudice
 1:15 am | Your Pretty Face is Going to Hell | Golden Fiddle Week
 1:30 am | American Dad | Railroaded
 2:00 am | Family Guy | Our Idiot Brian
 2:30 am | Family Guy | This Little Piggy
 3:00 am | Rick and Morty | Mortynight Run
 3:30 am | DREAM CORP LLC | Guys and Dads
 3:45 am | DREAM CORP LLC | 88's Weekly
 4:00 am | Venture Brothers  | Arrears in Science
 4:30 am | Squidbillies | Southern Pride and Prejudice
 4:45 am | Your Pretty Face is Going to Hell | Golden Fiddle Week
 5:00 am | Bob's Burgers | The Silence of the Louise
 5:30 am | Home Movies | Improving Your Life Through Improv