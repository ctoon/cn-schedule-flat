# 2018-11-03
 6:00 am | Teen Titans Go! | Power of Shrimps/Rad Dudes with Bad Tudes
 6:30 am | Transformers Cyberverse | Maccadam's
 6:45 am | Teen Titans Go! | My Name is Jose
 7:00 am | Teen Titans Go! | Tower Renovation/Obinray
 7:30 am | Teen Titans Go! | Arms Race with Legs/Finally a Lesson
 8:00 am | Amazing World of Gumball | Ad/The Choices
 8:30 am | Amazing World of Gumball | Understanding/The Vision
 9:00 am | Total DramaRama | A Ninjustice to Harold/Tiger Fail
 9:30 am | Total DramaRama | That's a Wrap/The Bad Guy Busters
10:00 am | Teen Titans Go! | Kabooms
10:30 am | Teen Titans Go! | Chicken in the Cradle/Bottle Episode
11:00 am | Teen Titans Go! | Scoop/Pyramid Scheme
11:30 am | Teen Titans Go! | TV Knight 3/The Cruel Giggling Ghoul
12:00 pm | Ben 10 | Bounty Ball
12:15 pm | Teen Titans Go! | Legendary Sandwich
12:30 pm | Teen Titans Go! | Driver's Ed/Dog Hand
 1:00 pm | Teen Titans Go! | Double Trouble/The Date
 1:30 pm | Teen Titans Go! | Dude Relax!/Laundry Day
 2:00 pm | Amazing World of Gumball | Transformation/The Boredom
 2:30 pm | Amazing World of Gumball | Spinoffs/The Guy
 3:00 pm | Amazing World of Gumball | Disaster/The Re-Run
 3:30 pm | Amazing World of Gumball | Intelligence/The Potion
 4:00 pm | Total DramaRama | A Ninjustice to Harold/The Date
 4:30 pm | Total DramaRama | Bad Guy Busters/Aquarium for a Dream
 5:00 pm | Amazing World of Gumball | Schooling/The Stories
 5:30 pm | Amazing World of Gumball | Founder/The Compilation
 6:00 pm | Amazing World of Gumball | Parents/The Fury
 6:30 pm | Amazing World of Gumball | Brain/The Detective
 7:00 pm | MOVIE | Night at the Museum: Secret of the Tomb