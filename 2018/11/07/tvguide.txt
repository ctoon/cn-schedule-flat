# 2018-11-07
 6:00 am | Amazing World of Gumball | The Worst; The Anybody
 6:30 am | Amazing World of Gumball | The Deal; The Pact
 7:00 am | Amazing World of Gumball | The Triangle; The Money
 7:30 am | Amazing World of Gumball | The Return; The Awkwardness
 8:00 am | Teen Titans Go! | Salty Codgers; Knowledge
 8:30 am | Teen Titans Go! | Baby Hands; Love Monsters
 9:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 9:30 am | Teen Titans Go! | Oregon Trail; Who's Laughing Now
10:00 am | Amazing World of Gumball | The Line; The Brain
10:30 am | Amazing World of Gumball | The List; The Parents
11:00 am | Amazing World of Gumball | The Others; The Bus
11:30 am | Amazing World of Gumball | The Signature; The Night
12:00 pm | Teen Titans Go! | The Mask; Slumber Party
12:30 pm | Teen Titans Go! | Serious Business; Thanksgiving
 1:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 1:30 pm | Amazing World of Gumball | The Internet; The Plan
 2:00 pm | Teen Titans Go! | Riding the Dragon; The Overbite
 2:30 pm | Teen Titans Go! | Flashback
 3:00 pm | Ben 10 | Safari Sa'Bad
 3:15 pm | Amazing World of Gumball | The Coach
 3:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 4:00 pm | Amazing World of Gumball | The Girlfriend; The Diet
 4:30 pm | Amazing World of Gumball | The Advice; The Ex
 5:00 pm | Craig of the Creek | Dinner At the Creek; The Takeout Mission
 5:30 pm | Total DramaRama | Cuttin' Corners; Tiger Fail
 6:00 pm | Teen Titans Go! | The Fight; Quantum Fun
 6:30 pm | Teen Titans Go! | Arms Race With Legs; Ones and Zeros
 7:00 pm | We Bare Bears | Adopted; Pizza Band
 7:30 pm | Total DramaRama | The Bad Guy Busters; The Date
 8:00 pm | Amazing World of Gumball | The Slip; The Awareness
 8:30 pm | Amazing World of Gumball | The Awkwardness; The Heist