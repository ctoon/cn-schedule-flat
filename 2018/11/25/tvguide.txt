# 2018-11-25
 6:00 am | Ben 10 | The Sound And The Furry; Vote Zombozo!
 6:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 7:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:00 am | Total DramaRama | Bananas & Cheese; Venthalla
 8:30 am | Total DramaRama | Free Chili; The Date
 9:00 am | Teen Titans Go! | Hose Water; Yearbook Madness
 9:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
10:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
10:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
11:00 am | Teen Titans Go! | Road Trip; The Best Robin
11:30 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
12:00 pm | Teen Titans Go! | Birds; Be Mine
12:30 pm | Teen Titans Go! | Brain Food; In and Out
 1:00 pm | Total DramaRama | Bananas & Cheese; Aquarium For A Dream
 1:30 pm | Total DramaRama | Cuttin' Corners; Sharing Is Caring
 2:00 pm | Amazing World of Gumball | The Loophole; The Nemesis
 2:30 pm | Amazing World of Gumball | The Slide; The Return
 3:00 pm | Alvin and the Chipmunks: The Road Chip | 
 5:00 pm | Amazing World of Gumball | The Buddy; The Founder
 5:30 pm | Amazing World of Gumball | The Drama; The Schooling
 6:00 pm | Total Drama Island | No So Happy Campers
 6:30 pm | Total Drama Island | Not So Happy Campers
 7:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:00 pm | Amazing World of Gumball | The Test; The Stink
 8:30 pm | Amazing World of Gumball | The Code; The Awareness