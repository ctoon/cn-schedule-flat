# 2018-11-25
 6:00 am | Ben 10 | The Sound and the Furry
 6:15 am | Ben 10 | Vote Zombozo
 6:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 7:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:00 am | Total DramaRama | Bananas & Cheese
 8:15 am | Total DramaRama | Venthalla
 8:30 am | Total DramaRama | Free Chili
 8:45 am | Total DramaRama | The Date
 9:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
 9:30 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
10:00 am | Teen Titans Go! | Robin Backwards; Crazy Day
10:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
11:00 am | Teen Titans Go! | Road Trip; The Best Robin
11:30 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
12:00 pm | Teen Titans Go! | Birds; Be Mine
12:30 pm | Teen Titans Go! | Brain Food; In and Out
 1:00 pm | Total DramaRama | Bananas & Cheese
 1:15 pm | Total DramaRama | Aquarium for a Dream
 1:30 pm | Total DramaRama | Cuttin' Corners
 1:45 pm | Total DramaRama | Sharing Is Caring
 2:00 pm | Amazing World of Gumball | The Loophole
 2:15 pm | Amazing World of Gumball | The Nemesis
 2:30 pm | Amazing World of Gumball | The Slide
 2:45 pm | Amazing World of Gumball | The Return
 3:00 pm | Alvin and the Chipmunks: The Road Chip | 
 5:00 pm | Amazing World of Gumball | The Buddy
 5:15 pm | Amazing World of Gumball | The Founder
 5:30 pm | Amazing World of Gumball | The Drama
 5:45 pm | Amazing World of Gumball | The Schooling
 6:00 pm | Total Drama Island | Not So Happy Campers
 6:30 pm | Total Drama Island | Not So Happy Campers
 7:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 8:00 pm | Amazing World of Gumball | The Test
 8:15 pm | Amazing World of Gumball | The Stink
 8:30 pm | Amazing World of Gumball | The Code
 8:45 pm | Amazing World of Gumball | The Awareness