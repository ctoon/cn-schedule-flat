# 2018-11-25
 8:00 pm | Amazing World of Gumball | Test/The Stink
 8:30 pm | Amazing World of Gumball | Code/The Awareness
 9:00 pm | Family Guy | Airport '07
 9:30 pm | Bob's Burgers | Sit Me Baby One More Time
10:00 pm | American Dad | My Purity Ball and Chain
10:30 pm | Family Guy | Quagmire's Mom
11:00 pm | Family Guy | Encyclopedia Griffin
11:30 pm | Rick and Morty | Total Rickall
12:00 am | DREAM CORP LLC | Dust Bunnies
12:15 am | DREAM CORP LLC | Can't Touch This
12:30 am | Venture Brothers  | The Inamorata Consequence
 1:00 am | Squidbillies | Trackwood Race-ist
 1:15 am | Your Pretty Face is Going to Hell | Hammerman
 1:30 am | American Dad | My Purity Ball and Chain
 2:00 am | Family Guy | Quagmire's Mom
 2:30 am | Family Guy | Encyclopedia Griffin
 3:00 am | Rick and Morty | Total Rickall
 3:30 am | DREAM CORP LLC | Dust Bunnies
 3:45 am | DREAM CORP LLC | Can't Touch This
 4:00 am | Venture Brothers  | The Inamorata Consequence
 4:30 am | Squidbillies | Trackwood Race-ist
 4:45 am | Your Pretty Face is Going to Hell | Hammerman
 5:00 am | Bob's Burgers | Sit Me Baby One More Time
 5:30 am | Home Movies | Guitarmageddon