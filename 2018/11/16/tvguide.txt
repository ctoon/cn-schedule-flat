# 2018-11-16
 6:00 am | Amazing World of Gumball | The Cage; The Stink
 6:30 am | Amazing World of Gumball | The Faith; The Awareness
 7:00 am | Amazing World of Gumball | The Disaster; The Re-run
 7:30 am | Amazing World of Gumball | The Uploads; The Guy
 8:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 8:30 am | Teen Titans Go! | Video Game References; Cool School
 9:00 am | Teen Titans Go! | Movie Night; My Name Is Jose
 9:30 am | Teen Titans Go! | The Power Of Shrimps; The Real Orangins!
10:00 am | Amazing World of Gumball | The Pact; The Buddy
10:30 am | Amazing World of Gumball | The Neighbor; The Potato
11:00 am | Amazing World of Gumball | The Wicked; The Choices
11:30 am | Amazing World of Gumball | The Traitor; The Code
12:00 pm | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
12:30 pm | Teen Titans Go! | A Cat's Fancy; The Fourth Wall
 1:00 pm | Amazing World of Gumball | The Bros; The Man
 1:30 pm | Amazing World of Gumball | The Pizza; The Lie
 2:00 pm | Teen Titans Go! | Permanent Record; The Fight
 2:30 pm | Teen Titans Go! | Titan Saving Time; The Groover
 3:00 pm | Ben 10 | All Koiled Up
 3:15 pm | Amazing World of Gumball | The Friend
 3:30 pm | Amazing World of Gumball | The Spoiler; The Society
 4:00 pm | Amazing World of Gumball | The Fury; The Rival
 4:30 pm | Amazing World of Gumball | The Compilation; The Lady
 5:00 pm | Craig of the Creek | Dinner At the Creek; The Brood
 5:30 pm | Total DramaRama | Tiger Fail; Cuttin' Corners
 6:00 pm | Teen Titans Go! | Oh Yeah!; Riding the Dragon
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | We Bare Bears | Icy Nights; The Audition
 7:30 pm | Total DramaRama | The Bad Guy Busters; The Date
 8:00 pm | Amazing World of Gumball | The One; The Vegging
 8:30 pm | Amazing World of Gumball | The Guy; The Father