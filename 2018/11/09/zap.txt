# 2018-11-09
 6:00 am | Amazing World of Gumball | The Line
 6:15 am | Amazing World of Gumball | The Brain
 6:30 am | Amazing World of Gumball | The List
 6:45 am | Amazing World of Gumball | The Parents
 7:00 am | Amazing World of Gumball | The Others
 7:15 am | Amazing World of Gumball | The Bus
 7:30 am | Amazing World of Gumball | The Signature
 7:45 am | Amazing World of Gumball | The Night
 8:00 am | Teen Titans Go! | The Mask; Slumber Party
 8:30 am | Teen Titans Go! | Serious Business; Thanksgiving
 9:00 am | Teen Titans Go! | Riding the Dragon
 9:15 am | Teen Titans Go! | The Overbite
 9:30 am | Teen Titans Go! | Flashback
10:00 am | Amazing World of Gumball | The Lady
10:15 am | Amazing World of Gumball | The Intelligence
10:30 am | Amazing World of Gumball | The Sucker
10:45 am | Amazing World of Gumball | The Potion
11:00 am | Amazing World of Gumball | The Sale
11:15 am | Amazing World of Gumball | The Blame
11:30 am | Amazing World of Gumball | The Gift
11:45 am | Amazing World of Gumball | The Detective
12:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
12:30 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 1:00 pm | Amazing World of Gumball | The Coach; The Joy
 1:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:00 pm | Teen Titans Go! | Fish Water
 2:15 pm | Teen Titans Go! | TV Knight 3
 2:30 pm | Teen Titans Go! | TV Knight
 2:45 pm | Teen Titans Go! | The Scoop
 3:00 pm | Ben 10 | Vote Zombozo
 3:15 pm | Amazing World of Gumball | The Fraud
 3:30 pm | Amazing World of Gumball | The Boss; The Move
 4:00 pm | Amazing World of Gumball | The Love
 4:15 pm | Amazing World of Gumball | The Uncle
 4:30 pm | Amazing World of Gumball | The Awkwardness
 4:45 pm | Amazing World of Gumball | The Heist
 5:00 pm | Craig of the Creek | Jextra Perrestrial
 5:15 pm | Craig of the Creek | Jessica's Trail
 5:30 pm | Total DramaRama | The Date
 5:45 pm | Total DramaRama | The Bad Guy Busters
 6:00 pm | Teen Titans Go! | The Real Orangins!
 6:15 pm | Teen Titans Go! | The Groover
 6:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 6:45 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 7:00 pm | We Bare Bears | Braces
 7:15 pm | We Bare Bears | Wingmen
 7:30 pm | Total DramaRama | Cluckwork Orange
 7:45 pm | Total DramaRama | Germ Factory
 8:00 pm | Amazing World of Gumball | The Buddy
 8:15 pm | Amazing World of Gumball | The Drama
 8:30 pm | Amazing World of Gumball | The Night
 8:45 pm | Amazing World of Gumball | The Deal