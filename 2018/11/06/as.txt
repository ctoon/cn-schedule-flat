# 2018-11-06
 8:00 pm | Amazing World of Gumball | The Awareness
 8:15 pm | Amazing World of Gumball | The Stink
 8:30 pm | Amazing World of Gumball | Parasite/The Menu
 9:00 pm | American Dad | Roy Rodgers McFreely
 9:30 pm | American Dad | Jack's Back
10:00 pm | Bob's Burgers | The Land Ship
10:30 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
11:00 pm | Family Guy | Fish Out of Water
11:30 pm | Family Guy | Emission Impossible
12:00 am | Rick and Morty | Lawnmower Dog
12:30 am | Robot Chicken | Book of Corinne
12:45 am | Robot Chicken | Werewolf vs. Unicorn
 1:00 am | Aqua Teen | Love Mummy
 1:15 am | Squidbillies | Bubba Trubba
 1:30 am | Bob's Burgers | The Land Ship
 2:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 2:30 am | Family Guy | Fish Out of Water
 3:00 am | Family Guy | Emission Impossible
 3:30 am | Rick and Morty | Lawnmower Dog
 4:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Dance
 4:15 am | Check It Out! | Planes
 4:30 am | Aqua Teen | Love Mummy
 4:45 am | Squidbillies | Bubba Trubba
 5:00 am | American Dad | Roy Rodgers McFreely
 5:30 am | American Dad | Jack's Back