# 2018-11-29
 9:00 pm | American Dad | A Pinata Named Desire
 9:30 pm | American Dad | You Debt Your Life
10:00 pm | Bob's Burgers | Eggs for Days
10:30 pm | Bob's Burgers | Zero Larp Thirty
11:00 pm | Family Guy | Patriot Games
11:30 pm | Family Guy | I Take Thee, Quagmire
12:00 am | Rick and Morty | Interdimensional Cable 2: Tempting Fate
12:30 am | Tender Touches | Breakup Cake
12:45 am | Tender Touches | Breakup Cake: Operetta
 1:00 am | Aqua Teen | Revenge of the Trees
 1:15 am | Squidbillies | Tuscaloosa Dumpling
 1:30 am | Bob's Burgers | Eggs for Days
 2:00 am | Bob's Burgers | Zero Larp Thirty
 2:30 am | Family Guy | Patriot Games
 3:00 am | Family Guy | I Take Thee, Quagmire
 3:30 am | Rick and Morty | Interdimensional Cable 2: Tempting Fate
 4:00 am | Joe Pera Talks With You | Joe Pera Answers Your Questions About Cold Weather Sports
 4:30 am | Aqua Teen | Revenge of the Trees
 4:45 am | Squidbillies | Tuscaloosa Dumpling
 5:00 am | American Dad | A Pinata Named Desire
 5:30 am | American Dad | You Debt Your Life