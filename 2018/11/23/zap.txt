# 2018-11-23
 6:00 am | Teen Titans Go! | Booty Scooty
 6:15 am | Teen Titans Go! | Think About Your Future
 6:30 am | Teen Titans Go! | BBCYFSHIPBDAY
 6:45 am | Teen Titans Go! | Who's Laughing Now
 7:00 am | Teen Titans Go! | Black Friday
 7:15 am | Teen Titans Go! | Beast Girl
 7:30 am | Teen Titans Go! | Snuggle Time
 7:45 am | Teen Titans Go! | Oh Yeah!
 8:00 am | Teen Titans Go! | Flashback
 8:30 am | Teen Titans Go! | Bro-Pocalypse
 8:45 am | Teen Titans Go! | Riding the Dragon
 9:00 am | Teen Titans Go! | Mo' Money Mo' Problems
 9:15 am | Teen Titans Go! | The Overbite
 9:30 am | Teen Titans Go! | TV Knight 3
 9:45 am | Teen Titans Go! | Shrimps and Prime Rib
10:00 am | Teen Titans Go! | The Scoop
10:15 am | Teen Titans Go! | Booby Trap House
10:30 am | Teen Titans Go! | Chicken in the Cradle
10:45 am | Teen Titans Go! | Fish Water
11:00 am | Teen Titans Go! | Kabooms
11:30 am | Teen Titans Go! | Black Friday
11:45 am | Teen Titans Go! | TV Knight
12:00 pm | Teen Titans Go! | Tower Renovation
12:15 pm | Teen Titans Go! | The Streak, Part 1
12:30 pm | Teen Titans Go! | The Streak, Part 2
12:45 pm | Teen Titans Go! | My Name Is Jose
 1:00 pm | Teen Titans Go! | The Power of Shrimps
 1:15 pm | Teen Titans Go! | Inner Beauty of a Cactus
 1:30 pm | Teen Titans Go! | The Real Orangins!
 1:45 pm | Teen Titans Go! | Movie Night
 2:00 pm | Teen Titans Go! | BBRAE
 2:30 pm | Teen Titans Go! | Quantum Fun
 2:45 pm | Teen Titans Go! | The Fight
 3:00 pm | Teen Titans Go! | Black Friday
 3:15 pm | Lego DC Comics Super Heroes: The Flash | 
 5:00 pm | Craig of the Creek | Power Punchers
 5:15 pm | Craig of the Creek | The Curse
 5:30 pm | Total DramaRama | Having the Timeout of Our Lives
 5:45 pm | Total DramaRama | Hic Hic Hooray
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:00 pm | We Bare Bears | Adopted
 7:15 pm | We Bare Bears | Braces
 7:30 pm | Total DramaRama | Tiger Fail
 7:45 pm | Total DramaRama | A Ninjustice to Harold
 8:00 pm | Amazing World of Gumball | The Stink
 8:15 pm | Amazing World of Gumball | The Brain
 8:30 pm | Amazing World of Gumball | The Parents
 8:45 pm | Amazing World of Gumball | The Ad