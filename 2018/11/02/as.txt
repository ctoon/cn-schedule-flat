# 2018-11-02
 8:00 pm | Amazing World of Gumball | Stars/The Origins
 8:30 pm | Amazing World of Gumball | Origins Part 2/The Grades
 9:00 pm | American Dad | Chimdale
 9:30 pm | American Dad | Stan Time
10:00 pm | Bob's Burgers | Hawk & Chick
10:30 pm | Family Guy | And the Wiener Is...
11:00 pm | Family Guy | Lethal Weapons
11:30 pm | Family Guy | Prick Up Your Ears
12:00 am | DREAM CORP LLC | Accordian Jim
12:15 am | DREAM CORP LLC | Amnesia
12:30 am | Tim & Eric's Bedtime Stories | The Endorsement
12:45 am | Tim & Eric's Bedtime Stories | Baby
 1:00 am | Aqua Teen | Bad Replicant
 1:15 am | Squidbillies | Meth O.D. To My Madness
 1:30 am | Bob's Burgers | Hawk & Chick
 2:00 am | Family Guy | And the Wiener Is...
 2:30 am | Family Guy | Lethal Weapons
 3:00 am | DREAM CORP LLC | Accordian Jim
 3:15 am | DREAM CORP LLC | Amnesia
 3:30 am | Tim & Eric's Bedtime Stories | The Endorsement
 3:45 am | Tim & Eric's Bedtime Stories | Baby
 4:00 am | As Seen on Adult Swim | As Seen on Adult Swim
 4:15 am | FishCenter | FishCenter
 4:30 am | Aqua Teen | Bad Replicant
 4:45 am | Squidbillies | Meth O.D. To My Madness
 5:00 am | American Dad | Chimdale
 5:30 am | American Dad | Stan Time