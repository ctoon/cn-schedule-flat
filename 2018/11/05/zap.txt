# 2018-11-05
 6:00 am | Amazing World of Gumball | The Weirdo
 6:15 am | Amazing World of Gumball | The Cringe
 6:30 am | Amazing World of Gumball | The Heist
 6:45 am | Amazing World of Gumball | The Cage
 7:00 am | Amazing World of Gumball | The Friend; The Saint
 7:30 am | Amazing World of Gumball | The Society; The Spoiler
 8:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Teen Titans Go! | History Lesson
 9:15 am | Teen Titans Go! | The Academy
 9:30 am | Teen Titans Go! | The Art of Ninjutsu
 9:45 am | Teen Titans Go! | Throne of Bones
10:00 am | Amazing World of Gumball | The Worst
10:15 am | Amazing World of Gumball | The Anybody
10:30 am | Amazing World of Gumball | The Deal
10:45 am | Amazing World of Gumball | The Pact
11:00 am | Amazing World of Gumball | The Triangle; The Money
11:30 am | Amazing World of Gumball | The Return
11:45 am | Amazing World of Gumball | The Awkwardness
12:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
12:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 1:00 pm | Amazing World of Gumball | The Tag; The Lesson
 1:30 pm | Amazing World of Gumball | The Limit; The Game
 2:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:30 pm | Teen Titans Go! | Oregon Trail
 2:45 pm | Teen Titans Go! | Who's Laughing Now
 3:00 pm | Ben 10 | The Sound and the Furry
 3:15 pm | Amazing World of Gumball | The Tape
 3:30 pm | Amazing World of Gumball | The Internet; The Plan
 4:00 pm | Amazing World of Gumball | The Wicked
 4:15 pm | Amazing World of Gumball | The Catfish
 4:30 pm | Amazing World of Gumball | The Traitor
 4:45 pm | Amazing World of Gumball | The Cycle
 5:00 pm | Craig of the Creek | Jextra Perrestrial
 5:15 pm | Craig of the Creek | Secret Book Club
 5:30 pm | Total DramaRama | Ant We All Just Get Along
 5:45 pm | Total DramaRama | Duck Duck Juice
 6:00 pm | Teen Titans Go! | The Real Orangins!
 6:15 pm | Teen Titans Go! | My Name Is Jose
 6:30 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 6:45 pm | Teen Titans Go! | Brain Percentages
 7:00 pm | We Bare Bears | Escandalosos
 7:15 pm | We Bare Bears | Mom App
 7:30 pm | Total DramaRama | Cuttin' Corners
 7:45 pm | Total DramaRama | Tiger Fail
 8:00 pm | Amazing World of Gumball | The Stink
 8:15 pm | Amazing World of Gumball | The Ad
 8:30 pm | Amazing World of Gumball | The Advice
 8:45 pm | Amazing World of Gumball | The Ex