# 2018-11-05
 6:00 am | Amazing World of Gumball | The Weirdo; The Cringe
 6:30 am | Amazing World of Gumball | The Heist; The Cage
 7:00 am | Amazing World of Gumball | The Friend; The Saint
 7:30 am | Amazing World of Gumball | The Spoiler; The Society
 8:00 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 8:30 am | Teen Titans Go! | Mr. Butt; Man Person
 9:00 am | Teen Titans Go! | History Lesson; The Academy
 9:30 am | Teen Titans Go! | The Art Of Ninjutsu; Throne Of Bones
10:00 am | Amazing World of Gumball | The Worst; The Anybody
10:30 am | Amazing World of Gumball | The Deal; The Pact
11:00 am | Amazing World of Gumball | The Triangle; The Money
11:30 am | Amazing World of Gumball | The Return; The Awkwardness
12:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
12:30 pm | Teen Titans Go! | Baby Hands; Love Monsters
 1:00 pm | Amazing World of Gumball | The Tag; The Lesson
 1:30 pm | Amazing World of Gumball | The Limit; The Game
 2:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:30 pm | Teen Titans Go! | Oregon Trail; Who's Laughing Now
 3:00 pm | Ben 10 | The Sound and the Furry
 3:15 pm | Amazing World of Gumball | The Tape
 3:30 pm | Amazing World of Gumball | The Internet; The Plan
 4:00 pm | Amazing World of Gumball | The Catfish; The Wicked
 4:30 pm | Amazing World of Gumball | The Cycle; The Traitor
 5:00 pm | Craig of the Creek | Jextra Perrestrial; Secret Book
 5:30 pm | Total DramaRama | Ant We All Just Get Along; Duck Duck Juice
 6:00 pm | Teen Titans Go! | The Real Orangins!; My Name Is Jose
 6:30 pm | Teen Titans Go! | The Cruel Giggling Ghoul; Brain Percentages
 7:00 pm | We Bare Bears | Escandalosos; Mom App
 7:30 pm | Total DramaRama | Cuttin' Corners; Tiger Fail
 8:00 pm | Amazing World of Gumball | The Stink; The Ad
 8:30 pm | Amazing World of Gumball | The Advice; The Ex