# 2018-11-14
 6:00 am | Amazing World of Gumball | The Vegging
 6:15 am | Amazing World of Gumball | The Spinoffs
 6:30 am | Amazing World of Gumball | The One
 6:45 am | Amazing World of Gumball | The Transformation
 7:00 am | Amazing World of Gumball | The Parking
 7:15 am | Amazing World of Gumball | The Fury
 7:30 am | Amazing World of Gumball | The Routine
 7:45 am | Amazing World of Gumball | The Compilation
 8:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 8:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
 9:00 am | Teen Titans Go! | BBSFBDAY
 9:15 am | Teen Titans Go! | Chicken in the Cradle
 9:30 am | Teen Titans Go! | The Streak, Part 1
 9:45 am | Teen Titans Go! | The Streak, Part 2
10:00 am | Amazing World of Gumball | The Cage
10:15 am | Amazing World of Gumball | The Stink
10:30 am | Amazing World of Gumball | The Faith
10:45 am | Amazing World of Gumball | The Awareness
11:00 am | Amazing World of Gumball | The Re-Run
11:15 am | Amazing World of Gumball | The Disaster
11:30 am | Amazing World of Gumball | The Uploads
11:45 am | Amazing World of Gumball | The Guy
12:00 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
12:30 pm | Teen Titans Go! | Video Game References; Cool School
 1:00 pm | Amazing World of Gumball | The Law; The Allergy
 1:30 pm | Amazing World of Gumball | The Mothers; The Password
 2:00 pm | Teen Titans Go! | Movie Night
 2:15 pm | Teen Titans Go! | My Name Is Jose
 2:30 pm | Teen Titans Go! | The Power of Shrimps
 2:45 pm | Teen Titans Go! | The Real Orangins!
 3:00 pm | Ben 10 | The Charm Offensive
 3:15 pm | Amazing World of Gumball | The Bros
 3:30 pm | Amazing World of Gumball | The Pizza; The Lie
 4:00 pm | Amazing World of Gumball | The Misunderstandings
 4:15 pm | Amazing World of Gumball | The Petals
 4:30 pm | Amazing World of Gumball | The Roots
 4:45 pm | Amazing World of Gumball | The Line
 5:00 pm | Craig of the Creek | The Invitation
 5:15 pm | Craig of the Creek | You're It
 5:30 pm | Total DramaRama | Duck Duck Juice
 5:45 pm | Total DramaRama | Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Booty Scooty
 6:15 pm | Teen Titans Go! | Thanksgetting
 6:30 pm | Teen Titans Go! | The Groover
 6:45 pm | Teen Titans Go! | Who's Laughing Now
 7:00 pm | We Bare Bears | Bear Flu
 7:15 pm | We Bare Bears | Baby Bears on a Plane
 7:30 pm | Total DramaRama | Tiger Fail
 7:45 pm | Total DramaRama | Cuttin' Corners
 8:00 pm | Amazing World of Gumball | The Fury
 8:15 pm | Amazing World of Gumball | The Rival
 8:30 pm | Amazing World of Gumball | The Compilation
 8:45 pm | Amazing World of Gumball | The Lady