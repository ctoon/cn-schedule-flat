# 2018-11-08
 8:00 pm | Amazing World of Gumball | The Drama
 8:15 pm | Amazing World of Gumball | The Slip
 8:30 pm | Amazing World of Gumball | Points/The Best
 9:00 pm | American Dad | Every Which Way But Lose
 9:30 pm | American Dad | Weiner of Our Discontent
10:00 pm | Bob's Burgers | Lice Things Are Lice
10:30 pm | Bob's Burgers | House of 1000 Bounces
11:00 pm | Family Guy | Peter Griffin: Husband, Father, Brother?
11:30 pm | Family Guy | Ready, Willing and Disabled
12:00 am | Rick and Morty | M. Night Shaym-Aliens!
12:30 am | Robot Chicken | Tapping a Hero
12:45 am | Robot Chicken | Shoe
 1:00 am | Aqua Teen | Interfection
 1:15 am | Squidbillies | Terminus Trouble
 1:30 am | Bob's Burgers | Lice Things Are Lice
 2:00 am | Bob's Burgers | House of 1000 Bounces
 2:30 am | Family Guy | Peter Griffin: Husband, Father, Brother?
 3:00 am | Family Guy | Ready, Willing and Disabled
 3:30 am | Rick and Morty | M. Night Shaym-Aliens!
 4:00 am | Joe Pera Talks With You | Joe Pera Reads You The Church Announcements
 4:15 am | Check It Out! | Home
 4:30 am | Aqua Teen | Interfection
 4:45 am | Squidbillies | Terminus Trouble
 5:00 am | American Dad | Every Which Way But Lose
 5:30 am | American Dad | Weiner of Our Discontent