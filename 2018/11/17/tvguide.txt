# 2018-11-17
 6:00 am | Teen Titans Go! | Jinxed; Tv Knight
 6:30 am | Transformers Cyberverse | Teletraan-X
 6:45 am | Teen Titans Go! | Orangins
 7:00 am | Teen Titans Go! | The Avogodo; Fish Water
 7:30 am | Teen Titans Go! | Booby Trap House; Employee Of The Month Redux
 8:00 am | Amazing World of Gumball | The Love; The Line
 8:30 am | Amazing World of Gumball | The Puppets; The Parasite
 9:00 am | Total DramaRama | Having The Timeout Of Our Lives; Hic Hic Hooray
 9:30 am | Total DramaRama | A Ninjustice To Harold; Tiger Fail
10:00 am | Teen Titans Go! | Hand Zombie; Shrimps And Prime Rib
10:30 am | Teen Titans Go! | Master Detective; The Overbite
11:00 am | Teen Titans Go! | Titan Saving Time; Riding The Dragon
11:30 am | Teen Titans Go! | Permanent Record; Oh Yeah!
12:00 pm | Ben 10 | Out to Launch
12:15 pm | Teen Titans Go! | Terra-ized
12:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 1:00 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 1:30 pm | Teen Titans Go! | Books; Lazy Sunday
 2:00 pm | Amazing World of Gumball | The Petals; The Signal
 2:30 pm | Amazing World of Gumball | The Deal; The Advice
 3:00 pm | Amazing World of Gumball | The Worst; The Girlfriend
 3:30 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 4:00 pm | Total DramaRama | Hic Hic Hooray; That's A Wrap
 4:30 pm | Total DramaRama | Free Chili; The Bad Guy Busters
 5:00 pm | Amazing World of Gumball | The Uncle; The Best
 5:30 pm | Amazing World of Gumball | The Menu; The Traitor
 6:00 pm | Amazing World of Gumball | The Sorcerer; The Wicked
 6:30 pm | Amazing World of Gumball | The Ex; The Hug
 7:00 pm | Madagascar | 