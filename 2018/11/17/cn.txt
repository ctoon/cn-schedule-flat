# 2018-11-17
 6:00 am | Teen Titans Go! | Jinxed/TV Knight
 6:30 am | Transformers Cyberverse | Teletraan X
 6:45 am | Teen Titans Go! | Orangins
 7:00 am | Teen Titans Go! | Avogodo/Fish Water
 7:30 am | Teen Titans Go! | Employee of the Month Redux/Booby Trap House
 8:00 am | Amazing World of Gumball | Line/The Love
 8:30 am | Amazing World of Gumball | Puppets/The Parasite
 9:00 am | Total DramaRama | Hic Hic Hooray/Having the Timeout of Our Lives
 9:30 am | Total DramaRama | A Ninjustice to Harold/Tiger Fail
10:00 am | Teen Titans Go! | Hand Zombie/Shrimps and Prime Rib
10:30 am | Teen Titans Go! | Master Detective/The Overbite
11:00 am | Teen Titans Go! | Titan Saving Time/Riding the Dragon
11:30 am | Teen Titans Go! | Permanent Record/Oh Yeah!
12:00 pm | Ben 10 | Out to Launch
12:15 pm | Teen Titans Go! | Terra-ized
12:30 pm | Teen Titans Go! | Burger vs. Burrito/Matched
 1:00 pm | Teen Titans Go! | Colors of Raven/The Left Leg
 1:30 pm | Teen Titans Go! | Books/Lazy Sunday
 2:00 pm | Amazing World of Gumball | Petals/The Signal
 2:30 pm | Amazing World of Gumball | Deal/The Advice
 3:00 pm | Amazing World of Gumball | Worst/The Girlfriend
 3:30 pm | Amazing World of Gumball | Origins/The Origins Part 2
 4:00 pm | Total DramaRama | Hic Hic Hooray/That's a Wrap
 4:30 pm | Total DramaRama | Free Chili/The Bad Guy Busters
 5:00 pm | Amazing World of Gumball | Uncle/The Best
 5:30 pm | Amazing World of Gumball | Menu/The Traitor
 6:00 pm | Amazing World of Gumball | Sorcerer/The Wicked
 6:30 pm | Amazing World of Gumball | Ex/The Hug
 7:00 pm | MOVIE | Lego Justice League: Gotham City Breakout/The Groover