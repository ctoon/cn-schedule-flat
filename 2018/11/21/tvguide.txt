# 2018-11-21
 6:00 am | Teen Titans Go! | Thanksgetting; Thanksgiving
 6:30 am | Teen Titans Go! | Master Detective; Two Parter Part 1
 7:00 am | Teen Titans Go! | Two Parter Part 2; Hand Zombie
 7:30 am | Teen Titans Go! | Employee of the Month Redux; Squash & Stretch
 8:00 am | Teen Titans Go! | The Avogodo; Garage Sale
 8:30 am | Teen Titans Go! | Orangins; Secret Garden
 9:00 am | Teen Titans Go! | Jinxed; The Cruel Giggling Ghoul
 9:30 am | Teen Titans Go! | Brain Percentages; Pyramid Scheme
10:00 am | Teen Titans Go! | Thanksgetting; Thanksgiving
10:30 am | Teen Titans Go! | Hot Salad Water; Finally a Lesson
11:00 am | Teen Titans Go: Island Adventures | 
12:00 pm | Teen Titans Go! | Operation Dude Rescue Part 2; Throne of Bones
12:30 pm | Teen Titans Go! | Thanksgetting; Thanksgiving
 1:00 pm | Alvin and the Chipmunks: The Road Chip | 
 3:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 3:30 pm | Teen Titans Go! | Flashback
 4:00 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 4:30 pm | Teen Titans Go! | The Groover; Quantum Fun
 5:00 pm | Teen Titans Go! | The Fight; The Real Orangins!
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 6:30 pm | Unikitty | Float On
 6:45 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:15 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 7:45 pm | Teen Titans Go! | Thanksgetting
 8:00 pm | Amazing World of Gumball | The Cringe; The Debt
 8:30 pm | Amazing World of Gumball | The Singing; The Extras