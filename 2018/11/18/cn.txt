# 2018-11-18
 6:00 am | Ben 10 | That's the Stuff/Drone On
 6:30 am | SPECIAL | TTG v PPG
 6:45 am | Teen Titans Go! | Snuggle Time
 7:00 am | Teen Titans Go! | BBRAE
 7:30 am | Teen Titans Go! | Movie Night/Oregon Trail
 8:00 am | Total DramaRama | Hic Hic Hooray/Cone in 60 Seconds
 8:30 am | Total DramaRama | Cluckwork Orange/Germ Factory
 9:00 am | Teen Titans Go! | Inner Beauty of a Cactus/Who's Laughing Now
 9:30 am | Teen Titans Go! | Booty Scooty/The Streak Part 1
10:00 am | Teen Titans Go! | Streak Part 2/Think About Your Future
10:30 am | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
11:00 am | Teen Titans Go! | History Lesson/The Art of Ninjutsu
11:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob/Some of Their Parts/Thanksgetting
12:00 pm | Teen Titans Go! | Power Moves/Staring at the Future
12:30 pm | Teen Titans Go! | Starfire the Terrible
12:45 pm | SPECIAL | TTG v PPG
 1:00 pm | Total DramaRama | Hic Hic Hooray/Ant We All Just Get Along
 1:30 pm | Total DramaRama | Duck Duck Juice/Sharing Is Caring
 2:00 pm | Amazing World of Gumball | Diet/The Apprentice
 2:30 pm | Amazing World of Gumball | Grades/The Uploads
 3:00 pm | Amazing World of Gumball | Stars/The Romantic
 3:30 pm | Amazing World of Gumball | Cycle/The Comic
 4:00 pm | The Powerpuff Girls | Small World
 5:00 pm | MOVIE | Lego Justice League: Gotham City Breakout
 6:45 pm | SPECIAL | TTG v PPG
 7:00 pm | Amazing World of Gumball | Catfish/The Upgrade
 7:30 pm | Amazing World of Gumball | Ollie/The Routine