# 2018-11-26
 9:00 pm | American Dad | Son of Stan
 9:30 pm | American Dad | Stan's Food Restaurant
10:00 pm | Bob's Burgers | There's No Business Like Mr. Business Business
10:30 pm | Bob's Burgers | A Few 'gurt Men
11:00 pm | Family Guy | PTV
11:30 pm | Family Guy | Brian Goes Back to College
12:00 am | Rick and Morty | Get Schwifty
12:30 am | Tender Touches | She's All We Had
12:45 am | Tender Touches | She's All We Had: Operetta
 1:00 am | Aqua Teen | Super Trivia
 1:15 am | Squidbillies | Condition: Demolition!
 1:30 am | Bob's Burgers | There's No Business Like Mr. Business Business
 2:00 am | Bob's Burgers | A Few 'gurt Men
 2:30 am | Family Guy | PTV
 3:00 am | Family Guy | Brian Goes Back to College
 3:30 am | Rick and Morty | Get Schwifty
 4:00 am | Joe Pera Talks With You | Joe Pera Reads You The Church Announcements
 4:15 am | Check It Out! | Stevie!
 4:30 am | Aqua Teen | Super Trivia
 4:45 am | Squidbillies | Condition: Demolition!
 5:00 am | American Dad | Son of Stan
 5:30 am | American Dad | Stan's Food Restaurant