# 2018-05-12
 6:00 am | OK K.O.! Let's Be Heroes | My Fair Carol
 6:15 am | Amazing World of Gumball | The Return
 6:30 am | Amazing World of Gumball | The Crew
 6:45 am | Amazing World of Gumball | The Others
 7:00 am | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 7:15 am | Amazing World of Gumball | The Signature
 7:30 am | Amazing World of Gumball | The Check
 7:45 am | Amazing World of Gumball | The Pest
 8:00 am | OK K.O.! Let's Be Heroes | Your World Is an Illusion
 8:15 am | Teen Titans Go! | Classic Titans
 8:30 am | Teen Titans Go! | Lication
 8:45 am | Teen Titans Go! | History Lesson
 9:00 am | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 9:15 am | Teen Titans Go! | The Art of Ninjutsu
 9:30 am | Teen Titans Go! | Hot Salad Water
 9:45 am | Teen Titans Go! | Think About Your Future
10:00 am | OK K.O.! Let's Be Heroes | My Fair Carol
10:15 am | Teen Titans Go! | Booty Scooty
10:30 am | Teen Titans Go! | BL4Z3
10:45 am | Teen Titans Go! | Who's Laughing Now
11:00 am | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
11:15 am | Teen Titans Go! | Oregon Trail
11:30 am | Teen Titans Go! | Brain Percentages
11:45 am | Teen Titans Go! | Snuggle Time
12:00 pm | OK K.O.! Let's Be Heroes | Your World Is an Illusion
12:15 pm | Ben 10 | Fear the Fogg
12:30 pm | Ben 10 | Bounty Ball
12:45 pm | Ben 10 | Xingo's Back
 1:00 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 1:15 pm | Amazing World of Gumball | The Line
 1:30 pm | Amazing World of Gumball | The Compilation
 1:45 pm | Amazing World of Gumball | The Stories
 2:00 pm | OK K.O.! Let's Be Heroes | My Fair Carol
 2:15 pm | Craig of the Creek | Lost in the Sewer
 2:30 pm | Craig of the Creek | Bring Out Your Beast
 2:45 pm | Craig of the Creek | Monster in the Garden
 3:00 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 3:15 pm | Amazing World of Gumball | The List
 3:30 pm | Amazing World of Gumball | The Guy
 3:45 pm | Amazing World of Gumball | The Vision
 4:00 pm | OK K.O.! Let's Be Heroes | Your World Is an Illusion
 4:15 pm | Unikitty | Dinner Apart-y
 4:30 pm | Unikitty | Wishing Well
 4:45 pm | Unikitty | Little Prince Puppycorn
 5:00 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 5:15 pm | Teen Titans Go! | Oh Yeah!
 5:30 pm | Teen Titans Go! | Top of the Titans: Beast Boy & Cyborg Songs
 5:45 pm | Teen Titans Go! | Top of the Titans: Best Love Songs
 6:00 pm | OK K.O.! Let's Be Heroes | My Fair Carol
 6:15 pm | Amazing World of Gumball | The News
 6:30 pm | Amazing World of Gumball | The Disaster
 6:45 pm | Amazing World of Gumball | The Re-Run
 7:00 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 7:15 pm | Craig of the Creek | The Brood
 7:30 pm | Craig of the Creek | The Future Is Cardboard
 7:45 pm | Craig of the Creek | Sunday Clothes