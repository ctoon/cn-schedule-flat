# 2018-05-23
6:00 a.m. | Steven Universe | House Guest
6:15 a.m. | Mighty Magiswords | The Incredible Tiny Warriors
6:45 a.m. | Teen Titans Go! | Top of the Titans: Dance Party!
7:00 a.m. | Teen Titans | Only Human
7:30 a.m. | Amazing World of Gumball | Ollie/The Catfish
8:00 a.m. | Amazing World of Gumball | Cycle/The Stars
8:30 a.m. | Teen Titans Go! | Road Trip/The Best Robin
9:00 a.m. | Teen Titans Go! | Mouth Hole/Hot Garbage
9:30 a.m. | Amazing World of Gumball | The Name/The Extras
10:00 a.m. | Amazing World of Gumball | The Gripes/The Vacation
10:30 a.m. | Craig of the Creek | Brood/The Future is Cardboard
11:00 a.m. | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums
11:30 a.m. | Teen Titans Go! | Video Game References/Cool School
12:00 p.m. | Amazing World of Gumball | Advice/The Signal
12:30 p.m. | Amazing World of Gumball | Parasite/The Love
1:00 p.m. | We Bare Bears | Panda's Art/Icy Nights II
1:30 p.m. | We Bare Bears | Kitty/Dog Hotel
2:00 p.m. | Craig of the Creek | Dog Decider/The Curse
2:30 p.m. | Teen Titans Go! | Overbite/The Cape
3:00 p.m. | Teen Titans Go! | Shrimps and Prime Rib/Booby Trap House
3:30 p.m. | OK K.O.! Let's Be Heroes | Seasons Change/Lord Cowboy Darrell
4:00 p.m. | SPECIAL | Teen Titans Go!: Time Traveling Titans
5:00 p.m. | Craig of the Creek | Sunday Clothes/Wildernessa
5:30 p.m. | Teen Titans Go! | Justice League's Next Top Talent Idol Star
6:00 p.m. | Ben 10 | All Koiled Up
6:15 p.m. | LEGO Ninjago | Dread on Arrival
6:45 p.m. | Amazing World of Gumball | The Pact
7:00 p.m. | Amazing World of Gumball | Box/The Ex
7:30 p.m. | Amazing World of Gumball | Candidate/The Diet