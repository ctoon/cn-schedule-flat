# 2018-05-31
 6:00 am | Steven Universe | Fusion Cuisine
 6:15 am | Justice League Action | Captain Bamboozle
 6:30 am | Teen Titans | Aftershock
 7:00 am | Teen Titans | Aftershock
 7:30 am | Amazing World of Gumball | The Puppets; The Cringe
 8:00 am | Amazing World of Gumball | The Line; The Cage
 8:30 am | Teen Titans Go! | Leg Day; Grube's Fairytales
 9:00 am | Teen Titans Go! | The Dignity of Teeth; A Farce
 9:30 am | Amazing World of Gumball | The Butterfly; The Question
10:00 am | Amazing World of Gumball | The Bumpkin
10:30 am | Craig of the Creek | The Curse; Jessica Goes to the Creek
11:00 am | Teen Titans Go! | Croissant; Animals, It's Just a Word!
11:30 am | Teen Titans Go! | The Spice Game
11:45 am | Ben 10 | Bon Voyage
12:00 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
12:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 1:00 pm | Amazing World of Gumball | The Petals; The Deal
 1:30 pm | Unikitty | Pet Pet; Kaiju Kitty
 2:00 pm | LEGO DC Comics Super Heroes: The Flash | 
 4:00 pm | Amazing World of Gumball | The Words; The Apology
 4:30 pm | Amazing World of Gumball | The Watch; The Bet
 5:00 pm | Craig of the Creek | Monster in the Garden; Lost in the Sewer
 5:30 pm | Teen Titans Go: Island Adventures | 
 6:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 7:00 pm | Craig of the Creek | The Curse; The Future Is Cardboard
 7:30 pm | Amazing World of Gumball | The Authority; The Virus