# 2018-05-10
 6:00 am | Steven Universe | Beach Party
 6:15 am | Mighty Magiswords | Ain't That a Kick in the Side?
 6:30 am | Teen Titans | Revolutions
 7:00 am | Teen Titans | Wavelength
 7:30 am | Amazing World of Gumball | The Origins Part 2; The Girlfriend; The Traitor
 8:00 am | Amazing World of Gumball | The Advice; The Signal
 8:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 9:00 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:30 am | Amazing World of Gumball | The Fridge; The Remote
10:00 am | Amazing World of Gumball | The Flower; The Banana
10:30 am | Craig of the Creek | The Future Is Cardboard; Escape From Family Dinner
11:00 am | Teen Titans Go! | Dreams; Grandma Voice
11:30 am | Teen Titans Go! | Real Magic
11:45 am | Ben 10 | Fear The Fogg
12:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
12:30 pm | Amazing World of Gumball | The Mirror; The Burden
 1:00 pm | We Bare Bears | Food Truck; Primal
 1:30 pm | We Bare Bears | Nom Nom; Everyday Bears
 2:00 pm | Craig of the Creek | Lost In The Sewer; Bring Out Your Beast
 2:30 pm | Teen Titans Go! | The Croissant; The Dignity of Teeth
 3:00 pm | Teen Titans Go! | The Spice Game; I'm the Sauce
 3:30 pm | OK K.O.! Let's Be Heroes | The Perfect Meal; Hope This Flies
 4:00 pm | Amazing World of Gumball | The News; The Compilation
 4:30 pm | Amazing World of Gumball | The Cage; The Fury
 5:00 pm | Craig of the Creek | Sunday Clothes; Wildernessa
 5:30 pm | Teen Titans Go!: Pizza Party | 
 6:30 pm | Unikitty | Little Prince Puppycorn; Kaiju Kitty
 7:00 pm | Amazing World of Gumball | The Heist; The Detective
 7:30 pm | Amazing World of Gumball | The Cringe; The Blame