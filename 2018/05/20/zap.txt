# 2018-05-20
 6:00 am | OK K.O.! Let's Be Heroes | Red Action to the Future
 6:15 am | Amazing World of Gumball | The Cage
 6:30 am | Amazing World of Gumball | The Ollie
 6:45 am | Amazing World of Gumball | The Diet
 7:00 am | OK K.O.! Let's Be Heroes | TKO's House
 7:15 am | OK K.O.! Let's Be Heroes | Dendy's Power
 7:30 am | Ben 10 | Fear the Fogg
 7:45 am | Ben 10 | Super-Villain Team-Up
 8:00 am | OK K.O.! Let's Be Heroes | Point to the Plaza
 8:15 am | Teen Titans Go! | Lication
 8:30 am | Teen Titans Go! | The Academy
 8:45 am | Teen Titans Go! | Classic Titans
 9:00 am | OK K.O.! Let's Be Heroes | TKO's House
 9:15 am | Teen Titans Go! | Ones and Zeroes
 9:30 am | Teen Titans Go! | Two Parter: Part One
 9:45 am | Teen Titans Go! | Two Parter: Part Two
10:00 am | OK K.O.! Let's Be Heroes | Red Action to the Future
10:15 am | Teen Titans Go! | Jinxed
10:30 am | Teen Titans Go! | TV Knight 2
10:45 am | Teen Titans Go! | Brain Percentages
11:00 am | OK K.O.! Let's Be Heroes | Dendy's Power
11:15 am | Teen Titans Go! | BL4Z3
11:30 am | Teen Titans Go! | Career Day
11:45 am | Teen Titans Go! | Hot Salad Water
12:00 pm | OK K.O.! Let's Be Heroes | Point to the Plaza
12:15 pm | Amazing World of Gumball | The Faith
12:30 pm | Amazing World of Gumball | The Cycle
12:45 pm | Amazing World of Gumball | The Sorcerer
 1:00 pm | OK K.O.! Let's Be Heroes | TKO's House
 1:15 pm | Amazing World of Gumball | The Candidate
 1:30 pm | Amazing World of Gumball | The Neighbor
 1:45 pm | Amazing World of Gumball | The Grades
 2:00 pm | OK K.O.! Let's Be Heroes | Red Action to the Future
 2:15 pm | Craig of the Creek | Too Many Treasures
 2:30 pm | Craig of the Creek | The Final Book
 2:45 pm | Craig of the Creek | You're It
 3:00 pm | OK K.O.! Let's Be Heroes | Dendy's Power
 3:15 pm | We Bare Bears | Everyone's Tube
 3:30 pm | We Bare Bears | Yuri and the Bear
 3:45 pm | We Bare Bears | Crowbar Jones
 4:00 pm | OK K.O.! Let's Be Heroes | Point to the Plaza
 4:15 pm | The Powerpuff Girls | Worship
 4:30 pm | The Powerpuff Girls | Blossom3
 4:45 pm | The Powerpuff Girls | Mojo the Great
 5:00 pm | OK K.O.! Let's Be Heroes | TKO's House
 5:15 pm | Amazing World of Gumball | The Menu
 5:30 pm | Amazing World of Gumball | The Fury
 5:45 pm | Amazing World of Gumball | The Pact
 6:00 pm | OK K.O.! Let's Be Heroes | Red Action to the Future
 6:15 pm | OK K.O.! Let's Be Heroes | Dendy's Power
 6:30 pm | Ben 10: Race Against Time | 