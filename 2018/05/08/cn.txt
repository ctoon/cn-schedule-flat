# 2018-05-08
6:00 a.m. | Steven Universe | Can't Go Back
6:15 a.m. | Mighty Magiswords | Fixing a Flonk
6:30 a.m. | Teen Titans | Betrothed
7:00 a.m. | Ben 10 | Fear the Fogg
7:15 a.m. | Amazing World of Gumball | The Sorcerer
7:30 a.m. | Amazing World of Gumball | Comic/The Romantic
8:00 a.m. | Amazing World of Gumball | Uploads/The Apprentice
8:30 a.m. | Teen Titans Go! | Colors of Raven/The Left Leg
9:00 a.m. | Teen Titans Go! | Books/Lazy Sunday
9:30 a.m. | Amazing World of Gumball | Father/The Cringe
10:00 a.m. | Amazing World of Gumball | Cage/The Faith
10:30 a.m. | Craig of the Creek | Jessica Goes to the Creek/The Final Book
11:00 a.m. | Teen Titans Go! | Birds/Be Mine
11:30 a.m. | Teen Titans Go! | Brain Food/In and Out
12:00 p.m. | Amazing World of Gumball | The Fraud/The Void
12:30 p.m. | Amazing World of Gumball | The Boss/The Move
1:00 p.m. | We Bare Bears | Bearz II Men/I Am Ice Bear
1:30 p.m. | We Bare Bears | Captain Craboo
2:00 p.m. | Craig of the Creek | Escape from Family Dinner/The Future is Cardboard
2:30 p.m. | Teen Titans Go! | Career Day/TV Knight 2
3:00 p.m. | Teen Titans Go! | Academy/Throne of Bones
3:30 p.m. | OK K.O.! Let's Be Heroes | Lad & Logic/OK Dendy
4:00 p.m. | Amazing World of Gumball | Vegging/The Test
4:30 p.m. | Amazing World of Gumball | Pact/The Code
5:00 p.m. | Craig of the Creek | Lost in the Sewer/Bring Out Your Beast
5:30 p.m. | SPECIAL | Teen Titans Go!: Road Trip
6:30 p.m. | Unikitty | Kitty Court/Rock Friend
7:00 p.m. | Amazing World of Gumball | Sucker/The Choices
7:30 p.m. | Amazing World of Gumball | Anybody/The Vision