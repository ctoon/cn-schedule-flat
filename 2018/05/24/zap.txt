# 2018-05-24
 6:00 am | Steven Universe | Space Race
 6:15 am | Mighty Magiswords | Helping Cattus Help
 6:30 am | Teen Titans | Fear Itself
 7:00 am | Teen Titans | Date With Destiny
 7:30 am | Amazing World of Gumball | The Grades
 7:45 am | Amazing World of Gumball | The Diet
 8:00 am | Amazing World of Gumball | The Ex
 8:15 am | Amazing World of Gumball | The Sorcerer
 8:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
 9:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:30 am | Amazing World of Gumball | The Fraud; The Void
10:00 am | Amazing World of Gumball | The Boss; The Move
10:30 am | Craig of the Creek | Lost in the Sewer
10:45 am | Craig of the Creek | Bring Out Your Beast
11:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:30 am | Teen Titans Go! | Operation Tin Man; Nean
12:00 pm | Amazing World of Gumball | The Awkwardness
12:15 pm | Amazing World of Gumball | The Nest
12:30 pm | Amazing World of Gumball | The Points
12:45 pm | Amazing World of Gumball | The Bus
 1:00 pm | We Bare Bears | Beehive
 1:15 pm | We Bare Bears | Baby Bears Can't Jump
 1:30 pm | We Bare Bears | Lazer Royale
 1:45 pm | We Bare Bears | Bear Lift
 2:00 pm | Craig of the Creek | Monster in the Garden
 2:15 pm | Craig of the Creek | Escape From Family Dinner
 2:30 pm | Teen Titans Go! | Fish Water
 2:45 pm | Teen Titans Go! | TV Knight
 3:00 pm | Teen Titans Go! | BBSFBDAY
 3:15 pm | Teen Titans Go! | Inner Beauty of a Cactus
 3:30 pm | OK K.O.! Let's Be Heroes | Be a Team
 3:45 pm | OK K.O.! Let's Be Heroes | My Fair Carol
 4:00 pm | Teen Titans Go! | 
 5:00 pm | Craig of the Creek | Too Many Treasures
 5:15 pm | Craig of the Creek | The Final Book
 5:30 pm | Teen Titans Go! | Beast Girl
 5:45 pm | Teen Titans Go! | BBCYFSHIPBDAY
 6:00 pm | Ben 10 | Can I Keep It?
 6:15 pm | Ninjago: Masters of Spinjitzu: Sons of Garmadon | True Potential
 6:45 pm | Amazing World of Gumball | The Shippening
 7:00 pm | Amazing World of Gumball | The Vase
 7:15 pm | Amazing World of Gumball | The Cycle
 7:30 pm | Amazing World of Gumball | The Cage
 7:45 pm | Amazing World of Gumball | The Catfish