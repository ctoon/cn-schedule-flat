# 2018-05-04
 6:00 am | Steven Universe | Steven the Sword Fighter
 6:15 am | Mighty Magiswords | Suitable Armor
 6:30 am | Teen Titans | Winner Take All
 7:00 am | Ben 10 | Bounty Ball
 7:15 am | Amazing World of Gumball | The Pact
 7:30 am | Amazing World of Gumball | The Signature
 7:45 am | Amazing World of Gumball | The Check
 8:00 am | Amazing World of Gumball | The Pest
 8:15 am | Amazing World of Gumball | The Sale
 8:30 am | Teen Titans Go! | Parasite; Starliar
 9:00 am | Teen Titans Go! | Meatball Party; Staff Meeting
 9:30 am | Amazing World of Gumball | The Petals
 9:45 am | Amazing World of Gumball | The Puppets
10:00 am | Amazing World of Gumball | The Line
10:15 am | Amazing World of Gumball | The List
10:30 am | Craig of the Creek | Monster in the Garden
10:45 am | Craig of the Creek | The Curse
11:00 am | Teen Titans Go! | Power Moves; Staring at the Future
11:30 am | Teen Titans Go! | No Power; Sidekick
12:00 pm | Amazing World of Gumball | The Pony; The Storm
12:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 1:00 pm | We Bare Bears | Bear Cleanse
 1:15 pm | We Bare Bears | The Island
 1:30 pm | We Bare Bears | Charlie Ball
 1:45 pm | We Bare Bears | Bear Flu
 2:00 pm | Craig of the Creek | Jessica Goes to the Creek
 2:15 pm | Craig of the Creek | The Final Book
 2:30 pm | Teen Titans Go! | The Avogodo
 2:45 pm | Teen Titans Go! | Orangins
 3:00 pm | Teen Titans Go! | Jinxed
 3:15 pm | Teen Titans Go! | Brain Percentages
 3:30 pm | OK K.O.! Let's Be Heroes | The Perfect Meal
 3:45 pm | OK K.O.! Let's Be Heroes | Hope This Flies
 4:00 pm | Amazing World of Gumball | The Anybody
 4:15 pm | Amazing World of Gumball | The Pact
 4:30 pm | Amazing World of Gumball | The Neighbor
 4:45 pm | Amazing World of Gumball | The Shippening
 5:00 pm | Craig of the Creek | Escape From Family Dinner
 5:15 pm | Craig of the Creek | The Future Is Cardboard
 5:30 pm | Teen Titans Go! | Flashback
 6:00 pm | Teen Titans Go! | Top of the Titans: Best Love Songs
 6:15 pm | Teen Titans Go! | Top of the Titans: Raddest Songs
 6:30 pm | Unikitty | Birthday Blowout
 6:45 pm | Unikitty | Unikitty News!
 7:00 pm | Amazing World of Gumball | The Origins
 7:15 pm | Amazing World of Gumball | The Origins
 7:30 pm | Amazing World of Gumball | The Disaster
 7:45 pm | Amazing World of Gumball | The Re-Run