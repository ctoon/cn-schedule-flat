# 2018-05-15
 6:00 am | Steven Universe | Joking Victim
 6:15 am | Mighty Magiswords | Goat!
 6:30 am | Teen Titans | Divide and Conquer
 7:00 am | Ben 10 | Super-Villain Team-Up
 7:15 am | Amazing World of Gumball | The Grades
 7:30 am | Amazing World of Gumball | The Roots
 7:45 am | Amazing World of Gumball | The Blame
 8:00 am | Amazing World of Gumball | The Detective
 8:15 am | Amazing World of Gumball | The Fury
 8:30 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 9:00 am | Teen Titans Go! | Dreams; Grandma Voice
 9:30 am | Amazing World of Gumball | The Pony; The Storm
10:00 am | Amazing World of Gumball | The Dream; The Sidekick
10:30 am | Craig of the Creek | The Curse
10:45 am | Craig of the Creek | Monster in the Garden
11:00 am | Teen Titans Go! | Love Monsters; Baby Hands
11:30 am | Teen Titans Go! | Sandwich Thief; Money Grandma
12:00 pm | Unikitty | License to Punch
12:15 pm | Unikitty | Wishing Well
12:30 pm | Amazing World of Gumball | The Downer; The Egg
 1:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 1:30 pm | We Bare Bears | Bear Cleanse
 1:45 pm | We Bare Bears | Charlie Ball
 2:00 pm | Craig of the Creek | Too Many Treasures
 2:15 pm | Craig of the Creek | The Brood
 2:30 pm | Teen Titans Go! | Two Parter: Part One
 2:45 pm | Teen Titans Go! | Two Parter: Part Two
 3:00 pm | Teen Titans Go! | Squash & Stretch
 3:15 pm | Teen Titans Go! | Garage Sale
 3:30 pm | OK K.O.! Let's Be Heroes | Plaza Film Festival
 3:45 pm | OK K.O.! Let's Be Heroes | Be a Team
 4:00 pm | Amazing World of Gumball | The Best
 4:15 pm | Amazing World of Gumball | The Parasite
 4:30 pm | Amazing World of Gumball | The Lady
 4:45 pm | Amazing World of Gumball | The Signal
 5:00 pm | Craig of the Creek | The Final Book
 5:15 pm | Craig of the Creek | Jessica Goes to the Creek
 5:30 pm | Teen Titans Go! | Movie Night
 5:45 pm | Teen Titans Go! | Ones and Zeroes
 6:00 pm | Teen Titans Go! | BBRAE
 6:30 pm | Amazing World of Gumball | The Heist
 6:45 pm | Amazing World of Gumball | The Advice
 7:00 pm | Craig of the Creek | Sunday Clothes
 7:15 pm | Craig of the Creek | The Future Is Cardboard
 7:30 pm | Amazing World of Gumball | The Rival
 7:45 pm | Amazing World of Gumball | The Girlfriend