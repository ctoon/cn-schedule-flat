# 2018-05-15
6:00 a.m. | Steven Universe | Joking Victim
6:15 a.m. | Mighty Magiswords | Goat!
6:30 a.m. | Teen Titans | Divide and Conquer
7:00 a.m. | Ben 10 | Super-Villain Team-Up
7:15 a.m. | Amazing World of Gumball | The Grades
7:30 a.m. | Amazing World of Gumball | Roots/The Blame
8:00 a.m. | Amazing World of Gumball | Detective/The Fury
8:30 a.m. | Teen Titans Go! | Uncle Jokes/Mas y Menos
9:00 a.m. | Teen Titans Go! | Dreams/Grandma Voice
9:30 a.m. | Amazing World of Gumball | The Pony/The Storm
10:00 a.m. | Amazing World of Gumball | The Dream/The Sidekick
10:30 a.m. | Craig of the Creek | Curse/Monster in the Garden
11:00 a.m. | Teen Titans Go! | Love Monsters/Baby Hands
11:30 a.m. | Teen Titans Go! | Sandwich Thief/Money Grandma
12:00 p.m. | Unikitty | License to Punch/Wishing Well
12:30 p.m. | Amazing World of Gumball | The Downer/The Egg
1:00 p.m. | Amazing World of Gumball | The Countdown/The Nobody
1:30 p.m. | We Bare Bears | Bear Cleanse/Charlie Ball
2:00 p.m. | Craig of the Creek | Too Many Treasures/The Brood
2:30 p.m. | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
3:00 p.m. | Teen Titans Go! | Squash & Stretch/Garage Sale
3:30 p.m. | OK K.O.! Let's Be Heroes | Plaza Film Festival/Be a Team
4:00 p.m. | Amazing World of Gumball | Best/The Parasite
4:30 p.m. | Amazing World of Gumball | Lady/The Signal
5:00 p.m. | Craig of the Creek | Final Book/Jessica Goes to the Creek
5:30 p.m. | Teen Titans Go! | Movie Night/Ones and Zeros
6:00 p.m. | Teen Titans Go! | BBRAE
6:30 p.m. | Amazing World of Gumball | Heist/The Advice
7:00 p.m. | Craig of the Creek | Sunday Clothes/The Future is Cardboard
7:30 p.m. | Amazing World of Gumball | Rival/The Girlfriend