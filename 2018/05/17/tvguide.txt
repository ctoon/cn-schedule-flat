# 2018-05-17
 6:00 am | Steven Universe | Monster Buddies
 6:15 am | Mighty Magiswords | Mall of Shame
 6:30 am | Teen Titans | Switched
 7:00 am | Teen Titans | Deep Six
 7:30 am | Amazing World of Gumball | The Boredom; The Guy
 8:00 am | Amazing World of Gumball | The Vision; The Choices
 8:30 am | Teen Titans Go! | Pirates; I See You
 9:00 am | Teen Titans Go! | Brian; Nature
 9:30 am | Amazing World of Gumball | The Limit; The Game
10:00 am | Amazing World of Gumball | The Promise; The Voice
10:30 am | Craig of the Creek | Too Many Treasures; The Brood
11:00 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
11:30 am | Teen Titans Go! | Road Trip
11:45 am | Ben 10 | Super-villain Team-up
12:00 pm | Unikitty | Dinner Apart-y; Kitchen Chaos
12:30 pm | Amazing World of Gumball | The Signature; The Check
 1:00 pm | Amazing World of Gumball | The Others; The Crew
 1:30 pm | We Bare Bears | Captain Craboo
 2:00 pm | Craig of the Creek | Jessica Goes to the Creek; The Final Book
 2:30 pm | Teen Titans Go! | Finally a Lesson; Arms Race With Legs
 3:00 pm | Teen Titans Go! | Obinray; Wally T
 3:30 pm | OK K.O.! Let's Be Heroes | Your World Is An Illusion; The So-bad-ical
 4:00 pm | Amazing World of Gumball | The Sorcerer; The Uploads
 4:30 pm | Amazing World of Gumball | The Line; The Romantic
 5:00 pm | Craig of the Creek | The Future Is Cardboard; The Brood
 5:30 pm | Teen Titans Go! | Master Detective; Throne Of Bones
 6:00 pm | Teen Titans Go! | Hand Zombie; Demon Prom
 6:30 pm | Amazing World of Gumball | The Ex; The Comic
 7:00 pm | Craig of the Creek | Escape From Family Dinner; Bring Out Your Beast
 7:30 pm | Amazing World of Gumball | The Petals; The Upgrade