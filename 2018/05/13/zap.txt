# 2018-05-13
 6:00 am | OK K.O.! Let's Be Heroes | Your World Is an Illusion
 6:15 am | Amazing World of Gumball | The Rival
 6:30 am | Amazing World of Gumball | The Boredom
 6:45 am | Amazing World of Gumball | The Code
 7:00 am | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 7:15 am | Craig of the Creek | The Brood
 7:30 am | Craig of the Creek | The Future Is Cardboard
 7:45 am | Craig of the Creek | Wildernessa
 8:00 am | OK K.O.! Let's Be Heroes | My Fair Carol
 8:15 am | Teen Titans Go! | TV Knight
 8:30 am | Teen Titans Go! | Booby Trap House
 8:45 am | Teen Titans Go! | Employee of the Month Redux
 9:00 am | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 9:15 am | Teen Titans Go! | Fish Water
 9:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:45 am | Teen Titans Go! | Operation Dude Rescue: Part 2
10:00 am | OK K.O.! Let's Be Heroes | Your World Is an Illusion
10:15 am | Teen Titans Go! | Riding the Dragon
10:30 am | Teen Titans Go! | Jinxed
10:45 am | Teen Titans Go! | The Overbite
11:00 am | OK K.O.! Let's Be Heroes | The So-Bad-Ical
11:15 am | Teen Titans Go! | The Cape
11:30 am | Teen Titans Go! | Orangins
11:45 am | Teen Titans Go! | Shrimps and Prime Rib
12:00 pm | OK K.O.! Let's Be Heroes | My Fair Carol
12:15 pm | Amazing World of Gumball | The Lady
12:30 pm | Amazing World of Gumball | The Choices
12:45 pm | Amazing World of Gumball | The Slide
 1:00 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 1:15 pm | Amazing World of Gumball | The Vegging
 1:30 pm | Amazing World of Gumball | The Loophole
 1:45 pm | Amazing World of Gumball | The Outside
 2:00 pm | OK K.O.! Let's Be Heroes | Your World Is an Illusion
 2:15 pm | Craig of the Creek | Dog Decider
 2:30 pm | Craig of the Creek | The Curse
 2:45 pm | Craig of the Creek | Escape From Family Dinner
 3:00 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 3:15 pm | We Bare Bears | Cupcake Job
 3:30 pm | We Bare Bears | Panda's Sneeze
 3:45 pm | We Bare Bears | Brother Up
 4:00 pm | OK K.O.! Let's Be Heroes | My Fair Carol
 4:15 pm | The Powerpuff Girls | The Blossom Files
 4:30 pm | The Powerpuff Girls | Aliver
 4:45 pm | The Powerpuff Girls | Not So Secret Service
 5:00 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 5:15 pm | Amazing World of Gumball | The One
 5:30 pm | Amazing World of Gumball | The Potato
 5:45 pm | Amazing World of Gumball | The Matchmaker
 6:00 pm | OK K.O.! Let's Be Heroes | Your World Is an Illusion
 6:15 pm | Craig of the Creek | The Brood
 6:30 pm | Craig of the Creek | The Final Book
 6:45 pm | Craig of the Creek | Too Many Treasures
 7:00 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 7:15 pm | Teen Titans Go! | Top of the Titans: Beast Boy & Cyborg Songs
 7:30 pm | Teen Titans Go! | BBB Day!
 7:45 pm | Teen Titans Go! | BBSFBDAY