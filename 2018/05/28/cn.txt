# 2018-05-28
 6:00 am | Teen Titans Go! | Lication/Permanent Record
 6:30 am | Teen Titans Go! | Ones and Zeros/Master Detective
 7:00 am | Teen Titans Go! | Career Day/Hand Zombie
 7:30 am | Teen Titans Go! | TV Knight 2/Employee of the Month Redux
 8:00 am | Teen Titans Go! | Academy/Orangins
 8:30 am | Teen Titans Go! | Throne of Bones/Jinxed
 9:00 am | Teen Titans Go! | Demon Prom/Brain Percentages
 9:30 am | Teen Titans Go! | Beast Girl/BL4Z3
10:00 am | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
10:30 am | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
11:00 am | Teen Titans Go! | BBSFBDAY!/BBCYFSHIPBDAY
11:30 am | Teen Titans Go! | Streak Part 1/Streak Part 2/40%, 40%, 20%
12:00 pm | Teen Titans Go! | Night Begins to Shine Special
 1:00 pm | Teen Titans Go! | BBRAE
 1:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:00 pm | SPECIAL | Teen Titans Go!: Island Adventures
 3:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 3:30 pm | Teen Titans Go! | Flashback
 4:00 pm | MOVIE | Alvin and the Chipmunks: The Road Chip
 6:00 pm | Teen Titans Go! | Bro-Pocalypse
 6:10 pm | MOVIE | Lego DC Comics Super Heroes: The Flash