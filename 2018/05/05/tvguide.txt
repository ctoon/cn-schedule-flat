# 2018-05-05
 6:00 am | OK K.O.! Let's Be Heroes | Seasons Change
 6:15 am | Amazing World of Gumball | The Routine
 6:30 am | Amazing World of Gumball | The Comic; The Upgrade
 7:00 am | OK K.O.! Let's Be Heroes | Lord Cowboy Darrell
 7:15 am | Amazing World of Gumball | The Apprentice
 7:30 am | Amazing World of Gumball | The Hug; The Wicked
 8:00 am | OK K.O.! Let's Be Heroes | Plaza Film Festival
 8:15 am | Teen Titans Go! | The Avogodo
 8:30 am | Teen Titans Go! | BBCYFSHIPBDAY
 9:00 am | OK K.O.! Let's Be Heroes | Be A Team
 9:15 am | Teen Titans Go! | A Farce
 9:30 am | Teen Titans Go! | Animals: It's Just a Word; Grube's Fairytales
10:00 am | OK K.O.! Let's Be Heroes | Seasons Change
10:15 am | Teen Titans Go! | BBBday!
10:30 am | Teen Titans Go! | Two Parter: Part One; Two Parter: Part Two
11:00 am | OK K.O.! Let's Be Heroes | Lord Cowboy Darrell
11:15 am | Teen Titans Go! | The Avogodo
11:30 am | Teen Titans Go! | Throne Of Bones; Squash & Stretch
12:00 pm | OK K.O.! Let's Be Heroes | Plaza Film Festival
12:15 pm | Ben 10 | Bounty Ball
12:30 pm | Ben 10 | Half-Sies; Xingo's Back
 1:00 pm | OK K.O.! Let's Be Heroes | Be A Team
 1:15 pm | Amazing World of Gumball | The Origins
 1:30 pm | Amazing World of Gumball | The Origins Part 2; The Girlfriend
 2:00 pm | OK K.O.! Let's Be Heroes | Seasons Change
 2:15 pm | Craig of the Creek | Dog Decider
 2:30 pm | Craig of the Creek | The Curse; Monster in the Garden
 3:00 pm | OK K.O.! Let's Be Heroes | Lord Cowboy Darrell
 3:15 pm | Amazing World of Gumball | The Weirdo
 3:30 pm | Amazing World of Gumball | The Advice; The Signal
 4:00 pm | OK K.O.! Let's Be Heroes | Plaza Film Festival
 4:15 pm | Unikitty | Action Forest
 4:30 pm | Unikitty | License To Punch; Stuck Together
 5:00 pm | OK K.O.! Let's Be Heroes | Be A Team
 5:15 pm | Teen Titans Go! | The Avogodo
 5:30 pm | Teen Titans Go! | Top Of The Titans: Best Love Songs; Top Of The Titans: Raddest Songs
 6:00 pm | OK K.O.! Let's Be Heroes | Seasons Change
 6:15 pm | Amazing World of Gumball | The Heist
 6:30 pm | Amazing World of Gumball | The Parasite; The Love
 7:00 pm | OK K.O.! Let's Be Heroes | Lord Cowboy Darrell
 7:15 pm | Craig of the Creek | The Future is Cardboard
 7:30 pm | Craig of the Creek | Lost In The Sewer; Bring Out Your Beast