# 2018-05-29
 6:00 am | Steven Universe | Island Adventure
 6:15 am | Justice League Action | Watchtower Tours
 6:30 am | Teen Titans | Winner Take All
 7:00 am | Ben 10 | Out to Launch
 7:15 am | Amazing World of Gumball | The Lady
 7:30 am | Amazing World of Gumball | The Best; The Sucker
 8:00 am | Amazing World of Gumball | The Worst; The Vegging
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 9:30 am | Amazing World of Gumball | The Procrastinators; The Shell
10:00 am | Amazing World of Gumball | The Mirror; The Burden
10:30 am | Craig of the Creek | Itch To Explore; The Brood
11:00 am | Teen Titans Go! | Video Game References; Cool School
11:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
12:00 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
12:30 pm | Amazing World of Gumball | The Heist; The Detective
 1:00 pm | Amazing World of Gumball | The Fury; The Singing
 2:00 pm | Alvin and the Chipmunks: The Road Chip | 
 4:00 pm | LEGO DC Comics Super Heroes: The Flash | 
 6:00 pm | Teen Titans Go! | Flashback
 6:30 pm | Amazing World of Gumball | The Origins; The Origins Part Two
 7:00 pm | Craig of the Creek | The Final Book; Itch To Explore
 7:30 pm | Amazing World of Gumball | The Fridge; The Remote