# 2018-01-02
 6:00 am | Unikitty | Crushing Defeat
 6:15 am | Amazing World of Gumball | The Others
 6:30 am | Amazing World of Gumball | Prank/The Mystery
 7:00 am | Unikitty | Action Forest
 7:15 am | Teen Titans Go! | Spice Game
 7:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory/I'm the Sauce
 8:00 am | Unikitty | Kaiju Kitty
 8:15 am | Amazing World of Gumball | The Hug
 8:30 am | Amazing World of Gumball | Wicked/The Traitor
 9:00 am | Unikitty | Fire & Nice
 9:15 am | Teen Titans Go! | Pyramid Scheme
 9:30 am | Teen Titans Go! | Secret Garden/Bottle Episode
10:00 am | Unikitty | Spoooooky Game
10:15 am | Amazing World of Gumball | The Misunderstandings
10:30 am | Amazing World of Gumball | Kiss/The GI
11:00 am | Unikitty | Sparkle Matter Matters
11:15 am | Teen Titans Go! | History Lesson
11:30 am | Teen Titans Go! | Art of Ninjutsu/Think About Your Future
12:00 pm | Unikitty | No Day Like Snow Day
12:15 pm | Amazing World of Gumball | The Copycats
12:30 pm | Amazing World of Gumball | Fuss/The Potato
 1:00 pm | Unikitty | Rock Friend
 1:15 pm | Teen Titans Go! | BBSFBDAY!
 1:30 pm | Teen Titans Go! | Streak: Part 2/The Streak: Part 1
 2:00 pm | Unikitty | Kitchen Chaos
 2:15 pm | Amazing World of Gumball | The Grades
 2:30 pm | Amazing World of Gumball | Ex/The Diet
 3:00 pm | Unikitty | Crushing Defeat
 3:15 pm | Ben 10 | Take 10
 3:30 pm | Ben 10 | Growing Pains/Shhh!
 4:00 pm | Unikitty | Action Forest
 4:15 pm | Teen Titans Go! | Orangins
 4:30 pm | Teen Titans Go! | Brain Percentages/Jinxed
 5:00 pm | Unikitty | Kaiju Kitty
 5:15 pm | OK K.O.! Let's Be Heroes | Let's Have a Stakeout
 5:30 pm | OK K.O.! Let's Be Heroes | Rad Likes Robots/My Dad Can Beat up Your Dad
 6:00 pm | Unikitty | Fire & Nice
 6:15 pm | Amazing World of Gumball | The Petals
 6:30 pm | Amazing World of Gumball | The Party/The Refund
 7:00 pm | Unikitty | Rock Friend
 7:15 pm | Teen Titans Go! | Ones and Zeros
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star