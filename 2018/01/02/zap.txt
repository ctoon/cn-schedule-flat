# 2018-01-02
 6:00 am | Unikitty | Kitchen Chaos
 6:15 am | Amazing World of Gumball | The Return
 6:30 am | Amazing World of Gumball | The Responsible; The Dress
 7:00 am | Unikitty | Crushing Defeat
 7:15 am | Teen Titans Go! | Accept the Next Proposition You Hear
 7:30 am | Teen Titans Go! | The Fourth Wall
 7:45 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 8:00 am | Unikitty | Action Forest
 8:15 am | Amazing World of Gumball | The Romantic
 8:30 am | Amazing World of Gumball | The Uploads
 8:45 am | Amazing World of Gumball | The Apprentice
 9:00 am | Unikitty | Kaiju Kitty
 9:15 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:30 am | Teen Titans Go! | Two Parter: Part One
 9:45 am | Teen Titans Go! | Two Parter: Part Two
10:00 am | Unikitty | Fire & Nice
10:15 am | Amazing World of Gumball | The Points
10:30 am | Amazing World of Gumball | The Laziest; The Ghost
11:00 am | Unikitty | Spoooooky Game
11:15 am | Teen Titans Go! | Booty Scooty
11:30 am | Teen Titans Go! | Who's Laughing Now
11:45 am | Teen Titans Go! | Oregon Trail
12:00 pm | Unikitty | Sparkle Matter Matters
12:15 pm | Amazing World of Gumball | The Test
12:30 pm | Amazing World of Gumball | The Slide
12:45 pm | Amazing World of Gumball | The Loophole
 1:00 pm | Unikitty | No Day Like Snow Day
 1:15 pm | Teen Titans Go! | Movie Night
 1:30 pm | Teen Titans Go! | BBRAE
 2:00 pm | Unikitty | A Rock Friend Indeed
 2:15 pm | Amazing World of Gumball | The Catfish
 2:30 pm | Amazing World of Gumball | The Cycle
 2:45 pm | Amazing World of Gumball | The Stars
 3:00 pm | Unikitty | Kitchen Chaos
 3:15 pm | Ben 10 | The Ring Leader
 3:30 pm | Ben 10 | Riding the Storm Out
 3:45 pm | Ben 10 | The Clocktopus
 4:00 pm | Unikitty | Crushing Defeat
 4:15 pm | Teen Titans Go! | Brain Percentages
 4:30 pm | Teen Titans Go! | BL4Z3
 4:45 pm | Teen Titans Go! | Hot Salad Water
 5:00 pm | Unikitty | Action Forest
 5:15 pm | OK K.O.! Let's Be Heroes | A Hero's Fate
 5:30 pm | OK K.O.! Let's Be Heroes | Villains' Night
 6:00 pm | Unikitty | Kaiju Kitty
 6:15 pm | Amazing World of Gumball | The Best
 6:30 pm | Amazing World of Gumball | The Worst
 6:45 pm | Amazing World of Gumball | The Deal
 7:00 pm | Unikitty | Fire & Nice
 7:15 pm | Teen Titans Go! | Demon Prom
 7:30 pm | Teen Titans Go! | The Academy
 7:45 pm | Teen Titans Go! | Throne of Bones