# 2018-01-18
 6:00 am | Teen Titans | Troq
 6:30 am | Teen Titans | The Prophecy
 7:00 am | Teen Titans Go! | BBCYFSHIPBDAY/Lication
 7:30 am | Teen Titans Go! | Career Day/Ones and Zeros
 8:00 am | Teen Titans Go! | Cruel Giggling Ghoul/Secret Garden
 8:30 am | Teen Titans Go! | Pyramid Scheme/Bottle Episode
 9:00 am | Clarence | Pizza Hero/Sumo Goes West
 9:30 am | Clarence | Clarence Loves Shoopy/Rock Show
10:00 am | Ben 10 | Waterfilter/Need for Speed
10:30 am | Ben 10 | Riding the Storm Out/The Ring Leader
11:00 am | Teen Titans Go! | Shrimps and Prime Rib/Booby Trap House
11:30 am | Teen Titans Go! | TV Knight/Fish Water
12:00 pm | Amazing World of Gumball | Stories/The Guy
12:30 pm | Amazing World of Gumball | The Boredom
12:45 pm | Unikitty | Fire & Nice
 1:00 pm | Adventure Time | Her Parents/To Cut a Woman's Hair
 1:30 pm | Adventure Time | The Chamber of Frozen Blades/The Other Tarts
 2:00 pm | Teen Titans Go! | Legendary Sandwich/Pie Bros
 2:30 pm | Teen Titans Go! | Driver's Ed/Dog Hand
 3:00 pm | Teen Titans Go! | Streak: Part 1/The Streak: Part 2
 3:30 pm | SPECIAL | Teen Titans Go: Island Adventures
 4:30 pm | Teen Titans Go! | Night Begins to Shine Special
 5:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:00 pm | Amazing World of Gumball | One/The Vegging
 6:30 pm | Unikitty | Stuck Together/Hide N' Seek
 7:00 pm | We Bare Bears | Hurricane Hal/Vacation
 7:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star