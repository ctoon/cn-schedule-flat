# 2018-01-01
 6:00 am | Unikitty | Action Forest
 6:15 am | Amazing World of Gumball | The Treasure
 6:30 am | Amazing World of Gumball | The Third; The Debt
 7:00 am | Unikitty | Kaiju Kitty
 7:15 am | Teen Titans Go! | Grube's Fairytales
 7:30 am | Teen Titans Go! | A Farce
 7:45 am | Teen Titans Go! | Animals: It's Just a Word
 8:00 am | Unikitty | Fire & Nice
 8:15 am | Amazing World of Gumball | The Girlfriend
 8:30 am | Amazing World of Gumball | The Origins
 8:45 am | Amazing World of Gumball | The Origins
 9:00 am | Unikitty | A Rock Friend Indeed
 9:15 am | Teen Titans Go! | Finally a Lesson
 9:30 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:45 am | Teen Titans Go! | Operation Dude Rescue: Part 2
10:00 am | Unikitty | Kitchen Chaos
10:15 am | Amazing World of Gumball | The Love
10:30 am | Amazing World of Gumball | The Pressure; The Painting
11:00 am | Unikitty | Crushing Defeat
11:15 am | Teen Titans Go! | Snuggle Time
11:30 am | Teen Titans Go! | Oh Yeah!
11:45 am | Teen Titans Go! | Riding the Dragon
12:00 pm | Unikitty | Action Forest
12:15 pm | Amazing World of Gumball | The Vision
12:30 pm | Amazing World of Gumball | The Choices
12:45 pm | Amazing World of Gumball | The Code
 1:00 pm | Unikitty | Kaiju Kitty
 1:15 pm | Teen Titans Go! | Permanent Record
 1:30 pm | Teen Titans Go! | Titan Saving Time
 1:45 pm | Teen Titans Go! | Master Detective
 2:00 pm | Unikitty | Fire & Nice
 2:15 pm | Amazing World of Gumball | The Box
 2:30 pm | Amazing World of Gumball | The Console
 2:45 pm | Amazing World of Gumball | The Ollie
 3:00 pm | Unikitty | A Rock Friend Indeed
 3:15 pm | Ben 10 | Waterfilter
 3:30 pm | Ben 10 | Xingo
 3:45 pm | Ben 10 | Need for Speed
 4:00 pm | Unikitty | Kitchen Chaos
 4:15 pm | Teen Titans Go! | Lication
 4:30 pm | Teen Titans Go! | Classic Titans
 4:45 pm | Teen Titans Go! | Ones and Zeroes
 5:00 pm | Unikitty | Crushing Defeat
 5:15 pm | OK K.O.! Let's Be Heroes | No More Pow Cards
 5:30 pm | OK K.O.! Let's Be Heroes | T.K.O.
 6:00 pm | Unikitty | Action Forest
 6:15 pm | Amazing World of Gumball | The Heist
 6:30 pm | Amazing World of Gumball | The Mirror; The Burden
 7:00 pm | Unikitty | Kaiju Kitty
 7:15 pm | Teen Titans Go! | BBCYFSHIPBDAY
 7:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!