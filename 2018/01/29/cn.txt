# 2018-01-29
 6:00 am | Amazing World of Gumball | The Responsible/The Dress
 6:30 am | Amazing World of Gumball | The Laziest/The Ghost
 7:00 am | Teen Titans Go! | Burger vs. Burrito/Matched
 7:30 am | Teen Titans Go! | Colors of Raven
 7:45 am | Unikitty | Stuck Together
 8:00 am | Teen Titans Go! | Master Detective/Titan Saving Time
 8:30 am | Teen Titans Go! | Employee of the Month Redux/Hand Zombie
 9:00 am | Clarence | Slumber Party/Dollar Hunt
 9:30 am | Clarence | Honk/Jeff's New Toy
10:00 am | Ben 10 | Bright Lights, Black Hearts/Ben 24hrs
10:30 am | Ben 10 | Don't Let the Bass Drop/Don't Laze Me, Bro
11:00 am | Teen Titans Go! | Double Trouble/The Date
11:30 am | Teen Titans Go! | Dude Relax!/Laundry Day
12:00 pm | Amazing World of Gumball | Heist/The Uncle
12:30 pm | Amazing World of Gumball | The Best
12:45 pm | Unikitty | Little Prince Puppycorn
 1:00 pm | Adventure Time | Wizard/Evicted!
 1:30 pm | Adventure Time | City of Thieves/The Witch's Garden
 2:00 pm | Amazing World of Gumball | Nemesis/The Return
 2:30 pm | Amazing World of Gumball | Crew/The Others
 3:00 pm | We Bare Bears | Bro Brawl/Hurricane Hal
 3:30 pm | We Bare Bears | Vacation
 3:45 pm | Unikitty | Fire & Nice
 4:00 pm | Teen Titans Go! | Power Moves/Staring at the Future
 4:30 pm | Teen Titans Go! | No Power/Sidekick
 5:00 pm | Unikitty | Sparkle Matter Matters
 5:15 pm | OK K.O.! Let's Be Heroes | Let's Be Heroes
 5:30 pm | OK K.O.! Let's Be Heroes | Let's Be Friends/You're Everybody's Sidekick
 6:00 pm | Amazing World of Gumball | Sucker/The Lady
 6:30 pm | Amazing World of Gumball | The Vegging
 6:45 pm | Unikitty | Hide N' Seek
 7:00 pm | Teen Titans Go! | Leg Day/Cat's Fancy
 7:30 pm | Teen Titans Go! | Dignity of Teeth
 7:45 pm | Unikitty | Action Forest