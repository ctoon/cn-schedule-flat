# 2018-01-24
 6:00 am | Teen Titans | Trust
 6:30 am | Teen Titans | For Real
 7:00 am | Teen Titans Go! | Double Trouble/The Date
 7:30 am | Teen Titans Go! | Dude Relax!
 7:45 am | Unikitty | Kitchen Chaos
 8:00 am | Teen Titans Go! | Booty Scooty/Who's Laughing Now
 8:30 am | Teen Titans Go! | Oregon Trail/Snuggle Time
 9:00 am | Clarence | Public Radio/Big Trouble in Little Aberdale
 9:30 am | Clarence | Boxcurse Children/The Big Game
10:00 am | Ben 10 | Brief Career of Lucky Girl/Animo Farm
10:30 am | Ben 10 | Clown College/Adventures in Babysitting
11:00 am | Teen Titans Go! | Orangins/The Avogodo
11:30 am | Teen Titans Go! | Brain Percentages/Jinxed
12:00 pm | Amazing World of Gumball | Vase/The Outside
12:30 pm | Amazing World of Gumball | The Box
12:45 pm | Unikitty | Crushing Defeat
 1:00 pm | Adventure Time | Belly of the Beast/The Limit
 1:30 pm | Adventure Time | Video Makers/Heat Signature
 2:00 pm | Teen Titans | Betrayal
 2:30 pm | Teen Titans | Fractured
 3:00 pm | We Bare Bears | Dance Lessons/Ice Cave
 3:30 pm | We Bare Bears | The Nom Nom Show
 3:45 pm | Unikitty | Wishing Well
 4:00 pm | Teen Titans Go! | Girls' Night Out/You're Fired
 4:30 pm | Teen Titans Go! | Super Robin/Tower Power
 5:00 pm | Unikitty | Hide N' Seek
 5:15 pm | OK K.O.! Let's Be Heroes | We Got Hacked
 5:30 pm | OK K.O.! Let's Be Heroes | T.K.O.
 6:00 pm | Amazing World of Gumball | Worst/The Deal
 6:30 pm | Amazing World of Gumball | The Petals
 6:45 pm | Unikitty | Stuck Together
 7:00 pm | Teen Titans Go! | Kicking a Ball and Pretending to Be Hurt/Head Fruit
 7:30 pm | Teen Titans Go! | Video Game References
 7:45 pm | Unikitty | Action Forest