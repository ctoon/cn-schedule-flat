# 2018-01-15
 6:00 am | Teen Titans | Titans East Part 1
 6:30 am | Teen Titans | Titans East Part 2
 7:00 am | Teen Titans Go! | Hand Zombie/Employee of the Month Redux
 7:30 am | Teen Titans Go! | Orangins/The Avogodo
 8:00 am | Teen Titans Go! | Spice Game/I'm the Sauce
 8:30 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory/Accept the Next Proposition You Hear
 9:00 am | Clarence | Plant Daddies/Bucky and the Howl
 9:30 am | Clarence | Clarence and Sumo's Rexcellent Adventure/Worm Bin
10:00 am | Ben 10 | Waterfilter/The Ring Leader
10:30 am | Ben 10 | Take 10/The Clocktopus
11:00 am | Teen Titans Go! | History Lesson/Rad Dudes with Bad Tudes
11:30 am | Teen Titans Go! | Art of Ninjutsu/Think About Your Future
12:00 pm | Amazing World of Gumball | Nest/The Awkwardness
12:30 pm | Amazing World of Gumball | The Points
12:45 pm | Unikitty | No Day Like Snow Day
 1:00 pm | Adventure Time | Henchman/Dungeon
 1:30 pm | Adventure Time | What Have You Done?/Rainy Day Daydream
 2:00 pm | Teen Titans | Apprentice Part 1
 2:30 pm | Teen Titans | Apprentice Part 2
 3:00 pm | We Bare Bears | Coffee Cave/Anger Management
 3:30 pm | We Bare Bears | The Demon
 3:45 pm | Unikitty | Kaiju Kitty
 4:00 pm | Teen Titans Go! | Brain Percentages/Jinxed
 4:30 pm | Teen Titans Go! | BL4Z3/Hot Salad Water
 5:00 pm | OK K.O.! Let's Be Heroes | Face Your Fears/Stop Attacking the Plaza
 5:30 pm | OK K.O.! Let's Be Heroes | One Last Score
 5:45 pm | Unikitty | Fire & Nice
 6:00 pm | Amazing World of Gumball | Console/The Ollie
 6:30 pm | Amazing World of Gumball | The Catfish
 6:45 pm | Unikitty | Rock Friend
 7:00 pm | Teen Titans Go! | Road Trip/The Best Robin
 7:30 pm | Teen Titans Go! | Boys vs Girls
 7:45 pm | Unikitty | Kitchen Chaos