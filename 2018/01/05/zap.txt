# 2018-01-05
 6:00 am | Amazing World of Gumball | The Parking
 6:15 am | Amazing World of Gumball | The Routine
 6:30 am | Amazing World of Gumball | The Upgrade
 6:45 am | Amazing World of Gumball | The Comic
 7:00 am | Unikitty | Action Forest
 7:15 am | Unikitty | Kaiju Kitty
 7:30 am | Teen Titans Go! | Cat's Fancy
 7:45 am | Teen Titans Go! | Some of Their Parts
 8:00 am | Teen Titans Go! | Demon Prom
 8:15 am | Teen Titans Go! | BBCYFSHIPBDAY
 8:30 am | Teen Titans Go! | The Academy
 8:45 am | Teen Titans Go! | Throne of Bones
 9:00 am | Unikitty | Fire & Nice
 9:15 am | Unikitty | A Rock Friend Indeed
 9:30 am | Amazing World of Gumball | The Compilation
 9:45 am | Amazing World of Gumball | The Stories
10:00 am | Amazing World of Gumball | The Guy
10:15 am | Amazing World of Gumball | The Boredom
10:30 am | Amazing World of Gumball | The Disaster
10:45 am | Amazing World of Gumball | The Re-Run
11:00 am | Unikitty | Kitchen Chaos
11:15 am | Unikitty | Crushing Defeat
11:30 am | Teen Titans Go! | Arms Race With Legs
11:45 am | Teen Titans Go! | Obinray
12:00 pm | Teen Titans Go! | Career Day
12:15 pm | Teen Titans Go! | TV Knight 2
12:30 pm | Teen Titans Go! | Classic Titans
12:45 pm | Teen Titans Go! | Ones and Zeroes
 1:00 pm | Unikitty | Kaiju Kitty
 1:15 pm | Unikitty | A Rock Friend Indeed
 1:30 pm | Teen Titans Go! | Caramel Apples
 1:45 pm | Teen Titans Go! | Multiple Trick Pony
 2:00 pm | Teen Titans Go! | The Hive Five
 2:15 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob
 2:30 pm | Teen Titans Go! | Cat's Fancy
 2:40 pm | Teen Titans Go! | Animals, It's Just a Word!
 2:50 pm | Teen Titans Go! | Island Adventures
 4:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 5:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 5:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:00 pm | Amazing World of Gumball | The Rival
 6:15 pm | Amazing World of Gumball | The Lady
 6:30 pm | Unikitty | Wishing Well
 6:45 pm | Unikitty | Action Forest
 7:00 pm | We Bare Bears | Bearz II Men
 7:15 pm | We Bare Bears | Bro Brawl
 7:30 pm | Steven Universe | 