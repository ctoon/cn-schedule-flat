# 2018-01-13
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Amazing World of Gumball | The Friend; The Saint
 7:30 am | Amazing World of Gumball | The Society; The Spoiler
 8:00 am | Amazing World of Gumball | The Helmet; The Fight
 8:30 am | Amazing World of Gumball | The End; The DVD
 9:00 am | Amazing World of Gumball | The Knights; The Colossus
 9:30 am | Amazing World of Gumball | The Fridge; The Remote
10:00 am | Amazing World of Gumball | The Laziest
10:15 am | Amazing World of Gumball | The Apprentice
10:30 am | Amazing World of Gumball | The Hug
10:45 am | Amazing World of Gumball | The Wicked
11:00 am | Amazing World of Gumball | The Traitor
11:15 am | Amazing World of Gumball | The Girlfriend
11:30 am | Amazing World of Gumball | The Advice
11:45 am | Amazing World of Gumball | The Signal
12:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
12:30 pm | Amazing World of Gumball | The Authority; The Virus
 1:00 pm | Amazing World of Gumball | The Pony; The Storm
 1:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 2:00 pm | Amazing World of Gumball | The Laziest
 2:15 pm | Amazing World of Gumball | The Choices
 2:30 pm | Amazing World of Gumball | The Code
 2:45 pm | Amazing World of Gumball | The Test
 3:00 pm | Amazing World of Gumball | The Slide
 3:15 pm | Amazing World of Gumball | The Loophole
 3:30 pm | Amazing World of Gumball | The Copycats
 3:45 pm | Amazing World of Gumball | The Potato
 4:00 pm | Amazing World of Gumball | The Boombox; The Castle
 4:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 5:00 pm | Amazing World of Gumball | The Internet; The Plan
 5:30 pm | Amazing World of Gumball | The World; The Finale
 6:00 pm | Amazing World of Gumball | The Cycle
 6:15 pm | Amazing World of Gumball | The Stars
 6:30 pm | Amazing World of Gumball | The Grades
 6:45 pm | Amazing World of Gumball | The Diet
 7:00 pm | Amazing World of Gumball | The Ex
 7:15 pm | Amazing World of Gumball | The Sorcerer
 7:30 pm | Amazing World of Gumball | The Menu
 7:45 pm | Amazing World of Gumball | The Uncle