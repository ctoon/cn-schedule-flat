# 2018-01-21
 6:00 am | Teen Titans | Sum of His Parts
 6:30 am | Teen Titans | Nevermore
 7:00 am | Teen Titans Go! | TV Knight 2/The Academy
 7:30 am | Teen Titans Go! | Throne of Bones
 7:45 am | Unikitty | Fire & Nice
 8:00 am | Teen Titans Go! | Finally a Lesson/Arms Race with Legs
 8:30 am | Teen Titans Go! | Obinray/Wally T
 9:00 am | Clarence | Missing Cat/Clarence for President
 9:30 am | Clarence | Officer Moody/Gilben's Different
10:00 am | Ben 10 | Clocktopus/Take 10
10:30 am | Ben 10 | Shhh!/Growing Pains
11:00 am | Teen Titans Go! | Inner Beauty of a Cactus/BBSFBDAY!
11:30 am | Teen Titans Go! | Movie Night/Permanent Record
12:00 pm | Amazing World of Gumball | Code/The Choices
12:30 pm | Amazing World of Gumball | The Test
12:45 pm | Unikitty | Rock Friend
 1:00 pm | Adventure Time | The Pods/The Silent King
 1:30 pm | Adventure Time | Real You, The/Guardians of Sunshine
 2:00 pm | Teen Titans | Date with Destiny
 2:30 pm | Teen Titans | Transformation
 3:00 pm | We Bare Bears | Panda's Art/Summer Love
 3:30 pm | We Bare Bears | Citizen Tabes
 3:45 pm | Unikitty | Kitchen Chaos
 4:00 pm | Teen Titans Go! | Double Trouble/The Date
 4:30 pm | Teen Titans Go! | Dude Relax!/Laundry Day
 5:00 pm | Unikitty | Crushing Defeat
 5:15 pm | OK K.O.! Let's Be Heroes | I Am Dendy
 5:30 pm | OK K.O.! Let's Be Heroes | Presenting Joe Cuppa/Sibling Rivalry
 6:00 pm | Amazing World of Gumball | Uncle/The Menu
 6:30 pm | Amazing World of Gumball | The Weirdo
 6:45 pm | Unikitty | Wishing Well
 7:00 pm | Teen Titans Go! | Rocks and Water/Multiple Trick Pony
 7:30 pm | Teen Titans Go! | Tamaranian Vacation
 7:45 pm | Unikitty | Hide N' Seek