# 2018-10-02
 6:00 am | Amazing World of Gumball | Traitor/The Detective
 6:30 am | Amazing World of Gumball | Origins/The Origins Part 2
 7:00 am | Amazing World of Gumball | The Mystery/The Prank
 7:30 am | Amazing World of Gumball | The GI/The Kiss
 8:00 am | Teen Titans Go! | Riding the Dragon/BBCYFSHIPBDAY
 8:30 am | Teen Titans Go! | Overbite/Beast Girl
 9:00 am | Teen Titans Go! | Caramel Apples/Halloween
 9:30 am | Teen Titans Go! | Sandwich Thief/Money Grandma
10:00 am | Amazing World of Gumball | Pest/The Fuss
10:30 am | Amazing World of Gumball | Sale/The Outside
11:00 am | Amazing World of Gumball | Advice/The Disaster
11:30 am | Amazing World of Gumball | Re-Run/The Signal
12:00 pm | Teen Titans Go! | BBBDay!/Rad Dudes with Bad Tudes
12:30 pm | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
 1:00 pm | Amazing World of Gumball | Halloween/The Treasure
 1:30 pm | Amazing World of Gumball | The Words/The Apology
 2:00 pm | Teen Titans Go! | Double Trouble/The Date
 2:30 pm | Teen Titans Go! | Dude Relax!/Laundry Day
 3:00 pm | Ben 10 | Drone On
 3:15 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 3:30 pm | Amazing World of Gumball | Father/The Founder
 4:00 pm | Amazing World of Gumball | Cringe/The Schooling
 4:30 pm | Amazing World of Gumball | Cage/The Intelligence
 5:00 pm | Craig of the Creek | Power Punchers/The Kid From 3030
 5:30 pm | Total DramaRama | Cluckwork Orange/Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Dreams/Grandma Voice
 6:30 pm | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
 7:00 pm | We Bare Bears | Subway/Neighbors
 7:30 pm | Total DramaRama | Free Chili/Aquarium for a Dream