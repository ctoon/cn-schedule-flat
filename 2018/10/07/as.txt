# 2018-10-07
 8:00 pm | Amazing World of Gumball | Stars/The Uploads
 8:30 pm | Amazing World of Gumball | Grades/The Apprentice
 9:00 pm | Home Movies | Therapy
 9:30 pm | Bob's Burgers | Fort Night
10:00 pm | American Dad | Hot Water
10:30 pm | Family Guy | Family Guy Viewer Mail #1
11:00 pm | Family Guy | Family Guy Viewer Mail #2
11:30 pm | Rick and Morty | Something Ricked This Way Comes
12:00 am | Venture Brothers  | The Saphrax Protocol
12:30 am | Mike Tyson Mysteries | The Bard's Curse
12:45 am | Hot Streets | The Final Stand
 1:00 am | DREAM CORP LLC | You Down with OCD?
 1:15 am | Your Pretty Face is Going to Hell | Three Demons and a Demon Baby
 1:30 am | American Dad | Hot Water
 2:00 am | Family Guy | Family Guy Viewer Mail #1
 2:30 am | Family Guy | Family Guy Viewer Mail #2
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Venture Brothers  | The Saphrax Protocol
 4:00 am | Mike Tyson Mysteries | The Bard's Curse
 4:15 am | Hot Streets | The Final Stand
 4:30 am | DREAM CORP LLC | You Down with OCD?
 4:45 am | Your Pretty Face is Going to Hell | Three Demons and a Demon Baby
 5:00 am | Bob's Burgers | Fort Night
 5:30 am | Home Movies | Therapy