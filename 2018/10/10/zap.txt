# 2018-10-10
 6:00 am | Amazing World of Gumball | The Night
 6:15 am | Amazing World of Gumball | The Slide
 6:30 am | Amazing World of Gumball | The Misunderstandings
 6:45 am | Amazing World of Gumball | The Loophole
 7:00 am | Amazing World of Gumball | The Helmet; The Fight
 7:30 am | Amazing World of Gumball | The End; The DVD
 8:00 am | Teen Titans Go! | BBRAE
 8:30 am | Teen Titans Go! | Hot Salad Water
 8:45 am | Teen Titans Go! | Permanent Record
 9:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 9:30 am | Teen Titans Go! | Video Game References; Cool School
10:00 am | Amazing World of Gumball | The Traitor
10:15 am | Amazing World of Gumball | The Ex
10:30 am | Amazing World of Gumball | The Sorcerer
10:45 am | Amazing World of Gumball | The Menu
11:00 am | Amazing World of Gumball | The Detective
11:15 am | Amazing World of Gumball | The Fuss
11:30 am | Bunnicula | Collar Me Crazy
11:45 am | Bunnicula | Brussel Boy
12:00 pm | Teen Titans Go! | Wally T
12:15 pm | Teen Titans Go! | The Overbite
12:30 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
12:45 pm | Teen Titans Go! | Shrimps and Prime Rib
 1:00 pm | Amazing World of Gumball | The Boombox; The Castle
 1:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 2:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 2:30 pm | Teen Titans Go! | No Power; Sidekick
 3:00 pm | Ben 10 | The Feels
 3:15 pm | Teen Titans Go! | Secret Garden
 3:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 4:00 pm | Amazing World of Gumball | The Hero; The Photo
 4:30 pm | Amazing World of Gumball | The Tag; The Lesson
 5:00 pm | Craig of the Creek | Jessica Goes to the Creek
 5:15 pm | Craig of the Creek | Power Punchers
 5:30 pm | Total DramaRama | Cone in 60 Seconds
 5:45 pm | Total DramaRama | Free Chili
 6:00 pm | Teen Titans Go! | My Name Is Jose
 6:15 pm | Teen Titans Go! | The Power of Shrimps
 6:30 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 6:45 pm | Teen Titans Go! | Bro-Pocalypse
 7:00 pm | We Bare Bears | The Kitty
 7:15 pm | We Bare Bears | Dog Hotel
 7:30 pm | Total DramaRama | Germ Factory
 7:45 pm | Total DramaRama | Duck Duck Juice
 8:00 pm | Amazing World of Gumball | The List
 8:15 pm | Amazing World of Gumball | The Scam
 8:30 pm | Amazing World of Gumball | The News
 8:45 pm | Amazing World of Gumball | The Code