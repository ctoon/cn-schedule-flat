# 2018-10-04
 8:00 pm | Amazing World of Gumball | Uncle/The Vase
 8:30 pm | Amazing World of Gumball | Heist/The Outside
 9:00 pm | American Dad | Lincoln Lover
 9:30 pm | American Dad | Dungeons and Wagons
10:00 pm | Bob's Burgers | Purple Rain-Union
10:30 pm | Bob's Burgers | Bob and Deliver
11:00 pm | Family Guy | Death Has a Shadow
11:30 pm | Family Guy | I Never Met the Dead Man
12:00 am | Rick and Morty | A Rickle in Time
12:30 am | Robot Chicken | Nutcracker Sweet
12:45 am | Robot Chicken | Gold Dust Gasoline
 1:00 am | Harvey Birdman | Incredible Hippo
 1:15 am | 12oz Mouse | Meat Warrior
 1:30 am | Bob's Burgers | Purple Rain-Union
 2:00 am | Bob's Burgers | Bob and Deliver
 2:30 am | Family Guy | Death Has a Shadow
 3:00 am | Family Guy | I Never Met the Dead Man
 3:30 am | Rick and Morty | A Rickle in Time
 4:00 am | Black Dynamite | "Roots: The White Album" or "The Blacker the Community the Deeper the Roots!"
 4:30 am | Harvey Birdman | Incredible Hippo
 4:45 am | 12oz Mouse | Meat Warrior
 5:00 am | American Dad | Lincoln Lover
 5:30 am | American Dad | Dungeons and Wagons