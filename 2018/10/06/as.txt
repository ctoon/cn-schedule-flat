# 2018-10-06
 9:00 pm | Dragon Ball Z Kai | The Enemy is Goku's Brother?! The Secret of the Mighty Saiyan Warriors!
 9:30 pm | My Hero Academia | Battle On, Challengers
10:00 pm | Naruto:Shippuden | Storage
10:30 pm | Boruto: Naruto Next Generations | The Hokage's Son!
11:00 pm | Dragon Ball Super | Goku Must Pay! The Warrior of Justice Top Barges In!!
11:30 pm | FLCL: Alternative | Shake It Off
12:00 am | Attack on Titan | Wish
12:30 am | JoJo's Bizarre Adventure: Diamond Is Unbreakable | Toshikazu Hazamada
 1:00 am | Black Clover | The Water Girl Grows Up
 1:30 am | Hunter x Hunter | Divide x and x Conquer
 2:00 am | One-Punch Man | The Deep Sea King
 2:30 am | Lupin The 3rd Part 4 | The Murdering Marionette
 3:00 am | Cowboy Bebop | The Real Folk Blues -- Part 1
 3:30 am | Samurai Jack | II
 4:00 am | Boondocks  | The Real
 4:30 am | Black Dynamite | "Black Jaws!" or "Finger Lickin' Chicken of the Sea"
 5:00 am | Venture Brothers  | The Forecast Manufacturer
 5:30 am | Home Movies | Dad