# 2018-10-01
 6:00 am | Amazing World of Gumball | The Hug
 6:15 am | Amazing World of Gumball | The Roots
 6:30 am | Amazing World of Gumball | The Wicked
 6:45 am | Amazing World of Gumball | The Blame
 7:00 am | Amazing World of Gumball | The Responsible; The Dress
 7:30 am | Amazing World of Gumball | The Laziest; The Ghost
 8:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 8:30 am | Teen Titans Go! | Snuggle Time
 8:45 am | Teen Titans Go! | Oh Yeah!
 9:00 am | Teen Titans Go! | Salty Codgers; Knowledge
 9:30 am | Teen Titans Go! | Love Monsters; Baby Hands
10:00 am | Amazing World of Gumball | The Signature
10:15 am | Amazing World of Gumball | The Copycats
10:30 am | Amazing World of Gumball | The Check
10:45 am | Amazing World of Gumball | The Potato
11:00 am | Bunnicula | Curse of the Weredude
11:15 am | Bunnicula | Sunn Day Bunn Day
11:30 am | Bunnicula | Brussel Boy
11:45 am | Bunnicula | The Juicy Problem
12:00 pm | Teen Titans Go! | Scary Figure Dance
12:15 pm | Teen Titans Go! | Obinray
12:30 pm | Teen Titans Go! | Animals: It's Just a Word!
12:45 pm | Teen Titans Go! | Wally T
 1:00 pm | Amazing World of Gumball | The Flower; The Banana
 1:30 pm | Amazing World of Gumball | The Phone; The Job
 2:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 2:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 3:00 pm | Ben 10 | Vote Zombozo
 3:15 pm | Teen Titans Go! | The Spice Game
 3:30 pm | Amazing World of Gumball | The Sucker
 3:45 pm | Amazing World of Gumball | The Shippening
 4:00 pm | Amazing World of Gumball | The Vegging
 4:15 pm | Amazing World of Gumball | The Brain
 4:30 pm | Amazing World of Gumball | The One
 4:45 pm | Amazing World of Gumball | The Parents
 5:00 pm | Craig of the Creek | The Kid From 3030
 5:15 pm | Craig of the Creek | Big Pinchy
 5:30 pm | Total DramaRama | The Date
 5:45 pm | Total DramaRama | Cuttin' Corners
 6:00 pm | Teen Titans Go! | Little Buddies; Missing
 6:30 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 7:00 pm | We Bare Bears | Grizz Helps
 7:15 pm | We Bare Bears | Yuri and the Bear
 7:30 pm | Total DramaRama | Germ Factory
 7:45 pm | Total DramaRama | Duck Duck Juice
 8:00 pm | Amazing World of Gumball | The Stars
 8:15 pm | Amazing World of Gumball | The Cycle
 8:30 pm | Amazing World of Gumball | The Grades
 8:45 pm | Adventure Time | Diamonds and Lemons