# 2018-10-27
 6:00 am | Teen Titans Go! | Parasite; Starliar
 6:30 am | Transformers: Cyberverse | Shadowstriker
 6:45 am | Teen Titans Go! | Meatball Party
 7:00 am | Teen Titans Go! | Terra-ized; Artful Dodgers
 7:30 am | Teen Titans Go! | Burger vs. Burrito; Matched
 8:00 am | Amazing World of Gumball | The Ad
 8:15 am | Amazing World of Gumball | The Night
 8:30 am | Amazing World of Gumball | The Understanding
 8:45 am | Amazing World of Gumball | The Misunderstandings
 9:00 am | Total DramaRama | Tiger Fail
 9:15 am | Total DramaRama | That's a Wrap
 9:30 am | Total DramaRama | The Bad Guy Busters
 9:45 am | Total DramaRama | The Date
10:00 am | Teen Titans Go! | Kabooms
10:30 am | Teen Titans Go! | Colors of Raven; The Left Leg
11:00 am | Teen Titans Go! | Books; Lazy Sunday
11:30 am | Teen Titans Go! | Power Moves; Staring at the Future
12:00 pm | Ben 10 | Innervasion
 1:15 pm | Teen Titans Go! | No Power
 1:30 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
 2:00 pm | Amazing World of Gumball | The Transformation
 2:15 pm | Amazing World of Gumball | The Roots
 2:30 pm | Amazing World of Gumball | The Spinoffs
 2:45 pm | Amazing World of Gumball | The Blame
 3:00 pm | Amazing World of Gumball | The Potion
 3:15 pm | Amazing World of Gumball | The Detective
 3:30 pm | Amazing World of Gumball | The Intelligence
 3:45 pm | Amazing World of Gumball | The Fury
 4:00 pm | Total DramaRama | Tiger Fail
 4:15 pm | Total DramaRama | Free Chili
 4:30 pm | Total DramaRama | Cluckwork Orange
 4:45 pm | Total DramaRama | Duck Duck Juice
 5:00 pm | Amazing World of Gumball | The Pact
 5:15 pm | Amazing World of Gumball | The Vision
 5:30 pm | Amazing World of Gumball | The Anybody
 5:45 pm | Amazing World of Gumball | The Choices
 6:00 pm | Puss in Boots | 
 8:00 pm | Amazing World of Gumball | The Schooling
 8:15 pm | Amazing World of Gumball | The Compilation
 8:30 pm | Amazing World of Gumball | The Founder
 8:45 pm | Amazing World of Gumball | The Stories