# 2018-10-18
 8:00 pm | Amazing World of Gumball | Anybody/The Blame
 8:30 pm | Amazing World of Gumball | Pact/The Roots
 9:00 pm | American Dad | 42-Year-Old Virgin
 9:30 pm | American Dad | Surro-Gate
10:00 pm | Bob's Burgers | Best Burger
10:30 pm | Bob's Burgers | Tina Tailor Soldier Spy
11:00 pm | Family Guy | Let's Go to the Hop
11:30 pm | Family Guy | Dammit Janet
12:00 am | Rick and Morty | The Rickshank Rickdemption
12:30 am | Robot Chicken | Federated Resources
12:45 am | Robot Chicken | Easter Basket
 1:00 am | Aqua Teen | Mayhem of the Mooninites
 1:15 am | Squidbillies | Chalky Trouble
 1:30 am | Bob's Burgers | Best Burger
 2:00 am | Bob's Burgers | Tina Tailor Soldier Spy
 2:30 am | Family Guy | Let's Go to the Hop
 3:00 am | Family Guy | Dammit Janet
 3:30 am | Rick and Morty | The Rickshank Rickdemption
 4:00 am | Joe Pera Talks With You | Joe Pera Talks To You About The Rat Wars of Alberta, Canada, 1950 - Present Day
 4:15 am | Check It Out! | Pleasure
 4:30 am | Aqua Teen | Mayhem of the Mooninites
 4:45 am | Squidbillies | Chalky Trouble
 5:00 am | American Dad | 42-Year-Old Virgin
 5:30 am | American Dad | Surro-Gate