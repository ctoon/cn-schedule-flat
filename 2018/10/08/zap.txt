# 2018-10-08
 6:00 am | Amazing World of Gumball | The Awkwardness
 6:15 am | Amazing World of Gumball | The Vision
 6:30 am | Amazing World of Gumball | The Nest
 6:45 am | Amazing World of Gumball | The Choices
 7:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 7:30 am | Amazing World of Gumball | The Quest; The Spoon
 8:00 am | Teen Titans Go! | Chicken in the Cradle
 8:15 am | Teen Titans Go! | The Scoop
 8:30 am | Teen Titans Go! | Kabooms
 9:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
 9:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
10:00 am | Amazing World of Gumball | The Uploads
10:15 am | Amazing World of Gumball | The Cycle
10:30 am | Amazing World of Gumball | The Apprentice
10:45 am | Amazing World of Gumball | The Stars
11:00 am | Amazing World of Gumball | The Night
11:15 am | Amazing World of Gumball | The Slide
11:30 am | Amazing World of Gumball | The Misunderstandings
11:45 am | Amazing World of Gumball | The Loophole
12:00 pm | Teen Titans Go! | Bottle Episode
12:15 pm | Teen Titans Go! | Oregon Trail
12:30 pm | Teen Titans Go! | Finally a Lesson
12:45 pm | Teen Titans Go! | Snuggle Time
 1:00 pm | Amazing World of Gumball | The Hero; The Photo
 1:30 pm | Amazing World of Gumball | The Tag; The Lesson
 2:00 pm | Teen Titans Go! | TV Knight
 2:15 pm | Teen Titans Go! | TV Knight 2
 2:30 pm | Teen Titans Go! | TV Knight 3
 2:45 pm | Lego Batman: The Movie - DC Super Heroes Unite | 
 4:15 pm | Lego DC Comics Super Heroes: The Flash | 
 6:00 pm | Teen Titans Go! | My Name Is Jose
 6:15 pm | Teen Titans Go! | The Power of Shrimps
 6:30 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 6:45 pm | MineCon Earth Remix | 
 7:00 pm | Teen Titans Go! | My Name Is Jose
 7:15 pm | Teen Titans Go! | The Power of Shrimps
 7:30 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 7:45 pm | MineCon Earth Remix | 
 8:00 pm | Amazing World of Gumball | The Deal
 8:15 pm | Amazing World of Gumball | The Copycats
 8:30 pm | Amazing World of Gumball | The Origins
 8:45 pm | Amazing World of Gumball | The Origins