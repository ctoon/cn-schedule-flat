# 2018-10-11
 6:00 am | Amazing World of Gumball | The Roots; The Copycats
 6:30 am | The Amazing World Of Gumball | The Blame; The Potato
 7:00 am | Amazing World of Gumball | The Knights; The Colossus
 7:30 am | Amazing World of Gumball | The Fridge; The Remote
 8:00 am | Teen Titans Go! | Titan Saving Time; Lication
 8:30 am | Teen Titans Go! | Ones And Zeros; Master Detective
 9:00 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 9:30 am | Teen Titans Go! | Operation Tin Man; Nean
10:00 am | Amazing World of Gumball | The Origins; The Origins Part Two
10:30 am | Amazing World of Gumball | The Girlfriend; The Uncle
11:00 am | Amazing World of Gumball | The Compilation; The Vase
11:30 am | Bunnicula | My Imaginary Fiend; The Juicy Problem
12:00 pm | Teen Titans Go! | Caged Tiger; Nose Mouth
12:30 pm | Teen Titans Go! | Legs; Breakfast Cheese
 1:00 pm | Amazing World of Gumball | The Internet; The Plan
 1:30 pm | Amazing World of Gumball | The World; The Finale
 2:00 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 2:30 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 2:45 pm | Teen Titans Go! | Halloween vs. Christmas
 3:00 pm | Ben 10 | Past Aliens Present
 3:15 pm | Teen Titans Go! | Pyramid Scheme
 3:30 pm | Amazing World of Gumball | The Limit; The Game
 4:00 pm | Amazing World of Gumball | The Promise; The Voice
 4:30 pm | Amazing World of Gumball | The Boombox; The Castle
 5:00 pm | Craig of the Creek | The Final Book; The Kid From 3030
 5:30 pm | Total DramaRama | Sharing Is Caring; Aquarium for a Dream
 6:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 6:30 pm | Teen Titans Go! | Smile Bones; Real Boy Adventures
 7:00 pm | We Bare Bears | Beehive; Baby Bears Can't Jump
 7:30 pm | Total DramaRama | Venthalla; Cluckwork Orange
 8:00 pm | Amazing World of Gumball | The Rival; The Choices
 8:30 pm | Amazing World of Gumball | The Lady; The Vision