# 2018-10-19
 6:00 am | Amazing World of Gumball | The Grades; The Code
 6:30 am | Amazing World of Gumball | The Diet; The Test
 7:00 am | Amazing World of Gumball | The Tag; The Lesson
 7:30 am | Amazing World of Gumball | The Limit; The Game
 8:00 am | Teen Titans Go! | Dude Relax!; Laundry Day
 8:30 am | Teen Titans Go! | Ghost Boy; La Larva Amor
 9:00 am | Teen Titans Go! | The Fourth Wall; Operation Dude Rescue Part 1
 9:30 am | Teen Titans Go! | Operation Dude Rescue Part 2; 40%, 40%, 20%
10:00 am | Amazing World of Gumball | The Roots; The Sucker
10:30 am | Amazing World of Gumball | The Blame; The Vegging
11:00 am | Amazing World of Gumball | The Menu; The Copycats
11:30 am | Bunnicula | The Juicy Problem; Pranks for the Memories
12:00 pm | Teen Titans Go! | Titan Saving Time; Shrimps and Prime Rib
12:30 pm | Teen Titans Go! | Halloween Vs. Christmas; Master Detective
 1:00 pm | Amazing World of Gumball | The Goons; The Secret
 1:30 pm | Amazing World of Gumball | The Sock; The Genius
 2:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
 2:30 pm | Teen Titans Go! | Baby Hands; Love Monsters
 3:00 pm | Ben 10 | The Sound and the Furry
 3:15 pm | Teen Titans Go! | Who's Laughing Now
 3:30 pm | Teen Titans Go! | Flashback
 4:00 pm | The Flash | 
 5:45 pm | Unikitty | Scary Tales
 6:00 pm | Teen Titans Go! | Monster Squad!; Costume Contest
 6:30 pm | Teen Titans Go! | Scary Figure Dance
 7:00 pm | We Bare Bears | Charlie's Halloween Thing 2
 7:30 pm | Amazing World of Gumball | The Scam; Halloween
 8:00 pm | Amazing World of Gumball | The Ghouls; The Mirror
 8:30 pm | Amazing World of Gumball | The Sorcerer; The Potion