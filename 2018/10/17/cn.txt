# 2018-10-17
 6:00 am | Amazing World of Gumball | Guy/The Ollie
 6:30 am | Amazing World of Gumball | Boredom/The Catfish
 7:00 am | Amazing World of Gumball | The Authority/The Virus
 7:30 am | Amazing World of Gumball | The Pony/The Storm
 8:00 am | Teen Titans Go! | BL4Z3/Throne of Bones
 8:30 am | Teen Titans Go! | Legendary Sandwich/Pie Bros
 9:00 am | Teen Titans Go! | Spice Game/Arms Race with Legs
 9:30 am | Teen Titans Go! | I'm the Sauce/Obinray
10:00 am | Amazing World of Gumball | Points/The List
10:30 am | Amazing World of Gumball | Bus/The News
11:00 am | Amazing World of Gumball | Code/The Grades
11:30 am | Bunnicula | Sunday Bunnday/Collar Me Crazy
12:00 pm | Teen Titans Go! | Snuggle Time/Movie Night
12:30 pm | Teen Titans Go! | Oh Yeah!/Riding the Dragon
 1:00 pm | Amazing World of Gumball | The Mystery/The Prank
 1:30 pm | Amazing World of Gumball | The GI/The Kiss
 2:00 pm | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
 2:30 pm | Teen Titans Go! | Mr. Butt/Man Person
 3:00 pm | Ben 10 | Safari Sa'Bad
 3:15 pm | Teen Titans Go! | History Lesson
 3:30 pm | Amazing World of Gumball | The Boss/The Move
 4:00 pm | Amazing World of Gumball | The Law/The Allergy
 4:30 pm | Amazing World of Gumball | The Mothers/The Password
 5:00 pm | Craig of the Creek | Escape from Family Dinner/Doorway to Helen
 5:30 pm | Total DramaRama | Bad Guy Busters/Ant We All Just Get Along
 6:00 pm | Teen Titans Go! | Operation Tin Man/Nean
 6:30 pm | Teen Titans Go! | Campfire Stories/The Hive Five
 7:00 pm | We Bare Bears | Bro Brawl/Bearz II Men
 7:30 pm | Total DramaRama | Free Chili/Sharing Is Caring