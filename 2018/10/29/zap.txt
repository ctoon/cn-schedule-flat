# 2018-10-29
 6:00 am | Amazing World of Gumball | The Ollie
 6:15 am | Amazing World of Gumball | The Puppets
 6:30 am | Amazing World of Gumball | The Catfish
 6:45 am | Amazing World of Gumball | The Line
 7:00 am | Amazing World of Gumball | The Ghouls
 7:15 am | Amazing World of Gumball | The Scam
 7:30 am | Amazing World of Gumball | The Boss; The Move
 8:00 am | Teen Titans Go! | No Power; Sidekick
 8:30 am | Teen Titans Go! | Caged Tiger; Nose Mouth
 9:00 am | Teen Titans Go! | The Cruel Giggling Ghoul
 9:15 am | Teen Titans Go! | Brain Percentages
 9:30 am | Teen Titans Go! | Pyramid Scheme
 9:45 am | Teen Titans Go! | BL4Z3
10:00 am | Amazing World of Gumball | Halloween
10:15 am | Amazing World of Gumball | The Vacation
10:30 am | Amazing World of Gumball | The Diet
10:45 am | Amazing World of Gumball | The Lady
11:00 am | Amazing World of Gumball | The Procrastinators; The Shell
11:30 am | Amazing World of Gumball | The Mirror; The Burden
12:00 pm | Teen Titans Go! | Birds; Be Mine
12:30 pm | Teen Titans Go! | Caramel Apples; Halloween
 1:00 pm | Amazing World of Gumball | The Ghouls
 1:15 pm | Amazing World of Gumball | The Scam
 1:30 pm | Amazing World of Gumball | The Phone; The Job
 2:00 pm | Teen Titans Go! | Arms Race With Legs
 2:15 pm | Teen Titans Go! | Classic Titans
 2:30 pm | Teen Titans Go! | Obinray
 2:45 pm | Teen Titans Go! | Ones and Zeroes
 3:00 pm | Ben 10 | Dreamtime
 3:15 pm | Amazing World of Gumball | The Watch
 3:30 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:00 pm | Amazing World of Gumball | Halloween
 4:15 pm | Amazing World of Gumball | The Vacation
 4:30 pm | Amazing World of Gumball | The Gift
 4:45 pm | Amazing World of Gumball | The Copycats
 5:00 pm | Craig of the Creek | Secret Book Club
 5:15 pm | Craig of the Creek | The Future Is Cardboard
 5:30 pm | Total DramaRama | Tiger Fail
 5:45 pm | Total DramaRama | Cuttin' Corners
 6:00 pm | Teen Titans Go! | BBRAE
 6:30 pm | Teen Titans Go! | Monster Squad!
 6:45 pm | Teen Titans Go! | Costume Contest
 7:00 pm | We Bare Bears | Paperboyz
 7:15 pm | We Bare Bears | Hot Sauce
 7:30 pm | Total DramaRama | The Bad Guy Busters
 7:45 pm | Total DramaRama | The Date
 8:00 pm | Amazing World of Gumball | The Ghouls
 8:15 pm | Amazing World of Gumball | The Scam
 8:30 pm | Amazing World of Gumball | The Comic
 8:45 pm | Amazing World of Gumball | The Vase