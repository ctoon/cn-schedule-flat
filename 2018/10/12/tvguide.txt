# 2018-10-12
 6:00 am | Amazing World of Gumball | The Detective; The Fuss
 6:30 am | Amazing World of Gumball | The Fury; The Outside
 7:00 am | Amazing World of Gumball | The Flower; The Banana
 7:30 am | Amazing World of Gumball | The Phone; The Job
 8:00 am | Teen Titans Go! | Career Day; Hand Zombie
 8:30 am | Teen Titans Go! | TV Knight 2; Employee of the Month Redux
 9:00 am | Teen Titans Go! | The Hive Five; More of the Same
 9:30 am | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
10:00 am | Amazing World of Gumball | The Heist; The Advice
10:30 am | Amazing World of Gumball | The Signal; The Singing
11:00 am | Amazing World of Gumball | The Box; The Disaster
11:30 am | Bunnicula | Pranks for the Memories; Down the Rabbit Hole
12:00 pm | Teen Titans Go! | History Lesson; Fish Water
12:30 pm | Teen Titans Go! | The Art Of Ninjutsu; Tv Knight
 1:00 pm | Amazing World of Gumball | The Kids; The Fan
 1:30 pm | Amazing World of Gumball | The Coach; The Joy
 2:00 pm | Teen Titans Go! | Waffles; Opposites
 2:30 pm | Teen Titans Go! | Birds; Be Mine
 3:00 pm | Ben 10 | Dreamtime
 3:15 pm | Teen Titans Go! | Finally a Lesson
 3:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 4:00 pm | Amazing World of Gumball | The Internet; The Plan
 4:30 pm | Amazing World of Gumball | The World; The Finale
 5:00 pm | Craig of the Creek | Too Many Treasures; Big Pinchy
 5:30 pm | Total DramaRama | Germ Factory; Duck Duck Juice
 6:00 pm | Teen Titans Go! | My Name Is Jose; The Power Of Shrimps
 6:30 pm | OK K.O.! Let's Be Heroes | Crossover Nexus
 6:45 pm | Teen Titans Go! | Costume Contest
 7:00 pm | We Bare Bears | Bear Lift; Tubin'
 7:30 pm | Total DramaRama | The Date; Cuttin' Corners
 8:00 pm | Amazing World of Gumball | The Sucker; The Boredom
 8:30 pm | Amazing World of Gumball | The Vegging; The Guy