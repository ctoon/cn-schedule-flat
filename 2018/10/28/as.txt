# 2018-10-28
 8:00 pm | Amazing World of Gumball | Candidate/The Code
 8:30 pm | Amazing World of Gumball | Faith/The Scam
 9:00 pm | Family Guy | Fat Guy Strangler
 9:30 pm | Bob's Burgers | Teen-a Witch
10:00 pm | American Dad | Poltergasm
10:30 pm | Family Guy | Halloween on Spooner Street
11:00 pm | Family Guy | Peternormal Activity
11:30 pm | Rick and Morty | Something Ricked This Way Comes
12:00 am | DREAM CORP LLC | Accordian Jim
12:15 am | DREAM CORP LLC | Amnesia
12:30 am | Venture Brothers  | Very Venture Halloween
 1:00 am | Squidbillies | The Tiniest Princess
 1:15 am | Your Pretty Face is Going to Hell | Torture
 1:30 am | American Dad | Poltergasm
 2:00 am | Family Guy | Halloween on Spooner Street
 2:30 am | Family Guy | Peternormal Activity
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | DREAM CORP LLC | Accordian Jim
 3:45 am | DREAM CORP LLC | Amnesia
 4:00 am | Venture Brothers  | Very Venture Halloween
 4:30 am | Squidbillies | The Tiniest Princess
 4:45 am | Your Pretty Face is Going to Hell | Torture
 5:00 am | Bob's Burgers | Teen-a Witch
 5:30 am | Home Movies | Coffins and Cradles