# 2018-10-20
 6:00 am | Teen Titans Go! | Monster Squad!; Ghostboy
 6:30 am | Transformers Cyberverse | Terminal Velocity
 6:45 am | Teen Titans Go! | Costume Contest
 7:00 am | Teen Titans Go! | Slumber Party; Halloween Vs. Christmas
 7:30 am | Teen Titans Go! | Oregon Trail; Campfire Stories
 8:00 am | Amazing World of Gumball | The Ghouls; The Mirror
 8:30 am | Amazing World of Gumball | The Scam; The Poltergeist
 9:00 am | Total DramaRama | That's A Wrap; The Bad Guy Busters
 9:30 am | Total DramaRama | Cone In 60 Seconds; Ant We All Just Get Along
10:00 am | Teen Titans Go! | Scary Figure Dance; The Cruel Giggling Ghoul
10:30 am | Teen Titans Go! | Halloween; Monster Squad!
11:00 am | Teen Titans Go! | Ghostboy; Costume Contest
11:30 am | Teen Titans Go! | Slumber Party; Halloween Vs. Christmas
12:00 pm | Ben 10 | Dreamtime
12:15 pm | Teen Titans Go! | Oregon Trail
12:30 pm | Teen Titans Go! | Campfire Stories; Scary Figure Dance
 1:00 pm | Teen Titans Go! | Halloween; The Cruel Giggling Ghoul
 1:30 pm | Teen Titans Go! | Monster Squad!; Ghostboy
 2:00 pm | Lego Batman: The Movie - DC Super Heroes Unite | 
 3:30 pm | Amazing World of Gumball | Halloween; The Ghost
 4:00 pm | Amazing World of Gumball | The Sorcerer; The Potion
 4:30 pm | Amazing World of Gumball | The Vacation; The Ghouls
 5:00 pm | Amazing World of Gumball | The Scam; The Mirror
 5:30 pm | Amazing World of Gumball | Halloween; The Poltergeist
 6:00 pm | Adventure Time Stakes | 
 8:00 pm | Regular Show | Terror Tales of the Park
 8:30 pm | Regular Show | Terror Tales of the Park II