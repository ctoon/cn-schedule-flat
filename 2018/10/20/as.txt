# 2018-10-20
 8:00 pm | SPECIAL | Regular Show: Terror Tales of the Park
 8:30 pm | SPECIAL | Regular Show Presents: Terror Tales of the Park II
 9:00 pm | Dragon Ball Z Kai | Run in the Afterlife, Goku! The One Million Mile Snake Way!
 9:30 pm | My Hero Academia | Shoto Todoroki: Origin
10:00 pm | Naruto:Shippuden | The Young Man and the Sea
10:30 pm | Boruto: Naruto Next Generations | A Ninjutsu Battle of the Sexes!
11:00 pm | Dragon Ball Super | Goku the Talent Scout - Recruit Krillin and Android 18
11:30 pm | FLCL: Alternative | Flying Memory
12:00 am | Attack on Titan | Ruler of the Walls
12:30 am | JoJo's Bizarre Adventure: Diamond Is Unbreakable | Yukako Yamagishi Falls in Love Part 2
 1:00 am | Black Clover | Temple Battle Royale
 1:30 am | Hunter x Hunter | Revenge x and x Recovery
 2:00 am | One-Punch Man | Unparalleled Peril
 2:30 am | Lupin The 3rd Part 4 | Dragons Sleep Soundly
 3:00 am | Samurai Jack | IV
 3:30 am | Samurai Jack | V
 4:00 am | Boondocks  | The Return of the King
 4:30 am | Black Dynamite | "How Honeybee Got Her Groove Back" or "Night of the Living D#@! Heads"
 5:00 am | Venture Brothers  | The Venture Bros. & The Curse of the Haunted Problem
 5:30 am | Home Movies | Writer's Block