# 2018-10-21
 6:00 am | Ben 10 | Scared Silly/Screamcatcher
 6:30 am | Teen Titans Go! | Halloween vs. Christmas/Oregon Trail
 7:00 am | Teen Titans Go! | Campfire Stories/Scary Figure Dance
 7:30 am | Teen Titans Go! | Costume Contest/Slumber Party
 8:00 am | Total DramaRama | That's a Wrap/Sharing Is Caring
 8:30 am | Total DramaRama | Cuttin' Corners/Aquarium for a Dream
 9:00 am | Teen Titans Go! | Cruel Giggling Ghoul/Halloween
 9:30 am | Teen Titans Go! | Monster Squad!/Ghostboy/Hand Zombie
10:00 am | Teen Titans Go! | Night Begins to Shine Special
11:00 am | Teen Titans Go! | Halloween vs. Christmas/Oregon Trail
11:30 am | Teen Titans Go! | Campfire Stories/Scary Figure Dance
12:00 pm | Teen Titans Go! | Costume Contest/Slumber Party
12:30 pm | Teen Titans Go! | Cruel Giggling Ghoul/Halloween
 1:00 pm | Total DramaRama | That's a Wrap/Free Chili
 1:30 pm | Total DramaRama | Duck Duck Juice/Cluckwork Orange
 2:00 pm | Amazing World of Gumball | Vacation/The Ghouls
 2:30 pm | Amazing World of Gumball | Mirror/The Scam
 3:00 pm | Amazing World of Gumball | Poltergeist/The Wicked
 3:30 pm | Amazing World of Gumball | Puppets/Halloween
 4:00 pm | The Powerpuff Girls | Witch's Crew
 4:15 pm | OK K.O.! Let's Be Heroes | Monster Party
 4:30 pm | MOVIE | Scooby-Doo! The Mystery Begins
 6:15 pm | Amazing World of Gumball | The Ghost
 6:30 pm | Amazing World of Gumball | Sorcerer/The Potion
 7:00 pm | Amazing World of Gumball | Ghouls/The Vacation
 7:30 pm | Amazing World of Gumball | Scam/The Mirror