# 2018-03-03
 6:00 am | Amazing World of Gumball | The Triangle; The Money
 6:30 am | Amazing World of Gumball | The Cringe
 6:45 am | Amazing World of Gumball | The Cage
 7:00 am | Amazing World of Gumball | The Candidate
 7:15 am | Amazing World of Gumball | The Faith
 7:30 am | Apple & Onion | Tips
 7:45 am | Apple & Onion | Falafel's Fun Day
 8:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 8:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 9:00 am | Teen Titans Go! | Double Trouble; The Date
 9:30 am | Teen Titans Go! | Dude Relax; Laundry Day
10:00 am | Teen Titans Go! | Ghost Boy
10:15 am | Teen Titans Go! | 
11:00 am | Apple & Onion | Tips
11:15 am | Apple & Onion | Falafel's Fun Day
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:00 pm | Ben 10 | Mayhem in Mascot
12:15 pm | Ben 10 | Bon Voyage
12:30 pm | OK K.O.! Let's Be Heroes | RMS & Brandon's First Episode
12:45 pm | OK K.O.! Let's Be Heroes | Lad & Logic
 1:00 pm | Teen Titans Go! | Parasite; Starliar
 1:30 pm | Teen Titans Go! | Meatball Party; Staff Meeting
 2:00 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 2:30 pm | Unikitty | The Zone
 2:45 pm | Unikitty | Lab Cat
 3:00 pm | Amazing World of Gumball | The Candidate
 3:15 pm | Amazing World of Gumball | The Faith
 3:30 pm | Amazing World of Gumball | The Kids; The Fan
 4:00 pm | Amazing World of Gumball | The Coach; The Joy
 4:30 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 5:00 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 5:30 pm | Teen Titans Go! | Burger vs. Burrito; Matched
 6:00 pm | Amazing World of Gumball | The One
 6:15 pm | Amazing World of Gumball | The Father
 6:30 pm | Unikitty | Fire & Nice
 6:45 pm | Unikitty | A Rock Friend Indeed
 7:00 pm | Amazing World of Gumball | The Sucker
 7:15 pm | Amazing World of Gumball | The Vegging
 7:30 pm | Amazing World of Gumball | The Rival
 7:45 pm | Ben 10 | Mayhem in Mascot