# 2018-03-11
 6:00 am | Unikitty | Pet Pet
 6:15 am | Amazing World of Gumball | The Detective
 6:30 am | Amazing World of Gumball | Compilation/The Fury
 7:00 am | Unikitty | Little Prince Puppycorn
 7:15 am | Teen Titans Go! | Riding the Dragon
 7:30 am | Teen Titans Go! | Overbite/Shrimps and Prime Rib
 8:00 am | Unikitty | Stuck Together
 8:15 am | Teen Titans Go! | Dignity of Teeth
 8:30 am | Teen Titans Go! | Spice Game/Croissant
 9:00 am | Unikitty | Too Many Unikittys
 9:15 am | Amazing World of Gumball | The Flower
 9:30 am | Amazing World of Gumball | The Phone/The Job
10:00 am | Unikitty | Film Fest
10:15 am | Amazing World of Gumball | The Return
10:30 am | Amazing World of Gumball | Nemesis/The Crew
11:00 am | Unikitty | Unikitty News!
11:15 am | Teen Titans Go! | Starfire the Terrible
11:30 am | Teen Titans Go! | In and Out/Brain Food
12:00 pm | Unikitty | Hide N' Seek
12:15 pm | Teen Titans Go! | Finally a Lesson
12:30 pm | Teen Titans Go! | Arms Race with Legs/Obinray
 1:00 pm | Unikitty | Wishing Well
 1:15 pm | Amazing World of Gumball | The Pony
 1:30 pm | Amazing World of Gumball | The Dream/The Sidekick
 2:00 pm | Unikitty | Crushing Defeat
 2:15 pm | Amazing World of Gumball | The Storm
 2:30 pm | Amazing World of Gumball | The Hero/The Photo
 3:00 pm | Unikitty | Kitchen Chaos
 3:15 pm | We Bare Bears | Rooms
 3:30 pm | We Bare Bears | Fashion Bears/Losing Ice
 4:00 pm | Unikitty | Film Fest
 4:15 pm | Amazing World of Gumball | The Apprentice
 4:30 pm | Amazing World of Gumball | Hug/The Wicked
 5:00 pm | Unikitty | Too Many Unikittys
 5:15 pm | SPECIAL | Teen Titans Go!: Opposite Titans
 6:00 pm | Ben 10 | Bomzobo Lives/Animorphosis
 6:30 pm | OK K.O.! Let's Be Heroes | Let's Not Be Skeletons/Action News
 7:00 pm | Unikitty | Unikitty News!
 7:15 pm | Amazing World of Gumball | The Heist
 7:30 pm | Amazing World of Gumball | The Singing
 7:45 pm | Ben 10 | Out to Launch