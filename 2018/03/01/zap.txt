# 2018-03-01
 6:00 am | Amazing World of Gumball | The Friend; The Saint
 6:30 am | Amazing World of Gumball | The Society; The Spoiler
 7:00 am | Teen Titans Go! | Grube's Fairytales
 7:15 am | Teen Titans Go! | A Farce
 7:30 am | Teen Titans Go! | Animals: It's Just a Word
 7:45 am | Teen Titans Go! | BBB Day!
 8:00 am | Teen Titans Go! | Road Trip; The Best Robin
 8:30 am | Amazing World of Gumball | The Grades
 8:45 am | Amazing World of Gumball | The Diet
 9:00 am | Amazing World of Gumball | The Ex
 9:15 am | Amazing World of Gumball | The Sorcerer
 9:30 am | Unikitty | Sparkle Matter Matters
 9:45 am | Unikitty | Action Forest
10:00 am | Unikitty | Kaiju Kitty
10:15 am | Unikitty | A Rock Friend Indeed
10:30 am | Teen Titans Go! | Mouth Hole; Hot Garbage
11:00 am | Teen Titans Go! | Girls' Night Out; You're Fired
11:30 am | Teen Titans Go! | Super Robin; Tower Power
12:00 pm | Unikitty | Lab Cat
12:15 pm | Unikitty | Kitty Court
12:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 1:00 pm | Teen Titans Go! | The Return of Slade; More of the Same
 1:30 pm | Amazing World of Gumball | The Faith
 1:45 pm | Amazing World of Gumball | The Ollie
 2:00 pm | Amazing World of Gumball | The Cycle
 2:15 pm | Amazing World of Gumball | The Stars
 2:30 pm | Amazing World of Gumball | The Menu
 2:45 pm | Amazing World of Gumball | The Uncle
 3:00 pm | We Bare Bears | Dog Hotel
 3:15 pm | We Bare Bears | Bear Lift
 3:30 pm | We Bare Bears | The Nom Nom Show
 3:45 pm | We Bare Bears | Ice Cave
 4:00 pm | Teen Titans Go! | Secret Garden
 4:15 pm | Teen Titans Go! | The Cruel Giggling Ghoul
 4:30 pm | Apple & Onion | A New Life
 4:45 pm | Apple & Onion | The Perfect Team
 5:00 pm | Teen Titans Go! | TV Knight
 5:15 pm | Teen Titans Go! | BBSFBDAY
 5:30 pm | Teen Titans Go! | Inner Beauty of a Cactus
 5:45 pm | Teen Titans Go! | Movie Night
 6:00 pm | Amazing World of Gumball | The Lady
 6:15 pm | Amazing World of Gumball | The Sucker
 6:30 pm | Unikitty | Crushing Defeat
 6:45 pm | Unikitty | Wishing Well
 7:00 pm | Amazing World of Gumball | The Vegging
 7:15 pm | Amazing World of Gumball | The One
 7:30 pm | Amazing World of Gumball | The Father
 7:45 pm | Ben 10 | Battle at Biggie Box