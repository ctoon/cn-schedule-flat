# 2018-03-05
 6:00 am | Amazing World of Gumball | Signature/The Check
 6:30 am | Amazing World of Gumball | Pest/The Sale
 7:00 am | Teen Titans Go! | Secret Garden/The Cruel Giggling Ghoul
 7:30 am | Teen Titans Go! | Pyramid Scheme/Beast Boy's St. Patrick's Day Luck, and It's Bad
 8:00 am | Teen Titans Go! | Yearbook Madness/Hose Water
 8:30 am | Amazing World of Gumball | Best/The Worst
 9:00 am | Amazing World of Gumball | Petals/The Deal
 9:30 am | Unikitty | Rock Friend/Wishing Well
10:00 am | Unikitty | Little Prince Puppycorn/Stuck Together
10:30 am | Teen Titans Go! | Rocks and Water/Multiple Trick Pony
11:00 am | Teen Titans Go! | Terra-ized/Artful Dodgers
11:30 am | Amazing World of Gumball | News/The List
12:00 pm | Amazing World of Gumball | The Authority/The Virus
12:30 pm | Unikitty | Sparkle Matter Matters/Action Forest
 1:00 pm | Teen Titans Go! | Dignity of Teeth/Croissant
 1:30 pm | Teen Titans Go! | Spice Game
 1:45 pm | Ben 10 | Mayhem in Mascot
 2:00 pm | Amazing World of Gumball | Line/The Puppets
 2:30 pm | Amazing World of Gumball | Rival/The Lady
 3:00 pm | We Bare Bears | Our Stuff/Viral Video
 3:30 pm | We Bare Bears | Food Truck/Everyday Bears
 4:00 pm | Teen Titans Go! | Rad Dudes with Bad Tudes/Wally T
 4:30 pm | Teen Titans Go! | Operation Dude Rescue Part 2/Operation Dude Rescue Part 1
 5:00 pm | SPECIAL | Teen Titans Go: Tooth Fairy Fun
 6:00 pm | Ben 10 | Creature Feature/Screamcatcher
 6:30 pm | OK K.O.! Let's Be Heroes | OK Dendy/Plaza Shorts
 7:00 pm | Apple & Onion | Falafel's Fun Day/Tips
 7:30 pm | Amazing World of Gumball | The Candidate
 7:45 pm | Ben 10 | Screamcatcher