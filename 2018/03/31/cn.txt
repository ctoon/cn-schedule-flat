# 2018-03-31
 6:00 am | Craig of the Creek | Too Many Treasures
 6:15 am | Amazing World of Gumball | The Mustache
 6:30 am | Amazing World of Gumball | The Club/The Wand
 7:00 am | Craig of the Creek | Itch to Explore
 7:15 am | OK K.O.! Let's Be Heroes | Hope This Flies
 7:30 am | Teen Titans Go! | Operation Dude Rescue Part 2/Operation Dude Rescue Part 1
 8:00 am | Craig of the Creek | You're It
 8:15 am | Ben 10 | Mayhem in Mascot
 8:30 am | SPECIAL | Teen Titans Go!: Easter Special
 9:00 am | Craig of the Creek | Jessica Goes to the Creek
 9:15 am | Teen Titans Go! | Two Parter Part 1
 9:30 am | Teen Titans Go! | Two Parter Part 2/Squash & Stretch
10:00 am | Craig of the Creek | The Final Book
10:15 am | Teen Titans Go! | Garage Sale
10:30 am | Teen Titans Go! | Sandwich Thief/Money Grandma
11:00 am | Craig of the Creek | Too Many Treasures
11:15 am | Teen Titans Go! | Friendship
11:30 am | Teen Titans Go! | Slumber Party/The Mask
12:00 pm | Craig of the Creek | Itch to Explore
12:15 pm | Amazing World of Gumball | The Cage
12:30 pm | Amazing World of Gumball | The Advice/The Traitor
 1:00 pm | Craig of the Creek | You're It
 1:15 pm | Apple & Onion | 4 on 1
 1:30 pm | Apple & Onion | Bottle Catch/Pancake's Bus Tour
 2:00 pm | Craig of the Creek | Jessica Goes to the Creek
 2:15 pm | Amazing World of Gumball | The Signal
 2:30 pm | Amazing World of Gumball | The Parasite/The Love
 3:00 pm | Craig of the Creek | The Final Book
 3:15 pm | Unikitty | Unikitty News!
 3:30 pm | Unikitty | Too Many Unikittys/Film Fest
 4:00 pm | Craig of the Creek | Too Many Treasures
 4:15 pm | Teen Titans Go! | No Power
 4:30 pm | Teen Titans Go! | Nose Mouth/Caged Tiger
 5:00 pm | Craig of the Creek | Itch to Explore/You're It
 5:30 pm | Apple & Onion | Block Party/The Perfect Team
 6:00 pm | Craig of the Creek | Too Many Treasures
 6:15 pm | Amazing World of Gumball | The Loophole
 6:30 pm | Amazing World of Gumball | The Copycats/The Potato
 7:00 pm | Craig of the Creek | Jessica Goes to the Creek/The Final Book
 7:30 pm | Amazing World of Gumball | The Outside/The Vase