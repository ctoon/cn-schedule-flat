# 2018-03-18
 6:00 am | Amazing World of Gumball | The Matchmaker
 6:15 am | Amazing World of Gumball | The Box
 6:30 am | Amazing World of Gumball | The Console
 6:45 am | Amazing World of Gumball | The Ollie
 7:00 am | Unikitty | 
 8:00 am | Ben 10 | Bomzobo Lives
 8:15 am | Ben 10 | Animorphosis
 8:30 am | OK K.O.! Let's Be Heroes | Let's Not Be Skeletons
 8:45 am | OK K.O.! Let's Be Heroes | Action News
 9:00 am | Teen Titans Go! | 
10:00 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
10:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
11:00 am | Teen Titans Go! | Yearbook Madness; Hose Water
11:30 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
12:00 pm | Teen Titans Go! | Oh Yeah!
12:15 pm | Teen Titans Go! | Riding the Dragon
12:30 pm | Teen Titans Go! | Master Detective
12:45 pm | Teen Titans Go! | The Avogodo
 1:00 pm | Apple & Onion | Hotdog's Movie Premiere
 1:15 pm | Apple & Onion | Bottle Catch
 1:30 pm | Amazing World of Gumball | The Slide
 1:45 pm | Amazing World of Gumball | The Loophole
 2:00 pm | Amazing World of Gumball | The Copycats
 2:15 pm | Amazing World of Gumball | The Potato
 2:30 pm | Amazing World of Gumball | The Fuss
 2:45 pm | Amazing World of Gumball | The Outside
 3:00 pm | We Bare Bears | Road Trip
 3:15 pm | We Bare Bears | Crowbar Jones
 3:30 pm | We Bare Bears | Bunnies
 3:45 pm | We Bare Bears | Panda 2
 4:00 pm | Teen Titans Go! | Booty Scooty
 4:15 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 4:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 4:45 pm | Teen Titans Go! | Orangins
 4:55 pm | Teen Titans Go! | Island Adventures
 6:00 pm | Teen Titans Go! | The Night Begins to Shine Special
 7:00 pm | Adventure Time | Blenanas
 7:15 pm | Adventure Time | Jake the Starchild
 7:30 pm | Adventure Time | Temple of Mars
 7:45 pm | Adventure Time | Gumbaldia