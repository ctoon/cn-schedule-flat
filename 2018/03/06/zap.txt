# 2018-03-06
 6:00 am | Amazing World of Gumball | The Gift
 6:15 am | Amazing World of Gumball | The Parking
 6:30 am | Amazing World of Gumball | The Routine
 6:45 am | Ben 10 | Creature Feature
 7:00 am | Teen Titans Go! | Bottle Episode
 7:15 am | Teen Titans Go! | Finally a Lesson
 7:30 am | Teen Titans Go! | Arms Race With Legs
 7:45 am | Teen Titans Go! | Obinray
 8:00 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 8:30 am | Amazing World of Gumball | The Puppets
 8:45 am | Amazing World of Gumball | The Line
 9:00 am | Amazing World of Gumball | The Candidate
 9:15 am | Amazing World of Gumball | The Faith
 9:30 am | Unikitty | The Zone
 9:45 am | Unikitty | Lab Cat
10:00 am | Unikitty | Pet Pet
10:15 am | Unikitty | Kitty Court
10:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
11:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
11:30 am | Amazing World of Gumball | The One
11:45 am | Amazing World of Gumball | The Father
12:00 pm | Amazing World of Gumball | Halloween; The Treasure
12:30 pm | Unikitty | Kaiju Kitty
12:45 pm | Unikitty | Fire & Nice
 1:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 1:15 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 1:30 pm | Teen Titans Go! | The Fourth Wall
 1:45 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 2:00 pm | Amazing World of Gumball | The Sucker
 2:15 pm | Amazing World of Gumball | The Vegging
 2:30 pm | Amazing World of Gumball | The Cringe
 2:45 pm | Amazing World of Gumball | The Cage
 3:00 pm | We Bare Bears | Burrito
 3:15 pm | We Bare Bears | Primal
 3:30 pm | We Bare Bears | Jean Jacket
 3:45 pm | We Bare Bears | Nom Nom
 4:00 pm | Teen Titans Go! | History Lesson
 4:15 pm | Teen Titans Go! | The Art of Ninjutsu
 4:30 pm | Ben 10 | Screamcatcher
 4:45 pm | OK K.O.! Let's Be Heroes | Ok Dendy
 5:00 pm | Teen Titans Go! | BBRAE
 5:30 pm | Teen Titans Go! | Permanent Record
 5:45 pm | Teen Titans Go! | Snuggle Time
 6:00 pm | Amazing World of Gumball | The Fury
 6:15 pm | Amazing World of Gumball | The Compilation
 6:30 pm | Unikitty | Hide n' Seek
 6:45 pm | Unikitty | Stuck Together
 7:00 pm | Apple & Onion | The Perfect Team
 7:15 pm | Amazing World of Gumball | The Disaster
 7:30 pm | Amazing World of Gumball | The Re-Run
 7:45 pm | Ben 10 | Xingo