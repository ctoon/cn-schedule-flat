# 2018-03-20
 6:00 am | Amazing World of Gumball | The Diet
 6:15 am | Amazing World of Gumball | The Ex
 6:30 am | Amazing World of Gumball | The Sorcerer
 6:45 am | Ben 10 | Assault on Pancake Palace
 7:00 am | Teen Titans Go! | Brain Percentages
 7:15 am | Teen Titans Go! | BL4Z3
 7:30 am | Teen Titans Go! | Hot Salad Water
 7:45 am | Teen Titans Go! | Lication
 8:00 am | Teen Titans Go! | Bottle Episode
 8:15 am | Teen Titans Go! | Finally a Lesson
 8:30 am | Amazing World of Gumball | The Limit; The Game
 9:00 am | Amazing World of Gumball | The Anybody
 9:15 am | Amazing World of Gumball | The Candidate
 9:30 am | Unikitty | Too Many Unikittys
 9:45 am | Unikitty | Film Fest
10:00 am | Teen Titans Go! | Brian; Nature
10:30 am | Teen Titans Go! | Arms Race With Legs
10:45 am | Teen Titans Go! | Obinray
11:00 am | Amazing World of Gumball | The Girlfriend
11:15 am | Amazing World of Gumball | The Advice
11:30 am | Amazing World of Gumball | The Signal
11:45 am | Amazing World of Gumball | The Parasite
12:00 pm | Apple & Onion | Falafel's Fun Day
12:15 pm | Apple & Onion | Apple's in Trouble
12:30 pm | Ben 10 | Creature Feature
12:45 pm | Teen Titans Go! | Fish Water
 1:00 pm | Teen Titans Go! | Movie Night
 1:15 pm | Teen Titans Go! | Permanent Record
 1:30 pm | Amazing World of Gumball | The Mothers; The Password
 2:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 2:30 pm | Amazing World of Gumball | The Mirror; The Burden
 3:00 pm | We Bare Bears | Food Truck
 3:15 pm | We Bare Bears | Everyday Bears
 3:30 pm | We Bare Bears | Burrito
 3:45 pm | We Bare Bears | Emergency
 4:00 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 4:30 pm | Ben 10 | Assault on Pancake Palace
 4:45 pm | Teen Titans Go! | Driver's Ed
 5:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 5:30 pm | Teen Titans Go! | No Power; Sidekick
 6:00 pm | Amazing World of Gumball | The Rival
 6:15 pm | Amazing World of Gumball | The Lady
 6:30 pm | Unikitty | Birthday Blowout
 6:45 pm | Unikitty | The Zone
 7:00 pm | Amazing World of Gumball | The Sucker
 7:15 pm | Amazing World of Gumball | The Vegging
 7:30 pm | Amazing World of Gumball | The One
 7:45 pm | Ben 10 | Mayhem in Mascot