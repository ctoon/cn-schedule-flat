# 2018-03-26
 6:00 am | Amazing World of Gumball | The Third; The Debt
 6:30 am | Amazing World of Gumball | The Pressure; The Painting
 7:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
 7:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
 8:00 am | Teen Titans Go! | Snuggle Time
 8:15 am | Teen Titans Go! | Oh Yeah!
 8:30 am | Teen Titans Go! | Riding the Dragon
 8:45 am | Teen Titans Go! | The Overbite
 9:00 am | Amazing World of Gumball | The Recipe; The Puppy
 9:30 am | Amazing World of Gumball | The Name; The Extras
10:00 am | Unikitty | Unikitty News!
10:15 am | Unikitty | Lab Cat
10:30 am | Teen Titans Go! | Inner Beauty of a Cactus
10:45 am | Teen Titans Go! | Movie Night
11:00 am | Teen Titans Go! | Permanent Record
11:15 am | Teen Titans Go! | Titan Saving Time
11:30 am | Teen Titans Go! | Master Detective
11:45 am | Teen Titans Go! | Hand Zombie
12:00 pm | Apple & Onion | Apple's in Trouble
12:15 pm | Apple & Onion | 4 on 1
12:30 pm | Amazing World of Gumball | The Countdown; The Nobody
 1:00 pm | Amazing World of Gumball | The Downer; The Egg
 1:30 pm | Amazing World of Gumball | The Triangle
 1:45 pm | Ben 10 | Animorphosis
 2:00 pm | Teen Titans Go! | Cat's Fancy
 2:15 pm | Teen Titans Go! | Leg Day
 2:30 pm | Teen Titans Go! | The Dignity of Teeth
 2:45 pm | Teen Titans Go! | The Croissant
 3:00 pm | Teen Titans Go! | 
 4:00 pm | Alvin and the Chipmunks: The Road Chip | 
 6:00 pm | Ben 10 | High Stress Express
 6:15 pm | Ben 10 | Omni-Tricked, Part 1
 7:15 pm | Teen Titans Go! | 