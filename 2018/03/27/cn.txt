# 2018-03-27
 6:00 am | Amazing World of Gumball | The Mystery/The Prank
 6:30 am | Amazing World of Gumball | The GI/The Kiss
 7:00 am | Teen Titans Go! | Ghostboy/La Larva de Amor
 7:30 am | Teen Titans Go! | Hey Pizza!/Gorilla
 8:00 am | Teen Titans Go! | BBSFBDAY!/TV Knight
 8:30 am | Teen Titans Go! | Inner Beauty of a Cactus/Movie Night
 9:00 am | Amazing World of Gumball | The Boss/The Move
 9:30 am | Amazing World of Gumball | The Law/The Allergy
10:00 am | Unikitty | Kitty Court/Too Many Unikittys
10:30 am | Teen Titans Go! | Ones and Zeros/Lication
11:00 am | Teen Titans Go! | TV Knight 2/Career Day
11:30 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:00 pm | Apple & Onion | Block Party/A New Life
12:30 pm | Amazing World of Gumball | The Sale/The Pest
 1:00 pm | Amazing World of Gumball | The Parking
 1:15 pm | Ben 10 | Screamcatcher
 1:30 pm | Amazing World of Gumball | The Upgrade/The Routine
 2:00 pm | Teen Titans Go! | Fourth Wall/40%, 40%, 20%
 2:30 pm | Teen Titans Go! | Grube's Fairytales/A Farce
 3:00 pm | Teen Titans Go! | Meatball Party/Staff Meeting
 3:30 pm | SPECIAL | Teen Titans Go!: Island Adventures
 4:30 pm | MOVIE | Alvin and the Chipmunks: The Road Chip
 6:30 pm | SPECIAL | Teen Titans Go!: Road Trip
 7:30 pm | Teen Titans Go! | BBRAE