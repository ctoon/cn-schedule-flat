# 2018-03-09
 6:00 am | Amazing World of Gumball | Nest/The Awkwardness
 6:30 am | Amazing World of Gumball | Anybody/The Candidate
 7:00 am | Amazing World of Gumball | Faith/The Cage
 7:30 am | Apple & Onion | Apple's in Trouble/4 on 1
 8:00 am | Teen Titans Go! | Brain Food/In and Out
 8:30 am | Teen Titans Go! | Little Buddies/Missing
 9:00 am | Teen Titans Go! | Animals, It's Just a Word!/BBBDay!
 9:30 am | Teen Titans Go! | Two Parter Part 2/Two Parter Part 1
10:00 am | Teen Titans Go! | Squash & Stretch/Garage Sale
10:30 am | Teen Titans Go! | Secret Garden/Uncle Jokes/The Cruel Giggling Ghoul
11:00 am | SPECIAL | Teen Titans Go: Beast Boy's Best Beasts
12:00 pm | Ben 10 | Creature Feature/Screamcatcher
12:30 pm | OK K.O.! Let's Be Heroes | OK Dendy/Plaza Shorts
 1:00 pm | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
 1:30 pm | Teen Titans Go! | Mr. Butt/Man Person
 2:00 pm | Apple & Onion | Apple's in Trouble/4 on 1
 2:30 pm | Amazing World of Gumball | Anybody/The Candidate
 3:00 pm | Amazing World of Gumball | Signature/The Check
 3:30 pm | Amazing World of Gumball | Others/The Crew
 4:00 pm | Teen Titans Go! | Night Begins to Shine Special
 5:00 pm | Teen Titans Go! | Pyramid Scheme/Bottle Episode
 5:30 pm | Teen Titans Go! | Arms Race with Legs/Finally a Lesson
 6:00 pm | Amazing World of Gumball | Lady/The Cringe
 6:30 pm | Amazing World of Gumball | News/The List
 7:00 pm | Amazing World of Gumball | Deal/The Petals
 7:30 pm | Amazing World of Gumball | The Line
 7:45 pm | Ben 10 | Mayhem in Mascot