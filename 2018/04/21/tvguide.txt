# 2018-04-21
 6:00 am | Amazing World of Gumball | The Coach; The Joy
 6:30 am | Amazing World of Gumball | The Recipe; The Puppy
 7:00 am | Teen Titans Go! | Orangins; Jinxed
 7:30 am | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 8:00 am | Teen Titans Go! | Birds; Be Mine
 8:30 am | Teen Titans Go! | Brain Food; In and Out
 9:00 am | Teen Titans Go!: Time Traveling Titans | 
10:00 am | Teen Titans Go! | The Art of Ninjutsu; Think About Your Future
10:30 am | Teen Titans Go! | Booty Scooty; Who's Laughing Now; Career Day
11:00 am | Teen Titans Go!: Birthday Celebration | 
12:00 pm | Ben 10 | Half-Sies; High Stress Express
12:30 pm | Craig of the Creek | Dog Decider; The Curse
 1:00 pm | Amazing World of Gumball | The Shippening
 1:15 pm | Unikitty | License to Punch
 1:30 pm | Amazing World of Gumball | The Grades; The Diet
 2:00 pm | Apple & Onion | The Perfect Team; Tips
 2:30 pm | Apple & Onion | 4 On 1; Block Party
 3:00 pm | Amazing World of Gumball | The List; The Line
 3:30 pm | Amazing World of Gumball | The Petals; The Deal
 4:00 pm | Unikitty | The Zone; Too Many Unikittys
 4:30 pm | Unikitty | Film Fest; Unikitty News!
 5:00 pm | Teen Titans Go!: Birthday Celebration | 
 6:00 pm | Amazing World of Gumball | The Ex; The Sorcerer
 6:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 7:00 pm | Craig of the Creek | Escape From Family Dinner; Monster in the Garden
 7:30 pm | Craig of the Creek | Dog Decider; The Curse