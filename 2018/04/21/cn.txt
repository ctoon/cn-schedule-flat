# 2018-04-21
 6:00 am | Amazing World of Gumball | The Name/The Extras
 6:30 am | Amazing World of Gumball | The Gripes/The Vacation
 7:00 am | Ben 10 | Animorphosis/Half-Sies
 7:30 am | Craig of the Creek | Dog Decider/The Curse
 8:00 am | Craig of the Creek | Sunday Clothes/Wildernessa
 8:30 am | Amazing World of Gumball | The Shippening
 8:45 am | Unikitty | License to Punch
 9:00 am | Teen Titans Go! | Avogodo/Brain Percentages
 9:30 am | Teen Titans Go! | Operation Tin Man/Nean
10:00 am | Teen Titans Go! | More of the Same/The Hive Five
10:30 am | Teen Titans Go! | Oregon Trail/Snuggle Time
11:00 am | SPECIAL | Teen Titans Go!: Time Traveling Titans
12:00 pm | Teen Titans Go! | Employee of the Month Redux/Hand Zombie
12:30 pm | Teen Titans Go! | Fish Water/TV Knight
 1:00 pm | Teen Titans | Apprentice Part 1/Apprentice Part 2/Lazy Sunday
 2:00 pm | Amazing World of Gumball | The Cringe/The Cage
 2:30 pm | Amazing World of Gumball | The Candidate/The Faith
 3:00 pm | SPECIAL | Teen Titans Go!: Birthday Celebration
 4:00 pm | The Powerpuff Girls | Toy Ploy/A Slight Hiccup
 4:30 pm | Amazing World of Gumball | The Shippening
 4:45 pm | Unikitty | License to Punch
 5:00 pm | Craig of the Creek | Dog Decider/The Final Book
 5:30 pm | Craig of the Creek | Too Many Treasures/The Curse
 6:00 pm | Amazing World of Gumball | The Menu/The Uncle
 6:30 pm | Amazing World of Gumball | The Origins/The The Origins Part 2
 7:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 pm | Mighty Magiswords | Taming of the Swords/School's In, Oh Bummer