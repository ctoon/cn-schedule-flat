# 2018-04-02
 6:00 am | Craig of the Creek | Too Many Treasures
 6:15 am | Amazing World of Gumball | The Car
 6:30 am | Amazing World of Gumball | The Microwave
 6:45 am | Ben 10 | Out to Launch
 7:00 am | Craig of the Creek | Itch to Explore
 7:15 am | Teen Titans Go! | Colors of Raven
 7:30 am | Teen Titans Go! | Lazy Sunday/Books
 8:00 am | Craig of the Creek | You're It
 8:15 am | Teen Titans Go! | TV Knight 2
 8:30 am | Teen Titans Go! | Throne of Bones/The Academy
 9:00 am | Craig of the Creek | Jessica Goes to the Creek
 9:15 am | Amazing World of Gumball | The Pizza
 9:30 am | Amazing World of Gumball | The Butterfly/The Question
10:00 am | Craig of the Creek | The Final Book
10:15 am | Unikitty | Crushing Defeat
10:30 am | Unikitty | Hide N' Seek/Wishing Well
11:00 am | Craig of the Creek | Too Many Treasures
11:15 am | Teen Titans Go! | Road Trip
11:30 am | Teen Titans Go! | Hot Garbage/Mouth Hole
12:00 pm | Craig of the Creek | Itch to Explore
12:15 pm | Amazing World of Gumball | The Sucker/The Vegging
12:45 pm | Amazing World of Gumball | The One
 1:00 pm | Craig of the Creek | You're It
 1:15 pm | Amazing World of Gumball | The Bus
 1:30 pm | Amazing World of Gumball | The Night
 1:45 pm | Ben 10 | High Stress Express
 2:00 pm | Craig of the Creek | Jessica Goes to the Creek
 2:15 pm | Teen Titans Go! | Oregon Trail
 2:30 pm | Teen Titans Go! | Oh Yeah!/Snuggle Time
 3:00 pm | Craig of the Creek | The Final Book
 3:15 pm | Apple & Onion | 4 on 1
 3:30 pm | Apple & Onion | Hotdog's Movie Premiere/Bottle Catch
 4:00 pm | Craig of the Creek | Too Many Treasures
 4:15 pm | Teen Titans Go! | Birds
 4:30 pm | Teen Titans Go! | Little Buddies/Missing
 5:00 pm | Craig of the Creek | Itch to Explore
 5:15 pm | Amazing World of Gumball | The Faith
 5:30 pm | Amazing World of Gumball | The Candidate/The Anybody
 6:00 pm | Craig of the Creek | Wildernessa
 6:15 pm | Teen Titans Go! | Beast Girl
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | Craig of the Creek | You're It
 7:15 pm | Amazing World of Gumball | The Catfish
 7:30 pm | Amazing World of Gumball | The Stars
 7:45 pm | Ben 10 | Assault on Pancake Palace