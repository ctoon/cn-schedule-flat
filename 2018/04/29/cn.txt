# 2018-04-29
 6:00 am | Teen Titans | Divide and Conquer
 6:30 am | Teen Titans | Forces of Nature
 7:00 am | Craig of the Creek | Wildernessa/Lost in the Sewer
 7:30 am | Craig of the Creek | Dog Decider/Too Many Treasures
 8:00 am | Teen Titans Go! | Flashback
 8:30 am | Teen Titans Go! | Operation Dude Rescue Part 1/Beast Girl
 9:00 am | Teen Titans Go! | Streak Part 1/Operation Dude Rescue Part 2
 9:30 am | Teen Titans Go! | The Streak Part 2
 9:45 am | Teen Titans Go! | BBRAE
10:15 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:45 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
11:15 am | Teen Titans Go! | BBBDay!/BBSFBDAY!
11:45 am | Teen Titans Go! | BBCYFSHIPBDAY/The Art of Ninjutsu
12:00 pm | OK K.O.! Let's Be Heroes | Perfect Meal/Hope This Flies
12:30 pm | OK K.O.! Let's Be Heroes | You're in Control
 1:00 pm | Amazing World of Gumball | The Shippening/The Cage
 1:30 pm | Amazing World of Gumball | The Cringe/The Neighbor
 2:00 pm | Craig of the Creek | Dog Decider/The Final Book
 2:30 pm | Craig of the Creek | Jessica Goes to the Creek/Lost in the Sewer
 3:00 pm | We Bare Bears | I Am Ice Bear/Bearz II Men
 3:30 pm | We Bare Bears | Bunnies/Baby Bears Can't Jump
 4:00 pm | The Powerpuff Girls | Derby Dollies/Bubbles the Blue
 4:30 pm | Unikitty | Action Forest/License to Punch
 5:00 pm | Amazing World of Gumball | The Father/The Pact
 5:30 pm | Amazing World of Gumball | The Shippening/The One
 6:00 pm | Craig of the Creek | You're It/Lost in the Sewer
 6:30 pm | Craig of the Creek | Bring Out Your Beast/Itch to Explore
 7:00 pm | Teen Titans Go! | Top of the Titans:  Raddest Songs/Beast Girl
 7:30 pm | Mighty Magiswords | Quest for Knowledge