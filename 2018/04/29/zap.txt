# 2018-04-29
 6:00 am | Teen Titans | Divide and Conquer
 6:30 am | Teen Titans | Forces of Nature
 7:00 am | Craig of the Creek | Lost in the Sewer
 7:15 am | Craig of the Creek | Wildernessa
 7:30 am | Craig of the Creek | Dog Decider
 7:45 am | Craig of the Creek | Too Many Treasures
 8:00 am | Teen Titans Go! | Flashback
 8:30 am | Teen Titans Go! | Beast Girl
 8:45 am | Teen Titans Go! | Operation Dude Rescue: Part 1
 9:00 am | Teen Titans Go! | Operation Dude Rescue: Part 2
 9:15 am | Teen Titans Go! | The Streak, Part 1
 9:30 am | Teen Titans Go! | The Streak, Part 2
 9:45 am | Teen Titans Go! | BBRAE
10:15 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
10:45 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
11:15 am | Teen Titans Go! | BBB Day!
11:30 am | Teen Titans Go! | BBSFBDAY
11:45 am | Teen Titans Go! | BBCYFSHIPBDAY
12:00 pm | OK K.O.! Let's Be Heroes | The Perfect Meal
12:15 pm | OK K.O.! Let's Be Heroes | Hope This Flies
12:30 pm | OK K.O.! Let's Be Heroes | You're in Control
 1:00 pm | Amazing World of Gumball | The Shippening
 1:15 pm | Amazing World of Gumball | The Cage
 1:30 pm | Amazing World of Gumball | The Neighbor
 1:45 pm | Amazing World of Gumball | The Cringe
 2:00 pm | Craig of the Creek | Dog Decider
 2:15 pm | Craig of the Creek | The Final Book
 2:30 pm | Craig of the Creek | Lost in the Sewer
 2:45 pm | Craig of the Creek | Jessica Goes to the Creek
 3:00 pm | We Bare Bears | I Am Ice Bear
 3:15 pm | We Bare Bears | Bearz II Men
 3:30 pm | We Bare Bears | Baby Bears Can't Jump
 3:45 pm | We Bare Bears | Bunnies
 4:00 pm | The Powerpuff Girls | Derby Dollies
 4:15 pm | The Powerpuff Girls | Bubbles The Blue
 4:30 pm | Unikitty | License to Punch
 4:45 pm | Unikitty | Action Forest
 5:00 pm | Amazing World of Gumball | The Pact
 5:15 pm | Amazing World of Gumball | The Father
 5:30 pm | Amazing World of Gumball | The Shippening
 5:45 pm | Amazing World of Gumball | The One
 6:00 pm | Craig of the Creek | Lost in the Sewer
 6:15 pm | Craig of the Creek | You're It
 6:30 pm | Craig of the Creek | Bring Out Your Beast
 6:45 pm | Craig of the Creek | Itch to Explore
 7:00 pm | Teen Titans Go! | Top of the Titans: Raddest Songs
 7:15 pm | Teen Titans Go! | Beast Girl
 7:30 pm | Mighty Magiswords | Quest for Knowledge