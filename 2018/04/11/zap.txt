# 2018-04-11
 6:00 am | Steven Universe | Laser Light Cannon
 6:15 am | Ben 10 | High Stress Express
 6:30 am | Amazing World of Gumball | The Authority; The Virus
 7:00 am | Apple & Onion | Apple's in Trouble
 7:15 am | Apple & Onion | A New Life
 7:30 am | Teen Titans Go! | Mr. Butt; Man Person
 8:00 am | Teen Titans Go! | Meatball Party; Staff Meeting
 8:30 am | Amazing World of Gumball | The Signature
 8:45 am | Amazing World of Gumball | The Check
 9:00 am | Amazing World of Gumball | The Pest
 9:15 am | Amazing World of Gumball | The Sale
 9:30 am | Amazing World of Gumball | The Words; The Apology
10:00 am | Unikitty | Kaiju Kitty
10:15 am | Unikitty | Fire & Nice
10:30 am | Teen Titans Go! | The Dignity of Teeth
10:45 am | Teen Titans Go! | The Croissant
11:00 am | Teen Titans Go! | Road Trip; The Best Robin
11:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes
11:45 am | Teen Titans Go! | The Art of Ninjutsu
12:00 pm | Unikitty | A Rock Friend Indeed
12:15 pm | Unikitty | Kitchen Chaos
12:30 pm | Teen Titans Go! | Dude Relax; Laundry Day
 1:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 1:30 pm | Teen Titans Go! | Hey Pizza!
 1:45 pm | Ben 10 | Mayhem in Mascot
 2:00 pm | Amazing World of Gumball | The Copycats
 2:15 pm | Amazing World of Gumball | The Potato
 2:30 pm | Amazing World of Gumball | The Fuss
 2:45 pm | Amazing World of Gumball | The Outside
 3:00 pm | Amazing World of Gumball | The Pact
 3:15 pm | Unikitty | Dinner Apart-y
 3:30 pm | Craig of the Creek | You're It
 3:45 pm | Teen Titans Go! | Squash & Stretch
 4:00 pm | Teen Titans Go! | Operation Tin Man; Nean
 4:30 pm | Teen Titans Go! | Campfire Stories; The Hive Five
 5:00 pm | Teen Titans Go! | Inner Beauty of a Cactus
 5:15 pm | Teen Titans Go! | Movie Night
 5:30 pm | Teen Titans Go! | BBRAE
 6:00 pm | Amazing World of Gumball | The Vegging
 6:15 pm | Amazing World of Gumball | The One
 6:30 pm | Unikitty | Too Many Unikittys
 6:45 pm | Unikitty | Unikitty News!
 7:00 pm | Amazing World of Gumball | The Pact
 7:15 pm | Unikitty | Dinner Apart-y
 7:30 pm | Craig of the Creek | Jessica Goes to the Creek
 7:45 pm | Ben 10 | Out to Launch