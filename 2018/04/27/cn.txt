# 2018-04-27
 6:00 am | Steven Universe | Giant Woman
 6:15 am | Teen Titans Go! | BBCYFSHIPBDAY
 6:30 am | Teen Titans | Mask
 7:00 am | Teen Titans | Mad Mod
 7:30 am | Amazing World of Gumball | The Cage/The Faith
 8:00 am | Amazing World of Gumball | The Anybody/The Candidate
 8:30 am | Amazing World of Gumball | The Vase/The Matchmaker
 9:00 am | Amazing World of Gumball | The Console/The Box
 9:30 am | Teen Titans Go! | Streak Part 2/The Streak Part 1
10:00 am | Teen Titans Go! | Squash & Stretch/BBBDay!
10:30 am | Craig of the Creek | Sunday Clothes/Too Many Treasures
11:00 am | Amazing World of Gumball | The Butterfly/The Question
11:30 am | Amazing World of Gumball | The Oracle/The Safety
12:00 pm | Teen Titans Go! | Legs/Breakfast Cheese
12:30 pm | Teen Titans Go! | Operation Dude Rescue Part 2/Operation Dude Rescue Part 1
 1:00 pm | Amazing World of Gumball | The Authority/The Virus
 1:30 pm | Amazing World of Gumball | The Pony/The Storm
 2:00 pm | We Bare Bears | Burrito/Jean Jacket
 2:30 pm | Craig of the Creek | Jessica Goes to the Creek/Wildernessa
 3:00 pm | Teen Titans Go! | Two Parter Part 1/Two Parter Part 2
 3:30 pm | Teen Titans Go! | Waffles
 3:45 pm | Ben 10 | Xingo's Back
 4:00 pm | Amazing World of Gumball | The Faith/The Neighbor
 4:30 pm | Amazing World of Gumball | The Shippening/The Pact
 5:00 pm | SPECIAL | Teen Titans Go!: Island Adventures
 6:00 pm | Teen Titans Go! | Top of the Titans:  Raddest Songs/Beast Girl
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Craig of the Creek | Lost in the Sewer/Bring Out Your Beast
 7:30 pm | Amazing World of Gumball | The One/The Vegging