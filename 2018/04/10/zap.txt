# 2018-04-10
 6:00 am | Steven Universe | Your Mother and Mine
 6:15 am | Ben 10 | Animorphosis
 6:30 am | Amazing World of Gumball | The Bumpkin; The Flakers
 7:00 am | Apple & Onion | Tips
 7:15 am | Apple & Onion | Hotdog's Movie Premiere
 7:30 am | Teen Titans Go! | Dreams; Grandma Voice
 8:00 am | Teen Titans Go! | Parasite; Starliar
 8:30 am | Amazing World of Gumball | The Return
 8:45 am | Amazing World of Gumball | The Nemesis
 9:00 am | Amazing World of Gumball | The Crew
 9:15 am | Amazing World of Gumball | The Others
 9:30 am | Amazing World of Gumball | The Flower; The Banana
10:00 am | Unikitty | Film Fest
10:15 am | Unikitty | Unikitty News!
10:30 am | Teen Titans Go! | Cat's Fancy
10:45 am | Teen Titans Go! | Leg Day
11:00 am | Teen Titans Go! | Boys vs. Girls; Body Adventure
11:30 am | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
11:45 am | Teen Titans Go! | Animals: It's Just a Word!
12:00 pm | Unikitty | Sparkle Matter Matters
12:15 pm | Unikitty | Action Forest
12:30 pm | Teen Titans Go! | Legendary Sandwich; Pie Bros
 1:00 pm | Teen Titans Go! | Dog Hand
 1:15 pm | Ben 10 | Screamcatcher
 1:30 pm | Teen Titans Go! | Double Trouble; The Date
 2:00 pm | Amazing World of Gumball | The Slide
 2:15 pm | Amazing World of Gumball | The Loophole
 2:30 pm | Amazing World of Gumball | The Pact
 2:45 pm | Unikitty | Dinner Apart-y
 3:00 pm | Craig of the Creek | Escape From Family Dinner
 3:15 pm | We Bare Bears | The Kitty
 3:30 pm | We Bare Bears | Dog Hotel
 3:45 pm | We Bare Bears | Bear Lift
 4:00 pm | Teen Titans Go! | Video Game References; Cool School
 4:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 5:00 pm | Teen Titans Go! | Fish Water
 5:15 pm | Teen Titans Go! | BBSFBDAY
 5:30 pm | Teen Titans Go! | The Streak, Part 1
 5:45 pm | Teen Titans Go! | The Streak, Part 2
 6:00 pm | Amazing World of Gumball | The News
 6:15 pm | Amazing World of Gumball | The Rival
 6:30 pm | Amazing World of Gumball | The Pact
 6:45 pm | Unikitty | Dinner Apart-y
 7:00 pm | Craig of the Creek | Wildernessa
 7:15 pm | Amazing World of Gumball | The Lady
 7:30 pm | Amazing World of Gumball | The Sucker
 7:45 pm | Ben 10 | Assault on Pancake Palace