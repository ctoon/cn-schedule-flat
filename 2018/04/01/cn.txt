# 2018-04-01
 6:00 am | Craig of the Creek | The Final Book
 6:15 am | Amazing World of Gumball | The Ape
 6:30 am | Amazing World of Gumball | The Quest/The Spoon
 7:00 am | Craig of the Creek | Too Many Treasures
 7:15 am | Teen Titans Go! | Terra-ized
 7:30 am | Teen Titans Go! | Matched/Burger vs. Burrito
 8:00 am | Craig of the Creek | Itch to Explore
 8:15 am | Teen Titans Go! | Lication
 8:30 am | Teen Titans Go! | Career Day/Ones and Zeros
 9:00 am | Craig of the Creek | You're It
 9:15 am | Amazing World of Gumball | The Mirror
 9:30 am | Amazing World of Gumball | The Bros/The Man
10:00 am | Craig of the Creek | Jessica Goes to the Creek
10:15 am | Unikitty | Fire & Nice
10:30 am | Unikitty | Rock Friend/Kitchen Chaos
11:00 am | Craig of the Creek | The Final Book
11:15 am | Teen Titans Go! | Serious Business
11:30 am | Teen Titans Go! | Body Adventure/Boys vs Girls
12:00 pm | Craig of the Creek | Too Many Treasures
12:15 pm | Amazing World of Gumball | The Rival/The News
12:45 pm | Amazing World of Gumball | The Lady
 1:00 pm | Craig of the Creek | Itch to Explore
 1:15 pm | Amazing World of Gumball | The Awkwardness
 1:30 pm | Amazing World of Gumball | The Nest/The Points
 2:00 pm | Craig of the Creek | You're It
 2:15 pm | Teen Titans Go! | Think About Your Future
 2:30 pm | Teen Titans Go! | Who's Laughing Now/Booty Scooty
 3:00 pm | Craig of the Creek | Jessica Goes to the Creek
 3:15 pm | Apple & Onion | Tips
 3:30 pm | Apple & Onion | Falafel's Fun Day/Apple's in Trouble
 4:00 pm | Craig of the Creek | The Final Book
 4:15 pm | Teen Titans Go! | Legs
 4:30 pm | Teen Titans Go! | Waffles
 4:45 pm | Ben 10 | Battle at Biggie Box
 5:00 pm | Craig of the Creek | Too Many Treasures
 5:15 pm | Amazing World of Gumball | The Father
 5:30 pm | Amazing World of Gumball | The Cage/The Cringe
 6:00 pm | Craig of the Creek | Itch to Explore
 6:15 pm | Teen Titans Go! | History Lesson
 6:30 pm | SPECIAL | Teen Titans Go!: Easter Special
 7:00 pm | Craig of the Creek | Wildernessa
 7:15 pm | Amazing World of Gumball | The Matchmaker
 7:30 pm | Amazing World of Gumball | The Console/The Ollie