# 2018-04-08
 6:00 am | Amazing World of Gumball | Halloween; The Treasure
 6:30 am | Amazing World of Gumball | The Words; The Apology
 7:00 am | Amazing World of Gumball | The Candidate; The Nuisance
 7:30 am | Craig of the Creek | Sunday Clothes; Wildernessa
 8:00 am | Craig of the Creek | Too Many Treasures; Jessica Goes To The Creek
 8:30 am | Amazing World of Gumball | The Faith; The Petals
 9:00 am | Amazing World of Gumball | The Cage; The News
 9:30 am | Unikitty | Unikitty News!; Little Prince Puppycorn
10:00 am | Unikitty | Film Fest; Stuck Together
10:30 am | Amazing World of Gumball | The Cringe; The Deal
11:00 am | Amazing World of Gumball | The Father; The Worst
11:30 am | Unikitty | Too Many Unikittys; Hide N' Seek
12:00 pm | Unikitty | The Zone; Wishing Well
12:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 1:00 pm | Amazing World of Gumball | The Boredom; The Guy
 1:30 pm | Unikitty | Lab Cat; Crushing Defeat
 2:00 pm | Unikitty | Birthday Blowout; 1 Action Forest
 2:30 pm | Amazing World of Gumball | The One; The Best
 3:00 pm | Amazing World of Gumball | The Anybody; The Origins
 3:30 pm | Amazing World of Gumball | The Origins Part 2
 3:45 pm | Teen Titans Go! | TTG v PPG
 4:00 pm | The Powerpuff Girls | Never Been Blissed
 4:30 pm | Amazing World of Gumball | The Vegging; The Heist
 5:00 pm | Amazing World of Gumball | The Sucker; The Menu
 5:30 pm | Unikitty | Kitty Court; Rock Friend
 6:00 pm | Unikitty | Pet Pet; Kaiju Kitty
 6:30 pm | Amazing World of Gumball | The List; The Line
 7:00 pm | Amazing World of Gumball | The Lady; The Sorcerer
 7:30 pm | Amazing World of Gumball | The Rival; The Diet