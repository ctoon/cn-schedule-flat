# 2018-04-23
 6:00 am | Steven Universe | Pool Hopping
 6:15 am | Teen Titans Go! | Demon Prom
 6:30 am | Teen Titans | Divide and Conquer
 7:00 am | Teen Titans | Forces of Nature
 7:30 am | Amazing World of Gumball | The Sorcerer/The Menu
 8:00 am | Amazing World of Gumball | The List/The Line
 8:30 am | Amazing World of Gumball | The Boredom/The Guy
 9:00 am | Amazing World of Gumball | The Choices
 9:15 am | Ben 10 | Xingo's Back
 9:30 am | Teen Titans Go! | Snuggle Time/The Overbite
10:00 am | Teen Titans Go! | Dignity of Teeth/Croissant
10:30 am | Craig of the Creek | Dog Decider/Monster in the Garden
11:00 am | Amazing World of Gumball | The Law/The Allergy
11:30 am | Amazing World of Gumball | The Mothers/The Password
12:00 pm | Unikitty | Fire & Nice/Rock Friend
12:30 pm | Teen Titans Go! | Hand Zombie/Employee of the Month Redux
 1:00 pm | Teen Titans Go! | Orangins/Jinxed
 1:30 pm | Amazing World of Gumball | The Knights/The Colossus
 2:00 pm | Amazing World of Gumball | The Phone/The Job
 2:30 pm | We Bare Bears | Fashion Bears/Losing Ice
 3:00 pm | We Bare Bears | Bear Flu/The Island
 3:30 pm | Amazing World of Gumball | The Pact
 3:45 pm | Unikitty | Dinner Apart-y
 4:00 pm | Teen Titans Go! | Lication/Ones and Zeros
 4:30 pm | Teen Titans Go! | Career Day/TV Knight 2
 5:00 pm | Apple & Onion | Perfect Team/Tips
 5:30 pm | Craig of the Creek | Bring Out Your Beast/The Curse
 6:00 pm | Amazing World of Gumball | The Heist/The Best
 6:30 pm | Amazing World of Gumball | The Shippening
 6:45 pm | Unikitty | License to Punch
 7:00 pm | Craig of the Creek | Jessica Goes to the Creek
 7:15 pm | Amazing World of Gumball | The Deal/The Worst
 7:45 pm | Apple & Onion | Falafel's Fun Day