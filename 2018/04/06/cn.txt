# 2018-04-06
 6:00 am | Amazing World of Gumball | The Flower/The Banana
 6:30 am | Amazing World of Gumball | The Phone/The Job
 7:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 am | Teen Titans Go! | Birds/Be Mine
 8:00 am | Teen Titans Go! | Dude Relax!/Laundry Day
 8:30 am | Teen Titans Go! | Ghostboy
 8:45 am | Craig of the Creek | Sunday Clothes
 9:00 am | Teen Titans Go! | Obinray/Wally T
 9:30 am | Teen Titans Go! | Love Monsters/Baby Hands
10:00 am | Teen Titans Go! | Sandwich Thief/Money Grandma
10:30 am | Teen Titans Go! | La Larva de Amor
10:45 am | Craig of the Creek | You're It
11:00 am | SPECIAL | Teen Titans Go!: Titans vs. the H.I.V.E. Five
12:00 pm | Ben 10 | The 11th Alien
12:30 pm | OK K.O.! Let's Be Heroes | You're in Control
 1:00 pm | Teen Titans Go! | Jinxed/Brain Percentages
 1:30 pm | Teen Titans Go! | Hot Salad Water/BL4Z3
 2:00 pm | Teen Titans Go! | Ones and Zeros/Lication
 2:30 pm | Teen Titans Go! | Shrimps and Prime Rib/The Academy
 2:45 pm | Craig of the Creek | Jessica Goes to the Creek
 3:00 pm | Teen Titans Go! | Two Parter Part 2/Two Parter Part 1
 3:30 pm | Teen Titans Go! | Operation Dude Rescue Part 2/Operation Dude Rescue Part 1
 4:00 pm | Teen Titans Go! | Robin Backwards/Crazy Day
 4:30 pm | Teen Titans Go! | Booby Trap House
 4:45 pm | Craig of the Creek | The Final Book
 5:00 pm | Teen Titans Go! | Streak Part 1/The Streak Part 2
 5:30 pm | Teen Titans Go! | BBRAE
 6:00 pm | SPECIAL | Teen Titans Go!: Titans vs. the H.I.V.E. Five
 7:00 pm | Craig of the Creek | Sunday Clothes/Wildernessa
 7:30 pm | Craig of the Creek | Itch to Explore/Too Many Treasures