# 2018-04-07
 6:00 am | Amazing World of Gumball | The Halloween/The Treasure
 6:30 am | Amazing World of Gumball | The Words/The Apology
 7:00 am | Amazing World of Gumball | The Candidate/The Nuisance
 7:30 am | Craig of the Creek | Wildernessa/Sunday Clothes
 8:00 am | Craig of the Creek | Too Many Treasures/Jessica Goes to the Creek
 8:30 am | Amazing World of Gumball | The Petals/The Faith
 9:00 am | Amazing World of Gumball | The News/The Cage
 9:30 am | Unikitty | Unikitty News!/Little Prince Puppycorn
10:00 am | Unikitty | Stuck Together/Film Fest
10:30 am | Amazing World of Gumball | The Cringe/The Deal
11:00 am | Amazing World of Gumball | The Father/The Worst
11:30 am | Unikitty | Too Many Unikittys/Hide N' Seek
12:00 pm | Unikitty | Zone/Wishing Well
12:30 pm | Amazing World of Gumball | The Disaster/The Re-Run
 1:00 pm | Amazing World of Gumball | The Guy/The Boredom
 1:30 pm | Unikitty | Crushing Defeat/Lab Cat
 2:00 pm | Unikitty | Action Forest/Birthday Blowout
 2:30 pm | Amazing World of Gumball | The One/The Best
 3:00 pm | Amazing World of Gumball | The Origins/The Anybody
 3:30 pm | Amazing World of Gumball | The Origins Part 2
 3:45 pm | SPECIAL | TTG v PPG
 4:00 pm | The Powerpuff Girls | Never Been Blissed
 4:30 pm | Amazing World of Gumball | The Vegging/The Heist
 5:00 pm | Amazing World of Gumball | The Menu/The Sucker
 5:30 pm | Unikitty | Rock Friend/Kitty Court
 6:00 pm | Unikitty | Kaiju Kitty/Pet Pet
 6:30 pm | Amazing World of Gumball | The List/The Line
 7:00 pm | Amazing World of Gumball | The Lady/The Sorcerer
 7:30 pm | Amazing World of Gumball | The Diet/The Rival