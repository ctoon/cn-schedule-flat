# 2018-04-25
 6:00 am | Steven Universe | Steven's Lion
 6:15 am | Teen Titans Go! | Orangins
 6:30 am | Teen Titans | Sum of His Parts
 7:00 am | Teen Titans | Nevermore
 7:30 am | Amazing World of Gumball | The Lady/The Sucker
 8:00 am | Amazing World of Gumball | The Vegging/The One
 8:30 am | Amazing World of Gumball | The Test/The Code
 9:00 am | Amazing World of Gumball | The Loophole/The Slide
 9:30 am | Teen Titans Go! | Shrimps and Prime Rib/Booby Trap House
10:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory/Accept the Next Proposition You Hear
10:30 am | Craig of the Creek | Jessica Goes to the Creek/Itch to Explore
11:00 am | Amazing World of Gumball | The Procrastinators/The Shell
11:30 am | Amazing World of Gumball | The Mirror/The Burden
12:00 pm | Unikitty | Wishing Well/Hide N' Seek
12:30 pm | Teen Titans Go! | Mr. Butt
12:45 pm | Ben 10 | Xingo's Back
 1:00 pm | Teen Titans Go! | Pirates/I See You
 1:30 pm | Amazing World of Gumball | The Skull/The Treasure
 2:00 pm | Amazing World of Gumball | The Words/The Apology
 2:30 pm | We Bare Bears | Cellie/Baby Bears on a Plane
 3:00 pm | We Bare Bears | Viral Video/Our Stuff
 3:30 pm | Amazing World of Gumball | The Shippening
 3:45 pm | Unikitty | License to Punch
 4:00 pm | Teen Titans Go! | Academy/Throne of Bones
 4:30 pm | Teen Titans Go! | Demon Prom/BBCYFSHIPBDAY
 5:00 pm | Apple & Onion | Apple's in Trouble/4 on 1
 5:30 pm | Craig of the Creek | Sunday Clothes/Wildernessa
 6:00 pm | Amazing World of Gumball | The Line/The Fuss
 6:30 pm | Amazing World of Gumball | The News/The Petals
 7:00 pm | Craig of the Creek | Too Many Treasures
 7:15 pm | Amazing World of Gumball | The Shippening
 7:30 pm | Unikitty | License to Punch
 7:45 pm | Apple & Onion | Hotdog's Movie Premiere