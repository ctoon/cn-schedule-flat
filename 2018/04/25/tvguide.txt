# 2018-04-25
 6:00 am | Steven Universe | Steven's Lion
 6:15 am | Teen Titans Go! | Orangins
 6:30 am | Teen Titans | The Sum of His Parts
 7:00 am | Teen Titans | Nevermore
 7:30 am | Amazing World of Gumball | The Sucker; The Lady
 8:00 am | Amazing World of Gumball | The One; The Vegging
 8:30 am | Amazing World of Gumball | The Code; The Test
 9:00 am | Amazing World of Gumball | The Slide; The Loophole
 9:30 am | Teen Titans Go! | Booby Trap House; Shrimps and Prime Rib
10:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory; Accept the Next Proposition You Hear
10:30 am | Craig of the Creek | Itch To Explore; Jessica Goes To The Creek
11:00 am | Amazing World of Gumball | The Procrastinators; The Shell
11:30 am | Amazing World of Gumball | The Mirror; The Burden
12:00 pm | Unikitty | Hide N' Seek; Wishing Well
12:30 pm | Teen Titans Go! | Mr. Butt
12:45 pm | Ben 10 | Xingo's Back
 1:00 pm | Teen Titans Go! | Pirates; I See You
 1:30 pm | Amazing World of Gumball | The Treasure; The Skull
 2:00 pm | Amazing World of Gumball | The Words; The Apology
 2:30 pm | We Bare Bears | Cellie; Baby Bears On A Plane
 3:00 pm | We Bare Bears | Our Stuff; Viral Video
 3:30 pm | Amazing World of Gumball | The Shippening
 3:45 pm | Unikitty | License to Punch
 4:00 pm | Teen Titans Go! | Throne of Bones; The Academy
 4:30 pm | Teen Titans Go! | Demon Prom; BBCYFSHIPBDAY
 5:00 pm | Apple & Onion | Apple's In Trouble; 4 on 1
 5:30 pm | Craig of the Creek | Sunday Clothes; Wildernessa
 6:00 pm | Amazing World of Gumball | The Fuss; The Line
 6:30 pm | Amazing World of Gumball | The News; The Petals
 7:00 pm | Craig of the Creek | Too Many Treasures
 7:15 pm | Amazing World of Gumball | The Shippening
 7:30 pm | Unikitty | License to Punch
 7:45 pm | Apple & Onion | Hotdog's Movie Premiere