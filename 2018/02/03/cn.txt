# 2018-02-03
 6:00 am | Amazing World of Gumball | The Ape/The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest/The Spoon
 7:00 am | Unikitty | Fire & Nice/Kaiju Kitty
 7:30 am | Unikitty | Kitchen Chaos/Rock Friend
 8:00 am | Ben 10 | Riding the Storm Out/The Clocktopus
 8:30 am | OK K.O.! Let's Be Heroes | You Get Me/My Dad Can Beat up Your Dad
 9:00 am | Teen Titans Go! | Overbite/Riding the Dragon
 9:30 am | Teen Titans Go! | Shrimps and Prime Rib/The Cape
10:00 am | Teen Titans Go! | Super Robin/Tower Power
10:30 am | Teen Titans Go! | Parasite/Starliar
11:00 am | Teen Titans Go! | Meatball Party/Staff Meeting/Artful Dodgers
11:30 am | MOVIE | Ben 10: Alien Swarm
 1:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 1:30 pm | Unikitty | Little Prince Puppycorn/Pet Pet
 2:00 pm | Unikitty | Action Forest/Crushing Defeat
 2:30 pm | Amazing World of Gumball | Uploads/The Romantic
 3:00 pm | Amazing World of Gumball | Traitor/The Apprentice
 3:30 pm | Amazing World of Gumball | Vase/The Wicked
 4:00 pm | Amazing World of Gumball | Hug/The Awkwardness
 4:30 pm | Amazing World of Gumball | One/The Cringe
 5:00 pm | Amazing World of Gumball | The OriginsPart 2/The Origins
 5:30 pm | Amazing World of Gumball | Disaster/The Re-Run
 5:55 pm | SPECIAL | Teen Titans Go: Island Adventures
 7:00 pm | Unikitty | Pet Pet/Wishing Well
 7:30 pm | Unikitty | Hide N' Seek/Stuck Together