# 2018-02-18
 6:00 am | Teen Titans Go! | TV Knight/Shrimps and Prime Rib
 6:30 am | Teen Titans Go! | TV Knight 2/Permanent Record
 7:00 am | Teen Titans Go! | Robin Backwards/Crazy Day
 7:30 am | Teen Titans Go! | Smile Bones/Real Boy Adventures
 8:00 am | Teen Titans Go! | Birds/Be Mine
 8:30 am | Teen Titans Go! | Brain Food/In and Out
 9:00 am | Teen Titans Go! | Titan Saving Time/Movie Night
 9:30 am | Teen Titans Go! | Employee of the Month Redux/The Gold Standard
10:00 am | Teen Titans Go! | Career Day/Ones and Zeros
10:30 am | Teen Titans Go! | BL4Z3/Brain Percentages
11:00 am | Teen Titans Go! | Serious Business/Caramel Apples
11:30 am | Teen Titans Go! | Sandwich Thief/Money Grandma
12:00 pm | Teen Titans Go! | Two Parter: Part 1/Two Parter: Part 2
12:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1/Operation Dude Rescue: Part 2
 1:00 pm | Teen Titans Go! | Streak: Part 1/The Streak: Part 2
 1:30 pm | Teen Titans Go! | Orangins
 1:45 pm | Teen Titans Go! | BBRAE
 2:15 pm | Teen Titans Go! | BBBDay!
 2:30 pm | Teen Titans Go! | BBCYFSHIPBDAY/BBSFBDAY!
 3:00 pm | SPECIAL | Teen Titans Go: Island Adventures
 4:00 pm | Teen Titans Go! | Night Begins to Shine Special
 5:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:00 pm | Teen Titans Go! | Beast Girl
 6:15 pm | Ben 10 | Out to Launch
 6:30 pm | Ben 10 | Battle at Biggie Box
 6:45 pm | OK K.O.! Let's Be Heroes | Let's Watch the Pilot
 7:00 pm | OK K.O.! Let's Be Heroes | Mystery Science Fair 201X
 7:15 pm | SPECIAL | Teen Titans Go!: Opposite Titans