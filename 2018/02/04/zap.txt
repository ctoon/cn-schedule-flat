# 2018-02-04
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Unikitty | Kaiju Kitty
 7:15 am | Unikitty | Fire & Nice
 7:30 am | Unikitty | A Rock Friend Indeed
 7:45 am | Unikitty | Kitchen Chaos
 8:00 am | Ben 10 | Riding the Storm Out
 8:15 am | Ben 10 | The Clocktopus
 8:30 am | OK K.O.! Let's Be Heroes | My Dad Can Beat Up Your Dad
 8:45 am | OK K.O.! Let's Be Heroes | You Get Me
 9:00 am | Teen Titans Go! | Riding the Dragon
 9:15 am | Teen Titans Go! | The Overbite
 9:30 am | Teen Titans Go! | The Cape
 9:45 am | Teen Titans Go! | Shrimps and Prime Rib
10:00 am | Teen Titans Go! | Super Robin; Tower Power
10:30 am | Teen Titans Go! | Parasite; Starliar
11:00 am | Teen Titans Go! | Meatball Party; Staff Meeting
11:30 am | Ben 10: Alien Swarm | 
 1:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 1:30 pm | Unikitty | Pet Pet
 1:45 pm | Unikitty | Little Prince Puppycorn
 2:00 pm | Unikitty | Action Forest
 2:15 pm | Unikitty | Crushing Defeat
 2:30 pm | Amazing World of Gumball | The Romantic
 2:45 pm | Amazing World of Gumball | The Uploads
 3:00 pm | Amazing World of Gumball | The Apprentice
 3:15 pm | Amazing World of Gumball | The Traitor
 3:30 pm | Amazing World of Gumball | The Wicked
 3:45 pm | Amazing World of Gumball | The Vase
 4:00 pm | Amazing World of Gumball | The Hug
 4:15 pm | Amazing World of Gumball | The Awkwardness
 4:30 pm | Amazing World of Gumball | The Cringe
 4:45 pm | Amazing World of Gumball | The One
 5:00 pm | Amazing World of Gumball | The Origins
 5:15 pm | Amazing World of Gumball | The Origins
 5:30 pm | Amazing World of Gumball | The Disaster
 5:45 pm | Amazing World of Gumball | The Re-Run
 5:55 pm | Teen Titans Go! | Island Adventures
 7:00 pm | Unikitty | Pet Pet
 7:15 pm | Unikitty | Wishing Well
 7:30 pm | Unikitty | Hide n' Seek
 7:45 pm | Unikitty | Stuck Together