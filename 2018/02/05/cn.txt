# 2018-02-05
 6:00 am | Amazing World of Gumball | The Helmet/The Fight
 6:30 am | Amazing World of Gumball | The End/The DVD
 7:00 am | Teen Titans Go! | Brain Food/In and Out
 7:30 am | Teen Titans Go! | Little Buddies/Missing
 8:00 am | Teen Titans Go! | Driver's Ed/Dog Hand
 8:30 am | Teen Titans Go! | Double Trouble/The Date
 9:00 am | Amazing World of Gumball | The Countdown/The Nobody
 9:30 am | Amazing World of Gumball | The Downer/The Egg
10:00 am | Unikitty | Wishing Well/Kaiju Kitty
10:30 am | Unikitty | Rock Friend/Kitchen Chaos
11:00 am | Teen Titans Go! | Colors of Raven/The Left Leg
11:30 am | Teen Titans Go! | Books/Lazy Sunday
12:00 pm | Teen Titans Go! | Squash & Stretch/BBBDay!
12:30 pm | Teen Titans Go! | Secret Garden/Garage Sale
 1:00 pm | Unikitty | Sparkle Matter Matters/Spoooooky Game
 1:30 pm | Amazing World of Gumball | The Name/The Extras
 2:00 pm | Amazing World of Gumball | The Gripes/The Vacation
 2:30 pm | Amazing World of Gumball | The Fraud/The Void
 3:00 pm | We Bare Bears | Primal/Everyday Bears
 3:30 pm | We Bare Bears | Nom Nom/The Road
 4:00 pm | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
 4:30 pm | Teen Titans Go! | Mr. Butt/Man Person
 5:00 pm | Teen Titans Go! | BL4Z3/Demon Prom
 5:30 pm | Amazing World of Gumball | Line/The Father
 6:00 pm | Amazing World of Gumball | Lady/The Best
 6:30 pm | Unikitty | Crushing Defeat/Little Prince Puppycorn
 7:00 pm | Teen Titans Go! | Career Day/The Avogodo
 7:30 pm | We Bare Bears | Bearz II Men/Beehive