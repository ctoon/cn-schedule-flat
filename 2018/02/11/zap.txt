# 2018-02-11
 6:00 am | Teen Titans | X
 6:30 am | Teen Titans | Betrothed
 7:00 am | Unikitty | A Rock Friend Indeed
 7:15 am | Unikitty | Crushing Defeat
 7:30 am | Unikitty | Wishing Well
 7:45 am | Unikitty | Stuck Together
 8:00 am | Ben 10 | Don't Let the Bass Drop
 8:15 am | Ben 10 | Bad Penny
 8:30 am | OK K.O.! Let's Be Heroes | No More Pow Cards
 8:45 am | OK K.O.! Let's Be Heroes | A Hero's Fate
 9:00 am | Amazing World of Gumball | The Origins
 9:15 am | Amazing World of Gumball | The Origins
 9:30 am | Amazing World of Gumball | The Girlfriend
 9:45 am | Amazing World of Gumball | The Advice
10:00 am | Amazing World of Gumball | The Signal
10:15 am | Amazing World of Gumball | The Parasite
10:30 am | Amazing World of Gumball | The Love
10:45 am | Amazing World of Gumball | The Awkwardness
11:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
11:30 am | Teen Titans Go! | Books; Lazy Sunday
12:00 pm | Ben 10 | Zombozo-Land
12:15 pm | Ben 10 | Forgeti
12:30 pm | OK K.O.! Let's Be Heroes | Let's Have a Stakeout
12:45 pm | OK K.O.! Let's Be Heroes | Rad Likes Robots
 1:00 pm | Amazing World of Gumball | The Nest
 1:15 pm | Amazing World of Gumball | The Points
 1:30 pm | Amazing World of Gumball | The Bus
 1:45 pm | Amazing World of Gumball | The Night
 2:00 pm | Teen Titans | Switched
 2:30 pm | Teen Titans | Deep Six
 3:00 pm | We Bare Bears | I Am Ice Bear
 3:15 pm | We Bare Bears | The Park
 3:30 pm | We Bare Bears | Hurricane Hal
 3:45 pm | We Bare Bears | Vacation
 4:00 pm | Teen Titans Go! | The Streak, Part 1
 4:15 pm | Teen Titans Go! | The Streak, Part 2
 4:30 pm | Teen Titans Go! | Inner Beauty of a Cactus
 4:45 pm | Teen Titans Go! | Movie Night
 5:00 pm | Unikitty | Hide n' Seek
 5:15 pm | OK K.O.! Let's Be Heroes | KO's Video Channel
 5:30 pm | OK K.O.! Let's Be Heroes | The Power Is Yours!
 5:45 pm | OK K.O.! Let's Be Heroes | Glory Days
 6:00 pm | Amazing World of Gumball | The Father
 6:15 pm | Amazing World of Gumball | The Cage
 6:30 pm | Amazing World of Gumball | The Matchmaker
 6:45 pm | Amazing World of Gumball | The Box
 7:00 pm | Unikitty | Kitty Court
 7:15 pm | Unikitty | Little Prince Puppycorn
 7:30 pm | Unikitty | Pet Pet
 7:45 pm | Unikitty | Sparkle Matter Matters