# 2018-02-27
 6:00 am | Amazing World of Gumball | The Bros; The Man
 6:30 am | Amazing World of Gumball | The Pizza; The Lie
 7:00 am | Teen Titans Go! | The Dignity of Teeth
 7:15 am | Teen Titans Go! | The Croissant
 7:30 am | Teen Titans Go! | The Spice Game
 7:45 am | Teen Titans Go! | I'm the Sauce
 8:00 am | Teen Titans Go! | Sandwich Thief; Money Grandma
 8:30 am | Amazing World of Gumball | The Vase
 8:45 am | Amazing World of Gumball | The Matchmaker
 9:00 am | Amazing World of Gumball | The Faith
 9:15 am | Amazing World of Gumball | The Cage
 9:30 am | Unikitty | Lab Cat
 9:45 am | Unikitty | Crushing Defeat
10:00 am | Unikitty | Stuck Together
10:15 am | Unikitty | Pet Pet
10:30 am | Teen Titans Go! | Friendship; Vegetables
11:00 am | Teen Titans Go! | Double Trouble; The Date
11:30 am | Teen Titans Go! | Dude Relax; Laundry Day
12:00 pm | Unikitty | Kitchen Chaos
12:15 pm | Unikitty | Wishing Well
12:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 1:00 pm | Teen Titans Go! | Video Game References; Cool School
 1:30 pm | Amazing World of Gumball | The Slide
 1:45 pm | Amazing World of Gumball | The Loophole
 2:00 pm | Amazing World of Gumball | The Copycats
 2:15 pm | Amazing World of Gumball | The Potato
 2:30 pm | Amazing World of Gumball | The Fuss
 2:45 pm | Amazing World of Gumball | The Outside
 3:00 pm | We Bare Bears | Private Lake
 3:15 pm | We Bare Bears | Lunch With Tabes
 3:30 pm | We Bare Bears | Road Trip
 3:45 pm | We Bare Bears | Summer Love
 4:00 pm | Teen Titans Go! | Grube's Fairytales
 4:15 pm | Teen Titans Go! | A Farce
 4:30 pm | Teen Titans Go! | Animals: It's Just a Word
 4:45 pm | Teen Titans Go! | BBB Day!
 5:00 pm | Teen Titans Go! | Oregon Trail
 5:15 pm | Teen Titans Go! | Snuggle Time
 5:30 pm | Apple & Onion | A New Life
 5:45 pm | Apple & Onion | The Perfect Team
 6:00 pm | Amazing World of Gumball | The Singing
 6:15 pm | Amazing World of Gumball | The Best
 6:30 pm | Unikitty | Sparkle Matter Matters
 6:45 pm | Unikitty | Action Forest
 7:00 pm | Amazing World of Gumball | The Worst
 7:15 pm | Amazing World of Gumball | The Deal
 7:30 pm | Amazing World of Gumball | The Petals
 7:45 pm | Ben 10 | Mayhem in Mascot