# 2018-02-16
 6:00 am | Teen Titans Go! | Legendary Sandwich/Pie Bros
 6:30 am | Teen Titans Go! | Driver's Ed/Dog Hand
 7:00 am | Teen Titans Go! | Employee of the Month Redux/The Avogodo
 7:30 am | Teen Titans Go! | Master Detective/Hand Zombie
 8:00 am | SPECIAL | Teen Titans Go: Island Adventures
 9:00 am | Teen Titans Go! | Night Begins to Shine Special/40%, 40%, 20%
10:00 am | Teen Titans Go! | BBRAE
10:30 am | Teen Titans Go! | Ghostboy
10:45 am | Teen Titans Go! | BBBDay!
11:00 am | Teen Titans Go! | BBCYFSHIPBDAY/BBSFBDAY!
11:30 am | Teen Titans Go! | Streak: Part 1/The Streak: Part 2
12:00 pm | Teen Titans Go! | Titan Saving Time/Permanent Record
12:30 pm | Teen Titans Go! | Movie Night/Inner Beauty of a Cactus
 1:00 pm | Teen Titans Go! | Booby Trap House/Fish Water
 1:30 pm | Teen Titans Go! | Orangins/TV Knight
 2:00 pm | Teen Titans Go! | Overbite/Shrimps and Prime Rib
 2:30 pm | Teen Titans Go! | Oh Yeah!/Riding the Dragon
 3:00 pm | Teen Titans Go! | Hey Pizza!/Gorilla
 3:30 pm | Teen Titans Go! | Girls' Night Out/You're Fired
 4:00 pm | Teen Titans Go! | Super Robin/Tower Power
 4:30 pm | Teen Titans Go! | Parasite/Starliar
 5:00 pm | Teen Titans Go! | Garage Sale/Secret Garden
 5:30 pm | Teen Titans Go! | Animals, It's Just a Word!/Squash & Stretch
 6:00 pm | Teen Titans Go! | Two Parter: Part 1/Two Parter: Part 2
 6:30 pm | Teen Titans Go! | Throne of Bones/Operation Dude Rescue: Part 1/Operation Dude Rescue: Part 2
 7:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 7:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!