# 2018-02-22
 6:00 am | Amazing World of Gumball | The Coach; The Joy
 6:30 am | Amazing World of Gumball | The Recipe; The Puppy
 7:00 am | Teen Titans Go! | Video Game References; Cool School
 7:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 8:00 am | Teen Titans Go! | Mr. Butt; Man Person
 8:30 am | Amazing World of Gumball | The Guy
 8:45 am | Amazing World of Gumball | The Boredom
 9:00 am | Amazing World of Gumball | The Vision
 9:15 am | Amazing World of Gumball | The Choices
 9:30 am | Unikitty | Stuck Together
 9:45 am | Unikitty | Little Prince Puppycorn
10:00 am | Unikitty | Pet Pet
10:15 am | Unikitty | Birthday Blowout
10:30 am | Teen Titans Go! | Pirates; I See You
11:00 am | Teen Titans Go! | Ones and Zeroes
11:15 am | Teen Titans Go! | Career Day
11:30 am | Teen Titans Go! | Hot Salad Water
11:45 am | Teen Titans Go! | Lication
12:00 pm | Unikitty | Action Forest
12:15 pm | Unikitty | Kaiju Kitty
12:30 pm | Teen Titans Go! | Mouth Hole; Hot Garbage
 1:00 pm | Teen Titans Go! | Robin Backwards; Crazy Day
 1:30 pm | Amazing World of Gumball | The Points
 1:45 pm | Amazing World of Gumball | The Bus
 2:00 pm | Amazing World of Gumball | The Night
 2:15 pm | Amazing World of Gumball | The Misunderstandings
 2:30 pm | Amazing World of Gumball | The Roots
 2:45 pm | Amazing World of Gumball | The Blame
 3:00 pm | We Bare Bears | Planet Bears
 3:15 pm | We Bare Bears | Coffee Cave
 3:30 pm | We Bare Bears | Charlie's Big Foot
 3:45 pm | We Bare Bears | The Demon
 4:00 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 4:30 pm | Teen Titans Go! | Cat's Fancy
 4:45 pm | Teen Titans Go! | Leg Day
 5:00 pm | Teen Titans Go! | Rad Dudes With Bad Tudes
 5:15 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 5:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 2
 5:45 pm | Teen Titans Go! | History Lesson
 6:00 pm | Amazing World of Gumball | The Cycle
 6:15 pm | Amazing World of Gumball | The Stars
 6:30 pm | Unikitty | Kitty Court
 6:45 pm | Unikitty | Spoooooky Game
 7:00 pm | Amazing World of Gumball | The Grades
 7:15 pm | Amazing World of Gumball | The Diet
 7:30 pm | Amazing World of Gumball | The Ex
 7:45 pm | Ben 10 | Need for Speed