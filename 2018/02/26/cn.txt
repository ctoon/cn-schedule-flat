# 2018-02-26
 6:00 am | Amazing World of Gumball | The Bros/The Man
 6:30 am | Amazing World of Gumball | The Pizza/The Lie
 7:00 am | Teen Titans Go! | Croissant/Dignity of Teeth
 7:30 am | Teen Titans Go! | Spice Game/I'm the Sauce
 8:00 am | Teen Titans Go! | Sandwich Thief/Money Grandma
 8:30 am | Amazing World of Gumball | Vase/The Matchmaker
 9:00 am | Amazing World of Gumball | Cage/The Faith
 9:30 am | Unikitty | Crushing Defeat/Lab Cat
10:00 am | Unikitty | Stuck Together/Pet Pet
10:30 am | Teen Titans Go! | Friendship/Vegetables
11:00 am | Teen Titans Go! | Double Trouble/The Date
11:30 am | Teen Titans Go! | Dude Relax!/Laundry Day
12:00 pm | Unikitty | Kitchen Chaos/Wishing Well
12:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums
 1:00 pm | Teen Titans Go! | Video Game References/Cool School
 1:30 pm | Amazing World of Gumball | Loophole/The Slide
 2:00 pm | Amazing World of Gumball | Copycats/The Potato
 2:30 pm | Amazing World of Gumball | Outside/The Fuss
 3:00 pm | We Bare Bears | Private Lake/Lunch with Tabes
 3:30 pm | We Bare Bears | Road Trip/Summer Love
 4:00 pm | Teen Titans Go! | A Farce/Grube's Fairytales
 4:30 pm | Teen Titans Go! | Animals, It's Just a Word!/BBBDay!
 5:00 pm | Teen Titans Go! | Oregon Trail/Snuggle Time
 5:30 pm | Apple & Onion | Perfect Team/A New Life
 6:00 pm | Amazing World of Gumball | Singing/The Best
 6:30 pm | Unikitty | Action Forest/Sparkle Matter Matters
 7:00 pm | Amazing World of Gumball | Worst/The Deal
 7:30 pm | Amazing World of Gumball | The Petals
 7:45 pm | Ben 10 | Mayhem in Mascot