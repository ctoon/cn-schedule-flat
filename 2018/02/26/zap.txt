# 2018-02-26
 6:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 am | Amazing World of Gumball | The Mirror; The Burden
 7:00 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 7:30 am | Teen Titans Go! | Cat's Fancy
 7:45 am | Teen Titans Go! | Leg Day
 8:00 am | Teen Titans Go! | Love Monsters; Baby Hands
 8:30 am | Amazing World of Gumball | The Copycats
 8:45 am | Amazing World of Gumball | The Potato
 9:00 am | Amazing World of Gumball | The Fuss
 9:15 am | Amazing World of Gumball | The Outside
 9:30 am | Unikitty | Fire & Nice
 9:45 am | Unikitty | A Rock Friend Indeed
10:00 am | Unikitty | Kitchen Chaos
10:15 am | Unikitty | Wishing Well
10:30 am | Teen Titans Go! | Caramel Apples
10:45 am | Teen Titans Go! | Serious Business
11:00 am | Teen Titans Go! | Legendary Sandwich; Pie Bros
11:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
12:00 pm | Unikitty | Crushing Defeat
12:15 pm | Unikitty | Hide n' Seek
12:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 1:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 1:30 pm | Amazing World of Gumball | The Guy
 1:45 pm | Amazing World of Gumball | The Boredom
 2:00 pm | Amazing World of Gumball | The Vision
 2:15 pm | Amazing World of Gumball | The Choices
 2:30 pm | Amazing World of Gumball | The Code
 2:45 pm | Amazing World of Gumball | The Test
 3:00 pm | We Bare Bears | Panda's Art
 3:15 pm | We Bare Bears | Poppy Rangers
 3:30 pm | We Bare Bears | Lucy's Brother
 3:45 pm | We Bare Bears | The Fair
 4:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 4:15 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 4:30 pm | Teen Titans Go! | The Fourth Wall
 4:45 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 5:00 pm | Teen Titans Go! | Island Adventures
 6:00 pm | Ben 10 | Bon Voyage
 6:15 pm | Ben 10 | Mayhem in Mascot
 6:30 pm | OK K.O.! Let's Be Heroes | RMS & Brandon's First Episode
 6:45 pm | OK K.O.! Let's Be Heroes | Lad & Logic
 7:00 pm | Apple & Onion | A New Life
 7:15 pm | Apple & Onion | The Perfect Team
 7:30 pm | Amazing World of Gumball | The Faith
 7:45 pm | Unikitty | Lab Cat