# 2018-06-04
 6:00 am | Steven Universe | Watermelon Steven
 6:15 am | Ben 10 | Bomzobo Lives
 6:30 am | Teen Titans Go! | The Cruel Giggling Ghoul
 6:45 am | Teen Titans Go! | Pyramid Scheme
 7:00 am | Unikitty | Bugging Out
 7:15 am | Craig of the Creek | Escape From Family Dinner
 7:30 am | Craig of the Creek | You're It
 7:45 am | Craig of the Creek | Monster in the Garden
 8:00 am | Unikitty | Chair
 8:15 am | Teen Titans Go! | Bro-Pocalypse
 8:30 am | Teen Titans Go! | Bottle Episode
 8:45 am | Teen Titans Go! | Finally a Lesson
 9:00 am | Unikitty | Kickflip McPuppycorn
 9:15 am | Amazing World of Gumball | The Countdown
 9:30 am | Amazing World of Gumball | The Downer; The Egg
10:00 am | Unikitty | Super Amazing Raft Adventure
10:15 am | Teen Titans Go! | Boys vs. Girls
10:30 am | Teen Titans Go! | Road Trip; The Best Robin
11:00 am | Unikitty | Tasty Heist
11:15 am | Teen Titans Go! | Mouth Hole
11:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
12:00 pm | Unikitty | Bugging Out
12:15 pm | Amazing World of Gumball | The Vision
12:30 pm | Amazing World of Gumball | The Choices
12:45 pm | Amazing World of Gumball | The List
 1:00 pm | Unikitty | Chair
 1:15 pm | Teen Titans Go! | Permanent Record
 1:30 pm | Teen Titans Go! | Titan Saving Time
 1:45 pm | Teen Titans Go! | Lication
 2:00 pm | Unikitty | Kickflip McPuppycorn
 2:15 pm | Teen Titans Go! | Master Detective
 2:30 pm | Teen Titans Go! | Employee of the Month Redux
 2:45 pm | Teen Titans Go! | Ones and Zeroes
 3:00 pm | Unikitty | Super Amazing Raft Adventure
 3:15 pm | OK K.O.! Let's Be Heroes | Let's Be Heroes
 3:30 pm | OK K.O.! Let's Be Heroes | Let's Be Friends
 3:45 pm | OK K.O.! Let's Be Heroes | You're Everybody's Sidekick
 4:00 pm | Unikitty | Tasty Heist
 4:15 pm | Amazing World of Gumball | The Pony
 4:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 5:00 pm | Unikitty | Bugging Out
 5:15 pm | Craig of the Creek | Itch to Explore
 5:30 pm | Craig of the Creek | Wildernessa
 5:45 pm | Craig of the Creek | Jessica Goes to the Creek
 6:00 pm | Unikitty | Chair
 6:15 pm | Teen Titans Go! | Jinxed
 6:30 pm | Teen Titans Go! | Two Parter: Part One
 6:45 pm | Teen Titans Go! | Two Parter: Part Two
 7:00 pm | Unikitty | Kickflip McPuppycorn
 7:15 pm | We Bare Bears | Cupcake Job
 7:30 pm | We Bare Bears | Panda's Sneeze
 7:45 pm | We Bare Bears | Brother Up