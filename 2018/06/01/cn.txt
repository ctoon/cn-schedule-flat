# 2018-06-01
 6:00 am | Steven Universe | Garnet's Universe
 6:15 am | Justice League Action | Keeping Up with the Kryptonians
 6:30 am | Teen Titans | Deception
 7:00 am | Ben 10 | Mayhem in Mascot
 7:15 am | Amazing World of Gumball | The Faith
 7:30 am | Amazing World of Gumball | News/The Candidate
 8:00 am | Amazing World of Gumball | Rival/The Anybody
 8:30 am | Teen Titans Go! | I'm the Sauce/BBBDay!
 9:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory/Squash & Stretch
 9:30 am | Amazing World of Gumball | The Friend/The Saint
10:00 am | Amazing World of Gumball | The Society/The Spoiler
10:30 am | Craig of the Creek | Dog Decider/The Final Book
11:00 am | Teen Titans Go! | Accept the Next Proposition You Hear/Garage Sale
11:30 am | Teen Titans Go! | Fourth Wall/Secret Garden
12:00 pm | Teen Titans Go! | Yearbook Madness/Hose Water
12:30 pm | Amazing World of Gumball | Guy/The Puppets
 1:00 pm | Amazing World of Gumball | Boredom/The Line
 1:30 pm | Unikitty | Fire & Nice/Kitty Court
 2:00 pm | Teen Titans Go! | Truth, Justice, and What?/Beast Man
 2:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums
 3:00 pm | Teen Titans Go! | Beast Girl
 3:15 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 3:45 pm | Teen Titans Go! | Flashback
 4:15 pm | MOVIE | Lego DC Comics Super Heroes: The Flash
 6:00 pm | Teen Titans Go! | Bro-Pocalypse
 6:15 pm | MOVIE | Lego DC Comics Super Heroes: The Flash