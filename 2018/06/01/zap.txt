# 2018-06-01
 6:00 am | Steven Universe | Garnet's Universe
 6:15 am | Justice League Action | Keeping Up With the Kryptonians
 6:30 am | Teen Titans | Deception
 7:00 am | Ben 10 | Mayhem in Mascot
 7:15 am | Amazing World of Gumball | The Faith
 7:30 am | Amazing World of Gumball | The News
 7:45 am | Amazing World of Gumball | The Candidate
 8:00 am | Amazing World of Gumball | The Rival
 8:15 am | Amazing World of Gumball | The Anybody
 8:30 am | Teen Titans Go! | I'm the Sauce
 8:45 am | Teen Titans Go! | BBB Day!
 9:00 am | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 9:15 am | Teen Titans Go! | Squash & Stretch
 9:30 am | Amazing World of Gumball | The Friend; The Saint
10:00 am | Amazing World of Gumball | The Society; The Spoiler
10:30 am | Craig of the Creek | Dog Decider
10:45 am | Craig of the Creek | The Final Book
11:00 am | Teen Titans Go! | Accept the Next Proposition You Hear
11:15 am | Teen Titans Go! | Garage Sale
11:30 am | Teen Titans Go! | The Fourth Wall
11:45 am | Teen Titans Go! | Secret Garden
12:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
12:30 pm | Amazing World of Gumball | The Guy
12:45 pm | Amazing World of Gumball | The Puppets
 1:00 pm | Amazing World of Gumball | The Boredom
 1:15 pm | Amazing World of Gumball | The Line
 1:30 pm | Unikitty | Fire & Nice
 1:45 pm | Unikitty | Kitty Court
 2:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 2:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 3:00 pm | Teen Titans Go! | Video Game References; Cool School
 3:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 4:00 pm | LEGO DC Comics Super Heroes: The Flash | 
 6:00 pm | Teen Titans Go! | Bro-Pocalypse
 6:15 pm | Teen Titans Go! | Top of the Titans: Beast Boy & Cyborg Songs
 6:30 pm | Teen Titans Go! | BBCYFSHIPBDAY
 6:45 pm | Teen Titans Go! | Career Day
 7:00 pm | Teen Titans Go! | Waffles
 7:15 pm | Teen Titans Go! | Pie Bros
 7:30 pm | Teen Titans Go! | Burger vs. Burrito
 7:45 pm | Teen Titans Go! | Hey Pizza!