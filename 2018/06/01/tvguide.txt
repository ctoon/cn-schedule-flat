# 2018-06-01
 6:00 am | Steven Universe | Garnet's Universe
 6:15 am | Justice League Action | Keeping Up With The Kryptonians
 6:30 am | Teen Titans | Deception
 7:00 am | Ben 10 | Mayhem in Mascot
 7:15 am | Amazing World of Gumball | The Faith
 7:30 am | Amazing World of Gumball | The News; The Candidate
 8:00 am | Amazing World of Gumball | The Rival; The Anybody
 8:30 am | Teen Titans Go! | I'm the Sauce; Bbb Day!
 9:00 am | Teen Titans Go! | Hey You, Don't Neglect Me in Your Memory; Squash & Stretch
 9:30 am | Amazing World of Gumball | The Friend; The Saint
10:00 am | Amazing World of Gumball | The Spoiler; The Society
10:30 am | Craig of the Creek | Dog Decider; The Final Book
11:00 am | Teen Titans Go! | Garage Sale; Accept the Next Proposition You Hear
11:30 am | Teen Titans Go! | The Fourth Wall; Secret Garden
12:00 pm | Teen Titans Go! | Hose Water; Yearbook Madness
12:30 pm | Amazing World of Gumball | The Guy; The Puppets
 1:00 pm | Amazing World of Gumball | The Boredom; The Line
 1:30 pm | Unikitty | Fire & Nice; Kitty Court
 2:00 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 2:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 3:00 pm | Teen Titans Go! | Video Game References; Cool School
 3:30 pm | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
 4:00 pm | LEGO DC Comics Super Heroes: The Flash | 
 6:00 pm | Teen Titans Go! | Bro-Pocalypse ; Top of the Titans: Beast Boy & Cyborg Songs
 6:30 pm | Teen Titans Go! | BBCYFSHIPBDAY
 7:00 pm | Teen Titans Go! | Pie Bros Waffles
 7:30 pm | Teen Titans Go! | Hey Pizza! Burger Vs. Burrito