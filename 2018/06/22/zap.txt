# 2018-06-22
 6:00 am | Steven Universe | The Return
 6:15 am | Ben 10 | The Charm Offensive
 6:30 am | Teen Titans Go! | Power Moves; Staring at the Future
 7:00 am | Amazing World of Gumball | The Founder
 7:15 am | Craig of the Creek | Dog Decider
 7:30 am | Craig of the Creek | Wildernessa
 7:45 am | Craig of the Creek | Itch to Explore
 8:00 am | Amazing World of Gumball | The Schooling
 8:15 am | Teen Titans Go! | Starfire the Terrible
 8:30 am | Teen Titans Go! | No Power; Sidekick
 9:00 am | Amazing World of Gumball | The Intelligence
 9:15 am | Amazing World of Gumball | The Stories
 9:30 am | Amazing World of Gumball | The Girlfriend
 9:45 am | Amazing World of Gumball | The Guy
10:00 am | Amazing World of Gumball | The Brain
10:15 am | Teen Titans Go! | Legendary Sandwich
10:30 am | Teen Titans Go! | Driver's Ed; Dog Hand
11:00 am | Amazing World of Gumball | The Parents
11:15 am | Teen Titans Go! | Double Trouble
11:30 am | Teen Titans Go! | Dude Relax; Laundry Day
12:00 pm | Amazing World of Gumball | The Founder
12:15 pm | Amazing World of Gumball | The Pressure
12:30 pm | Amazing World of Gumball | The Weirdo
12:45 pm | Amazing World of Gumball | The Painting
 1:00 pm | Amazing World of Gumball | The Schooling
 1:15 pm | Amazing World of Gumball | The Internet
 1:30 pm | Amazing World of Gumball | The World; The Finale
 2:00 pm | Amazing World of Gumball | The Intelligence
 2:15 pm | Teen Titans Go! | Friendship
 2:30 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 3:00 pm | Amazing World of Gumball | The Brain
 3:15 pm | Craig of the Creek | Escape From Family Dinner
 3:30 pm | Craig of the Creek | Sunday Clothes
 3:45 pm | Craig of the Creek | Jessica Goes to the Creek
 4:00 pm | Amazing World of Gumball | The Parents
 4:15 pm | Teen Titans Go! | Vegetables
 4:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp; Oil Drums
 5:00 pm | Amazing World of Gumball | The Founder
 5:15 pm | Amazing World of Gumball | The Triangle
 5:30 pm | Amazing World of Gumball | The Oracle; The Safety
 6:00 pm | Amazing World of Gumball | The Schooling
 6:15 pm | Teen Titans Go! | Bro-Pocalypse
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Amazing World of Gumball | The Intelligence
 7:15 pm | We Bare Bears | Hurricane Hal
 7:30 pm | We Bare Bears | The Park
 7:45 pm | We Bare Bears | Vacation