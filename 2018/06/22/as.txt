# 2018-06-22
 8:00 pm | King Of the Hill | Uh-Oh Canada
 8:30 pm | Cleveland Show  | Das Shrimp Boot
 9:00 pm | Cleveland Show  | March Dadness
 9:30 pm | American Dad | License to Till
10:00 pm | American Dad | Jenny Fromdabloc
10:30 pm | Bob's Burgers | Tina-Rannosaurus Wrecks
11:00 pm | Family Guy | Sibling Rivalry
11:30 pm | Family Guy | Deep Throats
12:00 am | Rick and Morty | Pilot
12:30 am | Robot Chicken | In a DVD Factory
12:45 am | Robot Chicken | Tell My Mom
 1:00 am | Joe Pera Talks With You | Joe Pera Answers Your Questions About Cold Weather Sports
 1:30 am | Bob's Burgers | Tina-Rannosaurus Wrecks
 2:00 am | Family Guy | Sibling Rivalry
 2:30 am | Family Guy | Deep Throats
 3:00 am | American Dad | License to Till
 3:30 am | American Dad | Jenny Fromdabloc
 4:00 am | As Seen on Adult Swim | Williams Stream 254
 4:15 am | Digikiss | Williams Stream 255
 4:30 am | Rick and Morty | Pilot
 5:00 am | Cleveland Show  | March Dadness
 5:30 am | King Of the Hill | Uh-Oh Canada