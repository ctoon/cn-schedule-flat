# 2018-06-18
 6:00 am | Steven Universe | Marble Madness
 6:15 am | Ben 10 | Can I Keep It?
 6:30 am | Teen Titans Go! | Double Trouble/The Date
 7:00 am | Amazing World of Gumball | The Brain
 7:15 am | Craig of the Creek | The Brood
 7:30 am | Craig of the Creek | Curse/Too Many Treasures
 8:00 am | Amazing World of Gumball | The Parents
 8:15 am | Teen Titans Go! | Dude Relax!
 8:30 am | Teen Titans Go! | Ghostboy/La Larva de Amor
 9:00 am | Amazing World of Gumball | Founder/The Misunderstandings
 9:30 am | Amazing World of Gumball | Wicked/The Roots
10:00 am | Amazing World of Gumball | The Schooling
10:15 am | Teen Titans Go! | BBSFBDAY!
10:30 am | Teen Titans Go! | Inner Beauty of a Cactus/Movie Night
11:00 am | Amazing World of Gumball | The Intelligence
11:15 am | Teen Titans Go! | Permanent Record
11:30 am | Teen Titans Go! | Titan Saving Time/Master Detective
12:00 pm | Amazing World of Gumball | Brain/The Faith
12:30 pm | Amazing World of Gumball | Ex/The Candidate
 1:00 pm | Amazing World of Gumball | Parents/The Pony
 1:30 pm | Amazing World of Gumball | The Dream/The Sidekick
 2:00 pm | Amazing World of Gumball | The Founder
 2:15 pm | Teen Titans Go! | Dreams
 2:30 pm | Teen Titans Go! | Real Magic/Puppets, Whaaaaat?
 3:00 pm | Amazing World of Gumball | The Schooling
 3:15 pm | Craig of the Creek | The Future is Cardboard
 3:30 pm | Craig of the Creek | Wildernessa/Lost in the Sewer
 4:00 pm | Amazing World of Gumball | The Intelligence
 4:15 pm | Teen Titans Go! | Grandma Voice
 4:30 pm | Teen Titans Go! | Pirates/I See You
 5:00 pm | Amazing World of Gumball | Brain/The Friend
 5:30 pm | Amazing World of Gumball | The Mirror/The Burden
 6:00 pm | Amazing World of Gumball | The Parents
 6:15 pm | Teen Titans Go! | Demon Prom
 6:30 pm | Teen Titans Go! | Streak Part 1/The Streak Part 2
 7:00 pm | Amazing World of Gumball | The Founder
 7:15 pm | We Bare Bears | Bunnies
 7:30 pm | We Bare Bears | Panda 2/Private Lake