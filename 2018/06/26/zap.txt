# 2018-06-26
 6:00 am | Steven Universe | Full Disclosure
 6:15 am | Ben 10 | Ye Olde Laser Duel
 6:30 am | Amazing World of Gumball | The Pressure; The Painting
 7:00 am | Teen Titans Go! | TV Knight 3
 7:15 am | Amazing World of Gumball | The Brain
 7:30 am | Amazing World of Gumball | The Signature
 7:45 am | Amazing World of Gumball | The Gift
 8:00 am | Teen Titans Go! | The Scoop
 8:15 am | Teen Titans Go! | The Croissant
 8:30 am | Teen Titans Go! | The Spice Game
 8:45 am | Teen Titans Go! | I'm the Sauce
 9:00 am | Teen Titans Go! | Chicken in the Cradle
 9:15 am | Teen Titans Go! | Double Trouble
 9:30 am | Teen Titans Go! | Dude Relax; Laundry Day
10:00 am | Teen Titans Go! | Mo' Money Mo' Problems
10:15 am | Amazing World of Gumball | The Parents
10:30 am | Amazing World of Gumball | The Name; The Extras
11:00 am | Teen Titans Go! | TV Knight 3
11:15 am | Amazing World of Gumball | The Schooling
11:30 am | Amazing World of Gumball | The Boredom
11:45 am | Amazing World of Gumball | The Vision
12:00 pm | Teen Titans Go! | The Scoop
12:15 pm | Teen Titans Go! | Pirates
12:30 pm | Teen Titans Go! | Brian; Nature
 1:00 pm | Teen Titans Go! | Chicken in the Cradle
 1:15 pm | Unikitty | Chair
 1:30 pm | Unikitty | R & Arr
 1:45 pm | Unikitty | Little Prince Puppycorn
 2:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 2:15 pm | We Bare Bears | Fashion Bears
 2:30 pm | We Bare Bears | The Island
 2:45 pm | We Bare Bears | Baby Bears on a Plane
 3:00 pm | Teen Titans Go! | TV Knight 3
 3:15 pm | OK K.O.! Let's Be Heroes | Red Action to the Future
 3:30 pm | OK K.O.! Let's Be Heroes | Let's Watch the Boxmore Show
 3:45 pm | OK K.O.! Let's Be Heroes | Seasons Change
 4:00 pm | Teen Titans Go! | The Scoop
 4:15 pm | Craig of the Creek | The Final Book
 4:30 pm | Craig of the Creek | Too Many Treasures
 4:45 pm | Craig of the Creek | Wildernessa
 5:00 pm | Teen Titans Go! | Chicken in the Cradle
 5:15 pm | Amazing World of Gumball | The Intelligence
 5:30 pm | Amazing World of Gumball | The Loophole
 5:45 pm | Amazing World of Gumball | The Fuss
 6:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 6:15 pm | Teen Titans Go! | TV Knight
 6:30 pm | Teen Titans Go! | BBSFBDAY
 6:45 pm | Teen Titans Go! | Inner Beauty of a Cactus
 7:00 pm | Teen Titans Go! | TV Knight 3
 7:15 pm | We Bare Bears | Jean Jacket
 7:30 pm | We Bare Bears | Food Truck
 7:45 pm | We Bare Bears | Primal