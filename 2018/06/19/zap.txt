# 2018-06-19
 6:00 am | Steven Universe | Rose's Scabbard
 6:15 am | Ben 10 | Chicken Nuggets of Wisdom
 6:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
 7:00 am | Amazing World of Gumball | The Schooling
 7:15 am | Craig of the Creek | The Future Is Cardboard
 7:30 am | Craig of the Creek | Monster in the Garden
 7:45 am | Craig of the Creek | The Final Book
 8:00 am | Amazing World of Gumball | The Intelligence
 8:15 am | Teen Titans Go! | Girls' Night Out
 8:30 am | Teen Titans Go! | Super Robin; Tower Power
 9:00 am | Amazing World of Gumball | The Brain
 9:15 am | Amazing World of Gumball | The Blame
 9:30 am | Amazing World of Gumball | The Traitor
 9:45 am | Amazing World of Gumball | The Detective
10:00 am | Amazing World of Gumball | The Parents
10:15 am | Teen Titans Go! | Hand Zombie
10:30 am | Teen Titans Go! | Employee of the Month Redux
10:45 am | Teen Titans Go! | The Avogodo
11:00 am | Amazing World of Gumball | The Founder
11:15 am | Teen Titans Go! | Orangins
11:30 am | Teen Titans Go! | Jinxed
11:45 am | Teen Titans Go! | Brain Percentages
12:00 pm | Amazing World of Gumball | The Schooling
12:15 pm | Amazing World of Gumball | The Anybody
12:30 pm | Amazing World of Gumball | The Sorcerer
12:45 pm | Amazing World of Gumball | The Pact
 1:00 pm | Amazing World of Gumball | The Intelligence
 1:15 pm | Amazing World of Gumball | The Hero
 1:30 pm | Amazing World of Gumball | The Tag; The Lesson
 2:00 pm | Amazing World of Gumball | The Brain
 2:15 pm | Teen Titans Go! | Mr. Butt
 2:30 pm | Teen Titans Go! | Salty Codgers; Knowledge
 3:00 pm | Amazing World of Gumball | The Parents
 3:15 pm | Craig of the Creek | Bring Out Your Beast
 3:30 pm | Craig of the Creek | Too Many Treasures
 3:45 pm | Craig of the Creek | The Brood
 4:00 pm | Amazing World of Gumball | The Founder
 4:15 pm | Teen Titans Go! | Man Person
 4:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 5:00 pm | Amazing World of Gumball | The Schooling
 5:15 pm | Amazing World of Gumball | The Society
 5:30 pm | Amazing World of Gumball | The Bros; The Man
 6:00 pm | Amazing World of Gumball | The Intelligence
 6:15 pm | Teen Titans Go! | BBCYFSHIPBDAY
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Amazing World of Gumball | The Brain
 7:15 pm | We Bare Bears | Panda's Art
 7:30 pm | We Bare Bears | Icy Nights II
 7:45 pm | We Bare Bears | The Kitty