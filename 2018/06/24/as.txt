# 2018-06-24
 8:00 pm | King Of the Hill | The Honeymooners
 8:30 pm | King Of the Hill | Bill Gathers Moss
 9:00 pm | Bob's Burgers | Mom, Lies, and Videotape
 9:30 pm | American Dad | Klaustastrophe.tv
10:00 pm | Family Guy | Carter and Tricia
10:30 pm | Family Guy | How the Griffin Stole Christmas
11:00 pm | Rick and Morty | Vindicators 3: The Return of Worldender
11:30 pm | Robot Chicken | Jew #1 Opens a Treasure Chest
11:45 pm | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
12:00 am | Mr. Neighbor's House 2 | Mr. Neighbor's House 2
12:30 am | Joe Pera Talks With You | Joe Pera Shows You Iron
12:45 am | Joe Pera Talks With You | Joe Pera Takes You to Breakfast
 1:00 am | Ballmastrz: 9009 | A Shooting Star Named Gaz Digzy Falls Fast and Hard
 1:15 am | Ballmastrz: 9009 | With the Burning Spirit of Teamwork in Her Heart
 1:30 am | Bob's Burgers | Mom, Lies, and Videotape
 2:00 am | American Dad | Klaustastrophe.tv
 2:30 am | Family Guy | Carter and Tricia
 3:00 am | Family Guy | How the Griffin Stole Christmas
 3:30 am | Rick and Morty | Vindicators 3: The Return of Worldender
 4:00 am | Robot Chicken | Jew #1 Opens a Treasure Chest
 4:15 am | Robot Chicken | 3 2 1 2 3 3 3, 2 2 2, 3...6 6?
 4:30 am | Mr. Neighbor's House 2 | Mr. Neighbor's House 2
 5:00 am | King Of the Hill | The Honeymooners
 5:30 am | King Of the Hill | Bill Gathers Moss