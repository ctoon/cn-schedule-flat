# 2018-06-12
 6:00 am | Steven Universe | On the Run
 6:15 am | Ben 10 | Ye Olde Laser Duel
 6:30 am | Teen Titans Go! | Employee of the Month Redux
 6:45 am | Teen Titans Go! | The Avogodo
 7:00 am | Ben 10 | Ben Again & Again
 7:15 am | Unikitty | Birthday Blowout
 7:30 am | Unikitty | Lab Cat
 7:45 am | Unikitty | R & Arr
 8:00 am | Ben 10 | King Koil
 8:15 am | Teen Titans Go! | Orangins
 8:30 am | Teen Titans Go! | Jinxed
 8:45 am | Teen Titans Go! | Brain Percentages
 9:00 am | Ben 10 | The Charm Offensive
 9:15 am | Amazing World of Gumball | The Parking
 9:30 am | Amazing World of Gumball | The Routine
 9:45 am | Amazing World of Gumball | The Nest
10:00 am | Ben 10 | Double Hex
10:15 am | Teen Titans Go! | Garage Sale
10:30 am | Teen Titans Go! | Secret Garden
10:45 am | Teen Titans Go! | The Cruel Giggling Ghoul
11:00 am | Ben 10 | Ye Olde Laser Duel
11:15 am | Teen Titans Go! | Pyramid Scheme
11:30 am | Teen Titans Go! | Bottle Episode
11:45 am | Teen Titans Go! | Finally a Lesson
12:00 pm | Ben 10 | Ben Again & Again
12:15 pm | Amazing World of Gumball | The Box
12:30 pm | Amazing World of Gumball | The Console
12:45 pm | Amazing World of Gumball | The One
 1:00 pm | Ben 10 | King Koil
 1:15 pm | Teen Titans Go! | Meatball Party
 1:30 pm | Teen Titans Go! | Terra-ized; Artful Dodgers
 2:00 pm | Ben 10 | The Charm Offensive
 2:15 pm | Teen Titans Go! | Burger vs. Burrito
 2:30 pm | Teen Titans Go! | Colors of Raven; The Left Leg
 3:00 pm | Ben 10 | Double Hex
 3:15 pm | OK K.O.! Let's Be Heroes | Presenting Joe Cuppa
 3:30 pm | OK K.O.! Let's Be Heroes | We've Got Pests
 3:45 pm | OK K.O.! Let's Be Heroes | The Power Is Yours!
 4:00 pm | Ben 10 | Ye Olde Laser Duel
 4:15 pm | Amazing World of Gumball | The Recipe
 4:30 pm | Amazing World of Gumball | The Name; The Extras
 5:00 pm | Ben 10 | Ben Again & Again
 5:15 pm | Craig of the Creek | The Curse
 5:30 pm | Craig of the Creek | The Final Book
 5:45 pm | Craig of the Creek | Lost in the Sewer
 6:00 pm | Ben 10 | King Koil
 6:15 pm | Teen Titans Go! | Ones and Zeroes
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Ben 10 | The Charm Offensive
 7:15 pm | We Bare Bears | Icy Nights
 7:30 pm | We Bare Bears | Captain Craboo