# 2018-06-15
 6:00 am | Steven Universe | Maximum Capacity
 6:15 am | Ben 10 | King Koil
 6:30 am | Teen Titans Go! | Beast Girl/Bro-Pocalypse
 7:00 am | Ben 10 | The Charm Offensive
 7:15 am | Unikitty | R & Arr
 7:30 am | Unikitty | Chair/Dinner Apart-y
 8:00 am | Ben 10 | Double Hex
 8:15 am | Teen Titans Go! | Legendary Sandwich
 8:30 am | Teen Titans Go! | Driver's Ed/Dog Hand
 9:00 am | Ben 10 | Ye Olde Laser Duel
 9:15 am | Amazing World of Gumball | The Apprentice
 9:30 am | Amazing World of Gumball | Hug/The Night
10:00 am | Ben 10 | Ben Again and Again
10:15 am | Teen Titans Go! | Riding the Dragon
10:30 am | Teen Titans Go! | Overbite/Shrimps and Prime Rib
11:00 am | Ben 10 | King Koil
11:15 am | Teen Titans Go! | Booby Trap House
11:30 am | Teen Titans Go! | Fish Water/TV Knight
12:00 pm | Ben 10 | The Charm Offensive
12:15 pm | Amazing World of Gumball | The Grades
12:30 pm | Amazing World of Gumball | Diet/The Cage
 1:00 pm | Ben 10 | Double Hex
 1:15 pm | Teen Titans Go! | Little Buddies
 1:30 pm | Teen Titans Go! | Brain Food/In and Out
 2:00 pm | Ben 10 | Ye Olde Laser Duel
 2:15 pm | Teen Titans Go! | Missing
 2:30 pm | Teen Titans Go! | Uncle Jokes/Mas y Menos
 3:00 pm | Ben 10 | Ben Again and Again
 3:15 pm | OK K.O.! Let's Be Heroes | Everybody Likes Rad?
 3:30 pm | OK K.O.! Let's Be Heroes | You Have To Care/Parents Day
 4:00 pm | SPECIAL | Lego Justice League: Gotham City Breakout/Booty Scooty
 6:00 pm | SPECIAL | Teen Titans Go!: Island Adventures
 7:00 pm | We Bare Bears | Captain Craboo
 7:30 pm | We Bare Bears | Road Trip/Coffee Cave