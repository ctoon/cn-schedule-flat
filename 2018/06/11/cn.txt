# 2018-06-11
 6:00 am | Steven Universe | Future Vision
 6:15 am | Ben 10 | All Koiled Up
 6:30 am | Teen Titans Go! | Movie Night/Permanent Record
 7:00 am | Ben 10 | King Koil
 7:15 am | Unikitty | Pet Pet
 7:30 am | Unikitty | Kitty Court/Dinner Apart-y
 8:00 am | Ben 10 | The Charm Offensive
 8:15 am | Teen Titans Go! | Titan Saving Time
 8:30 am | Teen Titans Go! | Master Detective/Hand Zombie
 9:00 am | Ben 10 | Double Hex
 9:15 am | Amazing World of Gumball | The Sale
 9:30 am | Amazing World of Gumball | Gift/The Awkwardness
10:00 am | Ben 10 | Ye Olde Laser Duel
10:15 am | Teen Titans Go! | 40%, 40%, 20%
10:30 am | Teen Titans Go! | Grube's Fairytales/A Farce
11:00 am | Ben 10 | Ben Again and Again
11:15 am | Teen Titans Go! | Animals, It's Just a Word!
11:30 am | Teen Titans Go! | BBBDay!/Squash & Stretch
12:00 pm | Ben 10 | King Koil
12:15 pm | Amazing World of Gumball | The Vase
12:30 pm | Amazing World of Gumball | Matchmaker/The Vegging
 1:00 pm | Ben 10 | The Charm Offensive
 1:15 pm | Teen Titans Go! | Hey Pizza!
 1:30 pm | Teen Titans Go! | Girls' Night Out/You're Fired
 2:00 pm | Ben 10 | Double Hex
 2:15 pm | Teen Titans Go! | Super Robin
 2:30 pm | Teen Titans Go! | Parasite/Starliar
 3:00 pm | Ben 10 | Ye Olde Laser Duel
 3:15 pm | OK K.O.! Let's Be Heroes | You Are Rad
 3:30 pm | OK K.O.! Let's Be Heroes | Just Be a Pebble/KO's Video Channel
 4:00 pm | Ben 10 | Ben Again and Again
 4:15 pm | Amazing World of Gumball | The Kids
 4:30 pm | Amazing World of Gumball | The Coach/The Joy
 5:00 pm | Ben 10 | King Koil
 5:15 pm | Craig of the Creek | Escape from Family Dinner
 5:30 pm | Craig of the Creek | Monster in the Garden/Dog Decider
 6:00 pm | Ben 10 | The Charm Offensive
 6:15 pm | Teen Titans Go! | Lication
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | Ben 10 | Double Hex
 7:15 pm | We Bare Bears | The Island
 7:30 pm | We Bare Bears | Bear Flu/Baby Bears on a Plane