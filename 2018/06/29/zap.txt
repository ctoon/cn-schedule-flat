# 2018-06-29
 6:00 am | Steven Universe | Say Uncle
 6:15 am | Ben 10 | Battle at Biggie Box
 6:30 am | Amazing World of Gumball | The Mystery; The Prank
 7:00 am | Teen Titans Go! | Mo' Money Mo' Problems
 7:15 am | Amazing World of Gumball | The Intelligence
 7:30 am | Amazing World of Gumball | The Upgrade
 7:45 am | Amazing World of Gumball | The Comic
 8:00 am | Teen Titans Go! | TV Knight 3
 8:15 am | Teen Titans Go! | Animals: It's Just a Word!
 8:30 am | Teen Titans Go! | BBB Day!
 8:45 am | Teen Titans Go! | Squash & Stretch
 9:00 am | Teen Titans Go! | The Scoop
 9:15 am | Teen Titans Go! | Parasite
 9:30 am | Teen Titans Go! | Meatball Party; Staff Meeting
10:00 am | Teen Titans Go! | Chicken in the Cradle
10:15 am | Amazing World of Gumball | The Schooling
10:30 am | Amazing World of Gumball | The Procrastinators; The Shell
11:00 am | Teen Titans Go! | Mo' Money Mo' Problems
11:15 am | Amazing World of Gumball | The Brain
11:30 am | Amazing World of Gumball | The Vase
11:45 am | Amazing World of Gumball | The Ollie
12:00 pm | Teen Titans Go! | TV Knight 3
12:15 pm | Teen Titans Go! | Friendship
12:30 pm | Teen Titans Go! | The Mask; Slumber Party
 1:00 pm | Teen Titans Go! | The Scoop
 1:15 pm | Unikitty | Tasty Heist
 1:30 pm | Unikitty | Film Fest
 1:45 pm | Unikitty | Wishing Well
 2:00 pm | Teen Titans Go! | Chicken in the Cradle
 2:15 pm | We Bare Bears | The Fair
 2:30 pm | We Bare Bears | Grizzly the Movie
 2:45 pm | We Bare Bears | Road Trip
 3:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 3:15 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 3:30 pm | OK K.O.! Let's Be Heroes | Plaza Film Festival
 3:45 pm | OK K.O.! Let's Be Heroes | The Perfect Meal
 4:00 pm | Teen Titans Go! | TV Knight 3
 4:15 pm | Craig of the Creek | Lost in the Sewer
 4:30 pm | Craig of the Creek | The Future Is Cardboard
 4:45 pm | Craig of the Creek | The Brood
 5:00 pm | Teen Titans Go! | The Scoop
 5:15 pm | Amazing World of Gumball | The Founder
 5:30 pm | Amazing World of Gumball | The Copycats
 5:45 pm | Amazing World of Gumball | The Catfish
 6:00 pm | Teen Titans Go! | Chicken in the Cradle
 6:15 pm | Teen Titans Go! | Titan Saving Time
 6:30 pm | Teen Titans Go! | Master Detective
 6:45 pm | Teen Titans Go! | The Avogodo
 7:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 7:15 pm | We Bare Bears | Panda's Sneeze
 7:30 pm | We Bare Bears | Hibernation
 7:45 pm | We Bare Bears | Occupy Bears