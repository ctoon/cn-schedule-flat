# 2018-06-29
 6:00 am | Steven Universe | Say Uncle
 6:15 am | Ben 10 | Battle at Biggie Box
 6:30 am | Amazing World of Gumball | The Mystery/The Prank
 7:00 am | Teen Titans Go! | Mo' Money Mo' Problems
 7:15 am | Amazing World of Gumball | The Intelligence
 7:30 am | Amazing World of Gumball | Upgrade/The Comic
 8:00 am | Teen Titans Go! | TV Knight 3/Animals, It's Just a Word!
 8:30 am | Teen Titans Go! | BBBDay!/Squash & Stretch
 9:00 am | Teen Titans Go! | Scoop/Parasite
 9:30 am | Teen Titans Go! | Meatball Party/Staff Meeting
10:00 am | Teen Titans Go! | Chicken in the Cradle
10:15 am | Amazing World of Gumball | The Schooling
10:30 am | Amazing World of Gumball | The Procrastinators/The Shell
11:00 am | Teen Titans Go! | Mo' Money Mo' Problems
11:15 am | Amazing World of Gumball | The Brain
11:30 am | Amazing World of Gumball | Vase/The Ollie
12:00 pm | Teen Titans Go! | TV Knight 3/Teen Titans Go! to the Movies: Lil Yachty Music Video/Friendship
12:30 pm | Teen Titans Go! | The Mask/Slumber Party
 1:00 pm | Teen Titans Go! | The Scoop
 1:15 pm | Unikitty | Tasty Heist
 1:30 pm | Unikitty | Film Fest/Wishing Well
 2:00 pm | Teen Titans Go! | Chicken in the Cradle
 2:15 pm | We Bare Bears | The Fair
 2:30 pm | We Bare Bears | Grizzly the Movie/Road Trip
 3:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 3:15 pm | OK K.O.! Let's Be Heroes | The So-Bad-Ical
 3:30 pm | OK K.O.! Let's Be Heroes | Plaza Film Festival/The Perfect Meal
 4:00 pm | Teen Titans Go! | TV Knight 3
 4:15 pm | Craig of the Creek | Lost in the Sewer
 4:30 pm | Craig of the Creek | Future is Cardboard/The Brood
 5:00 pm | Teen Titans Go! | The Scoop
 5:15 pm | Amazing World of Gumball | The Founder
 5:30 pm | Amazing World of Gumball | Copycats/The Catfish
 6:00 pm | Teen Titans Go! | Chicken in the Cradle/Teen Titans Go! to the Movies: Lil Yachty Music Video/Titan Saving Time
 6:30 pm | Teen Titans Go! | Master Detective/The Avogodo
 7:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 7:15 pm | We Bare Bears | Panda's Sneeze
 7:30 pm | We Bare Bears | Hibernation/Occupy Bears