# 2018-06-16
 6:00 am | Amazing World of Gumball | The Recipe/The Puppy
 6:30 am | Amazing World of Gumball | The Name/The Extras
 7:00 am | Amazing World of Gumball | Advice/The Sorcerer
 7:30 am | Amazing World of Gumball | Signal/The Menu
 8:00 am | Teen Titans Go! | Finally a Lesson/Lication
 8:30 am | Teen Titans Go! | Arms Race with Legs/Labor Day
 9:00 am | Teen Titans Go! | Obinray/Classic Titans
 9:30 am | Teen Titans Go! | Wally T/Ones and Zeros
10:00 am | Teen Titans Go! | Rad Dudes with Bad Tudes/Career Day
10:30 am | Teen Titans Go! | Operation Dude Rescue Part 1/Operation Dude Rescue Part 2
11:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star
11:30 am | Teen Titans Go! | History Lesson/TV Knight 2
12:00 pm | Ben 10 | King Koil/The Charm Offensive
12:30 pm | Ben 10 | Double Hex/Ye Olde Laser Duel
 1:00 pm | Ben 10 | Ben Again and Again
 1:15 pm | Amazing World of Gumball | The Parasite
 1:30 pm | Amazing World of Gumball | Love/The Line
 2:00 pm | Amazing World of Gumball | Awkwardness/The Singing
 2:30 pm | Amazing World of Gumball | Nest/The Best
 3:00 pm | Amazing World of Gumball | Points/The Worst
 3:30 pm | Amazing World of Gumball | Bus/The Deal
 4:00 pm | The Powerpuff Girls | Blundercup/Ragnarock and Roll
 4:30 pm | Amazing World of Gumball | Night/The Petals
 5:00 pm | Amazing World of Gumball | The Kids/The Fan
 5:30 pm | Amazing World of Gumball | The Coach/The Joy
 6:00 pm | SPECIAL | Lego Justice League: Gotham City Breakout/The Void