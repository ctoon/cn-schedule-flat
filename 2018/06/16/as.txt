# 2018-06-16
 8:00 pm | Cleveland Show  | Y Tu Junior Tambien
 8:30 pm | Family Guy | The Perfect Castaway
 9:00 pm | Rick and Morty | Vindicators 3: The Return of Worldender
 9:30 pm | Rick and Morty | The Whirly Dirly Conspiracy
10:00 pm | Family Guy | Jungle Love
10:30 pm | Dragon Ball Super | With New Hope in His Heart - Farewell, Trunks
11:00 pm | Dragon Ball Z Kai: The Final Chapters | And So, Ten Years Later. A Long-Awaited World Martial Arts Tournament!
11:30 pm | My Hero Academia | Rage, You Damn Nerd
12:00 am | FLCL: Progressive | Stone Skipping
12:30 am | JoJo's Bizarre Adventure: Stardust Crusaders | D'Arby the Player Part 1
 1:00 am | Hunter x Hunter | Combination x and x Evolution
 1:30 am | Black Clover | Wounded Beasts
 2:00 am | Naruto:Shippuden | Sakura's Feelings
 2:30 am | Space Dandy | The Gallant Space Gentleman, Baby
 3:00 am | Cowboy Bebop | Ganymede Elegy
 3:30 am | Lupin The 3rd Part 4 | 0.2% Chance of Survival
 4:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
 4:30 am | Rick and Morty | The Whirly Dirly Conspiracy
 5:00 am | King Of the Hill | Master of Puppets I'm Pulling Your Strings!
 5:30 am | King Of the Hill | Bwah My Nose