# 2018-06-09
 8:00 pm | Cleveland Show  | BFFs
 8:30 pm | Family Guy | When You Wish Upon a Weinstein
 9:00 pm | Rick and Morty | Rickmancing the Stone
 9:30 pm | Rick and Morty | Pickle Rick
10:00 pm | Family Guy | North by North Quahog
10:30 pm | Dragon Ball Super | Showdown! The Miraculous Power of Unyielding Warriors
11:00 pm | Dragon Ball Z Kai: The Final Chapters | Peace Returns! A Time of Rest for the Warriors!
11:30 pm | My Hero Academia | What I Can Do for Now
12:00 am | FLCL: Progressive | Freebie Honey
12:30 am | JoJo's Bizarre Adventure: Stardust Crusaders | The Guardian of Hell, Pet Shop Part 2
 1:00 am | Hunter x Hunter | Infiltration x and x Selection
 1:30 am | Black Clover | Adversity
 2:00 am | Naruto:Shippuden | Declaration of War
 2:30 am | Space Dandy | The Big Fish is Huge, Baby
 3:00 am | Cowboy Bebop | Jamming With Edward
 3:30 am | Lupin The 3rd Part 4 | The Fake Fantasista
 4:00 am | Rick and Morty | Rickmancing the Stone
 4:30 am | Rick and Morty | Pickle Rick
 5:00 am | King Of the Hill | Dia-Bill-Ic Shock
 5:30 am | King Of the Hill | Earthy Girls Are Easy