# 2018-06-10
 6:00 am | Clarence | A Pretty Great Day With a Girl
 6:15 am | Clarence | Lost in the Supermarket
 6:30 am | Craig of the Creek | The Final Book
 6:45 am | Craig of the Creek | The Future Is Cardboard
 7:00 am | Craig of the Creek | Too Many Treasures
 7:15 am | Craig of the Creek | The Brood
 7:30 am | Clarence | The Forgotten
 7:45 am | Clarence | Dollar Hunt
 8:00 am | Clarence | Man of the House
 8:15 am | Clarence | Zoo
 8:30 am | Craig of the Creek | Wildernessa
 8:45 am | Craig of the Creek | Escape From Family Dinner
 9:00 am | Clarence | A Sumoful Mind
 9:15 am | Clarence | The Trade
 9:30 am | Clarence | Rise 'n' Shine
 9:45 am | Clarence | Nature Clarence
10:00 am | Craig of the Creek | Sunday Clothes
10:15 am | Craig of the Creek | Monster in the Garden
10:30 am | Clarence | Animal Day
10:45 am | Clarence | The Dare Day
11:00 am | Clarence | Lizard Day Afternoon
11:15 am | Clarence | Patients
11:30 am | Craig of the Creek | Itch to Explore
11:45 am | Craig of the Creek | The Curse
12:00 pm | Clarence | The Tunnel
12:15 pm | Clarence | Big Trouble in Little Aberdale
12:30 pm | Clarence | Goose Chase
12:45 pm | Clarence | Breehn Ho!
 1:00 pm | Craig of the Creek | You're It
 1:15 pm | Craig of the Creek | Dog Decider
 1:30 pm | Clarence | Talent Show
 1:45 pm | Clarence | Missing Cat
 2:00 pm | Clarence | Chalmers Santiago
 2:15 pm | Clarence | Where the Wild Chads Are
 2:30 pm | Craig of the Creek | Jessica Goes to the Creek
 2:45 pm | Craig of the Creek | Bring Out Your Beast
 3:00 pm | Craig of the Creek | The Final Book
 3:15 pm | Craig of the Creek | Lost in the Sewer
 3:30 pm | Clarence | RC Car
 3:45 pm | Clarence | Just Wait in the Car
 4:00 pm | Clarence | In Dreams
 4:15 pm | Clarence | Ren Faire
 4:30 pm | Craig of the Creek | Too Many Treasures
 4:45 pm | Craig of the Creek | The Future Is Cardboard
 5:00 pm | Clarence | Escape From Beyond the Cosmic
 5:15 pm | Clarence | Time Crimes
 5:30 pm | Clarence | Lost Playground
 5:45 pm | Clarence | Attack the Block Party
 6:00 pm | Craig of the Creek | Wildernessa
 6:15 pm | Craig of the Creek | The Brood
 6:30 pm | Clarence | Field Trippin'
 6:45 pm | Clarence | Ice Cream Hunt
 7:00 pm | Craig of the Creek | Sunday Clothes
 7:15 pm | Craig of the Creek | Escape From Family Dinner
 7:30 pm | Craig of the Creek | Itch to Explore
 7:45 pm | Craig of the Creek | Monster in the Garden