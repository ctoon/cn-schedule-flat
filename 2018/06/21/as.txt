# 2018-06-21
 8:00 pm | King Of the Hill | Bad News Bill
 8:30 pm | American Dad | I Am the Walrus
 9:00 pm | Cleveland Show  | 'Til Deaf
 9:30 pm | American Dad | School Lies
10:00 pm | Bob's Burgers | An Indecent Thanksgiving Proposal
10:30 pm | Bob's Burgers | The Deepening
11:00 pm | Family Guy | Patriot Games
11:30 pm | Family Guy | I Take Thee, Quagmire
12:00 am | Rick and Morty | The Rickchurian Mortydate
12:30 am | Robot Chicken | They Took My Thumbs
12:45 am | Robot Chicken | I'm Trapped
 1:00 am | Joe Pera Talks With You | Joe Pera Lights Up the Night With You
 1:15 am | Joe Pera Talks With You | Joe Pera Talks To You About The Rat Wars of Alberta, Canada, 1950 - Present Day
 1:30 am | Bob's Burgers | An Indecent Thanksgiving Proposal
 2:00 am | Bob's Burgers | The Deepening
 2:30 am | Family Guy | Patriot Games
 3:00 am | Family Guy | I Take Thee, Quagmire
 3:30 am | American Dad | I Am the Walrus
 4:00 am | Infomercials | Dayworld
 4:15 am | Infomercials | Frank Pierre Presents: Pierre Resort & Casino
 4:30 am | Rick and Morty | The Rickchurian Mortydate
 5:00 am | Cleveland Show  | 'Til Deaf
 5:30 am | King Of the Hill | Bad News Bill