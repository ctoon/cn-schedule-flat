# 2018-06-30
 6:00 am | Amazing World of Gumball | The Kids/The Fan
 6:30 am | Amazing World of Gumball | The Coach/The Joy
 7:00 am | Amazing World of Gumball | The Recipe/The Puppy
 7:30 am | Amazing World of Gumball | The Name/The Extras
 8:00 am | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums
 8:30 am | Teen Titans Go! | Video Game References/Cool School
 9:00 am | Teen Titans Go! | Kicking a Ball and Pretending to Be Hurt/Head Fruit
 9:30 am | Teen Titans Go! | Operation Tin Man/Nean
10:00 am | SPECIAL | Teen Titans Go!: Island Adventures
11:00 am | Teen Titans Go! | Mo' Money Mo' Problems/TV Knight 3
11:30 am | Teen Titans Go! | Scoop/Chicken in the Cradle
12:00 pm | Ben 10 | All Koiled Up/King Koil
12:30 pm | Teen Titans Go! | Leg Day/Shrimps and Prime Rib
 1:00 pm | Amazing World of Gumball | The Gripes/The Vacation
 1:30 pm | Amazing World of Gumball | The Fraud/The Void
 2:00 pm | We Bare Bears | Our Stuff/Burrito
 2:30 pm | We Bare Bears | Viral Video/The Road
 3:00 pm | Adventure Time | Slumber Party Panic/Trouble in Lumpy Space
 3:30 pm | Adventure Time | Enchiridion/The Jiggler
 4:00 pm | Craig of the Creek | Itch to Explore/Dog Decider
 4:30 pm | Craig of the Creek | You're It/Bring Out Your Beast
 5:00 pm | Amazing World of Gumball | The Boss/The Move
 5:30 pm | Amazing World of Gumball | The Law/The Allergy
 6:00 pm | Amazing World of Gumball | Parents/The Founder
 6:30 pm | Amazing World of Gumball | Schooling/The Intelligence
 7:00 pm | Craig of the Creek | Jessica Goes to the Creek/Lost in the Sewer
 7:30 pm | Craig of the Creek | Final Book/The Future is Cardboard