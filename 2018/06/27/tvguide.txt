# 2018-06-27
 6:00 am | Steven Universe | Open Book
 6:15 am | Ben 10 | Ben Again and Again
 6:30 am | Amazing World of Gumball | The Responsible; The Dress
 7:00 am | Teen Titans Go! | The Scoop
 7:15 am | Amazing World of Gumball | The Founder
 7:30 am | Amazing World of Gumball | The Pest; The Check
 8:00 am | Teen Titans Go! | Chicken In The Cradle; Accept The Next Proposition You Hear
 8:30 am | Teen Titans Go! | The Fourth Wall; Hey You, Don't Forget About Me in Your Memory
 9:00 am | Teen Titans Go! | Mo' Money Mo' Problems; Ghostboy
 9:30 am | Teen Titans Go! | Hey Pizza!; Gorilla
10:00 am | Teen Titans Go! | Tv Knight 3
10:15 am | Amazing World of Gumball | The Brain
10:30 am | Amazing World of Gumball | The Fraud; The Void
11:00 am | Teen Titans Go! | The Scoop
11:15 am | Amazing World of Gumball | The Intelligence
11:30 am | Amazing World of Gumball | The Code; The Test
12:00 pm | Teen Titans Go! | Chicken In The Cradle; Salty Codgers
12:30 pm | Teen Titans Go! | Baby Hands; Love Monsters
 1:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 1:15 pm | Unikitty | Kickflip McPuppycorn
 1:30 pm | Unikitty | Dinner Apart-y; Stuck Together
 2:00 pm | Teen Titans Go! | Tv Knight 3
 2:15 pm | We Bare Bears | The Audition
 2:30 pm | We Bare Bears | Captain Craboo
 3:00 pm | Teen Titans Go! | The Scoop
 3:15 pm | OK K.O.! Let's Be Heroes | Tko's House
 3:30 pm | OK K.O.! Let's Be Heroes | You're in Control
 4:00 pm | Teen Titans Go! | Chicken In The Cradle
 4:15 pm | Craig of the Creek | Sunday Clothes
 4:30 pm | Craig of the Creek | Escape From Family Dinner; Monster in the Garden
 5:00 pm | Teen Titans Go! | Mo' Money Mo' Problems
 5:15 pm | Amazing World of Gumball | The Parents
 5:30 pm | Amazing World of Gumball | The Vase; The Ollie
 6:00 pm | Teen Titans Go! | Tv Knight 3; The Streak Part 1
 6:30 pm | Teen Titans Go! | The Streak: Part 2; Movie Night
 7:00 pm | Teen Titans Go! | The Scoop
 7:15 pm | We Bare Bears | Everyday Bears
 7:30 pm | We Bare Bears | The Road; Shush Ninjas