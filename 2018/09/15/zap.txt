# 2018-09-15
 6:00 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 6:30 am | Transformers: Cyberverse | Allspark
 6:45 am | Teen Titans Go! | Operation Tin Man
 7:00 am | Teen Titans Go! | Bottle Episode
 7:15 am | Teen Titans Go! | Titan Saving Time
 7:30 am | Teen Titans Go! | Finally a Lesson
 7:45 am | Teen Titans Go! | Master Detective
 8:00 am | Teen Titans Go! | Arms Race With Legs
 8:15 am | Teen Titans Go! | Hand Zombie
 8:30 am | Teen Titans Go! | Obinray
 8:45 am | Teen Titans Go! | Employee of the Month Redux
 9:00 am | Total DramaRama | Cuttin' Corners
 9:15 am | Total DramaRama | Sharing Is Caring
 9:30 am | Total DramaRama | The Date
 9:45 am | Total DramaRama | Aquarium for a Dream
10:00 am | Teen Titans Go! | Wally T
10:15 am | Teen Titans Go! | The Avogodo
10:30 am | Teen Titans Go! | Rad Dudes With Bad Tudes
10:45 am | Teen Titans Go! | Orangins
11:00 am | Teen Titans Go! | Jinxed
11:15 am | Teen Titans Go! | Operation Dude Rescue: Part 1
11:30 am | Teen Titans Go! | Operation Dude Rescue: Part 2
11:45 am | Teen Titans Go! | Brain Percentages
12:00 pm | Ben 10 | Drone On
12:15 pm | Teen Titans Go! | Nean
12:30 pm | Teen Titans Go! | History Lesson
12:45 pm | Teen Titans Go! | BL4Z3
 1:00 pm | Amazing World of Gumball | The Comic
 1:15 pm | Amazing World of Gumball | The Romantic
 1:30 pm | Amazing World of Gumball | The Uploads
 1:45 pm | Amazing World of Gumball | The Apprentice
 2:00 pm | Total DramaRama | Sharing Is Caring
 2:15 pm | Total DramaRama | Cuttin' Corners
 2:30 pm | Total DramaRama | Duck Duck Juice
 2:45 pm | Total DramaRama | Cluckwork Orange
 3:00 pm | Adventure Time | The Ultimate Adventure: Come Along With Me
 4:00 pm | Amazing World of Gumball | The One
 4:15 pm | Amazing World of Gumball | The Awkwardness
 4:30 pm | Amazing World of Gumball | The Vegging
 4:45 pm | Amazing World of Gumball | The Love
 5:00 pm | Total DramaRama | Cuttin' Corners
 5:15 pm | Total DramaRama | Sharing Is Caring
 5:30 pm | Total DramaRama | Free Chili
 5:45 pm | Total DramaRama | Venthalla
 6:00 pm | Amazing World of Gumball | The Rival
 6:15 pm | Amazing World of Gumball | The Advice
 6:30 pm | Amazing World of Gumball | The News
 6:45 pm | Amazing World of Gumball | The Girlfriend
 7:00 pm | Amazing World of Gumball | The Parasite
 7:15 pm | Amazing World of Gumball | The Sucker
 7:30 pm | Amazing World of Gumball | The Signal
 7:45 pm | Amazing World of Gumball | The Lady