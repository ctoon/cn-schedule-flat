# 2018-09-02
 6:00 am | Ben 10 | King Koil; The Charm Offensive
 6:30 am | Mega Man: Fully Charged | Tripping the Light Fantastic; Running Wild
 7:00 am | Amazing World of Gumball | The Spinoffs; The Vision
 7:30 am | Amazing World of Gumball | The Potion; The Boredom
 8:00 am | Total Dramarama | Free Chili; Venthalla
 8:30 am | Total Dramarama | Duck Duck Juice; Cluckwork Orange
 9:00 am | Teen Titans Go: Island Adventures | 
10:00 am | Teen Titans Go! | "Night Begins to Shine" Special
11:00 am | Teen Titans Go! | Labor Day
11:15 am | Teen Titans Go! | Kabooms: Part 1; ; Kabooms: Part 2
11:45 am | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
12:15 pm | Teen Titans Go! | BBRAE
12:45 pm | Teen Titans Go! | Tower Renovation
 1:00 pm | Total Dramarama | Venthalla; Duck Duck Juice
 1:30 pm | Total Dramarama | Cluckwork Orange; Free Chili
 2:00 pm | LEGO DC Comics Super Heroes: The Flash | 
 3:45 pm | Teen Titans Go! | Labor Day
 4:00 pm | The Powerpuff Girls | Small World: Abra-Disaster Part 1; Small World: Stone Cold Spider Part 2; Small World: Maze Daze Part 3; Small World: Heart
 5:00 pm | Total Dramarama | Duck Duck Juice; Cluckwork Orange
 5:30 pm | Total Dramarama | Free Chili; Venthalla
 6:00 pm | Amazing World of Gumball | The Intelligence; The Guy
 6:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 7:00 pm | Amazing World of Gumball | The Founder; The Schooling
 7:30 pm | Amazing World of Gumball | The Parents; The Stories