# 2018-09-02
 8:00 pm | Cleveland Show  | A Vas Deferens Between Men and Women
 8:30 pm | Cleveland Show  | 'Tis The Cleveland To Be Sorry
 9:00 pm | Bob's Burgers | Ex Machtina
 9:30 pm | American Dad | The Mural of the Story
10:00 pm | Family Guy | Christmas Guy
10:30 pm | Family Guy | Peter Problems
11:00 pm | Rick and Morty | M. Night Shaym-Aliens!
11:30 pm | Robot Chicken | Jew #1 Opens a Treasure Chest
11:45 pm | Robot Chicken | We Don't See Much of That in 1940s America
12:00 am | Venture Brothers  | The Inamorata Consequence
12:30 am | Mike Tyson Mysteries | Tyson of Arabia
12:45 am | Hot Streets | Nursery Rhyme Land
 1:00 am | Eric Andre Show | Warren G; Kelly Osbourne
 1:15 am | Mostly 4 Millennials | Entitlement
 1:30 am | Family Guy | Christmas Guy
 2:00 am | Family Guy | Peter Problems
 2:30 am | Rick and Morty | M. Night Shaym-Aliens!
 3:00 am | Robot Chicken | Jew #1 Opens a Treasure Chest
 3:15 am | Robot Chicken | We Don't See Much of That in 1940s America
 3:30 am | Venture Brothers  | The Inamorata Consequence
 4:00 am | Mike Tyson Mysteries | Tyson of Arabia
 4:15 am | Hot Streets | Nursery Rhyme Land
 4:30 am | Eric Andre Show | Warren G; Kelly Osbourne
 4:45 am | Mostly 4 Millennials | Entitlement
 5:00 am | American Dad | The Mural of the Story
 5:30 am | Bob's Burgers | Ex Machtina