# 2018-09-03
 6:00 am | Steven Universe | Now We're Only Falling Apart
 6:15 am | Adventure Time | From Bad to Worse
 6:30 am | Adventure Time | Jake vs. Me-Mow/Marceline's Closet
 7:00 am | Adventure Time | Five Short Graybles/Burning Low
 7:30 am | Adventure Time | King Worm/Who Would Win
 8:00 am | Adventure Time | Card Wars/Jake the Dad
 8:30 am | Adventure Time | Daddy-Daughter Card Wars/Reign of Gunters
 9:00 am | Adventure Time | Adventure Time with Fionna and Cake/Bad Little Boy
 9:30 am | Adventure Time | Prince Who Wanted Everything/BMO Lost
10:00 am | Adventure Time | James Baxter the Horse/Jakesuit
10:30 am | Adventure Time | Bad Jubies/Diamonds and Lemons
11:00 am | Adventure Time | Memories of Boom Boom Mountain/Food Chain
11:30 am | Adventure Time | Duke/The Other Tarts
12:00 pm | Adventure Time | Guardians of Sunshine/The Limit
12:30 pm | Adventure Time | Belly of the Beast/Conquest of Cuteness
 1:00 pm | Adventure Time | Hall of Egress/It Came From The Nightosphere
 1:30 pm | Adventure Time | Memory of a Memory/I Remember You
 2:00 pm | Adventure Time | Simon & Marcy/Blood Under the Skin
 2:30 pm | Adventure Time | Puhoy/Time Sandwich
 3:00 pm | Adventure Time | Princess Day/City of Thieves
 3:30 pm | Adventure Time | Princess Cookie/Jake the Brick
 4:00 pm | Adventure Time | Too Young/Thank You
 4:30 pm | Adventure Time | Mystery Train/What Was Missing
 5:00 pm | Adventure Time | Rainy Day Daydream/Trouble in Lumpy Space
 5:30 pm | Adventure Time | Slumber Party Panic/Enchiridion
 6:00 pm | Adventure Time | The Ultimate Adventure: Come Along with Me
 7:00 pm | Adventure Time | The Ultimate Adventure: Come Along with Me