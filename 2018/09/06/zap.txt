# 2018-09-06
 6:00 am | Steven Universe | Made of Honor
 6:15 am | Amazing World of Gumball | The Recipe
 6:30 am | Amazing World of Gumball | The Name; The Extras
 7:00 am | Amazing World of Gumball | The Best
 7:15 am | Amazing World of Gumball | The Worst
 7:30 am | Amazing World of Gumball | The Girlfriend
 7:45 am | Amazing World of Gumball | The Deal
 8:00 am | Teen Titans Go! | Leg Day
 8:15 am | Teen Titans Go! | Oh Yeah!
 8:30 am | Teen Titans Go! | The Dignity of Teeth
 8:45 am | Teen Titans Go! | Riding the Dragon
 9:00 am | Teen Titans Go! | Bro-Pocalypse
 9:15 am | Teen Titans Go! | Hand Zombie
 9:30 am | Teen Titans Go! | Employee of the Month Redux
 9:45 am | Teen Titans Go! | Mo' Money Mo' Problems
10:00 am | Amazing World of Gumball | The Bumpkin; The Flakers
10:30 am | Amazing World of Gumball | The Authority; The Virus
11:00 am | Amazing World of Gumball | The Boss; The Move
11:30 am | Amazing World of Gumball | The Law; The Allergy
12:00 pm | Total DramaRama | Venthalla
12:15 pm | Total DramaRama | Duck Duck Juice
12:30 pm | Total DramaRama | Cluckwork Orange
12:45 pm | Total DramaRama | Free Chili
 1:00 pm | Adventure Time | The Ultimate Adventure: Come Along With Me
 2:00 pm | Unikitty | Lab Cat
 2:15 pm | Unikitty | Beach Daze
 2:30 pm | Unikitty | The Zone
 2:45 pm | Unikitty | Big Pup, Little Problem
 3:00 pm | Ben 10 | The Clocktopus
 3:15 pm | OK K.O.! Let's Be Heroes | Plaza Prom
 3:30 pm | OK K.O.! Let's Be Heroes | Second First Date
 3:45 pm | OK K.O.! Let's Be Heroes | One Last Score
 4:00 pm | Amazing World of Gumball | The Vision
 4:15 pm | Amazing World of Gumball | The Menu
 4:30 pm | Amazing World of Gumball | The Choices
 4:45 pm | Amazing World of Gumball | The Uncle
 5:00 pm | Craig of the Creek | Big Pinchy
 5:15 pm | Craig of the Creek | Lost in the Sewer
 5:30 pm | Total DramaRama | Venthalla
 5:45 pm | Total DramaRama | Duck Duck Juice
 6:00 pm | Teen Titans Go! | The Streak, Part 1
 6:15 pm | Teen Titans Go! | The Streak, Part 2
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | We Bare Bears | Paperboyz
 7:15 pm | We Bare Bears | Teacher's Pet
 7:30 pm | Amazing World of Gumball | The Heist
 7:45 pm | Amazing World of Gumball | The Cage