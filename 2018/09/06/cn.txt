# 2018-09-06
 6:00 am | Steven Universe | Made of Honor
 6:15 am | Amazing World of Gumball | The Recipe
 6:30 am | Amazing World of Gumball | The Name/The Extras
 7:00 am | Amazing World of Gumball | Best/The Worst
 7:30 am | Amazing World of Gumball | Girlfriend/The Deal
 8:00 am | Teen Titans Go! | Leg Day/Oh Yeah!
 8:30 am | Teen Titans Go! | Dignity of Teeth/Riding the Dragon
 9:00 am | Teen Titans Go! | Bro-Pocalypse/Hand Zombie
 9:30 am | Teen Titans Go! | Employee of the Month Redux/Mo' Money Mo' Problems
10:00 am | Amazing World of Gumball | The Bumpkin/The Flakers
10:30 am | Amazing World of Gumball | The Authority/The Virus
11:00 am | Amazing World of Gumball | The Boss/The Move
11:30 am | Amazing World of Gumball | The Law/The Allergy
12:00 pm | Total DramaRama | Venthalla/Duck Duck Juice
12:30 pm | Total DramaRama | Cluckwork Orange/Free Chili
 1:00 pm | Adventure Time | The Ultimate Adventure: Come Along with Me
 2:00 pm | Unikitty | Lab Cat/Beach Daze
 2:30 pm | Unikitty | Zone/Big Pup, Little Problem
 3:00 pm | Ben 10 | The Clocktopus
 3:15 pm | OK K.O.! Let's Be Heroes | Plaza Prom
 3:30 pm | OK K.O.! Let's Be Heroes | Second First Date/One Last Score
 4:00 pm | Amazing World of Gumball | Vision/The Menu
 4:30 pm | Amazing World of Gumball | Choices/The Uncle
 5:00 pm | Craig of the Creek | Big Pinchy/Lost in the Sewer
 5:30 pm | Total DramaRama | Venthalla/Duck Duck Juice
 6:00 pm | Teen Titans Go! | Streak Part 1/The Streak Part 2
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | We Bare Bears | Paperboyz/Teacher's Pet
 7:30 pm | Amazing World of Gumball | Heist/The Cage