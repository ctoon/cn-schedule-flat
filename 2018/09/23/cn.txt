# 2018-09-23
 6:00 am | Ben 10 | Reststop Roustabout/That's the Stuff
 6:30 am | Mega Man: Fully Charged | Drill of the Hunt/Power Cycle
 7:00 am | Teen Titans Go! | Meatball Party/Staff Meeting
 7:30 am | Teen Titans Go! | Terra-ized/Artful Dodgers
 8:00 am | Total DramaRama | Ant We All Just Get Along/Cuttin' Corners
 8:30 am | Total DramaRama | Aquarium for a Dream/The Date
 9:00 am | Teen Titans Go! | Love Monsters/Baby Hands
 9:30 am | Teen Titans Go! | Sandwich Thief/Money Grandma
10:00 am | Teen Titans Go! | Friendship/Vegetables
10:30 am | Teen Titans Go! | The Mask/Slumber Party
11:00 am | Teen Titans Go! | Boys vs. Girls/Body Adventure
11:30 am | Teen Titans Go! | Road Trip/The Best Robin
12:00 pm | Teen Titans Go! | Fish Water/Booby Trap House
12:30 pm | Teen Titans Go! | Night Begins to Shine Special/Tower Renovation
 1:30 pm | Total DramaRama | Cluckwork Orange/Duck Duck Juice
 2:00 pm | Total DramaRama | Ant We All Just Get Along/Free Chili
 2:30 pm | Amazing World of Gumball | Stars/The Check
 3:00 pm | Amazing World of Gumball | Ex/The Gift
 3:30 pm | Amazing World of Gumball | Diet/The Sale
 4:00 pm | Amazing World of Gumball | Grades/The Pest
 4:30 pm | SPECIAL | Lego Justice League: Cosmic Clash
 6:15 pm | SPECIAL | Lego Justice League: Gotham City Breakout