# 2018-09-08
 8:00 pm | Cleveland Show  | Fist and the Furious
 8:30 pm | Family Guy | Bigfat
 9:00 pm | Family Guy | Total Recall
 9:30 pm | Rick and Morty | Auto Erotic Assimilation
10:00 pm | My Hero Academia | Strategy, Strategy, Strategy
10:30 pm | Dragon Ball Super | Even the Universes' Gods are Appalled?! The Lose-and-Perish "Tournament of Power"
11:00 pm | Attack on Titan | Old Story
11:30 pm | FLCL: Alternative | Flying Memory
12:00 am | Pop Team Epic | Ginza Hostess Detective
12:30 am | JoJo's Bizarre Adventure: Diamond Is Unbreakable | The Nijimura Brothers Part 1
 1:00 am | Hunter x Hunter | Confusion x and x Expectation
 1:30 am | Black Clover | The One with No Magic
 2:00 am | Naruto:Shippuden | Infiltrator
 2:30 am | One-Punch Man | The Modern Ninja
 3:00 am | Lupin The 3rd Part 4 | The End of Lupin III
 3:30 am | Cowboy Bebop | Boogie-Woogie Feng-Shui
 4:00 am | Boondocks  | The Trial of Robert Kelly
 4:30 am | Rick and Morty | Auto Erotic Assimilation
 5:00 am | Cleveland Show  | Who Done Did It?
 5:30 am | Cleveland Show  | Fist and the Furious