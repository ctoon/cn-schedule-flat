# 2018-09-16
 6:00 am | Ben 10 | Safari Sa'bad; The Nature of Things
 6:30 am | Mega Man: Fully Charged | I.C.E.; Trust Your Guts, Man!
 7:00 am | Teen Titans Go! | Super Robin; Tower Power
 7:30 am | Teen Titans Go! | Parasite; Starliar
 8:00 am | Total DramaRama | Sharing Is Caring; Cuttin' Corners
 8:30 am | Total DramaRama | Aquarium for a Dream; Cluckwork Orange
 9:00 am | Teen Titans Go! | Dreams; Grandma Voice
 9:30 am | Amazing World of Gumball | The Goons; The Secret
10:00 am | Teen Titans Go! | Mr. Butt; Man Person
10:30 am | Teen Titans Go! | Pirates; I See You
11:00 am | Teen Titans Go! | Brian; Nature
11:30 am | Teen Titans Go! | Salty Codgers; Knowledge
12:00 pm | Teen Titans Go! | Hot Salad Water; The Art of Ninjutsu
12:30 pm | Teen Titans Go! | Think About Your Future; Lication
 1:00 pm | Total DramaRama | Cuttin' Corners; Sharing Is Caring
 1:30 pm | Total DramaRama | The Date; Duck Duck Juice
 2:00 pm | Amazing World of Gumball | The Petals; The Traitor
 2:30 pm | Amazing World of Gumball | The Deal; The Wicked
 3:00 pm | Amazing World of Gumball | The Signature; The Others
 3:30 pm | Amazing World of Gumball | The Pest; The Check
 4:00 pm | Unikitty | Tragic Magic; Buggin' Out
 4:30 pm | Unikitty | Big Pup, Little Problem; License to Punch
 5:00 pm | Craig of the Creek | The Brood; You're It
 5:30 pm | Craig of the Creek | The Future Is Cardboard; Itch To Explore
 6:00 pm | Amazing World of Gumball | The Potion; The Spinoffs
 6:30 pm | Amazing World of Gumball | The Understanding; The Ad
 7:00 pm | Amazing World of Gumball | The Worst; The Hug
 7:30 pm | Amazing World of Gumball | The Best; The Apprentice