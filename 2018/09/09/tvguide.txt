# 2018-09-09
 6:00 am | Ben 10 | Ye Olde Laser Duel; Ben Again And Again
 6:30 am | Mega Man: Fully Charged | Unfriendly Competition; Opposites Attract
 7:00 am | Teen Titans Go! | Hey Pizza!; Gorilla
 7:30 am | Teen Titans Go! | Girl's Night Out; You're Fired
 8:00 am | Total DramaRama | Aquarium for a Dream; The Date
 8:30 am | Total DramaRama | Free Chili; Duck Duck Juice
 9:00 am | Teen Titans Go! | Legs; Breakfast Cheese
 9:30 am | Teen Titans Go! | Waffles; Opposites
10:00 am | Teen Titans Go! | Birds; Be Mine
10:30 am | Teen Titans Go! | Brain Food; In and Out; Chicken in the Cradle
11:00 am | Teen Titans Go! | Little Buddies; Missing
11:30 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
12:00 pm | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
12:30 pm | Teen Titans Go! | Pyramid Scheme; Permanent Record
 1:00 pm | Total DramaRama | The Date; Aquarium for a Dream
 1:30 pm | Total DramaRama | Venthalla; Duck Duck Juice
 2:00 pm | Amazing World of Gumball | The Cringe; The Points
 2:30 pm | Amazing World of Gumball | The Father; The Nest
 3:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 3:30 pm | Amazing World of Gumball | The Origins Part 2; The Traitor
 4:00 pm | Unikitty | Landlord Lord; Kickflip McPuppycorn
 4:30 pm | Unikitty | Dancer Danger; Chair
 5:00 pm | Craig of the Creek | Doorway to Helen; The Curse
 5:30 pm | Craig of the Creek | Ace of Squares; Monster in the Garden
 6:00 pm | Total DramaRama | Aquarium for a Dream; The Date
 6:30 pm | Total DramaRama | Cluckwork Orange; Free Chili
 7:00 pm | Adventure Time | The Ultimate Adventure; Come Along With Me