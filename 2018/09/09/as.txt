# 2018-09-09
 8:00 pm | Cleveland Show  | A Rodent Like This
 8:30 pm | Cleveland Show  | The Hangover Part Tubbs
 9:00 pm | Bob's Burgers | There's No Business Like Mr. Business Business
 9:30 pm | American Dad | (You Gotta) Strike for Your Right
10:00 pm | Family Guy | Grimm Job
10:30 pm | Family Guy | Brian's a Bad Father
11:00 pm | Rick and Morty | Meeseeks and Destroy
11:30 pm | Robot Chicken | He's Not Even Aiming at the Toilet
11:45 pm | Robot Chicken | Ext. Forest - Day
12:00 am | Venture Brothers  | The Bellicose Proxy
12:30 am | Mike Tyson Mysteries | Carol
12:45 am | Hot Streets | Operation: Large and in Charge
 1:00 am | DREAM CORP LLC | Episode One
 1:15 am | Mostly 4 Millennials | Responsibility
 1:30 am | Family Guy | Grimm Job
 2:00 am | Family Guy | Brian's a Bad Father
 2:30 am | Rick and Morty | Meeseeks and Destroy
 3:00 am | Robot Chicken | He's Not Even Aiming at the Toilet
 3:15 am | Robot Chicken | Ext. Forest - Day
 3:30 am | Venture Brothers  | The Bellicose Proxy
 4:00 am | Mike Tyson Mysteries | Carol
 4:15 am | Hot Streets | Operation: Large and in Charge
 4:30 am | DREAM CORP LLC | Episode One
 4:45 am | Mostly 4 Millennials | Responsibility
 5:00 am | American Dad | (You Gotta) Strike for Your Right
 5:30 am | Bob's Burgers | There's No Business Like Mr. Business Business