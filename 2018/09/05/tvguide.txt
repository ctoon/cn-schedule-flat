# 2018-09-05
 6:00 am | Steven Universe | The Question
 6:15 am | Amazing World of Gumball | The Kids
 6:30 am | Amazing World of Gumball | The Coach; The Joy
 7:00 am | Amazing World of Gumball | The Traitor; The Heist
 7:30 am | Amazing World of Gumball | The Origins; The Origins Part Two
 8:00 am | Teen Titans Go! | And The Award For Sound Design Goes To Rob; Some Of Their Parts
 8:30 am | Teen Titans Go! | Cat's Fancy; Snuggle Time
 9:00 am | Teen Titans Go! | Titan Saving Time; Master Detective
 9:30 am | Teen Titans Go! | Flashback
10:00 am | Amazing World of Gumball | The Words; The Apology
10:30 am | Amazing World of Gumball | The Watch; The Bet
11:00 am | Total Dramarama | Free Chili; Venthalla
11:30 am | Total Dramarama | Duck Duck Juice; Cluckwork Orange
12:00 pm | Teen Titans Go! | Mr. Butt; Man Person
12:30 pm | Teen Titans Go! | Pirates; I See You
 1:00 pm | Adventure Time | The Gut Grinder; Finn Meets His Hero
 1:30 pm | Adventure Time | Loyalty to the King; Blood Under the Skin
 2:00 pm | Unikitty | Kitty Court; Tasty Heist
 2:30 pm | Unikitty | Birthday Blowout; Brawl Bot
 3:00 pm | Ben 10 | Riding the Storm Out
 3:15 pm | OK K.O.! Let's Be Heroes | Face Your Fears
 3:30 pm | OK K.O.! Let's Be Heroes | You Have to Care; Everybody Likes Rad?
 4:00 pm | Total Dramarama | Free Chili; Venthalla
 4:30 pm | Total Dramarama | Duck Duck Juice; Cluckwork Orange
 5:00 pm | Craig of the Creek | Sunday Clothes; You're It
 5:30 pm | Craig of the Creek | Wildernessa; Itch To Explore
 6:00 pm | Teen Titans Go! | Fish Water; Demon Prom
 6:30 pm | Teen Titans Go! | BBCYFSHIPBDAY; BBSFBDAY!
 7:00 pm | We Bare Bears | Googs; I, Butler
 7:30 pm | Amazing World of Gumball | The Best; The Cringe