# 2018-09-04
 6:00 am | Steven Universe | What's Your Problem?
 6:15 am | Amazing World of Gumball | The Internet
 6:30 am | Amazing World of Gumball | The World; The Finale
 7:00 am | Amazing World of Gumball | The Menu; The Hug
 7:30 am | Amazing World of Gumball | The Wicked; The Uncle
 8:00 am | Teen Titans Go! | Operation Tin Man; Nean
 8:30 am | Teen Titans Go! | The Hive Five; More of the Same
 9:00 am | Teen Titans Go! | BBRAE
 9:30 am | Teen Titans Go! | Permanent Record; Beast Girl
10:00 am | Total Dramarama | Cluckwork Orange; Free Chili
10:30 am | Total Dramarama | Venthalla; Duck Duck Juice
11:00 am | Summer Camp Island | The First Day; Popular Banana Split
11:30 am | Summer Camp Island | Monster Babies; Time Traveling Quick Pants
12:00 pm | Teen Titans Go! | Dreams; Grandma Voice
12:30 pm | Amazing World of Gumball | The Goons; The Secret
 1:00 pm | Adventure Time | The Ultimate Adventure; Come Along With Me
 2:00 pm | Unikitty | Little Prince Puppycorn; Kickflip McPuppycorn
 2:30 pm | Unikitty | Pet Pet; Super Amazing Raft Adventure
 3:00 pm | Ben 10 | The Ring Leader
 3:15 pm | Total Dramarama | Free Chili
 3:30 pm | Total Dramarama | Venthalla; Duck Duck Juice
 4:00 pm | Amazing World of Gumball | The Grades; The Disaster
 4:30 pm | Amazing World of Gumball | The Re-run; The Diet
 5:00 pm | Craig of the Creek | Monster in the Garden; Too Many Treasures
 5:30 pm | Craig of the Creek | Jessica Goes to the Creek; Escape From Family Dinner
 6:00 pm | Teen Titans Go! | Shrimps and Prime Rib; The Academy
 6:30 pm | Teen Titans Go! | Throne of Bones; Booby Trap House
 7:00 pm | We Bare Bears | Baby Bears Can't Jump; Family Troubles
 7:30 pm | Amazing World of Gumball | The Father; The Worst