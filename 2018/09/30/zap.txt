# 2018-09-30
 6:00 am | Ben 10 | Past Aliens Present
 6:15 am | Ben 10 | Dreamtime
 6:30 am | Mega Man: Fully Charged | Bored to Be Wild
 6:45 am | Mega Man: Fully Charged | Enter the Wood Man!
 7:00 am | Teen Titans Go! | Burger vs. Burrito; Matched
 7:30 am | Teen Titans Go! | Colors of Raven; The Left Leg
 8:00 am | Total DramaRama | Germ Factory
 8:15 am | Total DramaRama | Ant We All Just Get Along
 8:30 am | Total DramaRama | Cuttin' Corners
 8:45 am | Total DramaRama | Sharing Is Caring
 9:00 am | Teen Titans Go! | Mouth Hole; Hot Garbage
 9:30 am | Teen Titans Go! | Robin Backwards; Crazy Day
10:00 am | Teen Titans Go! | Smile Bones; Real Boy Adventures
10:30 am | Teen Titans Go! | Yearbook Madness; Hose Water
11:00 am | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
11:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
12:00 pm | Teen Titans Go! | Master Detective
12:15 pm | Teen Titans Go! | Titan Saving Time
12:30 pm | Teen Titans Go! | Hand Zombie
12:45 pm | Teen Titans Go! | Tower Renovation
 1:00 pm | Total DramaRama | Germ Factory
 1:15 pm | Total DramaRama | Aquarium for a Dream
 1:30 pm | Total DramaRama | The Date
 1:45 pm | Total DramaRama | Cluckwork Orange
 2:00 pm | Amazing World of Gumball | The Copycats
 2:15 pm | Amazing World of Gumball | The Spinoffs
 2:30 pm | Lego Batman: The Movie - DC Super Heroes Unite | 
 4:00 pm | Total DramaRama | Germ Factory
 4:15 pm | Total DramaRama | Duck Duck Juice
 4:30 pm | Total DramaRama | Venthalla
 4:45 pm | Total DramaRama | Free Chili
 5:00 pm | Amazing World of Gumball | The Vase
 5:15 pm | Amazing World of Gumball | The Founder
 5:30 pm | Amazing World of Gumball | The Outside
 5:45 pm | Amazing World of Gumball | The Schooling
 6:00 pm | Amazing World of Gumball | The Girlfriend
 6:15 pm | Amazing World of Gumball | The Advice
 6:30 pm | Amazing World of Gumball | The Signal
 6:45 pm | Amazing World of Gumball | The Parasite
 7:00 pm | Amazing World of Gumball | The Slide
 7:15 pm | Amazing World of Gumball | The Understanding
 7:30 pm | Amazing World of Gumball | The Test
 7:45 pm | Amazing World of Gumball | The Ad