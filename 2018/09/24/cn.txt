# 2018-09-24
 6:00 am | Amazing World of Gumball | Pest/The Advice
 6:30 am | Amazing World of Gumball | Sale/The Signal
 7:00 am | Amazing World of Gumball | Code/The Intelligence
 7:30 am | Amazing World of Gumball | Test/The Potion
 8:00 am | Teen Titans Go! | Wally T/Hot Salad Water
 8:30 am | Teen Titans Go! | Rad Dudes with Bad Tudes/Lication
 9:00 am | Teen Titans Go! | Waffles/Opposites
 9:30 am | Teen Titans Go! | Birds/Be Mine
10:00 am | Amazing World of Gumball | The Pizza/The Lie
10:30 am | Amazing World of Gumball | The Butterfly/The Question
11:00 am | Amazing World of Gumball | Routine/The Awkwardness
11:30 am | Amazing World of Gumball | Upgrade/The Nest
12:00 pm | Teen Titans Go! | Dignity of Teeth/Two Parter Part 1
12:30 pm | Teen Titans Go! | Two Parter Part 2/Croissant
 1:00 pm | OK K.O.! Let's Be Heroes | T.K.O.
 1:30 pm | OK K.O.! Let's Be Heroes | We've Got Fleas/No More Pow Cards
 2:00 pm | Teen Titans Go! | Throne of Bones/Demon Prom
 2:30 pm | Teen Titans Go! | BBCYFSHIPBDAY/Beast Girl
 3:00 pm | Ben 10 | Reststop Roustabout
 3:15 pm | Teen Titans Go! | Two Bumble Bees and a Wasp
 3:30 pm | Amazing World of Gumball | The Watch/The Bet
 4:00 pm | Amazing World of Gumball | Heist/The Brain
 4:30 pm | Amazing World of Gumball | Best/The Parents
 5:00 pm | Craig of the Creek | Itch to Explore/The Future is Cardboard
 5:30 pm | Teen Titans Go! | Brian/Nature
 6:00 pm | Teen Titans Go! | Burger vs. Burrito/Matched
 6:30 pm | Teen Titans Go! | Colors of Raven/The Left Leg
 7:00 pm | We Bare Bears | Charlie Ball/Bear Flu
 7:30 pm | Amazing World of Gumball | Box/The Spinoffs