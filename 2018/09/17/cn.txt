# 2018-09-17
 6:00 am | Amazing World of Gumball | The Society/The Spoiler
 6:30 am | Amazing World of Gumball | The Countdown/The Nobody
 7:00 am | Amazing World of Gumball | Detective/The Faith
 7:30 am | Amazing World of Gumball | Fury/The Candidate
 8:00 am | Teen Titans Go! | Permanent Record/BBBDay!
 8:30 am | Teen Titans Go! | Squash & Stretch/Titan Saving Time
 9:00 am | Teen Titans Go! | Parasite/Starliar
 9:30 am | Teen Titans Go! | Meatball Party/Staff Meeting
10:00 am | Amazing World of Gumball | The Recipe/The Puppy
10:30 am | Amazing World of Gumball | The Name/The Extras
11:00 am | Amazing World of Gumball | Return/The Hug
11:30 am | Amazing World of Gumball | Nemesis/The Wicked
12:00 pm | Teen Titans Go! | Truth, Justice, and What?/Beast Man
12:30 pm | Teen Titans Go! | Two Bumble Bees and a Wasp/Oil Drums
 1:00 pm | Adventure Time | Too Young/Thank You
 1:30 pm | Adventure Time | Adventure Time with Fionna and Cake/The Monster
 2:00 pm | Teen Titans Go! | Lication/Ones and Zeros
 2:30 pm | Teen Titans Go! | Career Day/The Academy
 3:00 pm | Ben 10 | Vote Zombozo!
 3:15 pm | Teen Titans Go! | The Mask
 3:30 pm | Amazing World of Gumball | The Knights/The Colossus
 4:00 pm | Total DramaRama | Sharing Is Caring/Cuttin' Corners
 4:30 pm | Total DramaRama | Free Chili/Cluckwork Orange
 5:00 pm | Craig of the Creek | Lost in the Sewer/Big Pinchy
 5:30 pm | Teen Titans Go! | Little Buddies/Missing
 6:00 pm | Teen Titans Go! | Driver's Ed/Dog Hand
 6:30 pm | Teen Titans Go! | Double Trouble/The Date
 7:00 pm | We Bare Bears | Everyday Bears/Tote Life
 7:30 pm | Amazing World of Gumball | Stars/The Brain