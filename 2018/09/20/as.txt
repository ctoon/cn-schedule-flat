# 2018-09-20
 8:00 pm | Dragon Ball Super | With New Hope in His Heart - Farewell, Trunks
 8:30 pm | Dragon Ball Super | Come Forth, Shenron! Whose Wish Will Be Granted?!
 9:00 pm | American Dad | Deacon Stan, Jesus Man
 9:30 pm | American Dad | Bullocks to Stan
10:00 pm | Bob's Burgers | It Snakes a Village
10:30 pm | Bob's Burgers | Family Fracas
11:00 pm | Family Guy | Take a Letter
11:30 pm | Family Guy | The New Adventures of Old Tom
12:00 am | Rick and Morty | Anatomy Park
12:30 am | Robot Chicken | Zeb and Kevin Erotic Hot Tub Canvas
12:45 am | Robot Chicken | Cheese Puff Mountain
 1:00 am | Harvey Birdman | Bird Girl of Guantanamole
 1:15 am | 12oz Mouse | Spider
 1:30 am | Bob's Burgers | It Snakes a Village
 2:00 am | Bob's Burgers | Family Fracas
 2:30 am | Family Guy | Take a Letter
 3:00 am | Family Guy | The New Adventures of Old Tom
 3:30 am | American Dad | Deacon Stan, Jesus Man
 4:00 am | Black Dynamite | "Black Jaws!" or "Finger Lickin' Chicken of the Sea"
 4:30 am | Rick and Morty | Anatomy Park
 5:00 am | Robot Chicken | Lust for Puppets
 5:15 am | Robot Chicken | Anne Marie's Pride
 5:30 am | American Dad | Bullocks to Stan