# 2023-12-11
 6:00 am | Amazing World of Gumball | The Scam
 6:14 am | Amazing World of Gumball | The Test
 6:28 am | Amazing World of Gumball | The Slide
 6:42 am | Amazing World of Gumball | The Loophole
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Talking Tree
 7:00 am | Amazing World of Gumball | The Voice
 7:15 am | Amazing World of Gumball | The Promise
 7:29 am | CoComelon | Shape Song/ My Body Song/ Hide and Go Seek in the Snow (Jingle Bells)
 7:43 am | Meet the Batwheels | I'll Be Your Shelter
 7:46 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:00 am | CoComelon | Getting Ready for School Song/ Daisy Bell/ Doctor Checkup Song/ Lunch Song
 8:15 am | Thomas & Friends: All Engines Go | The Super Axle
 8:29 am | CoComelon | First Day of School/ Teacher Song/ Soccer (Football) Song/ Car Wash Song
 8:43 am | Bugs Bunny Builders: Hard Hat Time | Daffy's Spa
 8:46 am | Bugs Bunny Builders | Speedy
 9:00 am | The Looney Tunes Show | Sunday Night Slice
 9:28 am | The Looney Tunes Show | DMV
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Prom-Date
10:00 am | Craig of the Creek | Capture the Flag Part V: The Game
10:30 am | Craig of the Creek | Sink or Swim Team
10:45 am | Craig of the Creek | The Quick Name
11:00 am | Amazing World of Gumball | The Petals
11:15 am | Amazing World of Gumball | The Treasure
11:30 am | Amazing World of Gumball | The Apology
11:45 am | Amazing World of Gumball | The Words
12:00 pm | A Christmas Story Christmas | 
 2:15 pm | Teen Titans Go! | Serious Business
 2:30 pm | Teen Titans Go! | Boys vs Girls
 2:45 pm | Teen Titans Go! | Body Adventure
 3:00 pm | Tiny Toons Looniversity | General HOGspital
 3:30 pm | Amazing World of Gumball | The Skull
 3:45 pm | Amazing World of Gumball | The Bet
 4:00 pm | Amazing World of Gumball | Christmas
 4:15 pm | Amazing World of Gumball | The Watch
 4:30 pm | Regular Show | High Score
 4:45 pm | Regular Show | Rage Against the TV