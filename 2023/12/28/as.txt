# 2023-12-28
 5:00 pm | Dexter's Laboratory | Babe Sitter/Mountain Mandark/2Geniuses 2Gether
 5:30 pm | Ed, Edd n Eddy | Dawn of the Eds/Vert-Ed-Go
 6:00 pm | The Grim Adventures of Billy and Mandy | Dumb-Dumbs and Dragons/Fear and Loathing in Endsville
 6:30 pm | Courage the Cowardly Dog | Heads of Beef/Klub Katz
 7:00 pm | King of the Hill | Hank's On Board
 7:30 pm | King of the Hill | Bystand Me
 8:00 pm | Bob's Burgers | Y Tu Ga-Ga Tambien
 8:30 pm | Bob's Burgers | The Secret Ceramics Room of Secrets
 9:00 pm | Bob's Burgers | Sleeping with the Frenemy
 9:30 pm | American Dad! | Every Which Way But Lose
10:00 pm | American Dad! | Weiner of Our Discontent
10:30 pm | American Dad! | Daddy Queerest
11:00 pm | American Dad! | Stan's Night Out
11:30 pm | Rick and Morty | Air Force Wong
12:00 am | Rick and Morty | That's Amorte
12:30 am | Aqua Teen Hunger Force | The Hairy Bus
12:45 am | Aqua Teen Hunger Force | Sweet C
 1:00 am | Robot Chicken | The Bleepin' Robot Chicken Archie Comics Special
 1:30 am | American Dad! | Every Which Way But Lose
 2:00 am | American Dad! | Weiner of Our Discontent
 2:30 am | American Dad! | Daddy Queerest
 3:00 am | Rick and Morty | Air Force Wong
 3:30 am | Rick and Morty | That's Amorte
 4:00 am | Ambient Swim | Relaxing Old Footage With Joe Pera
 4:30 am | Ambient Swim | LoFi Beats to Relax/Study With
 5:00 am | Bob's Burgers | Y Tu Ga-Ga Tambien
 5:30 am | Bob's Burgers | The Secret Ceramics Room of Secrets