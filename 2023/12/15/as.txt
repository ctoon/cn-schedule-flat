# 2023-12-15
 5:00 pm | The Grim Adventures of Billy and Mandy | Billy Ocean/Hill Billy
 5:30 pm | The Grim Adventures of Billy and Mandy | Keeper of the Reaper
 6:00 pm | The Grim Adventures of Billy and Mandy | The Love That Dare Not Speak Its Name/Major Cheese
 6:30 pm | The Grim Adventures of Billy and Mandy | Modern Primitives/Giant Billy and Mandy All Out Attack
 7:00 pm | King of the Hill | The Redneck on Rainey Street
 7:30 pm | King of the Hill | A Rover Runs Through It
 8:00 pm | Bob's Burgers | The Grand Mama-Pest Hotel
 8:30 pm | Bob's Burgers | Aquaticism
 9:00 pm | Bob's Burgers | Ain't Miss Debatin'
 9:30 pm | American Dad! | Dancin' A-with My Cells
10:00 pm | American Dad! | Mused and Abused
10:30 pm | American Dad! | Henderson
11:00 pm | American Dad! | Hot Scoomp
11:30 pm | Rick and Morty | Mort: Ragnarick
12:00 am | Rick and Morty | Unmortricken
12:30 am | Rick and Morty | Rickfending Your Mort
 1:00 am | Rick and Morty | Wet Kuat Amortican Summer
 1:30 am | Rick and Morty | Rise of the Numbericons: The Movie
 2:00 am | Rick and Morty | Mort: Ragnarick
 2:30 am | Rick and Morty | Unmortricken
 3:00 am | Rick and Morty | Rickfending Your Mort
 3:30 am | Rick and Morty | Wet Kuat Amortican Summer
 4:00 am | Rick and Morty | Rise of the Numbericons: The Movie
 4:30 am | Rick and Morty | Mort: Ragnarick
 5:00 am | King of the Hill | The Redneck on Rainey Street
 5:30 am | King of the Hill | A Rover Runs Through It