# 2023-12-26
 6:00 am | Amazing World of Gumball | The Origins
 6:14 am | Amazing World of Gumball | The Origins
 6:28 am | Amazing World of Gumball | The Girlfriend
 6:42 am | Amazing World of Gumball | The Advice
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Hissy Swan
 7:00 am | The Looney Tunes Show | We're In Big Truffle
 7:29 am | Bugs Bunny Builders | Buzz In
 7:43 am | Lellobee City Farm | Wheels On the Bus Holidays
 7:46 am | Bugs Bunny Builders | Stories
 8:00 am | Cocomelon | Five Senses Song
 8:14 am | Lellobee City Farm | Snow Shuffle
 8:17 am | Meet the Batwheels | Bam's Clawful Mistake
 8:31 am | Cocomelon | Three Little Kittens
 8:45 am | Jessica's Big Little World | Grocery Store Friend
 9:00 am | Craig of the Creek | Who Is the Red Poncho?
 9:14 am | Craig of the Creek | The Once and Future King
 9:28 am | We Bare Bears | Bear Cleanse
 9:42 am | We Bare Bears | Nom Nom's Entourage
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Kissy Frog
10:00 am | Scooby-Doo! and Guess Who? | The Crown Jewel of Boxing!
10:30 am | Scooby-Doo! and Guess Who? | The Internet on Haunted House Hill!
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Feather Fluffer-Upper Part 1; The Prince and The Feather Fluffer-Upper Part 2
11:30 am | Amazing World of Gumball | The Crew
11:45 am | Amazing World of Gumball | The Others
12:00 pm | Apple & Onion | Nothing Can Stop Us
12:15 pm | Apple & Onion | Good Job
12:30 pm | Clarence | Public Radio
12:45 pm | Clarence | Chad and the Marathon
 1:00 pm | Clarence | Officer Moody
 1:15 pm | Clarence | Gilben's Different
 1:30 pm | Craig of the Creek | Who Is the Red Poncho?
 1:45 pm | Craig of the Creek | The Once and Future King
 2:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 2:15 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 2:30 pm | Teen Titans Go! | The Fourth Wall
 2:45 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 3:00 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 3:30 pm | Amazing World of Gumball | The Signature
 3:45 pm | Amazing World of Gumball | The Check
 4:00 pm | Amazing World of Gumball | The Pest
 4:15 pm | Amazing World of Gumball | The Sale
 4:30 pm | Regular Show | Temp Check
 4:45 pm | Regular Show | Jinx