# 2023-12-24
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Feast Of The Fopdoodles
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 6:30 am | Amazing World of Gumball | The Gripes
 6:45 am | Amazing World of Gumball | The Vacation
 7:00 am | Amazing World of Gumball | The Fraud
 7:15 am | Amazing World of Gumball | The Void
 7:30 am | Amazing World of Gumball | Christmas
 7:45 am | Amazing World of Gumball | The Lie
 8:00 am | Scooby-Doo! Haunted Holidays | 
 8:30 am | Be Cool, Scooby-Doo! | Scary Christmas
 9:00 am | Scooby-Doo! Mystery Inc | Wrath of the Krampus
 9:30 am | What's New Scooby-Doo? | A Scooby-Doo Christmas
10:00 am | The New Scooby-Doo Mysteries | A Nutcracker Scoob
10:30 am | Scooby-Doo, Where Are You! | That's Snow Ghost
11:00 am | We Bare Bears | Christmas Parties
11:15 am | We Bare Bears | Icy Nights II
11:30 am | We Bare Bears | The Perfect Tree
11:45 am | We Bare Bears | Christmas Movies
12:00 pm | The Looney Tunes Show | A Christmas Carol
12:30 pm | Tiny Toons Looniversity | Freshman Orientoontion
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Feast Of The Fopdoodles
 1:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 1:30 pm | Amazing World of Gumball | Christmas
 1:45 pm | Amazing World of Gumball | The Lie
 2:00 pm | Craig of the Creek | Snow Day
 2:15 pm | Craig of the Creek | Locked Out Cold
 2:30 pm | Craig of the Creek | Winter Break
 3:00 pm | Adventure Time | Holly Jolly Secrets Part I; Holly Jolly Secrets Part II
 3:30 pm | Adventure Time | The More You Moe; The Moe You Know
 4:00 pm | Regular Show | The Christmas Special
 4:30 pm | Regular Show | Merry Christmas Mordecai
 4:45 pm | Regular Show | Snow Tubing