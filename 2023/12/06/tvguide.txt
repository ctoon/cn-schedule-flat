# 2023-12-06
 6:00 am | Amazing World of Gumball | The Boredom
 6:14 am | Amazing World of Gumball | The Vision
 6:28 am | Amazing World of Gumball | The Painting
 6:42 am | Amazing World of Gumball | The Laziest
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Thieftress
 7:00 am | Amazing World of Gumball | The Ghost
 7:15 am | Amazing World of Gumball | The Mystery
 7:29 am | Cocomelon | 
 7:43 am | Meet the Batwheels | The Best Present in the World
 7:46 am | Meet the Batwheels | Keep Calm and Roll On
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | The Sights of Sodor
 8:29 am | Cocomelon | 
 8:43 am | Bugs Bunny Builders: Hard Hat Time | Hard Hat Time
 8:46 am | Bugs Bunny Builders | Catwalk
 9:00 am | The Looney Tunes Show | Peel of Fortune
 9:29 am | The Looney Tunes Show | Double Date
 9:57 am | Lamput | Hypnosis
10:00 am | Craig of the Creek | The New Jersey
10:15 am | Craig of the Creek | The Sunflower
10:30 am | Craig of the Creek | Craig World
10:45 am | Craig of the Creek | Body Swap
11:00 am | Amazing World of Gumball | The Mustache
11:15 am | Amazing World of Gumball | The Date
11:30 am | Amazing World of Gumball | The Club
11:45 am | Amazing World of Gumball | The Wand
12:00 pm | Apple & Onion | The Music Store Thief
12:15 pm | Apple & Onion | The Fly
12:30 pm | Clarence | Lost Playground
12:45 pm | Clarence | Bird Boy Man
 1:00 pm | Clarence | Freedom Cactus
 1:15 pm | Clarence | Plane Excited
 1:30 pm | Craig of the Creek | Copycat Carter
 1:45 pm | Craig of the Creek | Brother Builder
 2:00 pm | Teen Titans Go! | I See You
 2:15 pm | Teen Titans Go! | Brian
 2:30 pm | Teen Titans Go! | Nature
 2:45 pm | Teen Titans Go! | Salty Codgers
 3:00 pm | Tiny Toons Looniversity | Souffle, Girl Hey
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feather Fluffer-Upper (Part 1 & 2)
 4:00 pm | Amazing World of Gumball | The Car
 4:15 pm | Amazing World of Gumball | The Curse
 4:30 pm | Regular Show | Appreciation Day
 4:45 pm | Regular Show | Peeps