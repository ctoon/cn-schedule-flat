# 2023-12-03
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feather Fluffer-Upper (Part 1 & 2)
 6:30 am | Amazing World of Gumball | The BFFS
 6:45 am | Amazing World of Gumball | The Inquisition
 7:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Penny?
 7:15 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Leslie?
 7:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Bobert?
 7:45 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball ... and Anyone?
 8:00 am | Scooby-Doo! and Guess Who? | Hollywood Knights!
 8:30 am | Scooby-Doo! and Guess Who? | The New York Underground!
 9:00 am | Scooby-Doo! on Zombie Island | 
10:45 am | We Bare Bears | Chloe
11:00 am | We Bare Bears | Panda's Date
11:15 am | We Bare Bears | Everyday Bears
11:30 am | We Bare Bears | Burrito
11:45 am | We Bare Bears | Primal
12:00 pm | The Looney Tunes Show | Best Friends Redux
12:30 pm | The Looney Tunes Show | SuperRabbit
 1:00 pm | Tiny Toons Looniversity | General HOGspital
 1:30 pm | Teen Titans Go! | Christmas Magic
 1:45 pm | Teen Titans Go! | Bottle Episode
 2:00 pm | Teen Titans Go! | Finally a Lesson
 2:15 pm | Teen Titans Go! | Arms Race with Legs
 2:30 pm | Teen Titans Go! | Obinray
 2:45 pm | Teen Titans Go! | Wally T
 3:00 pm | Teen Titans Go! | Second Christmas
 3:15 pm | Teen Titans Go! | The True Meaning of Christmas
 3:30 pm | Teen Titans Go! | Halloween vs. Christmas
 3:45 pm | Teen Titans Go! | Teen Titans Save Christmas
 4:00 pm | Teen Titans Go! | Beast Boy on a Shelf
 4:15 pm | Teen Titans Go! | Christmas Crusaders
 4:30 pm | Teen Titans Go! | The Great Holiday Escape
 4:45 pm | Teen Titans Go! | Christmas Magic