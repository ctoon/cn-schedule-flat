# 2023-12-19
 5:00 pm | Dexter's Laboratory | Dexter's Little Dilemma/Faux Chapeau/D2
 5:30 pm | Ed, Edd n Eddy | The Ed-Touchables/Nagged to Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Herbicidal Maniac/Chaos Theory
 6:30 pm | Courage the Cowardly Dog | The Hunchback of Nowhere/The Gods Must Be Goosey
 7:00 pm | King of the Hill | Mutual of OmAbwah
 7:30 pm | King of the Hill | The Unbearable Blindness of Laying
 8:00 pm | Bob's Burgers | Gene's Christmas Break
 8:30 pm | Bob's Burgers | Bob Rest Ye Merry Gentle-Mannequins
 9:00 pm | Bob's Burgers | Christmas in the Car
 9:30 pm | American Dad! | The Best Christmas Story Never Told
10:00 pm | American Dad! | The Most Adequate Christmas Ever
10:30 pm | American Dad! | Minstrel Krampus
11:00 pm | American Dad! | Dreaming of a White Porsche Christmas
11:30 pm | Rick and Morty | Air Force Wong
12:00 am | Rick and Morty | That's Amorte
12:30 am | Aqua Teen Hunger Force | Big Bro
12:45 am | Aqua Teen Hunger Force | Mail Order Bride
 1:00 am | Robot Chicken | Robot Chicken's Santa's Dead (Spoiler Alert) Holiday Murder Thing Special
 1:15 am | Mike Tyson Mysteries | The Christmas Episode
 1:30 am | American Dad! | The Best Christmas Story Never Told
 2:00 am | American Dad! | The Most Adequate Christmas Ever
 2:30 am | American Dad! | Minstrel Krampus
 3:00 am | Rick and Morty | Air Force Wong
 3:30 am | Rick and Morty | That's Amorte
 4:00 am | Off the Air | Sex
 4:15 am | Off the Air | Drugs
 4:30 am | Off the Air | Music
 4:45 am | Off the Air | Farts
 5:00 am | Bob's Burgers | Bob Rest Ye Merry Gentle-Mannequins
 5:30 am | Bob's Burgers | Christmas in the Car