# 2023-12-19
 6:00 am | Amazing World of Gumball | The Society
 6:14 am | Amazing World of Gumball | The Spoiler
 6:28 am | Amazing World of Gumball | The Countdown
 6:42 am | Amazing World of Gumball | The Nobody
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Raspberry Fairy
 7:00 am | The Looney Tunes Show | Itsy Bitsy Gopher
 7:29 am | Bugs Bunny Builders | Ice Creamed
 7:43 am | Lellobee City Farm | Deck the Halls
 7:46 am | Bugs Bunny Builders | Race Track Race
 8:00 am | Cocomelon | 
 8:14 am | Lellobee City Farm | The Reindeer Song
 8:17 am | Meet the Batwheels | Batcomputer for a Day
 8:31 am | Cocomelon | 
 8:45 am | Jessica's Big Little World | Bedtime Routine
 9:00 am | Craig of the Creek | Craig to the Future
 9:14 am | Craig of the Creek | Craig of the Street
 9:28 am | We Bare Bears | Video Date
 9:42 am | We Bare Bears | Pet Shop
 9:57 am | Lamput | Doc Dog
10:00 am | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital Of Dr. Phineas Phrag!
10:30 am | Scooby-Doo! and Guess Who? | The Phantom, The Talking Dog And The Hot Hot Hot Sauce!
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Swoony Swanstress
11:30 am | Amazing World of Gumball | The Law
11:45 am | Amazing World of Gumball | The Allergy
12:00 pm | Apple & Onion | All Work and No Play
12:15 pm | Apple & Onion | Drone Shoes
12:30 pm | Clarence | Pizza Hero
12:45 pm | Clarence | Sumo Goes West
 1:00 pm | Clarence | Valentimes
 1:15 pm | Clarence | Clarence for President
 1:30 pm | Craig of the Creek | Craiggy & the Slime Factory; Craig of the Campus
 1:45 pm | Craig of the Creek | The Cursed Word
 2:00 pm | Teen Titans Go! | Yearbook Madness
 2:15 pm | Teen Titans Go! | Beast Man
 2:30 pm | Teen Titans Go! | Operation Tin Man
 2:45 pm | Teen Titans Go! | Nean
 3:00 pm | Tiny Toons Looniversity | Extra, So Extra
 3:30 pm | Amazing World of Gumball | The Mothers
 3:45 pm | Amazing World of Gumball | The Password
 4:00 pm | Amazing World of Gumball | The Procrastinators
 4:15 pm | Amazing World of Gumball | The Shell
 4:30 pm | Regular Show | White Elephant Gift Exchange
 4:45 pm | Regular Show | Merry Christmas Mordecai