# 2023-12-30
 6:00 am | Amazing World of Gumball | The Limit
 6:15 am | Amazing World of Gumball | The Voice
 6:30 am | Amazing World of Gumball | The Promise
 6:45 am | Amazing World of Gumball | The Castle
 7:00 am | Amazing World of Gumball | The Boombox
 7:15 am | Amazing World of Gumball | The Tape
 7:30 am | Amazing World of Gumball | The Sweaters
 7:45 am | Amazing World of Gumball | The Internet
 8:00 am | Amazing World of Gumball | The Plan
 8:15 am | Amazing World of Gumball | The World
 8:30 am | Amazing World of Gumball | The Finale
 8:45 am | Amazing World of Gumball | The Kids
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feather Fluffer-Upper (Part 1 & 2)
 9:30 am | Tiny Toons Looniversity | Extra, So Extra
10:00 am | Teen Titans Go! | Permanent Record
10:15 am | Teen Titans Go! | Titan Saving Time
10:30 am | Teen Titans Go! | Hand Zombie
10:45 am | Teen Titans Go! | Employee of the Month: Redux
11:00 am | Teen Titans Go! | The Avogodo
11:15 am | Teen Titans Go! | Master Detective
11:30 am | The Looney Tunes Show | Peel of Fortune
12:00 pm | The Looney Tunes Show | Double Date
12:30 pm | Tiny Toons Looniversity | Episode 1
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feast of the Fopdoodles
 1:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 1:30 pm | Amazing World of Gumball | The Fan
 1:45 pm | Amazing World of Gumball | The Coach
 2:00 pm | Amazing World of Gumball | The Joy
 2:15 pm | Amazing World of Gumball | The Puppy
 2:30 pm | Amazing World of Gumball | The Recipe
 2:45 pm | Amazing World of Gumball | The Name
 3:00 pm | Scooby-Doo! The Sword and the Scoob | 
 4:45 pm | Amazing World of Gumball | The Extras