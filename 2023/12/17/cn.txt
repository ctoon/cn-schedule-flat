# 2023-12-17
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Wise Wizard of Wisdom
 6:30 am | Amazing World of Gumball | The Fridge
 6:45 am | Amazing World of Gumball | The Flower
 7:00 am | Amazing World of Gumball | The Banana
 7:15 am | Amazing World of Gumball | The Phone
 7:30 am | Amazing World of Gumball | The Job
 7:45 am | Amazing World of Gumball | The Remote
 8:00 am | Scooby-Doo! and Guess Who? | The Wedding Witch of Wainsly Hall!
 8:30 am | Scooby-Doo! and Guess Who? | A Run Cycle Through Time!
 9:00 am | Straight Outta Nowhere: Scooby-Doo! Meets Courage the Cowardly Dog | 
10:45 am | We Bare Bears | Brother Up
11:00 am | We Bare Bears | Occupy Bears
11:15 am | We Bare Bears | Panda's Sneeze
11:30 am | We Bare Bears | The Road
11:45 am | We Bare Bears | Emergency
12:00 pm | The Looney Tunes Show | Casa de Calma
12:30 pm | The Looney Tunes Show | Devil Dog
 1:00 pm | Tiny Toons Looniversity | Tears of a Clone
 1:30 pm | Teen Titans Go! | Booby Trap House
 1:45 pm | Teen Titans Go! | Fish Water
 2:00 pm | Teen Titans Go! | TV Knight
 2:15 pm | Teen Titans Go! | Christmas Magic
 2:30 pm | Teen Titans Go! | Teen Titans Save Christmas
 2:45 pm | Teen Titans Go! | BBSFBDAY!
 3:00 pm | The Looney Tunes Show | A Christmas Carol
 3:30 pm | New Looney Tunes | Tis the Seasoning/Winter Blunderland
 3:45 pm | New Looney Tunes | Not Lyin' Lion/Ice Ice Bunny
 4:00 pm | Baby Looney Tunes | Shadow of a Doubt/John Jacob Jongle Elmer Fudd (song)/Christmas in July
 4:30 pm | The Sylvester & Tweety Mysteries | It Happened One Night Before Christmas