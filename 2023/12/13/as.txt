# 2023-12-13
 5:00 pm | Dexter's Laboratory | Beau Tie/Remember Me/Over-Labbing
 5:30 pm | Ed, Edd n Eddy | A Fistful of Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Ecto Cooler/The Schlubs
 6:30 pm | Courage the Cowardly Dog | The Demon in the Mattress/Freaky Fred
 7:00 pm | King of the Hill | How I Learned To Stop Worrying and Love the Alamo
 7:30 pm | King of the Hill | Girl, You'll Be a Giant Soon
 8:00 pm | Bob's Burgers | They Serve Horses, Don't They?
 8:30 pm | Bob's Burgers | Large Brother, Where Fart Thou?
 9:00 pm | Bob's Burgers | Ex Machtina
 9:30 pm | American Dad! | Who Smarted?
10:00 pm | American Dad! | Russian Doll
10:30 pm | American Dad! | Stan Moves to Chicago
11:00 pm | American Dad! | The Pink Sphinx Holds Her Hearts on the Turn
11:30 pm | Rick and Morty | Mort: Ragnarick
12:00 am | Rick and Morty | Never Ricking Morty
12:30 am | Aqua Teen Hunger Force | Multiple Meat
12:45 am | Aqua Teen Hunger Force | Intervention
 1:00 am | Robot Chicken | The Robot Chicken Lots of Holidays Special
 1:15 am | Robot Chicken | The Robot Chicken Christmas Special: X-Mas United
 1:30 am | American Dad! | Who Smarted?
 2:00 am | American Dad! | Russian Doll
 2:30 am | American Dad! | The Pink Sphinx Holds Her Hearts on the Turn
 3:00 am | Rick and Morty | Mort: Ragnarick
 3:30 am | Rick and Morty | Never Ricking Morty
 4:00 am | Joe Pera Talks With You | Joe Pera Shows You How to Keep Functioning in Mid-Late Winter
 4:15 am | Joe Pera Talks With You | Joe Pera Talks With You About Legacy
 4:30 am | Joe Pera Talks With You | Joe Pera Builds a Chair With You
 5:00 am | Bob's Burgers | They Serve Horses, Don't They?
 5:30 am | Bob's Burgers | Large Brother, Where Fart Thou?