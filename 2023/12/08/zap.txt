# 2023-12-08
 6:00 am | Amazing World of Gumball | The Choices
 6:14 am | Amazing World of Gumball | The Code
 6:28 am | Amazing World of Gumball | The Secret
 6:42 am | Amazing World of Gumball | The Sock
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Plucky Duck
 7:00 am | Amazing World of Gumball | The Genius
 7:15 am | Amazing World of Gumball | The Poltergeist
 7:29 am | Cocomelon | Looby Loo
 7:43 am | Meet the Batwheels | Bad Day
 7:46 am | Jessica's Big Little World | Grocery Store Friend
 8:00 am | Cocomelon | Boo Boo Song
 8:15 am | Lucas the Spider | The Mighty Boop and Fly Wonder; Lucas and Findley get Antsy; Mock Me Not
 8:42 am | Thomas & Friends: All Engines Go | The Number 1 Engine
 8:45 am | Bugs Bunny Builders | Crane Game
 9:00 am | The Looney Tunes Show | Bugs & Daffy Get a Job
 9:29 am | The Looney Tunes Show | That's My Baby
 9:57 am | Lamput | Diet Doc
10:00 am | Craig of the Creek | Capture the Flag Part 1: The Candy
10:15 am | Craig of the Creek | Capture the Flag Part 2: The King
10:30 am | Craig of the Creek | Capture the Flag Part 3: The Legend
10:45 am | Craig of the Creek | Capture the Flag Part 4: The Plan
11:00 am | Amazing World of Gumball | The Banana
11:15 am | Amazing World of Gumball | The Remote
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and The Feather Fluffer-Upper Part 1; The Prince and The Feather Fluffer-Upper Part 2
12:00 pm | Apple & Onion | Rotten Apple
12:15 pm | Apple & Onion | The Inbetween
12:30 pm | Clarence | Attack the Block Party
12:45 pm | Clarence | Field Trippin'
 1:00 pm | Clarence | Ice Cream Hunt
 1:15 pm | Clarence | Company Man
 1:30 pm | Craig of the Creek | Winter Break
 2:00 pm | Teen Titans Go! | Caramel Apples
 2:15 pm | Teen Titans Go! | Sandwich Thief
 2:30 pm | Teen Titans Go! | Friendship
 2:45 pm | Teen Titans Go! | Vegetables
 3:00 pm | Scooby-Doo! Haunted Holidays | 
 3:30 pm | Be Cool, Scooby-Doo! | Scary Christmas
 4:00 pm | What's New Scooby-Doo? | A Scooby-Doo Christmas
 4:30 pm | Scooby-Doo, Where Are You! | That's Snow Ghost