# 2023-12-20
 5:00 pm | Dexter's Laboratory | Head Band/Stuffed Animal House/Used Ink
 5:30 pm | Ed, Edd n Eddy | X Marks the Ed/From Here to Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Fiend Is Like Friend Without the "R"/Recipe for Disaster/A Dumb Wish
 6:30 pm | Courage the Cowardly Dog | Dome of Doom/Snowman's Revenge
 7:00 pm | King of the Hill | Pretty, Pretty Dresses
 7:30 pm | King of the Hill | Hillennium
 8:00 pm | Bob's Burgers | Father of the Bob
 8:30 pm | Bob's Burgers | Nice-Capades
 9:00 pm | Bob's Burgers | The Last Gingerbread House on the Left
 9:30 pm | American Dad! | For Whom the Sleigh Bell Tolls
10:00 pm | American Dad! | Season's Beatings
10:30 pm | American Dad! | Santa, Schmanta
11:00 pm | American Dad! | A Little Extra Scratch
11:30 pm | Rick and Morty | Fear No Mort
12:00 am | Rick and Morty | Unmortricken
12:30 am | Aqua Teen Hunger Force | Cybernetic Ghost of Christmas Past
12:45 am | Aqua Teen Hunger Force | T-Shirt of the Dead
 1:00 am | Smiling Friends | Charlie Dies and Doesn't Come Back
 1:15 am | YOLO | A Very Extremely Very YOLO Christmas: Reloaded
 1:30 am | American Dad! | For Whom the Sleigh Bell Tolls
 2:00 am | American Dad! | Season's Beatings
 2:30 am | American Dad! | A Little Extra Scratch
 3:00 am | Rick and Morty | Fear No Mort
 3:30 am | Rick and Morty | Unmortricken
 4:00 am | Off the Air | Sex
 4:15 am | Off the Air | Drugs
 4:30 am | Off the Air | Music
 4:45 am | Off the Air | Farts
 5:00 am | Bob's Burgers | Nice-Capades
 5:30 am | Bob's Burgers | The Last Gingerbread House on the Left