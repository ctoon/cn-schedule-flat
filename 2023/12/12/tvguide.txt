# 2023-12-12
 6:00 am | Amazing World of Gumball | The Copycats
 6:14 am | Amazing World of Gumball | The Potato
 6:28 am | Amazing World of Gumball | The Internet
 6:42 am | Amazing World of Gumball | The Plan
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Moany Mountain
 7:00 am | Amazing World of Gumball | The World
 7:15 am | Amazing World of Gumball | The Finale
 7:29 am | Cocomelon | 
 7:43 am | Meet the Batwheels | The Best Present in the World
 7:46 am | Meet the Batwheels | Cave Sweet Cave
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | The Sights of Sodor
 8:29 am | Cocomelon | 
 8:43 am | Bugs Bunny Builders: Hard Hat Time | Lemonade
 8:46 am | Bugs Bunny Builders | Dim Sum
 9:00 am | The Looney Tunes Show | Off Duty Cop
 9:29 am | The Looney Tunes Show | Working Duck
 9:57 am | Lamput | Alien
10:00 am | Craig of the Creek | The Chef's Challenge
10:15 am | Craig of the Creek | The Sparkle Solution
10:30 am | Craig of the Creek | Better Than You
10:45 am | Craig of the Creek | The Dream Team
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
11:30 am | Amazing World of Gumball | The Authority
11:45 am | Amazing World of Gumball | The Virus
12:00 pm | Apple & Onion | Falafel's Passion
12:15 pm | Apple & Onion | Hole in Roof
12:30 pm | Clarence | Game Show
12:45 pm | Clarence | Skater Sumo
 1:00 pm | Clarence | Mystery Girl
 1:15 pm | Clarence | The Substitute
 1:30 pm | Craig of the Creek | Adventures in Baby Casino
 1:45 pm | Craig of the Creek | Creek Talent Extravaganza
 2:00 pm | Teen Titans Go! | Christmas Magic
 2:15 pm | Teen Titans Go! | The Best Robin
 2:30 pm | Teen Titans Go! | Road Trip
 2:45 pm | Teen Titans Go! | Hot Garbage
 3:00 pm | Tiny Toons Looniversity | Give Pizza a Chance
 3:30 pm | Amazing World of Gumball | The Pony
 3:45 pm | Amazing World of Gumball | The Hero
 4:00 pm | Amazing World of Gumball | The Dream
 4:15 pm | Amazing World of Gumball | The Sidekick
 4:30 pm | Regular Show | Party Pete
 4:45 pm | Regular Show | Brain Eraser