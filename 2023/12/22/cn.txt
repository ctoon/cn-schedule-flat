# 2023-12-22
 6:00 am | Amazing World of Gumball | The Boss
 6:14 am | Amazing World of Gumball | The Move
 6:28 am | Amazing World of Gumball | The Law
 6:42 am | Amazing World of Gumball | The Allergy
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Moany Mountain
 7:00 am | The Looney Tunes Show | It's a Handbag
 7:29 am | Bugs Bunny Builders | Play Day
 7:43 am | Lellobee City Farm | Deck the Halls
 7:46 am | Bugs Bunny Builders | Rock On
 8:00 am | CoComelon | Tie Your Shoes Song/ Stick to It/ Valentine's Day Song
 8:15 am | Lucas the Spider | The Scent of a Dog / The Need for Seed / Bloom Patrol
 8:42 am | Lellobee City Farm | The Reindeer Song
 8:45 am | Jessica's Big Little World | The Big Kid Playground
 9:00 am | Craig of the Creek | Bernard of the Creek Part 1
 9:14 am | Craig of the Creek | Bernard of the Creek Part 2
 9:28 am | We Bare Bears | Yard Sale
 9:42 am | We Bare Bears | Slumber Party
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sassy Gnomes
10:00 am | Scooby-Doo! and Guess Who? | Scooby On Ice!
10:30 am | Scooby-Doo! and Guess Who? | Caveman on the Half Pipe!
11:00 am | Amazing World of Gumball | The Downer
11:15 am | Amazing World of Gumball | The Triangle
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Blobby Princess
11:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Wise Wizard of Wisdom
12:00 pm | Apple & Onion | Broccoli
12:15 pm | Apple & Onion | Cousin's Day
12:30 pm | Total DramaRama | Me, My Elf, and I
12:45 pm | Total DramaRama | Snow Country for Old Men
 1:00 pm | Total DramaRama | The Tree Stooges Save Christmas
 1:30 pm | Craig of the Creek | Snow Day
 1:45 pm | Craig of the Creek | Locked Out Cold
 2:00 pm | Teen Titans Go! | Second Christmas
 2:15 pm | Teen Titans Go! | The True Meaning of Christmas
 2:30 pm | Teen Titans Go! | Halloween vs. Christmas
 2:45 pm | Teen Titans Go! | Teen Titans Save Christmas
 3:00 pm | Teen Titans Go! | Beast Boy on a Shelf
 3:15 pm | Teen Titans Go! | Christmas Crusaders
 3:30 pm | Teen Titans Go! | A Holiday Story
 3:45 pm | Teen Titans Go! | The Great Holiday Escape
 4:00 pm | Teen Titans Go! | Christmas Magic
 4:15 pm | Regular Show | White Elephant Gift Exchange
 4:30 pm | Regular Show | Merry Christmas Mordecai
 4:45 pm | Regular Show | Snow Tubing