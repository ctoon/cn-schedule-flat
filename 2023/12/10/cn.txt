# 2023-12-10
 6:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Feast of the Fopdoodles
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Boasty Boar
 6:30 am | Amazing World of Gumball | The Prank
 6:45 am | Amazing World of Gumball | The GI
 7:00 am | Amazing World of Gumball | The Kiss
 7:15 am | Amazing World of Gumball | The Party
 7:30 am | Amazing World of Gumball | The Refund
 7:45 am | Amazing World of Gumball | The Robot
 8:00 am | Scooby-Doo! and Guess Who? | Fear of the Fire Beast!
 8:30 am | Scooby-Doo! and Guess Who? | Too Many Dummies!
 9:00 am | Tom & Jerry: Willy Wonka and the Chocolate Factory | 
10:45 am | We Bare Bears | Jean Jacket
11:00 am | We Bare Bears | Nom Nom
11:15 am | We Bare Bears | Shush Ninjas
11:30 am | We Bare Bears | My Clique
11:45 am | We Bare Bears | Charlie
12:00 pm | The Looney Tunes Show | Jailbird and Jailbunny
12:30 pm | The Looney Tunes Show | Fish and Visitors
 1:00 pm | Tiny Toons Looniversity | Save The Loo Bru
 1:30 pm | Teen Titans Go! | Booty Scooty
 1:45 pm | Teen Titans Go! | Who's Laughing Now
 2:00 pm | Teen Titans Go! | Oregon Trail
 2:15 pm | Teen Titans Go! | Snuggle Time
 2:30 pm | Teen Titans Go! | Oh Yeah!
 2:45 pm | Teen Titans Go! | Christmas Magic
 3:00 pm | Amazing World of Gumball | Christmas
 3:15 pm | Amazing World of Gumball | The Lie
 3:30 pm | We Bare Bears | Christmas Parties
 3:45 pm | We Bare Bears | The Perfect Tree
 4:00 pm | We Bare Bears | Christmas Movies
 4:15 pm | Clarence | Merry Moochmas
 4:30 pm | Uncle Grandpa | Christmas Special