# 2023-12-29
 5:00 pm | Dexter's Laboratory | Height Unseen/Bygone Errors/Folly Calls
 5:30 pm | Dexter's Laboratory | Voice Over/Blonde Leading the Blonde/Comic Stripper
 6:00 pm | Dexter's Laboratory | Tee Party/Dexter's Wacky Races
 6:30 pm | Dexter's Laboratory | The Lab of Tomorrow/Chicken Scratch/Garage Sale
 7:00 pm | King of the Hill | Bill's House
 7:30 pm | King of the Hill | Harlottown
 8:00 pm | Bob's Burgers | The Hurt Soccer
 8:30 pm | Bob's Burgers | Cheer Up Sleepy Gene
 9:00 pm | Bob's Burgers | The Trouble with Doubles
 9:30 pm | American Dad! | In Country...Club
10:00 pm | American Dad! | Moon Over Isla Island
10:30 pm | American Dad! | Home Adrone
11:00 pm | American Dad! | Brains, Brains, and Automobiles
11:30 pm | Rick and Morty | Unmortricken
12:00 am | Aqua Teen Hunger Force | Shaketopia
12:15 am | Aqua Teen Hunger Force | A Quiet Shake
12:30 am | Aqua Teen Hunger Force | Scrip2 2i2le: The Ts Are 2s
12:45 am | Aqua Teen Hunger Force | Get Lit Upon a Situpon
 1:00 am | Aqua Teen Hunger Force | Anubis
 1:15 am | Aqua Teen Hunger Force | Knapsack!
 1:30 am | Aqua Teen Hunger Force | Rabbit, Not Rabbot
 1:45 am | Aqua Teen Hunger Force | Hospice
 2:00 am | American Dad! | In Country...Club
 2:30 am | American Dad! | Moon Over Isla Island
 3:00 am | American Dad! | Home Adrone
 3:30 am | American Dad! | Brains, Brains, and Automobiles
 4:00 am | Futurama | The Butterjunk Effect
 4:30 am | Futurama | The Six Million Dollar Mon
 5:00 am | King of the Hill | Bill's House
 5:30 am | King of the Hill | Harlottown