# 2023-12-07
 6:00 am | Amazing World of Gumball | The Stories
 6:14 am | Amazing World of Gumball | The Disaster
 6:28 am | Amazing World of Gumball | The Re-Run
 6:42 am | Amazing World of Gumball | The Guy
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Pretty Poodle
 7:00 am | Amazing World of Gumball | The Kiss
 7:15 am | Amazing World of Gumball | The Party
 7:29 am | CoComelon | Merry / Deck the Halls/ Christmas Songs/ New Year/ Giving/ Farm/ Resolution
 7:57 am | Meet the Batwheels | Jet Set
 8:00 am | CoComelon | Where's My Little Dog Gone?/ Opposites Song/ The Muffin Man/ Thank You Song
 8:15 am | Thomas & Friends: All Engines Go | Something Broken, Someone Blue
 8:29 am | CoComelon | This Is The Way/ Old MacDonald/ Yes Save the Earth Song/ Tap Dancing Song
 8:43 am | Bugs Bunny Builders: Hard Hat Time | Lunch Break
 8:46 am | Bugs Bunny Builders | Dim Sum
 9:00 am | The Looney Tunes Show | A Christmas Carol
 9:28 am | The Looney Tunes Show | Newspaper Thief
 9:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Raspberry Fairy
10:00 am | Craig of the Creek | Copycat Carter
10:15 am | Craig of the Creek | Brother Builder
10:30 am | Craig of the Creek | Jessica the Intern
10:45 am | Craig of the Creek | Locked Out Cold
11:00 am | Amazing World of Gumball | The Microwave
11:15 am | Amazing World of Gumball | The Meddler
11:30 am | Amazing World of Gumball | The Helmet
11:45 am | Amazing World of Gumball | The Fight
12:00 pm | Apple & Onion | Dragonhead
12:15 pm | Apple & Onion | Pulling Your Weight
12:30 pm | Clarence | Escape from Beyond the Cosmic
12:45 pm | Clarence | Ren Faire
 1:00 pm | Clarence | Time Crimes
 1:15 pm | Clarence | Saturday School
 1:30 pm | Craig of the Creek | Alternate Creekiverse
 1:45 pm | Craig of the Creek | Snow Day
 2:00 pm | Teen Titans Go! | Knowledge
 2:15 pm | Teen Titans Go! | Slumber Party
 2:30 pm | Teen Titans Go! | Love Monsters
 2:45 pm | Teen Titans Go! | Christmas Magic
 3:00 pm | Tiny Toons Looniversity | Prank You Very Much
 3:30 pm | Amazing World of Gumball | The Colossus
 3:45 pm | Amazing World of Gumball | The Knights
 4:00 pm | Amazing World of Gumball | The Fridge
 4:15 pm | Amazing World of Gumball | The Flower
 4:30 pm | Regular Show | Dizzy
 4:45 pm | Regular Show | My Mom