# 2023-12-04
 6:00 am | Amazing World of Gumball | The Roots
 6:14 am | Amazing World of Gumball | The Blame
 6:28 am | Amazing World of Gumball | The Slap
 6:42 am | Amazing World of Gumball | The Detective
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sassy Gnomes
 7:00 am | Amazing World of Gumball | The Car
 7:15 am | Amazing World of Gumball | The Curse
 7:30 am | Thomas & Friends: All Engines Go | The Mystery of Lookout Mountain
 8:50 am | Lucas the Spider | Too Hot to Handle
 9:00 am | The Looney Tunes Show | Casa de Calma
 9:29 am | The Looney Tunes Show | Devil Dog
 9:57 am | Lamput | Gym
10:00 am | Craig of the Creek | Alternate Creekiverse
10:15 am | Craig of the Creek | Snow Day
10:30 am | Craig of the Creek | Winter Break
11:00 am | Amazing World of Gumball | The Painting
11:15 am | Amazing World of Gumball | The Laziest
11:30 am | Amazing World of Gumball | The Ghost
11:45 am | Amazing World of Gumball | The Mystery
12:00 pm | Charlie and the Chocolate Factory | 
 2:30 pm | Teen Titans Go! | Christmas Magic
 2:45 pm | Teen Titans Go! | Puppets, Whaaaaat?
 3:00 pm | Tiny Toons Looniversity | Episode 1
 3:30 pm | Amazing World of Gumball | The Prank
 3:45 pm | Amazing World of Gumball | The GI
 4:00 pm | Amazing World of Gumball | The Kiss
 4:15 pm | Amazing World of Gumball | The Party
 4:30 pm | Regular Show | Don
 4:45 pm | Regular Show | Rigby's Body