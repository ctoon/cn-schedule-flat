# 2023-12-21
 6:00 am | Amazing World of Gumball | The Grades
 6:14 am | Amazing World of Gumball | The Diet
 6:28 am | Amazing World of Gumball | The Procrastinators
 6:42 am | Amazing World of Gumball | The Shell
 6:56 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Prom-Date
 7:00 am | The Looney Tunes Show | Customer Service
 7:29 am | Bugs Bunny Builders | Tweety-Go-Round
 7:43 am | Lellobee City Farm | Snowman Showman Song
 7:46 am | Bugs Bunny Builders | Smash House
 8:00 am | Cocomelon | 
 8:28 am | Lellobee City Farm | Jingle Bells
 8:31 am | Cocomelon | 
 8:45 am | Jessica's Big Little World | Small Uncle's Big Bath
 9:00 am | Craig of the Creek | Craiggy & the Slime Factory; Craig of the Campus
 9:14 am | Craig of the Creek | The Cursed Word
 9:28 am | We Bare Bears | Hibernation
 9:42 am | We Bare Bears | Charlie Ball
 9:57 am | Lamput | Snow
10:00 am | Scooby-Doo! and Guess Who? | The Feast Of Dr. Frankenfooder!
10:30 am | Scooby-Doo! and Guess Who? | A Fashion Nightmare!
11:00 am | Amazing World of Gumball | The Saint
11:15 am | Amazing World of Gumball | The Friend
11:30 am | Amazing World of Gumball | The Oracle
11:45 am | Amazing World of Gumball | The Safety
12:00 pm | Apple & Onion | Panamanian Night Monkey
12:15 pm | Apple & Onion | Za
12:30 pm | Clarence | Clarence's Stormy Sleepover - Episode 4: Dingus & McNobrain
12:45 pm | Clarence | Clarence's Stormy Sleepover - Episode 5; Bye Bye Baker
 1:00 pm | Clarence | Clarence's Stormy Sleepover - Episode 6: Flood Brothers
 1:15 pm | Clarence | Pool's Out for Summer
 1:30 pm | Craig of the Creek | Bored Games
 1:45 pm | Craig of the Creek | Scoutguest
 2:00 pm | Teen Titans Go! | More of the Same
 2:15 pm | Teen Titans Go! | Some of Their Parts
 2:30 pm | Teen Titans Go! | Cat's Fancy
 2:45 pm | Teen Titans Go! | Leg Day
 3:00 pm | Tiny Toons Looniversity | Save the Loo Bru
 3:30 pm | Amazing World of Gumball | The Society
 3:45 pm | Amazing World of Gumball | The Spoiler
 4:00 pm | Amazing World of Gumball | The Countdown
 4:15 pm | Amazing World of Gumball | The Nobody
 4:30 pm | Regular Show | The Christmas Special