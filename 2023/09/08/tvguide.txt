# 2023-09-08
 6:00 am | Amazing World of Gumball | The End; The DVD
 6:30 am | Amazing World of Gumball | The Knights; The Colossus
 7:00 am | Amazing World of Gumball | The Promise; The Voice
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Wheel Side Story
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | Magnet Mayhem/Stop That Train
 8:45 am | Bugs Bunny Builders | Honey Bunny Hide N' Seek
 9:00 am | The Looney Tunes Show | Gribbler's Quest
 9:30 am | The Looney Tunes Show | The Grand Old Duck of York
10:00 am | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
10:30 am | Craig of the Creek | War of the Pieces
11:00 am | Amazing World of Gumball | The Fridge; The Remote
11:30 am | Amazing World of Gumball | The Flower; The Banana
12:00 pm | Summer Camp Island | Basketball Liaries; The Mop Forever
12:30 pm | Clarence | A Chad and the Marathon; Sumoful Mind
 1:00 pm | Clarence | Officer Moody; Animal Day
 1:30 pm | Craig of the Creek | Silver Fist Returns; Craiggy & the Slime Factory
 2:00 pm | Teen Titans Go! | Head Fruit; Kicking a Ball & Pretending To Be Hurt
 2:30 pm | Teen Titans Go! | Operation Tin Man; Nean
 3:00 pm | Amazing World of Gumball | The Boombox; The Castle
 3:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 4:00 pm | Amazing World of Gumball | The Internet; The Plan
 4:30 pm | Amazing World of Gumball | The World; The Finale