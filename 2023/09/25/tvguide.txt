# 2023-09-25
 6:00 am | Amazing World of Gumball | The Understanding; The Ad
 6:30 am | Amazing World Of Gumball | The Ad; The Ghouls
 7:00 am | Amazing World of Gumball | The Rival; The Lady
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Riddle Me This
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Bruno's Map Mishap
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Snow Cap
 9:00 am | The Looney Tunes Show | The DMV
 9:30 am | The Looney Tunes Show | Off Duty Cop
10:00 am | Craig of the Creek | The Mystery Of The Timekeeper; The Great Fossil Rush
10:30 am | Craig of the Creek | Alone Quest; Memories Of Bobby
11:00 am | Craig of the Creek | 
11:30 am | Craig of the Creek | 
12:00 pm | Summer Camp Island | 
12:30 pm | Clarence | 
 1:00 pm | Teen Titans Go! | 
 1:30 pm | Teen Titans Go! | 
 2:00 pm | Amazing World of Gumball | 
 2:30 pm | Amazing World of Gumball | 
 3:00 pm | Amazing World of Gumball | 
 3:30 pm | Amazing World of Gumball | 
 4:00 pm | Amazing World of Gumball | 
 4:30 pm | Amazing World of Gumball | 