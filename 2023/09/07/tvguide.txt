# 2023-09-07
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Amazing World of Gumball | The Pony; The Storm
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Tough Buff Blues
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Bad Luck Boxcar
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Batty Kathy Hide N' Seek
 9:00 am | The Looney Tunes Show | The Black Widow
 9:30 am | The Looney Tunes Show | Mrs. Porkbunny's
10:00 am | Craig of the Creek | Bernard of the Creek Part 1; Bernard of the Creek Part 2
10:30 am | Craig of the Creek | You're It; Jessica Goes to the Creek
11:00 am | Amazing World of Gumball | The Car; The Curse
11:30 am | Amazing World of Gumball | The Microwave; The Meddler
12:00 pm | Summer Camp Island | Computer Vampire; Susie's Fantastical Scavenger Hunt
12:30 pm | Clarence | A Clarence Loves Shoopy; Nightmare on Aberdale Street: Balance's Revenge
 1:00 pm | Clarence | Public Radio; Chadsgiving
 1:30 pm | Craig of the Creek | War of the Pieces
 2:00 pm | Teen Titans Go! | Oil Drums; Two Bumble Bees and a Wasp
 2:30 pm | Teen Titans Go! | Cool School; Video Game References
 3:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 3:30 pm | Amazing World of Gumball | The Hero; The Photo
 4:00 pm | Amazing World of Gumball | The Tag; The Lesson
 4:30 pm | Amazing World of Gumball | The Limit; The Game