# 2023-09-01
 6:00 am | Amazing World of Gumball | The Wish; The Decisions
 6:30 am | Amazing World of Gumball | The Factory; The Web
 7:00 am | Amazing World of Gumball | The Party; The Refund
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Sidekicked to the Curb Bat Surfin'
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | That Ol' Time Rock Rolls!/Make Like a Banana and Split
 8:45 am | Bugs Bunny Builders | Batty Kathy
 9:00 am | The Looney Tunes Show | Itsy Bitsy Gopher
 9:30 am | The Looney Tunes Show | Rebel Without a Glove
10:00 am | Craig of the Creek | The Scoutguest; Cow-Boy and Marie
10:30 am | Craig of the Creek | The My Stare Lady; Cursed Word
11:00 am | Amazing World of Gumball | The Revolt; The Inquisition
11:30 am | Amazing World of Gumball | The Third; The Debt
12:00 pm | Summer Camp Island | Pajama Pajimjams; I Heart Heartforde
12:30 pm | Clarence | Plant Daddies; Sumo Goes West
 1:00 pm | Clarence | Bucky and the Howl; Valentimes
 1:30 pm | Craig of the Creek | Fire & Ice; Hyde & Zeke
 2:00 pm | Teen Titans Go! | Road Trip; The Best Robin
 2:30 pm | Teen Titans Go! | Hot Garbage; Mouth Hole
 3:00 pm | Amazing World of Gumball | The Robot; The Picnic
 3:30 pm | Amazing World of Gumball | The Goons; The Secret
 4:00 pm | Amazing World of Gumball | The Sock; The Genius
 4:30 pm | Amazing World of Gumball | The Mustache; The Date