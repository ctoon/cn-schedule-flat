# 2023-09-20
 6:00 am | Amazing World of Gumball | The News
 6:30 am | Tiny Toons Looniversity | Extra, So Extra
 7:00 am | Amazing World of Gumball | The Bus
 7:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 7:45 am | Meet the Batwheels | Improvise My Ride
 8:00 am | Cocomelon | Tie Your Shoes Song
 8:15 am | Thomas & Friends: All Engines Go | Not So Easy-Greasy
 8:30 am | Cocomelon | Five Senses Song
 8:45 am | Bugs Bunny Builders | Honey Bunny
 9:00 am | The Looney Tunes Show | Double Date
 9:30 am | The Looney Tunes Show | Newspaper Thief
10:00 am | Craig of the Creek | Bedtime Routine
10:15 am | Craig of the Creek | Alternate Creekiverse
10:30 am | Craig of the Creek | Jessica Shorts
11:00 am | Amazing World of Gumball | The Ollie
11:30 am | Amazing World of Gumball | The Potato
12:00 pm | Summer Camp Island | Hedgehog Werewolf
12:30 pm | Clarence | Zoo
 1:00 pm | Clarence | Rise and Shine
 1:30 pm | Craig of the Creek | The Other Side: The Tournament
 2:00 pm | Teen Titans Go! | Pyramid Scheme
 2:30 pm | Tiny Toons Looniversity | Extra, So Extra
 3:00 pm | Amazing World of Gumball | The Night
 3:30 pm | Amazing World of Gumball | The Misunderstandings
 4:00 pm | Amazing World of Gumball | The Roots
 4:30 pm | Amazing World of Gumball | The Blame