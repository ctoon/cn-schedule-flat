# 2023-09-18
 6:00 am | Amazing World of Gumball | The Night; The Guy
 6:30 am | Amazing World of Gumball | The Misunderstandings; The Boredom
 7:00 am | Amazing World of Gumball | The Gift; The Advice
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | To the Batmobile!
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | It All Adds Up
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | The Looney Tunes Show | The Foghorn Leghorn Story
 9:30 am | The Looney Tunes Show | Casa de Calma
10:00 am | Craig of the Creek | Dog Decider; Jpony
10:30 am | Craig of the Creek | Bring out Your Beast; Ace of Squares
11:00 am | Amazing World of Gumball | The Roots; The Vision
11:30 am | Amazing World of Gumball | The Blame; The Choices
12:00 pm | Summer Camp Island | Ice Cream Headache; Midnight Quittance
12:30 pm | Clarence | Anywhere but Sumo; Jeff's New Toy
 1:00 pm | Clarence | Dinner Party; Nature Clarence
 1:30 pm | Craig of the Creek | Dinner At The Creek; The Climb
 2:00 pm | Teen Titans Go! | Hey You; Don't Forget About Me in Your Memory; Bbbday!
 2:30 pm | Teen Titans Go! | Accept the Next Proposition You Hear Squash & Stretch
 3:00 pm | Amazing World of Gumball | The Signal; The Apprentice
 3:30 pm | Tiny Toons Looniversity | Extra, So Extra
 4:00 pm | Amazing World of Gumball | The Pest; The Parasite
 4:30 pm | Amazing World of Gumball | The Hug; The Love