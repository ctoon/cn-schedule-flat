# 2023-09-18
 6:00 am | Amazing World of Gumball | The Night
 6:30 am | Amazing World of Gumball | The Misunderstandings
 7:00 am | Amazing World of Gumball | The Gift
 7:30 am | Cocomelon | JJ Wants a New Bed
 7:45 am | Meet the Batwheels | To the Batmobile!
 8:00 am | Cocomelon | Wait Your Turn
 8:15 am | Thomas & Friends: All Engines Go | It All Adds Up
 8:30 am | Cocomelon | Wheels on the Bus School
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | The Looney Tunes Show | The Foghorn Leghorn Story
 9:30 am | The Looney Tunes Show | Casa de Calma
10:00 am | Craig of the Creek | You're It
10:30 am | Craig of the Creek | Itch to Explore
11:00 am | Amazing World of Gumball | The Roots
11:30 am | Amazing World of Gumball | The Blame
12:00 pm | Summer Camp Island | Ice Cream Headache
12:30 pm | Clarence | Anywhere but Sumo
 1:00 pm | Clarence | Dinner Party
 1:30 pm | Craig of the Creek | Monster in the Garden
 2:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 2:30 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 3:00 pm | Amazing World of Gumball | The Apprentice
 3:30 pm | Tiny Toons Looniversity | Extra, So Extra
 4:00 pm | Amazing World of Gumball | The Pest
 4:30 pm | Amazing World of Gumball | The Hug