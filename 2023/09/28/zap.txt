# 2023-09-28
 6:00 am | Amazing World of Gumball | The Sucker
 6:15 am | Amazing World of Gumball | The Vegging
 6:30 am | Amazing World of Gumball | The One
 6:45 am | Amazing World of Gumball | The Father
 7:00 am | Amazing World of Gumball | The Intelligence
 7:15 am | Amazing World of Gumball | The Potion
 7:30 am | Cocomelon | Please and Thank You Song
 7:45 am | Meet the Batwheels | Batcomputer for a Day
 8:00 am | Cocomelon | Are We There Yet?
 8:15 am | Thomas & Friends: All Engines Go | Bruno's Map Mishap
 8:30 am | Cocomelon | My Name Song
 8:45 am | Bugs Bunny Builders | Play Day
 9:00 am | The Looney Tunes Show | The Shelf
 9:30 am | The Looney Tunes Show | Muh-Muh-Muh-Murder
10:00 am | Craig of the Creek | Council of the Creek
10:15 am | Craig of the Creek | Sparkle Cadet
10:30 am | Craig of the Creek | Cousin of the Creek
10:45 am | Craig of the Creek | Camper on the Run
11:00 am | Amazing World of Gumball | The Cringe
11:15 am | Amazing World of Gumball | The Cage
11:30 am | Amazing World of Gumball | The Faith
11:45 am | Amazing World of Gumball | The Candidate
12:00 pm | Summer Camp Island | Tub on the Run
12:15 pm | Summer Camp Island | Spotted Bear Stretch
12:30 pm | Clarence | Hoofin' It
12:45 pm | Clarence | Detention
 1:00 pm | Clarence | Hairence
 1:15 pm | Clarence | Lil Buddy
 1:30 pm | Craig of the Creek | The Great Fossil Rush
 1:45 pm | Craig of the Creek | The Mystery of the Timekeeper
 2:00 pm | Teen Titans Go! | The Streak, Part 1
 2:15 pm | Teen Titans Go! | The Streak, Part 2
 2:30 pm | Teen Titans Go! | Inner Beauty of a Cactus
 2:45 pm | Teen Titans Go! | Movie Night
 3:00 pm | Amazing World of Gumball | The Possession
 3:15 pm | Amazing World of Gumball | The Master
 3:30 pm | Amazing World of Gumball | The Silence
 3:45 pm | Amazing World of Gumball | The Future
 4:00 pm | Amazing World of Gumball | The Wish
 4:15 pm | Amazing World of Gumball | The Factory
 4:30 pm | Amazing World of Gumball | The Agent
 4:45 pm | Amazing World of Gumball | The Web