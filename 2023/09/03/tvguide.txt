# 2023-09-03
 6:00 am | Amazing World of Gumball | The Fraud; The Void
 6:30 am | Amazing World of Gumball | The Move; The Boss
 7:00 am | Amazing World of Gumball | The Law; The Allergy
 7:30 am | Amazing World of Gumball | The Password; The Mothers
 8:00 am | Apple & Onion | The Perfect Team; A New Life
 8:30 am | Apple & Onion | Tips; Falafel's Fun Day
 9:00 am | Scooby-Doo! Frankencreepy | 
10:45 am | We Bare Bears | Bro Brawl
11:00 am | We Bare Bears | Hurricane Hal; Googs
11:30 am | We Bare Bears | Vacation; Paperboyz
12:00 pm | The Looney Tunes Show | Customer Service
12:30 pm | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
 1:00 pm | Teen Titans Go! | Girls Night In
 1:30 pm | Teen Titans Go! | The Viewers Decide; The Great Disaster
 2:00 pm | Teen Titans Go! | Don't Be an Icarus; TV Knight 4
 2:30 pm | Teen Titans Go! | Little Elvis; Stockton; Ca!
 3:00 pm | Amazing World of Gumball | The Procrastinators; The Shell
 3:30 pm | Amazing World of Gumball | The Mirror; The Burden
 4:00 pm | Amazing World of Gumball | The Bros; The Man
 4:30 pm | Amazing World of Gumball | The Pizza; The Treasure