# 2023-09-19
 6:00 am | Amazing World of Gumball | The Detective
 6:30 am | Amazing World of Gumball | The Fury
 7:00 am | Amazing World of Gumball | The Sale
 7:30 am | Cocomelon | Five Senses Song
 7:45 am | Meet the Batwheels | Dynamic Du-Oh-No
 8:00 am | Cocomelon | Lost Hamster
 8:15 am | Thomas & Friends: All Engines Go | Bad Luck Boxcar
 8:30 am | Cocomelon | Winter Show and Tell
 8:45 am | Bugs Bunny Builders | Batty Kathy
 9:00 am | The Looney Tunes Show | Eligible Bachelors
 9:30 am | Tiny Toons Looniversity | Freshman Orientoontion
10:00 am | Craig of the Creek | Dinner at the Creek
10:30 am | Craig of the Creek | Sour Candy Trials
11:00 am | Amazing World of Gumball | The Disaster
11:30 am | Amazing World of Gumball | The Loophole
12:00 pm | Summer Camp Island | Pepper's Blanket Is Missing
12:30 pm | Clarence | Honk
 1:00 pm | Clarence | Dollar Hunt
 1:30 pm | Craig of the Creek | Creek Daycare
 2:00 pm | Teen Titans Go! | Two Parter: Part One
 2:30 pm | Teen Titans Go! | Secret Garden
 3:00 pm | Amazing World of Gumball | The Routine
 3:30 pm | Amazing World of Gumball | The Parking
 4:00 pm | Amazing World of Gumball | The Upgrade
 4:30 pm | Amazing World of Gumball | The Origins