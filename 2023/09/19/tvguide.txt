# 2023-09-19
 6:00 am | Amazing World of Gumball | The Detective; The Test
 6:30 am | Amazing World of Gumball | The Fury; The Slide
 7:00 am | Amazing World of Gumball | The Sale; The Scam
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Dynamic Du-Oh-No
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Bad Luck Boxcar
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Batty Kathy
 9:00 am | The Looney Tunes Show | Eligible Bachelors
 9:30 am | Tiny Toons Looniversity | Episode 1
10:00 am | Craig of the Creek | Jextra Perrestrial; Doorway To Helen
10:30 am | Craig of the Creek | The Takeout Mission; The Last Kid In The Creek
11:00 am | Amazing World of Gumball | The Disaster; The Re-run
11:30 am | Amazing World of Gumball | The Sorcerer; The Loophole
12:00 pm | Summer Camp Island | The Pepper's Blanket Is Missing; Great Elf Invention Convention
12:30 pm | Clarence | Average Jeff; Honk
 1:00 pm | Clarence | Lizard Day Afternoon; Dollar Hunt
 1:30 pm | Craig of the Creek | Kid From 3030; The Bug City
 2:00 pm | Teen Titans Go! | Two Parter Part 1; Two Parter Part 2
 2:30 pm | Teen Titans Go! | Secret Garden; The Cruel Giggling Ghoul
 3:00 pm | Amazing World of Gumball | The Awkwardness; The Routine
 3:30 pm | Amazing World of Gumball | The Parking; The Nest
 4:00 pm | Amazing World of Gumball | The Upgrade; The Points
 4:30 pm | Amazing World of Gumball | Origins; The; Origins Part 2; the Carly & Cranky's Big Lift-Off