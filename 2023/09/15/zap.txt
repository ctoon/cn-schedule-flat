# 2023-09-15
 6:00 am | Amazing World of Gumball | The Boss; The Move
 6:30 am | Amazing World of Gumball | The Law; The Allergy
 7:00 am | Amazing World of Gumball | The Return
 7:30 am | Cocomelon | Five Little Birds V3
 7:45 am | Meet the Batwheels | To the Batmobile!
 8:00 am | Cocomelon | Five Little Birds V3
 8:15 am | Mecha Builders | Picture Perfect Park Party; Sun Block
 8:45 am | Bugs Bunny Builders | Ice Creamed
 9:00 am | The Looney Tunes Show | Fish and Visitors
 9:30 am | The Looney Tunes Show | Monster Talent
10:00 am | Craig of the Creek | Monster in the Garden
10:30 am | Craig of the Creek | The Curse
11:00 am | Amazing World of Gumball | The Mothers; The Password
11:30 am | Amazing World of Gumball | The Procrastinators; The Shell
12:00 pm | Tiny Toons Looniversity | Give Pizza a Chance
12:30 pm | Clarence | Etiquette Clarence
 1:00 pm | Clarence | Video Store
 1:30 pm | Craig of the Creek | Wildernessa
 2:00 pm | Teen Titans Go! | The Spice Game
 2:30 pm | Teen Titans Go! | I'm the Sauce
 3:00 pm | Amazing World of Gumball | The Nemesis
 3:30 pm | Amazing World of Gumball | The Crew
 4:00 pm | Amazing World of Gumball | The Others
 4:30 pm | Amazing World of Gumball | The Signature