# 2023-09-05
 5:00 pm | Dexter's Laboratory | School Girl Crushed/Chess Mom/Father Knows Least
 5:30 pm | Ed, Edd n Eddy | Cool Hand Ed/Too Smart for His Own Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Which Came First?/Substitute Teacher
 6:30 pm | Courage the Cowardly Dog | Remembrance of Courage Past/Perfect
 7:00 pm | King of the Hill | Enrique-cilable Differences
 7:30 pm | King of the Hill | Mutual of OmAbwah
 8:00 pm | King of the Hill | Care-Takin' Care of Business
 8:30 pm | Bob's Burgers | Boywatch
 9:00 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 9:30 pm | American Dad! | Flavortown
10:00 pm | American Dad! | Persona Assistant
10:30 pm | American Dad! | The Legend of Old Ulysses
11:00 pm | American Dad! | Twinanigans
11:30 pm | Rick and Morty | Get Schwifty
12:00 am | Rick and Morty | The Ricks Must Be Crazy
12:30 am | Royal Crackers | Casa de Darby
 1:00 am | The Boondocks | The Fund-Raiser
 1:30 am | American Dad! | Flavortown
 2:00 am | American Dad! | Persona Assistant
 2:30 am | American Dad! | The Legend of Old Ulysses
 3:00 am | Rick and Morty | Get Schwifty
 3:30 am | Rick and Morty | The Ricks Must Be Crazy
 4:00 am | Royal Crackers | Casa de Darby
 4:30 am | Birdgirl | The S.I.M.M.
 5:00 am | Bob's Burgers | Boywatch
 5:30 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps