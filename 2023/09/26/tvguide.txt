# 2023-09-26
 6:00 am | Amazing World of Gumball | The Cringe; The Cage
 6:30 am | Amazing World of Gumball | The Candidate; The Faith
 7:00 am | Amazing World of Gumball | The Anybody; The Pact
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Zoomsday
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Not So Easy-Greasy/It All Adds Up
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Tweety-Go-Round
 9:00 am | The Looney Tunes Show | Working Duck
 9:30 am | The Looney Tunes Show | French Fries
10:00 am | Craig of the Creek | Return Of The Honeysuckle Rangers; Jacob Of The Creek
10:30 am | Craig of the Creek | Kelsey The Elder; Sour Candy Trials
11:00 am | Amazing World of Gumball | The Rival; The Lady
11:30 am | Amazing World of Gumball | The Vegging; The Sucker
12:00 pm | Summer Camp Island | Molar Moles; Tortilla Towel
12:30 pm | Clarence | Suspended; Turtle Hats
 1:00 pm | Clarence | Goldfish Follies; Goose Chase
 1:30 pm | Craig of the Creek | Sparkle Cadet; Council Of The Creek
 2:00 pm | Teen Titans Go! | Riding the Dragon; The Overbite
 2:30 pm | Teen Titans Go! | The Cape; Shrimps and Prime Rib
 3:00 pm | Amazing World of Gumball | The Neighbor; The Shippening
 3:30 pm | Amazing World of Gumball | The Brain; The Parents
 4:00 pm | Amazing World of Gumball | The Founder; The Schooling
 4:30 pm | Amazing World of Gumball | The Intelligence; The Potion