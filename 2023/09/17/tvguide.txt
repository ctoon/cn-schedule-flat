# 2023-09-17
 6:00 am | Amazing World of Gumball | The Disaster; The Re-run
 6:30 am | Amazing World of Gumball | The Guy; The Astological
 7:00 am | Amazing World of Gumball | The Boredom; The Outside
 7:30 am | Amazing World of Gumball | The Vision; The Xxxxx
 8:00 am | Apple & Onion | Pancake's Bus Tour; Block Party
 8:30 am | Apple & Onion | Apple's Focus; Lil Noodle
 9:00 am | Scooby-Doo! Legend of the Phantosaur | 
10:45 am | We Bare Bears | Baby Bears Can't Jump
11:00 am | We Bare Bears | Go Fish; I, Butler
11:30 am | We Bare Bears | Teacher's Pet; Family Troubles
12:00 pm | The Looney Tunes Show | Devil Dog
12:30 pm | Tiny Toons Looniversity | Extra, So Extra
 1:00 pm | Teen Titans Go! | Lucky Stars; Superhero Feud
 1:30 pm | Teen Titans Go! | Magic Man; Record Book
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 3:00 pm | Amazing World of Gumball | The Choices; The Matchmaker
 3:30 pm | Amazing World of Gumball | The Styles; The Box
 4:00 pm | Amazing World Of Gumball | The Diet; The Console
 4:30 pm | Amazing World of Gumball | The Test; The Ollie