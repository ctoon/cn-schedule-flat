# 2023-09-27
 6:00 am | Amazing World of Gumball | The Neighbor; The Shippening
 6:30 am | Amazing World of Gumball | The Brain; The Parents
 7:00 am | Amazing World of Gumball | The Founder; The Schooling
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Batcomputer for a Day
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | It All Adds Up
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Smash House
 9:00 am | The Looney Tunes Show | Beauty School
 9:30 am | The Looney Tunes Show | The Float
10:00 am | Craig of the Creek | Fort Williams
10:15 am | Craig of the Creek | Summer Wish
10:30 am | Craig of the Creek | Turning the Tables
10:45 am | Craig of the Creek | Kelsey The Author
11:00 am | Amazing World of Gumball | The Father; The One
11:30 am | Amazing World of Gumball | The Cringe; The Cage
12:00 pm | Summer Camp Island | Acorn Graduation; Dungeon Doug
12:30 pm | Clarence | Straight Illin; Chimney
 1:00 pm | Clarence | Dust Buddies
 1:15 pm | Clarence | Hurricane Dilliss
 1:30 pm | Craig of the Creek | Stink Bomb
 1:45 pm | Craig of the Creek | The Evolution Of Craig
 2:00 pm | Teen Titans Go! | Booby Trap House; Fish Water
 2:30 pm | Teen Titans Go! | TV Knight; BBSFBDAY!
 3:00 pm | Amazing World of Gumball | The Understanding; The Ad
 3:30 pm | Amazing World Of Gumball | The Ghouls; The Stink
 4:00 pm | Amazing World of Gumball | The Awareness; The Slip
 4:30 pm | Amazing World of Gumball | The Drama; The Buddy