# 2023-09-24
 6:00 am | Amazing World Of Gumball | The Petals; The Sucker
 6:30 am | Amazing World of Gumball | The Puppets; The Vegging
 7:00 am | Amazing World of Gumball | The Nuisance; The One
 7:30 am | Amazing World of Gumball | The Father; The Line
 8:00 am | Apple & Onion | Gyranoid Of The Future; Fun Proof
 8:30 am | Apple & Onion | Heatwave; Whale Spotting
 9:00 am | Scooby-Doo!: Camp Scare | 
10:45 am | We Bare Bears | Best Bears
11:00 am | We Bare Bears | Crowbar Jones: Origins; Mom App
11:30 am | We Bare Bears | The Limo; Hot Sauce
12:00 pm | The Looney Tunes Show | Off Duty Cop
12:30 pm | Tiny Toons Looniversity | Looneyball Lights
 1:00 pm | Teen Titans Go! | 287 Night Begins to Shine 2: You're the One; Bucket List
 2:30 pm | Teen Titans Go! | Warner Bros 100th Anniversary
 3:00 pm | Amazing World of Gumball | The Cringe; The List
 3:30 pm | Amazing World of Gumball | The Traffic; The Cage
 4:00 pm | Amazing World of Gumball | The Rival; The Faith
 4:30 pm | Amazing World of Gumball | The Lady; The Candidate