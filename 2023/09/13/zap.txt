# 2023-09-13
 6:00 am | Amazing World of Gumball | The Nemesis
 6:30 am | Tiny Toons Looniversity | Freshman Orientoontion
 7:00 am | Amazing World of Gumball | The Mirror; The Burden
 7:30 am | Cocomelon | Funny Face Song
 7:45 am | Meet the Batwheels | Improvise My Ride
 8:00 am | Cocomelon | Row Row Row Your Boat
 8:15 am | Thomas & Friends: All Engines Go | Bad Luck Boxcar
 8:30 am | Cocomelon | Shadow Puppets
 8:45 am | Bugs Bunny Builders | Batty Kathy
 9:00 am | The Looney Tunes Show | SuperRabbit
 9:30 am | The Looney Tunes Show | Best Friends Redux
10:00 am | Craig of the Creek | Too Many Treasures
10:30 am | Craig of the Creek | Wildernessa
11:00 am | Amazing World of Gumball | The Others
11:30 am | Amazing World of Gumball | The Signature
12:00 pm | Summer Camp Island | It's My Party
12:30 pm | Clarence | Trampoline
 1:00 pm | Clarence | Clarence The Movie
 1:30 pm | Craig of the Creek | The Team Up
 2:00 pm | Teen Titans Go! | Cat's Fancy
 2:30 pm | Tiny Toons Looniversity | Give Pizza a Chance
 3:00 pm | Amazing World of Gumball | The Bros; The Man
 3:30 pm | Amazing World of Gumball | The Butterfly; The Question
 4:00 pm | Amazing World of Gumball | The Oracle; The Safety
 4:30 pm | Amazing World of Gumball | The Friend; The Saint