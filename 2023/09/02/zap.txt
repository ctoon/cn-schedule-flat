# 2023-09-02
 6:00 am | Amazing World of Gumball | The Boombox; The Castle
 6:30 am | Amazing World of Gumball | The Tape; The Sweaters
 7:00 am | Amazing World of Gumball | The Internet; The Plan
 7:30 am | Amazing World of Gumball | The World; The Finale
 8:00 am | Amazing World of Gumball | The Kids; The Fan
 8:30 am | Amazing World of Gumball | The Coach; The Joy
 9:00 am | Looney Tunes Cartoons | Looney Tunes Cartoons Back To School Special
 9:25 am | Looney Tunes Cartoons | Funny Book Bunny; Balloon Salesman: All the Balloons; Kitty Krashers
 9:45 am | Teen Titans Go! | Our House
10:00 am | Teen Titans Go! | Beard Hunter
10:30 am | Teen Titans Go! | Them Soviet Boys
11:00 am | Craig of the Creek | Brother Builder
11:30 am | Craig of the Creek | Capture the Flag Part 1: The Candy
12:00 pm | The Looney Tunes Show | Semper Lie
12:30 pm | The Looney Tunes Show | Father Figures
 1:00 pm | Teen Titans Go! | Communicate Openly
 1:30 pm | Teen Titans Go! | Strength of a Grown Man
 2:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 2:30 pm | Amazing World of Gumball | The Name; The Extras
 3:00 pm | Scooby-Doo! WrestleMania Mystery | 
 4:45 pm | Amazing World of Gumball | The Ex