# 2023-03-15
 6:00 am | Amazing World of Gumball | The Party; The Refund
 6:30 am | Amazing World of Gumball | Robot; The Picnic; the Skinny at the Pool
 7:00 am | Teen Titans Go! | BBRAE
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Rev and Let Rev Calling All Batwheels
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Fast Friends
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Big Feet
 9:00 am | New Looney Tunes | Loon Raker Angry Bird; Gorky Pork Hard Hat Hare Porky Perfect
 9:30 am | New Looney Tunes | The Area Fifty Run Porker in the Court; Porky's Duck-Livery Service Wabbit Who Would Be King
10:00 am | Teen Titans Go! | The Mug; Hafo Safo
10:30 am | Teen Titans Go! | Pig in a Poke; Zimdings
11:00 am | Craig of the Creek | The Other Side: The Tournament
11:30 am | Craig of the Creek | Fall Anthology; Alternate Creekiverse
12:00 pm | Total DramaRama | Mad Math: Taffy Road; Code.T.
12:30 pm | Total DramaRama | Tp2: Judgement Bidet; I Dream of Meanie
 1:00 pm | Teen Titans Go! | The Streak: Part 1; The Streak: Part 2
 1:30 pm | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
 2:00 pm | Amazing World of Gumball | The Possession; The Web
 2:30 pm | Amazing World Of Gumball | The Inquisition; The Heart
 3:00 pm | We Baby Bears | Temple Bears; Happy Bouncy Fun Town
 3:30 pm | Amazing World of Gumball | Darwin's Yearbook - Banana Joe - Darwin's Yearbook - Clayton
 4:00 pm | Amazing World of Gumball | Darwin's Yearbook - Carrie Darwin's Yearbook - Alan
 4:30 pm | Amazing World of Gumball | Darwin's Yearbook - Sarah Darwin's Yearbook - Teachers
 5:00 pm | Craig of the Creek | Rise and Fall of the Green Poncho; The; Welcome to Creek Street
 5:30 pm | Teen Titans Go! | Island Adventures
 6:30 pm | Teen Titans Go! | Oh Yeah! Booby Trap House
 7:00 pm | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
 7:30 pm | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!