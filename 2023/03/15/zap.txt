# 2023-03-15
 6:00 am | Amazing World of Gumball | The Party; The Refund
 6:30 am | Amazing World of Gumball | The Robot; The Picnic
 7:00 am | Teen Titans Go! | BBRAE Pt 1
 7:30 am | Cocomelon | Tap Dancing Song
 7:45 am | Meet the Batwheels | Rev and Let Rev
 8:00 am | Cocomelon | YoYo's Arts and Crafts Time: Paper Airplanes
 8:15 am | Thomas & Friends: All Engines Go | Fast Friends
 8:30 am | Cocomelon | Opposites Song
 8:45 am | Bugs Bunny Builders | Big Feet
 9:00 am | New Looney Tunes | Loon Raker; Angry Bird
 9:30 am | New Looney Tunes | Area Fifty Run; Porker in the Court
10:00 am | Teen Titans Go! | The Mug
10:30 am | Teen Titans Go! | Zimdings
11:00 am | Craig of the Creek | The Other Side: The Tournament
11:30 am | Craig of the Creek | Fall Anthology
12:00 pm | Total DramaRama | Mad Math: Taffy Road
12:30 pm | Total DramaRama | TP2: Judgement Bidet
 1:00 pm | Teen Titans Go! | The Streak, Part 1
 1:30 pm | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
 2:00 pm | Amazing World of Gumball | The Possession
 2:30 pm | Amazing World of Gumball | The Heart
 3:00 pm | We Baby Bears | Temple Bears
 3:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 4:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 4:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 5:00 pm | Craig of the Creek | The Rise and Fall of the Green Poncho
 5:30 pm | Teen Titans Go! | Island Adventures
 6:30 pm | Teen Titans Go! | Oh Yeah!
 7:00 pm | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
 7:30 pm | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!