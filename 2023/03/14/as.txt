# 2023-03-14
 8:00 pm | King Of the Hill | Board Games
 8:30 pm | King Of the Hill | An Officer and a Gentle Boy
 9:00 pm | King Of the Hill | The Miseducation of Bobby Hill
 9:30 pm | Bob's Burgers | The Kids Run the Restaurant
10:00 pm | Bob's Burgers | Boyz 4 Now
10:30 pm | American Dad! | School Lies
11:00 pm | American Dad! | License to Till
11:30 pm | Rick and Morty | Mortynight Run
12:00 am | Rick and Morty | Auto Erotic Assimilation
12:30 am | Mike Tyson Mysteries | Broken Wings
12:45 am | Mike Tyson Mysteries | Ring of Fire
 1:00 am | YOLO: Silver Destiny | The Parents Episode
 1:15 am | Smiling Friends | The Smiling Friends Go To Brazil!
 1:30 am | American Dad! | Jenny Fromdabloc
 2:00 am | Rick and Morty | Mortynight Run
 2:30 am | Rick and Morty | Auto Erotic Assimilation
 3:00 am | Mike Tyson Mysteries | Broken Wings
 3:15 am | Mike Tyson Mysteries | Ring of Fire
 3:30 am | Metalocalypse | Doublebookedklok
 4:00 am | Futurama | Fry Am the Egg Man
 4:30 am | Futurama | The Tip of the Zoidberg
 5:00 am | Bob's Burgers | The Kids Run the Restaurant
 5:30 am | Bob's Burgers | Boyz 4 Now