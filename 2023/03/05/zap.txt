# 2023-03-05
 6:00 am | Amazing World of Gumball | The Sock; The Genius
 6:30 am | Amazing World of Gumball | The Mustache; The Date
 7:00 am | Amazing World of Gumball | The Club; The Wand
 7:30 am | Amazing World of Gumball | The Ape; The Poltergeist
 8:00 am | Amazing World of Gumball | The Quest; The Spoon
 8:30 am | Amazing World of Gumball | The Car; The Curse
 9:00 am | Craig of the Creek | Brother Builder
 9:30 am | Craig of the Creek | Beyond the Overpass
10:00 am | Teen Titans Go! | TV Knight 8
10:30 am | Teen Titans Go! | TV Knight 2
11:00 am | Teen Titans Go! | The Academy
11:30 am | Teen Titans Go! | Throne of Bones
12:00 pm | The Powerpuff Girls | The Headsucker's Moxy; Equal Fights
12:30 pm | The Powerpuff Girls | Major Competition
 1:00 pm | The Powerpuff Girls | The Boys Are Back in Town
 1:30 pm | The Powerpuff Girls | Insect Inside
 2:00 pm | The Powerpuff Girls | The Powerpuff Girls' Best Rainy-Day Adventure Ever; Just Desserts
 2:30 pm | The Powerpuff Girls | Telephonies; Tough Love
 3:00 pm | The Powerpuff Girls | Monstra City; Shut the Pup Up!
 3:30 pm | The Powerpuff Girls | Jewel of the Aisle; Super Zeroes
 4:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 4:30 pm | Amazing World of Gumball | The Helmet; The Fight
 5:00 pm | Amazing World of Gumball | The End; The DVD
 5:30 pm | Amazing World of Gumball | The Knights; The Colossus
 6:00 pm | The Neverending Story | 