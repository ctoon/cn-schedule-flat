# 2023-03-24
 6:00 am | Amazing World of Gumball | The Helmet; The Fight
 6:30 am | Amazing World of Gumball | End; The Dvd; the Memory Loss
 7:00 am | Teen Titans Go! | Master Detective Hot Salad Water
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Riddle Me This Interview With a Crime-Fighter
 8:00 am | Cocomelon | 
 8:15 am | Sesame Street Mecha Builders | Get in Gearfree-Wheelin' Ferris Wheel
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | New Looney Tunes | Downton Wabby Fowl Me Twice; Easter Bunny Imposter Easter Tweets Boogie Button
 9:30 am | New Looney Tunes | Hare to the Throne Part 1 Hare to the Throne Part 2; Hoarder up Cougar; Cougar
10:00 am | Teen Titans Go! | Glunkakakakah; Captain Cool
10:30 am | Teen Titans Go! | The Control Freak; Drip
11:00 am | Craig of the Creek | Craig of the Street; Puppy Love
11:30 am | Craig of the Creek | Galactic Goodbyes; Back to Cool
12:00 pm | Total DramaRama | Legends of the Paul; Cactus Makes Perfect
12:30 pm | Total DramaRama | Bridgette Too Far; A; Aches and Ladders
 1:00 pm | Teen Titans Go! | Little Elvis; Feed Me
 1:30 pm | Teen Titans Go! | Booty Eggs; Easter Creeps
 2:00 pm | Amazing World of Gumball | The Night; The Misunderstandings
 2:30 pm | Amazing World of Gumball | The Tag; The Lesson
 3:00 pm | We Baby Bears | Gross Worm; A; Dragon Pests
 3:30 pm | Amazing World of Gumball | The Limit; The Game
 4:00 pm | Amazing World of Gumball | The Promise; The Voice
 4:30 pm | Amazing World of Gumball | The Boombox; The Castle
 5:00 pm | Craig of the Creek | Jessica the Intern Brother Builder
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 6:00 pm | Teen Titans Go! | Always Be Crimefighting; Demon Prom
 6:30 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 7:00 pm | Scooby-Doo! and Guess Who? | Too Many Dummies!
 7:30 pm | Scooby-Doo! and Guess Who? | Dance Matron Of Mayhem!