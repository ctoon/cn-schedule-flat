# 2023-03-29
 6:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 am | Amazing World of Gumball | The Mirror; The Burden
 7:00 am | Teen Titans Go! | Winning a Golf Tournament Is the Solution to All of Life's Money Problems
 7:30 am | Cocomelon | The Hiccup Song
 7:45 am | Meet the Batwheels | Grass Is Greener
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Percy Disappears
 8:30 am | Cocomelon | Sharing Song
 8:45 am | Bugs Bunny Builders | Cheesy Peasy
 9:00 am | New Looney Tunes | Lifestyles of the Wealthy and Obnoxious; The Starship Mentalprise
 9:30 am | New Looney Tunes | State Fair and Balanced; Pussyfoot Soldier
10:00 am | Teen Titans Go! | Easter Annihilation
10:15 am | Teen Titans Go! | Stickier Situation, A
10:30 am | Teen Titans Go! | Flashback
11:00 am | Craig of the Creek | Too Many Treasures
11:30 am | Craig of the Creek | Wildernessa
12:00 pm | Total DramaRama | The Date
12:30 pm | Total DramaRama | Aquarium for a Dream
 1:00 pm | Teen Titans Go! | Ghost Boy; La Larva Amor
 1:30 pm | Teen Titans Go! | Hey Pizza!; Gorilla
 2:00 pm | Amazing World of Gumball | The Law; The Allergy
 2:30 pm | Amazing World of Gumball | The Mothers; The Password
 3:00 pm | We Baby Bears | Big Trouble Little Babies
 3:30 pm | Amazing World of Gumball | The Procrastinators; The Shell
 4:00 pm | Amazing World of Gumball | The Mirror; The Burden
 4:30 pm | Amazing World of Gumball | The Bros; The Man
 5:00 pm | Craig of the Creek | Monster in the Garden
 5:30 pm | Teen Titans Go! | The Scoop
 6:00 pm | Teen Titans Go! | Chicken in the Cradle
 6:30 pm | Teen Titans Go! | Kabooms
 7:00 pm | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital of Dr. Phineas Phrag!
 7:30 pm | Scooby-Doo! and Guess Who? | The Phantom, The Talking Dog, and the Hot Hot Hot Sauce!