# 2023-03-18
 6:00 am | Amazing World of Gumball | The Gripes; The Vacation
 6:30 am | Amazing World of Gumball | The Fraud; The Void
 7:00 am | Amazing World of Gumball | The Move; The Boss
 7:30 am | Amazing World of Gumball | The Law; The Allergy
 8:00 am | Amazing World of Gumball | The Password; The Mothers
 8:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 9:00 am | We Baby Bears | Ice Bear's Pet; Tooth Fairy Tech
 9:30 am | Amazing World of Gumball | The Mirror; The Burden
10:00 am | Total DramaRama | Robo Teacher; For a Few Duncans More
10:30 am | Total DramaRama | Glove Glove Me Do; He Who Wears the Clown
11:00 am | Total DramaRama | Tooth About Zombies; The Us 'r' Toys
11:30 am | Total DramaRama | Lie-Ranosaurus Wrecked; Dream Worriers
12:00 pm | Craig of the Creek | The Fire & Ice; Anniversary Box
12:30 pm | Craig of the Creek | Lost & Found; Back to Cool
 1:00 pm | Craig of the Creek | Grandma Smugglers; Itch to Explore
 1:30 pm | Craig of the Creek | Champion's Hike; The You're It
 2:00 pm | Teen Titans Go! | Power of Shrimps; The Nostalgia Is Not a Substitute for an Actual Story
 2:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 3:00 pm | Teen Titans Go! | How's This For A Special? Spaaaace
 3:30 pm | Teen Titans Go! | Winning a Golf Tournament Is the Solution to All of Life's Money Problems; Little Elvis
 4:00 pm | Amazing World of Gumball | The Bros; The Man
 4:30 pm | Amazing World of Gumball | The Pizza; The Lie
 5:00 pm | Amazing World of Gumball | The Butterfly; The Question
 5:30 pm | Amazing World of Gumball | The Oracle; The Safety
 6:00 pm | Scooby-Doo! Moon Monster Madness | 