# 2023-03-19
 8:30 pm | Bob's Burgers | Mission Impos-slug-ble
 9:00 pm | Futurama | Near-Death Wish
 9:30 pm | Futurama | Viva Mars Vegas
10:00 pm | American Dad! | The Book of Fischer
10:30 pm | American Dad! | A Roger Story
11:00 pm | Rick and Morty | Childrick of Mort
11:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
12:00 am | Aqua Teen | Aqua Teen Forever: Plantasm
 1:45 am | Aqua Teen Hunger Force Forever | The Greatest Story Ever Told
 2:00 am | American Dad! | The Book of Fischer
 2:30 am | American Dad! | A Roger Story
 3:00 am | Rick and Morty | Childrick of Mort
 3:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 4:00 am | Bob's Burgers | Wag the Hog
 4:30 am | Bob's Burgers | Mission Impos-slug-ble
 5:00 am | King Of the Hill | Cheer Factor
 5:30 am | King Of the Hill | Dale Be Not Proud