# 2023-03-12
 6:00 am | Amazing World of Gumball | The Limit; The Game
 6:30 am | Amazing World of Gumball | The Promise; The Voice
 7:00 am | Amazing World of Gumball | The Boombox; The Castle
 7:30 am | Amazing World of Gumball | The Tape; The Sweaters
 8:00 am | Amazing World of Gumball | The Internet; The Plan
 8:30 am | Amazing World of Gumball | The World; The Finale
 9:00 am | Craig of the Creek | Better Than You; Dodgy Decisions
 9:30 am | Craig of the Creek | Dream Team; The; Hyde & Zeke
10:00 am | Teen Titans Go! | The Stickier Situation; A; Fight
10:30 am | Teen Titans Go! | The Chicken in the Cradle; Groover
11:00 am | Teen Titans Go! | Tower Renovation; Bbrbday
11:30 am | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
12:00 pm | Steven Universe | Mirror Gem; Ocean Gem
12:30 pm | Steven Universe | Tiger Millionaire; Alone at Sea
 1:00 pm | Steven Universe | Historical Friction; Maximum Capacity
 1:30 pm | Steven Universe | Mindful Education; Garnet's Universe
 2:00 pm | Steven Universe | Three Gems and a Baby; Future Vision
 2:30 pm | Steven Universe | Hit the Diamond; Too Far
 3:00 pm | Steven Universe | Gem Hunt; Sworn to the Sword
 3:30 pm | Steven Universe | The Return; Jail Break
 4:00 pm | Amazing World of Gumball | The Kids; The Fan
 4:30 pm | Amazing World of Gumball | The Coach; The Joy
 5:00 pm | Amazing World of Gumball | The Recipe; The Puppy
 5:30 pm | Amazing World of Gumball | The Name; The Extras
 6:00 pm | Star Wars: Attack of the Clones | 