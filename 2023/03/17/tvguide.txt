# 2023-03-17
 6:00 am | Amazing World of Gumball | The Revolt,The Agent.
 6:30 am | Amazing World of Gumball | Mess; The Decisions; the Pup Mix
 7:00 am | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Batcomputer for a Day Faster
 8:00 am | Cocomelon | 
 8:15 am | Sesame Street Mecha Builders | Can't Stop the Martiansbest Nest Test
 8:45 am | Bugs Bunny Builders | Looney Science
 9:00 am | New Looney Tunes | You Can't Train a Pig Copy Quack; Knight and Duck Color of Bunny; the Don't Be Fooled by His Size
 9:30 am | New Looney Tunes | Sir Littlechin and the Phoenix Looney Luau; Sam and the Bullet Train Swine Dining
10:00 am | Teen Titans Go! | Space House
11:00 am | Craig of the Creek | King of Camping; Winter Creeklympics
11:30 am | Craig of the Creek | Rise and Fall of the Green Poncho; The; Welcome to Creek Street
12:00 pm | Total DramaRama | Tall Tale; A; Waterhose-Five
12:30 pm | Total DramaRama | Chews Wisely; Cody the Barbarian
 1:00 pm | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
 1:30 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 2:00 pm | Amazing World of Gumball | The Laziest; The Ghost
 2:30 pm | Amazing World of Gumball | The Mystery; The Prank
 3:00 pm | We Baby Bears | Doll's House; Bath on the Nile
 3:30 pm | Amazing World of Gumball | The GI; The Kiss
 4:00 pm | Amazing World of Gumball | The Party; The Refund
 4:30 pm | Amazing World of Gumball | The Robot; The Picnic
 5:00 pm | Craig of the Creek | The Other Side
 5:30 pm | Teen Titans Go! | The Gold Standard; Beast Boy's St. Patrick's Day Luck, and It's Bad
 6:00 pm | Teen Titans Go! | Winning a Golf Tournament Is the Solution to All of Life's Money Problems; Little Elvis
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Scooby-Doo! and Guess Who? | The Fastest Fast Food Fiend!
 7:30 pm | Scooby-Doo! and Guess Who? | Space Station Scooby!