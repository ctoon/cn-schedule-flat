# 2023-03-09
 6:00 am | Amazing World of Gumball | The Ollie
 6:30 am | Amazing World of Gumball | The Potato
 7:00 am | Cocomelon | The Hiccup Song
 7:30 am | Cocomelon | Bingo Farm Version
 8:00 am | Cocomelon | Opposites Song
 8:30 am | Meet the Batwheels | Bibi's Do-Over
 9:00 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 9:30 am | Thomas & Friends: All Engines Go | Calliope Crack-Up
10:00 am | Thomas & Friends: All Engines Go | Tyrannosaurus Wrecks
10:30 am | Bugs Bunny Builders | Tweety-Go-Round
11:00 am | New Looney Tunes | Then Things Got Weird; Duck Duck Ghost
11:30 am | New Looney Tunes | Acme Instant; When Marvin Comes Martian In
12:00 pm | We Baby Bears | The Little Mer-Bear
12:30 pm | Craig of the Creek | Creek Daycare
 1:00 pm | Craig of the Creek | Sugar Smugglers
 1:30 pm | Teen Titans Go! | Beast Boy's St. Patrick's Day Luck and It's Bad
 2:00 pm | Teen Titans Go! | The Dignity of Teeth
 2:30 pm | Amazing World of Gumball | The Singing
 3:00 pm | Amazing World of Gumball | The Puppets
 3:30 pm | Amazing World of Gumball | The Lady
 4:00 pm | Amazing World of Gumball | The Sucker
 4:30 pm | Amazing World of Gumball | The Cage
 5:00 pm | Craig of the Creek | Summer Wish
 5:30 pm | Teen Titans Go! | 40 Percent, 40 Percent, 20 Percent
 6:00 pm | Teen Titans Go! | Grube's Fairytales
 6:30 pm | Teen Titans Go! | A Farce
 7:00 pm | Scooby-Doo! and Guess Who? | Scooby-Doo, Dog Wonder!
 7:30 pm | Scooby-Doo! and Guess Who? | The Movieland Monsters!