# 2023-03-27
 8:00 pm | King Of the Hill | A Portrait of the Artist as a Young Clown
 8:30 pm | King Of the Hill | Orange You Sad I Did Say Banana?
 9:00 pm | King Of the Hill | You Gotta Believe (In Moderation)
 9:30 pm | Bob's Burgers | Uncle Teddy
10:00 pm | Bob's Burgers | The Kids Rob a Train
10:30 pm | American Dad! | Max Jets
11:00 pm | American Dad! | Shallow Vows
11:30 pm | American Dad! | Naked to the Limit, One More Time
12:00 am | Rick and Morty | The ABC's of Beth
12:30 am | Mike Tyson Mysteries | Time to Fly
12:45 am | Mike Tyson Mysteries | Make a Wish and Blow
 1:00 am | Robot Chicken | Ext. Forest - Day
 1:15 am | Robot Chicken | Never Forget
 1:30 am | American Dad! | For Black Eyes Only
 2:00 am | Rick and Morty | Morty's Mind Blowers
 2:30 am | Rick and Morty | The ABC's of Beth
 3:00 am | Mike Tyson Mysteries | Time to Fly
 3:15 am | Mike Tyson Mysteries | Make a Wish and Blow
 3:30 am | The Shivering Truth | The Diff
 3:45 am | Moral Orel | Waste
 4:00 am | Futurama | Murder on the Planet Express
 4:30 am | Futurama | Meanwhile
 5:00 am | Bob's Burgers | Uncle Teddy
 5:30 am | Bob's Burgers | The Kids Rob a Train