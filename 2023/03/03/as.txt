# 2023-03-03
 8:00 pm | King Of the Hill | Now Who's the Dummy?
 8:30 pm | King Of the Hill | Ho Yeah!
 9:00 pm | King Of the Hill | The Exterminator
 9:30 pm | King Of the Hill | Luanne Virgin 2.0
10:00 pm | American Dad! | DeLorean Story-An
10:30 pm | American Dad! | Every Which Way But Lose
11:00 pm | American Dad! | Weiner of Our Discontent
11:30 pm | Rick and Morty | Pilot
12:00 am | Ballmastrz: 9009 | Ballmastrz: Rubicon
12:30 am | Ballmastrz: 9009 | Infinite Hugs: Cold Embrace of the Bloodless Progenitors!
12:45 am | Ballmastrz: 9009 | Shameful Disease of Yackety Yack! Don't Talk Back! Be Silenced Forever!
 1:00 am | Ballmastrz: 9009 | Big Boy Body, Bigger Baby Boy Heart! Can You Endure the Pain of Love? Babyball, Discover It Now!
 1:15 am | Ballmastrz: 9009 | Children of the Night Serenade Wet Nurse of Reprisal; Scream, Bloodsucker, Scream!
 1:30 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 1:45 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 2:00 am | Rick and Morty | Pilot
 2:30 am | Futurama | Bender's Game Part 1
 3:00 am | Futurama | Bender's Game Part 2
 3:30 am | Futurama | Bender's Game Part 3
 4:00 am | Futurama | Bender's Game Part 4
 4:30 am | Futurama | Rebirth
 5:00 am | King Of the Hill | Now Who's the Dummy?
 5:30 am | King Of the Hill | Ho Yeah!