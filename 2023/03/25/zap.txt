# 2023-03-25
 6:00 am | Amazing World of Gumball | The Gift
 6:30 am | Amazing World of Gumball | The Apprentice
 7:00 am | Amazing World of Gumball | The Check
 7:30 am | Amazing World of Gumball | The Pest
 8:00 am | Amazing World of Gumball | The Hug
 8:30 am | Amazing World of Gumball | The Origins
 9:00 am | We Baby Bears | Dragon Pests
 9:30 am | Amazing World of Gumball | The Disaster
10:00 am | Total DramaRama | An Egg-stremely Bad Idea
10:30 am | Total DramaRama | Exercising the Demons
11:00 am | Total DramaRama | Pudding the Planet First
11:30 am | Total DramaRama | Dissing Cousins
12:00 pm | Craig of the Creek | My Stare Lady
12:30 pm | Craig of the Creek | Silver Fist Returns
 1:00 pm | Craig of the Creek | Craig to the Future
 1:30 pm | Craig of the Creek | Craig of the Street
 2:00 pm | Teen Titans Go! | Feed Me
 2:30 pm | Teen Titans Go! | Them Soviet Boys
 3:00 pm | Teen Titans Go! | Lil' Dimples
 3:30 pm | Teen Titans Go! | Always Be Crimefighting
 4:00 pm | Amazing World of Gumball | The Signal
 4:30 pm | Amazing World of Gumball | The Girlfriend
 5:00 pm | Amazing World of Gumball | The Parasite
 5:30 pm | Amazing World of Gumball | The Love
 6:00 pm | Scooby-Doo! Stage Fright | 