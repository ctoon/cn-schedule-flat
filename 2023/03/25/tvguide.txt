# 2023-03-25
 6:00 am | Amazing World of Gumball | The Gift; The Romantic
 6:30 am | Amazing World of Gumball | The Apprentice; The Uploads
 7:00 am | Amazing World of Gumball | The Check; The Wicked
 7:30 am | Amazing World of Gumball | The Test; The Traitor
 8:00 am | Amazing World of Gumball | The Hug; The Advice
 8:30 am | Amazing World of Gumball | The Origins; The Origins Part 2
 9:00 am | We Baby Bears | Dragon Pests; High Baby Fashion
 9:30 am | Amazing World of Gumball | The Disaster; The Re-run
10:00 am | Total DramaRama | Egg-Stremely Bad Idea; An; Grody to the Maximum
10:30 am | Total DramaRama | Exercising the Demons; Wiggin' Out
11:00 am | Total DramaRama | Pudding The Planet First; Supply Mom
11:30 am | Total DramaRama | Dissing Cousins
12:00 pm | Craig of the Creek | My Stare Lady; Too Many Treasures
12:30 pm | Craig of the Creek | Silver Fist Returns; Wildernessa
 1:00 pm | Craig of the Creek | Craig to the Future; Sunday Clothes
 1:30 pm | Craig of the Creek | Craig of the Street; Escape From Family Dinner
 2:00 pm | Teen Titans Go! | Feed Me; Little Elvis
 2:30 pm | Teen Titans Go! | Them Soviet Boys; What's Opera, Titans?
 3:00 pm | Teen Titans Go! | Lil' Dimples Royal Jelly
 3:30 pm | Teen Titans Go! | Always Be Crimefighting; Strength of a Grown Man
 4:00 pm | Amazing World of Gumball | The Detective; The Signal
 4:30 pm | Amazing World of Gumball | The Fury; The Girlfriend
 5:00 pm | Amazing World of Gumball | The Parasite; The Compilation
 5:30 pm | Amazing World of Gumball | The Love; The Stories
 6:00 pm | Scooby-Doo! Stage Fright | 