# 2023-03-07
 8:00 pm | King Of the Hill | Joust like a Woman
 8:30 pm | King Of the Hill | The Bluegrass is Always Greener
 9:00 pm | King Of the Hill | The Substitute Spanish Prisoner
 9:30 pm | Bob's Burgers | Nude Beach
10:00 pm | Bob's Burgers | Broadcast Wagstaff School News
10:30 pm | American Dad! | Don't Look a Smith Horse in the Mouth
11:00 pm | American Dad! | A Jones for a Smith
11:30 pm | Rick and Morty | M. Night Shaym-Aliens!
12:00 am | Rick and Morty | The Vat of Acid Episode
12:30 am | Mike Tyson Mysteries | The Bard's Curse
12:45 am | Mike Tyson Mysteries | Save Me!
 1:00 am | YOLO: Silver Destiny | Planet Bali
 1:15 am | Smiling Friends | Who Violently Murdered Simon S. Salty?
 1:30 am | American Dad! | Return of the Bling
 2:00 am | Rick and Morty | M. Night Shaym-Aliens!
 2:30 am | Rick and Morty | The Vat of Acid Episode
 3:00 am | Mike Tyson Mysteries | The Bard's Curse
 3:15 am | Mike Tyson Mysteries | Save Me!
 3:30 am | Metalocalypse | Fertilityklok
 4:00 am | Futurama | Lethal Inspection
 4:30 am | Futurama | The Late Philip J. Fry
 5:00 am | Bob's Burgers | Nude Beach
 5:30 am | Bob's Burgers | Broadcast Wagstaff School News