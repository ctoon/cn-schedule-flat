# 2023-03-06
 6:00 am | Amazing World of Gumball | The Matchmaker
 6:30 am | Amazing World of Gumball | The Line
 7:00 am | Cocomelon | Rain Rain Go Away Indoors Version
 7:30 am | Cocomelon | Yes Yes Playground Song
 8:00 am | Cocomelon | Reading Fun
 8:30 am | Meet the Batwheels | Rockin' Robin
 9:00 am | Mecha Builders | Roll Chickens Roll; Ramp Up, Up, and Away
 9:30 am | Thomas & Friends: All Engines Go | Backwards Day
10:00 am | Thomas & Friends: All Engines Go | Chasing Rainbows
10:30 am | Bugs Bunny Builders | Rock On
11:00 am | New Looney Tunes | For the Love of Fraud; Not So Special Delivery
11:30 am | New Looney Tunes | One Carroter in Search of an Artist; The Duck Days of Summer
12:00 pm | We Baby Bears | The Magical Box
12:30 pm | Craig of the Creek | Stink Bomb
 1:00 pm | Craig of the Creek | Summer Wish
 1:30 pm | Teen Titans Go! | Squash & Stretch
 2:00 pm | Teen Titans Go! | The Fourth Wall
 2:30 pm | Amazing World of Gumball | The Code
 3:00 pm | Amazing World of Gumball | The News
 3:30 pm | Amazing World of Gumball | The Vase
 4:00 pm | Amazing World of Gumball | The Ollie
 4:30 pm | Amazing World of Gumball | The Potato
 5:00 pm | Craig of the Creek | Cousin of the Creek
 5:30 pm | Teen Titans Go! | Teen Titans Action
 6:00 pm | Teen Titans Go! | Stickier Situation, A
 6:15 pm | Teen Titans Go! | TV Knight 8
 6:30 pm | Teen Titans Go! | The Dignity of Teeth
 7:00 pm | Scooby-Doo! and Guess Who? | The Legend of the Gold Microphone!
 7:30 pm | Scooby-Doo! and Guess Who? | Total Jeopardy!