# 2023-03-06
 6:00 am | Amazing World of Gumball | The Matchmaker; The Deal
 6:30 am | Amazing World of Gumball | Line; The Stink; the Fly
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Rockin' Robin; Grass Is Greener; Scaredy-Bat
 9:00 am | Sesame Street Mecha Builders | Roll Chickens Rollramp up; Up; and Away
 9:30 am | Thomas & Friends: All Engines Go | Backwards Day; What's in a Name? Mistake Is What It Takes; a This Was Unexpected
10:00 am | Thomas & Friends: All Engines Go | Chasing Rainbows; Roller Coasting
10:30 am | Bugs Bunny Builders | Beach Battle; Rock On
11:00 am | New Looney Tunes | For the Love of Fraud Not so Special Delivery; Squeak Show Rodeo Bugs
11:30 am | New Looney Tunes | One Carroter in Search of an Artist Duck Days of Summer; The Slugsworthy's Mega-Mansion Wile E.'s Walnuts
12:00 pm | We Baby Bears | Bears in the Dark; The Magical Box
12:30 pm | Craig of the Creek | Stink Bomb; Into the Overpast
 1:00 pm | Craig of the Creek | The Summer Wish; Time Capsule
 1:30 pm | Teen Titans Go! | Accept the Next Proposition You Hear Squash & Stretch
 2:00 pm | Teen Titans Go! | The Fourth Wall; Garage Sale
 2:30 pm | Amazing World of Gumball | Styles; The Astological; the Wig
 3:00 pm | Amazing World of Gumball | The Traffic; The Grades
 3:30 pm | Amazing World of Gumball | The Vase; The Diet
 4:00 pm | Amazing World of Gumball | The Ollie; The Ex
 4:30 pm | Amazing World of Gumball | The Potato; The Weirdo
 5:00 pm | Craig of the Creek | The Ground Is Lava! ; Cousin of the Creek
 5:30 pm | Teen Titans Go! | Teen Titans Action
 6:00 pm | Teen Titans Go! | Stickier Situation; A; Hair Bun
 6:15 pm | Teen Titans Go! | TV Knight 8
 6:30 pm | Teen Titans Go! | Dignity of Teeth; Croissant; Glasses; Orange Door; Vegetrouble
 7:00 pm | Scooby-Doo! and Guess Who? | The Legend Of The Gold Microphone!
 7:30 pm | Scooby-Doo! and Guess Who? | Total Jeopardy!