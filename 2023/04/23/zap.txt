# 2023-04-23
 6:00 am | Amazing World of Gumball | The Knights; The Colossus
 6:30 am | Amazing World of Gumball | The Fridge; The Remote
 7:00 am | Amazing World of Gumball | The Flower; The Banana
 7:30 am | Amazing World of Gumball | The Phone; The Job
 8:00 am | Amazing World of Gumball | The Words; The Apology
 8:30 am | Amazing World of Gumball | The Watch; The Bet
 9:00 am | Craig of the Creek | The Jump Off
 9:30 am | Craig of the Creek | The Cursed Word
10:00 am | Teen Titans Go! | Legendary Sandwich
10:30 am | Teen Titans Go! | Driver's Ed
11:00 am | Teen Titans Go! | Double Trouble
11:30 am | Teen Titans Go! | Dude Relax
12:00 pm | Teen Titans Go! | Ghost Boy
12:30 pm | Teen Titans Go! | Hey Pizza!
 1:00 pm | Teen Titans Go! | Girls' Night Out
 1:30 pm | Teen Titans Go! | Super Robin
 2:00 pm | Teen Titans Go! | Parasite
 2:30 pm | Teen Titans Go! | Meatball Party
 3:00 pm | Teen Titans Go! | Terra-ized
 3:30 pm | Teen Titans Go! | Burger vs. Burrito
 4:00 pm | Teen Titans Go! | Colors of Raven
 4:30 pm | Teen Titans Go! | Books
 5:00 pm | Teen Titans Go! | Starfire the Terrible
 5:30 pm | Superman III | 