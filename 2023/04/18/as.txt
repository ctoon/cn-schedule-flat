# 2023-04-18
 8:00 pm | King Of the Hill | Good Hill Hunting
 8:30 pm | King Of the Hill | A Fire Fighting We Will Go
 9:00 pm | King Of the Hill | To Spank with Love
 9:30 pm | Bob's Burgers | Sliding Bobs
10:00 pm | Bob's Burgers | The Land Ship
10:30 pm | American Dad! | The Unincludeds
11:00 pm | American Dad! | The Dentist's Wife
11:30 pm | American Dad! | Widow's Pique
12:00 am | Rick and Morty | Ricksy Business
12:30 am | Rick and Morty | A Rickle in Time
 1:00 am | The Boondocks | Riley Wuz Here
 1:30 am | Mike Tyson Mysteries | Broken Wings
 1:45 am | Mike Tyson Mysteries | Ring of Fire
 2:00 am | Aqua TV Show Show | Piranha Germs
 2:15 am | Aqua TV Show Show | Spacecadeuce
 2:30 am | Robot Chicken | Immortal
 2:45 am | Robot Chicken | G.I. Jogurt
 3:00 am | Rick and Morty | Ricksy Business
 3:30 am | Rick and Morty | A Rickle in Time
 4:00 am | Futurama | I Dated a Robot
 4:30 am | Futurama | Anthology of Interest II
 5:00 am | Bob's Burgers | Sliding Bobs
 5:30 am | Bob's Burgers | The Land Ship