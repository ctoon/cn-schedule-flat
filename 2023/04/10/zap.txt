# 2023-04-10
 6:00 am | Amazing World of Gumball | The Sorcerer
 6:30 am | Amazing World of Gumball | The Console
 7:00 am | Teen Titans Go! | TV Knight 6
 7:30 am | Meet the Batwheels | Secret Origin of the Batwheels
 8:15 am | Thomas & Friends: All Engines Go | Hot Air Percy
 8:30 am | Cocomelon | John Jacob Jingleheimer Schmidt
 8:45 am | Bugs Bunny Builders | Ice Creamed
 9:00 am | New Looney Tunes | Cold Medal Wabbit; No Duck Is an Island
 9:30 am | New Looney Tunes | My Kingdom for a Duck; Finders Keepers, Losers Sweepers
10:00 am | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
11:00 am | Craig of the Creek | Memories of Bobby
11:30 am | Craig of the Creek | Jacob of the Creek
12:00 pm | Total DramaRama | There Are No Hoppy Endings
12:30 pm | Total DramaRama | Too Much of a Goo'd Thing
 1:00 pm | Teen Titans Go! | Space House
 2:00 pm | Amazing World of Gumball | The Ollie
 2:30 pm | Amazing World of Gumball | The Potato
 3:00 pm | We Baby Bears | Back to Our Regular Time Period
 3:30 pm | Amazing World of Gumball | The Sorcerer
 4:00 pm | Amazing World of Gumball | The Console
 4:30 pm | Amazing World of Gumball | The Outside
 5:00 pm | Craig of the Creek | The Cursed Word
 5:30 pm | Teen Titans Go! | Teen Titans Vroom
 6:00 pm | Teen Titans Go! | That's What's Up
 7:00 pm | Scooby-Doo! and Guess Who? | Cher, Scooby And The Sargasso Sea!
 7:30 pm | Scooby-Doo! and Guess Who? | The Lost Mines of Kilimanjaro!