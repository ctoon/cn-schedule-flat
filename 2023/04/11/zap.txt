# 2023-04-11
 6:00 am | Amazing World of Gumball | The Cycle
 6:30 am | Amazing World of Gumball | The Best
 7:00 am | Teen Titans Go! | The Score
 7:30 am | Cocomelon | Funny Face Song
 7:45 am | Meet the Batwheels | Bam's Upgrade
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Carly's Screechy Squeak
 8:30 am | Cocomelon | Row Row Row Your Boat
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | New Looney Tunes | Daffy Crackpot; Fashion Viktor
 9:30 am | New Looney Tunes | Sam the Roughrider; Fool's Gold
10:00 am | Teen Titans Go! | Jump City Rock
10:30 am | Teen Titans Go! | 365!
11:00 am | Craig of the Creek | Return of the Honeysuckle Rangers
11:30 am | Craig of the Creek | Kelsey the Elder
12:00 pm | Total DramaRama | The Price of Advice
12:30 pm | Total DramaRama | Mother of All Cards
 1:00 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 1:30 pm | Teen Titans Go! | EEbows
 2:00 pm | Amazing World of Gumball | The Copycats
 2:30 pm | Amazing World of Gumball | The Catfish
 3:00 pm | We Baby Bears | Bath on the Nile
 3:30 pm | Amazing World of Gumball | The Cycle
 4:00 pm | Amazing World of Gumball | The Best
 4:30 pm | Amazing World of Gumball | The Worst
 5:00 pm | Craig of the Creek | Craiggy & the Slime Factory
 5:30 pm | Teen Titans Go! | Magic Man
 6:00 pm | Teen Titans Go! | The Titans Go Casual
 6:30 pm | Teen Titans Go! | Rain on Your Wedding Day
 7:00 pm | Scooby-Doo! and Guess Who? | The Legend of the Gold Microphone!
 7:30 pm | Scooby-Doo! and Guess Who? | Total Jeopardy!