# 2023-04-07
 6:00 am | Amazing World of Gumball | The Astological; The Box
 6:30 am | Amazing World of Gumball | Traffic; The Matchmaker; the Shape Shift
 7:00 am | Teen Titans Go! | Easter Creeps; The Teen Titans Go Easter Holiday Classic
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Keep Calm and Roll on Fly
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | Mecha Builders Pull Together/Lift up and Lift Off
 8:45 am | Bugs Bunny Builders | Splash Zone
 9:00 am | New Looney Tunes | Lola Rider Daredevil Duck; Vampire Me Love Tad Tucker Workout; Eye Test; Carrot; Newspaper
 9:30 am | New Looney Tunes | Planet of the Bigfoots Part 1 Planet of the Bigfoots Part 2; Canadian Bacon Bugs Bunny Saves the Universe Boogie Button
10:00 am | Teen Titans Go! | TV Knight 4; Royal Jelly
10:30 am | Teen Titans Go! | Lil' Dimples; Strength Of A Grown Man
11:00 am | Craig of the Creek | The Jump off; The Cow-Boy and Marie
11:30 am | Craig of the Creek | Cursed Word; The Craiggy & the Slime Factory
12:00 pm | Total DramaRama | Melter Skelter Driving Miss Crazy
12:30 pm | Total DramaRama | The Never Gwending Story; Weiner Takes All
 1:00 pm | The Peanuts Movie | 
 2:45 pm | Teen Titans Go! | Easter Annihilation
 3:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 3:30 pm | Amazing World of Gumball | The Astological; The Box
 4:00 pm | Amazing World of Gumball | The Traffic; The Matchmaker
 4:30 pm | Amazing World of Gumball | The Xxxxx; The Grades
 5:00 pm | Craig of the Creek | A Tattle Tale
 5:15 pm | Craig of the Creek | Craiggy & the Slime Factory
 5:30 pm | Craig of the Creek | The Jump off; The Cow-Boy and Marie
 6:00 pm | Craig of the Creek | A Cursed Word; The Tattle Tale
 6:30 pm | Teen Titans Go! | Feed Me; Easter Annihilation
 7:00 pm | Scooby-Doo! and Guess Who? | The Tao Of Scoob!
 7:30 pm | Scooby-Doo! and Guess Who? | Returning Of The Key Ring!