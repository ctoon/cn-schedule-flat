# 2023-04-17
 6:00 am | Amazing World Of Gumball | The Possession; The Transformation
 6:30 am | Amazing World of Gumball | Understanding; The Decisions; the Future Tense
 7:00 am | Teen Titans Go! | Just a Little Patience...Yeah...Yeah; Hafo Safo; Bee a Bee Dance
 7:30 am | Bugs Bunny Builders | Blast Off
 7:45 am | Bugs Bunny Builders | K-: Space Puppy Boogie Button
 8:00 am | Bugs Bunny Builders | Cousin Billy
 8:15 am | Bugs Bunny Builders | Cheddar Days Feel the Hush
 8:30 am | Bugs Bunny Builders | Taz Recycle
 8:45 am | Bugs Bunny Builders | Smash House Don't Be Fooled by His Size
 9:00 am | New Looney Tunes | To Be the Flea; You Gotta Beat the Flea Wild Blue Blunder; the Love Makes Me Daffy Genghis Cal
 9:30 am | New Looney Tunes | Thomas Fuddison Hiccups and Downs; You're Kiln Me Better Lake Than Never
10:00 am | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
10:30 am | Teen Titans Go! | The Real Art; Mug
11:00 am | Craig of the Creek | Creek Daycare; Ancients of the Creek
11:30 am | Craig of the Creek | Sugar Smugglers; Mortimor to the Rescue
12:00 pm | Total DramaRama | An Egg-Stremely Bad Idea; Beth and the Beanstalk
12:30 pm | Total DramaRama | Exercising the Demons; Pinata Regatta
 1:00 pm | Teen Titans Go! | Go! ; Porch Pirates
 1:30 pm | Teen Titans Go! | A Finding Aquaman; Sticky Situation
 2:00 pm | Amazing World of Gumball | The Potion; The Revolt
 2:30 pm | Amazing World of Gumball | The Spinoffs; The Mess
 3:00 pm | We Baby Bears | A Lighthouse; Real Crayon
 3:30 pm | Amazing World Of Gumball | The Possession; The Transformation
 4:00 pm | Amazing World of Gumball | The Understanding; The Decisions
 4:30 pm | Amazing World Of Gumball | The Ad; The Ghouls
 5:00 pm | Craig of the Creek | The Crisis at Elder Rock Time Capsule
 5:30 pm | Teen Titans Go! | Thumb War; Lucky Stars
 6:00 pm | Teen Titans Go! | Toddler Titans...Yay! ; Various Modes of Transportation
 6:30 pm | Teen Titans Go! | Huggbees; Cool Uncles
 7:00 pm | Scooby-Doo! and Guess Who? | What a Night for a Dark Knight!
 7:30 pm | Scooby-Doo! and Guess Who? | Elementary, My Dear Shaggy!