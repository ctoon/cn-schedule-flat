# 2023-04-20
 6:00 am | Amazing World of Gumball | The Mystery; The Prank
 6:30 am | Amazing World of Gumball | The Gi; The Kiss
 7:00 am | Teen Titans Go! | Huggbees
 7:30 am | Cocomelon | Yes Yes Bed Time Camping
 7:45 am | Meet the Batwheels | Rev and Let Rev
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Good as New
 8:30 am | Cocomelon | Five Senses Song
 8:45 am | Bugs Bunny Builders | Taz Recycle
 9:00 am | New Looney Tunes | Close Encounters of the Duck Kind; O.M. Genie
 9:30 am | New Looney Tunes | Brothers in Harms; Rhoda Derby
10:00 am | Teen Titans Go! | Thumb War
10:30 am | Teen Titans Go! | Toddler Titans...Yay!
11:00 am | Craig of the Creek | The End Was Here
11:30 am | Craig of the Creek | In the Key of the Creek
12:00 pm | Total DramaRama | Dream Worriers
12:30 pm | Total DramaRama | Grody to the Maximum
 1:00 pm | Teen Titans Go! | We'll Be Right Back
 1:30 pm | Teen Titans Go! | Jump City Rock
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | We Baby Bears | Sheep Bears
 3:30 pm | Amazing World of Gumball | The Mystery; The Prank
 4:00 pm | Amazing World of Gumball | The Gi; The Kiss
 4:30 pm | Amazing World of Gumball | The Party; The Refund
 5:00 pm | Total DramaRama | Total Dramarama: A Very Special Special That's Quite Special
 6:00 pm | Teen Titans Go! | I Am Chair
 6:30 pm | Teen Titans Go! | Bumgorf
 7:00 pm | Scooby-Doo! and Guess Who? | Ollie Ollie In-Come Free!
 7:30 pm | Scooby-Doo! and Guess Who? | The Cursed Cabinet of Professor Madds Markson!