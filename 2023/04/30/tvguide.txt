# 2023-04-30
 6:00 am | Teen Titans Go! | Manor and Mannerisms; Creative Geniuses
 6:30 am | Teen Titans Go! | Trans Oceanic Magical Cruise; Polly Ethylene and Tara Phthalate
 7:00 am | Teen Titans Go! | Eebows; Batman's Birthday Gift
 7:30 am | Teen Titans Go! | Doomsday Preppers; What a Boy Wonders
 8:00 am | Teen Titans Go! | Jam; Fat Cats
 8:30 am | Teen Titans Go! | Dc Pepo the Pumpkinman
 9:00 am | Teen Titans Go! | Captain Cool; Breakfast
 9:30 am | Teen Titans Go! | A Doom Patrol Thanksgiving
10:00 am | Teen Titans Go! | Control Freak; Glunkakakakah
10:30 am | Teen Titans Go! | The Holiday Story; A; Drip
11:00 am | Teen Titans Go! | Standards & Practices; Belly Math
11:30 am | Teen Titans Go! | Free Perk; Go!
12:00 pm | Teen Titans Go! | Whodundidit? ; Finding Aquaman
12:30 pm | Teen Titans Go! | Porch Pirates; Sweet Revenge
 1:00 pm | Teen Titans Go! | The Sticky Situation; A; Perfect Pitch?
 1:30 pm | Teen Titans Go! | Pool Season; Kyle
 2:00 pm | Teen Titans Go! | TV Knight 7; We'll Be Right Back
 2:30 pm | Teen Titans Go! | Jump City Rock; Natural Gas
 3:00 pm | Teen Titans Go! | The 50%; Chad; Score
 3:30 pm | Teen Titans Go! | 365! Welcome to Halloween
 4:00 pm | Teen Titans Go! | Great Holiday Escape; The Looking for Love
 4:30 pm | Teen Titans Go! | Teen Titans Action
 5:00 pm | Teen Titans Go! | A TV Knight 8; Stickier Situation
 5:30 pm | Superman IV: The Quest for Peace | 
 7:30 pm | Samurai Jack | Jack and the Three Blind Archers