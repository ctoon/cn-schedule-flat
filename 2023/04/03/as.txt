# 2023-04-03
 8:00 pm | King Of the Hill | Four Wave Intersection
 8:30 pm | King Of the Hill | Death Picks Cotton
 9:00 pm | King Of the Hill | Raise the Steaks
 9:30 pm | Bob's Burgers | Friends with Burger-Fits
10:00 pm | Bob's Burgers | Dawn of the Peck
10:30 pm | Royal Crackers | Pilot
11:00 pm | American Dad! | Introducing the Naughty Stewardesses
11:30 pm | American Dad! | I Ain't No Holodeck Boy
12:00 am | Rick and Morty | Promortyus
12:30 am | Rick and Morty | The Vat of Acid Episode
 1:00 am | The Boondocks | The Garden Party
 1:30 am | Mike Tyson Mysteries | Greece is the Word
 1:45 am | Mike Tyson Mysteries | Ogopogo!
 2:00 am | Aqua Unit Patrol Squad 1 | Allen Part 1
 2:15 am | Aqua Unit Patrol Squad 1 | Allen Part 2
 2:30 am | Robot Chicken | Executed by the State
 2:45 am | Robot Chicken | Crushed by a Steamroller on My 53rd Birthday
 3:00 am | Royal Crackers | Pilot
 3:30 am | Rick and Morty | The Vat of Acid Episode
 4:00 am | Futurama | I Second That Emotion
 4:30 am | Futurama | Brannigan, Begin Again
 5:00 am | Bob's Burgers | Friends with Burger-Fits
 5:30 am | Bob's Burgers | Dawn of the Peck