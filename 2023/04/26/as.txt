# 2023-04-26
 8:00 pm | King Of the Hill | To Kill a Ladybird
 8:30 pm | King Of the Hill | Old Glory
 9:00 pm | King Of the Hill | Rodeo Days
 9:30 pm | Bob's Burgers | Wag the Hog
10:00 pm | Bob's Burgers | The Hormone-Iums
10:30 pm | American Dad! | A Nice Night for a Drive
11:00 pm | American Dad! | Casino Normale
11:30 pm | American Dad! | Bazooka Steve
12:00 am | Rick and Morty | The Whirly Dirly Conspiracy
12:30 am | Rick and Morty | Rest and Ricklaxation
 1:00 am | The Boondocks | Shinin'
 1:30 am | Mike Tyson Mysteries | Mike Tysonland
 1:45 am | Mike Tyson Mysteries | The Gift
 2:00 am | Aqua Teen Hunger Force Forever | Hospice
 2:15 am | Aqua Teen Hunger Force Forever | The Greatest Story Ever Told
 2:30 am | Robot Chicken | Catdog on a Stick
 2:45 am | Robot Chicken | Super Guitario Center
 3:00 am | Rick and Morty | The Whirly Dirly Conspiracy
 3:30 am | Rick and Morty | Rest and Ricklaxation
 4:00 am | Futurama | Less Than Hero
 4:30 am | Futurama | The Farnsworth Parabox
 5:00 am | Bob's Burgers | Wag the Hog
 5:30 am | Bob's Burgers | The Hormone-Iums