# 2023-04-27
 8:00 pm | King Of the Hill | Hanky Panky (1)
 8:30 pm | King Of the Hill | High Anxiety (2)
 9:00 pm | King Of the Hill | Naked Ambition
 9:30 pm | Bob's Burgers | Pro Tiki/Con Tiki
10:00 pm | Bob's Burgers | Bye Bye Boo Boo
10:30 pm | American Dad! | Camp Campawanda
11:00 pm | American Dad! | Julia Rogerts
11:30 pm | American Dad! | The Life and Times of Stan Smith
12:00 am | Rick and Morty | The Ricklantis Mixup
12:30 am | Rick and Morty | Morty's Mind Blowers
 1:00 am | The Boondocks | Ballin'
 1:30 am | Mike Tyson Mysteries | Real Bitches of Newport Beach
 1:45 am | Mike Tyson Mysteries | The Pigeon Has Come Home to Roost
 2:00 am | Aqua Unit Patrol Squad 1 | Allen Part 1
 2:15 am | Aqua Unit Patrol Squad 1 | Allen Part 2
 2:30 am | Robot Chicken | Noidstrom Rack
 2:45 am | Robot Chicken | Stone Cold Steve Cold Stone
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Rick and Morty | Morty's Mind Blowers
 4:00 am | Futurama | Teenage Mutant Leela's Hurdles
 4:30 am | Futurama | The Why of Fry
 5:00 am | Bob's Burgers | Pro Tiki/Con Tiki
 5:30 am | Bob's Burgers | Bye Bye Boo Boo