# 2023-04-21
 8:00 pm | King Of the Hill | Love Hurts and So Does Art
 8:30 pm | King Of the Hill | Hank's Cowboy Movie
 9:00 pm | American Dad! | Gift Me Liberty
 9:30 pm | American Dad! | Next of Pin
10:00 pm | American Dad! | Standard Deviation
10:30 pm | Royal Crackers | Business Mom
11:00 pm | Rick and Morty | The Ricks Must Be Crazy
11:30 pm | Rick and Morty | Big Trouble in Little Sanchez
12:00 am | Aqua Teen | Aqua Teen Forever: Plantasm
 1:45 am | Aqua Unit Patrol Squad 1 | Lasagna
 2:00 am | Royal Crackers | Business Mom
 2:30 am | Rick and Morty | Big Trouble in Little Sanchez
 3:00 am | Futurama | Godfellas
 3:30 am | Futurama | Future Stock
 4:00 am | Futurama | A Leela of Her Own
 4:30 am | Futurama | 30% Iron Chef
 5:00 am | King Of the Hill | Dog Dale Afternoon
 5:30 am | King Of the Hill | Death and Texas