# 2023-04-12
 6:00 am | Amazing World of Gumball | The Line
 6:30 am | Amazing World of Gumball | The Singing
 7:00 am | Teen Titans Go! | Teen Titans Vroom
 7:30 am | Cocomelon | Basketball Song
 7:45 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:00 am | Cocomelon | Numbers Song With Little Chicks
 8:15 am | Thomas & Friends: All Engines Go | Shake, Rattle, & Bruno
 8:30 am | Cocomelon | Guess the Animal Song
 8:45 am | Bugs Bunny Builders | Dino Fright
 9:00 am | New Looney Tunes | Bonjour, DarkBat; Renaissance Fair-Thee Well
 9:30 am | New Looney Tunes | CinderPorker
10:00 am | Teen Titans Go! | That's What's Up
11:00 am | Craig of the Creek | Sour Candy Trials
11:30 am | Craig of the Creek | Fort Williams
12:00 pm | Total DramaRama | Duncan Disorderly
12:30 pm | Total DramaRama | Camping Is in Tents
 1:00 pm | Teen Titans Go! | Batman's Birthday Gift
 1:30 pm | Teen Titans Go! | What a Boy Wonders
 2:00 pm | Amazing World of Gumball | The List
 2:30 pm | Amazing World of Gumball | The Deal
 3:00 pm | We Baby Bears | Bubble Fields
 3:30 pm | Amazing World of Gumball | The Line
 4:00 pm | Amazing World of Gumball | The Singing
 4:30 pm | Amazing World of Gumball | The Puppets
 5:00 pm | Craig of the Creek | A Tattle Tale
 5:30 pm | Teen Titans Go! | Teen Titans Roar!
 6:00 pm | Teen Titans Go! | Egg Hunt
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 7:00 pm | Scooby-Doo! and Guess Who? | Scooby-Doo and the Sky Town Cool School!
 7:30 pm | Scooby-Doo! and Guess Who? | Falling Star Man!