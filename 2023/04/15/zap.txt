# 2023-04-15
 6:00 am | Amazing World of Gumball | The Silence
 6:30 am | Amazing World of Gumball | The Future
 7:00 am | Amazing World of Gumball | The Wish
 7:30 am | Amazing World of Gumball | The Factory
 8:00 am | Teen Titans Go! | TV Knight 8
 8:30 am | Teen Titans Go! | Super Hero Summer Camp
 9:30 am | Looney Tunes Cartoons | Bone Head; Relax
10:00 am | Total DramaRama | Total Dramarama: A Very Special Special That's Quite Special
11:00 am | The Smurfs | 
 1:00 pm | Craig of the Creek | The Cow-Boy and Marie
 1:30 pm | Craig of the Creek | Craiggy & the Slime Factory
 2:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 2:30 pm | Teen Titans Go! | Bucket List
 3:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro Part 1
 3:30 pm | Teen Titans Go! | TV Knight 6
 4:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 4:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 5:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 5:30 pm | Amazing World of Gumball | The Third; The Debt
 6:00 pm | The Smurfs 2 | 