# 2023-04-05
 8:00 pm | King Of the Hill | Doggone Crazy
 8:30 pm | King Of the Hill | Trans-Fascism
 9:00 pm | King Of the Hill | Three Men And A Bastard
 9:30 pm | Bob's Burgers | Tina Tailor Soldier Spy
10:00 pm | Bob's Burgers | Midday Run
10:30 pm | American Dad! | Rubberneckers
11:00 pm | American Dad! | Permanent Record Wrecker
11:30 pm | American Dad! | News Glance with Genevieve Vavance
12:00 am | Royal Crackers | Factory 37
12:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 1:00 am | The Boondocks | Guess Hoe's Coming to Dinner
 1:30 am | Mike Tyson Mysteries | Losin' it
 1:45 am | Mike Tyson Mysteries | Yves Klein Blues
 2:00 am | Aqua Unit Patrol Squad 1 | The Creditor
 2:15 am | Aqua Unit Patrol Squad 1 | Vampirus
 2:30 am | Robot Chicken | Hurtled from a Helicopter into a Speeding Train
 2:45 am | Robot Chicken | Disemboweled by an Orphan
 3:00 am | Royal Crackers | Factory 37
 3:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 4:00 am | Futurama | Put Your Head on My Shoulder
 4:30 am | Futurama | The Lesser of Two Evils
 5:00 am | Bob's Burgers | Tina Tailor Soldier Spy
 5:30 am | Bob's Burgers | Midday Run