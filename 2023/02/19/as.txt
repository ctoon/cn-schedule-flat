# 2023-02-19
 8:00 pm | Bob's Burgers | Boywatch
 8:30 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 9:00 pm | Futurama | A Farewell to Arms
 9:30 pm | Futurama | Decision 3012
10:00 pm | American Dad! | Hayley Was a Girl Scout?
10:30 pm | American Dad! | Please Please Jeff
11:00 pm | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
11:30 pm | Rick and Morty | The Old Man and the Seat
12:00 am | YOLO: Silver Destiny | The Parents Episode
12:15 am | YOLO: Silver Destiny | Chaise and the City
12:30 am | Aqua Teen | The Creditor
12:45 am | Aqua Teen | Vampirus
 1:00 am | King Star King | KING STAR KING!/!/!
 1:30 am | American Dad! | Hayley Was a Girl Scout?
 2:00 am | American Dad! | Please Please Jeff
 2:30 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:00 am | Rick and Morty | The Old Man and the Seat
 3:30 am | YOLO: Silver Destiny | The Parents Episode
 3:45 am | YOLO: Silver Destiny | Chaise and the City
 4:00 am | Bob's Burgers | Boywatch
 4:30 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
 5:00 am | King Of the Hill | Hank's Cowboy Movie
 5:30 am | King Of the Hill | Dog Dale Afternoon