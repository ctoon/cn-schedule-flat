# 2023-02-13
 6:00 am | Amazing World of Gumball | The Kiss; The Romantic
 6:30 am | Amazing World of Gumball | Date; The Girlfriend; the Calling All Batwheels
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Knight Shift; The Scaredy-Bat; Riddle Me This
 9:00 am | Mecha Builders | Whatsacallit of Treetop Woods; Thenat in the Dark!
 9:30 am | Thomas & Friends: All Engines Go | Valentine's Heart; Very Percy Valentine's Day; a Chugga-Chugga; Snooze Snooze
10:00 am | Thomas & Friends: All Engines Go | Hide and Surprise! ; Fast Friends
10:30 am | Bugs Bunny Builders | Hard Hat Time; Soup up; Snow Cap
11:00 am | New Looney Tunes | Riverboat Rabbit Dorlock; P. I.; Inside Bugs; the Sun Valley Freeze
11:30 am | New Looney Tunes | To Be the Flea; You Gotta Beat the Flea Wild Blue Blunder; the St. Bugs and the Dragon Leaf It Alone
12:00 pm | We Baby Bears | Temple Bears; Doll's House
12:30 pm | Craig of the Creek | Grandma Smugglers; Galactic Goodbyes
 1:00 pm | Craig of the Creek | Champion's Hike; The Back to Cool
 1:30 pm | Teen Titans Go! | Go! ; TV Knight 7
 2:00 pm | Teen Titans Go! | The We'll Be Right Back; Score
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Amazing World of Gumball | The Mystery; The Prank
 3:30 pm | Amazing World of Gumball | The GI; The Kiss
 4:00 pm | Amazing World of Gumball | The Party; The Refund
 4:30 pm | Amazing World of Gumball | The Robot; The Picnic
 5:00 pm | Craig of the Creek | The Final Book; Under the Overpass
 5:30 pm | Teen Titans Go! | Driver's Ed; Dog Hand
 6:00 pm | Teen Titans Go! | Power of Shrimps; The Looking for Love
 6:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 7:00 pm | Scooby-Doo! and Guess Who? | Quit Clowning!
 7:30 pm | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!