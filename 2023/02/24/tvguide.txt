# 2023-02-24
 6:00 am | Amazing World of Gumball | The Kids; The Fan
 6:30 am | Amazing World of Gumball | Coach; The Joy; the Don't Be Fooled by His Size
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:15 am | Meet the Batwheels | Secret Origin of the Batwheels
 9:00 am | Mecha Builders | Get in Gearfree-Wheelin' Ferris Wheel
 9:30 am | Thomas & Friends: All Engines Go | Blame It on Beresford; Quiet Delivery; a; Panda
10:00 am | Thomas & Friends: All Engines Go | Thomas in Charge; Kana Goes Slow
10:30 am | Bugs Bunny Builders | Big Feet; Buzz in Buckle Up
11:00 am | New Looney Tunes | The Daffy the Stowaway Superscooter 3000; Survivalist of the Fittest Imposter
11:30 am | New Looney Tunes | Hoggin' the Road Timmmmmmbugs; Bugs Over Par Fast Feud
12:00 pm | We Baby Bears | Meat House; Bug City Sleuths
12:30 pm | Craig of the Creek | Dibs Court; The Takeout Mission
 1:00 pm | Craig of the Creek | The Dinner at the Creek; Great Fossil Rush
 1:30 pm | Teen Titans Go! | Brain Food; In and Out
 2:00 pm | Teen Titans Go! | Little Buddies; Missing
 2:30 pm | Amazing World of Gumball | The Butterfly; The Question
 3:00 pm | Amazing World of Gumball | The Oracle; The Safety
 3:30 pm | Amazing World of Gumball | The Friend; The Saint
 4:00 pm | Amazing World of Gumball | The Spoiler; The Society
 4:30 pm | Amazing World of Gumball | The Nobody; The Countdown
 5:00 pm | Craig of the Creek | Big Pinchy; Bring Out Your Beast
 5:30 pm | NBA All Star Slam Dunk Contest | 
 5:45 pm | Teen Titans Go! | Knowledge
 6:00 pm | Teen Titans Go! | Teen Titans Action
 6:30 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 7:00 pm | Scooby-Doo! and Guess Who? | Scooby On Ice!
 7:30 pm | Scooby-Doo! and Guess Who? | Caveman On The Half Pipe!