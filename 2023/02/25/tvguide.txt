# 2023-02-25
 6:00 am | Amazing World of Gumball | The Neighbor; The Cringe
 6:30 am | Amazing World of Gumball | The Pact; The Potion
 7:00 am | Amazing World of Gumball | The Faith; The Spinoffs
 7:30 am | Amazing World of Gumball | The Candidate; The Transformation
 8:00 am | Amazing World of Gumball | The Anybody; The Understanding
 8:30 am | Amazing World of Gumball | The Ad; The Shippening
 9:00 am | We Baby Bears | Who Crashed the Rv; Doll's House
 9:30 am | Amazing World of Gumball | The Slip; The Brain
10:00 am | Total DramaRama | From Badge to Worse; Simons Are Forever
10:30 am | Total DramaRama | Snow Way out; Mother of All Cards
11:00 am | Total DramaRama | All Up In Your Drill; Toys Will Be Toys
11:30 am | Total DramaRama | Camping Is In Tents
12:00 pm | Craig of the Creek | Winter Break
12:30 pm | Craig of the Creek | Alternate Creekiverse; Welcome to Creek Street
 1:00 pm | Craig of the Creek | Snow Day; Fan or Foe
 1:30 pm | Craig of the Creek | Snow Place Like Home; New Jersey
 2:00 pm | NBA All Star Slam Dunk Contest | 
 2:15 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 2:45 pm | Teen Titans Go! | The 170 Bbrae Cruel Giggling Ghoul
 3:15 pm | Teen Titans Go! | Employee of the Month Redux
 3:30 pm | Teen Titans Go! | Teen Titans Action
 4:00 pm | Amazing World of Gumball | The Stink; The Drama
 4:30 pm | Amazing World of Gumball | The Awareness; The Buddy
 5:00 pm | Amazing World of Gumball | The Parents; The Master
 5:30 pm | Amazing World of Gumball | The Founder; The Silence
 6:00 pm | Scooby-Doo! Legend of the Phantosaur | 