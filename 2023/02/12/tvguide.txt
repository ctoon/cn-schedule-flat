# 2023-02-12
 6:00 am | Amazing World of Gumball | The Comic; The Awkwardness
 6:30 am | Amazing World of Gumball | The Origins; The Origins Part 2
 7:00 am | Amazing World of Gumball | The Nest; The; Blame
 7:30 am | Amazing World of Gumball | The Party; The Love
 8:00 am | Amazing World of Gumball | The Shell; The Matchmaker
 8:30 am | Amazing World of Gumball | The Misunderstandings; The Heart
 9:00 am | Craig of the Creek | The Time Capsule; The; Bike Thief
 9:30 am | Craig of the Creek | Fire & Ice; Breaking the Ice
10:00 am | Teen Titans Go! | Looking for Love; How 'bout Some Effort
10:30 am | Teen Titans Go! | Snuggle Time; Booty Scooty
11:00 am | Teen Titans Go! | Who's Laughing Now; Oh Yeah!
11:30 am | Teen Titans Go! | Legendary Sandwich Matched
12:00 pm | Codename: Kids Next Door | Operation: L.O.V.E.; Operation: C.O.U.C.H.
12:30 pm | Codename: Kids Next Door | Operation: G.I.R.L.F.R.I.E.N.D.
 1:00 pm | Teen Titans | Date with Destiny
 1:30 pm | Teen Titans | Betrothed
 2:00 pm | Adventure Time | Prisoners of Love Dream of Love
 2:30 pm | Adventure Time | Love Games; Cherry Cream Soda
 3:00 pm | Steven Universe | Gem Harvest; Back to the Kindergarten
 3:30 pm | Steven Universe | Reunited
 4:00 pm | Amazing World of Gumball | The Misunderstandings; The; Compilation
 4:30 pm | Amazing World of Gumball | The Roots; The Stories
 5:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 5:30 pm | Amazing World of Gumball | The Date; The Girlfriend
 6:00 pm | Ella Enchanted | 