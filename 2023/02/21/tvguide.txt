# 2023-02-21
 6:00 am | Amazing World of Gumball | The Mirror; The Burden
 6:30 am | Amazing World of Gumball | Bros; The Man; the Buckle Up
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Up in the Air; Bibi's Do-Over Together We Can
 9:00 am | Mecha Builders | Silly Hat Catastrophe! Zee in a Bottle
 9:30 am | Thomas & Friends: All Engines Go | Christmas Mountain; Big Skunk Funk; the Fruit & Vegetables
10:00 am | Thomas & Friends: All Engines Go | Whiteout! ; Off the Rails
10:30 am | Bugs Bunny Builders | Game Time; Play Day Don't Be Fooled by His Size
11:00 am | New Looney Tunes | Love Is in the Hare Valentine's Dayffy; Raising Your Spirits Dust Bugster
11:30 am | New Looney Tunes | Bigs Bunny Wahder; Wahder; Everywhere; Computer Bugs Oils Well That Ends Well
12:00 pm | We Baby Bears | A Panda's Family; Tale of Two Ice Bears
12:30 pm | Craig of the Creek | Big Pinchy; Bring Out Your Beast
 1:00 pm | Craig of the Creek | Lost in the Sewer; The Kid From 3030
 1:30 pm | Teen Titans Go! | Pirates; I See You
 2:00 pm | Teen Titans Go! | Brian/Nature
 2:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 3:00 pm | Amazing World of Gumball | The Internet; The Plan
 3:30 pm | Amazing World of Gumball | The World; The Finale
 4:00 pm | Amazing World of Gumball | The Kids; The Fan
 4:30 pm | Amazing World of Gumball | The Coach; The Joy
 5:00 pm | Craig of the Creek | Secret Book Club; Deep Creek Salvage
 5:30 pm | Teen Titans Go! | Birds; Be Mine
 6:00 pm | Teen Titans Go! | Brain Food; In and Out
 6:30 pm | Teen Titans Go! | Little Buddies; Missing
 7:00 pm | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital Of Dr. Phineas Phrag!
 7:30 pm | Scooby-Doo! and Guess Who? | The Phantom, The Talking Dog And The Hot Hot Hot Sauce!