# 2023-11-25
 6:00 am | Amazing World of Gumball | The Rival
 6:15 am | Amazing World of Gumball | The Lady
 6:30 am | Amazing World of Gumball | The Sucker
 6:45 am | Amazing World of Gumball | The Vegging
 7:00 am | Amazing World of Gumball | The One
 7:15 am | Amazing World of Gumball | The Father
 7:30 am | Amazing World of Gumball | The Cringe
 7:45 am | Amazing World of Gumball | The Cage
 8:00 am | Amazing World of Gumball | The Faith
 8:15 am | Amazing World of Gumball | The Candidate
 8:30 am | Amazing World of Gumball | The Anybody
 8:45 am | Amazing World of Gumball | The Pact
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
 9:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Swoony Swanstress
 9:30 am | Tiny Toons Looniversity | General HOGspital
10:00 am | Teen Titans Go! | Hey Pizza!
10:15 am | Teen Titans Go! | Gorilla
10:30 am | Teen Titans Go! | Girls' Night Out
10:45 am | Teen Titans Go! | You're Fired
11:00 am | Teen Titans Go! | Super Robin
11:15 am | Teen Titans Go! | Tower Power
11:30 am | The Looney Tunes Show | Year of the Duck
12:00 pm | The Looney Tunes Show | That's My Baby
12:30 pm | Tiny Toons Looniversity | Extra, So Extra
 1:00 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Turnip Tricksters
 1:15 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Swoony Swanstress
 1:30 pm | Amazing World of Gumball | The Skull
 1:45 pm | Amazing World of Gumball | The Bet
 2:00 pm | Amazing World of Gumball | The Future
 2:15 pm | Amazing World of Gumball | The Wish
 2:30 pm | Amazing World of Gumball | The Factory
 2:45 pm | Amazing World of Gumball | The Agent
 3:00 pm | Scooby-Doo! and the Monster of Mexico | 
 4:30 pm | Amazing World of Gumball | The Master
 4:45 pm | Amazing World of Gumball | The Silence