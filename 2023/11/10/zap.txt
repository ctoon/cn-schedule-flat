# 2023-11-10
 6:00 am | Amazing World of Gumball | The Tag
 6:15 am | Amazing World of Gumball | The Storm
 6:30 am | Amazing World of Gumball | The Lesson
 6:45 am | Amazing World of Gumball | The Game
 7:00 am | Amazing World of Gumball | The Limit
 7:15 am | Amazing World of Gumball | The Voice
 7:30 am | Cocomelon | The Hiccup Song
 7:45 am | Meet the Batwheels | Batty Body Swap
 8:00 am | Cocomelon | Are You Sleeping (Brother John)?
 8:15 am | Mecha Builders | Special Delivery; Super Duper Sled Slide
 8:45 am | Bugs Bunny Builders | Snow Cap
 9:00 am | The Looney Tunes Show | The Float
 9:30 am | The Looney Tunes Show | The Shelf
10:00 am | Craig of the Creek | Kelsey Quest
10:15 am | Craig of the Creek | JPony
10:30 am | Craig of the Creek | Ace of Squares
10:45 am | Craig of the Creek | Doorway to Helen
11:00 am | Amazing World of Gumball | The Password
11:15 am | Amazing World of Gumball | The Procrastinators
11:30 am | Amazing World of Gumball | The Shell
11:45 am | Amazing World of Gumball | The Burden
12:00 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
12:30 pm | Clarence | A Pretty Great Day With a Girl
12:45 pm | Clarence | Money Broom Wizard
 1:00 pm | Clarence | Lost in the Supermarket
 1:15 pm | Clarence | Clarence's Millions
 1:30 pm | Craig of the Creek | Dog Decider
 1:45 pm | Craig of the Creek | Bring Out Your Beast
 2:00 pm | Teen Titans Go! | A Little Help Please
 2:15 pm | Teen Titans Go! | Marv Wolfman and George Pérez
 2:30 pm | Teen Titans Go! | Cy & Beasty
 2:45 pm | Teen Titans Go! | T Is for Titans
 3:00 pm | Amazing World of Gumball | The Bros
 3:15 pm | Amazing World of Gumball | The Mirror
 3:30 pm | Amazing World of Gumball | The Man
 3:45 pm | Amazing World of Gumball | The Pizza
 4:00 pm | Amazing World of Gumball | The Lie
 4:15 pm | Amazing World of Gumball | The Butterfly
 4:30 pm | Amazing World of Gumball | The Question
 4:45 pm | Amazing World of Gumball | The Saint