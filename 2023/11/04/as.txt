# 2023-11-04
 5:00 pm | King of the Hill | Flush with Power
 5:30 pm | King of the Hill | Transnational Amusements Presents: Peggy's Magic Sex Feet
 6:00 pm | King of the Hill | Peggy's Fan Fair
 6:30 pm | King of the Hill | The Perils of Polling
 7:00 pm | Bob's Burgers | Moody Foodie
 7:30 pm | Bob's Burgers | Bad Tina
 8:00 pm | Bob's Burgers | Beefsquatch
 8:30 pm | Bob's Burgers | Ear-sy Rider
 9:00 pm | American Dad! | She Swill Survive
 9:30 pm | American Dad! | Rubberneckers
10:00 pm | American Dad! | Great Space Roaster
10:30 pm | American Dad! | News Glance with Genevieve Vavance
11:00 pm | Rick and Morty | Big Trouble in Little Sanchez
11:30 pm | Rick and Morty | JuRicksic Mort
12:00 am | Attack on Titan | THE FINAL CHAPTERS Special 1
 1:30 am | Attack on Titan | THE FINAL CHAPTERS Special 1
 1:30 am | Attack on Titan | THE FINAL CHAPTERS Special 1
 3:00 am | Unicorn: Warriors Eternal | The Awakening
 3:30 am | The Venture Brothers | The Terminus Mandate
 4:00 am | Futurama | The Mutants Are Revolting
 4:30 am | Futurama | Neutopia