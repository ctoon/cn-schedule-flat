# 2023-11-04
 6:00 am | Amazing World of Gumball | The Signature
 6:15 am | Amazing World of Gumball | The Check
 6:30 am | Amazing World of Gumball | The Pest
 6:45 am | Amazing World of Gumball | The Sale
 7:00 am | Amazing World of Gumball | The Gift
 7:15 am | Amazing World of Gumball | The Parking
 7:30 am | Amazing World of Gumball | The Routine
 7:45 am | Amazing World of Gumball | The Upgrade
 8:00 am | Amazing World of Gumball | The Comic
 8:15 am | Amazing World of Gumball | The Romantic
 8:30 am | Amazing World of Gumball | The Uploads
 8:45 am | Amazing World of Gumball | The Apprentice
 9:00 am | Tiny Toons Looniversity | The Show Must Hop On
 9:30 am | Teen Titans Go! | Glunkakakakah
 9:45 am | Teen Titans Go! | Control Freak
10:00 am | Teen Titans Go! | Standards & Practices
10:15 am | Teen Titans Go! | Belly Math
10:30 am | Teen Titans Go! | Free Perk
10:45 am | Teen Titans Go! | Go!
11:00 am | Craig of the Creek | Itch to Explore
11:15 am | Craig of the Creek | You're It
11:30 am | Craig of the Creek | Jessica Goes to the Creek
11:45 am | Craig of the Creek | The Final Book
12:00 pm | Tiny Toons Looniversity | The Show Must Hop On
12:30 pm | The Looney Tunes Show | Newspaper Thief
 1:00 pm | Teen Titans Go! | Finding Aquaman
 1:15 pm | Teen Titans Go! | Whodundidit?
 1:30 pm | Teen Titans Go! | Sweet Revenge
 1:45 pm | Teen Titans Go! | Porch Pirates
 2:00 pm | Amazing World of Gumball | The Hug
 2:15 pm | Amazing World of Gumball | The Wicked
 2:30 pm | Amazing World of Gumball | The Traitor
 2:45 pm | Amazing World of Gumball | The Girlfriend
 3:00 pm | Scooby-Doo! Abracadabra-Doo! | 
 4:30 pm | Amazing World of Gumball | The Stink
 4:45 pm | Amazing World of Gumball | The Awareness