# 2023-11-30
 5:00 pm | Dexter's Laboratory | Dos Boot/A Dee Dee Cartoon/Would You Like That in the Can
 5:30 pm | Ed, Edd n Eddy | Rock-A-Bye Ed/O-Ed Eleven
 6:00 pm | The Grim Adventures of Billy and Mandy | Runaway Pants/Scythe 2.0
 6:30 pm | Courage the Cowardly Dog | The Mask
 7:00 pm | King of the Hill | Megalo Dale
 7:30 pm | King of the Hill | Boxing Luanne
 8:00 pm | Bob's Burgers | Sliding Bobs
 8:30 pm | Bob's Burgers | The Land Ship
 9:00 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 9:30 pm | American Dad! | Fleabiscuit
10:00 pm | American Dad! | The Future Is Borax
10:30 pm | American Dad! | Fantasy Baseball
11:00 pm | American Dad! | I Am The Jeans: The Gina Lavetti Story
11:30 pm | Rick and Morty | Pilot
12:00 am | Rick and Morty | Wet Kuat Amortican Summer
12:30 am | Aqua Teen Hunger Force | Shaketopia
12:45 am | Aqua Teen Hunger Force | A Quiet Shake
 1:00 am | Robot Chicken | Robot Chicken DC Comics Special III: Magical Friendship
 1:30 am | American Dad! | Steve and Snot's Test-Tubular Adventure
 2:00 am | American Dad! | Buck, Wild
 2:30 am | American Dad! | Crotchwalkers
 3:00 am | Rick and Morty | Lawnmower Dog
 3:30 am | Rick and Morty | Wet Kuat Amortican Summer
 4:00 am | Aqua Teen Hunger Force | Shaketopia
 4:15 am | Aqua Teen Hunger Force | A Quiet Shake
 4:30 am | Your Pretty Face is Going to Hell | Devil in the Details
 4:45 am | Your Pretty Face is Going to Hell | People in Hell Want Ice Water
 5:00 am | Bob's Burgers | Sliding Bobs
 5:30 am | Bob's Burgers | The Land Ship