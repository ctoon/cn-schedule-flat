# 2023-11-08
 6:00 am | Amazing World of Gumball | The Bros
 6:15 am | Amazing World of Gumball | The Mirror
 6:30 am | Amazing World of Gumball | The Man
 6:45 am | Amazing World of Gumball | The Pizza
 7:00 am | Amazing World of Gumball | The Lie
 7:15 am | Amazing World of Gumball | The Butterfly
 7:30 am | Cocomelon | Balloon Boat Race
 7:45 am | Meet the Batwheels | Bibi's Bad Day
 8:00 am | Cocomelon | Five Senses Song
 8:15 am | Thomas & Friends: All Engines Go | The Super Axle
 8:30 am | Cocomelon | Three Little Kittens
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | The Looney Tunes Show | Off Duty Cop
 9:30 am | The Looney Tunes Show | Working Duck
10:00 am | Craig of the Creek | Dog Decider
10:15 am | Craig of the Creek | Bring Out Your Beast
10:30 am | Craig of the Creek | Lost in the Sewer
10:45 am | Craig of the Creek | The Future Is Cardboard
11:00 am | Amazing World of Gumball | The Boombox
11:15 am | Amazing World of Gumball | The Tape
11:30 am | Amazing World of Gumball | The Sweaters
11:45 am | Amazing World of Gumball | The Internet
12:00 pm | Tiny Toons Looniversity | Save The Loo Bru
12:30 pm | Clarence | Chadsgiving
12:45 pm | Clarence | Clarence The Movie
 1:00 pm | Clarence | Belson Gets a Girlfriend
 1:15 pm | Clarence | Brain TV
 1:30 pm | Craig of the Creek | Heart of the Forrest Finale
 2:00 pm | Teen Titans Go! | Villains in a Van Getting Gelato
 2:15 pm | Teen Titans Go! | I Am Chair
 2:30 pm | Teen Titans Go! | Bumgorf
 2:45 pm | Teen Titans Go! | The Mug
 3:00 pm | Amazing World of Gumball | The Plan
 3:15 pm | Amazing World of Gumball | The World
 3:30 pm | Amazing World of Gumball | The Finale
 3:45 pm | Amazing World of Gumball | The Kids
 4:00 pm | Amazing World of Gumball | The Fan
 4:15 pm | Amazing World of Gumball | The Coach
 4:30 pm | Amazing World of Gumball | The Joy
 4:45 pm | Amazing World of Gumball | The Puppy