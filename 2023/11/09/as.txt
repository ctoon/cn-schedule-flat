# 2023-11-09
 5:00 pm | Dexter's Laboratory | Quiet Riot/Accent You Hate/Catch of the Day
 5:30 pm | Ed, Edd n Eddy | An Ed in the Bush/See No Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Bearded Billy/The Nerve
 6:30 pm | Courage the Cowardly Dog | Stormy Weather/The Sandman Sleeps
 7:00 pm | King of the Hill | Now Who's the Dummy?
 7:30 pm | King of the Hill | Ho Yeah!
 8:00 pm | Bob's Burgers | Long Time Listener, First Time Bob
 8:30 pm | Bob's Burgers | The Gene Mile
 9:00 pm | Bob's Burgers | PTA It Ain't So
 9:30 pm | American Dad! | The Shrink
10:00 pm | American Dad! | Holy S***, Jeff's Back!
10:30 pm | American Dad! | American Fung
11:00 pm | American Dad! | Seizures Suit Stanny
11:30 pm | Rick and Morty | The Old Man and the Seat
12:00 am | Rick and Morty | That's Amorte
12:30 am | Robot Chicken | Robot Fight Accident
12:45 am | Robot Chicken | Joel Hurwitz
 1:00 am | Aqua Teen Hunger Force | Rubberman
 1:15 am | Aqua Teen Hunger Force | Eggball
 1:30 am | American Dad! | A Ward Show
 2:00 am | American Dad! | The Worst Stan
 2:30 am | American Dad! | Virtual In-Stanity
 3:00 am | Rick and Morty | The Old Man and the Seat
 3:30 am | Rick and Morty | That's Amorte
 4:00 am | Three Busy Debras | The Milk Drought
 4:15 am | Three Busy Debras | The Great Debpression
 4:30 am | Your Pretty Face is Going to Hell | The Tree-Huger Bomber
 4:45 am | Your Pretty Face is Going to Hell | Torture
 5:00 am | Bob's Burgers | Long Time Listener, First Time Bob
 5:30 am | Bob's Burgers | The Gene Mile