# 2023-11-27
 6:00 am | Amazing World of Gumball | The Vegging
 6:15 am | Amazing World of Gumball | The One
 6:29 am | Amazing World of Gumball | The Father
 6:43 am | Amazing World of Gumball | The Cringe
 6:57 am | Lamput | Signs
 7:00 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:15 am | The Amazing World of Gumball: Darwin's Yearbook | Teachers
 7:29 am | Cocomelon | Wheels on the Bus
 7:43 am | Meet the Batwheels | I'll Be Your Shelter
 7:46 am | Meet the Batwheels | Dynamic Du-Oh-No
 8:00 am | Cocomelon | Wait Your Turn
 8:14 am | Thomas & Friends: All Engines Go | The Sights of Sodor
 8:28 am | Cocomelon | Lost Hamster
 8:42 am | Bugs Bunny Builders: Hard Hat Time | Bugs Bunny Boogie
 8:45 am | Bugs Bunny Builders | Castle Hassle
 9:00 am | The Looney Tunes Show | A Christmas Carol
 9:29 am | The Looney Tunes Show | Mr. Weiner
 9:57 am | Lamput | Hilltop
10:00 am | Craig of the Creek | Ferret Quest
10:15 am | Craig of the Creek | Into the Overpast
10:30 am | Craig of the Creek | The Time Capsule
10:45 am | Craig of the Creek | Beyond the Rapids
11:00 am | Amazing World of Gumball | The Slip
11:15 am | Amazing World of Gumball | The Drama
11:30 am | Amazing World of Gumball | The Buddy
11:45 am | Amazing World of Gumball | The Possession
12:00 pm | Elf | 
 2:00 pm | Teen Titans Go! | Starfire the Terrible
 2:15 pm | Teen Titans Go! | Power Moves
 2:30 pm | Teen Titans Go! | Staring at the Future
 2:45 pm | Teen Titans Go! | No Power
 3:00 pm | Tiny Toons Looniversity | Tooney Ball Lights
 3:30 pm | Amazing World of Gumball | The Master
 3:45 pm | Amazing World of Gumball | The Silence
 4:00 pm | Amazing World of Gumball | The Future
 4:15 pm | Amazing World of Gumball | The Wish
 4:30 pm | Regular Show | The Power
 4:45 pm | Regular Show | Just Set Up the Chairs