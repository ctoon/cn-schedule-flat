# 2023-11-05
 6:00 am | Amazing World of Gumball | The Origins
 6:15 am | Amazing World of Gumball | The Origins
 6:30 am | Amazing World of Gumball | The Advice
 6:45 am | Amazing World of Gumball | The Signal
 7:00 am | Amazing World of Gumball | The Parasite
 7:15 am | Amazing World of Gumball | The Love
 7:30 am | Amazing World of Gumball | The Awkwardness
 7:45 am | Amazing World of Gumball | The Nest
 8:00 am | We Bare Bears | The Mall
 8:15 am | We Bare Bears | Tunnels
 8:30 am | We Bare Bears | Ramen
 8:45 am | We Bare Bears | The Gym
 9:00 am | Scooby-Doo!: Mask of the Blue Falcon | 
10:45 am | We Bare Bears | Imaginary Friend
11:00 am | The Powerpuff Girls | Moral Decay; Meet the Beat Alls
11:30 am | The Powerpuff Girls | The Headsucker's Moxy; Equal Fights
12:00 pm | The Powerpuff Girls | Keen on Keane; Not So Awesome Blossom
12:30 pm | The Powerpuff Girls | Bubblevicious; The Bare Facts
 1:00 pm | Teen Titans Go! | A Sticky Situation
 1:15 pm | Teen Titans Go! | The Perfect Pitch?
 1:30 pm | Teen Titans Go! | Pool Season
 1:45 pm | Teen Titans Go! | Kyle
 2:00 pm | Teen Titans Go! | We'll Be Right Back
 2:15 pm | Teen Titans Go! | TV Knight 7
 2:30 pm | Teen Titans Go! | Jump City Rock
 2:45 pm | Teen Titans Go! | Natural Gas
 3:00 pm | Amazing World of Gumball | The Points
 3:15 pm | Amazing World of Gumball | The Bus
 3:30 pm | Amazing World of Gumball | The Night
 3:45 pm | Amazing World of Gumball | The Misunderstandings
 4:00 pm | Amazing World of Gumball | The Roots
 4:15 pm | Amazing World of Gumball | The Blame
 4:30 pm | Amazing World of Gumball | The Detective
 4:45 pm | Amazing World of Gumball | The Fury