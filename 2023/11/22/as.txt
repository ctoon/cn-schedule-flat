# 2023-11-22
 5:00 pm | Dexter's Laboratory | Boy Named Sue/Lab on the Run
 5:30 pm | Ed, Edd n Eddy | Stiff Upper Ed/Heres Mud in Your Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Zip Your Fly/Puddle Jumping
 6:30 pm | Courage the Cowardly Dog | Dome of Doom/Snowman's Revenge
 7:00 pm | King of the Hill | A Beer Can Named Desire
 7:30 pm | King of the Hill | Spin the Choice
 8:00 pm | Bob's Burgers | The Quirk-Ducers
 8:30 pm | Bob's Burgers | Thanks-hoarding
 9:00 pm | Bob's Burgers | I Bob Your Pardon
 9:30 pm | American Dad! | Family Plan
10:00 pm | American Dad! | The Long Bomb
10:30 pm | American Dad! | Garbage Stan
11:00 pm | American Dad! | Between a Ring and a Hardass
11:30 pm | Rick and Morty | Rickfending Your Mort
12:00 am | The Venture Brothers | 
 2:00 am | American Dad! | Finger Lenting Good
 2:30 am | American Dad! | Between a Ring and a Hardass
 3:00 am | Rick and Morty | Rickfending Your Mort
 3:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 4:00 am | Three Busy Debras | ATM (All The Money)
 4:15 am | Three Busy Debras | Debspringa
 4:30 am | Your Pretty Face is Going to Hell | Stan the Man
 4:45 am | Your Pretty Face is Going to Hell | Gary Bunda: Demon Killer
 5:00 am | Bob's Burgers | The Quirk-Ducers
 5:30 am | Bob's Burgers | Thanks-hoarding