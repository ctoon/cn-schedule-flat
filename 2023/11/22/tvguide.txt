# 2023-11-22
 6:00 am | Amazing World of Gumball | The Transformation
 6:15 am | Amazing World of Gumball | The Understanding
 6:30 am | Amazing World of Gumball | The Ad
 6:45 am | Amazing World of Gumball | The Ghouls
 7:00 am | Amazing World of Gumball | The Stink
 7:15 am | Amazing World of Gumball | The Awareness
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | A Tale of Two Bibis
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | All Wheels on Track
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Looney Science
 9:00 am | The Looney Tunes Show | Mrs. Porkbunny's
 9:30 am | The Looney Tunes Show | Gribbler's Quest
10:00 am | Craig of the Creek | Stink Bomb
10:15 am | Craig of the Creek | The Evolution Of Craig
10:30 am | Craig of the Creek | Sugar Smugglers
10:45 am | Craig of the Creek | Sleepover at Jp's
11:00 am | Amazing World of Gumball | The Password
11:15 am | Amazing World of Gumball | The Vegging
11:30 am | Amazing World of Gumball | The Downer
11:45 am | Amazing World of Gumball | The Brain
12:00 pm | The Polar Express | 
 2:00 pm | The Year Without a Santa Claus | 
 3:15 pm | Teen Titans Go! | A Doom Patrol Thanksgiving
 3:45 pm | Teen Titans Go! | Thanksgiving
 4:00 pm | Teen Titans Go! | Thanksgetting
 4:15 pm | Teen Titans Go! | Wishbone
 4:30 pm | Regular Show | White Elephant Gift Exchange
 4:45 pm | Regular Show | Merry Christmas Mordecai