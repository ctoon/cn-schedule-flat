# 2023-11-23
 6:00 am | Amazing World of Gumball | The Outside
 6:15 am | Amazing World of Gumball | The Vase
 6:30 am | Amazing World of Gumball | The Matchmaker
 6:45 am | Amazing World of Gumball | The Box
 7:00 am | Amazing World of Gumball | The Console
 7:15 am | Amazing World of Gumball | The Ollie
 7:30 am | CoComelon | Guess the Animal Song/ Ten Little Dinos/ Yes Yes Stay Healthy Song
 7:45 am | Meet the Batwheels | Improvise My Ride
 8:00 am | CoComelon | Five Little Birds V3/ Class Pet Sleepover/ Arts & Crafts: Paper Airplanes
 8:15 am | Thomas & Friends: All Engines Go | Something Broken, Someone Blue
 8:30 am | CoComelon | What Makes Me Happy/ Jello Color Song/ This Little Piggy
 8:45 am | Bugs Bunny Builders | Big Feet
 9:00 am | The Looney Tunes Show | The Grand Old Duck of York
 9:30 am | The Looney Tunes Show | Ridiculous Journey
10:00 am | Craig of the Creek | Craig and the Kid's Table
10:30 am | Craig of the Creek | Locked Out Cold
10:45 am | The Year Without a Santa Claus | 
12:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
12:30 pm | Tiny Toons Looniversity | Give Pizza a Chance
 1:00 pm | Tiny Toons Looniversity | Extra, So Extra
 1:30 pm | Tiny Toons Looniversity | Tooney Ball Lights
 2:00 pm | Tiny Toons Looniversity | Save The Loo Bru
 2:30 pm | Tiny Toons Looniversity | Prank You Very Much
 3:00 pm | Tiny Toons Looniversity | General HOGspital
 3:30 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 4:00 pm | Tiny Toons Looniversity | Tears of a Clone
 4:30 pm | Tiny Toons Looniversity | The Show Must Hop On