# 2023-11-15
 5:00 pm | Dexter's Laboratory | Mind Over Chatter/A Quakor Cartoon/Momdark
 5:30 pm | Ed, Edd n Eddy | Your Ed Here/The Good Ol' Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Chocolate Sailor/The Good, The Bad and The Toothless
 6:30 pm | Courage the Cowardly Dog | Katz Under the Sea/Curtain of Cruelty
 7:00 pm | King of the Hill | Joust like a Woman
 7:30 pm | King of the Hill | The Bluegrass is Always Greener
 8:00 pm | Bob's Burgers | Uncle Teddy
 8:30 pm | Bob's Burgers | The Kids Rob a Train
 9:00 pm | Bob's Burgers | I Get Psy-Chic Out of You
 9:30 pm | American Dad! | The Nova Centauris-burgh Board of Tourism Presents: American Dad
10:00 pm | American Dad! | Daesong Heavy Industries
10:30 pm | American Dad! | Daesong Heavy Industries II: Return to Innocence
11:00 pm | American Dad! | A New Era for the Smith House
11:30 pm | Rick and Morty | Unmortricken
12:00 am | Rick and Morty | Childrick of Mort
12:30 am | Robot Chicken | 
 1:00 am | Aqua Teen Hunger Force | Multiple Meat
 1:15 am | Aqua Teen Hunger Force | Freedom Cobra
 1:30 am | American Dad! | The Kidney Stays in the Picture
 2:00 am | American Dad! | Ricky Spanish
 2:30 am | American Dad! | A New Era for the Smith House
 3:00 am | Rick and Morty | Unmortricken
 3:30 am | Rick and Morty | Childrick of Mort
 4:00 am | Three Busy Debras | Debra Gets a Boyfriend
 4:15 am | Three Busy Debras | Women's History Hour
 4:30 am | Your Pretty Face is Going to Hell | Snow Job
 4:45 am | Your Pretty Face is Going to Hell | The Dammed
 5:00 am | Bob's Burgers | Uncle Teddy
 5:30 am | Bob's Burgers | The Kids Rob a Train