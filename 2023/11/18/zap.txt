# 2023-11-18
 6:00 am | Amazing World of Gumball | The Outside
 6:15 am | Amazing World of Gumball | The Vase
 6:30 am | Amazing World of Gumball | The Matchmaker
 6:45 am | Amazing World of Gumball | The Box
 7:00 am | Amazing World of Gumball | The Console
 7:15 am | Amazing World of Gumball | The Ollie
 7:30 am | Amazing World of Gumball | The Catfish
 7:45 am | Amazing World of Gumball | The Cycle
 8:00 am | Amazing World of Gumball | The Stars
 8:15 am | Amazing World of Gumball | The Grades
 8:30 am | Amazing World of Gumball | The Diet
 8:45 am | Amazing World of Gumball | The Ex
 9:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Sing-Songy Stag
 9:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Tears Of Triumph
 9:30 am | Teen Titans Go! | The Wishbone
 9:45 am | Teen Titans Go! | Thanksgiving
10:00 am | Teen Titans Go! | Warner Bros 100th Anniversary
10:30 am | Teen Titans Go! | Legendary Sandwich
10:45 am | Teen Titans Go! | Pie Bros
11:00 am | The Powerpuff Girls | Members Only
11:30 am | The Powerpuff Girls | Abracadaver
11:45 am | The Powerpuff Girls | Cover Up
12:00 pm | The Powerpuff Girls | Town and Out; Child Fearing
12:30 pm | The Powerpuff Girls | Insect Inside
12:45 pm | The Powerpuff Girls | Sweet N' Sour
 1:00 pm | The Powerpuff Girls | Supper Villain
 1:15 pm | The Powerpuff Girls | Buttercrush
 1:30 pm | The Powerpuff Girls | Roughing It Up; What's the Big Idea
 2:00 pm | The Powerpuff Girls | Los Dos Mojos
 2:15 pm | The Powerpuff Girls | 
 2:30 pm | The Powerpuff Girls | Major Competition
 2:45 pm | The Powerpuff Girls | Slumbering With the Enemy
 3:00 pm | Aloha, Scooby-Doo! | 
 4:30 pm | Amazing World of Gumball | The Buddy
 4:45 pm | Amazing World of Gumball | The Possession