# 2023-11-02
 6:00 am | Amazing World of Gumball | The Vacation
 6:15 am | Amazing World of Gumball | The Scam
 6:30 am | Amazing World of Gumball | The Gumball Chronicles: The Curse of Elmore
 6:45 am | Amazing World of Gumball | Halloween
 7:00 am | Amazing World of Gumball | The Mirror
 7:15 am | Amazing World of Gumball | The Ghouls
 7:30 am | CoComelon | Five Little Birds V3/ Class Pet Sleepover/ Arts & Crafts: Paper Airplanes
 7:45 am | Meet the Batwheels | To the Batmobile!
 8:00 am | CoComelon | What Makes Me Happy/ Jello Color Song/ This Little Piggy
 8:15 am | Thomas & Friends: All Engines Go | The Super Axle
 8:30 am | CoComelon | JJ Wants New Bed/ JJ's New Bed / This Is the Way V2/ Mom and Daughter Song
 8:45 am | Bugs Bunny Builders | Party Boat
 9:00 am | Tiny Toons Looniversity | Tears of a Clone
 9:30 am | The Looney Tunes Show | Peel of Fortune
10:00 am | Craig of the Creek | Who Is the Red Poncho?
10:15 am | Craig of the Creek | The Once and Future King
10:30 am | Craig of the Creek | War of the Pieces
11:00 am | Amazing World of Gumball | The Goons
11:15 am | Amazing World of Gumball | The Secret
11:30 am | Amazing World of Gumball | The Sock
11:45 am | Amazing World of Gumball | The Genius
12:00 pm | Tiny Toons Looniversity | Extra, So Extra
12:30 pm | Clarence | Chad and the Marathon
12:45 pm | Clarence | Officer Moody
 1:00 pm | Clarence | Gilben's Different
 1:15 pm | Clarence | Cool Guy Clarence
 1:30 pm | Craig of the Creek | The Jump Off
 1:45 pm | Craig of the Creek | The Cow-Boy and Marie
 2:00 pm | Teen Titans Go! | TV Knight 6
 2:15 pm | Teen Titans Go! | Kryponite
 2:30 pm | Teen Titans Go! | Thumb War
 2:45 pm | Teen Titans Go! | Toddler Titans ... Yay!
 3:00 pm | Amazing World of Gumball | The Poltergeist
 3:15 pm | Amazing World of Gumball | The Mustache
 3:30 pm | Amazing World of Gumball | The Date
 3:45 pm | Amazing World of Gumball | The Club
 4:00 pm | Amazing World of Gumball | The Wand
 4:15 pm | Amazing World of Gumball | The Ape
 4:30 pm | Amazing World of Gumball | The Car
 4:45 pm | Amazing World of Gumball | The Curse