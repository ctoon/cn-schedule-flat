# 2023-11-02
 6:00 am | Amazing World of Gumball | The Vacation; The Scam
 6:30 am | Amazing World of Gumball | Gumball Chronicles: The Curse of Elmore; Halloween
 7:00 am | Amazing World of Gumball | The Ghouls; The Mirror
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | To the Batmobile!
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | The Super Axle
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Mail Whale
 9:00 am | Tiny Toons Looniversity | Tears of a Clone
 9:30 am | The Looney Tunes Show | Peel of Fortune
10:00 am | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
10:30 am | Craig of the Creek | War of the Pieces
11:00 am | Amazing World of Gumball | The Goons; The Secret
11:30 am | Amazing World of Gumball | The Sock; The Genius
12:00 pm | Tiny Toons Looniversity | Extra, So Extra
12:30 pm | Clarence | Chad and the Marathon; Officer Moody
 1:00 pm | Clarence | Cool Guy Clarence; Gilben's Different
 1:30 pm | Craig of the Creek | The Jump off; The Cow-Boy and Marie
 2:00 pm | Teen Titans Go! | TV Knight 6 Kryponite
 2:30 pm | Teen Titans Go! | Thumb War; Toddler Titans...Yay!
 3:00 pm | Amazing World of Gumball | The Poltergeist:The Mustache
 3:30 pm | Amazing World of Gumball | The Club / The Date
 4:00 pm | Amazing World of Gumball | The Wand; The Ape
 4:30 pm | Amazing World of Gumball | The Car; the curse