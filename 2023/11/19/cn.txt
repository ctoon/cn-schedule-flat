# 2023-11-19
 6:00 am | Amazing World of Gumball | The Sorcerer
 6:15 am | Amazing World of Gumball | The Menu
 6:30 am | Amazing World of Gumball | The Uncle
 6:45 am | Amazing World of Gumball | The Weirdo
 7:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
 7:15 am | Amazing World of Gumball | The Heist
 7:30 am | Amazing World of Gumball | The Singing
 7:45 am | Amazing World of Gumball | The Best
 8:00 am | We Bare Bears | Cousin Jon
 8:15 am | We Bare Bears | Lord of the Poppies
 8:30 am | We Bare Bears | The Mummy's Curse
 8:45 am | We Bare Bears | Band of Outsiders
 9:00 am | Scooby-Doo! in Where's my Mummy? | 
10:30 am | We Bare Bears | Sandcastle
10:45 am | We Bare Bears | Bros in the City
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
11:30 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Sing-Songy Stag
11:45 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Tears of Triumph
12:00 pm | The Looney Tunes Show | We're in Big Truffle
12:30 pm | Tiny Toons Looniversity | Tears of a Clone
 1:00 pm | Teen Titans Go! | Wishbone
 1:15 pm | Teen Titans Go! | Dog Hand
 1:30 pm | Teen Titans Go! | Double Trouble
 1:45 pm | Teen Titans Go! | The Date
 2:00 pm | Teen Titans Go! | Dude Relax!
 2:15 pm | Teen Titans Go! | Laundry Day
 2:30 pm | Teen Titans Go! | Ghostboy
 2:45 pm | Teen Titans Go! | La Larva de Amor
 3:00 pm | Amazing World of Gumball | The Worst
 3:15 pm | Amazing World of Gumball | The Deal
 3:30 pm | Amazing World of Gumball | The Petals
 3:45 pm | Amazing World of Gumball | The Puppets
 4:00 pm | Amazing World of Gumball | The Nuisance
 4:15 pm | Amazing World of Gumball | The Line
 4:30 pm | Amazing World of Gumball | The List
 4:45 pm | Amazing World of Gumball | The News