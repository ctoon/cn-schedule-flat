# 2023-11-12
 5:00 pm | Four Christmases | 
 7:00 pm | Bob's Burgers | Roller? I Hardly Know Her!
 7:30 pm | Bob's Burgers | Bobby Driver
 8:00 pm | Bob's Burgers | Lorenzo's Oil? No, Linda's
 8:30 pm | Bob's Burgers | The Helen Hunt
 9:00 pm | American Dad! | Beyond the Alcove or: How I Learned to Stop Worrying and Love Klaus
 9:30 pm | American Dad! | A Song of Knives and Fire
10:00 pm | American Dad! | Z.O.I.N.C.S.
10:30 pm | Rick and Morty | That's Amorte
11:00 pm | Rick and Morty | Unmortricken
11:30 pm | Royal Crackers | Theo's Comeback Tour
12:00 am | The Eric Andre Show | Rim the Reaper
12:15 am | The Eric Andre Show | Football is Back
12:30 am | Aqua Teen Hunger Force | The Last One Forever and Ever (For Real This Time)(We ****ing Mean It)
 1:00 am | American Dad! | Beyond the Alcove or: How I Learned to Stop Worrying and Love Klaus
 1:30 am | American Dad! | A Song of Knives and Fire
 2:00 am | American Dad! | Z.O.I.N.C.S.
 2:30 am | Rick and Morty | That's Amorte
 3:00 am | Rick and Morty | Unmortricken
 3:30 am | Royal Crackers | Theo's Comeback Tour
 4:00 am | The Eric Andre Show | Rim the Reaper
 4:15 am | The Eric Andre Show | Football is Back
 4:30 am | Aqua Teen Hunger Force | The Last One Forever and Ever (For Real This Time)(We ****ing Mean It)
 5:00 am | Bob's Burgers | Roller? I Hardly Know Her!
 5:30 am | Bob's Burgers | Bobby Driver