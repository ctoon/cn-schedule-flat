# 2023-11-12
 6:00 am | Amazing World of Gumball | The Outside
 6:15 am | Amazing World of Gumball | The Vase
 6:30 am | Amazing World of Gumball | The Matchmaker
 6:45 am | Amazing World of Gumball | The Box
 7:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Chicken Of Doom
 7:15 am | Amazing World of Gumball | The Console
 7:30 am | Amazing World of Gumball | The Ollie
 7:45 am | Amazing World of Gumball | The Catfish
 8:00 am | We Bare Bears | Fire!
 8:15 am | We Bare Bears | Ranger Norm
 8:30 am | We Bare Bears | Shmorby
 8:45 am | We Bare Bears | Snake Babies
 9:00 am | Scooby-Doo! Pirates Ahoy! | 
10:30 am | We Bare Bears | Bubble
10:45 am | We Bare Bears | Baby Orphan Ninja Bears
11:00 am | The Powerpuff Girls | Monstra City; Shut the Pup Up!
11:30 am | The Powerpuff Girls | The Boys Are Back in Town
12:00 pm | The Powerpuff Girls | Just Another Manic Mojo; Mime for a Change
12:30 pm | The Powerpuff Girls | Paste Makes Waste; Ice Sore
 1:00 pm | Teen Titans Go! | Our House
 1:15 pm | Teen Titans Go! | Beard Hunter
 1:30 pm | Teen Titans Go! | New Chum
 1:45 pm | Teen Titans Go! | Negative Feels
 2:00 pm | Teen Titans Go! | Elasti-Bot
 2:15 pm | Teen Titans Go! | Arthur
 2:30 pm | Teen Titans Go! | Plot Holes
 2:45 pm | Teen Titans Go! | Toilet Water
 3:00 pm | Amazing World of Gumball | The Cycle
 3:15 pm | Amazing World of Gumball | The Stars
 3:30 pm | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Valiant Quest
 3:45 pm | Amazing World of Gumball | The Grades
 4:00 pm | Amazing World of Gumball | The Diet
 4:15 pm | Amazing World of Gumball | The Ex
 4:30 pm | Amazing World of Gumball | The Sorcerer
 4:45 pm | Amazing World of Gumball | The Menu