# 2023-11-01
 6:00 am | Amazing World of Gumball | The Colossus
 6:15 am | Amazing World of Gumball | The Knights
 6:30 am | Amazing World of Gumball | The Fridge
 6:45 am | Amazing World of Gumball | The Flower
 7:00 am | Amazing World of Gumball | The Banana
 7:15 am | Amazing World of Gumball | The Phone
 7:30 am | Cocomelon | Basketball Song
 7:45 am | Meet the Batwheels | Wheel Side Story
 8:00 am | Cocomelon | Itsy Bitsy Spider (Birdie Edition)
 8:15 am | Thomas & Friends: All Engines Go | Thomas for a Day
 8:30 am | Cocomelon | Guess the Animal Song
 8:45 am | Bugs Bunny Builders | Mini Golf
 9:00 am | The Looney Tunes Show | The Foghorn Leghorn Story
 9:30 am | The Looney Tunes Show | Eligible Bachelors
10:00 am | Craig of the Creek | Wheels Collide
10:15 am | Craig of the Creek | Left in the Dark
10:30 am | Craig of the Creek | Bernard of the Creek, Part 1
10:45 am | Craig of the Creek | Bernard of the Creek, Part 2
11:00 am | Amazing World of Gumball | The Pressure
11:15 am | Amazing World of Gumball | The Painting
11:30 am | Amazing World of Gumball | The Laziest
11:45 am | Amazing World of Gumball | The Ghost
12:00 pm | Tiny Toons Looniversity | Freshman Orientoontion
12:30 pm | Clarence | The Boxcurse Children
12:45 pm | Clarence | Karate Mom
 1:00 pm | Clarence | Clarence Loves Shoopy
 1:15 pm | Clarence | Public Radio
 1:30 pm | Craig of the Creek | The Team Up
 1:45 pm | Craig of the Creek | Putting Together the Pieces
 2:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 3:00 pm | Amazing World of Gumball | The Mystery
 3:15 pm | Amazing World of Gumball | The Prank
 3:30 pm | Amazing World of Gumball | The GI
 3:45 pm | Amazing World of Gumball | The Kiss
 4:00 pm | Amazing World of Gumball | The Party
 4:15 pm | Amazing World of Gumball | The Refund
 4:30 pm | Amazing World of Gumball | The Robot
 4:45 pm | Amazing World of Gumball | The Picnic