# 2023-11-01
 5:00 pm | Dexter's Laboratory | Blackfoot and Slim/Trapped with a Vengeance/The Parrot Trap
 5:30 pm | Ed, Edd n Eddy | Rock-A-Bye Ed/O-Ed Eleven
 6:00 pm | The Grim Adventures of Billy and Mandy | Nigel Planter and the Chamber Pot of Secrets/Circus of Fear
 6:30 pm | Courage the Cowardly Dog | Family Business/1,000 Years of Courage
 7:00 pm | King of the Hill | Movin' On Up
 7:30 pm | King of the Hill | Bill of Sales
 8:00 pm | Bob's Burgers | Just One of the Boyz 4 Now for Now
 8:30 pm | Bob's Burgers | The Taking of Funtime One Two Three
 9:00 pm | Bob's Burgers | Tweentrepreneurs
 9:30 pm | American Dad! | Steve and Snot's Test-Tubular Adventure
10:00 pm | American Dad! | Buck, Wild
10:30 pm | American Dad! | Crotchwalkers
11:00 pm | American Dad! | Viced Principal
11:30 pm | Rick and Morty | Air Force Wong
12:00 am | Rick and Morty | Get Schwifty
12:30 am | Robot Chicken | The Hobbit: There and Bennigan's
12:45 am | Robot Chicken | Garbage Sushi
 1:00 am | Aqua Teen Hunger Force | Gene E
 1:15 am | Aqua Teen Hunger Force | She Creature
 1:30 am | American Dad! | 100 A.D.
 2:00 am | American Dad! | Son of Stan
 2:30 am | American Dad! | Viced Principal
 3:00 am | Rick and Morty | Air Force Wong
 3:30 am | Rick and Morty | Get Schwifty
 4:00 am | Neon Joe, Werewolf Hunter | Made Ya Look
 4:30 am | Your Pretty Face is Going to Hell | Cerberus
 4:45 am | Your Pretty Face is Going to Hell | Nü-Byle
 5:00 am | Bob's Burgers | Just One of the Boyz 4 Now for Now
 5:30 am | Bob's Burgers | The Taking of Funtime One Two Three