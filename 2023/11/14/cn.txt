# 2023-11-14
 6:00 am | Amazing World of Gumball | The Bus
 6:15 am | Amazing World of Gumball | The Night
 6:30 am | Amazing World of Gumball | The Misunderstandings
 6:45 am | Amazing World of Gumball | The Roots
 7:00 am | Amazing World of Gumball | The Blame
 7:15 am | Amazing World of Gumball | The Slap
 7:30 am | CoComelon | Are We There Yet?/ The Tortoise and the Hare/ Humpty Dumpty/ Breakfast Song
 7:45 am | Meet the Batwheels | A Jet Out of Water
 8:00 am | CoComelon | My Name Song/ Yes Yes Vegetables Song/ Wheels on the Bus V2 (Play Version)
 8:15 am | Thomas & Friends: All Engines Go | The Super Axle
 8:30 am | CoComelon | Ten Little Duckies/ The Colors Song (With Popsicles)/ Yes Playground Song
 8:45 am | Bugs Bunny Builders | Smash House
 9:00 am | The Looney Tunes Show | You've Got Hate Mail
 9:30 am | The Looney Tunes Show | Itsy Bitsy Gopher
10:00 am | Craig of the Creek | The Takeout Mission
10:15 am | Craig of the Creek | Dinner at the Creek
10:30 am | Craig of the Creek | Jessica's Trail
10:45 am | Craig of the Creek | Bug City
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
11:15 am | Amazing World of Gumball | The Crew
11:30 am | Amazing World of Gumball | The Others
11:45 am | Amazing World of Gumball | The Signature
12:00 pm | Tiny Toons Looniversity | The Show Must Hop On
12:30 pm | Clarence | A Nightmare on Aberdale Street: Balance's Revenge
12:45 pm | Clarence | Dollar Hunt
 1:00 pm | Clarence | Zoo
 1:15 pm | Clarence | Rise and Shine
 1:30 pm | Craig of the Creek | The Mystery of the Timekeeper
 1:45 pm | Craig of the Creek | Alone Quest
 2:00 pm | Teen Titans Go! | Creative Geniuses
 2:15 pm | Teen Titans Go! | Manor and Mannerisms
 2:30 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 2:45 pm | Teen Titans Go! | Polly Ethylene and Tara Phthalate
 3:00 pm | Amazing World of Gumball | The Check
 3:15 pm | Amazing World of Gumball | The Pest
 3:30 pm | Amazing World of Gumball | The Sale
 3:45 pm | Amazing World of Gumball | The Gift
 4:00 pm | Amazing World of Gumball | The Parking
 4:15 pm | Amazing World of Gumball | The Routine
 4:30 pm | Amazing World of Gumball | The Upgrade
 4:45 pm | Amazing World of Gumball | The Comic