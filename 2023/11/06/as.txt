# 2023-11-06
 5:00 pm | Dexter's Laboratory | Big Bots/Gooey Aliens/Misplaced in Space
 5:30 pm | Ed, Edd n Eddy | Brother, Can You Spare an Ed/The Day the Ed Stood Still
 6:00 pm | The Grim Adventures of Billy and Mandy | Which Came First?/Substitute Teacher
 6:30 pm | Courage the Cowardly Dog | Nowhere TV/Mega Muriel the Magnificent
 7:00 pm | King of the Hill | The Buck Stops Here
 7:30 pm | King of the Hill | I Don't Want to Wait
 8:00 pm | Bob's Burgers | Motor, She Boat
 8:30 pm | Bob's Burgers | Legends of the Mall
 9:00 pm | Bob's Burgers | The Hawkening: Look Who's Hawking Now!
 9:30 pm | Bob's Burgers | Land of the Loft
10:00 pm | Bob's Burgers | All That Gene
10:30 pm | American Dad! | Blonde Ambition
11:00 pm | American Dad! | CIAPOW
11:30 pm | Rick and Morty | The Ricklantis Mixup
12:00 am | Rick and Morty | Morty's Mind Blowers
12:30 am | Robot Chicken | Executed by the State
12:45 am | Robot Chicken | Crushed by a Steamroller on My 53rd Birthday
 1:00 am | Aqua Teen Hunger Force | Time Machine
 1:15 am | Aqua Teen Hunger Force | 2-And-A-Half-Star Wars Out of Five
 1:30 am | American Dad! | Scents and Sensei-bility
 2:00 am | American Dad! | I Am the Walrus
 2:30 am | American Dad! | School Lies
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Rick and Morty | Morty's Mind Blowers
 4:00 am | Three Busy Debras | A Very Debra Christmas
 4:15 am | Three Busy Debras | Cartwheel Club
 4:30 am | Your Pretty Face is Going to Hell | Krampus Nacht
 4:45 am | Your Pretty Face is Going to Hell | Heaven
 5:00 am | Bob's Burgers | Motor, She Boat
 5:30 am | Bob's Burgers | Legends of the Mall