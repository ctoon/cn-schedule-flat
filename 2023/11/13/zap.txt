# 2023-11-13
 6:00 am | Amazing World of Gumball | The Wicked
 6:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince And The Chicken Of Doom
 6:30 am | Amazing World of Gumball | The Origins
 6:45 am | Amazing World of Gumball | The Origins
 7:00 am | Amazing World of Gumball | The Girlfriend
 7:15 am | Amazing World of Gumball | The Advice
 7:30 am | Cocomelon | Twinkle Twinkle Little Star
 7:45 am | Meet the Batwheels | Batty Body Swap
 8:00 am | Cocomelon | Johny Johny Yes Papa
 8:15 am | Thomas & Friends: All Engines Go | All Wheels on Track
 8:30 am | Cocomelon | Please and Thank You Song
 8:45 am | Bugs Bunny Builders | Tweety-Go-Round
 9:00 am | The Looney Tunes Show | Point, Laser, Point
 9:30 am | The Looney Tunes Show | Bobcats on Three
10:00 am | Craig of the Creek | Power Punchers
10:15 am | Craig of the Creek | Creek Cart Racers
10:30 am | Craig of the Creek | Secret Book Club
10:45 am | Craig of the Creek | Jextra Perrestrial
11:00 am | Amazing World of Gumball | The Friend
11:15 am | Amazing World of Gumball | The Oracle
11:30 am | Amazing World of Gumball | The Safety
11:45 am | Amazing World of Gumball | The Society
12:00 pm | Tiny Toons Looniversity | General HOGspital
12:30 pm | Clarence | Clarence Gets a Girlfriend
12:45 pm | Clarence | Jeff's New Toy
 1:00 pm | Clarence | Dinner Party
 1:15 pm | Clarence | Honk
 1:30 pm | Craig of the Creek | Deep Creek Salvage
 1:45 pm | Craig of the Creek | The Shortcut
 2:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 3:00 pm | Amazing World of Gumball | The Spoiler
 3:15 pm | Amazing World of Gumball | The Countdown
 3:30 pm | Amazing World of Gumball | The Nobody
 3:45 pm | Amazing World of Gumball | The Downer
 4:00 pm | Amazing World of Gumball | The Triangle
 4:15 pm | Amazing World of Gumball | The Money
 4:30 pm | Amazing World of Gumball | The Egg
 4:45 pm | Amazing World of Gumball | The Return