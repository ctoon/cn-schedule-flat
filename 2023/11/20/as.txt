# 2023-11-20
 5:00 pm | Dexter's Laboratory | Poppa Wheely/A Mom Cartoon/Mockside of the Moon
 5:30 pm | Ed, Edd n Eddy | Robbin' Ed/A Case of Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Dream Mutt/Scythe for Sale
 6:30 pm | Courage the Cowardly Dog | So in Louvre Are We Two/Night of the Scarecrow
 7:00 pm | King of the Hill | Hank's Cowboy Movie
 7:30 pm | King of the Hill | Nine Pretty Darn Angry Men
 8:00 pm | Bob's Burgers | A Fish Called Tina
 8:30 pm | Bob's Burgers | Three Girls and a Little Wharfy
 9:00 pm | Bob's Burgers | Wag the Song
 9:30 pm | Bob's Burgers | Yurty Rotten Scoundrels
10:00 pm | Bob's Burgers | Flat-Top O' the Morning to Ya
10:30 pm | American Dad! | Roger's Baby
11:00 pm | American Dad! | A Nice Night for a Drive
11:30 pm | Rick and Morty | Mort Dinner Rick Andre
12:00 am | Rick and Morty | Mortyplicity
12:30 am | Aqua Teen Hunger Force | Intervention
12:45 am | Aqua Teen Hunger Force | The Creditor
 1:00 am | Robot Chicken | Hurtled From a Helicopter Into a Speeding Train
 1:15 am | Robot Chicken | In Bed Surrounded by Loved Ones
 1:30 am | American Dad! | Bazooka Steve
 2:00 am | American Dad! | Can I Be Frank (With You)
 2:30 am | American Dad! | American Stepdad
 3:00 am | Rick and Morty | Mort Dinner Rick Andre
 3:30 am | Rick and Morty | Mortyplicity
 4:00 am | Three Busy Debras | A Very Debra Christmas
 4:15 am | Three Busy Debras | Cartwheel Club
 4:30 am | Your Pretty Face is Going to Hell | OMGouija
 4:45 am | Your Pretty Face is Going to Hell | The High Heel
 5:00 am | Bob's Burgers | A Fish Called Tina
 5:30 am | Bob's Burgers | Three Girls and a Little Wharfy