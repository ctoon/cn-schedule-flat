# 2023-11-28
 6:00 am | Amazing World of Gumball | The Cage
 6:15 am | Amazing World of Gumball | The Faith
 6:29 am | Amazing World of Gumball | The DVD
 6:43 am | Amazing World of Gumball | The Third
 6:57 am | Lamput | Superstore
 7:00 am | Amazing World of Gumball | The Debt
 7:15 am | Amazing World of Gumball | The End
 7:29 am | CoComelon | Merry / Deck the Halls/ Christmas Songs/ New Year/ Giving/ Farm/ Resolution
 7:57 am | Meet the Batwheels | Jet Set
 8:00 am | CoComelon | Winter Show & Tell/ Yes Yes Dress for the Rain/ Construction Vehicles Song
 8:14 am | Thomas & Friends: All Engines Go | All Wheels on Track
 8:28 am | CoComelon | 12345 Once I Caught A Fish Alive! V2/ Bed Time Camping/ Be Like Mommy
 8:42 am | Bugs Bunny Builders: Hard Hat Time | Buckle Up
 8:45 am | Bugs Bunny Builders | Catwalk
 9:00 am | The Looney Tunes Show | Best Friends Redux
 9:29 am | The Looney Tunes Show | SuperRabbit
 9:57 am | Lamput | Boxing
10:00 am | Craig of the Creek | The Jinxening
10:15 am | Craig of the Creek | Creek Shorts
10:30 am | Craig of the Creek | The Ground Is Lava!
10:45 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
11:00 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Valiant Quest
11:15 am | The Heroic Quest of the Valiant Prince Ivandoe | The Prince and the Chicken of Doom
11:30 am | Amazing World of Gumball | The Web
11:45 am | Amazing World of Gumball | The Mess
12:00 pm | Apple & Onion | Onionless
12:15 pm | Apple & Onion | Party Popper
12:30 pm | Clarence | Bedside Manners
12:45 pm | Clarence | Jeff Wins
 1:00 pm | Clarence | Suspended
 1:15 pm | Clarence | Turtle Hats
 1:30 pm | Craig of the Creek | Pencil Break Mania
 1:45 pm | Craig of the Creek | The Last Game of Summer
 2:00 pm | Teen Titans Go! | Sidekick
 2:15 pm | Teen Titans Go! | Caged Tiger
 2:30 pm | Teen Titans Go! | Second Christmas
 2:45 pm | Teen Titans Go! | Nose Mouth
 3:00 pm | Tiny Toons Looniversity | The Show Must Hop On
 3:30 pm | Amazing World of Gumball | The Heart
 3:45 pm | Amazing World of Gumball | The Revolt
 4:00 pm | Amazing World of Gumball | The Decisions
 4:15 pm | Amazing World of Gumball | The BFFS
 4:30 pm | Regular Show | Caffeinated Concert Tickets
 4:45 pm | Regular Show | Death Punchies