# 2023-01-13
 6:00 am | Amazing World of Gumball | The Flower; The Banana
 6:30 am | Amazing World of Gumball | The Phone; The Job
 7:00 am | Cocomelon | Row Row Row Your Boat
 7:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 8:00 am | Cocomelon | Johny Johny Yes Papa
 8:30 am | Meet the Batwheels | Scaredy-Bat
 9:00 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 9:30 am | Thomas & Friends: All Engines Go | Kana Recharges
10:00 am | Thomas & Friends: All Engines Go | Shake, Rattle, & Bruno
10:30 am | Bugs Bunny Builders | Stories
11:00 am | New Looney Tunes | Pork Lift; Thes in the City
11:30 am | New Looney Tunes | The Wedding Quacksher; The Food Notwork
12:00 pm | We Baby Bears | Who Crashed the RV?
12:30 pm | Craig of the Creek | Camper on the Run
 1:00 pm | Craig of the Creek | Kelsey the Author
 1:30 pm | Teen Titans Go! | Master Detective
 2:00 pm | Teen Titans Go! | Hand Zombie
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Promise; The Voice
 3:30 pm | Amazing World of Gumball | The Boombox; The Castle
 4:00 pm | Amazing World of Gumball | The Tape; The Sweaters
 4:30 pm | Amazing World of Gumball | The News
 5:00 pm | Craig of the Creek | The Shortcut
 5:30 pm | Teen Titans Go! | Career Day
 6:00 pm | Teen Titans Go! | The Academy
 6:30 pm | Teen Titans Go! | Flashback
 7:00 pm | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital of Dr. Phineas Phrag!
 7:30 pm | Scooby-Doo! and Guess Who? | The Phantom, The Talking Dog, and the Hot Hot Hot Sauce!