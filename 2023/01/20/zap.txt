# 2023-01-20
 6:00 am | Amazing World of Gumball | The Gripes; The Vacation
 6:30 am | Amazing World of Gumball | The Fraud; The Void
 7:00 am | Cocomelon | The Laughing Song
 7:30 am | Cocomelon | Yes Yes Playground Song
 8:00 am | Cocomelon | Chinese New Year
 8:30 am | Meet the Batwheels | Up in the Air
 9:00 am | Mecha Builders | Get in Gear/Free-Wheelin' Ferris Wheel
 9:30 am | Thomas & Friends: All Engines Go | The Big Skunk Funk
10:00 am | Thomas & Friends: All Engines Go | Thomas' Promise
10:30 am | Bugs Bunny Builders | Play Day
11:00 am | New Looney Tunes | One Carroter in Search of an Artist; The Duck Days of Summer
11:30 am | New Looney Tunes | Etiquette Shmetiquette; Daffy in the Science Museum
12:00 pm | We Baby Bears | Temple Bears
12:30 pm | Craig of the Creek | Into the Overpast
 1:00 pm | Craig of the Creek | The Time Capsule
 1:30 pm | Teen Titans Go! | The Groover
 2:00 pm | Teen Titans Go! | The Power of Shrimps
 2:30 pm | Amazing World of Gumball | The Oracle; The Safety
 3:00 pm | Amazing World of Gumball | The Friend; The Saint
 3:30 pm | Amazing World of Gumball | The Society; The Spoiler
 4:00 pm | Amazing World of Gumball | The Countdown; The Nobody
 4:30 pm | Amazing World of Gumball | The Catfish
 5:00 pm | Craig of the Creek | Sleepover at JP's
 5:30 pm | Teen Titans Go! | Teen Titans Roar!
 6:00 pm | Teen Titans Go! | Super Hero Summer Camp
 7:00 pm | Scooby-Doo! and Guess Who? | The Crown Jewel of Boxing!
 7:30 pm | Scooby-Doo! and Guess Who? | The Internet on Haunted House Hill!