# 2023-01-08
 6:00 am | Teen Titans Go! | Dreams; Grandma Voice
 6:30 am | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 7:00 am | Teen Titans Go! | Mr. Butt; Man Person
 7:30 am | Teen Titans Go! | Pirates; I See You
 8:00 am | Amazing World of Gumball | The Recipe; The Puppy
 8:30 am | Amazing World of Gumball | The Name; The Extras
 9:00 am | We Baby Bears | The Boo-Dunnit; Little Mer-Bear
 9:30 am | We Baby Bears | Bubble Fields; Who Crashed the Rv
10:00 am | Craig of the Creek | The Invitation; Creek Cart Racers
10:30 am | Craig of the Creek | Vulture's Nest; Secret Book Club
11:00 am | Craig of the Creek | Kelsey Quest; Jextra Perrestrial
11:30 am | Craig of the Creek | Jpony; The Takeout Mission
12:00 pm | Teen Titans | Betrothed
12:30 pm | Teen Titans | Crash
 1:00 pm | Teen Titans | Haunted
 1:30 pm | Teen Titans | Spellbound
 2:00 pm | Teen Titans | Revolutions
 2:30 pm | Teen Titans | Wavelength
 3:00 pm | Teen Titans | The Beast Within
 3:30 pm | Teen Titans | Can I Keep Him?
 4:00 pm | Amazing World of Gumball | The Gripes; The Vacation
 4:30 pm | Amazing World of Gumball | The Fraud; The Void
 5:00 pm | Amazing World of Gumball | The Move; The Boss
 5:30 pm | Amazing World of Gumball | The Allergy; The Law
 6:00 pm | Smallfoot | 