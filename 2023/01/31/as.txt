# 2023-01-31
 8:00 pm | King Of the Hill | Chasing Bobby
 8:30 pm | King Of the Hill | Master of Puppets I'm Pulling Your Strings!
 9:00 pm | King Of the Hill | Bwah My Nose
 9:30 pm | Bob's Burgers | Mo Mommy Mo Problems
10:00 pm | Bob's Burgers | Mission Impos-slug-ble
10:30 pm | American Dad! | Stan of Arabia Part 2
11:00 pm | American Dad! | Star Trek
11:30 pm | Rick and Morty | The Rickchurian Mortydate
12:00 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
12:30 am | Mike Tyson Mysteries | Tyson of Arabia
12:45 am | Mike Tyson Mysteries | Carol
 1:00 am | Robot Chicken | Scoot to the Gute
 1:15 am | Robot Chicken | Things Look Bad for the Streepster
 1:30 am | American Dad! | Stan of Arabia Part 1
 2:00 am | Rick and Morty | The Rickchurian Mortydate
 2:30 am | Rick and Morty | Edge of Tomorty: Rick Die Rickpeat
 3:00 am | Mike Tyson Mysteries | Tyson of Arabia
 3:15 am | Mike Tyson Mysteries | Carol
 3:30 am | Metalocalypse | Bluesklok
 3:45 am | Superjail | Lord Stingray Crash Party
 4:00 am | Futurama | The Deep South
 4:30 am | Futurama | Bender Gets Made
 5:00 am | Bob's Burgers | Mo Mommy Mo Problems
 5:30 am | Bob's Burgers | Mission Impos-slug-ble