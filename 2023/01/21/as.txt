# 2023-01-21
 8:00 pm | King Of the Hill | To Sirloin With Love
 8:30 pm | King Of the Hill | The Honeymooners
 9:00 pm | King Of the Hill | Just Another Manic Kahn-Day
 9:30 pm | King Of the Hill | And They Call It Bobby Love
10:00 pm | American Dad! | Henderson
10:30 pm | American Dad! | Hot Scoomp
11:00 pm | American Dad! | Lumberjerk
11:30 pm | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
12:00 am | My Hero Academia | Encounter, Part 2
12:30 am | Made In Abyss | The Return
 1:00 am | Yashahime: Princess Half-Demon - The Second Act | Osamu Kirin's Apparition Conquest
 1:30 am | One Piece | The Deadliest Weapon of Mass Destruction in History! Shinokuni!
 2:00 am | Naruto:Shippuden | The Infinite Dream
 2:30 am | Genndy Tartakovsky's Primal | Sea of Despair
 3:00 am | Samurai Jack | CI
 3:30 am | Ballmastrz: 9009 | Leather Passions! 2 Hearts, 2 Wheelz, Infinite Roadz. Ride Now!
 3:45 am | Ballmastrz: 9009 | Hunger Cramps on the Playing Field of Friendship
 4:00 am | Futurama | A Fishful of Dollars
 4:30 am | Futurama | My Three Suns
 5:00 am | King Of the Hill | To Sirloin With Love
 5:30 am | King Of the Hill | The Honeymooners