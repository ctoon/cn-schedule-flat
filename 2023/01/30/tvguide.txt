# 2023-01-30
 6:00 am | Amazing World of Gumball | The Uncle; The Potato
 6:30 am | Amazing World of Gumball | The Sorcerer; The; Heist
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Bibi's Do-Over; Sidekicked to the Curb
 9:00 am | Mecha Builders | The Sent US a Pie / Dust In the Wind
 9:30 am | Thomas & Friends: All Engines Go | Off the Rails/Diesel's Dilemma
10:00 am | Thomas & Friends: All Engines Go | Lost and Found; Eggsellent Adventure
10:30 am | Bugs Bunny Builders | Awesome Duck; Dino Fright; Stories
11:00 am | New Looney Tunes | Weiner Lose Yankee Doodle Bunny; Cold Medal Wabbit No Duck Is an Island
11:30 am | New Looney Tunes | Meanie and the Genie; The in Cold Fudd; My Kingdom for a Duck Finders Keepers; Losers Sweepers
12:00 pm | We Baby Bears | Doll's House; Lighthouse
12:30 pm | Craig of the Creek | Welcome to Creek Street; Beyond the Overpass
 1:00 pm | Craig of the Creek | Breaking the Ice; Sink or Swim Team
 1:30 pm | Teen Titans Go! | Space House
 2:30 pm | Amazing World of Gumball | The Stories; The Console
 3:00 pm | Amazing World of Gumball | The Guy; The Outside
 3:30 pm | Amazing World of Gumball | The Boredom; The Copycats
 4:00 pm | Amazing World of Gumball | The Vision; The; Catfish
 4:30 pm | Amazing World of Gumball | The Choices; The Cycle
 5:00 pm | Craig of the Creek | The Sunflower; The Sparkle Solution
 5:30 pm | Teen Titans Go! | Bbraebday I Am Chair
 6:00 pm | Teen Titans Go! | Real Art; Bumgorf
 6:30 pm | Teen Titans Go! | The Just a Little Patience...Yeah...Yeah Mug
 7:00 pm | Scooby-Doo! and Guess Who? | Scooby-Doo And The Sky Town Cool School!
 7:30 pm | Scooby-Doo! and Guess Who? | Falling Star Man!