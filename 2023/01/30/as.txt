# 2023-01-30
 8:00 pm | King Of the Hill | What Makes Bobby Run?
 8:30 pm | King Of the Hill | Lucky See, Monkey Do
 9:00 pm | King Of the Hill | What Happens at the National Propane Gas Convention
 9:30 pm | Bob's Burgers | Boywatch
10:00 pm | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps
10:30 pm | American Dad! | Con Heir
11:00 pm | American Dad! | Stannie Get Your Gun
11:30 pm | Rick and Morty | Morty's Mind Blowers
12:00 am | Rick and Morty | The ABC's of Beth
12:30 am | Mike Tyson Mysteries | Thy Neighbor's Life
12:45 am | Mike Tyson Mysteries | My Favorite Mystery
 1:00 am | Robot Chicken | The Angelic Sounds of Mike Giggling
 1:15 am | Robot Chicken | Hey I Found Another Sock
 1:30 am | American Dad! | All About Steve
 2:00 am | Rick and Morty | Morty's Mind Blowers
 2:30 am | Rick and Morty | The ABC's of Beth
 3:00 am | Mike Tyson Mysteries | Thy Neighbor's Life
 3:15 am | Mike Tyson Mysteries | My Favorite Mystery
 3:30 am | Metalocalypse | Go Forth and Die
 3:45 am | Superjail | Mayhem Donor
 4:00 am | Futurama | How Hermes Requisitioned His Groove Back
 4:30 am | Futurama | A Clone of My Own
 5:00 am | Bob's Burgers | Boywatch
 5:30 am | Bob's Burgers | As I Walk Through the Alley of the Shadow of Ramps