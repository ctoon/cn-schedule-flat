# 2023-01-12
 8:00 pm | King Of the Hill | Nancy Boys
 8:30 pm | King Of the Hill | Edu-macating Lucky
 9:00 pm | King Of the Hill | The Peggy Horror Picture Show
 9:30 pm | Bob's Burgers | Eggs for Days
10:00 pm | Bob's Burgers | Zero Larp Thirty
10:30 pm | American Dad! | Trophy Wife, Trophy Life
11:00 pm | American Dad! | Game Night
11:30 pm | Rick and Morty | Something Ricked This Way Comes
12:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
12:30 am | Mike Tyson Mysteries | Heavyweight Champion of the Moon
12:45 am | Mike Tyson Mysteries | Is Magic Real?
 1:00 am | Robot Chicken | Zeb and Kevin Erotic Hot Tub Canvas
 1:15 am | Robot Chicken | Cheese Puff Mountain
 1:30 am | American Dad! | Exquisite Corpses
 2:00 am | Rick and Morty | Something Ricked This Way Comes
 2:30 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 3:00 am | Mike Tyson Mysteries | Heavyweight Champion of the Moon
 3:15 am | Mike Tyson Mysteries | Is Magic Real?
 3:30 am | Metalocalypse | Dethtroll
 3:45 am | Superjail | Ladies Night
 4:00 am | Futurama | Naturama
 4:30 am | Futurama | 2-D Blacktop
 5:00 am | Bob's Burgers | Eggs for Days
 5:30 am | Bob's Burgers | Zero Larp Thirty