# 2023-01-04
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Riddle Me This; Grass Is Greener
 9:00 am | Mecha Builders | The Sent US a Pie / Dust In the Wind
 9:30 am | Thomas & Friends: All Engines Go | Thomas in Charge; Hide and Surprise!
10:00 am | Thomas & Friends: All Engines Go | Real Number One; The; Pop a Wheelie
10:30 am | Bugs Bunny Builders | Squirreled Away; Beach Battle
11:00 am | New Looney Tunes | The Thirst Things Firstbugs of Chance; Porky's Duck-Livery Servicewabbit Who Would Be King
11:30 am | New Looney Tunes | The Pigmallian/Bugs the Gladiator Bugs for Mayor/Lepra-Con
12:00 pm | Craig of the Creek | Power Punchers; Alone Quest
12:30 pm | Craig of the Creek | Creek Cart Racers; Memories of Bobby
 1:00 pm | Teen Titans Go! | Island Adventures
 2:00 pm | Amazing World of Gumball | The Responsible; The Dress
 2:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 3:00 pm | Amazing World of Gumball | The Mystery; The Prank
 3:30 pm | Amazing World of Gumball | The GI; The Kiss
 4:00 pm | Craig of the Creek | Takeout Mission; The Return of the Honeysuckle Rangers
 4:30 pm | Craig of the Creek | Jessica's Trail; Fort Williams
 5:00 pm | Teen Titans Go! | History Lesson; Obinray
 5:30 pm | Teen Titans Go! | The Art of Ninjutsu; Batman vs. Teen Titans: Dark Injustice
 6:00 pm | Teen Titans Go! | Think About Your Future; Wally T
 6:30 pm | Teen Titans Go! | Who's Laughing Now; Booty Scooty
 7:00 pm | Scooby-Doo! and Guess Who? | Now You Sia, Now You Don't!
 7:30 pm | Scooby-Doo! and Guess Who? | The Nightmare Ghost Of Psychic U!