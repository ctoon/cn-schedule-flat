# 2023-01-18
 8:00 pm | King Of the Hill | Peggy's Fan Fair
 8:30 pm | King Of the Hill | Bobby Rae
 9:00 pm | King Of the Hill | The Powder Puff Boys
 9:30 pm | Bob's Burgers | Into the Mild
10:00 pm | Bob's Burgers | Brunchsquatch
10:30 pm | American Dad! | Russian Doll
11:00 pm | American Dad! | Stan Moves to Chicago
11:30 pm | Rick and Morty | Total Rickall
12:00 am | Rick and Morty | Get Schwifty
12:30 am | Mike Tyson Mysteries | Kidnapped!
12:45 am | Mike Tyson Mysteries | House Haunters
 1:00 am | Robot Chicken | Joel Hurwitz
 1:15 am | Robot Chicken | Blackout Window Heat Stroke
 1:30 am | American Dad! | Who Smarted?
 2:00 am | Rick and Morty | Total Rickall
 2:30 am | Rick and Morty | Get Schwifty
 3:00 am | Mike Tyson Mysteries | Kidnapped!
 3:15 am | Mike Tyson Mysteries | House Haunters
 3:30 am | Metalocalypse | Performance Klok
 3:45 am | Superjail | Terrorarium
 4:00 am | Futurama | Murder on the Planet Express
 4:30 am | Futurama | Stench and Stenchibility
 5:00 am | Bob's Burgers | Into the Mild
 5:30 am | Bob's Burgers | Brunchsquatch