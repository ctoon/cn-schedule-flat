# 2023-01-28
 6:00 am | Amazing World of Gumball | The Lady
 6:30 am | Amazing World of Gumball | The Sucker
 7:00 am | Amazing World of Gumball | The Cage
 7:30 am | Amazing World of Gumball | The Rival
 8:00 am | Amazing World of Gumball | The One
 8:30 am | Amazing World of Gumball | The Vegging
 9:00 am | We Baby Bears | Doll's House
 9:15 am | We Baby Bears | Lighthouse
 9:30 am | Amazing World of Gumball | The Tag; The Lesson
10:00 am | Total DramaRama | The Cone-versation
10:30 am | Total DramaRama | Oozing Talent
11:00 am | Total DramaRama | Senior Sinisters
11:30 am | Total DramaRama | Be Claws I Love You, Shelley
12:00 pm | Craig of the Creek | Sour Candy Trials
12:30 pm | Craig of the Creek | The Other Side
 1:00 pm | Craig of the Creek | Summer Wish
 1:30 pm | Craig of the Creek | Turning the Tables
 2:00 pm | Teen Titans Go! | Yearbook Madness; Hose Water
 2:30 pm | Teen Titans Go! | Let's Get Serious; Tamaranian Vacation
 3:00 pm | Teen Titans Go! | Rocks and Water; Multiple Trick Pony
 3:30 pm | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 4:00 pm | Amazing World of Gumball | The Father
 4:30 pm | Amazing World of Gumball | The Cringe
 5:00 pm | Amazing World of Gumball | The Neighbor
 5:30 pm | Amazing World of Gumball | The Pact
 6:00 pm | Scooby-Doo! and the Alien Invaders | 