# 2023-01-28
 8:00 pm | King Of the Hill | Peggy's Headache
 8:30 pm | King Of the Hill | Pregnant Paws
 9:00 pm | King Of the Hill | Next of Shin
 9:30 pm | King Of the Hill | Peggy's Pageant Fever
10:00 pm | American Dad! | Deacon Stan, Jesus Man
10:30 pm | American Dad! | Bullocks to Stan
11:00 pm | American Dad! | A Smith in the Hand
11:30 pm | Rick and Morty | Solaricks
12:00 am | My Hero Academia | Disaster Walker
12:30 am | Made In Abyss | All That You Gather
 1:00 am | Yashahime: Princess Half-Demon - The Second Act | The Grim Butterfly of Despair
 1:30 am | One Piece | Launching the Counter Attack! Luffy and Law's Great Escape!
 2:00 am | Naruto:Shippuden | The Infinite Tsukuyomi
 2:30 am | Genndy Tartakovsky's Primal | Shadow of Fate
 3:00 am | Ballmastrz: 9009 | Chaste Wing of the Cold Turkey vs Flaming Fist of Indulgence!
 3:15 am | Ballmastrz: 9009 | Strength through Song; Brotherhood through Blood; Redemption through Rage. Sing, Fallen Angel!
 3:30 am | Superjail | Stingstress
 3:45 am | Superjail | Superfail
 4:00 am | Futurama | Raging Bender
 4:30 am | Futurama | A Bicyclops Built for Two
 5:00 am | King Of the Hill | Peggy's Headache
 5:30 am | King Of the Hill | Pregnant Paws