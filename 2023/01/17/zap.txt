# 2023-01-17
 6:00 am | Amazing World of Gumball | The Pizza; The Lie
 6:30 am | Amazing World of Gumball | The Butterfly; The Question
 7:00 am | Cocomelon | Chinese New Year
 7:30 am | Cocomelon | Shadow Puppets
 8:00 am | Cocomelon | Bingo Farm Version
 8:30 am | Meet the Batwheels | Zoomsday
 9:00 am | Mecha Builders | Silly Hat Catastrophe!/Zee In A Bottle
 9:30 am | Thomas & Friends: All Engines Go | Ashima's Amazing Arrival
10:00 am | Thomas & Friends: All Engines Go | Tri-and-a-Half-a-Lon
10:30 am | Bugs Bunny Builders | Lunar New Year
11:00 am | New Looney Tunes | 10-4 Good Bunny; Gold Medal Wabbit
11:30 am | New Looney Tunes | Cyrano de Bugs; Point Duck Percent
12:00 pm | We Baby Bears | Temple Bears
12:30 pm | Craig of the Creek | Sugar Smugglers
 1:00 pm | Craig of the Creek | Sleepover at JP's
 1:30 pm | Teen Titans Go! | Them Soviet Boys
 2:00 pm | Teen Titans Go! | Lil' Dimples
 2:30 pm | Amazing World of Gumball | The Recipe; The Puppy
 3:00 pm | Amazing World of Gumball | The Name; The Extras
 3:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 4:00 pm | Amazing World of Gumball | The Fraud; The Void
 4:30 pm | Amazing World of Gumball | The Ollie
 5:00 pm | Craig of the Creek | Ferret Quest
 5:30 pm | Teen Titans Go! | Little Elvis
 6:00 pm | Teen Titans Go! | The Groover
 6:30 pm | Teen Titans Go! | The Power of Shrimps
 7:00 pm | Scooby-Doo! and Guess Who? | The Hot Dog Dog!
 7:30 pm | Scooby-Doo! and Guess Who? | A Moveable Mystery!