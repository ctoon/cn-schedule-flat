# 2023-01-06
 8:00 pm | King Of the Hill | Hank's On Board
 8:30 pm | King Of the Hill | Bystand Me
 9:00 pm | King Of the Hill | Bill's House
 9:30 pm | King Of the Hill | Harlottown
10:00 pm | American Dad! | Hamerican Dad!
10:30 pm | American Dad! | Demolition Daddy
11:00 pm | American Dad! | Pride Before the Fail
11:30 pm | Rick and Morty | Lawnmower Dog
12:00 am | Ballmastrz: 9009 | Infinite Hugs: Cold Embrace of the Bloodless Progenitors!
12:15 am | Ballmastrz: 9009 | Shameful Disease of Yackety Yack! Don't Talk Back! Be Silenced Forever!
12:30 am | Ballmastrz: 9009 | Big Boy Body, Bigger Baby Boy Heart! Can You Endure the Pain of Love? Babyball, Discover It Now!
12:45 am | Ballmastrz: 9009 | Children of the Night Serenade Wet Nurse of Reprisal; Scream, Bloodsucker, Scream!
 1:00 am | Ballmastrz: 9009 | When You Wish Upon a Spore
 1:15 am | Ballmastrz: 9009 | Can't Stand the Heat? Ultimate Kitchen Technique! Finish Them, Warrior Bard!!
 1:30 am | Ballmastrz: 9009 | Defective Affection?! Dodge the Wayward Strikes of Cupid's Calamitous Quiver!
 1:45 am | Ballmastrz: 9009 | Dance Dance Convolution?! Egos Warped by the Hair Gel of Hubris! Atonement, Now!
 2:00 am | Rick and Morty | Lawnmower Dog
 2:30 am | Futurama | T.: The Terrestrial
 3:00 am | Futurama | The Bots and the Bees
 3:30 am | Futurama | A Farewell to Arms
 4:00 am | Futurama | Decision 3012
 4:30 am | Futurama | The Thief of Baghead
 5:00 am | King Of the Hill | Bill's House
 5:30 am | King Of the Hill | Harlottown