# 2023-01-19
 6:00 am | Amazing World of Gumball | The Kids; The Fan
 6:30 am | Amazing World of Gumball | Coach; Thejoy; the Lunar New Year
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Buff's Bff; Cave Sweet Cave Wheelin' and Dealin'
 9:00 am | Mecha Builders | The Can't Stop the Martiansbest Nest Test; Beepers: Funky Time
 9:30 am | Thomas & Friends: All Engines Go | New Mail Engine in Town; Percy's Lucky Bell I Sure Am Feeling Lucky
10:00 am | Thomas & Friends: All Engines Go | Big Skunk Funk; The; Sandy's Sandy Shipment
10:30 am | Bugs Bunny Builders | Smash House; Dino Fright Lunar New Year
11:00 am | New Looney Tunes | Abracawabbitponce de Calzone; Hip Hop Hare Part 1hip Hop Hare Part 2
11:30 am | New Looney Tunes | For the Love of Fraudnot so Special Delivery; Gettin' Your Goatspelunkheads
12:00 pm | We Baby Bears | Modern-Ish Stone Age Family; Unica
12:30 pm | Craig of the Creek | Jessica Shorts in the Key of the Creek
 1:00 pm | Craig of the Creek | The Ferret Quest; Ground Is Lava!
 1:30 pm | Teen Titans Go! | Quantum Fun; Bbrbday
 2:00 pm | Teen Titans Go! | The Fight; Tall Titan Tales
 2:30 pm | Amazing World of Gumball | The Mirror; The Burden
 3:00 pm | Amazing World of Gumball | The Man; The Bros
 3:30 pm | Amazing World of Gumball | The Lie; The Pizza
 4:00 pm | Amazing World of Gumball | The Butterfly; The Question
 4:30 pm | Amazing World of Gumball | The Copycats; The Outside
 5:00 pm | Craig of the Creek | Creek Daycare; Secret in a Bottle
 5:30 pm | Teen Titans Go! | Curse Of The Booty Scooty; Collect Them All
 6:00 pm | Teen Titans Go! | Them Soviet Boys Cartoon Feud
 6:30 pm | Teen Titans Go! | Don't Be An Icarus; Lil' Dimples
 7:00 pm | Scooby-Doo! and Guess Who? | Scooby On Ice!
 7:30 pm | Scooby-Doo! and Guess Who? | Caveman On The Half Pipe!