# 2023-01-22
 6:00 am | Amazing World of Gumball | The Authority; The Virus
 6:30 am | Amazing World of Gumball | The Pony; The Storm
 7:00 am | Amazing World of Gumball | The Dream; The Sidekick
 7:30 am | Amazing World of Gumball | The Hero; The Photo
 8:00 am | Amazing World of Gumball | The Crew
 8:30 am | Amazing World of Gumball | The Others
 9:00 am | Craig of the Creek | Jessica's Trail
 9:30 am | Craig of the Creek | Bug City
10:00 am | Teen Titans Go! | Video Game References; Cool School
10:30 am | Teen Titans Go! | Kicking a Ball & Pretending to Be Hurt; Head Fruit
11:00 am | Teen Titans Go! | Operation Tin Man; Nean
11:30 am | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
12:00 pm | Adventure Time | Slumber Party Panic
12:30 pm | Adventure Time | I Remember You
 1:00 pm | Adventure Time | Simon & Marcy
 1:30 pm | Adventure Time | Stakes, Part 2: Everything Stays
 2:00 pm | Adventure Time | Wizard Battle
 2:30 pm | Adventure Time | The Hall of Egress
 3:00 pm | Adventure Time | Mortal Folly
 3:30 pm | Adventure Time | Adventure Time With Fionna and Cake
 4:00 pm | Amazing World of Gumball | The Signature
 4:30 pm | Amazing World of Gumball | The Gift
 5:00 pm | Amazing World of Gumball | The Apprentice
 5:30 pm | Amazing World of Gumball | The Check
 6:00 pm | Alice Through the Looking Glass | 