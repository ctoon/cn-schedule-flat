# 2023-01-22
 8:15 pm | Wipeout | OMFG Girl Run!
 8:30 pm | Bob's Burgers | Sleeping with the Frenemy
 9:00 pm | Futurama | Fry Am the Egg Man
 9:30 pm | Futurama | The Tip of the Zoidberg
10:00 pm | American Dad! | The Three Fs
10:30 pm | American Dad! | Smooshed: A Love Story
11:00 pm | Rick and Morty | Final DeSmithation
11:30 pm | Rick and Morty | JuRicksic Mort
12:00 am | YOLO: Silver Destiny | Sausage Sizzle
12:15 am | YOLO: Silver Destiny | Planet Bali
12:30 am | Aqua Teen | Larry Miller Hair System
12:45 am | Aqua Teen | One Hundred
 1:00 am | Joe Pera Talks With You | Joe Pera Gives You Piano Lessons
 1:15 am | Joe Pera Talks With You | Joe Pera Watches Internet Videos with You
 1:30 am | American Dad! | The Three Fs
 2:00 am | American Dad! | Smooshed: A Love Story
 2:30 am | Rick and Morty | Final DeSmithation
 3:00 am | Rick and Morty | JuRicksic Mort
 3:30 am | YOLO: Silver Destiny | Sausage Sizzle
 3:45 am | YOLO: Silver Destiny | Planet Bali
 4:00 am | Bob's Burgers | The Secret Ceramics Room of Secrets
 4:30 am | Bob's Burgers | Sleeping with the Frenemy
 5:00 am | King Of the Hill | Three Men And A Bastard
 5:30 am | King Of the Hill | The Accidental Terrorist