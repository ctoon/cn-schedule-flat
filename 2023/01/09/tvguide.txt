# 2023-01-09
 6:00 am | Amazing World of Gumball | The Bumpkin; the Flakers
 6:30 am | Amazing World of Gumball | Authority; Thevirus; the Interview With a Crime-Fighter
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Keep Calm and Roll on; Buff's Bff Silly Stowaway
 9:00 am | Mecha Builders | The Magnet Mayhemstop That Train; Beepers: Funky Time
 9:30 am | Thomas & Friends: All Engines Go | Kana Recharges; Thomas in Charge Diesel's Seagull
10:00 am | Thomas & Friends: All Engines Go | Tiger Train; The; Skiff Sails Sodor
10:30 am | Bugs Bunny Builders | Race Track Race; Dino Fright Buckle Up
11:00 am | New Looney Tunes | Bigs Bunny/Wahder, Wahder, Everywhere Proud to Be a Coal Miner's Wabbit/Cabin Fervor
11:30 am | New Looney Tunes | Porky the Disorderly/Game, Set, Wabbit Grand Barbari-Yon, the/Giant Rabbit Hunters
12:00 pm | We Baby Bears | Who Crashed the Rv; Bubble Fields
12:30 pm | Craig of the Creek | Dinner at the Creek; Kelsey the Elder
 1:00 pm | Craig Of The Creek | Sour Candy Trials; Bug City
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
 2:00 pm | Teen Titans Go! | The Self-Indulgent 200th Episode Spectacular!
 2:30 pm | Amazing World of Gumball | The Car; The Curse
 3:00 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:30 pm | Amazing World of Gumball | The Helmet; The Fight
 4:00 pm | Amazing World of Gumball | The End; The DVD
 4:30 pm | Amazing World of Gumball | The Vision; The Boredom
 5:00 pm | Craig Of The Creek | The Other Side
 5:30 pm | Teen Titans Go! | Inner Beauty of a Cactus; Movie Night
 6:00 pm | Teen Titans Go! | Permanent Record; Titan Saving Time
 6:30 pm | Teen Titans Go! | BBRAE
 7:00 pm | Scooby-Doo! and Guess Who? | The New York Underground!
 7:30 pm | Scooby-Doo! and Guess Who? | Fear Of The Fire Beast!