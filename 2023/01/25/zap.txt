# 2023-01-25
 6:00 am | Amazing World of Gumball | The Bus
 6:30 am | Amazing World of Gumball | The Disaster
 7:00 am | Cocomelon | Row Row Row Your Boat
 7:30 am | Cocomelon | Johny Johny Yes Papa
 8:00 am | Cocomelon | Yes Yes Playground Song
 8:30 am | Meet the Batwheels | Bam's Upgrade
 9:00 am | Mecha Builders | Get in Gear/Free-Wheelin' Ferris Wheel
 9:30 am | Thomas & Friends: All Engines Go | Off the Rails
10:00 am | Thomas & Friends: All Engines Go | A Quiet Delivery
10:30 am | Bugs Bunny Builders | Stories
11:00 am | New Looney Tunes | Amaduckus; Fowl Me Once
11:30 am | New Looney Tunes | Daffy the Gaucho; Free Slugsworthy
12:00 pm | We Baby Bears | Triple T Tigers
12:30 pm | Craig of the Creek | Pencil Break Mania
 1:00 pm | Craig of the Creek | The Last Game of Summer
 1:30 pm | Teen Titans Go! | Baby Mouth
 2:00 pm | Teen Titans Go! | Lucky Stars
 2:30 pm | Amazing World of Gumball | The Apprentice
 3:00 pm | Amazing World of Gumball | The Check
 3:30 pm | Amazing World of Gumball | The Pest
 4:00 pm | Amazing World of Gumball | The Hug
 4:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 5:00 pm | Craig of the Creek | Winter Break
 5:30 pm | Teen Titans Go! | Magic Man
 6:00 pm | Teen Titans Go! | The Titans Go Casual
 6:30 pm | Teen Titans Go! | We're Off to Get Awards
 7:00 pm | Scooby-Doo! and Guess Who? | The Tao Of Scoob!
 7:30 pm | Scooby-Doo! and Guess Who? | Returning Of The Key Ring!