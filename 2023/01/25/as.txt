# 2023-01-25
 8:00 pm | King Of the Hill | Peggy Makes the Big Leagues
 8:30 pm | King Of the Hill | The Courtship of Joseph's Father
 9:00 pm | King Of the Hill | Strangeness on a Train
 9:30 pm | Bob's Burgers | Cheer Up Sleepy Gene
10:00 pm | Bob's Burgers | The Trouble with Doubles
10:30 pm | American Dad! | Cry Baby
11:00 pm | American Dad! | Crystal Clear
11:30 pm | Rick and Morty | Pickle Rick
12:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
12:30 am | Mike Tyson Mysteries | Old Man of the Mountain
12:45 am | Mike Tyson Mysteries | Jason B. Sucks
 1:00 am | Robot Chicken | Yogurt in a Bag
 1:15 am | Robot Chicken | Secret of the Flushed Footlong
 1:30 am | American Dad! | Family Time
 2:00 am | Rick and Morty | Pickle Rick
 2:30 am | Rick and Morty | Vindicators 3: The Return of Worldender
 3:00 am | Mike Tyson Mysteries | Old Man of the Mountain
 3:15 am | Mike Tyson Mysteries | Jason B. Sucks
 3:30 am | Metalocalypse | Skwisklok
 3:45 am | Superjail | Time-Police 2
 4:00 am | Futurama | When Aliens Attack
 4:30 am | Futurama | Fry and the Slurm Factory
 5:00 am | Bob's Burgers | Cheer Up Sleepy Gene
 5:30 am | Bob's Burgers | The Trouble with Doubles