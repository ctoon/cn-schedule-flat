# 2023-01-02
 6:00 am | Amazing World of Gumball | The Mystery; The Prank
 6:30 am | Amazing World of Gumball | The Gi; The Kiss
 7:00 am | Cocomelon | Basketball Song
 7:30 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 8:00 am | Cocomelon | Row Row Row Your Boat
 8:30 am | Meet the Batwheels | Rev and Let Rev
 9:00 am | Mecha Builders | Cross That Bridge; Knock Knock! Who's There?
 9:30 am | Thomas & Friends: All Engines Go | Thomas in Charge
10:00 am | Thomas & Friends: All Engines Go | The Joke Is on Thomas
10:30 am | Bugs Bunny Builders | Tweety-Go-Round
11:00 am | New Looney Tunes | Raising Your Spirits; Dust Bugster
11:30 am | New Looney Tunes | Computer Bugs; Oils Well That Ends Well
12:00 pm | Craig of the Creek | The Future Is Cardboard
12:30 pm | Craig of the Creek | Dog Decider
 1:00 pm | Teen Titans Go! | Wally T
 1:30 pm | Teen Titans Go! | Booty Scooty
 2:00 pm | Amazing World of Gumball | The Awkwardness
 2:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 3:00 pm | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 3:30 pm | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 4:00 pm | Craig of the Creek | Power Punchers
 4:30 pm | Craig of the Creek | Creek Cart Racers
 5:00 pm | Teen Titans Go! | I'm the Sauce
 5:30 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 6:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:30 pm | Teen Titans Go! | The Fourth Wall
 7:00 pm | Scooby-Doo and Guess Who? | Attack of the Weird Al-losaurus!
 7:30 pm | Scooby-Doo and Guess Who? | When Urkel-Bots Go Bad!