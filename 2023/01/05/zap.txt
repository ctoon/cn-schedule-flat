# 2023-01-05
 6:00 am | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 6:30 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:00 am | Cocomelon | Yes Yes Playground Song
 7:30 am | Cocomelon | Rain Rain Go Away Indoors Version
 8:00 am | Cocomelon | Bingo Farm Version
 8:30 am | Meet the Batwheels | Stop That Ducky!
 9:00 am | Mecha Builders | Mecha Builders Pull Together; Lift Up and Lift Off
 9:30 am | Thomas & Friends: All Engines Go | Roller Coasting
10:00 am | Thomas & Friends: All Engines Go | Thomas in Charge
10:30 am | Bugs Bunny Builders | Game Time
11:00 am | New Looney Tunes | Squeak Show; Rodeo Bugs
11:30 am | New Looney Tunes | Slugsworthy's Mega-Mansion; Wile E.'s Walnuts
12:00 pm | Craig of the Creek | Secret Book Club
12:30 pm | Craig of the Creek | Jextra Perrestrial
 1:00 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 1:30 pm | Teen Titans Go! | The Fourth Wall
 2:00 pm | Amazing World of Gumball | The Party; The Refund
 2:30 pm | Amazing World of Gumball | The Robot; The Picnic
 3:00 pm | Amazing World of Gumball | The Goons; The Secret
 3:30 pm | Amazing World of Gumball | The Sock; The Genius
 4:00 pm | Craig of the Creek | The Future Is Cardboard
 4:30 pm | Craig of the Creek | Dog Decider
 5:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1
 5:30 pm | Teen Titans Go! | Oregon Trail
 6:00 pm | Teen Titans Go! | Snuggle Time
 6:30 pm | Teen Titans Go! | Oh Yeah!
 7:00 pm | Scooby-Doo! and Guess Who? | Quit Clowning!
 7:30 pm | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!