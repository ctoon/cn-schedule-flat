# 2023-01-05
 6:00 am | Amazing World of Gumball | Darwin's Yearbook - Carrie Darwin's Yearbook - Alan
 6:30 am | Amazing World of Gumball | Darwin's Yearbook - Sarah Darwin's Yearbook - Teachers
 7:00 am | Cocomelon | 
 7:30 am | Cocomelon | 
 8:00 am | Cocomelon | 
 8:30 am | Meet the Batwheels | Stop That Ducky! ; Zoomsday
 9:00 am | Mecha Builders | Mecha Builders Pull Togetherlift up and Lift Off
 9:30 am | Thomas & Friends: All Engines Go | Roller Coasting; Goodbye; Ghost-Scaring Machine
10:00 am | Thomas & Friends: All Engines Go | Thomas in Charge; More Cowbell
10:30 am | Bugs Bunny Builders | Game Time; Soup Up
11:00 am | New Looney Tunes | A Duck in the Penthouse / Tour de Bugs / Squeak Show / Rodeo Bugs
11:30 am | New Looney Tunes | Knight and Duck/Color of Bunny/Slugsworthy's Mega-Mansion/Wile E.'s Walnuts
12:00 pm | Craig of the Creek | Secret Book Club; Jacob of the Creek
12:30 pm | Craig Of The Creek | The Mystery Of The Timekeeper; Jextra Perrestrial
 1:00 pm | Teen Titans Go! | Hey You; Don't Forget About Me in Your Memory; Squash & Stretch
 1:30 pm | Teen Titans Go! | The Fourth Wall; Garage Sale
 2:00 pm | Amazing World of Gumball | The Party; The Refund
 2:30 pm | Amazing World of Gumball | The Robot; The Picnic
 3:00 pm | Amazing World of Gumball | The Goons; The Secret
 3:30 pm | Amazing World of Gumball | The Sock; The Genius
 4:00 pm | Craig of the Creek | The Future Is Cardboard; Doorway to Helen
 4:30 pm | Craig of the Creek | Dog Decider; The Climb
 5:00 pm | Teen Titans Go! | Operation Dude Rescue: Part 1; Operation Dude Rescue: Part 2
 5:30 pm | Teen Titans Go! | The Oregon Trail Overbite
 6:00 pm | Teen Titans Go! | Snuggle Time Shrimps and Prime Rib
 6:30 pm | Teen Titans Go! | Oh Yeah! Booby Trap House
 7:00 pm | Scooby-Doo! and Guess Who? | Quit Clowning!
 7:30 pm | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!