# 2023-01-15
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 7:00 am | Amazing World of Gumball | The Third; The Debt
 7:30 am | Amazing World of Gumball | The Pressure; The Painting
 8:00 am | Amazing World of Gumball | The Wicked
 8:30 am | Amazing World of Gumball | The Origins
 9:00 am | We Baby Bears | Meat House
 9:30 am | We Baby Bears | Who Crashed the RV?
10:00 am | We Baby Bears | Hashtag Number One Fan
10:30 am | We Baby Bears | Snow Place Like Home
11:00 am | Craig of the Creek | Monster in the Garden
11:30 am | Craig of the Creek | Big Pinchy
12:00 pm | Craig of the Creek | Deep Creek Salvage
12:30 pm | Scooby-Doo! and the Samurai Sword | 
 2:30 pm | Craig of the Creek | Capture the Flag Part 1: The Candy
 3:00 pm | Craig of the Creek | Capture the Flag Part 3: The Legend
 3:30 pm | Craig of the Creek | Capture the Flag Part 5: The Game
 4:00 pm | Scooby-Doo! and the Loch Ness Monster | 
 6:00 pm | Shrek Forever After | 