# 2023-10-24
 5:00 pm | Dexter's Laboratory | Critical Gas/Let's Save the World, You Jerk/Average Joe
 5:30 pm | Ed, Edd n Eddy | X Marks the Ed/From Here to Ed
 6:00 pm | The Grim Adventures of Billy and Mandy | Dad Day Afternoon/Scary Poppins
 6:30 pm | Courage the Cowardly Dog | Evil Weevil/McPhearson Phantom
 7:00 pm | King of the Hill | Cotton's Plot
 7:30 pm | King of the Hill | Bills are Made to be Broken
 8:00 pm | Bob's Burgers | Sexy Dance Fighting
 8:30 pm | Bob's Burgers | Hamburger Dinner Theater
 9:00 pm | Bob's Burgers | Sheesh! Cab, Bob?
 9:30 pm | American Dad! | G-String Circus
10:00 pm | American Dad! | Rapture's Delight
10:30 pm | American Dad! | Don't Look a Smith Horse in the Mouth
11:00 pm | American Dad! | A Jones for a Smith
11:30 pm | Rick and Morty | Night Family
12:00 am | Rick and Morty | Total Rickall
12:30 am | Rick and Morty | Summer's Sleepover
12:45 am | Robot Chicken | Nightmare Generator
 1:00 am | Aqua Teen Hunger Force | MC Pee Pants
 1:15 am | Aqua Teen Hunger Force | Love Mummy
 1:30 am | American Dad! | G-String Circus
 2:00 am | American Dad! | Rapture's Delight
 2:30 am | American Dad! | Don't Look a Smith Horse in the Mouth
 3:00 am | Rick and Morty | Night Family
 3:30 am | Rick and Morty | Total Rickall
 4:00 am | Neon Joe, Werewolf Hunter | Loose Lips Drink Sips
 4:30 am | Your Pretty Face is Going to Hell | Take Life by the Horns
 4:45 am | Your Pretty Face is Going to Hell | Schmickler83!
 5:00 am | King of the Hill | Cotton's Plot
 5:30 am | King of the Hill | Bills are Made to be Broken