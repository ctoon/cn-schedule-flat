# 2023-10-22
 6:00 am | Amazing World of Gumball | The Allergy
 6:15 am | Amazing World of Gumball | The Mothers
 6:30 am | Amazing World of Gumball | The Password
 6:45 am | Amazing World of Gumball | The Procrastinators
 7:00 am | Amazing World of Gumball | The Shell
 7:15 am | Amazing World of Gumball | The Burden
 7:30 am | Amazing World of Gumball | The Bros
 7:45 am | Amazing World of Gumball | The Gumball Chronicles: The Curse of Elmore
 8:00 am | Scooby-Doo! Camp Scare | 
 9:30 am | Teen Titans Go! | Cartoon Feud
 9:45 am | Adventure Time | From Bad to Worse
10:00 am | Regular Show | Terror Tales of the Park V
10:30 am | Regular Show | Terror Tales of the Park VI
11:00 am | Adventure Time | The Creeps
11:15 am | We Bare Bears | Charlie's Halloween Thing
11:30 am | We Bare Bears | Charlie's Halloween Thing 2
12:00 pm | The Looney Tunes Show | Year of the Duck
12:30 pm | Tiny Toons Looniversity | Soufflé, Girl Hey
 1:00 pm | Teen Titans Go! | Sweet Revenge
 1:15 pm | Teen Titans Go! | Porch Pirates
 1:30 pm | Teen Titans Go! | A Sticky Situation
 1:45 pm | Teen Titans Go! | The Perfect Pitch?
 2:00 pm | Teen Titans Go! | Pool Season
 2:15 pm | Teen Titans Go! | Kyle
 2:30 pm | Teen Titans Go! | TV Knight 7
 2:45 pm | Teen Titans Go! | We'll Be Right Back
 3:00 pm | Amazing World of Gumball | The Mirror
 3:15 pm | Amazing World of Gumball | The Man
 3:30 pm | Amazing World of Gumball | The Pizza
 3:45 pm | Amazing World of Gumball | The Lie
 4:00 pm | Amazing World of Gumball | The Butterfly
 4:15 pm | Amazing World of Gumball | The Question
 4:30 pm | Amazing World of Gumball | The Saint
 4:45 pm | Amazing World of Gumball | The Gumball Chronicles: The Curse of Elmore