# 2023-10-23
 5:00 pm | Dexter's Laboratory | Dexter's Lab: A Story/Coupon for Craziness/Better Off Wet
 5:30 pm | Ed, Edd n Eddy | Will Work for Ed/Ed, Ed and Away
 6:00 pm | The Grim Adventures of Billy and Mandy | Dumb-Dumbs and Dragons/Fear and Loathing in Endsville
 6:30 pm | Courage the Cowardly Dog | Courage Meets Big Foot/Hothead
 7:00 pm | King of the Hill | As Old as the Hills
 7:30 pm | King of the Hill | Peggy Hill: the Decline and Fall
 8:00 pm | Bob's Burgers | The Fresh Princ-ipal
 8:30 pm | Bob's Burgers | Roamin' Bob-iday
 9:00 pm | Bob's Burgers | What About Blob?
 9:30 pm | Bob's Burgers | If You Love It So Much, Why Don't You Marionette?
10:00 pm | Bob's Burgers | Long Time Listener, First Time Bob
10:30 pm | American Dad! | Man in the Moonbounce
11:00 pm | American Dad! | Shallow Vows
11:30 pm | Rick and Morty | Rick Potion No. 9
12:00 am | Rick and Morty | Big Trouble in Little Sanchez
12:30 am | Robot Chicken | Ghandi Mulholland in: Plastic Doesn't Get Cancer
12:45 am | Robot Chicken | Endgame
 1:00 am | Aqua Teen Hunger Force | Bus of the Undead
 1:15 am | Aqua Teen Hunger Force | Monster
 1:30 am | American Dad! | Man in the Moonbounce
 2:00 am | American Dad! | Shallow Vows
 2:30 am | American Dad! | My Morning Straitjacket
 3:00 am | Rick and Morty | Rick Potion No. 9
 3:30 am | Rick and Morty | Big Trouble in Little Sanchez
 4:00 am | Neon Joe, Werewolf Hunter | Not Earth China
 4:30 am | Your Pretty Face is Going to Hell | Welcome to Hell
 4:45 am | Your Pretty Face is Going to Hell | Bone Garden
 5:00 am | Bob's Burgers | The Fresh Princ-ipal
 5:30 am | Bob's Burgers | Roamin' Bob-iday