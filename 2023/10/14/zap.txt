# 2023-10-14
 6:00 am | Amazing World of Gumball | The Treasure
 6:15 am | Amazing World of Gumball | The Apology
 6:30 am | Amazing World of Gumball | The Words
 6:45 am | Amazing World of Gumball | The Mirror
 7:00 am | Amazing World of Gumball | The Skull
 7:15 am | Amazing World of Gumball | The Bet
 7:30 am | Amazing World of Gumball | The Watch
 7:45 am | Amazing World of Gumball | The Bumpkin
 8:00 am | Amazing World of Gumball | The Flakers
 8:15 am | Amazing World of Gumball | The Authority
 8:30 am | Amazing World of Gumball | The Virus
 8:45 am | Amazing World of Gumball | The Pony
 9:00 am | Tiny Toons Looniversity | General HOGspital
 9:30 am | Teen Titans Go! | Warner Bros 100th Anniversary
10:00 am | Teen Titans Go! | Space House
11:00 am | Craig of the Creek | Dodgy Decisions
11:15 am | Craig of the Creek | Hyde & Zeke
11:30 am | Craig of the Creek | The Anniversary Box
11:45 am | Craig of the Creek | Lost & Found
12:00 pm | Tiny Toons Looniversity | General HOGspital
12:30 pm | Tiny Toons Looniversity | Prank You Very Much
 1:00 pm | Teen Titans Go! | Warner Bros 100th Anniversary
 1:30 pm | Teen Titans Go! | Doomsday Preppers
 1:45 pm | Teen Titans Go! | Fat Cats
 2:00 pm | Amazing World of Gumball | The Hero
 2:15 pm | Amazing World of Gumball | The Dream
 2:30 pm | Amazing World of Gumball | The Sidekick
 2:45 pm | Amazing World of Gumball | The Vacation
 3:00 pm | Scooby-Doo! The Sword and the Scoob | 
 4:45 pm | Amazing World of Gumball | The Downer