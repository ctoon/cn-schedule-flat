# 2023-10-08
 6:00 am | Amazing World of Gumball | The DVD
 6:15 am | Amazing World of Gumball | The Debt
 6:30 am | Amazing World of Gumball | The Dress
 6:45 am | Amazing World of Gumball | The Spoon
 7:00 am | Amazing World of Gumball | The Painting
 7:15 am | Amazing World of Gumball | The Ghost
 7:30 am | Amazing World of Gumball | The Prank
 7:45 am | Amazing World of Gumball | The Kiss
 8:00 am | Happy Halloween, Scooby-Doo! | 
 9:45 am | We Bare Bears | Escandalosos
10:00 am | Regular Show | Terror Tales of the Park
10:30 am | Regular Show | Terror Tales of the Park II
11:00 am | We Bare Bears | Pizza Band
11:15 am | We Bare Bears | Adopted
11:30 am | We Bare Bears | Wingmen
11:45 am | We Bare Bears | Braces
12:00 pm | Tiny Toons Looniversity | Save The Loo Bru
12:30 pm | Tiny Toons Looniversity | Prank You Very Much
 1:00 pm | Teen Titans Go! | Haunted Tank
 1:15 pm | Teen Titans Go! | Creative Geniuses
 1:30 pm | Teen Titans Go! | Trans Oceanic Magical Cruise
 1:45 pm | Teen Titans Go! | EEbows
 2:00 pm | Teen Titans Go! | What a Boy Wonders
 2:15 pm | Teen Titans Go! | Hafo Safo
 2:30 pm | Teen Titans Go! | Pig in a Poke
 2:45 pm | Teen Titans Go! | A Little Help Please
 3:00 pm | Amazing World of Gumball | The Banana
 3:15 pm | Amazing World of Gumball | The Phone
 3:30 pm | Amazing World of Gumball | The Fridge
 3:45 pm | Amazing World of Gumball | The Remote
 4:00 pm | Amazing World of Gumball | The Flower
 4:15 pm | Amazing World of Gumball | The Job
 4:30 pm | Amazing World of Gumball | The Knights
 4:45 pm | Amazing World of Gumball | The Colossus