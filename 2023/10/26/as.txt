# 2023-10-26
 5:00 pm | Dexter's Laboratory | Filet of Soul/Golden Diskette
 5:30 pm | Ed, Edd n Eddy's Boo-Haw Haw | 
 6:00 pm | The Grim Adventures of Billy and Mandy | Billy and Mandy's Jacked Up Halloween
 6:30 pm | Courage the Cowardly Dog | Feast of the Bullfrogs/Tulip's Worm
 7:00 pm | King of the Hill | A Beer Can Named Desire
 7:30 pm | King of the Hill | Hilloween
 8:00 pm | Bob's Burgers | Fort Night
 8:30 pm | Bob's Burgers | Tina and the Real Ghost
 9:00 pm | Bob's Burgers | The Hauntening
 9:30 pm | American Dad! | Best Little Horror House in Langley Falls
10:00 pm | American Dad! | Hot Water
10:30 pm | American Dad! | Exquisite Corpses
11:00 pm | American Dad! | Steve's Franken Out
11:30 pm | Rick and Morty | Lawnmower Dog
12:00 am | Rick and Morty | The Jerrick Trap
12:30 am | The Robot Chicken Walking Dead Special: Look Who's Walking | 
 1:00 am | Aqua Teen Hunger Force | The Shaving
 1:15 am | Aqua Teen Hunger Force | Vampirus
 1:30 am | American Dad! | Best Little Horror House in Langley Falls
 2:00 am | American Dad! | Hot Water
 2:30 am | American Dad! | Exquisite Corpses
 3:00 am | Rick and Morty | Lawnmower Dog
 3:30 am | Rick and Morty | The Jerrick Trap
 4:00 am | Neon Joe, Werewolf Hunter | Rules of the Road
 4:30 am | Your Pretty Face is Going to Hell | Psyklone and the Thin Twins
 4:45 am | Your Pretty Face is Going to Hell | Shoulder Work
 5:00 am | King of the Hill | A Beer Can Named Desire
 5:30 am | King of the Hill | Hilloween