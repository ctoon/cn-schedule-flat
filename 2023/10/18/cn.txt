# 2023-10-18
 6:00 am | Amazing World of Gumball | The Boredom
 6:15 am | Amazing World of Gumball | The Vision
 6:30 am | Amazing World of Gumball | The Choices
 6:45 am | Amazing World of Gumball | The Code
 7:00 am | Amazing World of Gumball | The Ghouls
 7:15 am | Amazing World of Gumball | The Scam
 7:30 am | CoComelon | Hide and Seek Song/3 Little Kittens/Head Shoulders Knees Toes/Ball Game
 7:45 am | CoComelon | The Country Mouse and the City Mouse/Jobs and Career Song
 8:00 am | Jessica's Big Little World | The Big Kid Playground
 8:15 am | Meet the Batwheels | Harley Did It
 8:30 am | CoComelon | Ten in the Bed/The Bear Went Over the Mountain/Hickory Dickory Dock
 8:45 am | Bugs Bunny Builders | Goofballs
 9:00 am | The Looney Tunes Show | Spread Those Wings and Fly
 9:30 am | The Looney Tunes Show | The Black Widow
10:00 am | Craig of the Creek | Sink or Swim Team
10:15 am | Craig of the Creek | The Quick Name
10:30 am | Craig of the Creek | The Chef's Challenge
10:45 am | Craig of the Creek | The Sparkle Solution
11:00 am | Amazing World of Gumball | The Stories
11:15 am | Amazing World of Gumball | The Detective
11:30 am | Amazing World of Gumball | The Re-Run
11:45 am | Amazing World of Gumball | The Test
12:00 pm | Scooby-Doo! and Guess Who? | I Put a Hex on You!
12:30 pm | Scooby-Doo! and Guess Who? | The High School Wolfman's Musical Lament!
 1:00 pm | Clarence | Space Race
 1:15 pm | Clarence | Plant Daddies
 1:30 pm | Craig of the Creek | Trick or Creek
 2:00 pm | Teen Titans Go! | Tall Titan Tales
 2:15 pm | Teen Titans Go! | I Used to Be a Peoples
 2:30 pm | Teen Titans Go! | The Metric System vs. Freedom
 2:45 pm | Teen Titans Go! | The Chaff
 3:00 pm | Amazing World of Gumball | The Slide
 3:15 pm | Amazing World of Gumball | The Loophole
 3:30 pm | Amazing World of Gumball | The Copycats
 3:45 pm | Amazing World of Gumball | The Potato
 4:00 pm | Amazing World of Gumball | The Fuss
 4:15 pm | Amazing World of Gumball | The Outside
 4:30 pm | What's New Scooby-Doo? | The Unnatural