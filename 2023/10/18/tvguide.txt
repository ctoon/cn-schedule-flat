# 2023-10-18
 6:00 am | Amazing World of Gumball | The Vision; The Boredom
 6:30 am | Amazing World of Gumball | The Choices; The Code
 7:00 am | Amazing World of Gumball | The Ghouls; The Scam
 7:30 am | Cocomelon | 
 7:45 am | Cocomelon | 
 8:00 am | Jessica's Big Little World | The Big Kid Playground
 8:15 am | Meet the Batwheels | Harley Did It
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Goofballs
 9:00 am | The Looney Tunes Show | Double Date
 9:30 am | The Looney Tunes Show | To Bowl or Not to Bowl
10:00 am | Craig of the Creek | Sink or Swim Team; The Quick Name
10:30 am | Craig of the Creek | The Chef's Challenge; The Sparkle Solution
11:00 am | Amazing World of Gumball | The Detective; The Stories
11:30 am | Amazing World of Gumball | The Re-Run
11:45 am | Amazing World of Gumball | The Test
12:00 pm | Scooby-Doo! and Guess Who? | I Put a Hex on You!
12:30 pm | Scooby-Doo! and Guess Who? | The High School Wolfman's Musical Lament!
 1:00 pm | Clarence | Space Race; Plant Daddies
 1:30 pm | Craig of the Creek | Trick or Creek
 2:00 pm | Teen Titans Go! | I Used To Be A Peoples; Tall Titan Tales
 2:30 pm | Teen Titans Go! | The Chaff; The Metric System Vs. Freedom
 3:00 pm | Amazing World of Gumball | The Slide; The Loophole
 3:30 pm | Amazing World of Gumball | The Copycats; The Potato
 4:00 pm | Amazing World of Gumball | The Fuss; The Outside
 4:30 pm | What's New Scooby-Doo? | The Unnatural