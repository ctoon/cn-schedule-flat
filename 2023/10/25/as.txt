# 2023-10-25
 5:00 pm | Dexter's Laboratory | Rushmore Rumble/A Boy and His Bug/You Vegetabelieve It
 5:30 pm | Ed, Edd n Eddy | Ed or Tails/Boys Will Be Eds
 6:00 pm | The Grim Adventures of Billy and Mandy | Billy Gets an "A"/Yeti or Not, Here I Come
 6:30 pm | Courage the Cowardly Dog | Night of the Weremole/Mother's Day
 7:00 pm | King of the Hill | Little Horrors of Shop
 7:30 pm | King of the Hill | Aisle 8A
 8:00 pm | Bob's Burgers | Bed & Breakfast
 8:30 pm | Bob's Burgers | Art Crawl
 9:00 pm | Bob's Burgers | Spaghetti Western and Meatballs
 9:30 pm | American Dad! | May the Best Stan Win
10:00 pm | American Dad! | Return of the Bling
10:30 pm | American Dad! | Cops and Roger
11:00 pm | American Dad! | The Professor and the Coach
11:30 pm | Rick and Morty | The Jerrick Trap
12:00 am | Rick and Morty | Look Who's Purging Now
12:30 am | Robot Chicken | Vegetable Fun Fest
12:45 am | Robot Chicken | Happy Russian Deathdog Dolloween 2 U
 1:00 am | Aqua Teen Hunger Force | Broodwich
 1:15 am | Aqua Teen Hunger Force | Little Brittle
 1:30 am | American Dad! | May the Best Stan Win
 2:00 am | American Dad! | Return of the Bling
 2:30 am | American Dad! | The Professor and the Coach
 3:00 am | Rick and Morty | The Jerrick Trap
 3:30 am | Rick and Morty | Look Who's Purging Now
 4:00 am | Neon Joe, Werewolf Hunter | Parenthetical Head Nod
 4:30 am | Your Pretty Face is Going to Hell | Devil in the Details
 4:45 am | Your Pretty Face is Going to Hell | People in Hell Want Ice Water
 5:00 am | King of the Hill | Little Horrors of Shop
 5:30 am | King of the Hill | Aisle 8A