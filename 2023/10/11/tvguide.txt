# 2023-10-11
 6:00 am | Amazing World of Gumball | The Uploads; The Romantic
 6:30 am | Amazing World of Gumball | The Hug; The Apprentice
 7:00 am | Amazing World of Gumball | The Traitor; The Wicked
 7:30 am | Cocomelon | 
 8:00 am | Jessica's Big Little World | Bedtime Routine
 8:15 am | Meet the Batwheels | Buff's BFF
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Soup Up
 9:00 am | The Looney Tunes Show | SuperRabbit
 9:30 am | The Looney Tunes Show | Best Friends
10:00 am | Craig of the Creek | The Haunted Dollhouse
10:15 am | Craig of the Creek | Snow Day
10:30 am | Craig of the Creek | The Other Side: The Tournament
11:00 am | Amazing World of Gumball | The Limit
11:15 am | Amazing World of Gumball | The Voice
11:30 am | Amazing World of Gumball | The Promise
11:45 am | Amazing World of Gumball | The Castle
12:00 pm | Scooby-Doo! and Guess Who? | Quit Clowning!
12:30 pm | Scooby-Doo! and Guess Who? | The Sword, the Fox and the Scooby Doo!
 1:00 pm | Over the Garden Wall | Chapter 5: Mad Love; Chapter 6: Lullaby In Frogland
 1:30 pm | Craig of the Creek | New Jersey
 1:45 pm | Jessica's Big Little World | The Big Kid Playground
 2:00 pm | Teen Titans Go! | Beast Girl
 2:15 pm | Teen Titans Go! | Thumb War
 2:30 pm | Teen Titans Go! | Toddler Titans...Yay!
 2:45 pm | Teen Titans Go! | Bro-Pocalypse
 3:00 pm | Amazing World of Gumball | The Boombox
 3:15 pm | Amazing World of Gumball | The Tape
 3:30 pm | Amazing World of Gumball | The Sweaters
 3:45 pm | Amazing World of Gumball | The Re-Run
 4:00 pm | Amazing World of Gumball | The Plan
 4:15 pm | Amazing World of Gumball | The World
 4:30 pm | What's New Scooby-Doo? | Safari So Goodie!