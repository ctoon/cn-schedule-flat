# 2023-10-28
 6:00 am | Amazing World of Gumball | The Friend;The Gumball Chronicles: The Curse of Elmore
 6:30 am | Amazing World of Gumball | The Safety; The Oracle
 7:00 am | Amazing World of Gumball | The Vacation; The Scam
 7:30 am | Amazing World of Gumball | The Spoiler; The Society
 8:00 am | Amazing World of Gumball | The Ghouls;The Countdown
 8:30 am | Amazing World of Gumball | The Nobody;The Gumball Chronicles: The Curse of Elmore
 9:00 am | Tiny Toons Looniversity | Tears of a Clone
 9:30 am | Teen Titans Go! | Pepo the Pumpkinman; Halloween
10:00 am | Teen Titans Go! | Campfire Stories; Haunted Tank
10:30 am | Teen Titans Go! | Scary Figure Dance;Ghost with the Most
11:00 am | Craig of the Creek | Silver Fist Returns; Craig to the Future
11:30 am | Craig of the Creek | Craig of the Street; Puppy Love
12:00 pm | Tiny Toons Looniversity | Tears of a Clone
12:30 pm | The Looney Tunes Show | Reunion
 1:00 pm | Teen Titans Go! | Witches Brew; 365
 1:30 pm | Teen Titans Go! | Monster Squad!;A Stickier Situation
 2:00 pm | Amazing World of Gumball | The Halloween Mirror
 2:30 pm | Amazing World of Gumball | The Vacation; The Scam
 3:00 pm | Scooby-Doo! Legend of the Phantosaur | 
 4:45 pm | Amazing World of Gumball | Halloween