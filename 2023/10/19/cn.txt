# 2023-10-19
 6:00 am | Amazing World of Gumball | The Signal
 6:15 am | Amazing World of Gumball | The Parasite
 6:30 am | Amazing World of Gumball | The Love
 6:45 am | Amazing World of Gumball | The Awkwardness
 7:00 am | Amazing World of Gumball | The Nest
 7:15 am | Amazing World of Gumball | The Points
 7:30 am | CoComelon | Toy Balloon Car Race/Bingo/Sorry, Excuse Me
 7:45 am | CoComelon | Hiccup Song/ Laughing Song/ Happy Birthday / Rock-a-bye Baby/ Skidamarink
 8:00 am | Jessica's Big Little World | Small Uncle's Big Bath
 8:15 am | Meet the Batwheels | Buff in a China Shop
 8:30 am | CoComelon | Are You Sleeping?/ Hot Cross Buns/ Clean Up Song/ Three Little Pigs
 8:45 am | Bugs Bunny Builders | Skate Park
 9:00 am | The Looney Tunes Show | Mrs. Porkbunny's
 9:30 am | The Looney Tunes Show | Gribbler's Quest
10:00 am | Craig of the Creek | Better Than You
10:15 am | Craig of the Creek | The Dream Team
10:30 am | Craig of the Creek | Fire & Ice
10:45 am | Craig of the Creek | The Haunted Dollhouse
11:00 am | Amazing World of Gumball | The Vase
11:15 am | Amazing World of Gumball | The Matchmaker
11:30 am | Amazing World of Gumball | The Box
11:45 am | Amazing World of Gumball | The Console
12:00 pm | Scooby-Doo! and Guess Who? | The Horrible Haunted Hospital of Dr. Phineas Phrag!
12:30 pm | Scooby-Doo! and Guess Who? | The Phantom, the Talking Dog and the Hot Hot Hot Sauce!
 1:00 pm | Clarence | Bucky and the Howl
 1:15 pm | Clarence | Spooky Boo
 1:30 pm | Craig of the Creek | The Legend of the Library
 1:45 pm | Craig of the Creek | Brother Builder
 2:00 pm | Teen Titans Go! | Them Soviet Boys
 2:15 pm | Teen Titans Go! | Little Elvis
 2:30 pm | Teen Titans Go! | Warner Bros 100th Anniversary
 3:00 pm | Amazing World of Gumball | The Ollie
 3:15 pm | Amazing World of Gumball | The Catfish
 3:30 pm | Amazing World of Gumball | The Cycle
 3:45 pm | Amazing World of Gumball | The Stars
 4:00 pm | Amazing World of Gumball | The Ghouls
 4:15 pm | Amazing World of Gumball | The Grades
 4:30 pm | What's New Scooby-Doo? | High-Tech House of the Future