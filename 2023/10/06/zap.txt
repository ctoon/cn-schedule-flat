# 2023-10-06
 6:00 am | Amazing World of Gumball | The Pressure
 6:15 am | Amazing World of Gumball | The Laziest
 6:30 am | Amazing World of Gumball | The Mystery
 6:45 am | Amazing World of Gumball | The GI
 7:00 am | Amazing World of Gumball | The Party
 7:15 am | Amazing World of Gumball | The Robot
 7:30 am | Cocomelon | Funny Face Song
 7:45 am | Cocomelon | My Big Brother Song
 8:00 am | Jessica's Big Little World | Glow Toy
 8:15 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:30 am | Cocomelon | Row Row Row Your Boat
 8:45 am | Bugs Bunny Builders | Looney Science
 9:00 am | Teen Titans Go! | Labor Day
 9:15 am | Teen Titans Go! | Ones and Zeroes
 9:30 am | Teen Titans Go! | TV Knight 2
 9:45 am | Teen Titans Go! | The Academy
10:00 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
10:15 am | Craig of the Creek | Craig of the Beach
10:30 am | Jessica's Big Little World | Glow Toy
10:45 am | Craig of the Creek | The Ice Pop Trio
11:00 am | Amazing World of Gumball | The Master
11:15 am | Amazing World of Gumball | The Silence
11:30 am | Amazing World of Gumball | The Future
11:45 am | Amazing World of Gumball | The Wish
12:00 pm | Scooby-Doo! and Guess Who? | Attack of the Weird Al-losaurus!
12:30 pm | Scooby-Doo! and Guess Who? | When Urkel-Bots Go Bad!
 1:00 pm | Clarence | Saturday School
 1:15 pm | Clarence | Attack the Block Party
 1:30 pm | Craig of the Creek | The Ground Is Lava!
 1:45 pm | Craig of the Creek | Plush Kingdom
 2:00 pm | Teen Titans Go! | Labor Day
 2:15 pm | Teen Titans Go! | Ones and Zeroes
 2:30 pm | Teen Titans Go! | TV Knight 2
 2:45 pm | Teen Titans Go! | The Academy
 3:00 pm | Amazing World of Gumball | The Poltergeist
 3:15 pm | Amazing World of Gumball | The Mustache
 3:30 pm | Amazing World of Gumball | The Date
 3:45 pm | Amazing World of Gumball | The Club
 4:00 pm | Amazing World of Gumball | The Wand
 4:15 pm | Amazing World of Gumball | The Ape
 4:30 pm | What's New Scooby-Doo? | It's Mean, It's Green, It's the Mystery Machine