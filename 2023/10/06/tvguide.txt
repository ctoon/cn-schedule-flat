# 2023-10-06
 6:00 am | Amazing World of Gumball | The Pressure ;The Laziest
 6:30 am | Amazing World of Gumball | The GI; The Mystery
 7:00 am | Amazing World of Gumball | The Robot; The Party
 7:30 am | Cocomelon | 
 8:00 am | Jessica's Big Little World | Glow Toy
 8:15 am | Meet the Batwheels | Redbird's Bogus Beach Day
 8:30 am | Cocomelon | 
 9:00 am | The Looney Tunes Show | The Grand Old Duck of York
 9:30 am | The Looney Tunes Show | Ridiculous Journey
10:00 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind; Craig of the Beach
10:30 am | Jessica's Big Little World | Glow Toy
10:45 am | Craig of the Creek | The Ice Pop Trio
11:00 am | Amazing World of Gumball | The Silence; The Master
11:30 am | Amazing World of Gumball | The Wish; The Future
12:00 pm | Scooby-Doo! and Guess Who? | Attack of the Weird Al-Losaurus!
12:30 pm | Scooby-Doo! and Guess Who? | When Urkel-Bots Go Bad!
 1:00 pm | Clarence | Attack the Block Party; Saturday School
 1:30 pm | Craig of the Creek | The Ground Is Lava!; Plush Kingdom
 2:00 pm | Teen Titans Go! | Labor Day; Ones and Zeros
 2:30 pm | Teen Titans Go! | The Academy; TV Knight 2
 3:00 pm | Amazing World of Gumball | The Poltergeist:The Mustache
 3:30 pm | Amazing World of Gumball | The Club / The Date
 4:00 pm | Amazing World of Gumball | The Wand; The Ape
 4:30 pm | What's New Scooby-Doo? | It's Mean, It's Green, It's the Mystery Machine