# 2023-10-15
 5:00 pm | Tim Burton's Corpse Bride | 
 6:45 pm | SHOP: A Pop Opera | SHOP: A Pop Opera
 7:00 pm | Bob's Burgers | Full Bars
 7:30 pm | Bob's Burgers | Fort Night
 8:00 pm | Bob's Burgers | Tina and the Real Ghost
 8:30 pm | Bob's Burgers | The Hauntening
 9:00 pm | American Dad! | Steve's Franken Out
 9:30 pm | American Dad! | Best Little Horror House in Langley Falls
10:00 pm | American Dad! | Multiverse of American Dadness
10:30 pm | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
11:00 pm | Rick and Morty | How Poopy Got His Poop Back
11:30 pm | Royal Crackers | Casa de Darby
12:00 am | The Eric Andre Show | Woodchipper HIjinks
12:15 am | The Eric Andre Show | Don't You Say a Word
12:30 am | Aqua Teen Hunger Force | Sweet C
12:45 am | Aqua Teen Hunger Force | Brain Fairy
 1:00 am | American Dad! | Steve's Franken Out
 1:30 am | American Dad! | Best Little Horror House in Langley Falls
 2:00 am | American Dad! | Multiverse of American Dadness
 2:30 am | Rick and Morty | Ricktional Mortpoon's Rickmas Mortcation
 3:00 am | Rick and Morty | How Poopy Got His Poop Back
 3:30 am | Royal Crackers | Casa de Darby
 4:00 am | The Eric Andre Show | Woodchipper HIjinks
 4:15 am | The Eric Andre Show | Don't You Say a Word
 4:30 am | Aqua Teen Hunger Force | Sweet C
 4:45 am | Aqua Teen Hunger Force | Brain Fairy
 5:00 am | Bob's Burgers | Full Bars
 5:30 am | Bob's Burgers | Fort Night