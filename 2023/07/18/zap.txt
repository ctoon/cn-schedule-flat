# 2023-07-18
 6:00 am | Amazing World of Gumball | The Comic
 6:30 am | Amazing World of Gumball | The Romantic
 7:00 am | Amazing World of Gumball | The Uploads
 7:30 am | Cocomelon | Wait Your Turn
 7:45 am | Meet the Batwheels | When You're a Jet
 8:00 am | Cocomelon | Lost Hamster
 8:15 am | Thomas & Friends: All Engines Go | Rules of the Game
 8:30 am | Cocomelon | Wheels on the Bus School
 8:45 am | Bugs Bunny Builders | K-9: Space Puppy
 9:00 am | The Looney Tunes Show | The Shell Game
 9:30 am | The Looney Tunes Show | Bobcats on Three
10:00 am | Total DramaRama | Toys Will Be Toys
10:30 am | Craig of the Creek | Lost & Found
11:00 am | Amazing World of Gumball | The Apprentice
11:30 am | Amazing World of Gumball | The Hug
12:00 pm | Summer Camp Island | Hark the Gerald Sings
12:30 pm | Clarence | Lost in the Supermarket
 1:00 pm | Teen Titans Go! | EEbows
 1:30 pm | Teen Titans Go! | Batman's Birthday Gift
 2:00 pm | Amazing World of Gumball | The Triangle; The Money
 2:30 pm | Amazing World of Gumball | The Treasure
 3:00 pm | Amazing World of Gumball | The Nemesis
 3:30 pm | Amazing World of Gumball | The Others
 4:00 pm | Amazing World of Gumball | The Signature
 4:30 pm | Amazing World of Gumball | The Check
 5:00 pm | Craig of the Creek | War of the Pieces
 5:30 pm | Teen Titans Go! | Fat Cats
 6:00 pm | Teen Titans Go! | Jam
 6:30 pm | Adventure Time | What Is Life?; Ocean of Fear