# 2023-07-20
 6:00 am | Amazing World of Gumball | The Butterfly; The Question
 6:30 am | Amazing World of Gumball | The Oracle; The Safety
 7:00 am | Amazing World of Gumball | The Friend; The Saint
 7:30 am | Cocomelon | Tie Your Shoes Song
 7:45 am | Meet the Batwheels | Ride Along
 8:00 am | Cocomelon | Balloon Boat Race
 8:15 am | Thomas & Friends: All Engines Go | Kana Goes Slow
 8:30 am | Cocomelon | Five Senses Song
 8:45 am | Bugs Bunny Builders | Cheddar Days
 9:00 am | The Looney Tunes Show | Gossamer Is Awesomer
 9:30 am | The Looney Tunes Show | Itsy Bitsy Gopher
10:00 am | Total DramaRama | Mother of All Cards
10:30 am | Craig of the Creek | Galactic Goodbyes
11:00 am | Amazing World of Gumball | The Society; The Spoiler
11:30 am | Amazing World of Gumball | The Countdown; The Nobody
12:00 pm | Summer Camp Island | Barb and the Spotted Bears Chapter 2: Hot Milk and Careless Whispers
12:30 pm | Clarence | Clarence Gets a Girlfriend
 1:00 pm | Teen Titans Go! | Fat Cats
 1:30 pm | Teen Titans Go! | Jam
 2:00 pm | Amazing World of Gumball | The Comic
 2:30 pm | Amazing World of Gumball | The Romantic
 3:00 pm | Amazing World of Gumball | The Uploads
 3:30 pm | Amazing World of Gumball | The Apprentice
 4:00 pm | Amazing World of Gumball | The Hug
 4:30 pm | Amazing World of Gumball | The Wicked
 5:00 pm | Craig of the Creek | The Team Up
 5:30 pm | Teen Titans Go! | Space House
 6:30 pm | Adventure Time | The Duke; Donny