# 2023-07-02
 6:00 am | Amazing World of Gumball | The Flower; The Banana
 6:30 am | Amazing World of Gumball | The Phone; The Job
 7:00 am | Amazing World of Gumball | The Words; The Apology
 7:30 am | Amazing World of Gumball | The Watch; The Bet
 8:00 am | Uncle Grandpa | Shorts
 8:30 am | Uncle Grandpa | Treasure Map
 9:00 am | Scooby-Doo! Stage Fright | 
10:45 am | We Bare Bears | Planet Bears
11:00 am | We Bare Bears | Captain Craboo
11:30 am | We Bare Bears | Baby Bears on a Plane
12:00 pm | Teen Titans Go! | Salty Codgers; Knowledge
12:30 pm | Teen Titans Go! | Love Monsters; Baby Hands
 1:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 1:30 pm | Teen Titans Go! | Friendship; Vegetables
 2:00 pm | Summer Camp Island | French Toasting
 2:30 pm | Summer Camp Island | Catacombs
 3:00 pm | Summer Camp Island | The Later Pile
 3:30 pm | Summer Camp Island | Light as a Feather
 4:00 pm | Amazing World of Gumball | The Bumpkin; The Flakers
 4:30 pm | Amazing World of Gumball | The Authority; The Virus
 5:00 pm | Amazing World of Gumball | The Pony; The Storm
 5:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 6:00 pm | Unicorn: Warriors Eternal | The Mystery of Secrets
 6:30 pm | Unicorn: Warriors Eternal | The Heart of Kings