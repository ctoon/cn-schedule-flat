# 2023-07-26
 7:00 pm | King of the Hill | To Sirloin With Love
 7:30 pm | King of the Hill | The Honeymooners
 8:00 pm | King of the Hill | Bill Gathers Moss
 8:30 pm | King of the Hill | When Joseph Met Lori & Made Out
 9:00 pm | Bob's Burgers | The Oeder Games
 9:30 pm | Bob's Burgers | Sliding Bobs
10:00 pm | American Dad! | American Stepdad
10:30 pm | American Dad! | Adventures in Hayleysitting
11:00 pm | American Dad! | Viced Principal
11:30 pm | Rick and Morty | Meeseeks and Destroy
12:00 am | Teenage Euthanasia | Remember Fun?
12:30 am | Royal Crackers | Theo's Comeback Tour
 1:00 am | The Boondocks | A Huey Freeman Christmas
 1:30 am | Mike Tyson Mysteries | Time to Fly
 1:45 am | Mike Tyson Mysteries | Make a Wish and Blow
 2:00 am | Aqua TV Show Show | Freda
 2:15 am | Aqua TV Show Show | Storage Zeebles
 2:30 am | Rick and Morty | Meeseeks and Destroy
 3:00 am | Rick and Morty | Rick Potion #9
 3:30 am | Teenage Euthanasia | Remember Fun?
 4:00 am | American Dad! | American Stepdad
 4:30 am | American Dad! | Adventures in Hayleysitting
 5:00 am | Bob's Burgers | The Oeder Games
 5:30 am | Bob's Burgers | Sliding Bobs