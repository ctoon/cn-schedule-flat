# 2023-07-15
 6:00 am | Amazing World of Gumball | The Procrastinators; The Shell
 6:30 am | Amazing World of Gumball | The Mirror; The Burden
 7:00 am | Amazing World of Gumball | The Bros; The Man
 7:30 am | Amazing World of Gumball | The Pizza; The Lie
 8:00 am | Teen Titans Go! | Multiple Trick Pony; Rocks and Water
 8:30 am | Teen Titans Go! | Truth, Justice, and What?; Beast Man
 9:00 am | Teen Titans Go! | The Brain of the Family
 9:30 am | Looney Tunes Cartoons | Grand Canyon Canary Hole in Dumb; Funeral for a Fudd Love Goat
10:00 am | We Baby Bears | A Polly's New Crew; Real Crayon
10:30 am | Total DramaRama | Be Claws I Love You; Shelley; Free Chili
11:00 am | Total DramaRama | Total Trauma Rama; Cuttin' Corners
11:30 am | Total DramaRama | Doomed Ballooned Marooned; The Sharing Is Caring
12:00 pm | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
12:30 pm | Craig of the Creek | War of the Pieces
 1:00 pm | Craig of the Creek | The League of Maya's Own; A; Cheese Stands Alone
 1:30 pm | Craig of the Creek | Team up; The Putting Together the Pieces
 2:00 pm | Craig of the Creek | Heart of the Forest Finale
 2:30 pm | Amazing World of Gumball | The Nemesis; The Comic
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Oracle; The Safety
 4:00 pm | Amazing World of Gumball | The Friend; The Saint
 4:30 pm | Amazing World of Gumball | The Spoiler; The Society
 5:00 pm | Scooby-Doo! Abracadabra-Doo! | 
 6:45 pm | Amazing World of Gumball | The Singing