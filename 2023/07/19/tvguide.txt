# 2023-07-19
 6:00 am | Amazing World of Gumball | The Traitor; The Compilation
 6:30 am | Amazing World of Gumball | Origins; The Origins Part 2; the Robin's Ride
 7:00 am | Amazing World of Gumball | The Disaster; The Re-run
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Scaredy-Bat
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | A Quiet Delivery
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Cousin Billy Hard Hat Time
 9:00 am | The Looney Tunes Show | Year of the Duck Daffy's Spa
 9:30 am | The Looney Tunes Show | You've Got Hate Mail
10:00 am | Total DramaRama | Gum And Gummer; Mutt Ado About Owen
10:30 am | Craig of the Creek | Champion's Hike; The Craig of the Street
11:00 am | Amazing World of Gumball | The Stories; The Guy
11:30 am | Amazing World of Gumball | The Boredom;The Potato
12:00 pm | Summer Camp Island | Pepper and the Fog; Barb and the Spotted Bears Chapter 1: A Barb Is Born
12:30 pm | Clarence | Man of the House; Clarence's Millions
 1:00 pm | Teen Titans Go! | The Cape; The Fourth Wall
 1:30 pm | Teen Titans Go! | The Dreams Mask
 2:00 pm | Amazing World of Gumball | The Pest; The Parasite
 2:30 pm | Amazing World of Gumball | The Sale; The Love
 3:00 pm | Amazing World of Gumball | The Gift; The Awkwardness
 3:30 pm | Amazing World of Gumball | The Parking; The Nest
 4:00 pm | Amazing World of Gumball | The Routine; The Points
 4:30 pm | Amazing World of Gumball | The Upgrade; The Bus
 5:00 pm | Craig of the Creek | The League of Maya's Own; A; Cheese Stands Alone
 5:30 pm | Teen Titans Go! | The Cape; The Academy
 6:00 pm | Teen Titans Go! | Mas y Menos Caramel Apples
 6:30 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | When Wedding Bells Thaw; Freak City