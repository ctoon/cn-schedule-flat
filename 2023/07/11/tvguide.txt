# 2023-07-11
 6:00 am | Amazing World of Gumball | The Kids; The Fan
 6:30 am | Amazing World of Gumball | Coach; The Joy; the Bam!
 7:00 am | Amazing World of Gumball | Recipe; The Puppy; the Surprise!
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | A Zoomsday Joking Matter
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Speedster Sandy; Toothbrush
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Bright Light
 9:00 am | The Looney Tunes Show | Mrs. Porkbunny's Buckle Up
 9:30 am | The Looney Tunes Show | Beauty School
10:00 am | Craig of the Creek | Craig to the Future; Craiggy & the Slime Factory
10:30 am | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
11:00 am | Amazing World of Gumball | The Name; The Extras
11:30 am | Amazing World of Gumball | The Gripes; The Vacation
12:00 pm | Summer Camp Island | Sea Bunnies; Mushrumours
12:30 pm | Clarence | Belson Gets a Girlfriend; Video Store
 1:00 pm | Teen Titans Go! | The Mug; Don't Press Play
 1:30 pm | Teen Titans Go! | Real Art; Hafo Safo
 2:00 pm | Amazing World of Gumball | The Bumpkin; the Flakers
 2:30 pm | Amazing World of Gumball | The Authority; The Virus
 3:00 pm | Amazing World of Gumball | The Pony; The Storm
 3:30 pm | Amazing World of Gumball | The Dream; The Sidekick
 4:00 pm | Amazing World of Gumball | The Hero; The Photo
 4:30 pm | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
 5:00 pm | Craig of the Creek | War of the Pieces
 5:30 pm | Teen Titans Go! | Marv Wolfman and George Pérez; Manor and Mannerisms
 6:00 pm | Teen Titans Go! | Trans Oceanic Magical Cruise; Polly Ethylene and Tara Phthalate
 6:30 pm | Adventure Time | Enchiridion; The Jiggler