# 2023-07-29
 7:00 pm | King of the Hill | Nine Pretty Darn Angry Men
 7:30 pm | King of the Hill | Good Hill Hunting
 8:00 pm | King of the Hill | Pretty, Pretty Dresses
 8:30 pm | King of the Hill | A Fire Fighting We Will Go
 9:00 pm | Rick and Morty | Childrick of Mort
 9:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
10:00 pm | American Dad! | Spelling Bee My Baby
10:30 pm | American Dad! | The Missing Kink
11:00 pm | American Dad! | The Full Cognitive Redaction of Avery Bullock by the Coward Stan Smith
11:30 pm | American Dad! | Lost in Space
12:00 am | My Adventures with Superman | You Will Believe a Man Can Lie
12:30 am | Dr. Stone | Ray of Despair, Ray of Hope
 1:00 am | FLCL | Marquis de Carabas
 1:30 am | One Piece | Intense! Aokiji vs. Doflamingo!
 2:00 am | Naruto: Shippuden | Comrade
 2:30 am | My Hero Academia | Threads of Hope
 3:00 am | The Venture Brothers | The Inamorata Consequence
 3:30 am | Metalocalypse | Bookklok
 3:45 am | Metalocalypse | Writersklok
 4:00 am | Futurama | Leela and the Genestalk
 4:30 am | Futurama | Game of Tones
 5:00 am | Futurama | Murder on the Planet Express
 5:30 am | Futurama | Stench and Stenchibility