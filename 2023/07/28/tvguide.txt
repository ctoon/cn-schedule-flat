# 2023-07-28
 6:00 am | Amazing World of Gumball | The Catfish; The Worst
 6:30 am | Amazing World of Gumball | Cycle; The Deal; the Buff Tuff
 7:00 am | Amazing World of Gumball | The Stars; The; Petals
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Tough Buff Blues; Wide Load Vacay: Eye Spy
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | Macbarm's Day off Playhouse Problems Lemonade
 8:45 am | Bugs Bunny Builders | Bright Light
 9:00 am | The Looney Tunes Show | Father Figures Buckle Up
 9:30 am | The Looney Tunes Show | It's a Handbag
10:00 am | Total DramaRama | A-Bok-Bok-Bokalypse; The Royal Flush
10:30 am | Craig of the Creek | Heart of the Forest Finale
11:00 am | Amazing World of Gumball | The Grades; The Puppets
11:30 am | Amazing World of Gumball | The Diet; The Nuisance
12:00 pm | Summer Camp Island | The Babies Chapter 2: Teacup Giant; the Babies Chapter 3: Lem Is Nothing
12:30 pm | Clarence | Too Gross For Comfort; The Forgotten
 1:00 pm | Teen Titans Go! | Teen Titans Action
 1:30 pm | Teen Titans Go! | Stickier Situation; A; Winning a Golf Tournament Is the Solution to All of Life's Money Problems
 2:00 pm | Amazing World of Gumball | The Schooling; The Anybody
 2:30 pm | Amazing World of Gumball | The Pact; The; Intelligence
 3:00 pm | Amazing World of Gumball | The Ad; The Potion
 3:30 pm | Amazing World of Gumball | The Spinoffs; The Slip
 4:00 pm | Amazing World of Gumball | The Stink; The Transformation
 4:30 pm | Amazing World of Gumball | The Awareness; The Understanding
 5:00 pm | Teen Titans Go! | Our House; Beard Hunter
 5:30 pm | Teen Titans Go! | Elasti-Bot; Negative Feels
 6:00 pm | Teen Titans Go! | New Chum
 6:15 pm | Teen Titans Go! | Utility Belt
 6:30 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | It Came From the Nightosphere; The Eyes