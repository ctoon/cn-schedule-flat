# 2023-07-28
 6:00 am | Amazing World of Gumball | The Catfish
 6:30 am | Amazing World of Gumball | The Cycle
 7:00 am | Amazing World of Gumball | The Stars
 7:30 am | Cocomelon | Itsy Bitsy Spider
 7:45 am | Meet the Batwheels | Tough Buff Blues
 8:00 am | Cocomelon | Old MacDonald
 8:15 am | Mecha Builders | MacBarm's Day Off/Playhouse Problems
 8:45 am | Bugs Bunny Builders | Bright Light
 9:00 am | The Looney Tunes Show | Father Figures
 9:30 am | The Looney Tunes Show | It's a Handbag!
10:00 am | Craig of the Creek | The Great Fossil Rush
10:30 am | Craig of the Creek | Heart of the Forrest Finale
11:00 am | Amazing World of Gumball | The Grades
11:30 am | Amazing World of Gumball | The Diet
12:00 pm | Summer Camp Island | The Babies Chapter 2: Teacup Giant
12:30 pm | Clarence | The Forgotten
 1:00 pm | Teen Titans Go! | Teen Titans Action
 1:30 pm | Teen Titans Go! | A Stickier Situation
 2:00 pm | Amazing World of Gumball | The Anybody
 2:30 pm | Amazing World of Gumball | The Pact
 3:00 pm | Amazing World of Gumball | The Potion
 3:30 pm | Amazing World of Gumball | The Spinoffs
 4:00 pm | Amazing World of Gumball | The Transformation
 4:30 pm | Amazing World of Gumball | The Understanding
 5:00 pm | Teen Titans Go! | Our House
 5:30 pm | Teen Titans Go! | Elasti-Bot
 6:00 pm | Teen Titans Go! | New Chum
 6:15 pm | Teen Titans Go! | Utility Belt
 6:30 pm | Adventure Time | It Came From the Nightosphere; The Eyes