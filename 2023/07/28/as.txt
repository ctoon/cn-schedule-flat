# 2023-07-28
 7:00 pm | King of the Hill | Talking Shop
 7:30 pm | King of the Hill | A Rover Runs Through It
 8:00 pm | King of the Hill | Next of Shin
 8:30 pm | King of the Hill | Peggy's Pageant Fever
 9:00 pm | Bob's Burgers | Gayle Makin' Bob Sled
 9:30 pm | Bob's Burgers | Nice-Capades
10:00 pm | American Dad! | Blood Crieth Unto Heaven
10:30 pm | American Dad! | Max Jets
11:00 pm | American Dad! | Naked to the Limit, One More Time
11:30 pm | American Dad! | For Black Eyes Only
12:00 am | Futurama | Bender's Big Score Part 1
12:30 am | Futurama | Bender's Big Score Part 2
 1:00 am | Futurama | Bender's Big Score Part 3
 1:30 am | Futurama | Bender's Big Score Part 4
 2:00 am | Futurama | Bender's Game Part 1
 2:30 am | Futurama | Bender's Game Part 2
 3:00 am | Futurama | Bender's Game Part 3
 3:30 am | Futurama | Bender's Game Part 4
 4:00 am | American Dad! | Blood Crieth Unto Heaven
 4:30 am | American Dad! | Max Jets
 5:00 am | Bob's Burgers | Gayle Makin' Bob Sled
 5:30 am | Bob's Burgers | Nice-Capades