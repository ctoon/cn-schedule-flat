# 2023-07-06
 6:00 am | Amazing World of Gumball | The Spinoffs; The Revolt
 6:30 am | Amazing World of Gumball | A Transformation; The Mess; the Joking Matter
 7:00 am | Amazing World of Gumball | The Understanding; The Possession
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Rev and Let Rev; Sneeze
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Blue Engine Blues
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Bright Light; Wide Load Vacay: Eye Spy
 9:00 am | The Looney Tunes Show | Spread Those Wings and Fly Boogie Button
 9:30 am | We Baby Bears | 45 Bug City Frame-up; Meet the Shrink
10:00 am | Total DramaRama | The Bad Guy Busters; Duck Duck Juice
10:30 am | Total DramaRama | Cluckwork Orange; That's a Wrap
11:00 am | Craig of the Creek | Snow Day; Craig World
11:30 am | Craig of the Creek | Snow Place Like Home; Body Swap
12:00 pm | Summer Camp Island | Puddle and the King Chapter 3: All the King's Slides; Yeti Confetti Chapter 1: Don't Tell Lucy
12:30 pm | Clarence | Nightmare on Aberdale Street: Balance's Revenge; a; Trampoline
 1:00 pm | Teen Titans Go! | Thumb War; Lucky Stars
 1:30 pm | Teen Titans Go! | Toddler Titans...Yay! ; Various Modes of Transportation
 2:00 pm | Amazing World of Gumball | The Party; The Refund
 2:30 pm | Amazing World of Gumball | The Robot; The Picnic
 3:00 pm | Amazing World of Gumball | The Goons; The Secret
 3:30 pm | Amazing World of Gumball | The Sock; The Genius
 4:00 pm | Amazing World of Gumball | The Mustache; The Date
 4:30 pm | Amazing World of Gumball | The Club; The Wand
 5:00 pm | Craig of the Creek | The Other Side: The Tournament
 5:30 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl Sanpedro
 6:30 pm | We Bare Bears | Library; The Ralph