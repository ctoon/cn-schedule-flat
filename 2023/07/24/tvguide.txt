# 2023-07-24
 6:00 am | Amazing World of Gumball | The Sorcerer; The List
 6:30 am | Amazing World of Gumball | Menu; The Traffic; the Bad to the Chrome
 7:00 am | Amazing World of Gumball | The Uncle; The Rival
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | A Tough Buff Blues Joking Matter
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Dragon Run
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Sea School
 9:00 am | The Looney Tunes Show | Here Comes the Pig Hard Hat Time
 9:30 am | The Looney Tunes Show | Semper Lie
10:00 am | Total DramaRama | An Driving Miss Crazy; Egg-Stremely Bad Idea
10:30 am | Craig of the Creek | The Who Is the Red Poncho? ; Once and Future King
11:00 am | Amazing World of Gumball | The Weirdo; The Lady
11:30 am | Amazing World of Gumball | The Heist; The Sucker
12:00 pm | Summer Camp Island | Betsy and Ghost Chapter 3: There's a Racket in My Hope Chest; Susie and Her Sister Chapter 1: Heathers
12:30 pm | Clarence | Dinner Party; Slumber Party
 1:00 pm | Teen Titans Go! | Free Perk; Sweet Revenge
 1:30 pm | Teen Titans Go! | Go! ; Porch Pirates
 2:00 pm | Amazing World of Gumball | The Outside; The Choices
 2:30 pm | Amazing World of Gumball | The Styles; The Xxxxx
 3:00 pm | Amazing World Of Gumball | The Matchmaker; The Copycats
 3:30 pm | Amazing World of Gumball | The Test; The Box
 4:00 pm | Teen Titans Go! | Super Summer Hero Camp
 5:00 pm | Teen Titans Go! | Beast Boy's That's What's Up
 6:00 pm | Teen Titans Go! | Our House
 6:15 pm | Teen Titans Go! | Kyle
 6:30 pm | Adventure Time | Henchman; Dungeon