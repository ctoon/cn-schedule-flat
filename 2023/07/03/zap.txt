# 2023-07-03
 6:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 6:30 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Bobert?
 7:00 am | Amazing World of Gumball | The Responsible; The Dress
 7:30 am | Cocomelon | Ten in the Bed
 7:45 am | Meet the Batwheels | When You're a Jet
 8:00 am | Cocomelon | Toy Balloon Car Race
 8:15 am | Thomas & Friends: All Engines Go | Retrieve the Kraken
 8:30 am | Cocomelon | The Hiccup Song
 8:45 am | Bugs Bunny Builders | Sea School
 9:00 am | The Looney Tunes Show | We're In Big Truffle
 9:30 am | We Baby Bears | The Great Veggie War
10:00 am | Total DramaRama | Ticking Crime Bomb
10:30 am | Total DramaRama | Knit Wit
11:00 am | Craig of the Creek | The Other Side: The Tournament
11:30 am | Craig of the Creek | Creature Feature
12:00 pm | Summer Camp Island | Susie and Ramona Chapter 1: Susie's Ark
12:30 pm | Summer Camp Island | Susie and Ramona Chapter 3: Meet Me in Massachusetts
 1:00 pm | Teen Titans Go! | Where Exactly on the Globe Is Carl SanPedro
 2:00 pm | Amazing World of Gumball | The Potion
 2:30 pm | Amazing World of Gumball | The Spinoffs
 3:00 pm | Amazing World of Gumball | The Transformation
 3:30 pm | Amazing World of Gumball | The Understanding
 4:00 pm | Amazing World of Gumball | The Heart
 4:30 pm | Amazing World of Gumball | The Agent
 5:00 pm | Craig of the Creek | Alternate Creekiverse
 5:30 pm | Teen Titans Go! | TV Knight 6
 6:00 pm | Teen Titans Go! | Kryptonite
 6:30 pm | We Bare Bears | Icy Nights