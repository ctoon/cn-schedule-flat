# 2023-07-16
 6:00 am | Amazing World of Gumball | The Countdown; The Nobody
 6:30 am | Amazing World of Gumball | The Downer; The Egg
 7:00 am | Amazing World of Gumball | The Triangle; The Money
 7:30 am | Amazing World of Gumball | The Return
 8:00 am | Uncle Grandpa | Big in Japan
 8:30 am | Uncle Grandpa | Perfect Kid
 9:00 am | Scooby-Doo! Camp Scare | 
10:45 am | We Bare Bears | The Demon
11:00 am | We Bare Bears | Everyone's Tube
11:30 am | We Bare Bears | Creature Mysteries
12:00 pm | Teen Titans Go! | Campfire Stories; The Hive Five
12:30 pm | Teen Titans Go! | The Return of Slade; More of the Same
 1:00 pm | Teen Titans Go! | And the Award for Sound Design Goes to Rob; Some of Their Parts
 1:30 pm | Teen Titans Go! | Cat's Fancy
 2:00 pm | Craig of the Creek | Who Is the Red Poncho?
 2:30 pm | Craig of the Creek | War of the Pieces
 3:00 pm | Craig of the Creek | A League of Maya's Own
 3:30 pm | Craig of the Creek | The Team Up
 4:00 pm | Craig of the Creek | Heart of the Forrest Finale
 4:30 pm | Amazing World of Gumball | The Crew
 5:00 pm | Amazing World of Gumball | The Others
 5:30 pm | Amazing World of Gumball | The Signature
 6:00 pm | Ocean's Eleven | 