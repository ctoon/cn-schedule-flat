# 2023-07-31
 7:00 pm | King of the Hill | De-Kahnstructing Henry
 7:30 pm | King of the Hill | The Wedding of Bobby Hill
 8:00 pm | King of the Hill | Sleight of Hank
 8:30 pm | King of the Hill | Jon Vitti Presents: Return to La Grunta
 9:00 pm | King of the Hill | Escape from Party Island
 9:30 pm | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
10:00 pm | Bob's Burgers | The Gene & Courtney Show
10:30 pm | American Dad! | Da Flippity Flop
11:00 pm | American Dad! | Steve and Snot's Test-Tubular Adventure
11:30 pm | Rick and Morty | Something Ricked This Way Comes
12:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
12:30 am | Royal Crackers | Stebe
 1:00 am | The Boondocks | Let's Nab Oprah
 1:30 am | Mike Tyson Mysteries | Pits and Peaks
 1:45 am | Mike Tyson Mysteries | The Monahans and MacGoverns
 2:00 am | Aqua Teen Hunger Force Forever | Mouth Quest
 2:15 am | Aqua Teen Hunger Force Forever | Brain Fairy
 2:30 am | Rick and Morty | Something Ricked This Way Comes
 3:00 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 3:30 am | Royal Crackers | Stebe
 4:00 am | American Dad! | Da Flippity Flop
 4:30 am | American Dad! | Steve and Snot's Test-Tubular Adventure
 5:00 am | Bob's Burgers | The Cook, the Steve, the Gayle, & Her Lover
 5:30 am | Bob's Burgers | The Gene & Courtney Show