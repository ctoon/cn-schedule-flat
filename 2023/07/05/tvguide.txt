# 2023-07-05
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | A Quest; The Spoon; the Joking Matter
 7:00 am | Amazing World of Gumball | The Car; The Curse
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Batcomputer for a Day; Spiderweb
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Details? What Details?
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Underwater Star; Wide Load Vacay: Big City Dancers
 9:00 am | The Looney Tunes Show | Dear John Hard Hat Time
 9:30 am | We Baby Bears | Tooth Fairy Tech; High Baby Fashion; Sunday Holiday
10:00 am | Total DramaRama | Total Dramarama: A Very Special; Special That's Quite Special
11:00 am | Craig of the Creek | I Don't Need a Hat New Jersey
11:30 am | Craig of the Creek | Alternate Creekiverse; The Sunflower
12:00 pm | Summer Camp Island | Puddle and the King Chapter 1: Honey Moondog; Puddle and the King Chapter 2: Royally Bored
12:30 pm | Clarence | Trade; The Dog King Clarence
 1:00 pm | Teen Titans Go! | The TV Knight 6; Cast
 1:30 pm | Teen Titans Go! | Kryponite; Superhero Feud
 2:00 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Penny? ; the Gumball Chronicles: Vote Gumball...and Leslie? ; the Gumball Chronicles:
 2:30 pm | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball...and Bobert? ; the; Gumball Chronicles: Vote Gumball...and Anyone?
 3:00 pm | Amazing World of Gumball | The Responsible; The Dress
 3:30 pm | Amazing World of Gumball | The Laziest; The Ghost
 4:00 pm | Amazing World of Gumball | The Mystery; The Prank
 4:30 pm | Amazing World of Gumball | The GI; The Kiss
 5:00 pm | Craig of the Creek | Jessica the Intern Brother Builder
 5:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 6:00 pm | Teen Titans Go! | Cool Uncles Villains in a Van; Getting Gelato
 6:30 pm | We Bare Bears | Creature Mysteries; Professor Lampwick