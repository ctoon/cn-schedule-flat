# 2023-07-07
 7:00 pm | My Adventures with Superman | Adventures of a Normal Man Pt. 1
 7:30 pm | My Adventures with Superman | Adventures of a Normal Man Pt. 2
 8:00 pm | King of the Hill | Bystand Me
 8:30 pm | King of the Hill | Bill's House
 9:00 pm | Bob's Burgers | Equestranauts
 9:30 pm | Bob's Burgers | Ambergris
10:00 pm | American Dad! | Rapture's Delight
10:30 pm | American Dad! | Don't Look a Smith Horse in the Mouth
11:00 pm | American Dad! | A Jones for a Smith
11:30 pm | American Dad! | May the Best Stan Win
12:00 am | Robot Chicken | Robot Chicken: DC Comics Special
12:30 am | Robot Chicken | Robot Chicken: DC Comics Special II: Villains in Paradise
 1:00 am | Robot Chicken | Robot Chicken DC: Comics Special III: Magical Friendship
 1:30 am | Robot Chicken | May Cause Weebles to Fall Down
 1:45 am | Robot Chicken | May Cause Season 11 to End
 2:00 am | Rick and Morty | The Rickchurian Mortydate
 2:30 am | American Dad! | Rapture's Delight
 3:00 am | American Dad! | Don't Look a Smith Horse in the Mouth
 3:30 am | Futurama | Rebirth
 4:00 am | Futurama | In-A-Gadda-Da-Leela
 4:30 am | Futurama | Attack of the Killer App
 5:00 am | Bob's Burgers | Equestranauts
 5:30 am | Bob's Burgers | Ambergris