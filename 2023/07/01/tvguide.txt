# 2023-07-01
 6:00 am | Amazing World of Gumball | The Ape; The Poltergeist
 6:30 am | Amazing World of Gumball | The Quest; The Spoon
 7:00 am | Amazing World of Gumball | The Car; The Curse
 7:30 am | Amazing World of Gumball | The Microwave; The Meddler
 8:00 am | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 8:30 am | Teen Titans Go! | Dreams; Grandma Voice
 9:00 am | Teen Titans Go! | Winning a Golf Tournament Is the Solution to All of Life's Money Problems; Always Be Crimefighting
 9:30 am | Looney Tunes Cartoons | Stained by Me Pilgwim's Pwogwess; Hook Line and Stinker! Balloon Salesman: Everything Pops Don't Treadmill on Me
10:00 am | We Baby Bears | The Great Veggie War
10:30 am | Total DramaRama | Ticking Crime Bomb; Venthalla
11:00 am | Total DramaRama | The Knit Wit; Date
11:30 am | Total DramaRama | The Duck Duck Juice; Fuss on the Bus
12:00 pm | Craig of the Creek | Plush Kingdom; Fall Anthology
12:30 pm | Craig of the Creek | Ice Pop Trio; The Afterschool Snackdown
 1:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 1:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 2:00 pm | Teen Titans Go! | Pirates; I See You
 2:30 pm | Teen Titans Go! | Brian/Nature
 3:00 pm | Amazing World of Gumball | The Helmet; The Fight
 3:30 pm | Amazing World of Gumball | The End; The DVD
 4:00 pm | Amazing World of Gumball | The Knights; The Colossus
 4:30 pm | Amazing World of Gumball | The Fridge; The Remote
 5:00 pm | Scooby-Doo! Mask of the Blue Falcon | 
 6:45 pm | Amazing World of Gumball | The Deal