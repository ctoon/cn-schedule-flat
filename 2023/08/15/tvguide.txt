# 2023-08-15
 6:00 am | Amazing World of Gumball | The Traitor; The Parking
 6:30 am | Amazing World of Gumball | Routine; The Girlfriend; the Bam's Bubble Trouble
 7:00 am | Amazing World of Gumball | Origins; The; Origins Part 2; the Carly & Cranky's Big Lift-Off
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Wheel Side Story
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Overnight Stop
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Cheesy Peasy
 9:00 am | The Looney Tunes Show | Double Date Daffy's Spa
 9:30 am | The Looney Tunes Show | Year of the Duck
10:00 am | Craig of the Creek | Winter Break
10:30 am | Craig of the Creek | End Was Here; The Ancients of the Creek
11:00 am | Amazing World Of Gumball | The Advice; The Roots
11:30 am | Amazing World of Gumball | The Signal; The Blame
12:00 pm | Summer Camp Island | Chapter 3: See Bees? Gee! Bees! ; Chapter 4: Pepper's Funeral
12:30 pm | Clarence | Saturday School; Time Crimes
 1:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 1:30 pm | Teen Titans Go! | No Power; Sidekick
 2:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 2:30 pm | Amazing World of Gumball | The Egg; The Downer
 3:00 pm | Amazing World of Gumball | The Triangle; The Money
 3:30 pm | Amazing World of Gumball | The Treasure; The Skull
 4:00 pm | Amazing World of Gumball | The Return; The Nemesis
 4:30 pm | Amazing World of Gumball | The Upgrade; The Crew