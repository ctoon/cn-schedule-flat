# 2023-08-03
 7:00 pm | King of the Hill | Peggy Hill: the Decline and Fall (2)
 7:30 pm | King of the Hill | Cotton's Plot
 8:00 pm | King of the Hill | Bills are Made to be Broken
 8:30 pm | King of the Hill | Little Horrors of Shop
 9:00 pm | Bob's Burgers | Stand by Gene
 9:30 pm | Bob's Burgers | Wag the Hog
10:00 pm | American Dad! | Faking Bad
10:30 pm | American Dad! | Minstrel Krampus
11:00 pm | American Dad! | Vision: Impossible
11:30 pm | Rick and Morty | Total Rickall
12:00 am | My Adventures with Superman | My Adventures With Mad Science
12:30 am | Royal Crackers | The .1%
 1:00 am | The Boondocks | The Story of Thugnificent
 1:30 am | Mike Tyson Mysteries | The Gift That Keeps on Giving
 1:45 am | Mike Tyson Mysteries | Your Old Man
 2:00 am | Aqua Teen Hunger Force Forever | The Last One Forever and Ever (For Real This Time)(We ****ing Mean It)
 2:30 am | Rick and Morty | Total Rickall
 3:00 am | Rick and Morty | Get Schwifty
 3:30 am | Royal Crackers | The .1%
 4:00 am | American Dad! | Faking Bad
 4:30 am | American Dad! | Minstrel Krampus
 5:00 am | Bob's Burgers | Stand by Gene
 5:30 am | Bob's Burgers | Wag the Hog