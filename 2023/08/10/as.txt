# 2023-08-10
 7:00 pm | King of the Hill | Peggy's Fan Fair
 7:30 pm | King of the Hill | The Perils of Polling
 8:00 pm | King of the Hill | The Buck Stops Here
 8:30 pm | King of the Hill | I Don't Want to Wait
 9:00 pm | Bob's Burgers | Teen-a Witch
 9:30 pm | Bob's Burgers | They Serve Horses, Don't They?
10:00 pm | American Dad! | CIAPOW
10:30 pm | American Dad! | Scents and Sensei-bility
11:00 pm | American Dad! | Big Stan on Campus
11:30 pm | Rick and Morty | Pickle Rick
12:00 am | My Adventures with Superman | Kiss Kiss Fall In Portal
12:30 am | Royal Crackers | Theo's Comeback Tour
 1:00 am | The Boondocks | Ballin'
 1:30 am | Mike Tyson Mysteries | Clam Bam Thank You Ma'am
 1:45 am | Mike Tyson Mysteries | You Can't Go Home Again
 2:00 am | Aqua Teen Hunger Force | Balloonenstein
 2:15 am | Aqua Teen Hunger Force | Space Conflict from Beyond Pluto
 2:30 am | Rick and Morty | Pickle Rick
 3:00 am | Rick and Morty | Vindicators 3: The Return of Worldender
 3:30 am | Royal Crackers | Theo's Comeback Tour
 4:00 am | American Dad! | CIAPOW
 4:30 am | American Dad! | Scents and Sensei-bility
 5:00 am | Bob's Burgers | Teen-a Witch
 5:30 am | Bob's Burgers | They Serve Horses, Don't They?