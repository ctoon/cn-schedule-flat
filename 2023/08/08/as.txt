# 2023-08-08
 7:00 pm | King of the Hill | Movin' On Up
 7:30 pm | King of the Hill | Bill of Sales
 8:00 pm | King of the Hill | Won't You Pimai Neighbor?
 8:30 pm | King of the Hill | Hank's Bad Hair Day
 9:00 pm | Bob's Burgers | Secret Admiral-Irer
 9:30 pm | Bob's Burgers | Glued, Where's My Bob?
10:00 pm | American Dad! | The Longest Distance Relationship
10:30 pm | American Dad! | A Boy Named Michael
11:00 pm | American Dad! | Roger Passes the Bar
11:30 pm | Rick and Morty | Look Who's Purging Now
12:00 am | Rick and Morty | The Wedding Squanchers
12:30 am | Royal Crackers | CrackerCon
 1:00 am | The Boondocks | Shinin'
 1:30 am | Mike Tyson Mysteries | Shop 'Til You Drop
 1:45 am | Mike Tyson Mysteries | The Christmas Episode
 2:00 am | Aqua Teen Hunger Force | Rabbot
 2:15 am | Aqua Teen Hunger Force | Escape from Leprechaupolis
 2:30 am | Rick and Morty | Look Who's Purging Now
 3:00 am | Rick and Morty | The Wedding Squanchers
 3:30 am | Royal Crackers | CrackerCon
 4:00 am | American Dad! | The Longest Distance Relationship
 4:30 am | American Dad! | A Boy Named Michael
 5:00 am | Bob's Burgers | Secret Admiral-Irer
 5:30 am | Bob's Burgers | Glued, Where's My Bob?