# 2023-08-21
 6:00 am | Amazing World of Gumball | The Worst; The Ex
 6:30 am | Amazing World of Gumball | The Sorcerer; The Deal; the Knight Shift
 7:00 am | Amazing World of Gumball | The Petals; The Menu
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | When You're A Jet
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Thomas' Day Off
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Skate Park
 9:00 am | The Looney Tunes Show | That's My Baby Buckle Up
 9:30 am | The Looney Tunes Show | Best Friends Redux
10:00 am | Craig of the Creek | Craig World; Body Swap
10:30 am | Craig of the Creek | The in the Key of the Creek; Last Game of Summer
11:00 am | Amazing World of Gumball | The Puppets; The Uncle
11:30 am | Amazing World of Gumball | The Nuisance; The One
12:00 pm | Summer Camp Island | Chapter 11: The Metaphysical Reserve; Chapter 12: Night's Pockets
12:30 pm | Clarence | Bird Boy Man; Company Man
 1:00 pm | Teen Titans Go! | Uncle Jokes; Mas Y Menos
 1:30 pm | Teen Titans Go! | Dreams; Grandma Voice
 2:00 pm | Amazing World of Gumball | The Misunderstandings; The Choices
 2:30 pm | Amazing World of Gumball | The Styles; The Outside
 3:00 pm | Amazing World of Gumball | The Astological; The Xxxxx
 3:30 pm | Amazing World of Gumball | The Test; The Matchmaker
 4:00 pm | Amazing World of Gumball | The Slide; The; Box
 4:30 pm | Amazing World of Gumball | The Loophole; The Console