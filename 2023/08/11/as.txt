# 2023-08-11
 7:00 pm | King of the Hill | Dale To the Chief
 7:30 pm | King of the Hill | The Petriot Act
 8:00 pm | King of the Hill | Spin the Choice
 8:30 pm | King of the Hill | Peggy Makes the Big Leagues
 9:00 pm | Bob's Burgers | Large Brother, Where Fart Thou?
 9:30 pm | Bob's Burgers | The Quirk-Ducers
10:00 pm | American Dad! | Now and Gwen
10:30 pm | American Dad! | Dreaming of a White Porsche Christmas
11:00 pm | American Dad! | LGBSteve
11:30 pm | American Dad! | Morning Mimosa
12:00 am | Space Ghost Coast to Coast | Snatch
12:15 am | Space Ghost Coast to Coast | Girl Hair
12:30 am | Space Ghost Coast to Coast | Kentucky Nightmare
12:45 am | Space Ghost Coast to Coast | Knifin' Around
 1:00 am | Space Ghost Coast to Coast | Flipmode
 1:15 am | Space Ghost Coast to Coast | Sweet for Brak
 1:30 am | Space Ghost Coast to Coast | Eat A Peach
 1:45 am | Space Ghost Coast to Coast | Idlewild South
 2:00 am | Rick and Morty | The Whirly Dirly Conspiracy
 2:30 am | American Dad! | Now and Gwen
 3:00 am | American Dad! | Dreaming of a White Porsche Christmas
 3:30 am | Futurama | Raging Bender
 4:00 am | Futurama | A Bicyclops Built for Two
 4:30 am | Futurama | How Hermes Requisitioned His Groove Back
 5:00 am | Bob's Burgers | Large Brother, Where Fart Thou?
 5:30 am | Bob's Burgers | The Quirk-Ducers