# 2023-08-28
 6:00 am | The Amazing World of Gumball: Darwin's Yearbook | Carrie
 6:30 am | The Amazing World of Gumball: Darwin's Yearbook | Sarah
 7:00 am | Amazing World of Gumball | The Slip
 7:30 am | Cocomelon | JJ Wants a New Bed
 7:45 am | Meet the Batwheels | Stop That Ducky!
 8:00 am | Cocomelon | Five Senses Song
 8:15 am | Thomas & Friends: All Engines Go | Percy in the Middle
 8:30 am | Cocomelon | Wait Your Turn
 8:45 am | Bugs Bunny Builders | Batty Kathy
 9:00 am | The Looney Tunes Show | French Fries
 9:30 am | The Looney Tunes Show | Beauty School
10:00 am | Craig of the Creek | Better Than You
10:30 am | Craig of the Creek | The Dream Team
11:00 am | Amazing World of Gumball | The Pressure; The Painting
11:30 am | Amazing World of Gumball | The Responsible; The Dress
12:00 pm | Summer Camp Island | The First Day
12:30 pm | Clarence | Game Show
 1:00 pm | Clarence | Capture the Flag
 1:30 pm | Craig of the Creek | In Search of Lore
 2:00 pm | Teen Titans Go! | Pirates; I See You
 2:30 pm | Teen Titans Go! | Brian; Nature
 3:00 pm | Amazing World of Gumball | The Drama
 3:30 pm | Amazing World of Gumball | The Buddy
 4:00 pm | Amazing World of Gumball | The Master
 4:30 pm | Amazing World of Gumball | The Silence