# 2023-08-23
 6:00 am | Amazing World of Gumball | The Intelligence; The Neighbor
 6:30 am | Amazing World of Gumball | Shippening; The Potion; the Bibi Bops
 7:00 am | Amazing World of Gumball | The Brain; The Spinoffs
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Wheels Just Want to Have Fun
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Roller Coasting
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Goofballs
 9:00 am | The Looney Tunes Show | Dmv Lunar New Year
 9:30 am | The Looney Tunes Show | Best Friends
10:00 am | Craig of the Creek | Beyond the Overpass; Jessica the Intern
10:30 am | Craig of the Creek | Craig of the Beach; King of Camping
11:00 am | Amazing World of Gumball | The Parents; The Transformation
11:30 am | Amazing World of Gumball | The Founder; The Understanding
12:00 pm | Summer Camp Island | Chapter 15: Croissant Moon; Chapter 16: Retrace Our Hooves
12:30 pm | Clarence | The Plane Excited; Tails of Mardrynia
 1:00 pm | Teen Titans Go! | Pirates; I See You
 1:30 pm | Teen Titans Go! | Brian/Nature
 2:00 pm | Amazing World of Gumball | The Worst; The Ex
 2:30 pm | Amazing World of Gumball | The Deal; The Sorcerer
 3:00 pm | Amazing World of Gumball | The Petals; The Menu
 3:30 pm | Amazing World of Gumball | The Puppets; The Uncle
 4:00 pm | Amazing World of Gumball | The Nuisance; The One
 4:30 pm | Amazing World of Gumball | The Father; The Line