# 2023-08-22
 6:00 am | Amazing World of Gumball | The List
 6:30 am | Amazing World of Gumball | The News
 7:00 am | Amazing World of Gumball | The Rival
 7:30 am | Cocomelon | Boo Boo Song
 7:45 am | Meet the Batwheels | Ride Along
 8:00 am | Cocomelon | Getting Ready for School Song
 8:15 am | Thomas & Friends: All Engines Go | The Real Number One
 8:30 am | Cocomelon | The Hiccup Song
 8:45 am | Bugs Bunny Builders | Mini Golf
 9:00 am | The Looney Tunes Show | Sunday Night Slice
 9:30 am | The Looney Tunes Show | SuperRabbit
10:00 am | Craig of the Creek | Copycat Carter
10:30 am | Craig of the Creek | Council of the Creek: Operation Hive-Mind
11:00 am | Amazing World of Gumball | The Lady
11:30 am | Amazing World of Gumball | The Sucker
12:00 pm | Summer Camp Island | Chapter 13: Jar Guard
12:30 pm | Clarence | Freedom Cactus
 1:00 pm | Teen Titans Go! | Real Magic; Puppets, Whaaaaat?
 1:30 pm | Teen Titans Go! | Mr. Butt; Man Person
 2:00 pm | Amazing World of Gumball | The Copycats
 2:30 pm | Amazing World of Gumball | The Potato
 3:00 pm | Amazing World of Gumball | The Cycle
 3:30 pm | Amazing World of Gumball | The Stars
 4:00 pm | Amazing World of Gumball | The Grades
 4:30 pm | Amazing World of Gumball | The Diet
 5:00 pm | Craig of the Creek | Winter Break
 5:30 pm | Teen Titans Go! | Caramel Apples; Halloween
 6:00 pm | Teen Titans Go! | Sandwich Thief; Money Grandma
 6:30 pm | Adventure Time | Jake vs. Me-Mow; The New Frontier