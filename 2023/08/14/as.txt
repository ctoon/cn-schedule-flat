# 2023-08-14
 7:00 pm | King of the Hill | Yankee Hankie
 7:30 pm | King of the Hill | Hank and the Great Glass Elevator
 8:00 pm | King of the Hill | Now Who's the Dummy?
 8:30 pm | King of the Hill | Ho Yeah!
 9:00 pm | King of the Hill | The Exterminator
 9:30 pm | Bob's Burgers | The Last Gingerbread House on the Left
10:00 pm | Bob's Burgers | Ex Machtina
10:30 pm | American Dad! | Holy S***, Jeff's Back!
11:00 pm | American Dad! | American Fung
11:30 pm | Rick and Morty | Rest and Ricklaxation
12:00 am | Rick and Morty | The Ricklantis Mixup
12:30 am | Royal Crackers | Factory 37
 1:00 am | The Boondocks | The Invasion of the Katrinas
 1:30 am | Mike Tyson Mysteries | Help a Brother Out
 1:45 am | Mike Tyson Mysteries | The Beginning
 2:00 am | Aqua Teen Hunger Force | Ol' Drippy
 2:15 am | Aqua Teen Hunger Force | Revenge of the Mooninites
 2:30 am | Rick and Morty | Rest and Ricklaxation
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Royal Crackers | Factory 37
 4:00 am | American Dad! | Holy S***, Jeff's Back!
 4:30 am | American Dad! | American Fung
 5:00 am | Bob's Burgers | The Last Gingerbread House on the Left
 5:30 am | Bob's Burgers | Ex Machtina