# 2023-06-27
 6:00 am | Amazing World of Gumball | The Pact
 6:30 am | Amazing World of Gumball | The Deal
 7:00 am | Amazing World of Gumball | The Line
 7:30 am | Cocomelon | Wait Your Turn
 7:45 am | Meet the Batwheels | A Tale of Two Bibis
 8:00 am | Cocomelon | Lost Hamster
 8:15 am | Thomas & Friends: All Engines Go | Not-So-Secret Mission
 8:30 am | Bugs Bunny Builders | Party Boat
 9:00 am | The Looney Tunes Show | The Stud, the Nerd, the Average Joe and the Saint
 9:30 am | We Baby Bears | Unica
10:00 am | Total DramaRama | Quiche It Goodbye
10:30 am | Total DramaRama | Ice Guys Finish Last
11:00 am | Craig of the Creek | The Children of the Dog
11:30 am | Craig of the Creek | Jessica Shorts
12:00 pm | Summer Camp Island | The Later Pile
12:30 pm | Clarence | Just Wait in the Car
 1:00 pm | Teen Titans Go! | Butt Atoms
 1:30 pm | Teen Titans Go! | TV Knight 5
 2:00 pm | Amazing World of Gumball | The Sorcerer
 2:30 pm | Amazing World of Gumball | The Console
 3:00 pm | Amazing World of Gumball | The Diet
 3:30 pm | Amazing World of Gumball | The Ex
 4:00 pm | Amazing World of Gumball | The Weirdo
 4:30 pm | Amazing World of Gumball | The Menu
 5:00 pm | Craig of the Creek | Beyond the Rapids
 5:30 pm | Teen Titans Go! | That's What's Up
 6:30 pm | We Bare Bears | Captain Craboo