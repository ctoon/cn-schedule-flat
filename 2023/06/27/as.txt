# 2023-06-27
 7:00 pm | King of the Hill | Patch Boomhauer
 7:30 pm | King of the Hill | Reborn To Be Wild
 8:00 pm | King of the Hill | New Cowboy on the Block
 8:30 pm | King of the Hill | The Incredible Hank
 9:00 pm | Bob's Burgers | Carpe Museum
 9:30 pm | Bob's Burgers | The Unnatural
10:00 pm | American Dad! | Choosey Wives Choose Smith
10:30 pm | American Dad! | Escape from Pearl Bailey
11:00 pm | American Dad! | Pulling Double Booty
11:30 pm | Rick and Morty | Get Schwifty
12:00 am | Rick and Morty | The Ricks Must Be Crazy
12:30 am | The Eric Andre Show | The A$AP Ferg Show
12:45 am | The Eric Andre Show | Blannibal Quits
 1:00 am | Robot Chicken | May Cause Light Cannibalism
 1:15 am | Robot Chicken | May Cause Immaculate Conception
 1:30 am | Aqua Teen Hunger Force Forever | Sweet C
 1:45 am | Aqua Teen Hunger Force Forever | Knapsack!
 2:00 am | The Boondocks | Good Times
 2:30 am | American Dad! | Choosey Wives Choose Smith
 3:00 am | Rick and Morty | Get Schwifty
 3:30 am | Rick and Morty | The Ricks Must Be Crazy
 4:00 am | Futurama | A Taste of Freedom
 4:30 am | Futurama | Kif Gets Knocked Up a Notch
 5:00 am | Bob's Burgers | Carpe Museum
 5:30 am | Bob's Burgers | The Unnatural