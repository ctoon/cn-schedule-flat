# 2023-06-25
 6:00 am | Amazing World of Gumball | The Mystery; The Prank
 6:30 am | Amazing World of Gumball | The Gi; The Kiss
 7:00 am | Amazing World of Gumball | The Gumball Chronicles: Vote Gumball…and Penny?
 8:00 am | Uncle Grandpa | Moustache Cream
 8:30 am | Uncle Grandpa | Nickname
 9:00 am | Scooby-Doo! WrestleMania Mystery | 
10:45 am | Amazing World of Gumball | The Crew
11:00 am | We Bare Bears | Hibernation
11:30 am | We Bare Bears | Nom Nom's Entourage
12:00 pm | Teen Titans Go! | Waffles; Opposites
12:30 pm | Teen Titans Go! | Birds; Be Mine
 1:00 pm | Teen Titans Go! | Brain Food; In and Out
 1:30 pm | Teen Titans Go! | Little Buddies; Missing
 2:00 pm | Summer Camp Island | Meeting of the Minds
 2:30 pm | Summer Camp Island | Molar Moles
 3:00 pm | Summer Camp Island | Acorn Graduation
 3:30 pm | Summer Camp Island | Tub on the Run
 4:00 pm | Amazing World of Gumball | The Goons; The Secret
 4:30 pm | Amazing World of Gumball | The Sock; The Genius
 5:00 pm | Amazing World of Gumball | The Mustache; The Date
 5:30 pm | Amazing World of Gumball | The Club; The Wand
 6:00 pm | Unicorn: Warriors Eternal | The Awakening
 6:30 pm | Unicorn: Warriors Eternal | The Awakening, Part 2