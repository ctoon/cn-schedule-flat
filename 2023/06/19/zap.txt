# 2023-06-19
 6:00 am | Amazing World of Gumball | The Advice
 6:30 am | Amazing World of Gumball | The Signal
 7:00 am | Amazing World of Gumball | The Parasite
 7:30 am | Cocomelon | What Makes Me Happy
 7:45 am | Meet the Batwheels | Buff in a China Shop
 8:00 am | Cocomelon | Boo Boo Song
 8:15 am | Thomas & Friends: All Engines Go | Blue Engine Blues
 8:30 am | Bugs Bunny Builders | Mail Whale
 9:00 am | New Looney Tunes | Bigs Bunny; Wahder, Wahder, Everywhere
 9:30 am | We Baby Bears | Doll's House
10:00 am | Total DramaRama | Carmageddon
10:30 am | Total DramaRama | Sugar & Spice & Lightning & Frights
11:00 am | Craig of the Creek | Bernard of the Creek, Part 1
11:30 am | Craig of the Creek | Craig of the Campus
12:00 pm | Summer Camp Island | Meeting of the Minds
12:30 pm | Clarence | Rock Show
 1:00 pm | Teen Titans Go! | The Real Orangins!
 1:30 pm | Teen Titans Go! | Quantum Fun
 2:00 pm | Amazing World of Gumball | The Triangle; The Money
 2:30 pm | Amazing World of Gumball | The Return
 3:00 pm | Amazing World of Gumball | The Future
 3:30 pm | Amazing World of Gumball | The Crew
 4:00 pm | Amazing World of Gumball | The Others
 4:30 pm | Amazing World of Gumball | The Signature
 5:00 pm | Craig of the Creek | The Other Side
 5:30 pm | Teen Titans Go! | The Fight
 6:00 pm | Teen Titans Go! | The Groover
 6:30 pm | We Bare Bears | Pet Shop