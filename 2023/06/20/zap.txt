# 2023-06-20
 6:00 am | Amazing World of Gumball | The Nest
 6:30 am | Amazing World of Gumball | The Points
 7:00 am | Amazing World of Gumball | The Disaster
 7:30 am | Cocomelon | Getting Ready for School Song
 7:45 am | Meet the Batwheels | Harley Did It
 8:00 am | Cocomelon | First Day of School
 8:15 am | Thomas & Friends: All Engines Go | Hay Fort Frenzy
 8:30 am | Bugs Bunny Builders | Dino Fright
 9:00 am | New Looney Tunes | Lucky Duck; Free Range Foghorn
 9:30 am | We Baby Bears | The Little Fallen Star
10:00 am | Total DramaRama | Breaking Bite
10:30 am | Total DramaRama | I Dream of Meanie
11:00 am | Craig of the Creek | Wheels Collide
11:30 am | Craig of the Creek | Left in the Dark
12:00 pm | Summer Camp Island | Molar Moles
12:30 pm | Clarence | The Big Game
 1:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 1:30 pm | Teen Titans Go! | How's This for a Special? Spaaaace: Pt. 1
 2:00 pm | Amazing World of Gumball | The Check
 2:30 pm | Amazing World of Gumball | The Routine
 3:00 pm | Amazing World of Gumball | The Factory
 3:30 pm | Amazing World of Gumball | The Upgrade
 4:00 pm | Amazing World of Gumball | The Comic
 4:30 pm | Amazing World of Gumball | The Romantic
 5:00 pm | Craig of the Creek | Sugar Smugglers
 5:30 pm | Teen Titans Go! | I Used to Be a Peoples
 6:00 pm | Teen Titans Go! | The Metric System vs. Freedom
 6:30 pm | We Bare Bears | Cupcake Job