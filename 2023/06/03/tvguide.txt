# 2023-06-03
 6:00 am | Amazing World of Gumball | The Disaster; The Re-run
 6:30 am | Amazing World of Gumball | The Roots; The Stories
 7:00 am | Amazing World of Gumball | The Blame; The Guy
 7:30 am | Amazing World of Gumball | The Detective; The Boredom
 8:00 am | Teen Titans Go! | The Free Perk; Perfect Pitch?
 8:30 am | Teen Titans Go! | Go! ; Pool Season
 9:00 am | Teen Titans Go! | Finding Aquaman; Kyle
 9:30 am | Looney Tunes Cartoons | Bathy Daffy End of the Leash: Bullseye Painting Rabbit Sandwich Maker Put the Cat out: Window; Pardon the Garden Put the Cat out
10:00 am | We Baby Bears | Little Mer-Bear; The; Modern-Ish Stone Age Family
10:30 am | Total DramaRama | Chews Wisely; Ice Guys Finish Last
11:00 am | Total DramaRama | Dingo Ate My Duncan; A; Trousering Inferno
11:30 am | Total DramaRama | The Erase Yer Head; Big Bangs Theory
12:00 pm | Craig of the Creek | Creek Daycare; Trading Day
12:30 pm | Craig of the Creek | Sugar Smugglers; Crisis at Elder Rock
 1:00 pm | Teen Titans Go! | Whodundidit? ; TV Knight 7
 1:30 pm | Teen Titans Go! | Sweet Revenge; We'll Be Right Back
 2:00 pm | Teen Titans Go! | Porch Pirates; Jump City Rock
 2:30 pm | Teen Titans Go! | Sticky Situation; A; Natural Gas
 3:00 pm | Amazing World of Gumball | The Fury; The Vision
 3:30 pm | Amazing World of Gumball | The Compilation; The Choices
 4:00 pm | Amazing World of Gumball | The Styles; The Test
 4:30 pm | Amazing World of Gumball | The Slide; The Loophole
 5:00 pm | Scooby-Doo! and the Gourmet Ghost | 
 6:45 pm | Amazing World of Gumball | The Misunderstandings