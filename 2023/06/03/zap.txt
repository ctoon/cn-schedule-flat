# 2023-06-03
 6:00 am | Amazing World of Gumball | The Disaster
 6:30 am | Amazing World of Gumball | The Roots
 7:00 am | Amazing World of Gumball | The Blame
 7:30 am | Amazing World of Gumball | The Detective
 8:00 am | Teen Titans Go! | Free Perk
 8:30 am | Teen Titans Go! | Go!
 9:00 am | Teen Titans Go! | Finding Aquaman
 9:30 am | Looney Tunes Cartoons | Bathy Daffy; End of the Leash: Bullseye Painting; Rabbit Sandwich Maker; Put the Cat Out: Window
10:00 am | We Baby Bears | The Little Mer-Bear
10:30 am | Total DramaRama | Chews Wisely
11:00 am | Total DramaRama | A Dingo Ate My Duncan
11:30 am | Total DramaRama | Erase Yer Head
12:00 pm | Craig of the Creek | Creek Daycare
12:30 pm | Craig of the Creek | Sugar Smugglers
 1:00 pm | Teen Titans Go! | Whodundidit?
 1:30 pm | Teen Titans Go! | Sweet Revenge
 2:00 pm | Teen Titans Go! | Porch Pirates
 2:30 pm | Teen Titans Go! | A Sticky Situation
 3:00 pm | Amazing World of Gumball | The Fury
 3:30 pm | Amazing World of Gumball | The Compilation
 4:00 pm | Amazing World of Gumball | The Code
 4:30 pm | Amazing World of Gumball | The Slide
 5:00 pm | Scooby-Doo! and the Gourmet Ghost | 
 6:45 pm | Amazing World of Gumball | The Misunderstandings