# 2023-06-30
 6:00 am | Amazing World of Gumball | The Sorcerer
 6:30 am | Amazing World of Gumball | The Console
 7:00 am | Amazing World of Gumball | The Diet
 7:30 am | Cocomelon | The Duck Hide and Seek Song
 7:45 am | Meet the Batwheels | Bam's Clawful Mistake
 8:00 am | Cocomelon | The Country Mouse and the City Mouse
 8:15 am | Mecha Builders | Cross That Bridge; Knock Knock! Who's There?
 8:45 am | Bugs Bunny Builders | Bright Light
 9:00 am | The Looney Tunes Show | Bobcats on Three
 9:30 am | We Baby Bears | Bug City Frame-Up
10:00 am | Total DramaRama | The Opening Act
10:30 am | Total DramaRama | Van Hogling
11:00 am | Craig of the Creek | The Jinxening
11:30 am | Craig of the Creek | In the Key of the Creek
12:00 pm | Summer Camp Island | Just You and Me
12:30 pm | Clarence | The Dare Day
 1:00 pm | Teen Titans Go! | Rain on Your Wedding Day
 1:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Justice League Edition
 2:00 pm | Amazing World of Gumball | The Stink
 2:30 pm | Amazing World of Gumball | The Awareness
 3:00 pm | Amazing World of Gumball | The Parents
 3:30 pm | Amazing World of Gumball | The Founder
 4:00 pm | Amazing World of Gumball | The Schooling
 4:30 pm | Amazing World of Gumball | The Intelligence
 5:00 pm | Adventure Time | Slumber Party Panic
 5:30 pm | Adventure Time | Too Young
 6:00 pm | Adventure Time | Lady & Peebles
 6:30 pm | Adventure Time | The Suitor