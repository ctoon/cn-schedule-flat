# 2023-06-23
 7:00 pm | Unicorn: Warriors Eternal | Darkness Before the Dawn
 7:30 pm | Unicorn: Warriors Eternal | A Love's Last Light
 8:00 pm | King of the Hill | Boxing Luanne
 8:30 pm | King of the Hill | Megalo Dale
 9:00 pm | Bob's Burgers | It Snakes a Village
 9:30 pm | Bob's Burgers | Family Fracas
10:00 pm | American Dad! | Widowmaker
10:30 pm | American Dad! | Red October Sky
11:00 pm | American Dad! | Office Spaceman
11:30 pm | Rick and Morty | A Rickle in Time
12:00 am | Teenage Euthanasia | First Date with the Second Coming
12:30 am | Teenage Euthanasia | Teen Eggs and Scram
 1:00 am | Teenage Euthanasia | The Bad Bang Theory
 1:30 am | Teenage Euthanasia | Nobody Beats the Baba
 2:00 am | Rick and Morty | A Rickle in Time
 2:30 am | American Dad! | Widowmaker
 3:00 am | American Dad! | Office Spaceman
 3:30 am | Futurama | Leela's Homeworld
 4:00 am | Futurama | Where the Buggalo Roam
 4:30 am | Futurama | A Pharaoh to Remember
 5:00 am | Bob's Burgers | It Snakes a Village
 5:30 am | Bob's Burgers | Family Fracas