# 2023-06-17
 7:00 pm | King of the Hill | Joust like a Woman
 7:30 pm | King of the Hill | The Bluegrass is Always Greener
 8:00 pm | King of the Hill | The Substitute Spanish Prisoner
 8:30 pm | King of the Hill | Unfortunate Son
 9:00 pm | Rick and Morty | The Ricklantis Mixup
 9:30 pm | Rick and Morty | Morty's Mind Blowers
10:00 pm | American Dad! | Adventures in Hayleysitting
10:30 pm | American Dad! | I Can't Stan You
11:00 pm | American Dad! | The Magnificent Steven
11:30 pm | American Dad! | Joint Custody
12:00 am | Unicorn: Warriors Eternal | Darkness Before the Dawn
12:30 am | Dr. Stone | First Contact
 1:00 am | Food Wars! | Food Wars
 1:30 am | One Piece | A Critical Situation! Punk Hazard Explodes!
 2:00 am | Naruto: Shippuden | The Difference in Power
 2:30 am | My Hero Academia | Disaster Walker
 3:00 am | The Venture Brothers | A Party for Tarzan
 3:30 am | Metalocalypse | Dethsiduals
 4:00 am | Futurama | Bendless Love
 4:30 am | Futurama | Birdbot of Ice-Catraz
 5:00 am | Futurama | Luck of the Fryrish
 5:30 am | Futurama | The Cyber House Rules