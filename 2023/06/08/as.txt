# 2023-06-08
 7:00 pm | King of the Hill | Hank's Bad Hair Day
 7:30 pm | King of the Hill | Meet the Propaniacs
 8:00 pm | King of the Hill | Nancy Boys
 8:30 pm | King of the Hill | Flush with Power
 9:00 pm | Bob's Burgers | Bob Day Afternoon
 9:30 pm | Bob's Burgers | Synchronized Swimming
10:00 pm | American Dad! | Star Trek
10:30 pm | American Dad! | Not Particularly Desperate Housewives
11:00 pm | American Dad! | Rough Trade
11:30 pm | Rick and Morty | Star Mort Rickturn of the Jerri
12:00 am | Unicorn: Warriors Eternal | The Heart of Kings
12:45 am | Robot Chicken | Fila Ogden in: Maggie's Got a Full Load
 1:00 am | Robot Chicken | Boogie Bardstown in: No Need, I have Coupons
 1:15 am | Robot Chicken | Hermie Nursery in: Seafood Sensation
 1:30 am | Aqua Something You Know Whatever | Shirt Herpes
 1:45 am | Aqua Something You Know Whatever | Rocket Horse and Jet Chicken
 2:00 am | The Boondocks | Smokin' With Cigarettes
 2:30 am | Rick and Morty | Star Mort Rickturn of the Jerri
 3:00 am | Rick and Morty | Mort Dinner Rick Andre
 3:30 am | Royal Crackers | Factory 37
 4:00 am | Futurama | Fry and the Slurm Factory
 4:30 am | Futurama | I Second That Emotion
 5:00 am | Bob's Burgers | Bob Day Afternoon
 5:30 am | Bob's Burgers | Synchronized Swimming