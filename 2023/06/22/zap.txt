# 2023-06-22
 6:00 am | Amazing World of Gumball | The Crew
 6:30 am | Amazing World of Gumball | The Others
 7:00 am | Amazing World of Gumball | The Signature
 7:30 am | Cocomelon | Basketball Song
 7:45 am | Meet the Batwheels | Batty Race
 8:00 am | Cocomelon | 1, 2, 3, 4, 5, Once I Caught a Fish Alive!
 8:15 am | Thomas & Friends: All Engines Go | Sheep Shenanigans
 8:30 am | Bugs Bunny Builders | Underwater Star
 9:00 am | New Looney Tunes | Hoggin' the Road; Timmmmmmbugs
 9:30 am | We Baby Bears | Pirate Parrot Polly
10:00 am | Total DramaRama | WaterHose-Five
10:30 am | Total DramaRama | Cody the Barbarian
11:00 am | Craig of the Creek | Sugar Smugglers
11:30 am | Craig of the Creek | Sleepover at JP's
12:00 pm | Summer Camp Island | Tub on the Run
12:30 pm | Clarence | Karate Mom
 1:00 pm | Teen Titans Go! | Strength of a Grown Man
 1:30 pm | Teen Titans Go! | Had to Be There
 2:00 pm | Amazing World of Gumball | The Love
 2:30 pm | Amazing World of Gumball | The Awkwardness
 3:00 pm | Amazing World of Gumball | The Heart
 3:30 pm | Amazing World of Gumball | The Nest
 4:00 pm | Amazing World of Gumball | The Points
 4:30 pm | Amazing World of Gumball | The Disaster
 5:00 pm | Craig of the Creek | Craig of the Campus
 5:30 pm | Teen Titans Go! | Communicate Openly
 6:00 pm | Teen Titans Go! | Girls Night In
 6:30 pm | We Bare Bears | Chloe and Ice Bear