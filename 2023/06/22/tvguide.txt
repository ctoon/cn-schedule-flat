# 2023-06-22
 6:00 am | Amazing World of Gumball | The Pest; The Crew
 6:30 am | Amazing World of Gumball | Others; The Sale; the Skinny at the Pool
 7:00 am | Amazing World of Gumball | The Signature; The Gift
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Batty Race; Toothbrush
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Sheep Shenanigans Good as New
 8:30 am | Bugs Bunny Builders | Underwater Star; Party Boat
 9:00 am | New Looney Tunes | Hoggin' the Road Timmmmmmbugs; Bugs Over Par Fast Feud Don't Be Fooled by His Size
 9:30 am | We Baby Bears | Pirate Parrot Polly; Hashtag; Fan; Orange Street
10:00 am | Total DramaRama | Waterhose-Five; Erase Yer Head
10:30 am | Total DramaRama | Cody the Barbarian; Teacher; Soldier; Chef; Spy
11:00 am | Craig of the Creek | Sugar Smugglers; Mortimor to the Rescue
11:30 am | Craig of the Creek | Sleepover at Jp's; Secret in a Bottle
12:00 pm | Summer Camp Island | Tub on the Run; Spotted Bear Stretch
12:30 pm | Clarence | Officer Moody; Karate Mom
 1:00 pm | Teen Titans Go! | The Strength of a Grown Man ;Viewers Decide
 1:30 pm | Teen Titans Go! | Cartoon Feud Had to Be There
 2:00 pm | Amazing World of Gumball | The Love; The Roots
 2:30 pm | Amazing World of Gumball | The Awkwardness; The Blame
 3:00 pm | Amazing World of Gumball | The Heart; The Bffs
 3:30 pm | Amazing World of Gumball | The Nest; The Detective
 4:00 pm | Amazing World of Gumball | The Points; The Fury
 4:30 pm | Amazing World of Gumball | The Disaster; The Re-run
 5:00 pm | Craig of the Creek | Craig of the Campus; Camper on the Run
 5:30 pm | Teen Titans Go! | Curse of the Booty Scooty; Communicate Openly
 6:00 pm | Teen Titans Go! | Girls Night In
 6:30 pm | We Bare Bears | Chloe and Ice Bear; Chicken and Waffles