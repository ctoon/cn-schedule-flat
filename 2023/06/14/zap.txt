# 2023-06-14
 6:00 am | Amazing World of Gumball | The Society; The Spoiler
 6:30 am | Amazing World of Gumball | The Countdown; The Nobody
 7:00 am | Amazing World of Gumball | The Downer; The Egg
 7:30 am | Cocomelon | Are We There Yet?
 7:45 am | Meet the Batwheels | Cave Sweet Cave
 8:00 am | Cocomelon | My Name Song
 8:15 am | Thomas & Friends: All Engines Go | Rocket's Fall
 8:30 am | Bugs Bunny Builders | Tweety-Go-Round
 9:00 am | New Looney Tunes | Porky's Duck-Livery Service; The Wabbit Who Would Be King
 9:30 am | We Baby Bears | Who Crashed the RV?
10:00 am | Total DramaRama | Bad Seed
10:30 am | Total DramaRama | A Fish Called Leshawna
11:00 am | Craig of the Creek | Memories of Bobby
11:30 am | Craig of the Creek | Jacob of the Creek
12:00 pm | Summer Camp Island | Susie's Fantastical Scavenger Hunt
12:30 pm | Summer Camp Island | Pajamas Party
 1:00 pm | Teen Titans Go! | BBCYFSHIPBDAY
 1:30 pm | Teen Titans Go! | Beast Girl
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Amazing World of Gumball | The Ad
 3:30 pm | Amazing World of Gumball | The Fraud; The Void
 4:00 pm | Amazing World of Gumball | The Boss; The Move
 4:30 pm | Amazing World of Gumball | The Law; The Allergy
 5:00 pm | Craig of the Creek | Sour Candy Trials
 5:30 pm | Teen Titans Go! | Flashback
 6:00 pm | Teen Titans Go! | TV Knight 3
 6:30 pm | We Bare Bears | Everyday Bears