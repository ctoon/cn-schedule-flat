# 2023-06-14
 6:00 am | Amazing World of Gumball | The Spoiler; The Society
 6:30 am | Amazing World of Gumball | Countdown; The Nobody; the Thief
 7:00 am | Amazing World of Gumball | Downer; The Egg; the Father's Day
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Cave Sweet Cave Robin's Ride
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Rocket's Fall Stuck in the Mud at Night
 8:30 am | Bugs Bunny Builders | Tweety-Go-Round; Cheddar Days
 9:00 am | New Looney Tunes | Porky's Duck-Livery Service Wabbit Who Would Be King; The Hareplane Mode Bugs of Steel Bugs Bunny Boogie
 9:30 am | We Baby Bears | Who Crashed the Rv; Boo-Dunnit; Signs
10:00 am | Total DramaRama | Bad Seed; Gumbearable
10:30 am | Total DramaRama | Fish Called Leshawna; A; Whack Mirror
11:00 am | Craig of the Creek | Memories of Bobby; Summer Wish
11:30 am | Craig of the Creek | Jacob of the Creek; Turning the Tables
12:00 pm | Summer Camp Island | Susie's Fantastical Scavenger Hunt; Mop Forever
12:30 pm | Summer Camp Island | The Pajamas Party; Soundhouse
 1:00 pm | Teen Titans Go! | Bbcyfshipbday Bro-Pocalypse
 1:30 pm | Teen Titans Go! | Mo' Money Mo' Problems; Beast Girl
 2:00 pm | Amazing World of Gumball | The Name; The Extras
 2:30 pm | Amazing World of Gumball | The Gripes; The Vacation
 3:00 pm | Amazing World of Gumball | The Slip / The Ad
 3:30 pm | Amazing World of Gumball | The Fraud; The Void
 4:00 pm | Amazing World of Gumball | The Move; The Boss
 4:30 pm | Amazing World of Gumball | The Law; The Allergy
 5:00 pm | Craig of the Creek | Sour Candy Trials; Sparkle Cadet
 5:30 pm | Teen Titans Go! | Flashback
 6:00 pm | Teen Titans Go! | The Scoop; TV Knight 3
 6:30 pm | We Bare Bears | Everyday Bears; The Road