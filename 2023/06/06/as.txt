# 2023-06-06
 7:00 pm | King of the Hill | Old Glory
 7:30 pm | King of the Hill | Rodeo Days
 8:00 pm | King of the Hill | Hanky Panky (1)
 8:30 pm | King of the Hill | High Anxiety (2)
 9:00 pm | Bob's Burgers | Weekend at Morts
 9:30 pm | Bob's Burgers | Lobsterfest
10:00 pm | American Dad! | A Smith in the Hand
10:30 pm | American Dad! | All About Steve
11:00 pm | American Dad! | Con Heir
11:30 pm | Rick and Morty | Never Ricking Morty
12:00 am | Rick and Morty | Promortyus
12:30 am | Royal Crackers | Pilot
 1:00 am | Robot Chicken | No Wait, He Has a Cane
 1:15 am | Robot Chicken | Bugs Keith in: I Can't Call Heaven, Doug
 1:30 am | Aqua Unit Patrol Squad 1 | Lasagna
 1:45 am | Aqua Unit Patrol Squad 1 | Last Dance for Napkin Lad
 2:00 am | The Boondocks | A Date With the Booty Warrior
 2:30 am | Rick and Morty | Never Ricking Morty
 3:00 am | Rick and Morty | Promortyus
 3:30 am | Royal Crackers | Pilot
 4:00 am | Futurama | Hell Is Other Robots
 4:30 am | Futurama | A Flight to Remember
 5:00 am | Bob's Burgers | Weekend at Morts
 5:30 am | Bob's Burgers | Lobsterfest