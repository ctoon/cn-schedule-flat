# 2023-06-12
 6:00 am | Amazing World of Gumball | The Fraud; The Void
 6:30 am | Amazing World of Gumball | Boss; The Move; the Alien
 7:00 am | Amazing World of Gumball | Law; The Allergy; the Father's Day
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Rev and Let Rev Bad to the Chrome
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Salty's Sea Shanty; Snowflake
 8:30 am | Bugs Bunny Builders | Party Boat; Stories
 9:00 am | New Looney Tunes | Wet Feet There's a Soccer Born Every Minute; Wabbit's Best Friend Annoying Ex-Boydfriend Lunch Break
 9:30 am | We Baby Bears | Bath on the Nile; Magical Box; the Rock Concert
10:00 am | Total DramaRama | Baby Brother Blues; Abaracaduncan
10:30 am | Total DramaRama | Space Codyty; Shock & Aww
11:00 am | Craig of the Creek | Power Punchers; Dibs Court
11:30 am | Craig of the Creek | Creek Cart Racers; The Great Fossil Rush
12:00 pm | Summer Camp Island | Cosmic Bupkiss; Radio Silence
12:30 pm | Summer Camp Island | Director's Cut; Haunted Campfire
 1:00 pm | Teen Titans Go! | Orangins; Labor Day
 1:30 pm | Teen Titans Go! | Jinxed; The Academy
 2:00 pm | Amazing World of Gumball | The Tag; The Lesson
 2:30 pm | Amazing World of Gumball | The Limit; The Game
 3:00 pm | Amazing World of Gumball | The Potion; The Spinoffs
 3:30 pm | Amazing World of Gumball | The Promise; The Voice
 4:00 pm | Amazing World of Gumball | The Boombox; The Castle
 4:30 pm | Amazing World of Gumball | The Tape; The Sweaters
 5:00 pm | Craig of the Creek | Memories of Bobby; Summer Wish
 5:30 pm | Teen Titans Go! | Brain Percentages; Ones and Zeros
 6:00 pm | Teen Titans Go! | Bl4z3; Career Day
 6:30 pm | We Bare Bears | Brother Up; Charlie