# 2023-06-12
 7:00 pm | King of the Hill | Chasing Bobby
 7:30 pm | King of the Hill | Yankee Hankie
 8:00 pm | King of the Hill | Hank and the Great Glass Elevator
 8:30 pm | King of the Hill | Now Who's the Dummy?
 9:00 pm | King of the Hill | Ho Yeah!
 9:30 pm | Bob's Burgers | Moody Foodie
10:00 pm | Bob's Burgers | Bad Tina
10:30 pm | American Dad! | The American Dad After School Special
11:00 pm | American Dad! | Failure Is Not a Factory-Installed Option
11:30 pm | Rick and Morty | Rickdependence Spray
12:00 am | Rick and Morty | Amortycan Grickfitti
12:30 am | The Eric Andre Show | Pauly D; Rick Springfield
12:45 am | The Eric Andre Show | Bird Up!
 1:00 am | Robot Chicken | Snoopy Camino Lindo in: Quick and Dirty Squirrel Shot
 1:15 am | Robot Chicken | Musya Shakhtyorov in: Honeyboogers
 1:30 am | Aqua Something You Know Whatever | The Granite Family
 1:45 am | Aqua Something You Know Whatever | Bookie
 2:00 am | The Boondocks | The Uncle Ruckus Reality Show
 2:30 am | American Dad! | The American Dad After School Special
 3:00 am | Rick and Morty | Rickdependence Spray
 3:30 am | Rick and Morty | Amortycan Grickfitti
 4:00 am | Futurama | A Clone of My Own
 4:30 am | Futurama | The Deep South
 5:00 am | Bob's Burgers | Moody Foodie
 5:30 am | Bob's Burgers | Bad Tina