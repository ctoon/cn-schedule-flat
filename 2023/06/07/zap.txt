# 2023-06-07
 6:00 am | Amazing World of Gumball | The Friend; The Saint
 6:30 am | Amazing World of Gumball | The Pony; The Storm
 7:00 am | Amazing World of Gumball | The Dream; The Sidekick
 7:30 am | Cocomelon | Fix It
 7:45 am | Meet the Batwheels | Scaredy-Bat
 8:00 am | Cocomelon | Yes Yes Bed Time Camping
 8:15 am | Thomas & Friends: All Engines Go | Speedster Sandy
 8:30 am | Bugs Bunny Builders | Beach Battle
 9:00 am | New Looney Tunes | The Grand Barbari-yon; Giant Rabbit Hunters
 9:30 am | We Baby Bears | Sheep Bears
10:00 am | Total DramaRama | He Who Wears the Clown
10:30 am | Total DramaRama | The Upside of Hunger
11:00 am | Craig of the Creek | Ace of Squares
11:30 am | Craig of the Creek | Doorway to Helen
12:00 pm | Summer Camp Island | Computer Vampire
12:30 pm | Summer Camp Island | Popular Banana Split
 1:00 pm | Teen Titans Go! | Riding the Dragon
 1:30 pm | Teen Titans Go! | The Overbite
 2:00 pm | Amazing World of Gumball | The Car; The Curse
 2:30 pm | Amazing World of Gumball | The Microwave; The Meddler
 3:00 pm | Amazing World of Gumball | The Butterfly; The Question
 3:30 pm | Amazing World of Gumball | The Helmet; The Fight
 4:00 pm | Amazing World of Gumball | The End; The DVD
 4:30 pm | Amazing World of Gumball | The Knights; The Colossus
 5:00 pm | Craig of the Creek | Big Pinchy
 5:30 pm | Teen Titans Go! | TV Knight
 6:00 pm | Teen Titans Go! | Shrimps and Prime Rib
 6:30 pm | We Bare Bears | Our Stuff