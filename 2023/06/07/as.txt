# 2023-06-07
 7:00 pm | King of the Hill | Naked Ambition
 7:30 pm | King of the Hill | Movin' On Up
 8:00 pm | King of the Hill | Bill of Sales
 8:30 pm | King of the Hill | Won't You Pimai Neighbor?
 9:00 pm | Bob's Burgers | Torpedo
 9:30 pm | Bob's Burgers | The Belchies
10:00 pm | American Dad! | Stan of Arabia Part 1
10:30 pm | American Dad! | Stan of Arabia Part 2
11:00 pm | American Dad! | Stannie Get Your Gun
11:30 pm | Rick and Morty | The Vat of Acid Episode
12:00 am | Rick and Morty | Childrick of Mort
12:30 am | Royal Crackers | Theo's Comeback Tour
 1:00 am | Robot Chicken | Ginger Hill in: Bursting Pipes
 1:15 am | Robot Chicken | Garfield Stockman in: A Voice Like Wet Ham
 1:30 am | Aqua Something You Know Whatever | Big Bro
 1:45 am | Aqua Something You Know Whatever | Chicken and Beans
 2:00 am | The Boondocks | The Hunger Strike
 2:30 am | Rick and Morty | The Vat of Acid Episode
 3:00 am | Rick and Morty | Childrick of Mort
 3:30 am | Royal Crackers | Theo's Comeback Tour
 4:00 am | Futurama | Mars University
 4:30 am | Futurama | When Aliens Attack
 5:00 am | Bob's Burgers | Torpedo
 5:30 am | Bob's Burgers | The Belchies