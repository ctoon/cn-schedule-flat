# 2023-06-11
 6:00 am | Amazing World of Gumball | The Copycats
 6:30 am | Amazing World of Gumball | The Catfish
 7:00 am | Amazing World of Gumball | The Cycle
 7:30 am | Amazing World of Gumball | The Stars
 8:00 am | Uncle Grandpa | Belly Bros
 8:30 am | Uncle Grandpa | Funny Face
 9:00 am | Scooby-Doo! and the Curse of the 13th Ghost | 
10:45 am | We Bare Bears | Food Truck
11:00 am | We Bare Bears | Panda's Date
11:30 am | We Bare Bears | Burrito
12:00 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star
12:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Second Greatest Team Edition
 1:00 pm | Teen Titans Go! | BBCYFSHIPBDAY
 1:30 pm | Teen Titans Go! | Fat Cats
 2:00 pm | Teen Titans Go! | Flashback
 2:30 pm | Teen Titans Go! | The Streak, Part 1
 3:00 pm | Teen Titans Go! | Two Parter: Part One
 3:30 pm | Teen Titans Go! | Classic Titans
 4:00 pm | Teen Titans Go! | Walk Away
 4:30 pm | Teen Titans Go! | Justice League's Next Top Talent Idol Star: Dance Crew Edition
 5:00 pm | Teen Titans Go! | Manor and Mannerisms
 5:30 pm | Teen Titans Go! | Teen Titans Action
 6:00 pm | Batman | 