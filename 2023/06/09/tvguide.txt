# 2023-06-09
 6:00 am | Amazing World of Gumball | The Club; The Wand
 6:30 am | Amazing World of Gumball | Ape; The Poltergeist; the Diet Doc
 7:00 am | Amazing World of Gumball | Quest; The Spoon; the 5 Little Ducks - Playtime at the Farm!
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Zoomsday Bad to the Chrome
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | Whatsacallit of Treetop Woods; The Nat in the Dark! ; Australia Kangaroo
 8:45 am | Bugs Bunny Builders | Sea School
 9:00 am | The Looney Tunes Show | Point, Laser Point
 9:30 am | We Baby Bears | Dragon Pests; Back to Our Regular Time Period; Fashion Show
10:00 am | Total DramaRama | Way Back Wendel; Jelly Aches
10:30 am | Total DramaRama | Stingin' in the Rain; Simply Perfect
11:00 am | Craig of the Creek | Deep Creek Salvage; Big Pinchy
11:30 am | Craig of the Creek | The Kid From 3030; The Shortcut
12:00 pm | Summer Camp Island | Pepper's Blanket Is Missing; Hedgehog Werewolf
12:30 pm | Summer Camp Island | Mr. Softball; Fuzzy Pink Time Babies
 1:00 pm | Teen Titans Go! | Titan Saving Time Employee of the Month Redux
 1:30 pm | Teen Titans Go! | Avogodo; The; Lication
 2:00 pm | Amazing World of Gumball | The Bumpkin; the Flakers
 2:30 pm | Amazing World of Gumball | The Authority; The Virus
 3:00 pm | Amazing World of Gumball | The Friend; The Saint
 3:30 pm | Amazing World of Gumball | The Pony; The Storm
 4:00 pm | Amazing World of Gumball | The Dream; The Sidekick
 4:30 pm | Amazing World of Gumball | The Hero; The Photo
 5:00 pm | Adventure Time | The Simon & Marcy Party's Over; Isla de Senorita
 5:30 pm | Adventure Time | Frost & Fire; Earth & Water
 6:00 pm | Adventure Time | Business Time Storytelling
 6:30 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | Dungeon; Wizard