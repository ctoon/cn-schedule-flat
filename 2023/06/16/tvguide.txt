# 2023-06-16
 6:00 am | Amazing World of Gumball | The Kids; The Fan
 6:30 am | Amazing World of Gumball | Coach; The Joy; the Doc Dog
 7:00 am | Amazing World of Gumball | The Recipe; The Puppy
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Bam's Upgrade; Sandcastle
 8:00 am | Cocomelon | 
 8:15 am | Mecha Builders | Let's Get Rolling! Bit of a Stretch! ; A Bam!
 8:45 am | Bugs Bunny Builders | Party Boat
 9:00 am | The Looney Tunes Show | Bobcats on Three!
 9:30 am | We Baby Bears | Lighthouse; Modern-Ish Stone Age Family
10:00 am | Total DramaRama | Encore'tney; Macarthur Park
10:30 am | Total DramaRama | Life of Pie; Last Mom Standing
11:00 am | Craig of the Creek | The Final Book; Itch to Explore
11:30 am | Craig of the Creek | Escape From Family Dinner; Monster in the Garden
12:00 pm | Summer Camp Island | Great Elf Invention Convention; The Twelve Angry Hedgehogs
12:30 pm | Summer Camp Island | The Spell Crushers; Library
 1:00 pm | Teen Titans Go! | The Power of Shrimps; Slapping Butts and Celebrating for No Reason
 1:30 pm | Teen Titans Go! | Kabooms
 2:00 pm | Amazing World of Gumball | The Oracle; The Safety
 2:30 pm | Amazing World of Gumball | The Friend; The Saint
 3:00 pm | Amazing World of Gumball | The Silence; The Master
 3:30 pm | Amazing World of Gumball | The Spoiler; The Society
 4:00 pm | Amazing World of Gumball | The Nobody; The Countdown
 4:30 pm | Amazing World of Gumball | The Egg; The Downer
 5:00 pm | Adventure Time - Abenteuerzeit mit Finn und Jake | Betty / Play Date
 5:30 pm | Adventure Time | Evergreen Friends Forever
 6:00 pm | Adventure Time | My Two Favorite People Ocean of Fear
 6:30 pm | Adventure Time | Memories of Boom Boom Mountain Loyalty to the King