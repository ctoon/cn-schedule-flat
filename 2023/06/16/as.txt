# 2023-06-16
 7:00 pm | Unicorn: Warriors Eternal | The Heart of Kings
 7:45 pm | Unicorn: Warriors Eternal | Darkness Before the Dawn
 8:15 pm | Rick and Morty: Extras | Summer's Sleepover
 8:30 pm | King of the Hill | I'm with Cupid
 9:00 pm | Bob's Burgers | Tina-Rannosaurus Wrecks
 9:30 pm | Bob's Burgers | The Unbearable Like-Likeness of Gene
10:00 pm | American Dad! | An Apocalypse to Remember
10:30 pm | American Dad! | Four Little Words
11:00 pm | American Dad! | When Stan Loves a Woman
11:30 pm | Rick and Morty | Lawnmower Dog
12:00 am | Unicorn: Warriors Eternal | The Past Within
12:30 am | Unicorn: Warriors Eternal | The Mystery of Secrets
 1:00 am | Unicorn: Warriors Eternal | The Heart of Kings
 1:45 am | Unicorn: Warriors Eternal | Darkness Before the Dawn
 2:15 am | Aqua TV Show Show | Merlo Sauvignon Blanco
 2:30 am | American Dad! | An Apocalypse to Remember
 3:00 am | American Dad! | When Stan Loves a Woman
 3:30 am | Futurama | The Cryonic Woman
 4:00 am | Futurama | Parasites Lost
 4:30 am | Futurama | Amazon Women in the Mood
 5:00 am | Bob's Burgers | Tina-Rannosaurus Wrecks
 5:30 am | Bob's Burgers | The Unbearable Like-Likeness of Gene