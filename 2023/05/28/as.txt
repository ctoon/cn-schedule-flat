# 2023-05-28
 7:00 pm | Unicorn: Warriors Eternal | A Fateful Encounter
 7:30 pm | Unicorn: Warriors Eternal | What Lies Beneath
 8:00 pm | Unicorn: Warriors Eternal | The Past Within
 8:30 pm | Futurama | Parasites Lost
 9:00 pm | American Dad! | Gold Top Nuts
 9:30 pm | American Dad! | The Three Fs
10:00 pm | American Dad! | Smooshed: A Love Story
10:30 pm | Rick and Morty | Analyze Piss
11:00 pm | Royal Crackers | Pilot
11:30 pm | Royal Crackers | Theo's Comeback Tour
12:00 am | Royal Crackers | Factory 37
12:30 am | Royal Crackers | Stebe
 1:00 am | Royal Crackers | Business Mom
 1:30 am | Royal Crackers | Mayworth
 2:00 am | Royal Crackers | The .1%
 2:30 am | Royal Crackers | Casa de Darby
 3:00 am | Royal Crackers | CrackerCon
 3:30 am | Royal Crackers | Craftopia
 4:00 am | Futurama | Naturama
 4:30 am | Futurama | 2-D Blacktop
 5:00 am | Futurama | Fry and the Slurm Factory
 5:30 am | Futurama | Parasites Lost