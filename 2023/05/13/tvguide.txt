# 2023-05-13
 6:00 am | Amazing World of Gumball | The Move; The Boss
 6:30 am | Amazing World of Gumball | The Law; The Allergy
 7:00 am | Amazing World of Gumball | The Password; The Mothers
 7:30 am | Amazing World of Gumball | The Procrastinators; The Shell
 8:00 am | Teen Titans Go! | Cy & Beasty; T Is for Titans
 8:30 am | Teen Titans Go! | The Brain of the Family
 9:00 am | Teen Titans Go! | Arthur
 9:15 am | Teen Titans Go! | Toilet Water
 9:30 am | Looney Tunes Cartoons | Sam-merica/Put the Cat Out - Door Spin/Barbecue Bandit / Happy Birdy To You/Daffy Psychic: New Love/Spring Forward, Fall Flat
10:00 am | We Baby Bears | The Little Fallen Star
10:30 am | Total DramaRama | Weekend at Buddy's; Waterhose-Five
11:00 am | Total DramaRama | Macarthur Park; Cody the Barbarian
11:30 am | Total DramaRama | Last Mom Standing; Tp2: Judgement Bidet
12:00 pm | Craig of the Creek | Return of the Honeysuckle Rangers; Kelsey the Author
12:30 pm | Craig of the Creek | Fort Williams; Cousin of the Creek
 1:00 pm | Teen Titans Go! | Arthur; Toilet Water
 1:30 pm | Teen Titans Go! | Eebows; Creative Geniuses
 2:00 pm | Teen Titans Go! | Batman's Birthday Gift; Manor and Mannerisms
 2:30 pm | Teen Titans Go! | What a Boy Wonders; Trans Oceanic Magical Cruise
 3:00 pm | Amazing World of Gumball | The Mirror; The Burden
 3:30 pm | Amazing World of Gumball | The Bros; The Man
 4:00 pm | Amazing World of Gumball | The Pizza; The Lie
 4:30 pm | Amazing World of Gumball | The Butterfly; The Question
 5:00 pm | Big Top Scooby-Doo! | 
 6:45 pm | Amazing World of Gumball | The Awkwardness