# 2023-05-12
 6:00 am | Teen Titans Go! | Kyle
 6:30 am | Amazing World of Gumball | The Boss; The Move
 7:00 am | Amazing World of Gumball | The Mothers; The Password
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Bam's Clawful Mistake
 8:00 am | Cocomelon | Winter Show and Tell
 8:15 am | Mecha Builders | Yip Yip Book Book; Stick To It
 8:45 am | Bugs Bunny Builders | Blast Off
 9:00 am | New Looney Tunes | Ice Ice Bunny; Not Lyin' Lion
 9:30 am | New Looney Tunes | All Belts Are Off; Wabbit's Wild
10:00 am | Total DramaRama | Total Trauma Rama
10:30 am | Total DramaRama | The Doomed Ballooned Marooned
11:00 am | Craig of the Creek | The Quick Name
11:30 am | Craig of the Creek | The Chef's Challenge
12:00 pm | Clarence | Lizard Day Afternoon
12:30 pm | Clarence | The Forgotten
 1:00 pm | Teen Titans Go! | Girls' Night Out; You're Fired
 1:30 pm | Teen Titans Go! | Super Robin; Tower Power
 2:00 pm | Amazing World of Gumball | The Nemesis
 2:30 pm | Amazing World of Gumball | The Crew
 3:00 pm | We Baby Bears | The Magical Box
 3:30 pm | Amazing World of Gumball | The Others
 4:00 pm | Amazing World of Gumball | The Signature
 4:30 pm | Amazing World of Gumball | The Gift
 5:00 pm | Adventure Time | The Eyes
 5:30 pm | Adventure Time | Mortal Recoil
 6:00 pm | Steven Universe | Alone Together
 6:30 pm | Steven Universe | Sworn to the Sword