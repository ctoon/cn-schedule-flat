# 2023-05-07
 7:45 pm | Unicorn: Warriors Eternal | The Awakening
 8:15 pm | Unicorn: Warriors Eternal | The Awakening, Part 2
 8:45 pm | Adult Swim Smalls Presents | Adult Swim Smalls Presents: SHOP: A Pop Opera
 9:00 pm | American Dad! | Langley Dollar Listings
 9:30 pm | American Dad! | Dressed Down
10:00 pm | American Dad! | The Book of Fischer
10:30 pm | Rick and Morty | Rick: A Mort Well Lived
11:00 pm | Royal Crackers | Casa de Darby
11:30 pm | Royal Crackers | The .1%
12:00 am | YOLO: Silver Destiny | The Parents Episode
12:15 am | Smiling Friends | Desmond's Big Day Out
12:30 am | Aqua TV Show Show | The Dudies
12:45 am | Aqua TV Show Show | Merlo Sauvignon Blanco
 1:00 am | American Dad! | Langley Dollar Listings
 1:30 am | American Dad! | The Book of Fischer
 2:00 am | Rick and Morty | Rick: A Mort Well Lived
 2:30 am | Royal Crackers | Casa de Darby
 3:00 am | Royal Crackers | The .1%
 3:30 am | YOLO: Silver Destiny | The Parents Episode
 3:45 am | Smiling Friends | Desmond's Big Day Out
 4:00 am | Futurama | How Hermes Requisitioned His Groove Back
 4:30 am | Futurama | A Clone of My Own
 5:00 am | Futurama | The Deep South
 5:30 am | Futurama | Bender Gets Made