# 2023-05-26
 6:00 am | Amazing World of Gumball | The Nuisance
 6:30 am | Amazing World of Gumball | The Best
 7:00 am | Amazing World of Gumball | The Heist
 7:30 am | Cocomelon | Boo Boo Song
 7:45 am | Meet the Batwheels | Sidekicked to the Curb
 8:00 am | Cocomelon | Shape Song
 8:15 am | Mecha Builders | Mecha Machine Makers; That's The Way The Windmill Blows
 8:45 am | Bugs Bunny Builders | K-9: Space Puppy
 9:00 am | New Looney Tunes | Airpork Security; Home a Clone
 9:30 am | New Looney Tunes | Bugs on Ice; Bug Scouts
10:00 am | Total DramaRama | Melter Skelter
10:30 am | Total DramaRama | The Never Gwending Story
11:00 am | Teen Titans Go! | Arthur
11:30 am | Teen Titans Go! | Plot Holes
12:00 pm | Clarence | Capture the Flag
12:30 pm | Clarence | Hairence
 1:00 pm | Craig of the Creek | Itch to Explore
 1:30 pm | Craig of the Creek | Jessica Goes to the Creek
 2:00 pm | Craig of the Creek | Too Many Treasures
 2:30 pm | Craig of the Creek | Sunday Clothes
 3:00 pm | Craig of the Creek | Monster in the Garden
 3:30 pm | Craig of the Creek | Dog Decider
 4:00 pm | Craig of the Creek | Lost in the Sewer
 4:30 pm | Craig of the Creek | The Brood
 5:00 pm | Craig of the Creek | The Invitation
 5:30 pm | Craig of the Creek | Kelsey Quest
 6:00 pm | Craig of the Creek | Ace of Squares
 6:30 pm | Craig of the Creek | The Last Kid in the Creek