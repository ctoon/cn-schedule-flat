# 2023-05-02
 6:00 am | Amazing World of Gumball | The Dream; The Sidekick
 6:30 am | Amazing World of Gumball | Hero; The Photo; the Future Tense
 7:00 am | Teen Titans Go! | The Jam; Drip
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | The Bam's Clawful Mistake Best Present in the World
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Joke Is on Thomas; The When I Go Fast
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Squirreled Away
 9:00 am | New Looney Tunes | Armageddon Outta Here Part 1 Armageddon Outta Here Part 2; Lifestyles of the Wealthy and Obnoxious Starship Mentalprise; the Hau
 9:30 am | New Looney Tunes | Substitute Porky Viktor the Psychic; State Fair and Balanced Pussyfoot Soldier Don't Be Fooled by His Size
10:00 am | Teen Titans Go! | Villains in a Van Getting Gelato; Pig in a Poke
10:30 am | Teen Titans Go! | I Am Chair; P.P.
11:00 am | Craig of the Creek | Plush Kingdom Creature Feature
11:30 am | Craig of the Creek | The Ice Pop Trio; King of Camping
12:00 pm | Clarence | The Money Broom Wizard; Big Petey Pizza Problem
12:30 pm | Clarence | The Lost in the Supermarket; Break Up
 1:00 pm | Teen Titans Go! | A Bumgorf; Little Help Please
 1:30 pm | Teen Titans Go! | Marv Wolfman and George Pérez; Cy & Beasty
 2:00 pm | Amazing World of Gumball | The End; The DVD
 2:30 pm | Amazing World of Gumball | The Knights; The Colossus
 3:00 pm | We Baby Bears | Ice Bear's Pet; Witches
 3:30 pm | Amazing World of Gumball | The Limit; The Game
 4:00 pm | Amazing World of Gumball | The Flower; The Banana
 4:30 pm | Amazing World of Gumball | The Phone; The Job
 5:00 pm | Craig of the Creek | Alternate Creekiverse; Snow Place Like Home
 5:30 pm | Teen Titans Go! | Manor and Mannerisms; What a Boy Wonders
 6:00 pm | Teen Titans Go! | Trans Oceanic Magical Cruise; Doomsday Preppers
 6:30 pm | Scooby-Doo! and Guess Who? | When Urkel-Bots Go Bad!