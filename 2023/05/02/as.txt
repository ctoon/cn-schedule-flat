# 2023-05-02
 7:00 pm | [missing data] | [missing data]
 8:00 pm | King Of the Hill | What Makes Bobby Run?
 8:30 pm | King Of the Hill | Chasing Bobby
 9:00 pm | Bob's Burgers | Glued, Where's My Bob?
 9:30 pm | Bob's Burgers | Flu-Ouise
10:00 pm | American Dad! | Shell Game
10:30 pm | American Dad! | The Mural of the Story
11:00 pm | American Dad! | (You Gotta) Strike for Your Right
11:30 pm | Rick and Morty | One Crew Over the Crewcoo's Morty
12:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
12:30 am | Royal Crackers | Pilot
 1:00 am | Mike Tyson Mysteries | The Bucket List
 1:15 am | Mike Tyson Mysteries | The Missing Package
 1:30 am | Aqua Unit Patrol Squad 1 | The Creditor
 1:45 am | Aqua Unit Patrol Squad 1 | Vampirus
 2:00 am | Robot Chicken | Batman Forever 21
 2:15 am | Robot Chicken | The Hobbit: There and Bennigan's
 2:30 am | Rick and Morty | One Crew Over the Crewcoo's Morty
 3:00 am | Rick and Morty | Claw and Hoarder: Special Ricktim's Morty
 3:30 am | Royal Crackers | Pilot
 4:00 am | Futurama | Bender's Big Score Part 3
 4:30 am | Futurama | Bender's Big Score Part 4
 5:00 am | Bob's Burgers | Glued, Where's My Bob?
 5:30 am | Bob's Burgers | Flu-Ouise