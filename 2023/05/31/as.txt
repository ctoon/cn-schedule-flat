# 2023-05-31
 7:00 pm | King of the Hill | Business Is Picking Up
 7:30 pm | King of the Hill | The Year of Washing Dangerously
 8:00 pm | King of the Hill | Hank Fixes Everything
 8:30 pm | King of the Hill | Church Hopping
 9:00 pm | Bob's Burgers | Sacred Cow
 9:30 pm | Bob's Burgers | Sexy Dance Fighting
10:00 pm | American Dad! | The Sinister Fate!!
10:30 pm | American Dad! | Dr. Sunderson's SunSuckers
11:00 pm | American Dad! | Family Time
11:30 pm | Rick and Morty | Morty's Mind Blowers
12:00 am | Rick and Morty | The ABC's of Beth
12:30 am | Royal Crackers | The .1%
 1:00 am | Robot Chicken | Why Is It Wet?
 1:15 am | Robot Chicken | Jew #1 Opens a Treasure Chest
 1:30 am | Aqua Unit Patrol Squad 1 | Intervention
 1:45 am | Aqua Unit Patrol Squad 1 | Freedom Cobra
 2:00 am | The Boondocks | The Story of Catcher Freeman
 2:30 am | Rick and Morty | Morty's Mind Blowers
 3:00 am | Rick and Morty | The ABC's of Beth
 3:30 am | Royal Crackers | The .1%
 4:00 am | Futurama | Saturday Morning Fun Pit
 4:30 am | Futurama | Calculon 2.0
 5:00 am | Bob's Burgers | Sacred Cow
 5:30 am | Bob's Burgers | Sexy Dance Fighting