# 2023-05-31
 6:00 am | The Amazing World of Gumball: Darwin's Yearbook | Banana Joe
 7:30 am | Cocomelon | My Big Brother Song
 7:45 am | Meet the Batwheels | Buff in a China Shop
 8:00 am | Cocomelon | My Big Brother Song
 8:15 am | Thomas & Friends: All Engines Go | Hay Fort Frenzy
 8:30 am | Cocomelon | Row Row Row Your Boat
 8:45 am | Bugs Bunny Builders | Race Track Race
 9:00 am | New Looney Tunes | Rabbits of the Lost Ark; Appropriate Technology
 9:30 am | We Baby Bears | Big Trouble Little Babies
10:00 am | Total DramaRama | Gnome More Mister Nice Guy
10:30 am | Total DramaRama | Look Who's Clocking
11:00 am | Craig of the Creek | Left in the Dark
11:30 am | Craig of the Creek | Lost in the Sewer
12:00 pm | Clarence | Motel
12:30 pm | Clarence | Merry Moochmas
 1:00 pm | Teen Titans Go! | The Spice Game
 1:30 pm | Teen Titans Go! | I'm the Sauce
 2:00 pm | Amazing World of Gumball | The Drama
 2:30 pm | Amazing World of Gumball | The Buddy
 3:00 pm | Amazing World of Gumball | The Mothers; The Password
 3:30 pm | Amazing World of Gumball | The Possession
 4:00 pm | Amazing World of Gumball | The Master
 4:30 pm | Amazing World of Gumball | The Silence
 5:00 pm | Craig of the Creek | Bernard of the Creek, Part 1
 5:30 pm | Teen Titans Go! | Hey You, Don't Forget About Me in Your Memory
 6:00 pm | Teen Titans Go! | Accept the Next Proposition You Hear
 6:30 pm | We Bare Bears | Our Stuff