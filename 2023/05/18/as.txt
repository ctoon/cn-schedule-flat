# 2023-05-18
 7:00 pm | King of the Hill | Patch Boomhauer
 7:30 pm | King of the Hill | Reborn To Be Wild
 8:00 pm | King of the Hill | New Cowboy on the Block
 8:30 pm | King of the Hill | The Incredible Hank
 9:00 pm | Bob's Burgers | Sit Me Baby One More Time
 9:30 pm | Bob's Burgers | Thanks-hoarding
10:00 pm | American Dad! | Tapped Out
10:30 pm | American Dad! | Into the Woods
11:00 pm | American Dad! | One Fish, Two Fish
11:30 pm | Rick and Morty | Rixty Minutes
12:00 am | Unicorn: Warriors Eternal | What Lies Beneath
12:30 am | Royal Crackers | Pilot
 1:00 am | Mike Tyson Mysteries | Love Letters
 1:15 am | Mike Tyson Mysteries | All About That Bass
 1:30 am | Aqua TV Show Show | Piranha Germs
 1:45 am | Aqua TV Show Show | Spacecadeuce
 2:00 am | Robot Chicken | Not Enough Women
 2:15 am | Robot Chicken | The Angelic Sounds of Mike Giggling
 2:30 am | Rick and Morty | Rixty Minutes
 3:00 am | Rick and Morty | Something Ricked This Way Comes
 3:30 am | Royal Crackers | Pilot
 4:00 am | Futurama | Yo Leela Leela
 4:30 am | Futurama | All the Presidents' Heads
 5:00 am | Bob's Burgers | Sit Me Baby One More Time
 5:30 am | Bob's Burgers | Thanks-hoarding