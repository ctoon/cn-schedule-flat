# 2023-05-17
 7:00 pm | [missing data] | [missing data]
 8:00 pm | King Of the Hill | Night and Deity
 8:30 pm | King Of the Hill | Maid in Arlen
 9:00 pm | Bob's Burgers | The Silence of the Louise
 9:30 pm | Bob's Burgers | The Wolf of Wharf Street
10:00 pm | American Dad! | Downtown
10:30 pm | American Dad! | Cheek to Cheek: A Stripper's Story
11:00 pm | American Dad! | A Starboy is Born
11:30 pm | Rick and Morty | Rick Potion #9
12:00 am | Rick and Morty | Raising Gazorpazorp
12:30 am | Royal Crackers | Casa de Darby
 1:00 am | Mike Tyson Mysteries | Help a Brother Out
 1:15 am | Mike Tyson Mysteries | The Beginning
 1:30 am | Aqua TV Show Show | Merlo Sauvignon Blanco
 1:45 am | Aqua TV Show Show | Storage Zeebles
 2:00 am | Robot Chicken | Secret of the Flushed Footlong
 2:15 am | Robot Chicken | Food
 2:30 am | Rick and Morty | Rick Potion #9
 3:00 am | Rick and Morty | Raising Gazorpazorp
 3:30 am | Royal Crackers | Casa de Darby
 4:00 am | Futurama | Law & Oracle
 4:30 am | Futurama | The Silence of the Clamps
 5:00 am | Bob's Burgers | The Silence of the Louise
 5:30 am | Bob's Burgers | The Wolf of Wharf Street