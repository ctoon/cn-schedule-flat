# 2023-05-30
 7:00 pm | King of the Hill | Bill's House
 7:30 pm | King of the Hill | Harlottown
 8:00 pm | King of the Hill | A Portrait of the Artist as a Young Clown
 8:30 pm | King of the Hill | Orange You Sad I Did Say Banana?
 9:00 pm | Bob's Burgers | Human Flesh
 9:30 pm | Bob's Burgers | Crawl Space
10:00 pm | American Dad! | Flush After Reading
10:30 pm | American Dad! | Comb Over: A Hair Piece
11:00 pm | American Dad! | Plot Heavy
11:30 pm | Rick and Morty | Rest and Ricklaxation
12:00 am | Rick and Morty | The Ricklantis Mixup
12:30 am | Royal Crackers | Mayworth
 1:00 am | Robot Chicken | What Can You Tell Me About Butt Rashes?
 1:15 am | Robot Chicken | Gimme That Chocolate Milk
 1:30 am | Aqua Unit Patrol Squad 1 | Allen
 2:00 am | The Boondocks | The S Word
 2:30 am | Rick and Morty | Rest and Ricklaxation
 3:00 am | Rick and Morty | The Ricklantis Mixup
 3:30 am | Royal Crackers | Mayworth
 4:00 am | Futurama | Forty Percent Leadbelly
 4:30 am | Futurama | The Inhuman Torch
 5:00 am | Bob's Burgers | Human Flesh
 5:30 am | Bob's Burgers | Crawl Space