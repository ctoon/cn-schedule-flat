# 2023-05-19
 7:00 pm | Unicorn: Warriors Eternal | A Fateful Encounter
 7:30 pm | Unicorn: Warriors Eternal | What Lies Beneath
 8:00 pm | King of the Hill | Meet the Propaniacs
 8:30 pm | King of the Hill | Nancy Boys
 9:00 pm | Bob's Burgers | The Bleakening Part 1
 9:30 pm | Bob's Burgers | The Bleakening Part 2
10:00 pm | American Dad! | Exquisite Corpses
10:30 pm | American Dad! | Trophy Wife, Trophy Life
11:00 pm | American Dad! | Game Night
11:30 pm | Rick and Morty | Close Rick-Counters of the Rick Kind
12:00 am | Unicorn: Warriors Eternal | The Awakening
12:30 am | Unicorn: Warriors Eternal | The Awakening, Part 2
 1:00 am | Unicorn: Warriors Eternal | A Fateful Encounter
 1:30 am | Unicorn: Warriors Eternal | What Lies Beneath
 2:00 am | Royal Crackers | CrackerCon
 2:30 am | Rick and Morty | Close Rick-Counters of the Rick Kind
 3:00 am | Futurama | Mobius Dick
 3:30 am | Futurama | Fry Am the Egg Man
 4:00 am | Futurama | The Tip of the Zoidberg
 4:30 am | Futurama | Cold Warriors
 5:00 am | Bob's Burgers | The Bleakening Part 1
 5:30 am | Bob's Burgers | The Bleakening Part 2