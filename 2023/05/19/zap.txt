# 2023-05-19
 6:00 am | Amazing World of Gumball | The Signal
 6:30 am | Amazing World of Gumball | The Girlfriend
 7:00 am | Amazing World of Gumball | The Parasite
 7:30 am | Cocomelon | Are You Sleeping (Brother John)?
 7:45 am | Meet the Batwheels | Cave Sweet Cave
 8:00 am | Cocomelon | Twinkle Twinkle Little Star
 8:15 am | Mecha Builders | The Treasure Of Treetop Woods; The Zee Mobile
 8:45 am | Bugs Bunny Builders | Big Feet
 9:00 am | New Looney Tunes | Snow Wabbit; Aromatherapest
 9:30 am | New Looney Tunes | Raising Your Spirits; Dust Bugster
10:00 am | Total DramaRama | Hic Hic Hooray
10:30 am | Total DramaRama | Bananas & Cheese
11:00 am | Craig of the Creek | My Stare Lady
11:30 am | Craig of the Creek | Silver Fist Returns
12:00 pm | Clarence | Jeff Wins
12:30 pm | Clarence | Suspended
 1:00 pm | Teen Titans Go! | Pirates; I See You
 1:30 pm | Teen Titans Go! | Brian; Nature
 2:00 pm | Amazing World of Gumball | The News
 2:30 pm | Amazing World of Gumball | The Vase
 3:00 pm | We Baby Bears | The Little Fallen Star
 3:30 pm | Amazing World of Gumball | The Ollie
 4:00 pm | Amazing World of Gumball | The Potato
 4:30 pm | Amazing World of Gumball | The Sorcerer
 5:00 pm | Adventure Time | Adventure Time With Fionna and Cake
 5:30 pm | Adventure Time | Holly Jolly Secrets Part II
 6:00 pm | Adventure Time | The Creeps
 6:30 pm | Adventure Time | The Vault