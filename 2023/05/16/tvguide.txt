# 2023-05-16
 6:00 am | Amazing World of Gumball | The Misunderstandings; The Styles
 6:30 am | Amazing World of Gumball | Roots; The Test; the Zoom Oil
 7:00 am | Teen Titans Go! | Dreams Grandma Voice; Monster Truck
 7:30 am | Cocomelon | 
 7:45 am | Meet the Batwheels | Zoomsday Batwingin' It
 8:00 am | Cocomelon | 
 8:15 am | Thomas & Friends: All Engines Go | Speedster Sandy How to Solve a Mystery
 8:30 am | Cocomelon | 
 8:45 am | Bugs Bunny Builders | Stories
 9:00 am | New Looney Tunes | Bugs in the Garden Scarecrow; Bonjour; Darkbat Renaissance Fair-Thee Well; Shape Shift
 9:30 am | New Looney Tunes | Painter Paint Hare Spy Who Bugged Me; The Cinderporker Part 1 Cinderporker Part 2 Hard Hat Time
10:00 am | Teen Titans Go! | Colors of Raven; The Left Leg
10:30 am | Teen Titans Go! | Books; Lazy Sunday
11:00 am | Craig of the Creek | The Dream Team; The Anniversary Box
11:30 am | Craig of the Creek | Fire & Ice; Lost & Found
12:00 pm | Clarence | Too Gross for Comfort; Clarence Wendle and the Eye of Coogan
12:30 pm | Clarence | Pilot Expansion; Sneaky Peeky
 1:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 1:30 pm | Teen Titans Go! | No Power; Sidekick
 2:00 pm | Amazing World of Gumball | The Detective; The Signal
 2:30 pm | Amazing World of Gumball | The Fury; The Girlfriend
 3:00 pm | We Baby Bears | Boo-Dunnit; Excalibear
 3:30 pm | Amazing World of Gumball | The Parasite; The Compilation
 4:00 pm | Amazing World of Gumball | The Disaster; The Re-run
 4:30 pm | Amazing World of Gumball | The Love; The Blame
 5:00 pm | Craig of the Creek | Opposite Day; Bored Games
 5:30 pm | Teen Titans Go! | Brain Food; In and Out
 6:00 pm | Teen Titans Go! | Little Buddies; Missing
 6:30 pm | Scooby-Doo! and Guess Who? | Hollywood Knights!