# 2023-05-16
 6:00 am | Amazing World of Gumball | The Misunderstandings
 6:30 am | Amazing World of Gumball | The Roots
 7:00 am | Amazing World of Gumball | The Slide
 7:30 am | Cocomelon | Fix It
 7:45 am | Meet the Batwheels | Zoomsday
 8:00 am | Cocomelon | Yes Yes Bed Time Camping
 8:15 am | Thomas & Friends: All Engines Go | Speedster Sandy
 8:30 am | Cocomelon | Wash Your Hands Song
 8:45 am | Bugs Bunny Builders | Stories
 9:00 am | New Looney Tunes | Bugs in the Garden; Scarecrow
 9:30 am | New Looney Tunes | Painter Paint Hare; The Spy Who Bugged Me
10:00 am | Total DramaRama | Total Dramarama: A Very Special Special That's Quite Special
11:00 am | Craig of the Creek | The Dream Team
11:30 am | Craig of the Creek | Fire & Ice
12:00 pm | Clarence | Too Gross for Comfort
12:30 pm | Clarence | Pilot Expansion
 1:00 pm | Teen Titans Go! | Power Moves; Staring at the Future
 1:30 pm | Teen Titans Go! | No Power; Sidekick
 2:00 pm | Amazing World of Gumball | The Signal
 2:30 pm | Amazing World of Gumball | The Girlfriend
 3:00 pm | We Baby Bears | Boo-Dunnit
 3:30 pm | Amazing World of Gumball | The Parasite
 4:00 pm | Amazing World of Gumball | The Disaster
 4:30 pm | Amazing World of Gumball | The Blame
 5:00 pm | Craig of the Creek | Opposite Day
 5:30 pm | Teen Titans Go! | Brain Food; In and Out
 6:00 pm | Teen Titans Go! | Little Buddies; Missing
 6:30 pm | Scooby-Doo! and Guess Who? | Hollywood Knights!