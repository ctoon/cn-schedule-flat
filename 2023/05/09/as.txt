# 2023-05-09
 7:00 pm | [missing data] | [missing data]
 8:00 pm | King Of the Hill | Of Mice and Little Green Men
 8:30 pm | King Of the Hill | A Man Without a Country Club
 9:00 pm | Bob's Burgers | Like Gene for Chocolate
 9:30 pm | Bob's Burgers | The Grand Mama-Pest Hotel
10:00 pm | American Dad! | The Future Is Borax
10:30 pm | American Dad! | Fantasy Baseball
11:00 pm | American Dad! | I Am The Jeans: The Gina Lavetti Story
11:30 pm | Rick and Morty | A Rickconvenient Mort
12:00 am | Rick and Morty | Rickdependence Spray
12:30 am | Royal Crackers | Stebe
 1:00 am | Mike Tyson Mysteries | The Gift That Keeps on Giving
 1:15 am | Mike Tyson Mysteries | Your Old Man
 1:30 am | Aqua Something You Know Whatever | Shirt Herpes
 1:45 am | Aqua Something You Know Whatever | Rocket Horse and Jet Chicken
 2:00 am | Robot Chicken | The Unnamed One
 2:15 am | Robot Chicken | Zero Vegetables
 2:30 am | Rick and Morty | A Rickconvenient Mort
 3:00 am | Rick and Morty | Rickdependence Spray
 3:30 am | Royal Crackers | Stebe
 4:00 am | Futurama | Into the Wild Green Yonder Part 3
 4:30 am | Futurama | Into the Wild Green Yonder Part 4
 5:00 am | Bob's Burgers | Like Gene for Chocolate
 5:30 am | Bob's Burgers | The Grand Mama-Pest Hotel