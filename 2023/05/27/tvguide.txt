# 2023-05-27
 6:00 am | Craig of the Creek | The Last Kid in the Creek; The Climb
 6:30 am | Craig of the Creek | The Kid From 3030; Big Pinchy
 7:00 am | Craig of the Creek | Creek Cart Racers; Power Punchers
 7:30 am | Craig of the Creek | Secret Book Club; Jextra Perrestrial
 8:00 am | Craig of the Creek | Dinner At the Creek; The Takeout Mission
 8:30 am | Craig of the Creek | Jessica's Trail; Bug City
 9:00 am | Craig of the Creek | Shortcut; The; Deep Creek Salvage
 9:30 am | Craig of the Creek | The Great Fossil Rush; Dibs Court
10:00 am | Craig of the Creek | Alone Quest; The Mystery Of The Timekeeper
10:30 am | Craig of the Creek | Memories Of Bobby; Jacob Of The Creek
11:00 am | Craig of the Creek | Return of the Honeysuckle Rangers; Kelsey the Elder
11:30 am | Craig of the Creek | Fort Williams Sour Candy Trials
12:00 pm | Craig of the Creek | The Other Side
12:30 pm | Craig of the Creek | Summer Wish ;Turning the Tables
 1:00 pm | Craig of the Creek | Council Of The Creek; Kelsey The Author
 1:30 pm | Craig of the Creek | Cousin Of The Creek; Sparkle Cadet
 2:00 pm | Craig of the Creek | Camper on the Run; Stink Bomb
 2:30 pm | Craig of the Creek | The Evolution of Craig; The Haunted Dollhouse
 3:00 pm | Craig of the Creek | Craig And The Kid's Table
 3:30 pm | Craig of the Creek | Creek Daycare; Sugar Smugglers
 4:00 pm | Craig of the Creek | Sleepover at Jp's Tea Timer's Ball
 4:30 pm | Craig of the Creek | Cardboard Identity; The Ancients of the Creek
 5:00 pm | Craig of the Creek | Secret in a Bottle; Mortimor to the Rescue
 5:30 pm | Craig of the Creek | Crisis at Elder Rock; Trading Day
 6:00 pm | Craig of the Creek | The End Was Here; Kelsey the Worthy
 6:30 pm | Craig of the Creek | The in the Key of the Creek Children of the Dog